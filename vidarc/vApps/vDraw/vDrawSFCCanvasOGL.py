#Boa:Dialog:vgdSFCstep
#----------------------------------------------------------------------------
# Name:         vDrawSFCCanvasOGL.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawSFCCanvasOGL.py,v 1.2 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl
import string,os,sys

import vidarc.vApps.vDraw.latex as latex
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import images

#from vidarc.vApps.vDraw.voglSFCstepActions import voglSFCstepActions

def create(parent):
    return vgdSFCstep(parent)

[wxID_VGDSFCSTEP, wxID_VGDSFCSTEPCBCANCEL, wxID_VGDSFCSTEPCHCCOMBINE, 
 wxID_VGDSFCSTEPCHCFOLLOWTYPE, wxID_VGDSFCSTEPCHCOPERATOR, 
 wxID_VGDSFCSTEPGCBBADD, wxID_VGDSFCSTEPGCBBADDACTION, 
 wxID_VGDSFCSTEPGCBBBROWSE, wxID_VGDSFCSTEPGCBBBROWSEVAL, 
 wxID_VGDSFCSTEPGCBBDEL, wxID_VGDSFCSTEPGCBBDELACTION, 
 wxID_VGDSFCSTEPGCBBDELETEFOLLOW, wxID_VGDSFCSTEPGCBBOK, 
 wxID_VGDSFCSTEPGCBBSET, wxID_VGDSFCSTEPLBLACTION, wxID_VGDSFCSTEPLBLCOMB, 
 wxID_VGDSFCSTEPLBLDESC, wxID_VGDSFCSTEPLBLLEVEL, wxID_VGDSFCSTEPLBLOP, 
 wxID_VGDSFCSTEPLBLSTEPDESC, wxID_VGDSFCSTEPLBLSTEPNAME, 
 wxID_VGDSFCSTEPLBLSTEPNR, wxID_VGDSFCSTEPLBLTAG, wxID_VGDSFCSTEPLBLTRANS, 
 wxID_VGDSFCSTEPLBLVAL, wxID_VGDSFCSTEPLSTACTION, wxID_VGDSFCSTEPLSTFOLLOW, 
 wxID_VGDSFCSTEPLSTTRANS, wxID_VGDSFCSTEPSPNLEVEL, wxID_VGDSFCSTEPTXTDESC, 
 wxID_VGDSFCSTEPTXTNAME, wxID_VGDSFCSTEPTXTNR, wxID_VGDSFCSTEPTXTSTEPDESC, 
 wxID_VGDSFCSTEPTXTSTEPNAME, wxID_VGDSFCSTEPTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(35)]

class vgdSFCstep(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDSFCSTEP, name=u'vgdSFCstep',
              parent=prnt, pos=wx.Point(281, 24), size=wx.Size(480, 509),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'SFC Step')
        self.SetClientSize(wx.Size(472, 482))

        self.txtNr = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTNR, name=u'txtNr',
              parent=self, pos=wx.Point(24, 4), size=wx.Size(48, 21), style=0,
              value=u'')

        self.lblStepNr = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPNR, label=u'Nr',
              name=u'lblStepNr', parent=self, pos=wx.Point(4, 6),
              size=wx.Size(11, 13), style=0)

        self.txtStepName = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTSTEPNAME,
              name=u'txtStepName', parent=self, pos=wx.Point(112, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblStepName = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPNAME,
              label=u'Name', name=u'lblStepName', parent=self, pos=wx.Point(80,
              8), size=wx.Size(28, 13), style=0)

        self.txtStepDesc = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTSTEPDESC,
              name=u'txtStepDesc', parent=self, pos=wx.Point(280, 8),
              size=wx.Size(184, 88), style=wx.TE_MULTILINE, value=u'')

        self.lblStepDesc = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPDESC,
              label=u'Description', name=u'lblStepDesc', parent=self,
              pos=wx.Point(224, 8), size=wx.Size(53, 13), style=0)

        self.chcFollowType = wx.CheckBox(id=wxID_VGDSFCSTEPCHCFOLLOWTYPE,
              label=u'parallel', name=u'chcFollowType', parent=self,
              pos=wx.Point(280, 104), size=wx.Size(53, 13), style=0)
        self.chcFollowType.SetValue(False)
        self.chcFollowType.Bind(wx.EVT_CHECKBOX, self.OnChcFollowTypeCheckbox,
              id=wxID_VGDSFCSTEPCHCFOLLOWTYPE)

        self.lstFollow = wx.ListView(id=wxID_VGDSFCSTEPLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(224, 84), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected, id=wxID_VGDSFCSTEPLSTFOLLOW)

        self.gcbbDeleteFollow = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDELETEFOLLOW,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete',
              name=u'gcbbDeleteFollow', parent=self, pos=wx.Point(112, 120),
              size=wx.Size(76, 30), style=0)
        self.gcbbDeleteFollow.Bind(wx.EVT_BUTTON, self.OnGcbbDeleteFollowButton,
              id=wxID_VGDSFCSTEPGCBBDELETEFOLLOW)

        self.lstTrans = wx.ListView(id=wxID_VGDSFCSTEPLSTTRANS,
              name=u'lstTrans', parent=self, pos=wx.Point(8, 156),
              size=wx.Size(224, 88), style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstTrans.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTransListItemSelected, id=wxID_VGDSFCSTEPLSTTRANS)
        self.lstTrans.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstTransListItemDeselected, id=wxID_VGDSFCSTEPLSTTRANS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(40, 250), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VGDSFCSTEPGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(128, 250), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDSFCSTEPGCBBDEL)

        self.txtName = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(64, 290), size=wx.Size(312, 21),
              style=0, value=u'')

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(384, 288),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDSFCSTEPGCBBBROWSE)

        self.chcOperator = wx.Choice(choices=[u'', u'=', u'?', u'==', u'<',
              u'<=', u'>', u'>=', u'!=', u'1==1', u'0==1', u'?'],
              id=wxID_VGDSFCSTEPCHCOPERATOR, name=u'chcOperator', parent=self,
              pos=wx.Point(8, 330), size=wx.Size(50, 21), style=0)
        self.chcOperator.SetSelection(0)
        self.chcOperator.SetToolTipString(u'operation')

        self.txtVal = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTVAL, name=u'txtVal',
              parent=self, pos=wx.Point(64, 330), size=wx.Size(312, 21),
              style=0, value=u'')

        self.gcbbBrowseVal = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBBROWSEVAL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseVal', parent=self, pos=wx.Point(384, 326),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseVal.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseValButton,
              id=wxID_VGDSFCSTEPGCBBBROWSEVAL)

        self.chcCombine = wx.Choice(choices=[u'and', u'nand', u'or', u'nor'],
              id=wxID_VGDSFCSTEPCHCCOMBINE, name=u'chcCombine', parent=self,
              pos=wx.Point(64, 356), size=wx.Size(50, 21), style=0)
        self.chcCombine.SetSelection(0)

        self.spnLevel = wx.SpinCtrl(id=wxID_VGDSFCSTEPSPNLEVEL, initial=0,
              max=10, min=0, name=u'spnLevel', parent=self, pos=wx.Point(160,
              356), size=wx.Size(50, 21), style=wx.SP_ARROW_KEYS)
        self.spnLevel.SetValue(0)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(384, 360), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDSFCSTEPGCBBSET)

        self.lblTag = wx.StaticText(id=wxID_VGDSFCSTEPLBLTAG, label=u'Tag',
              name=u'lblTag', parent=self, pos=wx.Point(32, 294),
              size=wx.Size(22, 13), style=0)

        self.lblOp = wx.StaticText(id=wxID_VGDSFCSTEPLBLOP, label=u'Operation',
              name=u'lblOp', parent=self, pos=wx.Point(8, 314), size=wx.Size(46,
              13), style=0)

        self.lblVal = wx.StaticText(id=wxID_VGDSFCSTEPLBLVAL, label=u'Value',
              name=u'lblVal', parent=self, pos=wx.Point(68, 314),
              size=wx.Size(27, 13), style=0)

        self.lblLevel = wx.StaticText(id=wxID_VGDSFCSTEPLBLLEVEL,
              label=u'Level', name=u'lblLevel', parent=self, pos=wx.Point(128,
              360), size=wx.Size(26, 13), style=0)

        self.lblComb = wx.StaticText(id=wxID_VGDSFCSTEPLBLCOMB,
              label=u'Combine', name=u'lblComb', parent=self, pos=wx.Point(8,
              360), size=wx.Size(41, 13), style=0)

        self.lblTrans = wx.StaticText(id=wxID_VGDSFCSTEPLBLTRANS,
              label=u'Transition', name=u'lblTrans', parent=self,
              pos=wx.Point(8, 140), size=wx.Size(46, 13), style=0)

        self.lstAction = wx.ListCtrl(id=wxID_VGDSFCSTEPLSTACTION,
              name=u'lstAction', parent=self, pos=wx.Point(240, 156),
              size=wx.Size(224, 88), style=wx.LC_REPORT)
        self.lstAction.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstActionListItemSelected, id=wxID_VGDSFCSTEPLSTACTION)
        self.lstAction.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstActionListItemDeselected, id=wxID_VGDSFCSTEPLSTACTION)

        self.lblAction = wx.StaticText(id=wxID_VGDSFCSTEPLBLACTION,
              label=u'Action', name=u'lblAction', parent=self, pos=wx.Point(240,
              140), size=wx.Size(30, 13), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTDESC, name=u'txtDesc',
              parent=self, pos=wx.Point(8, 400), size=wx.Size(368, 40),
              style=wx.TE_MULTILINE, value=u'')

        self.lblDesc = wx.StaticText(id=wxID_VGDSFCSTEPLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(8, 384), size=wx.Size(53, 13), style=0)

        self.gcbbAddAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBADDACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add',
              name=u'gcbbAddAction', parent=self, pos=wx.Point(272, 250),
              size=wx.Size(76, 30), style=0)
        self.gcbbAddAction.Bind(wx.EVT_BUTTON, self.OnGcbbAddActionButton,
              id=wxID_VGDSFCSTEPGCBBADDACTION)

        self.gcbbDelAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDELACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete',
              name=u'gcbbDelAction', parent=self, pos=wx.Point(360, 250),
              size=wx.Size(76, 30), style=0)
        self.gcbbDelAction.Bind(wx.EVT_BUTTON, self.OnGcbbDelActionButton,
              id=wxID_VGDSFCSTEPGCBBDELACTION)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(128, 448), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDSFCSTEPGCBBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(216, 448), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VGDSFCSTEPCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.sortOrder=0
        self.sortOrderAction=0
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        self.actions=[]
        self.selTrans=-1
        self.selAction=-1
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        self.gcbbDelAction.SetBitmapLabel(img)
        self.gcbbDeleteFollow.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        self.gcbbAddAction.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowseVal.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,200)
        
        self.lstTrans.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,150)
        self.lstTrans.InsertColumn(2,u'Oper',wx.LIST_FORMAT_CENTER,50)
        self.lstTrans.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,70)
        self.lstTrans.InsertColumn(4,u'Comb',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(5,u'Level',wx.LIST_FORMAT_LEFT,30)
        
        self.lstAction.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,40)
        self.lstAction.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,120)
        self.lstAction.InsertColumn(2,u'Oper',wx.LIST_FORMAT_LEFT,40)
        self.lstAction.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,120)
        
        self.__setEnable__(False)
    def __setEnable__(self,state):
        #self.txtName.Enable(state)
        #self.chcOperator.Enable(state)
        #self.gcbbBrowse.Enable(state)
        #self.txtVal.Enable(state)
        #self.gcbbBrowseVal.Enable(state)
        #self.chcCombine.Enable(state)
        #self.spnLevel.Enable(state)
        self.gcbbSet.Enable(state)
    def __clear__(self):
        self.txtName.SetValue(u'')
        self.txtVal.SetValue(u'')
        self.txtDesc.SetValue(u'')
        self.chcOperator.SetSelection(0)
        self.spnLevel.SetValue(0)
    def SetNrs(self,nrs):
        self.nrs=nrs
    def SetInfos(self,voglSFCstep,voglDrawSFC):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglSFC=voglDrawSFC
        self.voglSFCstep=voglSFCstep
        self.txtNr.SetValue(self.voglSFCstep.nr)
        self.txtStepName.SetValue(self.voglSFCstep.name)
        self.txtStepDesc.SetValue(self.voglSFCstep.desc)
        self.actions=self.voglSFCstep.actions
        if self.voglSFCstep.parallel:
            self.chcFollowType.SetValue(True)
        else:
            self.chcFollowType.SetValue(False)
        self.__updateFollowing__()
        self.__updateActions__()
        self.cbCancel.Enable(True)
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgBrowseVal=dlg
    def __updateTrans__(self):
        self.selTrans=-1
        self.IDname=''
        self.IDval=''
        self.lstTrans.DeleteAllItems()
        self.__setEnable__(False)
        self.__clear__()
        if self.selFollow<0:
            return
        #self.txtName.SetValue('')
        #self.txtVal.SetValue('')
        self.selTrans=-1
        self.lstTrans.DeleteAllItems()
        transitionLst=self.voglSFCstep.nextSteps[self.__selFollow__()][2]
        # build steps
        transNames=[]
        i=0
        for a in transitionLst:
            transNames.append((a['name'],i))
            i+=1
        def cmpAct(x,y):
            return cmp(x[0],y[0])
        transNames.sort(cmpAct)
        if self.sortOrder>0:
            transNames.reverse()
        i=0
        for a in transNames:
            trans=transitionLst[a[1]]
            index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i),-1)
            self.lstTrans.SetStringItem(index,1,trans['name'], -1)
            self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
            self.lstTrans.SetStringItem(index,3,trans['val'],-1)
            self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
            self.lstTrans.SetStringItem(index,5,trans['level'],-1)
            self.lstTrans.SetItemData(index,a[1])
            i+=1
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        lst=[]
        i=0
        for s in self.voglSFCstep.nextSteps:
            lst.append((s[0].nr,s[0].GetName(),i))
            i+=1
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        lst.sort(cmpFunc)
        i=0
        for tup in lst:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, tup[0], -1)
            self.lstFollow.SetStringItem(index,1,tup[1],-1)
            self.lstFollow.SetItemData(index,tup[2])
            i+=1
            pass
        self.__updateTrans__()
        self.__clear__()
    def __selFollow__(self):
        if self.selFollow<0:
            return -1
        return self.lstFollow.GetItemData(self.selFollow)
    def __updateActions__(self):
        # build steps
        self.selIdx=-1
        self.IDname=''
        self.IDval=''
        self.lstAction.DeleteAllItems()
        self.__setEnable__(False)
        self.__clear__()
        actNames=[]
        i=0
        for a in self.actions:
            actNames.append((a['name'],i))
            i+=1
        def cmpAct(x,y):
            return cmp(x[0],y[0])
        actNames.sort(cmpAct)
        if self.sortOrderAction>0:
            actNames.reverse()
        i=0
        for a in actNames:
            index = self.lstAction.InsertImageStringItem(sys.maxint,"%03d"%i , -1)
            self.lstAction.SetStringItem(index,1,a[0],-1)
            try:
                sOp=self.actions[a[1]]['operation']
            except:
                sOp=''
            self.lstAction.SetStringItem(index,2,sOp,-1)
            try:
                sVal=self.actions[a[1]]['val']
            except:
                sVal=''
            self.lstAction.SetStringItem(index,3,sVal,-1)
            self.lstAction.SetItemData(index,a[1])
            i+=1
            pass
    def OnGcbbOkButton(self, event):
        self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglSFCstep.name=self.txtStepName.GetValue()
        self.voglSFCstep.desc=self.txtStepDesc.GetValue()
        #self.OnGcbbSetButton(None)
        self.EndModal(1)
        event.Skip()
    def OnGcbbAddButton(self, event):
        #print self.spnLevel.GetValue(),type(self.spnLevel.GetValue())
        if self.selFollow<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue()),
            'desc':self.txtDesc.GetValue()}
        #i=len(self.voglSFCstep.nextSteps[self.selFollow][2])
        #index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i), -1)
        #self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        #self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        #self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        #self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        #self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        self.voglSFCstep.nextSteps[self.__selFollow__()][2].append(trans)
        self.cbCancel.Enable(False)
        self.__updateTrans__()
        event.Skip()

    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        self.__updateTrans__()
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selTrans<0:
            return
        idx=self.lstTrans.GetItemData(self.selTrans)
        #self.lstTrans.DeleteItem(self.selTrans)
        #i=self.lstFollow.GetItemData(self.selFollow)
        s=self.voglSFCstep.nextSteps[self.__selFollow__()][0]
        l=self.voglSFCstep.nextSteps[self.__selFollow__()][1]
        lst=self.voglSFCstep.nextSteps[self.__selFollow__()][2]
        #lst=lst[:self.selTrans]+lst[self.selTrans+1:]
        lst=lst[:idx]+lst[idx+1:]
        self.voglSFCstep.nextSteps[self.__selFollow__()]=(s,l,lst)
        self.cbCancel.Enable(False)
        self.__updateTrans__()
        self.selTrans=-1
        #self.__updateFollowing__()
        event.Skip()
    
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            self.dlgBrowse.SelectByID(self.IDname)
            if self.dlgBrowse.ShowModal()>0:
                s=self.dlgBrowse.GetTagName()
                self.IDname=self.dlgBrowse.GetId()
                sOld=self.txtName.GetValue()
                s=self.__replace__(sOld,s)
                self.txtName.SetValue(s)
        event.Skip()

    def OnGcbbCreateButton(self, event):
        self.EndModal(2)
        event.Skip()

    def OnChcFollowTypeCheckbox(self, event):
        event.Skip()

    def OnLstTransListItemSelected(self, event):
        if self.selAction>=0:
            self.lstAction.SetItemState(self.selAction,0,wx.LIST_STATE_SELECTED)
        self.selAction=-1
        self.selTrans=event.GetIndex()
        if self.selTrans<0:
            return
        self.__setEnable__(True)
        idx=event.GetIndex()
        it=self.lstTrans.GetItem(idx,1)
        self.txtName.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,2)
        self.chcOperator.SetStringSelection(it.m_text)
        
        it=self.lstTrans.GetItem(idx,3)
        self.txtVal.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,4)
        self.chcCombine.SetStringSelection(it.m_text)
        
        try:
            it=self.lstTrans.GetItem(idx,5)
            self.spnLevel.SetValue(int(it.m_text))
        except:
            self.spnLevel.SetValue(0)
        
        lst=self.voglSFCstep.nextSteps[self.__selFollow__()][2]
        idx=self.lstTrans.GetItemData(self.selTrans)
        trans=lst[idx]
        try:
            self.IDname=trans['fid_name']
        except:
            self.IDname=''
        try:
            self.IDval=trans['fid_val']
        except:
            self.IDval=''
        try:
            sDesc=trans['desc']
        except:
            sDesc=u''
        self.txtDesc.SetValue(sDesc)
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selFollow>=0:
            if self.selTrans>=0:
                trans={'name':self.txtName.GetValue(),
                    'fid_name':self.IDname,
                    'operation':self.chcOperator.GetStringSelection(),
                    'val':self.txtVal.GetValue(),
                    'fid_val':self.IDval,
                    'combine':self.chcCombine.GetStringSelection(),
                    'level':str(self.spnLevel.GetValue()),
                    'desc':self.txtDesc.GetValue()}
                #index = self.selTrans
                #self.lstTrans.SetStringItem(index,1,trans['name'],-1)
                #self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
                #self.lstTrans.SetStringItem(index,3,trans['val'],-1)
                #self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
                #self.lstTrans.SetStringItem(index,5,trans['level'],-1)
                idx=self.lstTrans.GetItemData(self.selTrans)
                
                s=self.voglSFCstep.nextSteps[self.__selFollow__()][0]
                l=self.voglSFCstep.nextSteps[self.__selFollow__()][1]
                lst=self.voglSFCstep.nextSteps[self.__selFollow__()][2]
                #lst=lst[:self.selTrans]+[trans]+lst[self.selTrans+1:]
                lst=lst[:idx]+[trans]+lst[idx+1:]
                self.voglSFCstep.nextSteps[self.__selFollow__()]=(s,l,lst)
                self.cbCancel.Enable(False)
                self.__updateTrans__()
        if self.selAction>=0:
            self.lstAction.SetStringItem(self.selActionIdx,1,self.txtName.GetValue())
            self.lstAction.SetStringItem(self.selActionIdx,2,self.chcOperator.GetStringSelection())
            self.lstAction.SetStringItem(self.selActionIdx,3,self.txtVal.GetValue())
            self.actions[self.selAction]['name']=self.txtName.GetValue()
            self.actions[self.selAction]['desc']=self.txtDesc.GetValue()
            self.actions[self.selAction]['operation']=self.chcOperator.GetStringSelection()
            self.actions[self.selAction]['val']=self.txtVal.GetValue()
            self.actions[self.selAction]['fid_name']=self.IDname
            self.actions[self.selAction]['fid_val']=self.IDval
            self.cbCancel.Enable(False)
            self.__updateActions__()
        if event is not None:
            event.Skip()

    def OnGcbbBrowseValButton(self, event):
        if self.dlgBrowseVal is not None:
            self.dlgBrowseVal.Centre()
            self.dlgBrowseVal.SelectByID(self.IDval)
            if self.dlgBrowseVal.ShowModal()>0:
                s=self.dlgBrowseVal.GetTagName()
                self.IDval=self.dlgBrowseVal.GetId()
                s=self.__replace__(self.txtVal.GetValue(),s)
                self.txtVal.SetValue(s)
        event.Skip()

    def OnGcbbDeleteFollowButton(self, event):
        if self.selFollow<0:
            return
        self.voglSFCstep.RemoveLink(self.__selFollow__())
        self.lstFollow.DeleteItem(self.__selFollow__())
        self.selFollow=-1
        self.__updateTrans__()
        event.Skip()

    def OnLstTransListItemDeselected(self, event):
        self.selTrans=-1
        self.selAction=-1
        self.__clear__()
        event.Skip()

    def OnLstActionListItemSelected(self, event):
        if self.selTrans>=0:
            self.lstTrans.SetItemState(self.selTrans,0,wx.LIST_STATE_SELECTED)
        self.selTrans=-1
        #self.selFollow=-1
        self.selActionIdx=event.GetIndex()
        it=self.lstAction.GetItem(self.selActionIdx,1)
        self.txtName.SetValue(it.m_text)
        self.selAction=self.lstAction.GetItemData(self.selActionIdx)
        try:
            self.txtDesc.SetValue(self.actions[self.selAction]['desc'])
        except:
            self.txtDesc.SetValue('')
        try:
            self.chcOperator.SetStringSelection(self.actions[self.selAction]['operation'])
        except:
            self.chcOperator.SetSelection(0)
        try:
            self.txtVal.SetValue(self.actions[self.selAction]['val'])
        except:
            self.txtVal.SetValue('')
        try:
            self.IDname=self.actions[self.selAction]['fid_name']
        except:
            self.IDname=''
        try:
            self.IDval=self.actions[self.selAction]['fid_val']
        except:
            self.IDval=''
        self.__setEnable__(True)
        event.Skip()

    def OnLstActionListItemDeselected(self, event):
        self.selAction=-1
        self.selTrans=-1
        self.__clear__()
        event.Skip()

    def OnGcbbAddActionButton(self, event):
        sName=self.txtName.GetValue()
        sDesc=self.txtDesc.GetValue()
        sOper=self.chcOperator.GetStringSelection()
        sVal=self.txtVal.GetValue()
        self.actions.append({'name':sName,'fid_name':self.IDname,'operation':sOper,
                'val':sVal,'fid_val':self.IDval,'desc':sDesc})
        self.cbCancel.Enable(False)
        self.__updateActions__()
        event.Skip()

    def OnGcbbDelActionButton(self, event):
        if self.selAction<0:
            return
        self.actions=self.actions[:self.selAction]+self.actions[self.selAction+1:]
        self.txtDesc.SetValue('')
        self.txtVal.SetValue('')
        self.IDname=''
        self.IDval=''
        self.cbCancel.Enable(False)
        self.__updateActions__()
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()


class voglSFCstep(ogl.CompositeShape):
    def __init__(self, nr,id,name,desc,actions,width, height, canvas):
        ogl.CompositeShape.__init__(self)
        self.nr=nr
        self.id=id
        self.name=name
        self.desc=desc
        self.parallel=0
        self.dlg=None
        self.canvas=canvas
        self.SetCanvas(canvas)
        self.prevStep=None
        self.nextSteps=[]
        self.prevSteps=[]
        
        # add a shape to the original division
        s=self.__getName__()
        grid=self.canvas.GetDiagram().GetGridSpacing()
        #wa=ba[0]/2.0+grid
        w=grid * int(len(s)*8 / grid + 1.5)
        #w=2.0 *grid*(int((wa+w)/2.0)/grid + 0.5)-wa
        
        self.step = ogl.RectangleShape(w, 40)
        self.step.AddText(s)
        self.__setSize__(s)
        
        self.step.SetDraggable(False)
        self.step.SetSensitivityFilter(0,True)
        self.AddChild(self.step)
        
        
        # and add a shape to the second division (and move it to the
        # centre of the division)
        self.actions = actions
        b2=self.step.GetBoundingBoxMax()
        #shape3.SetBrush(wx.CYAN_BRUSH)
        #self.GetDivisions()[1].AddChild(shape3)
        grid=self.canvas.GetDiagram().GetGridSpacing()
        
        self.Recompute()
    def __getName__(self):
        return u'Nr: %s %s'%(self.nr,self.name)
    def __setSize__(self,s):
        grid=self.canvas.GetDiagram().GetGridSpacing()
        #w=grid * int(w*8 / grid + 1.5)
        ba=self.step.GetBoundingBoxMax()
        wa=ba[0]/2.0+grid
        w=grid * int(len(s)*8 / grid + 1.5)
        w=2.0 *grid*(int((wa+w)/2.0)/grid + 0.5)-wa
        #w=grid * int(w*8 / grid + 0.5)
        #w=w*8
        self.SetSize(w,ba[1])
        self.ReformatRegions(self.canvas)
    def Update(self):
        b2=self.step.GetBoundingBoxMax()
        
        grid=self.canvas.GetDiagram().GetGridSpacing()
        
        self.Recompute()
        
        self.canvas.Refresh()
        pass
    def Delete(self):
        for n in self.nextSteps:
            n[0].prevSteps.remove(self)
            #for l in n[0].nextSteps:
                #if l[0]==self:
                #    print "found"
            n[1].Detach()
            n[1].RemoveFromCanvas(self.canvas)
            n[1].Unlink()
        for n in self.prevSteps:
            for l in n.nextSteps:
                if l[0]==self:
                    l[1].Detach()
                    l[1].RemoveFromCanvas(self.canvas)
                    l[1].Unlink()
                    n.nextSteps.remove(l)
        #self.Show(False)
        #self.Detach()
        #self.step.Unlink()
        self.RemoveFromCanvas(self.canvas)
        #self.canvas.RemoveShape(self)
        #self.canvas.GetDiagram().RemoveShape(self)
    def GetName(self):
        return self.name
    def GetDesc(self):
        return self.desc
    def AddLink(self,step,trans=None):
        line = ogl.LineShape()
        line.SetCanvas(self.canvas)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(6)
        
        #self.step.AddLine(line, step.step)
        self.canvas.GetDiagram().AddShape(line)
        #line.SetFrom(self)
        #line.SetTo(step)
        if trans is None:
            trans=[{'name':'',
                'operation':'0==1',
                'val':'',
                'combine':'and',
                'level':'0'}]
        self.nextSteps.append((step,line,trans))
        step.prevSteps.append(self)
        #self.AddChild(line)
        line.Show(True)
        self.__processLines__(None)
        
    def __processLines__(self,dc):
        for tup in self.nextSteps:
            
            xa=self.step.GetX()
            ya=self.step.GetY()
            ba=self.step.GetBoundingBoxMax()
            
            xe=tup[0].step.GetX()
            ye=tup[0].step.GetY()
            be=tup[0].step.GetBoundingBoxMax()
            
            if dc is not None:
                tup[1].Erase(dc)
            #tup[1].Show(False)
            if ya>=ye:
                #tup[1].MakeLineControlPoints(6)
                
                if xa<xe:
                    x=xa-ba[0]/2.0-20
                else:
                    x=xe-be[0]/2.0-20
                
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                controlPoints[1][0]=xa
                controlPoints[1][1]=ya+ba[1]/2.0+20
                
                controlPoints[2][0]=x
                controlPoints[2][1]=ya+ba[1]/2.0+20
                
                controlPoints[3][0]=x
                controlPoints[3][1]=ye-be[1]/2.0-20
                
                controlPoints[4][0]=xe
                controlPoints[4][1]=ye-be[1]/2.0-20
                
                controlPoints[5][0]=xe
                controlPoints[5][1]=ye
                
            else:
                #tup[1].MakeLineControlPoints(4)
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                ym=self.GetY()+ba[1]/2.0+20
                controlPoints[1][0]=xa
                controlPoints[1][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[2][0]=xe
                controlPoints[2][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[3][0]=xe
                controlPoints[3][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[4][0]=xe
                controlPoints[4][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[5][0]=xe
                controlPoints[5][1]=ye
            #tup[1].Show(True)
            if dc is not None:
                tup[1].Draw(dc)
            #tup[1].OnDrawControlPoints(dc)
    def HideLinks(self,dc):
        for tup in self.nextSteps:
            tup[1].Show(False)
            tup[1].Draw(dc)
    def HideLinksRec(self,dc):
        self.HideLinks(dc)
        for s in self.prevSteps:
            s.HideLinks(dc)
            
    def UpdateLinks(self,dc):
        self.__processLines__(dc)
    def UpdateLinksRev(self,dc):
        self.__processLines__(dc)
        for s in self.prevSteps:
            s.__processLines__(dc)
    def RemoveLink(self,idx):
        if idx > len(self.nextSteps):
            return
        if idx<0:
            return
        n=self.nextSteps[idx]
        n[0].prevSteps.remove(self)
        n[1].Detach()
        n[1].RemoveFromCanvas(self.canvas)
        n[1].Unlink()
        self.nextSteps=self.nextSteps[:idx]+self.nextSteps[idx+1:]
    def ReformatRegions(self, canvas=None):
        rnum = 0
        if canvas is None:
            canvas = self.GetCanvas()
        dc = wx.ClientDC(canvas)  # used for measuring
        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        #print "***", self
        #ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        #self.SetRegionSizes()
        #self.ReformatRegions()
        #self.GetCanvas().Refresh()
        pass
    def OnLeftClick(self, x, y, keys = 0, attachment = 0):
        #print "left click step"
        self.updateSelect()
    def updateSelect(self):
        shape = self
        canvas = self.canvas
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []
        
            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            #try:
            #    if shape.IsSelectable()>0:
            #        shape.Select(True)
            #        print "selected 1",shape.Selected()
    def OnBeginDragLeft(self, x, y, keys=0, attachment=0):
        x,y=self.canvas.GetDiagram().Snap(x,y)
        ogl.CompositeShape.OnBeginDragLeft(self,x,y,keys,attachment)
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        ogl.CompositeShape.OnEndDragLeft(self,x,y,keys,attachment)
    def OnRightClick(self, x, y, keys = 0, attachment = 0):
        #print "right clicked"
        pass
    def IsSelectable(self):
        return 0
    def DoEdit(self,nrs):
        if self.canvas.dlgSFCstep is None:
            self.canvas.dlgSFCstep=vgdSFCstep(self.canvas)
        self.canvas.dlgSFCstep.Centre()
        self.canvas.dlgSFCstep.SetInfos(self,self.canvas)
        self.canvas.dlgSFCstep.SetBrowseDlg(self.canvas.GetStepBrowseDlg())
        self.canvas.dlgSFCstep.SetBrowseValDlg(self.canvas.GetStepBrowseValDlg())
        if self.canvas.dlgSFCstep.ShowModal()>0:
            self.canvas.__setModified__(True)
            return 1
        return 0
    

#----------------------------------------------------------------------
class DrawingEvtHandler(ogl.ShapeEvtHandler):
    def __init__(self):
        ogl.ShapeEvtHandler.__init__(self)
        
    def UpdateStatusBar(self, shape):
        x, y = shape.GetX(), shape.GetY()
        width, height = shape.GetBoundingBoxMax()
        

    def OnLeftClick(self, x, y, keys=0, attachment=0):
        #print keys,ogl.KEY_CTRL
        shape = self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)

        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []

            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            shape.Select(True, dc)
            if toUnselect:
                for s in toUnselect:
                    s.Select(False, dc)
            if isinstance(shape,voglSFCstep):
                if keys==ogl.KEY_CTRL:
                    canvas.LinkSteps(shape)
            canvas.SetSelectStep(shape)
            canvas.Redraw(dc)
        self.UpdateStatusBar(shape)
    def OnStartDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        #x,y=shape.GetCanvas().GetDiagram().Snap(x,y)
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        if not shape.Selected():
            self.OnLeftClick(x, y, keys, attachment)
        self.UpdateStatusBar(shape)
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        ogl.ShapeEvtHandler.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.UpdateStatusBar(self.GetShape())
    def OnMovePost(self, dc, x, y, oldX, oldY, display):
        #print x,y,oldX,oldY
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        #canvas.Erase(dc)
        ogl.ShapeEvtHandler.OnMovePost(self, dc, x, y, oldX, oldY, display)
        self.UpdateStatusBar(self.GetShape())
        #print "moved"
        if isinstance(self.GetShape(),voglSFCstep):
            shape=self.GetShape()
            shape.UpdateLinksRev(dc)
            #canvas = shape.GetCanvas()
            #dc = wx.ClientDC(canvas)
            #canvas.PrepareDC(dc)
            #canvas.GetDiagram().Clear(dc)
            #canvas.Erase(dc)
            #canvas.Redraw(dc)
    def OnRightClick(self, *dontcare):
        #print "evt handler right click"
        #self.log.WriteText("%s\n" % self.GetShape())
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.DoEdit(canvas.GetStepNrs())>0:
            if isinstance(shape,voglSFCstep):
                s=shape.__getName__()
                shape.step.FormatText(dc,s,0)
                shape.__setSize__(s)
            canvas.GetDiagram().Clear(dc)
            canvas.Redraw(dc)
        pass

class voglSFCdiagram(ogl.Diagram):
    def __init__(self):
        ogl.Diagram(self)
    def Snap(self,x,y):
        """'Snaps' the coordinate to the nearest grid position, if
        snap-to-grid is on."""
        if self._snapToGrid:
            
            return self._gridSpacing * int(x / self._gridSpacing + 0.5), self._gridSpacing * int(y / self._gridSpacing + 0.5)
        return x, y
    
#----------------------------------------------------------------------

class vDrawSFCCanvasOGL(ogl.ShapeCanvas):
    def __init__(self, parent=None,id = -1, pos = wx.DefaultPosition, size = wx.DefaultSize, 
                    style = wx.BORDER, name = "ShapeCanvas",
                    gridSize=20):
        ogl.ShapeCanvas.__init__(self, parent,id, pos, size, style, name)
        self.parent=parent
        self.dlgStepBrowse=None
        self.dlgStepValBrowse=None
        self.dlgActionBrowse=None
        self.dlgActionValBrowse=None
    
        maxWidth  = 650
        maxHeight = 1000
        #self.SetScrollbars(20, 20, maxWidth/20, maxHeight/20)
        self.SetScrollbars(1, 1, maxWidth, maxHeight)
        
        #self.log = log
        #self.frame = frame
        self.SetBackgroundColour("LIGHT GRAY")
        #self.SetBackgroundColour(wx.WHITE)
        self.diagram = ogl.Diagram()
        self.SetDiagram(self.diagram)
        self.diagram.SetCanvas(self)
        self.shapes = []
        self.save_gdi = []
        
        self.selStep=None
        self.dlgSFCstep=None
        self.dlgSFCstepActions=None
        self.dlgSFCstepTransition=None
        self.relNode=None
        
        self.diagram.SetGridSpacing(gridSize)
        self.diagram.SetSnapToGrid(True)
        
        self.stepNrs=[]
        self.steps=[]
        self.stepIDs={}
        #rRectBrush = wx.Brush("MEDIUM TURQUOISE", wx.SOLID)
        #dsBrush = wx.Brush("WHEAT", wx.SOLID)
    
    def __setModified__(self,state):
        self.parent.__setModified__(state)
    def Clear(self):
        self.relNode=None
        unit=self.GetScrollPixelsPerUnit()
        size=self.GetVirtualSize()
        self.SetScrollbars(unit[0], unit[1], size[0], size[1])
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepNrs=[]
        self.stepIDs={}
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
    def GetStepNrs(self):
        return self.stepNrs
    def GetSteps(self):
        return self.steps
    def UpdateAxis(self):
        pass
    def AddShapeSimple(self, shape, x, y):
        x,y=self.GetDiagram().Snap(x,y)
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(False, False)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape
    def MyAddShape(self, shape, x, y, pen, brush, text):
        # Composites have to be moved for all children to get in place
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(True, True)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        if pen:    shape.SetPen(pen)
        if brush:  shape.SetBrush(brush)
        if text:
            for line in text.split('\n'):
                shape.AddText(line)
        #shape.SetShadowMode(ogl.SHADOW_RIGHT)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape


    def OnBeginDragLeft(self, x, y, keys):
        #print "OnBeginDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnEndDragLeft(self, x, y, keys):
        #print "OnEndDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnRightClick(self,x,y,keys):
        #print "canvas right"
        if not hasattr(self,'popupIdAddStep'):
            self.popupIdAddStep=wx.NewId()
            self.popupIdDel=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddStep,self.OnAddStep)
            wx.EVT_MENU(self,self.popupIdDel,self.OnDel)
        menu=wx.Menu()
        menu.Append(self.popupIdAddStep,'Add Step')
        menu.AppendSeparator()
        menu.Append(self.popupIdDel,'Del')
        
        
        x,y=self.GetDiagram().Snap(x,y)
        self.popupPos=(x,y)
        
        p=self.GetViewStart()
        u=self.GetScrollPixelsPerUnit()
        self.PopupMenu(menu,wx.Point(x-p[0]*u[0], y-p[1]*u[1]))
        
        menu.Destroy()
    def OnAddStep(self,evt):
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        
        stepAdd=voglSFCstep('999','','---','',[{'name':'--','desc':''}],140, 150, self)
        ba=stepAdd.GetBoundingBoxMax()
        x=self.popupPos[0]+ba[0]/2.0
        y=self.popupPos[1]+ba[1]/2.0
        x,y=self.GetDiagram().Snap(x,y)
        
        ds2 = self.AddShapeSimple(stepAdd,x, y)
        self.steps.append(ds2)
        
        self.Redraw(dc)
    def LinkSteps(self,newstep):
        if isinstance(newstep,voglSFCstep)<1:
            return
        if self.selStep is not None:
            if isinstance(self.selStep,voglSFCstep):
                idxFrom=self.steps.index(self.selStep)
                idxTo=self.steps.index(newstep)
                self.selStep.AddLink(newstep)
    def SetSelectStep(self,step):
        if step is not None:
            if step.Selected()>0:
                self.selStep=step
            else:
                self.selStep=None
        else:
            self.selStep=None
    def OnDel(self,evt):
        if self.selStep is None:
            return
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        shapeList = self.GetDiagram().GetShapeList()
        for s in shapeList:
            if s.Selected():
                s.Select(False, dc)
        self.selStep.Move(dc, -300, -300)
        self.RemoveShape(self.selStep.step)
        self.selStep.Delete()
        idx=self.steps.index(self.selStep)
        self.steps.remove(self.selStep)
        self.shapes.remove(self.selStep)
        self.selStep=None
    def SetStepBrowseDlg(self,dlg):
        self.dlgStepBrowse=dlg
    def SetStepBrowseValDlg(self,dlg):
        self.dlgStepValBrowse=dlg
    def SetActionBrowseDlg(self,dlg):
        self.dlgActionBrowse=dlg
    def SetActionBrowseValDlg(self,dlg):
        self.dlgActionValBrowse=dlg
        
    def GetStepBrowseDlg(self):
        return self.dlgStepBrowse
    def GetStepBrowseValDlg(self):
        return self.dlgStepValBrowse
    def GetActionBrowseDlg(self):
        return self.dlgActionBrowse
    def GetActionBrowseValDlg(self):
        return self.dlgActionValBrowse
        
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return strOld
    def SetNodeStep(self,node):
        #self.node=node
        #self.doc=doc
        #self.prevSteps=[]
        #self.nextSteps=[]
        id=''
        nr=''
        name=''
        parallel=0
        if node is None:
            return
        id=self.doc.getAttribute(node,'id')
        x=self.doc.getNodeText(node,'x')
        y=self.doc.getNodeText(node,'y')
        nr=self.doc.getNodeText(node,'nr')
        name=self.doc.getNodeText(node,'name')
        desc=self.doc.getNodeText(node,'desc')
        try:
            self.parallel=int(self.doc.getNodeText(node,'parallel'))
        except:
            self.parallel=int(self.doc.getNodeText(node,'nr'))
        actions=[]
        childs=self.doc.getChilds(node,'action')
        for c in childs:
            attrs=self.doc.getChilds(c)
            t={}
            for a in attrs:
                tagName=self.doc.getTagName(a)
                t[tagName]=self.doc.getText(a)
                fid=self.doc.getAttribute(a,'fid')
                if len(fid)>0:
                    t['fid_'+tagName]=fid
                    try:
                        #sNew=vtXmlDomTree.getNodeText(self.ids.GetIdStr(fid)[1])
                        self.doc.GetIdStr(fid)
                        sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(fid)[1])
                        sName=self.__replace__(t[tagName],sNew)
                        t[tagName]=sName
                    except:
                        pass
            actions.append(t)
        x,y=self.GetDiagram().Snap(float(x), float(y))
        step = self.AddShapeSimple(
                voglSFCstep(nr,id,name,desc,actions,140, 150, self),
                x, y)
        self.steps.append(step)
        self.stepIDs[id]=step
        pass
    def GetNodeStep(self,step,node):
        self.doc.setNodeText(node,'x',str(step.GetX()))
        self.doc.setNodeText(node,'y',str(step.GetY()))
        self.doc.setNodeText(node,'nr',step.nr)
        self.doc.setNodeText(node,'name',step.name)
        self.doc.setNodeText(node,'desc',step.desc)
        self.doc.setNodeText(node,'parallel',str(step.parallel))
        childs=self.doc.getChilds(node,'action')
        for c in childs:
            self.doc.deleteNode(c,node)
        for action in step.actions:
            c=self.doc.createSubNode(node,'action',False)
            keys=action.keys()
            keys.sort()
            for k in keys:
                if k[:4]=='fid_':
                    continue
                self.doc.setNodeText(c,k,action[k])
            for k in keys:
                if k[:4]=='fid_':
                    aNode=self.doc.getChild(c,k[4:])
                    if aNode is not None:
                        self.doc.setAttribute(aNode,'fid',action[k])
    def SetLink(self,node,step):
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            id=self.doc.getAttribute(c,'fid')
            transChilds=self.doc.getChilds(c,'transition')
            lstTrans=[]
            for t in transChilds:
                sName=self.doc.getNodeText(t,'name')
                sVal=self.doc.getNodeText(t,'val')
                try:
                    nodeTransName=self.doc.getChild(t,'name')
                    sIDname=self.doc.getAttribute(nodeTransName,'fid')
                    if len(sIDname)>0:
                        sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDname)[1])
                        sName=self.__replace__(sName,sNew)
                except:
                    sIDname=''
                try:
                    nodeTransVal=self.doc.getChild(t,'val')
                    sIDval=self.doc.getAttribute(nodeTransVal,'fid')
                    if len(sIDval)>0:
                        sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDval)[1])
                        sVal=self.__replace__(sVal,sNew)
                except:
                    sIDval=''
                trans={'name':sName,
                    'fid_name':sIDname,
                    'operation':self.doc.getNodeText(t,'operation'),
                    'val':sVal,
                    'fid_val':sIDval,
                    'combine':self.doc.getNodeText(t,'combine'),
                    'level':self.doc.getNodeText(t,'level'),
                    'desc':self.doc.getNodeText(t,'desc')}
                lstTrans.append(trans)
            if len(lstTrans)<1:
                lstTrans=None
            try:
                step.AddLink(self.stepIDs[id],lstTrans)
            except:
                pass
        pass
    def GetLink(self,node,step):
        if node is None:
            return
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            self.doc.deleteNode(c,node)
        for nextStep in step.nextSteps:
            n=self.doc.createSubNode(node,'follow',False)
            self.doc.setAttribute(n,'fid',nextStep[0].id)
            for trans in nextStep[2]:
                nt=self.doc.createSubNode(n,'transition',False)
                keys=trans.keys()
                keys.sort()
                for k in keys:
                    if k[:4]=='fid_':
                        continue
                    self.doc.setNodeText(nt,k,trans[k])
                for k in keys:
                    if k[:4]=='fid_':
                        aNode=self.doc.getChild(nt,k[4:])
                        if aNode is not None:
                            self.doc.setAttribute(aNode,'fid',trans[k])
    def SetDoc(self,doc,bNet):
        self.doc=doc
    def SetNode(self,node):
        self.node=node
        self.baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
        self.baseNode=self.doc.getChild(self.baseNode,'instance')
        self.relNode=Hierarchy.getRelBaseNode(self.doc,node)
        #self.canvas
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepIDs={}
        childs=self.doc.getChilds(node,'step')
        for c in childs:
            self.SetNodeStep(c)
        i=0
        for c in childs:
            self.SetLink(c,self.steps[i])
            i+=1
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
    def __getNode__(self,childs,id):
        for c in childs:
            if self.doc.getAttribute(c,'id')==id:
                return c
        return None
    def GetNode(self,node):
        self.node=node
        if self.node is None:
            return
        self.stepIDs={}
        self.used=[]
        childs=self.doc.getChilds(node,'step')
        for step in self.steps:
            if len(step.id)>0:
                n=self.__getNode__(childs,step.id)
                self.used.append(n)
            else:
                #new node
                n=self.doc.createSubNode(node,'step',False)
                self.doc.setAttribute(n,'id','')
                self.doc.addNode(node,n)
            if n is not None:
                self.GetNodeStep(step,n)
                step.id=self.doc.getAttribute(n,'id')
                self.stepIDs[step.id]=step
        # delete not used
        for c in childs:
            try:
                idx=self.used.index(c)
            except:
                self.doc.deleteNode(c,node)
        
        childs=self.doc.getChilds(node,'step')
        for step in self.steps:
            self.GetLink(self.__getNode__(childs,step.id),step)
                
        self.doc.AlignNode(node,iRec=10)
    def GenTexPic(self):
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=1.5
        scaleY=2.0
        strs=[]
        for step in self.steps:
            x=step.GetX()
            y=step.GetY()
            ba=step.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0)-40.0
            ya=y-(ba[1]/2.0)-40.0
            xe=x+(ba[0]/2.0)+40.0
            ye=y+(ba[1]/2.0)+40.0
            if xa<xMin:
                xMin=xa
            if ya<yMin:
                yMin=ya
            if xe>xMax:
                xMax=xe
            if ye>yMax:
                yMax=ye
        xMin/=scaleX
        yMin/=scaleY
        xMax/=scaleX
        yMax/=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        for step in self.steps:
            x=step.step.GetX()/scaleX
            y=step.step.GetY()/scaleY
            ba=step.step.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0/scaleX)
            ya=y-(ba[1]/2.0/scaleY)
            tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,'Nr: %s %s'%(step.nr,latex.texReplace(step.name)))
            strs.append('    \\put(%4.1f,%4.1f){\\framebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
            
            xal=x+(ba[0]/2.0/scaleX)
            yl=yMax-y
            
            for nextNode in step.nextSteps:
                line=nextNode[1]
                points=line.GetLineControlPoints()
                for i in range(0,5):
                    xa=points[i][0]/scaleX
                    ya=yMax-points[i][1]/scaleY
                    xe=points[i+1][0]/scaleX
                    ye=yMax-points[i+1][1]/scaleY
                    dx=0
                    dy=0
                    if xa==xe:
                        if ya==ye:
                            continue
                        elif ya<ye:
                            dy=1
                            l=ye-ya
                        else:
                            dy=-1
                            l=ya-ye
                    elif xa<xe:
                        dx=1
                        l=xe-xa
                    else:
                        dx=-1
                        l=xa-xe
                    tup=(xa,ya,dx,dy,l)
                    if i==4:
                        strs.append('    \\put(%4.1f,%4.1f){\\vector(%d,%d){%4.1f}}'%tup)
                    else:
                        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
                    
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,
                        relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
            sHier=Hierarchy.getTagNames(self.doc,self.baseNode,None,
                        par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node,doc):
        if node is not None:
            par=self.doc.getParent(node)
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def GenTexTab(self,sHier=None):
        if sHier is None:
            if self.node is not None:
                par=self.doc.getParent(self.node)
                #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
                relNode=Hierarchy.getRelBaseNode(self.doc,par)
                sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' SFC'
                sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' SFC'
            else:
                sHier=''
                sHierFull=''
        strs=[]
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}p{0.05\linewidth}p{0.3\linewidth}p{0.05\linewidth}')
        strs.append('       p{0.3\linewidth}p{0.02\linewidth}p{0.01\linewidth}}')
        strs.append('  \\multicolumn{6}{c}{%s} \\\\ \\hline'%'\\textbf{SFC specification}')
        tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        nrs={}
        i=0
        for step in self.steps:
            try:
                lst=nrs[step.nr]
                lst.append(i)
            except:
                nrs[step.nr]=[i]
            i+=1
        keys=nrs.keys()
        keys.sort()
        for k in keys:
            for idx in nrs[k]:
                step=self.steps[idx]
                tup=(step.nr,step.name)
                strs.append('    \\textbf{%s} &  & \\multicolumn{5}{l}{\\textbf{%s}} \\\\'%tup)
                sDesc=step.desc
                if len(sDesc)>0:
                    sDesc=latex.tableMultiLine(sDesc,'p{0.68\linewidth}')
                    strs.append('     &  & \\multicolumn{5}{l}{\\hspace{1cm}%s} \\\\'%sDesc)
                for act in step.actions:
                    try:
                        sName=act['name']
                    except:
                        sName=''
                    try:
                        sOp=act['operation']
                    except:
                        sOp=''
                    try:
                        sVal=act['val']
                    except:
                        sVal=''
                    tup=('',latex.texReplace(sName),sOp,
                            latex.texReplace(sVal),'','')
                    strs.append('    & %s & %s & %s & %s & %s & %s \\\\'%tup)
                    try:
                        sDesc=act['desc']
                    except:
                        sDesc=''
                    if len(sDesc)>0:
                        sDesc=latex.tableMultiLine(sDesc,'p{0.68\linewidth}')
                        tup=('',sDesc)
                        strs.append('  &  %s &  \\multicolumn{5}{l}{\\hspace{1cm}%s} \\\\'%tup)
                if len(step.nextSteps)>0:
                    strs.append('    \\cline{2-7}')
                for nextNode in step.nextSteps:
                    strs.append('    \\cline{2-7}')
                    tup=(nextNode[0].nr,latex.texReplace(nextNode[0].GetName()))
                    strs.append('   $\\to$ & %s  & %s &  &  &  &  \\\\'%tup)
                    for trans in nextNode[2]:
                        try:
                            sName=trans['name']
                        except:
                            sName=''
                        try:
                            sOp=trans['operation']
                        except:
                            sOp=''
                        try:
                            sVal=trans['val']
                        except:
                            sVal=''
                        try:
                            sComb=trans['combine']
                        except:
                            sComb=''
                        try:
                            sLv=trans['level']
                        except:
                            sLv=''
                        tup=('',latex.texReplace(sName),sOp,
                                latex.texReplace(sVal),sComb,sLv)
                        strs.append('  &  %s  & %s & %s & %s & %s & %s \\\\'%tup)
                        try:
                            sDesc=trans['desc']
                        except:
                            pass
                        if len(sDesc)>0:
                            sDesc=latex.tableMultiLine(sDesc,'p{0.68\linewidth}')
                            tup=('',sDesc)
                            strs.append('    & %s &  \\multicolumn{5}{l}{\\hspace{1cm}%s} \\\\'%tup)
                        #strs.append('    %s & \\multicolumn{3}{l}{%s} \\\\'%tup)
                        #tup=('',sOp,sVal)
                        #strs.append('    %s  & %s & %s & %s \\\\'%tup)
                        #strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\'%tup)
                        #tup=('',sComb,sLv,'')
                        #strs.append('    %s & %s & %s & %s \\\\'%tup)
                strs.append('    \\hline ')
        strs.append('  \\caption{table %s}'%latex.texReplace(sHier))
        strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sHierFull))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return string.join(strs,os.linesep)
    def GenTexTabRef(self,node,doc):
        if node is not None:
            par=self.doc.getParent(node)
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=sHier=Hierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' SFC'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{tab:%s} \#\\pageref{tab:%s}'%(sHier,sHier)
            return sRef
        return ''
    
