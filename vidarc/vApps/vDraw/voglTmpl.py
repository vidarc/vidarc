#Boa:Dialog:vgdTimeAxis

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import images

def create(parent):
    return vgdTimeAxis(parent)

[wxID_VGDTIMEAXIS, wxID_VGDTIMEAXISGCBBCANCEL, wxID_VGDTIMEAXISGCBBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vgdTimeAxis(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDTIMEAXIS, name=u'vgdTimeAxis',
              parent=prnt, pos=wx.Point(325, 103), size=wx.Size(400, 380),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Time Axis')
        self.SetClientSize(wx.Size(392, 353))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(96, 312), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDTIMEAXISGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(192, 312),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDTIMEAXISGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
class voglTimeAxis(ogl.DrawnShape):
    def __init__(self, w=5, h=5,count=10):
        ogl.DrawnShape.__init__(self)
        
        self.SetDrawnPen(wx.BLACK_PEN)
        for i in range(1,count):
            self.DrawLine((i*w, -h/2), (i*w, h/2))

        self.DrawLine((0, 0), (count*w, 0))

        self.SetDrawnTextColour(wx.BLACK)
        self.SetDrawnFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.DrawText("time", (count*w+w/2, -h/2))
        self.CalculateSize()
    def IsSelectable(self):
        return 0
