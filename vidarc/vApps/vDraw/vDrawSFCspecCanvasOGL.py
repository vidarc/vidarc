#Boa:FramePanel:vgpDraw
#----------------------------------------------------------------------------
# Name:         vDrawSFCspecCanvasOGL.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawSFCspecCanvasOGL.py,v 1.2 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.ogl as ogl
import string,os,sys

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

from vidarc.vApps.vDraw.voglSFCstep import voglSFCstep
#----------------------------------------------------------------------
        

class DrawingEvtHandler(ogl.ShapeEvtHandler):
    def __init__(self):
        ogl.ShapeEvtHandler.__init__(self)
        
    def UpdateStatusBar(self, shape):
        x, y = shape.GetX(), shape.GetY()
        width, height = shape.GetBoundingBoxMax()
        

    def OnLeftClick(self, x, y, keys=0, attachment=0):
        #print keys,ogl.KEY_CTRL
        shape = self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)

        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []

            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            shape.Select(True, dc)
            if toUnselect:
                for s in toUnselect:
                    s.Select(False, dc)
            if isinstance(shape,voglSFCstep):
                if keys==ogl.KEY_CTRL:
                    canvas.LinkSteps(shape)
            canvas.SetSelectStep(shape)
            canvas.Redraw(dc)
        self.UpdateStatusBar(shape)
    def OnStartDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        #x,y=shape.GetCanvas().GetDiagram().Snap(x,y)
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        if not shape.Selected():
            self.OnLeftClick(x, y, keys, attachment)
        self.UpdateStatusBar(shape)
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        ogl.ShapeEvtHandler.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.UpdateStatusBar(self.GetShape())
    def OnMovePost(self, dc, x, y, oldX, oldY, display):
        #print x,y,oldX,oldY
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        #canvas.Erase(dc)
        ogl.ShapeEvtHandler.OnMovePost(self, dc, x, y, oldX, oldY, display)
        self.UpdateStatusBar(self.GetShape())
        #print "moved"
        if isinstance(self.GetShape(),voglSFCstep):
            shape=self.GetShape()
            shape.UpdateLinksRev(dc)
            #canvas = shape.GetCanvas()
            #dc = wx.ClientDC(canvas)
            #canvas.PrepareDC(dc)
            #canvas.GetDiagram().Clear(dc)
            #canvas.Erase(dc)
            #canvas.Redraw(dc)
    def OnRightClick(self, *dontcare):
        #print "evt handler right click"
        #self.log.WriteText("%s\n" % self.GetShape())
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.DoEdit(canvas.GetStepNrs())>0:
            if isinstance(shape,voglSFCstep):
                shape.step.FormatText(dc,'Nr: %s'%shape.nr)
            canvas.GetDiagram().Clear(dc)
            canvas.Redraw(dc)
        pass

class voglSFCdiagram(ogl.Diagram):
    def __init__(self):
        ogl.Diagram(self)
    def Snap(self,x,y):
        """'Snaps' the coordinate to the nearest grid position, if
        snap-to-grid is on."""
        if self._snapToGrid:
            
            return self._gridSpacing * int(x / self._gridSpacing + 0.5), self._gridSpacing * int(y / self._gridSpacing + 0.5)
        return x, y
    
#----------------------------------------------------------------------

class vDrawSFCspecCanvasOGL(ogl.ShapeCanvas):
    def __init__(self, parent=None,id = -1, pos = wx.DefaultPosition, size = wx.DefaultSize, 
                    style = wx.BORDER, name = "ShapeCanvas",
                    gridSize=20):
        ogl.ShapeCanvas.__init__(self, parent,id, pos, size, style, name)
        self.parent=parent
        self.dlgStepBrowse=None
        self.dlgStepValBrowse=None
        self.dlgActionBrowse=None
        self.dlgActionValBrowse=None
    
        maxWidth  = 650
        maxHeight = 1000
        #self.SetScrollbars(20, 20, maxWidth/20, maxHeight/20)
        self.SetScrollbars(1, 1, maxWidth, maxHeight)
        
        #self.log = log
        #self.frame = frame
        self.SetBackgroundColour("LIGHT GRAY")
        #self.SetBackgroundColour(wx.WHITE)
        self.diagram = ogl.Diagram()
        self.SetDiagram(self.diagram)
        self.diagram.SetCanvas(self)
        self.shapes = []
        self.save_gdi = []
        
        self.selStep=None
        self.dlgSFCstep=None
        self.dlgSFCstepActions=None
        self.dlgSFCstepTransition=None
        self.relNode=None
        
        self.diagram.SetGridSpacing(gridSize)
        self.diagram.SetSnapToGrid(True)
        
        self.stepNrs=[]
        self.steps=[]
        self.stepIDs={}
        #rRectBrush = wx.Brush("MEDIUM TURQUOISE", wx.SOLID)
        #dsBrush = wx.Brush("WHEAT", wx.SOLID)
    
    def __setModified__(self,state):
        self.parent.__setModified__(state)
    def Clear(self):
        self.relNode=None
        unit=self.GetScrollPixelsPerUnit()
        size=self.GetVirtualSize()
        self.SetScrollbars(unit[0], unit[1], size[0], size[1])
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepNrs=[]
        self.stepIDs={}
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
    def GetStepNrs(self):
        return self.stepNrs
    def GetSteps(self):
        return self.steps
    def UpdateAxis(self):
        pass
    def AddShapeSimple(self, shape, x, y):
        x,y=self.GetDiagram().Snap(x,y)
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(False, False)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape
    def MyAddShape(self, shape, x, y, pen, brush, text):
        # Composites have to be moved for all children to get in place
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(True, True)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        if pen:    shape.SetPen(pen)
        if brush:  shape.SetBrush(brush)
        if text:
            for line in text.split('\n'):
                shape.AddText(line)
        #shape.SetShadowMode(ogl.SHADOW_RIGHT)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape


    def OnBeginDragLeft(self, x, y, keys):
        #print "OnBeginDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnEndDragLeft(self, x, y, keys):
        #print "OnEndDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnRightClick(self,x,y,keys):
        #print "canvas right"
        if not hasattr(self,'popupIdAddStep'):
            self.popupIdAddStep=wx.NewId()
            self.popupIdDel=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddStep,self.OnAddStep)
            wx.EVT_MENU(self,self.popupIdDel,self.OnDel)
        menu=wx.Menu()
        menu.Append(self.popupIdAddStep,'Add Step')
        menu.AppendSeparator()
        menu.Append(self.popupIdDel,'Del')
        
        
        x,y=self.GetDiagram().Snap(x,y)
        self.popupPos=(x,y)
        
        p=self.GetViewStart()
        u=self.GetScrollPixelsPerUnit()
        self.PopupMenu(menu,wx.Point(x-p[0]*u[0], y-p[1]*u[1]))
        
        menu.Destroy()
    def OnAddStep(self,evt):
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        
        stepAdd=voglSFCstep('???','','???',[{'name':'??','desc':''}],140, 150, self)
        ba=stepAdd.GetBoundingBoxMax()
        x=self.popupPos[0]+ba[0]/2.0
        y=self.popupPos[1]+ba[1]/2.0
        x,y=self.GetDiagram().Snap(x,y)
        
        ds2 = self.AddShapeSimple(stepAdd,x, y)
        self.steps.append(ds2)
        
        self.Redraw(dc)
    def LinkSteps(self,newstep):
        if isinstance(newstep,voglSFCstep)<1:
            return
        if self.selStep is not None:
            if isinstance(self.selStep,voglSFCstep):
                idxFrom=self.steps.index(self.selStep)
                idxTo=self.steps.index(newstep)
                self.selStep.AddLink(newstep)
    def SetSelectStep(self,step):
        if step is not None:
            if step.Selected()>0:
                self.selStep=step
            else:
                self.selStep=None
        else:
            self.selStep=None
    def OnDel(self,evt):
        if self.selStep is None:
            return
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        shapeList = self.GetDiagram().GetShapeList()
        for s in shapeList:
            if s.Selected():
                s.Select(False, dc)
        self.selStep.Move(dc, -300, -300)
        self.RemoveShape(self.selStep.step)
        self.selStep.Delete()
        idx=self.steps.index(self.selStep)
        self.steps.remove(self.selStep)
        self.shapes.remove(self.selStep)
        self.selStep=None
    def SetStepBrowseDlg(self,dlg):
        self.dlgStepBrowse=dlg
    def SetStepBrowseValDlg(self,dlg):
        self.dlgStepValBrowse=dlg
    def SetActionBrowseDlg(self,dlg):
        self.dlgActionBrowse=dlg
    def SetActionBrowseValDlg(self,dlg):
        self.dlgActionValBrowse=dlg
        
    def GetStepBrowseDlg(self):
        return self.dlgStepBrowse
    def GetStepBrowseValDlg(self):
        return self.dlgStepValBrowse
    def GetActionBrowseDlg(self):
        return self.dlgActionBrowse
    def GetActionBrowseValDlg(self):
        return self.dlgActionValBrowse
        
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return strOld
    def SetNodeStep(self,node,doc):
        #self.node=node
        #self.doc=doc
        #self.prevSteps=[]
        #self.nextSteps=[]
        id=''
        nr=''
        name=''
        parallel=0
        if node is None:
            return
        id=doc.getAttribute(node,'id')
        x=doc.getNodeText(node,'x')
        y=doc.getNodeText(node,'y')
        nr=doc.getNodeText(node,'nr')
        name=doc.getNodeText(node,'name')
        try:
            self.parallel=int(doc.getNodeText(node,'parallel'))
        except:
            self.parallel=int(doc.getNodeText(node,'nr'))
        actions=[]
        childs=doc.getChilds(node,'action')
        for c in childs:
            attrs=doc.getChilds(c)
            t={}
            for a in attrs:
                tagName=doc.getTagName(a)
                t[tagName]=doc.getText(a)
                fid=doc.getAttribute(a,'fid')
                if len(fid)>0:
                    t['fid_'+tagName]=fid
                    try:
                        #sNew=vtXmlDomTree.getNodeText(self.ids.GetIdStr(fid)[1])
                        self.ids.GetIdStr(fid)
                        sNew=Hierarchy.getTagNames(doc,self.baseNode,self.relNode,self.ids.GetIdStr(fid)[1])
                        sName=self.__replace__(t[tagName],sNew)
                        t[tagName]=sName
                    except:
                        pass
            actions.append(t)
        x,y=self.GetDiagram().Snap(float(x), float(y))
        step = self.AddShapeSimple(
                voglSFCstep(nr,id,name,actions,140, 150, self),
                x, y)
        self.steps.append(step)
        self.stepIDs[id]=step
        pass
    def GetNodeStep(self,step,node,doc):
        doc.setNodeText(node,'x',str(step.GetX()))
        doc.setNodeText(node,'y',str(step.GetY()))
        doc.setNodeText(node,'nr',step.nr)
        doc.setNodeText(node,'name',step.name)
        doc.setNodeText(node,'parallel',str(step.parallel))
        childs=doc.getChilds(node,'action')
        for c in childs:
            doc.deleteNode(c,node)
        for action in step.actions.actions:
            c=doc.createSubNode(node,'action',False)
            keys=action.keys()
            keys.sort()
            for k in keys:
                if k[:4]=='fid_':
                    continue
                doc.setNodeText(c,k,action[k])
            for k in keys:
                if k[:4]=='fid_':
                    aNode=doc.getChild(c,k[4:])
                    if aNode is not None:
                        doc.setAttribute(aNode,'fid',action[k])
    def SetLink(self,node,doc,step):
        childs=doc.getChilds(node,'follow')
        for c in childs:
            print c
            id=doc.getAttribute(c,'fid')
            transChilds=doc.getChilds(c,'transition')
            lstTrans=[]
            for t in transChilds:
                print t
                sName=doc.getNodeText(t,'name')
                sVal=doc.getNodeText(t,'val')
                try:
                    nodeTransName=doc.getChild(t,'name')
                    sIDname=doc.getAttribute(nodeTransName,'fid')
                    sNew=Hierarchy.getTagNames(doc,self.baseNode,self.relNode,self.ids.GetIdStr(sIDname)[1])
                    sName=self.__replace__(sName,sNew)
                except:
                    sIDname=''
                try:
                    nodeTransVal=doc.getChild(t,'val')
                    sIDval=doc.getAttribute(nodeTransVal,'fid')
                    sNew=Hierarchy.getTagNames(doc,self.baseNode,self.relNode,self.ids.GetIdStr(sIDval)[1])
                    sVal=self.__replace__(sVal,sNew)
                except:
                    sIDval=''
                trans={'name':sName,
                    'fid_name':sIDname,
                    'operation':doc.getNodeText(t,'operation'),
                    'val':sVal,
                    'fid_val':sIDval,
                    'combine':doc.getNodeText(t,'combine'),
                    'level':doc.getNodeText(t,'level')}
                
                lstTrans.append(trans)
            if len(lstTrans)<1:
                lstTrans=None
            try:
                print lstTrans
                step.AddLink(self.stepIDs[id],lstTrans)
            except:
                pass
        pass
    def GetLink(self,node,doc,step):
        if node is None:
            return
        childs=doc.getChilds(node,'follow')
        for c in childs:
            doc.deleteNode(c,node)
        for nextStep in step.nextSteps:
            n=doc.createSubNode(node,'follow',False)
            doc.setAttribute(n,'fid',nextStep[0].id)
            for trans in nextStep[2]:
                nt=doc.createSubNode(n,'transition',False)
                keys=trans.keys()
                keys.sort()
                for k in keys:
                    if k[:4]=='fid_':
                        continue
                    doc.setNodeText(nt,k,trans[k])
    def SetNode(self,node,ids,doc):
        self.ids=ids
        self.node=node
        self.doc=doc
        self.baseNode=doc.getChild(doc.getRoot(),'prjengs')
        self.baseNode=doc.getChild(self.baseNode,'instance')
        self.relNode=Hierarchy.getRelBaseNode(doc,node)
        #self.canvas
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepIDs={}
        childs=doc.getChilds(node,'step')
        for c in childs:
            print c
            self.SetNodeStep(c,doc)
        i=0
        for c in childs:
            print '   ',c
            self.SetLink(c,doc,self.steps[i])
            i+=1
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
    def __getNode__(self,doc,childs,id):
        for c in childs:
            if doc.getAttribute(c,'id')==id:
                return c
        return None
    def GetNode(self,node,ids,doc):
        self.node=node
        self.doc=doc
        self.ids=ids
        if self.node is None:
            return
        self.stepIDs={}
        self.used=[]
        childs=doc.getChilds(node,'step')
        for step in self.steps:
            if len(step.id)>0:
                n=self.__getNode__(doc,childs,step.id)
                #n=vtXmlDomTree.createSubNode(node,'step',doc,False)
                #n.setAttribute('id',step.id)
                self.used.append(n)
            else:
                #new node
                n=doc.createSubNode(node,'step',False)
                doc.setAttribute(n,'id','')
                self.ids.CheckId(n)
            if n is not None:
                self.GetNodeStep(step,n,doc)
                self.ids.ProcessMissing()
                self.ids.ClearMissing()
                step.id=n.getAttribute('id')
                self.stepIDs[step.id]=step
        # delete not used
        for c in childs:
            try:
                idx=self.used.index(c)
            except:
                doc.deleteNode(c,node)
        
        childs=doc.getChilds(node,'step')
        for step in self.steps:
            self.GetLink(self.__getNode__(childs,step.id),step)
        # finally generate latex picture
        #texNode=vtXmlDomTree.getChild(node,'latex_picture')
        #if texNode is None:
        #    texNode=vtXmlDomTree.createSubNode(node,'latex_picture',doc,False)
        
        if 1==0:
            pic=self.GenTexPic()
            doc.setNodeCData(node,'latex_picture',pic,doc)
            sHier=Hierarchy.getTagNames(doc,self.baseNode,self.relNode,node)
            #sHier=self.__getHierarchy__(self.baseNode,node)
            sCaption='  \\caption{%s}'%sHier
            sRef='  \\label{fig:%s}'%sHier
            fig=['\\begin{figure}[!hb]',pic,sRef,sCaption,'\\end{figure}']
            doc.setNodeCData(node,'latex_figure',
                                    string.join(fig,os.linesep),doc)
            tab=self.GenTexTab(sHier)
            doc.setNodeCData(node,'latex_table',tab,doc)
        
        doc.AlignNode(node)
        
        #    #except:
        #    shape.Select(True)
                
        #    if toUnselect:
        #        for s in toUnselect:
        #            s.Select(False, dc)

        #    canvas.Redraw(dc)
        #self.canvas.SetSelectStep(self)
    def GenTexPic(self):
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=1.5
        scaleY=2.0
        strs=[]
        for step in self.steps:
            x=step.GetX()
            y=step.GetY()
            ba=step.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0)-40.0
            ya=y-(ba[1]/2.0)-40.0
            xe=x+(ba[0]/2.0)+40.0
            ye=y+(ba[1]/2.0)+40.0
            if xa<xMin:
                xMin=xa
            if ya<yMin:
                yMin=ya
            if xe>xMax:
                xMax=xe
            if ye>yMax:
                yMax=ye
        xMin/=scaleX
        yMin/=scaleY
        xMax/=scaleX
        yMax/=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        for step in self.steps:
            x=step.step.GetX()/scaleX
            y=step.step.GetY()/scaleY
            ba=step.step.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0/scaleX)
            ya=y-(ba[1]/2.0/scaleY)
            tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,'Nr: '+step.nr)
            strs.append('    \\put(%4.1f,%4.1f){\\framebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
            
            xal=x+(ba[0]/2.0/scaleX)
            yl=yMax-y
            
            x=step.actions.GetX()/scaleX
            y=step.actions.GetY()/scaleY
            ba=step.actions.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0/scaleX)
            #print ya
            #ya=y+(ba[1]/2.0/scale)
            #print ya
            
            dy=ba[1]/scaleY/float(len(step.actions.actions))
            y=yMax-ya-dy
            for act in step.actions.actions:
                tup=(xa,y,ba[0]/scaleX,dy,act['name'])
                strs.append('    \\put(%4.1f,%4.1f){\\framebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
                y-=dy
            tup=(xal,yl,xa-xal)
            strs.append('    \\put(%4.1f,%4.1f){\\line(1,0){%d}}'%tup)
            
            for nextNode in step.nextSteps:
                line=nextNode[1]
                points=line.GetLineControlPoints()
                for i in range(0,5):
                    xa=points[i][0]/scaleX
                    ya=yMax-points[i][1]/scaleY
                    xe=points[i+1][0]/scaleX
                    ye=yMax-points[i+1][1]/scaleY
                    dx=0
                    dy=0
                    if xa==xe:
                        if ya==ye:
                            continue
                        elif ya<ye:
                            dy=1
                            l=ye-ya
                        else:
                            dy=-1
                            l=ya-ye
                    elif xa<xe:
                        dx=1
                        l=xe-xa
                    else:
                        dx=-1
                        l=xa-xe
                    tup=(xa,ya,dx,dy,l)
                    if i==4:
                        strs.append('    \\put(%4.1f,%4.1f){\\vector(%d,%d){%4.1f}}'%tup)
                    else:
                        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
                    
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,self.node)+ ' ' + \
                        self.doc.getNodeText(self.node,'name') + ' SFC'
            sHier=Hierarchy.getTagNames(self.doc,self.baseNode,None,self.node)+ ' ' + \
                        self.doc.getNodeText(self.node,'name') + ' SFC'
        sCaption='  \\caption{%s}'%sHier
        sRef='  \\label{fig:%s}'%sHierFull
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,self.node)+ ' ' + \
                        self.doc.getNodeText(self.node,'name') + ' SFC'
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,self.node)+ ' ' + \
                        self.doc.getNodeText(self.node,'name') + ' SFC'
        sCaption='  \\caption{%s}'%sHier
        sRef='  \\label{fig:%s}'%sHierFull
        fig=['\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node,doc):
        if node is not None:
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,node)
            par=self.doc.getParetn(node)
            sHier=Hierarchy.getTagNames(self.doc,baseNode,None,node)+ ' ' + \
                        self.doc.getNodeText(node,'name') + ' SFC'
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def GenTexTab(self,sHier=None):
        if sHier is None:
            if self.node is not None:
                par=self.doc.getParent(self.node)
                #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
                relNode=Hierarchy.getRelBaseNode(self.doc,par)
                sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,self.node)+ ' ' + \
                            self.doc.getNodeText(self.node,'name') + ' SFC'
                sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,self.node)+ ' ' + \
                            self.doc.getNodeText(self.node,'name') + ' SFC'
            else:
                sHier=''
                sHierFull=''
        strs=[]
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}p{0.33\linewidth}p{0.05\linewidth}')
        strs.append('       p{0.33\linewidth}p{0.02\linewidth}p{0.01\linewidth}}')
        strs.append('  \\multicolumn{5}{c}{%s} \\\\ \\hline'%'\\textbf{SFC specification}')
        tup=('    \\textbf{Nr}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        nrs={}
        i=0
        for step in self.steps:
            try:
                lst=nrs[step.nr]
                lst.append(i)
            except:
                nrs[step.nr]=[i]
            i+=1
        keys=nrs.keys()
        keys.sort()
        for k in keys:
            for idx in nrs[k]:
                step=self.steps[idx]
                tup=(step.nr,step.name)
                strs.append('    \\textbf{%s} & \\multicolumn{5}{l}{\\textbf{%s}} \\\\'%tup)
                for act in step.actions.actions:
                    try:
                        sName=act['name']
                    except:
                        sName=''
                    try:
                        sOp=act['operation']
                    except:
                        sOp=''
                    try:
                        sVal=act['val']
                    except:
                        sVal=''
                    tup=('',string.replace(sName,'.','.\-'),sOp,
                            string.replace(sVal,'.','.\-'),'','')
                    strs.append('    %s & %s & %s & %s & %s & %s \\\\'%tup)
                    try:
                        sDesc=act['desc']
                    except:
                        pass
                    sDesc=string.replace(sDesc,'\n','\\newline ')
                    tup=('',sDesc)
                    strs.append('    %s &  \\multicolumn{5}{l}{\\vbox{%s}} \\\\'%tup)
                if len(step.nextSteps)>0:
                    strs.append('    \\cline{2-6}')
                for nextNode in step.nextSteps:
                    for trans in nextNode[2]:
                        try:
                            sName=trans['name']
                        except:
                            sName=''
                        try:
                            sOp=trans['operation']
                        except:
                            sOp=''
                        try:
                            sVal=trans['val']
                        except:
                            sVal=''
                        try:
                            sComb=trans['combine']
                        except:
                            sComb=''
                        try:
                            sLv=trans['level']
                        except:
                            sLv=''
                        tup=('',string.replace(sName,'.','.\-'),sOp,
                                string.replace(sVal,'.','.\-'),sComb,sLv)
                        strs.append('    %s  & %s & %s & %s & %s & %s \\\\'%tup)
                        #strs.append('    %s & \\multicolumn{3}{l}{%s} \\\\'%tup)
                        #tup=('',sOp,sVal)
                        #strs.append('    %s  & %s & %s & %s \\\\'%tup)
                        #strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\'%tup)
                        #tup=('',sComb,sLv,'')
                        #strs.append('    %s & %s & %s & %s \\\\'%tup)
                strs.append('    \\hline ')
                        
        strs.append('  \\caption{%s}'%sHier)
        strs.append('  \\label{tab:%s}'%sHierFull)
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return string.join(strs,os.linesep)
    def GenTexTabRef(self,node,doc):
        if node is not None:
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,node)
            par=self.doc.getParent(node)
            sHier=sHier=Hierarchy.getTagNames(self.doc,baseNode,None,node)+ ' ' + \
                        self.doc.getNodeText(node,'name') + ' SFC'
            sRef='\\ref{tab:%s} \#\\pageref{tab:%s}'%(sHier,sHier)
            return sRef
        return ''
    