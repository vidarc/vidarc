#Boa:Frame:vMsgMainFrame
#----------------------------------------------------------------------------
# Name:         vMsgMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060704
# CVS-ID:       $Id: vMsgMainFrame.py,v 1.1 2006/07/19 12:45:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.net.vtNetSecXmlGuiSlaveMultiple
import vidarc.tool.net.vtNetSecXmlGuiSlavePanel
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import getpass,time

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.tool.log.vtLogFileViewerFrame import vtLogFileViewerFrame

from vidarc.tool.net.vtNetMsg import vtNetMsg
#from vidarc.vApps.vMsg.vMsgListBook import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
from vidarc.tool.net.vtNetSecXmlGuiSlaveMultiple import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
#from vidarc.tool.xml.vtXmlMsgList import vtXmlMsgList
from vidarc.tool.xml.vtXmlMsgListNav import vtXmlMsgListNav

from vidarc.vApps.vHum.vNetHum import *

import vidarc.vApps.vMsg.images as images

def create(parent):
    return vMsgMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VMSGMAINFRAME, wxID_VMSGMAINFRAMECBCREATE, wxID_VMSGMAINFRAMECBDEL, 
 wxID_VMSGMAINFRAMECBGOTO, wxID_VMSGMAINFRAMECBVIEW, 
 wxID_VMSGMAINFRAMENETSLAVE, wxID_VMSGMAINFRAMEPNDATA, 
 wxID_VMSGMAINFRAMEPNNAV, wxID_VMSGMAINFRAMESBSTATUS, 
 wxID_VMSGMAINFRAMESLWNAV, wxID_VMSGMAINFRAMEVINETSLAVE, 
] = [wx.NewId() for _init_ctrls in range(11)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VMSGMAINFRAMEMNHELPMN_HELP_ABOUT, wxID_VMSGMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VMSGMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VMSGMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

class vMsgMainFrame(wx.Frame,vMainFrame,vStatusBarMessageLine):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbView, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbGoTo, 0, border=0, flag=0)

    def _init_coll_fgsNav_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsNav_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viNetSlave, 0, border=0, flag=0)
        parent.AddWindow(self.netSlave, 0, border=0, flag=0)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMSGMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VMSGMAINFRAMEMNFILEITEM_EXIT)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VMSGMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsNav = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsNav_Growables(self.fgsNav)
        self._init_coll_fgsNav_Items(self.fgsNav)

        self.pnData.SetSizer(self.fgsData)

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VMSGMAINFRAME, name=u'vMsgMainFrame',
              parent=prnt, pos=wx.Point(167, 21), size=wx.Size(819, 564),
              style=wx.DEFAULT_FRAME_STYLE, title=u'VIDARC Msg')
        self._init_utils()
        self.SetClientSize(wx.Size(811, 537))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VMSGMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VMSGMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              496), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VMSGMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VMSGMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(592, 272),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.cbView = wx.lib.buttons.GenBitmapButton(ID=wxID_VMSGMAINFRAMECBVIEW,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'cbView',
              parent=self.pnData, pos=wx.Point(4, 0), size=wx.Size(31, 30),
              style=0)
        self.cbView.Bind(wx.EVT_BUTTON, self.OnCbViewButton,
              id=wxID_VMSGMAINFRAMECBVIEW)

        self.cbGoTo = wx.lib.buttons.GenBitmapButton(ID=wxID_VMSGMAINFRAMECBGOTO,
              bitmap=vtArt.getBitmap(vtArt.Navigate), name=u'cbGoTo',
              parent=self.pnData, pos=wx.Point(4, 38), size=wx.Size(31, 30),
              style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,
              id=wxID_VMSGMAINFRAMECBGOTO)

        self.pnNav = wx.Panel(id=wxID_VMSGMAINFRAMEPNNAV, name=u'pnNav',
              parent=self.slwNav, pos=wx.Point(0, 0), size=wx.Size(197, 496),
              style=wx.TAB_TRAVERSAL)

        self.viNetSlave = vidarc.tool.net.vtNetSecXmlGuiSlavePanel.vtNetSecXmlGuiSlavePanel(id=wxID_VMSGMAINFRAMEVINETSLAVE,
              name=u'viNetSlave', parent=self.pnNav, pos=wx.Point(0, 0),
              size=wx.Size(184, 182), style=0)

        self.netSlave = vidarc.tool.net.vtNetSecXmlGuiSlaveMultiple.vtNetSecXmlGuiSlaveMultiple(id=wxID_VMSGMAINFRAMENETSLAVE,
              name=u'netSlave', parent=self.pnNav, pos=wx.Point(0, 390),
              size=wx.Size(32, 32), style=0)

        self.cbDel = wx.Button(id=wxID_VMSGMAINFRAMECBDEL, label=u'del',
              name=u'cbDel', parent=self.pnNav, pos=wx.Point(88, 424),
              size=wx.Size(75, 23), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VMSGMAINFRAMECBDEL)

        self.cbCreate = wx.Button(id=wxID_VMSGMAINFRAMECBCREATE,
              label=u'create', name=u'cbCreate', parent=self.pnNav,
              pos=wx.Point(80, 352), size=wx.Size(75, 23), style=0)
        self.cbCreate.Bind(wx.EVT_BUTTON, self.OnCbCreateButton,
              id=wxID_VMSGMAINFRAMECBCREATE)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        vMainFrame.__init__(self)
        vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,5000)
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='cfg')
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        #appls=['vMsg','vHum']
        
        #self.lstMsg=vtXmlMsgList(parent=self.pnData,pos=(0,0),size=(500,80))
        self.lstMsg=vtXmlMsgListNav(parent=self.pnData,pos=(0,0),size=(500,80))
        self.fgsData.AddWindow(self.lstMsg,1, border=4, flag=wx.EXPAND | wx.LEFT)
        
        rect = self.sbStatus.GetFieldRect(0)
        #self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
        #            size=(rect.width-4, rect.height-4),verbose=1)
        self.netMaster=None
        #self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        #self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=1)
        #self.netMaster.AddNetControl(None,None,verbose=0)
        
        #self.netMaster.SetCfg(self.xdCfg,[],appls,'vMsgAppl')
        #EVT_NET_XML_OPEN_OK(self.netMsg,self.OnOpenOk)
        
        #EVT_NET_XML_SYNCH_PROC(self.netMsg,self.OnSynchProc)
        #EVT_NET_XML_SYNCH_FINISHED(self.netMsg,self.OnSynchFinished)
        #EVT_NET_XML_SYNCH_ABORTED(self.netMsg,self.OnSynchAborted)
        
        #EVT_NET_XML_GOT_CONTENT(self.netMsg,self.OnGotContent)
        
        self.viNetSlave.SetNetMaster(self.netSlave)
        self.netSlave.InitWidgetStatus(self.viNetSlave)
        self.netSlave.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=1)
        self.netSlave.AddNetControl(vNetHum,'vHum',synch=True,verbose=1)
        self.netSlave.AddNetControl(None,None)
        self.netSlave.SetCfg(self.xdCfg,[],['vMsg'])
        self.netSlave.SetCB('vMsg',self.__doAddMsg__,self.lstMsg)
        
        self.lstMsg.SetCfgDoc(self.xdCfg)
        
        #self.netSlave=vtNetSecXmlGuiSlaveMultiple(self.pnNav,'Slave',pos=(0, 0),
        #            size=(16, 16),verbose=1)
        #self.fgsNav.AddWindow(self.netSlave, 0, border=0, flag=0)
        #self.fgsNav.Fit(self.pnNav)
        #self.pnNav.Layout()
        
        self.bAutoConnect=True
        
        self.vgpTree=None
        
        #self.nbData=vMsgListBook(self.slwTop,wx.NewId(),
        #        pos=(4,4),size=(470, 188),style=0,name="nbMsg")
        #self.nbData.SetConstraints(
        #    anchors.LayoutAnchors(self.nbData, True, True, True, True)
        #    )
        #self.nbData.SetDoc(self.netMsg,True)
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        self.Layout()
        self.Fit()
        self.OpenCfgFile()
        self.__makeTitle__()
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __doAddMsg__(self,appl,alias,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s'%(appl,alias),self)
        vtLog.CallStack('')
        netDoc=self.netSlave.GetNetDoc(appl,alias)
        self.lstMsg.AddMsg(appl,alias,netDoc,True)
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC Msg")
        fn=None
        #fn=self.netMsg.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        if self.node is not None:
            self.netMsg.endEdit(self.node)
        if event.GetTreeNodeData() is not None:
            self.netMsg.startEdit(node)
            self.nbData.SetNode(node)
        else:
            # grouping tree item selected
            self.nbData.SetNode(None)
            pass
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('login ... '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netMsg.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        lAlias=self.netMsg.GetMessagesApplAliasLst()
        bOffline=not self.netMsg.IsActive()
        self.netSlave.AddAliasLst(lAlias,bOffline)
        self.__getXmlBaseNodes__()
        evt.Skip()
    def __setSlaveAliasLst__(self,lAlias,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.DEBUG,'alias:%s'%(vtLog.pformat(lAlias)),self)
        self.PrintMsg(_('add slave aliases ...'))
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OpenFile(self,fn):
        return
        if self.netMsg.ClearAutoFN()>0:
            self.netMsg.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                #self.netMaster.SetCfgNode()
                self.netSlave.SetCfgNode()
                self.lstMsg.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            #self.netMaster.SetCfgNode()
            self.netSlave.SetCfgNode()
            self.lstMsg.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        self.netMsg.New()
        
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        return
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netMsg.getBaseNode()
        self.baseSettings=self.netMsg.getChildForced(self.netMsg.getRoot(),'settings')
        self.__getSettings__()
    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netMsg.getChild(self.netMsg.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vMsgSettingsDialog(self)
            dlg.SetXml(self.netMsg,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netMsg,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnFrameClose(self, event):
        if self.netSlave.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vMsg'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.netSlave.Save(bModified=True)
        self.netSlave.ShutDown()
        event.Skip()

    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netSlave.ShowPopup()
                self.bActivated=True
        event.Skip()
    def __setCfg__(self):
        try:
            node=self.xdCfg.getChildForced(None,'size')
            iX=self.xdCfg.GetValue(node,'x',int,20)
            iY=self.xdCfg.GetValue(node,'y',int,20)
            iWidth=self.xdCfg.GetValue(node,'width',int,800)
            iHeight=self.xdCfg.GetValue(node,'height',int,600)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.xdCfg.getChildForced(None,'layout')
            
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            
            self.xdCfg.AlignDoc()
            self.xdCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.xdCfg.SetValue(node,'nav_sash',iWidth)
            self.xdCfg.Save()
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'width',sz[0])
            self.xdCfg.SetValue(node,'height',sz[1])
            self.xdCfg.Save()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        try:
            self.hlp=wx.html.HtmlHelpController()
            self.hlp.AddBook('vMsgHelpBook.hhp')
            self.hlp.DisplayContents()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        pass
    def GetCfgData(self):
        d={}
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)

    def OnCbViewButton(self, event):
        event.Skip()

    def OnCbGoToButton(self, event):
        event.Skip()

    def OnCbDelButton(self, event):
        event.Skip()
        self.fgsData.Detach(self.lstMsg)
        self.lstMsg.Destroy()
        

    def OnCbCreateButton(self, event):
        event.Skip()
        #self.lstMsg=vtXmlMsgList(parent=self.pnData,pos=(0,0),size=(500,80))
        self.lstMsg=vtXmlMsgListNav(parent=self.pnData,pos=(0,0),size=(500,80))
        self.fgsData.AddWindow(self.lstMsg,1, border=4, flag=wx.EXPAND | wx.LEFT)
        lSettled=self.netSlave.GetSettled()
        for appl,alias,func,args,kwargs in lSettled:
            func(appl,alias,*args,**kwargs)
        self.fgsData.Layout()
