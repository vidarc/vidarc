#----------------------------------------------------------------------------
# Name:         vXmlContact.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlContact.py,v 1.6 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vContact.vXmlNodeContactRoot import vXmlNodeContactRoot
from vidarc.vApps.vContact.vXmlNodeCompany import vXmlNodeCompany
from vidarc.vApps.vContact.vXmlNodeCompanyAttr import vXmlNodeCompanyAttr
from vidarc.vApps.vContact.vXmlNodeCompanyAddr import vXmlNodeCompanyAddr
from vidarc.vApps.vContact.vXmlNodeDepartment import vXmlNodeDepartment
from vidarc.vApps.vContact.vXmlNodeDepartmentAttr import vXmlNodeDepartmentAttr
#from vidarc.vApps.vContact.vXmlNodeCompanyCategory import vXmlNodeCompanyCategory
from vidarc.vApps.vContact.vXmlNodeContact import vXmlNodeContact
from vidarc.vApps.vContact.vXmlNodeContactAttr import vXmlNodeContactAttr
from vidarc.vApps.vContact.vXmlNodeContactAddr import vXmlNodeContactAddr
from vidarc.vApps.vContact.vXmlNodeProvide import vXmlNodeProvide

from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML
from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd4IDAT8\x8d\xa5\x93?h\x13a\x18\xc6\x7fw=\xee.G\xae\x07MO=\x92\x13\
\xd4A09\x89\x93"R$\x06A\xd2t\x8aP\xc5?\xd15\xd1\x0eup\xea\xa0\x0e\xd1Q\x10\
\xe2\xa2\x83T\x14\x82\x88\x7f\xa0\x83 \x05\xc5-\xd0J\xa5B(\xc1EL\x8dihl\x141\
\xe7\x14\x9b4w \xf8N\x1f\x0f\xcf\xf3|\xcf\xfb}\xef+\x08\xe2\x10[\xcb)d\xdc\
\x01\x10x\x7f\xb5$l\xc5\xc4\x7f%za\x00B7\x81\xdf\xad~\xd55\x94\xfc\x08\xc5#S\
\x04\x02\x01\xda\xed6\x86ap\xea\xe55O\x9e\xa7\xc1\xf5\xbd\xa7\xa9V\xab\xb8\
\xaeK0\x18\xa4R\xa9peG\x8aX,F\xf6\xd5\xcd~\x03\xa7\x90q{\xfb\xbb\xe5dQT\x95d\
2I\xbd^\xc7\xb6m>./s\xb7XDU\x94>\xb1S\xc8\xb8b\xf7\xd0\x05\xb7Y\x16\x9a\xae\
\xd3Z_G\xd34\x9a\xcd&\xbaa\xb0=\x1caH\x928\xf93F\xaff\xa0\x85\xe7\xb3\xb3\
\x8c\x8e\x8c\xf0!\x12A\x93e:\x80\x86\xc8\xdb\x17O\xd8\x93\xbbD\xad\xb6\n\xb6\
\xcf\x1b<\x8a\x9ec*\x9dFL$\xb0l\x9b\xa5\xc5E\xe2\x87\x0e\xf3\xb9<\xcf\xc4\
\xbb\x06\xf7Vn3~\xe3"\xf3\xb5/\xde\x06J4\xca\x81\xc9I\x8e\xa6\xd3\xfc\xd2u.\
\xe7\xf3\xac6\xd6X\xd9m\xf1\xfa\xdb3\xc6/d\xd9\x15\xde\t\xb5M\x8d\x08\x9b\
\x7f\xaaH\x12\'r9\x9c\xb11&R)\xf6\xc7\xe3\x1c?\x96\xe0\xd3\xd7\x16\xf9\xc7wX\
\x93[<\xbd\xff\x80^\xcd\xc0 \x9d\xff\x11%\x18\n\xd1\xd8\xd8@\xe8tP\x15\x05ex\
\x98\xa5R\t\xd54\xf9^.\xf3\xe6\xec\xbe\xbf\x06\x03\xa3|\xd0\xb2x8=\xcd\xa8i\
\x12\xb6,\xccP\x08d\x99\xc6\xc2\x02\xbf\xe7\xe6833\xd3\xc7\xff\xefQ\x16\xbc\
\xb6\xd1\xcb\xd0o\x99<\xb7\xd1+\x8d_\xc2?o\xde\x98\xb8b\x1aLW\x00\x00\x00\
\x00IEND\xaeB`\x82' 

class vXmlContact(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='contactroot'
    APPL_REF=['vHum','vGlobals']
    def __init__(self,appl='vContact',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                        verbose=verbose,audit_trail=audit_trail)
            oRoot=vXmlNodeContactRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeContactRoot()
            self.RegisterNode(oRoot,False)
            oComp=vXmlNodeCompany()
            oCompAddr=vXmlNodeCompanyAddr()
            oCompAttr=vXmlNodeCompanyAttr()
            oDepart=vXmlNodeDepartment()
            oDepartAttr=vXmlNodeDepartmentAttr()
            #oCompCat=vXmlNodeCompanyCategory()
            oContact=vXmlNodeContact()
            oContactAttr=vXmlNodeContactAttr()
            #oAddr=vtXmlNodeAddr()
            #oAddrs=vXmlNodeContactAddr()
            oProvide=vXmlNodeProvide()
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            oCalc=vtXmlNodeCalc()
            self.RegisterNode(oComp,True)
            self.RegisterNode(oCompAddr,False)
            self.RegisterNode(oCompAttr,False)
            self.RegisterNode(oDepart,False)
            self.RegisterNode(oDepartAttr,False)
            #self.RegisterNode(oCompCat,False)
            self.RegisterNode(oContact,True)
            self.RegisterNode(oContactAttr,False)
            self.RegisterNode(oCompAddr,False)
            #self.RegisterNode(oAddrs,False)
            self.RegisterNode(oProvide,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(oCalc,False)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            self.LinkRegisteredNode(oRoot,oContact)
            self.LinkRegisteredNode(oRoot,oComp)
            self.LinkRegisteredNode(oComp,oComp)
            self.LinkRegisteredNode(oComp,oDepart)
            self.LinkRegisteredNode(oComp,oContact)
            self.LinkRegisteredNode(oComp,oCompAttr)
            self.LinkRegisteredNode(oComp,oCompAddr)
            #self.LinkRegisteredNode(oComp,oCompCat)
            #self.LinkRegisteredNode(oComp,oAddrs)
            self.LinkRegisteredNode(oComp,oProvide)
            self.LinkRegisteredNode(oComp,oLog)
            
            self.LinkRegisteredNode(oDepart,oContact)
            self.LinkRegisteredNode(oDepart,oDepartAttr)
            self.LinkRegisteredNode(oDepart,oCompAddr)
            self.LinkRegisteredNode(oDepart,oProvide)
            self.LinkRegisteredNode(oDepart,oLog)
            
            #oComp.AddNotContainedChild(oCompAddr.GetTagName())
            oComp.AddNotContainedChild(oDepart.GetTagName())
            oComp.AddNotContainedChild(oContact.GetTagName())
            oDepart.AddNotContainedChild(oContact.GetTagName())
            self.LinkRegisteredNode(oComp,oLog)
            self.LinkRegisteredNode(oContact,oContactAttr)
            #self.LinkRegisteredNode(oContact,oAddrs)
            self.LinkRegisteredNode(oContact,oProvide)
            self.LinkRegisteredNode(oContact,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'contacts')
    #def New(self,revision='1.0',root='contactroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'contacts')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
