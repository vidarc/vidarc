#----------------------------------------------------------------------------
# Name:         vXmlContactInputMulti.py
# Purpose:      input widget for contacts xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060720
# CVS-ID:       $Id: vXmlContactInputMulti.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputMultipleTree import vtInputMultipleTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vContact.vXmlContactTree import vXmlContactTree
import vidarc.tool.lang.vtLgBase as vtLgBase

class vXmlContactInputMulti(vtInputMultipleTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        try:
            _kwargs['columns']
        except:
            _kwargs['columns']=[(_(u'tag'),-1),(_(u'name'),-1),('id',-1)]
        try:
            _kwargs['show_attrs']
        except:
            _kwargs['show_attrs']=['tag','name','|id']
        apply(vtInputMultipleTree.__init__,(self,) + _args,_kwargs)
        
        self.tagGrp='contacts'
        self.tagName='contact'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vContact')
    def __createTree__(*args,**kwargs):
        tr=vXmlContactTree(**kwargs)
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
