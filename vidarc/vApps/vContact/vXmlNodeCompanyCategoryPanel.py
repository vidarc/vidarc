#Boa:FramePanel:vXmlNodeCompanyCategoryPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeCompanyCategoryPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060719
# CVS-ID:       $Id: vXmlNodeCompanyCategoryPanel.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTextML

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODECOMPANYCATEGORYPANEL, 
 wxID_VXMLNODECOMPANYCATEGORYPANELCHCCUSTOMER, 
 wxID_VXMLNODECOMPANYCATEGORYPANELCHCSUPPLIER, 
 wxID_VXMLNODECOMPANYCATEGORYPANELLBLAREA, 
 wxID_VXMLNODECOMPANYCATEGORYPANELLBLCAT, 
 wxID_VXMLNODECOMPANYCATEGORYPANELLBLDUM1, 
 wxID_VXMLNODECOMPANYCATEGORYPANELLBLDUM2, 
 wxID_VXMLNODECOMPANYCATEGORYPANELVIAREA, 
 wxID_VXMLNODECOMPANYCATEGORYPANELVICAT, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vXmlNodeCompanyCategoryPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCat, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsArea, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsCust, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsSup, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viArea, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsCat_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCat, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viCat, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsCust_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDum1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcCustomer, 2, border=4,
              flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsSup_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDum2, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcSupplier, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsCat = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCust = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSup = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsCat_Items(self.bxsCat)
        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_bxsCust_Items(self.bxsCust)
        self._init_coll_bxsSup_Items(self.bxsSup)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECOMPANYCATEGORYPANEL,
              name=u'vXmlNodeCompanyCategoryPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblCat = wx.StaticText(id=wxID_VXMLNODECOMPANYCATEGORYPANELLBLCAT,
              label=_(u'categorie'), name=u'lblCat', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(97, 30), style=wx.ALIGN_RIGHT)

        self.viCat = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODECOMPANYCATEGORYPANELVICAT,
              name=u'viCat', parent=self, pos=wx.Point(101, 0),
              size=wx.Size(202, 30), style=0)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODECOMPANYCATEGORYPANELLBLAREA,
              label=_(u'area'), name=u'lblArea', parent=self, pos=wx.Point(0,
              34), size=wx.Size(97, 24), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.viArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODECOMPANYCATEGORYPANELVIAREA,
              name=u'viArea', parent=self, pos=wx.Point(101, 34),
              size=wx.Size(202, 24), style=0)

        self.chcCustomer = wx.CheckBox(id=wxID_VXMLNODECOMPANYCATEGORYPANELCHCCUSTOMER,
              label=u'customer', name=u'chcCustomer', parent=self,
              pos=wx.Point(105, 62), size=wx.Size(198, 13), style=0)
        self.chcCustomer.SetValue(False)
        self.chcCustomer.SetMinSize(wx.Size(-1, -1))

        self.chcSupplier = wx.CheckBox(id=wxID_VXMLNODECOMPANYCATEGORYPANELCHCSUPPLIER,
              label=u'supplier', name=u'chcSupplier', parent=self,
              pos=wx.Point(105, 79), size=wx.Size(198, 13), style=0)
        self.chcSupplier.SetValue(False)
        self.chcSupplier.SetMinSize(wx.Size(-1, -1))

        self.lblDum1 = wx.StaticText(id=wxID_VXMLNODECOMPANYCATEGORYPANELLBLDUM1,
              label=u'', name=u'lblDum1', parent=self, pos=wx.Point(0, 62),
              size=wx.Size(101, 13), style=0)
        self.lblDum1.SetMinSize(wx.Size(-1, -1))

        self.lblDum2 = wx.StaticText(id=wxID_VXMLNODECOMPANYCATEGORYPANELLBLDUM2,
              label=u'', name=u'lblDum2', parent=self, pos=wx.Point(0, 79),
              size=wx.Size(101, 13), style=0)
        self.lblDum2.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viCat.SetTagName('categorie')
        
        self.viArea.SetTagNames('mainArea','name')
        self.viArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.viArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viArea.SetTagNames2Base(['globalsdata','globalsArea'])
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viCat.Clear()
        self.viArea.Clear()
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viArea.SetDocTree(dd['doc'],dd['bNet'])
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viCat.SetDoc(doc)
        self.viArea.SetDoc(doc,bNet,bExternal=True)
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.viCat.SetNode(self.node)
            self.viArea.SetNode(self.node)
            
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.viCat.GetNode(node)
            self.viArea.GetNode(node)
            
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viCat.Close()
        self.viArea.Close()
    def Cancel(self):
        # add code here
        self.viCat.Cancel()
        self.viArea.Cancel()
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viCat.Enable(False)
            self.viArea.Enable(False)
        else:
            # add code here
            self.viCat.Enable(True)
            self.viArea.Enable(True)
