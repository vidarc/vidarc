#----------------------------------------------------------------------------
# Name:         vContactInputTest.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060720
# CVS-ID:       $Id: vContactInputTest.py,v 1.1 2006/07/20 19:34:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.art.vtArt as vtArt
from vidarc.vApps.vContact.vXmlContact import *
from vidarc.vApps.vContact.vXmlContactInputTree import *
from vidarc.vApps.vContact.vXmlContactInputTreeSupplier import *
from vidarc.tool.xml.vtXmlDom import *
import sys

def create(self):
    self.doc=vtXmlDom()
    self.doc.Open(self.FNbase+'_0.xml')
    
    self.docContact=vXmlContact()
    self.docContact.Open('contact.xml')
    
    self.inTst1=vXmlContactInputTree(self.panel,-1,pos=(0,0),size=(200,30),name='viContact')
    self.inTst2=vXmlContactInputTreeSupplier(self.panel,-1,pos=(0,0),size=(200,30),name='viSupplier')
    #self.inUsr=vXmlHumInputSelUsrPanel(self.panel,-1,(0,0),(200,100),0,name='selUsr')
    
    self.inTst1.SetDoc(self.doc)
    self.inTst1.SetDocTree(self.docContact)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input01'])
    self.inTst1.SetNode(node)

    self.inTst2.SetDoc(self.doc)
    self.inTst2.SetDocTree(self.docContact)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input02'])
    self.inTst2.SetNode(node)

    return [self.inTst1,self.inTst2],-1,[]
def SetNode(self):
    pass

import test.vTestApplSizer
test.vTestApplSizer.run(__file__,create,SetNode)
