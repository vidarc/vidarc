#----------------------------------------------------------------------------
# Name:         vXmlNodeCompanyAttr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060806
# CVS-ID:       $Id: vXmlNodeCompanyAttr.py,v 1.2 2010/03/03 02:17:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0
    
class vXmlNodeCompanyAttr(vtXmlNodeAttrML):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='companyAttr',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'company attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return True
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True

