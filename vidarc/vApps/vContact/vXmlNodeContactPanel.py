#----------------------------------------------------------------------------
# Name:         vXmlNodeContactPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeContactPanel.py,v 1.5 2010/03/10 22:41:41 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeContactPanel(vtInputAttrCfgMLPanel):
    def __GetNode__(self,node):
        try:
            bDbg=False
            if VERBOSE>0 or self.VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if self.__isLogDebug__():
                self.__logDebug__('attrs:%s;attrsFlt:%s;lLstAttr:%s'%(\
                        self.__logFmt__(self.attrs),
                        self.__logFmt__(self.attrsFlt),
                        self.__logFmt__(self.lLstAttr)))
            if node is None:
                self.__markModified__(False)
                self.SetModified(False)
                return
            childs=self.doc.getChilds(node)
            d={}
            for c in childs:
                sAttrName=self.doc.getTagName(c)
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if self.funcIs2Skip is not None:
                    if self.funcIs2Skip(sAttr):
                        continue
                if self.objRegNode.IsAttrHidden(sAttr):
                    continue
                if self.__hasAttrKey__(sAttr):
                    self.doc.deleteNode(c,node)
            keys=self.attrs.keys()
            keys.sort()
            sTag=''
            sName=''
            for k in keys:
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                id=self.__getSelTypeID__(sAttr)
                d=self.attrs[k]
                if d.has_key(''):
                    if k=='firstname':
                        sVal=d['']
                        iLen=len(sVal)
                        if iLen>=3:
                            sTag=sVal[:3]
                        else:
                            sTag=sVal+'___'[:3-iLen]
                        sName=sVal
                    if k=='surname':
                        sVal=d['']
                        iLen=len(sVal)
                        if iLen>=3:
                            sTag=sVal[:3]+sTag
                        else:
                            sTag=sVal+'___'[:3-iLen]+sTag
                        sName=sVal+', '+sName
                    if id is None:
                        self.doc.setNodeText(node,k,d[''])
                    else:
                        c=self.doc.createSubNodeDict(node,{'tag':k,'attr':('aid','%08d'%id),'val':d['']})
                else:
                    kll=d.keys()
                    kll.sort()
                    for kl in kll:
                        if id is None:
                            self.doc.setNodeTextLang(node,k,d[kl],kl)
                        else:
                            c=self.doc.createSubNodeDict(node,{'tag':k,'attr':('aid','%08d'%id),'val':d[kl]})
                            self.doc.setAttribute(c,'language',kl)
            #self.objRegNode.SetTag(node,sTag)
            #self.objRegNode.SetName(node,sName)
            self.__delAttrFltDeleted__(node)
            if bDbg:
                self.__logDebug__('done'%())
        except:
            self.__logTB__()
