#----------------------------------------------------------------------------
# Name:         vXmlNodeDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vXmlNodeDepartment.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vContact.vXmlNodeCompany import vXmlNodeCompany

class vXmlNodeDepartment(vXmlNodeCompany):
    def __init__(self,tagName='department'):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        vXmlNodeCompany.__init__(self,tagName)
    def GetDescription(self):
        return _(u'department')
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x88IDAT8\x8d\xc5\x93o\n\x80 \x0c\xc5\xdf\xd4k\x95\xeb`\xd5\xd5V\x1d\
\xcc>DPs\xf6\x07\x83\x1e\x082\x9f\x0f\xb7\x1f\x129\x8f\x1a\x05]\x18\x87>YF\
\x11\xc14/t\x1b\x00\x00\x0cFD<\x07@\xcc\x178\xab\xa8/_\xc9\x0cx#\xb3\x85N\
\xba\xe7\t\xe4|q1s\xba:\'\xe7?hAc+\xe1\x02r\xc4"\xb2\xcd\xe0\x88\xad\x84k\
\x97\xf6\x06 \xc7\x16\xdb&Y{\xcb\x1b\x80|\xea\xa5\x16\x989i/\xd5\xfe\x85j\n\
\xff\x07\xac$\x1d);\x9d\xcb\xa6\xf2\x00\x00\x00\x00IEND\xaeB`\x82'
