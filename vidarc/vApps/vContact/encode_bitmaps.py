#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/VidMod_Contact_16.ico               images.py",
    "-a -u -n Plugin        img/VidMod_Contact_16.png               images.py",
    "-a -u -n Contact       img/Contact01_16.png                    images.py",
    "-a -u -n Factory       img/factroy01_16.png                    images.py",
    "-a -u -n Department    img/CompanyDepartment01_16.png          images.py",
        
    "-u -i -n Splash img/splashContact01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

