#----------------------------------------------------------------------------
# Name:         vXmlNodeContactAddr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060622
# CVS-ID:       $Id: vXmlNodeContactAddr.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeAddrMultiple import vtXmlNodeAddrMultiple

class vXmlNodeContactAddr(vtXmlNodeAddrMultiple):
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return True
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
