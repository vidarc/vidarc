#----------------------------------------------------------------------------
# Name:         vXmlNodeContact.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeContact.py,v 1.8 2010/03/03 02:17:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.tool.xml.vtXmlNodeContact import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeContactPanel import vXmlNodeContactPanel
        #from vXmlNodeContactAddDialog import *
        #from vXmlNodeContactEditDialog import *
        #from vidarc.tool.xml.vtXmlNodeContactPanel import *
        #from vidarc.tool.xml.vtXmlNodeCfgMLMultiplePanel import vtXmlNodeCfgMLMultiplePanel
        #from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

#class vXmlNodeContact(vtXmlNodeTag,vtXmlNodeContact):
class vXmlNodeContact(vtXmlNodeContact):
    #NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
    #        ('Category',None,'category',None),
    #    ]
    NODE_ATTRS=[('SurName',None,'surname',None),
        ('FirstName',None,'firstname',None),
        ]
    FUNCS_GET_SET_4_LST=['SurName','FirstName',]
    #FUNCS_GET_4_TREE=vtXmlNodeTag.FUNCS_GET_4_TREE+['Category',]
    FUNCS_GET_4_TREE=['SurName','FirstName',]
    ##FUNCS_GET_4_TREE=['surname','firstname',]
    #FUNCS_GET_4_TREE=['SurName','FirstName',]
    #FMT_GET_4_TREE=vtXmlNodeTag.FMT_GET_4_TREE+[('Category','')]
    FMT_GET_4_TREE=[('SurName',''),('FirstName','')]
    ##FMT_GET_4_TREE=[('surname',''),('firstname','')]
    #GRP_GET_4_TREE=None
    def __init__(self,tagName='contact',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        #vtXmlNodeTag.__init__(self,tagName)
        vtXmlNodeContact.__init__(self,tagName,cfgBase=cfgBase)
        ##self.ATTR_HIDDEN=['tag','name','description']
    def GetDescription(self):
        return _(u'contact')
    # ---------------------------------------------------------
    # specific
    #def GetCategory(self,node):
    #    return self.GetML(node,'category')
    #def SetCategory(self,node,val,lang=None):
    #    self.SetML(node,'category',val,lang)
    def SetSurName(self,node,sVal,lang=None):
        pass
        #return self.Get(node,'surname')
    def SetFirstName(self,node,sVal,lang=None):
        pass
        #return self.Get(node,'firstname')
    def GetSurName(self,node,lang=None):
        return self.Get(node,'surname')
    def GetFirstName(self,node,lang=None):
        return self.Get(node,'firstname')
    # ---------------------------------------------------------
    # inheritance
    def Build(self):
        vtXmlNodeContact.Build(self)
    def BuildLang(self):
        vtXmlNodeContact.BuildLang(self)
    def GetPossibleAcl(self):
        return vtXmlNodeContact.GetPossibleAcl(self) | vtSecXmlAclDom.ACL_MSK_MOVE
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return vtXmlNodeContact.GetAttrFilterTypes(self)
    def GetTranslation(self,name):
        return vtXmlNodeContact.GetTranslation(self,name)
    def GetNodeValues(self):
        return vtXmlNodeContact.GetNodeValues(self)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01UIDAT8\x8d\xa5\x93\xefM\xc30\x10\xc5\x7fN\xfb=\xd9\xa0+d\x816\xe7v\
\x01F\xe8\x08^\xa0\x8d\x81\x05\xb2A;B\x16\x00\x0eX\xa0#x\x04w\x000\x1f\xaa\
\x98\x04Z\t\x89\'Y\xf2\xe9|\xef\xde\xfd\xb11\xc5\x8c\xff`>6\xda\xfd.]{$"\xd8\
\xf5\xc6\\\xf3\x99AA\xbb\xdf%\xe7\x1c1\xc6\xec\\,\x16\x00Xk\x11\x11\x00\xee\
\x1f\x1e\'D\xc5-iUUM\x14\x0c\xe7\'n\x12\xc4\x189\x9f\xcf\xd9VUT\x95v\xbfK\
\xcdj\x99K\x9d\x10\x94e\x89\xaaf\xe9\xa7\xd3iB\xda\xb6-\x00\xafo\xef\xb9\x8c\
9@\xb3Z&\xf5\x9e\x18#1FB\x08\xf4}\x8fs\xee\x92\xd9\xfb\x89\x92f\xb5L\xde{\
\xeczc\xe6\x00\xa2\x8a\x8a\x10B\xa0\xaa*B\x08\x99\xac\xef{\xee\x80\x16\x8f\
\xf1\x1eU\xc5\xae7\xc6\xae7\xdf%\xa8\x08\x02\x1c\x8fG\x00\x0e\x87\x03\xce9\
\xea\xba\xa6\xeb:\x14\xb0h\xee\xc3\x18\x931n\xb7\xdbI\xf7\x01\xba\xae\x03.\
\x93\x18\x82\xc7{Q\x0c\xc1\xce\xb9_\xc1eY\xe6{\xd34\x99\xc0\x8b\xf0\xf2\xfc\
\x94r\x13\xc7\x99~b\xc8l\x8dA\xbc\x07\xd5\x89\x82\xef\x12>?\x92\xdeX\x16UETa\
\xd8\xc6\xd1\x181\xc5\x0cS\xccp\xce%/\x92\x06{|T5\xb9\xba\xbe\xee\xff\xefo\
\xbc\xb9\xca\x7f\xc5\x175\x98\x9c\xda%\x00\xc9\xf1\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeContactEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnContact',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeContactAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnContact',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeContactPanel
            #return vtXmlNodeCfgMLMultiplePanel
            #return vtInputAttrCfgMLPanel
        else:
            return None
