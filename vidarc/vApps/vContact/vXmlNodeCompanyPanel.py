#Boa:FramePanel:vXmlNodeCompanyPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeCompanyPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060720
# CVS-ID:       $Id: vXmlNodeCompanyPanel.py,v 1.5 2010/03/10 22:41:41 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    import vidarc.tool.input.vtInputTreeMLInternal
    import vidarc.tool.xml.vtXmlNodeTagPanel
    import vidarc.tool.input.vtInputTextML
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODECOMPANYPANEL, wxID_VXMLNODECOMPANYPANELLBLAREA, 
 wxID_VXMLNODECOMPANYPANELLBLDUM, wxID_VXMLNODECOMPANYPANELTGCUSTOMER, 
 wxID_VXMLNODECOMPANYPANELTGSUPPLIER, wxID_VXMLNODECOMPANYPANELVIAREA, 
 wxID_VXMLNODECOMPANYPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeCompanyPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsCustSupp_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDum, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.tgCustomer, 2, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.tgSupplier, 2, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsArea, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsCustSupp, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viArea, 4, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCustSupp = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_bxsCustSupp_Items(self.bxsCustSupp)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECOMPANYPANEL,
              name=u'vXmlNodeCompanyPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODECOMPANYPANELLBLAREA,
              label=_(u'area'), name=u'lblArea', parent=self, pos=wx.Point(0, 116),
              size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagPanel.vtXmlNodeTagPanel(id=wxID_VXMLNODECOMPANYPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(0, 0), size=wx.Size(304,
              104), style=0)

        self.viArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODECOMPANYPANELVIAREA,
              name=u'viArea', parent=self, pos=wx.Point(64, 116),
              size=wx.Size(239, 30), style=0)

        self.tgCustomer = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VXMLNODECOMPANYPANELTGCUSTOMER,
              bitmap=vtArt.getBitmap(vtArt.Customer), label=_(u'customer'), name=u'tgCustomer',
              parent=self, pos=wx.Point(63, 150), size=wx.Size(110, 30),
              style=0)
        self.tgCustomer.SetToggle(False)

        self.tgSupplier = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VXMLNODECOMPANYPANELTGSUPPLIER,
              bitmap=vtArt.getBitmap(vtArt.Supplier), label=_(u'supplier'), name=u'tgSupplier',
              parent=self, pos=wx.Point(189, 150), size=wx.Size(114, 30),
              style=0)
        self.tgSupplier.SetToggle(False)

        self.lblDum = wx.StaticText(id=wxID_VXMLNODECOMPANYPANELLBLDUM,
              label=u'', name=u'lblDum', parent=self, pos=wx.Point(0, 150),
              size=wx.Size(59, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[self.tgCustomer,self.tgSupplier],
                lEvent=[(self.tgCustomer,wx.EVT_BUTTON),
                        (self.tgSupplier,wx.EVT_BUTTON)])
        
        self.viTag.SetSuppressNetNotify(True)
        self.viArea.SetTagNames('mainArea','name')
        self.viArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.viArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viArea.SetTagNames2Base(['globalsdata','globalsArea'])
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        try:
            # add code here
            self.viTag.Clear()
            self.viArea.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__(''%())
        return
        # add code here
        self.viTag.SetNetDocs(d)
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viArea.SetDocTree(dd['doc'],dd['bNet'])
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viTag.SetRegNode(obj)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetDoc(doc,bNet=False)       # 100307:wro don't watch events
            self.viArea.SetDoc(doc,bNet,bExternal=True)
            if dDocs is not None:
                if 'vGlobals' in dDocs:
                    dd=dDocs['vGlobals']
                    self.viArea.SetDocTree(dd['doc'],dd['bNet'])
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.__SetNode__(self.node)
            self.viArea.SetNode(self.node)
            self.tgCustomer.SetValue(self.objRegNode.GetCustomer(self.node))
            self.tgSupplier.SetValue(self.objRegNode.GetSupplier(self.node))
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.__GetNode__(node)
            self.viArea.GetNode(node)
            self.objRegNode.SetCustomer(node,self.tgCustomer.GetValue())
            self.objRegNode.SetSupplier(node,self.tgSupplier.GetValue())
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Close()
        self.viArea.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Cancel()
        self.viArea.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viArea.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viArea.Enable(True)
