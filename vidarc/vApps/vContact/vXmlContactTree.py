#----------------------------------------------------------------------------
# Name:         vXmlContactTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlContactTree.py,v 1.2 2007/04/18 15:59:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTreeReg import *
from vidarc.vApps.vContact.vNetContact import *

import vidarc.vApps.vContact.images as imgContact

class vXmlContactTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        return
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfos_old(self):
        self.SetNodeInfos(['tag','name','|id'])
    def SetupImageList(self):
        if vtXmlGrpTreeReg.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgContact.getPluginImage(),
                            imgContact.getPluginImage(),True)
            self.__addElemImage2ImageList__('Contact',
                            imgContact.getPluginImage(),
                            imgContact.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)
