#----------------------------------------------------------------------------
# Name:         vXmlNodeCompany.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060622
# CVS-ID:       $Id: vXmlNodeCompanyCategory.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeCompanyCategoryPanel import *
        #from vXmlNodeCompanyEditDialog import *
        #from vXmlNodeCompanyAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeCompanyCategory(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Category',None,'category',None),
        ]
    FUNCS_GET_SET_4_LST=['Category',]
    FUNCS_GET_4_TREE=['Category',]
    FMT_GET_4_TREE=[('Category','')]
    GRP_GET_4_TREE=None
    #ATTR_HIDDEN=['Tag','name','description']
    def __init__(self,tagName='companyCategory'):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'category')
    # ---------------------------------------------------------
    # specific
    def GetCategory(self,node):
        return self.GetML(node,'category')
    def SetCategory(self,node,val,lang=None):
        self.SetML(node,'category',val,lang)
    def GetCustomer(self,node):
        sVal=self.Get(node,'customer')
        return sVal in ['1','True']
    def SetCustomer(self,node,val):
        if val:
            sVal='1'
        else:
            sVal='0'
        self.Set(node,'customer',sVal)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [
            ('category',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        if name=='category':
            return _(u'category')
        else:
            return '???'
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02`IDAT8\x8d\x8d\x93\xbfK\x1bq\x18\xc6?w\xe7AB\xa8\xf1z\x17\xa1\xb9\
\x98\xd0+iZ\x02R\r\x84\xa0\xb4\xd9TtR\xba:t\x91\xe2"\xb8\t\x1d\xfa7\xd8\xb5P\
(\xd8\xb1TT\x10\x82\x88`\x82\xda%5d\x10c\xad\xbf\x82\xb1\xa0\x84$\x12\x8dw\
\xdf\x0e\x05\xdb\x0e\x89}\xe1\x9d\x9e\x87\x87\xcf\xfb\xc2\x83$+\xb4\xda\x9e\
\x9e/bffF4\xd3e\xee\x98\x8b\x8b\x0b:;;\x9b\xea\x92$+M\xc5\xf6{_\x85i\xbegb\
\xe21\xd5j\x15\xb7\xdbM8\x1cfe\xc5f\xf6\xddK\thM\xd0\xd1\xf1\x8dh\xf4\'[[[\
\xd8\xb6\x8d\xcb\xe5\xa2\\.\xe3\xf3\xe5o=m\xad\x02\xfc\xfe\x07\x0c\x0f\x0f\
\xe3r\xb9\xf0x<x\xbd^\xd2\xe94\x85B\xe1\xd6\xd3\x92@\x96e\xf2\xf9<\xf5z\x9d\
\xcb\xcbK666\xe8\xeb\xeb#\x16\x8b\xfd_@[\x9b\x8a$I\x1c\x1d\x1d\xe1v\xbb\x19\
\x1a\x1abww\x97\xe3\xe3\xe3\xbbO\x08v\x15\x84,\x7f\xa4^\xaf\xb3\xb6\xb6\x86i\
\x9a,--\x11\x89DPU\xb59A\xf2\xc5s\xf1\xf4ID\x8c\x8d\xcdbY\x87,//355E6\x9bepp\
\x90\xd1\xd1Q\x0c\xc3\xf8\x97 `\xee\x88\x8e\x8e,\xa7\xa7\xa7\x8c\x8f\xbbY]]\
\x05`dd\x84\\.G\xb9\\\xe6\xea\xea\x8a\xcd\xcdM\xd6\xd7\xd79<<\xc4z\xf8A|\xdf\
\x7f%I\x93\x93\x93B\x08\x81\xdf\xefg``\x80\x85\x85\x05\xce\xce\xce\xe8\xed\
\xedE\x08A,\x16#\x93\xc9\xb0\xbf\xbf\x8f\xa6i\x9c\x9f\x9fsrrB\xb1X\xe4\xe6\
\xe6\rr<\x1egzz\x9a\xae\xae.R\xa9\x14\x8dF\x83h4\x8a\xe38H\x92D:\x9d\xc60\
\x0c\x92\xc9$\x00\xc5b\x91@ @\xb5Z\xfd\xfd\x83Z\xad\xc6\xc1\xc1\x01\x86a\xe0\
\xf3\xf9\xd04\x8dZ\xadF"\x91 \x1e\x8f\xb3\xb3\xb3\x83\xaa\xaa\xa4R)\x14EA\
\xd7u\xae\xaf\xaf\xe9\xef\xefG\xd7\xef\xa3\x94J\xaf\xdf\xee\xed\x15\x08\x85\
\xa0\xd1h`\xdb6\x8e\xe3\xb0\xb8\xb8\x88eYh\x9aF\xa5R\xa1T*Q\xa9T\xd0u\x1d\
\xcb\xb2\xe8\xee\xeef~\xbe\xe7O\x17\x1eY?\x84\xaafH$\x96\xb1m\x9bP(\x84\xe38\
\x08!\x08\x06\x83h\x9a\x86\xa2(x<\x1e\xe6\xe6\xc2\xcc}\x8aH\xd0\xa4L=\xcf>\
\x0b\xd34io\xf7\xa2(2B\x00\x08\xb6\xb7u\xb6s\x86\xf4\xb7\xf7\x17\xb1i\xeb\
\xfc\xa3W\xa8V\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeCompanyCategoryPanel
        else:
            return None
