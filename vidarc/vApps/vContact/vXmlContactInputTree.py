#----------------------------------------------------------------------------
# Name:         vXmlContactInputTree.py
# Purpose:      input widget for contact xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060720
# CVS-ID:       $Id: vXmlContactInputTree.py,v 1.2 2007/11/22 11:13:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vContact.vXmlContactTree import vXmlContactTree

class vXmlContactInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='contact'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vContact')
    def __createTree__(*args,**kwargs):
        tr=vXmlContactTree(**kwargs)
        #tr.SetNodeInfos(['tag','firstname',args[0].tagNameInt,'|id'])
        #tr.SetGrouping([],[('surname',''),('firstname',''),(args[0].tagNameInt,'')])
        #tr.SetSkipInfo(['users'])
        #tr.SetValidInfo(['user'])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        try:
            if doc.getTagName(node)=='contact':
                oReg=doc.GetRegByNode(node)
                if oReg is not None:
                    l=oReg.AddValuesToLst(node)
                    if len(l)>0:
                        return u', '.join(l)
        except:
            vtLog.vtLngTB(self.GetName())
        return doc.getNodeText(node,self.tagNameInt)
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'UpdateLang',
        #                self)
        return
    def __setNode__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__setNode__;node:%s'%(self.node),
        #                self)
        try:
            docTree=self.docTreeTup[0]
            if self.trShow is not None:
                sVal=self.trShow(self.docTreeTup[0],self.nodeSel,self.doc.GetLang())
            else:
                sVal=''
            self.doc.setNodeText(node,self.tagName,sVal)
            tmp=self.doc.getChild(node,self.tagName)
            if tmp is not None:
                if len(self.fid)==0:
                    self.doc.removeAttribute(tmp,'fid')
                else:
                    self.doc.setForeignKey(tmp,attr='fid',attrVal=self.fid,appl=self.appl)
        except:
            vtLog.vtLngTB(self.GetName())
