#----------------------------------------------------------------------------
# Name:         vXmlContactInputTreeSupplier.py
# Purpose:      input widget for contact / supplier xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060720
# CVS-ID:       $Id: vXmlContactInputTreeSupplier.py,v 1.1 2006/07/20 19:34:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vContact.vXmlContactTree import vXmlContactTree

class vXmlContactInputTreeSupplier(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='supplier'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vContact')
    def __createTree__(*args,**kwargs):
        tr=vXmlContactTree(**kwargs)
        o=args[0].docTreeTup[0].GetReg('company')
        tr.SetSkipInfo(['contact'])
        tr.SetBlockInfo([o.IsNotSupplier])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
