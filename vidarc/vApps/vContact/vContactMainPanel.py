#Boa:FramePanel:vContactMainPanel
#----------------------------------------------------------------------------
# Name:         vContactMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060624
# CVS-ID:       $Id: vContactMainPanel.py,v 1.8 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
import time

try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.vApps.common.vSystem as vSystem
    
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vContact.vNetContact import *
    from vidarc.vApps.vContact.vXmlContactTree import *
    from vidarc.tool.xml.vtXmlNodeRegListBook import *
    
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')
def create(parent):
    return vContactMainPanel(parent)

[wxID_VCONTACTMAINPANEL, wxID_VCONTACTMAINPANELPNDATA, 
 wxID_VCONTACTMAINPANELSLWNAV, wxID_VCONTACTMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(4)]

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getPluginBitmap())
    return icon

class vContactMainPanel(wx.Panel):
    VERBOSE=0
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)


    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCONTACTMAINPANEL,
              name=u'vContactMainPanel', parent=prnt, pos=wx.Point(325, 29),
              size=wx.Size(650, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VCONTACTMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VCONTACTMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VCONTACTMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 4), size=wx.Size(424, 364),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VCONTACTMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(416, 356), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vContact')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.xdCfg=vtXmlDom(appl='vContactCfg',audit_trail=False)
        
        appls=['vContact','vHum','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netContact=self.netMaster.AddNetControl(vNetContact,'vContact',synch=True,
                                            verbose=0,audit_trail=True)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vContactNetMaster')
        
        self.vgpTree=vXmlContactTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trContact",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.EnableMoveMenu(True)
        self.vgpTree.EnableClipMenu(True)
        
        self.nbComp=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbCompany")
        oComp=self.netContact.GetRegisteredNode('company')
        self.nbComp.SetDoc(self.netContact,True)
        self.viRegSel.AddWidgetDependent(self.nbComp,oComp)
        self.nbComp.Show(False)

        self.nbDepart=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbDepart")
        oDepart=self.netContact.GetReg('department')
        self.nbDepart.SetDoc(self.netContact,True)
        self.viRegSel.AddWidgetDependent(self.nbDepart,oDepart)
        self.nbDepart.Show(False)
        
        self.nbContact=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbContact")
        oContact=self.netContact.GetReg('contact')
        self.nbContact.SetDoc(self.netContact,True)
        self.viRegSel.AddWidgetDependent(self.nbContact,oContact)
        self.nbContact.Show(False)
        
        self.viRegSel.SetDoc(self.netContact,True)
        self.viRegSel.SetRegNode(oComp,bIncludeBase=True)
        self.viRegSel.SetRegNode(oDepart,bIncludeBase=True)
        self.viRegSel.SetRegNode(oContact,bIncludeBase=True)
        self.viRegSel.SetNetDocs(self.netContact.GetNetDocs())
        
        #try:
        #    self.tbMain=parent.tbMain
        #    self.dToolBar=parent.dToolBar
        #except:
        #    pass
        
        self.vgpTree.SetDoc(self.netContact,True)
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,
                ##lFindTagName=['tag','name','surname','firstname'],
                lFindTagName={'company':['tag','name'],
                        'contact':['surname','firstname']},
                origin='vContact:thdFindQuick')
        #self.thdFindQuick.BindEvents(funcProc=self.OnFindQuickProc,
        #            funcFinished=self.OnFindQuickFin)
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netContact
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        try:
            id=wx.NewId()
            sz=self.GetClientSize()
            self.tbMain = wx.ToolBar(id=id,
                    name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                    size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
            self.tbMain.SetToolBitmapSize(wx.Size(20,20))
            self.dToolBar={}
            self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
            #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
            #                _(u"duplicate"))
            #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
            self.tbMain.AddSeparator()
            id=wx.NewId()
            self.dToolBar['quickFind']=id
            txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
            self.tbMain.AddControl(txtCtrl)
            self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
            self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
            self.txtQuickFind=txtCtrl
            
            parent.SetToolBar(self.tbMain)
            self.tbMain.Realize()
            return self.tbMain
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickProc(self,evt):
        try:
            self.gProcess.SetRange(evt.GetCount())
            self.gProcess.SetValue(evt.GetAct())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFindQuickFin(self,evt):
        try:
            self.gProcess.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        event.Skip()
    def OpenFile(self,fn):
        if self.netContact.ClearAutoFN()>0:
            self.netContact.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Contact module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Contact'),desc,version
