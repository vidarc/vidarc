#----------------------------------------------------------------------------
# Name:         vMDIFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060529
# CVS-ID:       $Id: vMDIFrame.py,v 1.46 2010/05/19 13:35:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import time
import sys
import types
import zipfile
import webbrowser,urllib

import vidarc.config.vcCust as vcCust
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.images as imgCommon
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.tool.log.vtLogFileViewerFrame import *
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThreadMixinWX import vtThreadMixinWX
from vidarc.tool.misc.vtmThread import vtmThreadInterFace
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.gui.vtgPopup import vtgPopup
from vidarc.vTools.vCalendar.vCalendarPanel import vCalendarPanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.gui.vtgCore as vtgCore
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLogScreen as vtLogScreen
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

class vFrameThread(vtThread,vtThreadMixinWX):
    def __init__(self,par,bPost=False,verbose=0,origin=None):
        vtThread.__init__(self,par,bPost,verbose,origin)
        vtThreadMixinWX.__init__(self)

class vMDIFrame(vtmThreadInterFace,vMainFrame,vStatusBarMessageLine):
    VERBOSE=0
    STATUS_PROCESS=1
    STATUS_TEXT_POS=2
    STATUS_LOG_POS=-1
    STATUS_CLK_POS=-1
    def __init__(self,appl,bToolBar=False,iNotifyTime=1000):
        global _
        _=vtLgBase.assignPluginLang('common')
        self.appl=appl
        self.dCfg={'x':10,'y':10,'width':400,'height':300}
        self.cfgFunc=None
        self.cfgOrigin=None
        self.bActivated=False
        self.autoConn=None
        self.mnbMain = wx.MenuBar()
        self.__createMenus__()
        self.SetMenuBar(self.mnbMain)
        self.__createStatusBar__()
        vMainFrame.__init__(self)
        vStatusBarMessageLine.__init__(self,self.sbStatus,self.STATUS_TEXT_POS,-1)#5000)
        self.zMsgLast=-1#time.clock()
        self.lScreeShots=[]
        
        #self.Bind(wx.EVT_CLOSE, self.OnMainClose)
        # 070702:wro make sure this method is called at first
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnMainClose)
        #self.Bind(wx.EVT_ACTIVATE, self.OnMainActivate)
        #self.Bind(wx.EVT_SIZE, self.OnMainFrameSize)
        #self.Bind(wx.EVT_MOVE, self.OnMainFrameMove)
        wx.CallAfter(self.Bind,wx.EVT_SIZE, self.OnMainFrameSize)
        self.Bind(wx.EVT_IDLE, self.OnMainFrameIdle)
        
        self.thdFrm=vFrameThread(self,bPost=True,
                origin=':'.join([self.appl,'frmThd']))
        vtmThreadInterFace.__init__(self,self.thdFrm)
        
        # setup statusbar
        self.sbStatus.Bind(wx.EVT_RIGHT_UP, self.OnSbMainRightUp)
        if self.STATUS_PROCESS>=0:
            rect = self.sbStatus.GetFieldRect(self.STATUS_PROCESS)
            self.gProcess = wx.Gauge(
                        self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                        (rect.width-4, rect.height-4), 
                        wx.GA_HORIZONTAL|wx.GA_SMOOTH
                        )
            self.gProcess.SetRange(1000)
            self.gProcess.SetValue(0)
        else:
            self.gProcess=None
        self.iProcCount=1
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        if iNotifyTime>0:
            iNotifyTime=max(500,iNotifyTime)
            #self.timer = wx.PyTimer(self.Notify)
            self.timer=wx.Timer(self)
            self.Bind(wx.EVT_TIMER,self.OnNotify)
            self.timer.Start(iNotifyTime)
            #self.Notify()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def __createMenus__(self):
        self.__createMenuFile__()
        self.__createMenuView__()
        self.__createMenuAnalyse__()
        self.__createMenuTools__()
        self.__createMenuHelp__()
    def __createMenuFileIds__(self):
        if not hasattr(self,'popupIdExit'):
            self.popupIdExit=wx.NewId()
            wx.EVT_MENU(self,self.popupIdExit,self.OnExit)
    def __createMenuFile__(self):
        self.__createMenuFileIds__()
        self.mnFile = wx.Menu(title=u'')
        self.mnFile.AppendSeparator()
        mnIt=wx.MenuItem(self.mnFile,self.popupIdExit,_('E&xit\tALT+X'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.PowerDown))
        self.mnFile.AppendItem(mnIt)
        self.mnbMain.Append(menu=self.mnFile, title=_(u'&File'))

    def __createMenuViewIds__(self):
        #if not hasattr(self,'popupIdView'):
        #    self.popupIdExit=wx.NewId()
        #    wx.EVT_MENU(self,self.popupIdExit,self.OnExit)
        pass
    def __createMenuView__(self):
        self.__createMenuViewIds__()
        self.mnView = wx.Menu(title=u'')
        #mnIt=wx.MenuItem(self.mnFile,self.popupIdExit,_('E&xit\tALT+X'))
        #mnIt.SetBitmap(vtArt.getBitmap(vtArt.Exit))
        #self.mnView.AppendItem(mnIt)
        self.mnbMain.Append(menu=self.mnView, title=_(u'&View'))

    def __createMenuAnalyseIds__(self):
        #if not hasattr(self,'popupIdView'):
        #    self.popupIdExit=wx.NewId()
        #    wx.EVT_MENU(self,self.popupIdExit,self.OnExit)
        pass
    def __createMenuAnalyse__(self):
        self.__createMenuAnalyseIds__()
        self.mnAnalyse = wx.Menu(title=u'')
        #mnIt=wx.MenuItem(self.mnFile,self.popupIdExit,_('E&xit\tALT+X'))
        #mnIt.SetBitmap(vtArt.getBitmap(vtArt.Exit))
        #self.mnView.AppendItem(mnIt)
        self.mnbMain.Append(menu=self.mnAnalyse, title=_(u'&Analyse'))
        
    def __createMenuToolsIds__(self):
        if not hasattr(self,'popupIdToolsRequestLock'):
            self.popupIdToolsRequestLock=wx.NewId()
            wx.EVT_MENU(self,self.popupIdToolsRequestLock,self.OnMnToolsMn_tools_req_lockMenu)
            if hasattr(self,'dSettings'):
                self.popupIdSettings=wx.NewId()
                wx.EVT_MENU(self,self.popupIdSettings,self.OnMnToolsMn_tools_settings)
    def __createMenuTools__(self):
        self.__createMenuToolsIds__()
        self.mnTools = wx.Menu(title=u'')
        if hasattr(self,'popupIdSettings'):
            self.mnTools.AppendSeparator()
            mnIt=wx.MenuItem(self.mnTools,self.popupIdSettings,_(u'Settings'))
            mnIt.SetBitmap(imgCommon.getSettingsBitmap())
            self.mnTools.AppendItem(mnIt)
        self.mnTools.AppendSeparator()
        mnIt=wx.MenuItem(self.mnTools,self.popupIdToolsRequestLock,_(u'Request Lock\tF4'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Pin))
        self.mnTools.AppendItem(mnIt)
        self.mnbMain.Append(menu=self.mnTools, title=_(u'&Tools'))

    def __createMenuHelpIds__(self):
        if not hasattr(self,'popupIdHelp'):
            self.popupIdHelp=wx.NewId()
            self.popupIdAbout=wx.NewId()
            self.popupIdLog=wx.NewId()
            self.popupIdErrRep=wx.NewId()
            self.popupIdScreenShotTop=wx.NewId()
            self.popupIdScreenShotMDI=wx.NewId()
            wx.EVT_MENU(self,self.popupIdHelp,self.OnHelp)
            wx.EVT_MENU(self,self.popupIdAbout,self.OnAbout)
            wx.EVT_MENU(self,self.popupIdLog,self.OnLog)
            wx.EVT_MENU(self,self.popupIdErrRep,self.OnErrRep)
            wx.EVT_MENU(self,self.popupIdScreenShotTop,self.OnScreenShotTop)
            wx.EVT_MENU(self,self.popupIdScreenShotMDI,self.OnScreenShotMDI)
    def __createMenuHelp__(self):
        self.__createMenuHelpIds__()
        self.mnHelp = wx.Menu(title=u'')
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdHelp,_('?\tF1'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Help))
        self.mnHelp.AppendItem(mnIt)
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdAbout,_('About'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.About))
        #mnIt.SetBitmap(wx.ArtProvider_GetBitmap(wx.ART_HELP_BOOK, wx.ART_TOOLBAR, (16,16)))
        self.mnHelp.AppendItem(mnIt)
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdLog,_('Log'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.FirstAid))
        self.mnHelp.AppendItem(mnIt)
        self.mnbMain.Append(menu=self.mnHelp, title=_(u'Help'))
        self.mnHelp.AppendSeparator()
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdScreenShotTop,_('Screenshot full\tF11'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Snap))
        self.mnHelp.AppendItem(mnIt)
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdScreenShotMDI,_('Screenshot plugin\tF12'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Snap))
        self.mnHelp.AppendItem(mnIt)
        self.mnHelp.AppendSeparator()
        mnIt=wx.MenuItem(self.mnHelp,self.popupIdErrRep,_('Error Report'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Bug))
        self.mnHelp.AppendItem(mnIt)
    def AddMenuItems(self,sMnName,iOfs,l):
        if hasattr(self,sMnName):
            mn=getattr(self,sMnName)
            sAttr='_popIdMenu'
            if hasattr(self,sAttr):
                d=getattr(self,sAttr)
            else:
                d={}
                setattr(self,sAttr,d)
                #wx.EVT_MENU(self,self.popupIdHelp,self.OnHelp)
            def addItems(mn,d,iOfs,l):
                for t in l:
                    if t is None:
                        mn.InsertSeparator(iOfs)
                        iOfs+=1
                    elif type(t)==types.ListType:
                        iId=wx.NewId()
                        mnSub=wx.Menu()
                        addItems(mnSub,d,0,t[5])
                        mnIt=wx.MenuItem(mn,iId,t[0],subMenu=mnSub)
                        if t[1] is not None:
                            mnIt.SetBitmap(t[1])
                        wx.EVT_MENU(self,iId,self.OnAddMenuItems)
                        d[iId]=(t[2],t[3],t[4])
                        mn.InsertItem(iOfs,mnIt)
                        iOfs+=1
                    else:
                        iId=wx.NewId()
                        mnIt=wx.MenuItem(mn,iId,t[0])
                        if t[1] is not None:
                            mnIt.SetBitmap(t[1])
                        wx.EVT_MENU(self,iId,self.OnAddMenuItems)
                        d[iId]=(t[2],t[3],t[4])
                        mn.InsertItem(iOfs,mnIt)
                        iOfs+=1
            addItems(mn,d,iOfs,l)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'menu:%s;d:;%s'%(sMnName,
                        vtLog.pformat(self._popIdMenu)),self)
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'menu:%s not found'%(sMnName),self)
    def OnAddMenuItems(self,evt):
        try:
            eId=evt.GetId()
            sLang=None
            if eId in self._popIdMenu:
                t=self._popIdMenu[eId]
                t[0](*t[1],**t[2])
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'eId:%d not found;%s'%(eId,
                        vtLog.pformat(self._popIdMenu)),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def __createStatusBar__(self):
        self.sbStatus = wx.StatusBar(id=-1,
              name='statusBar', parent=self, style=wx.ST_SIZEGRIP)
        iCount=3
        lSz=[20, 120, -1]
        if self.STATUS_LOG_POS>=0:
            iCount+=1
            lSz.append(100)
        if self.STATUS_CLK_POS>=0:
            iCount+=1
            #lSz.append(200)
            tSz=self.sbStatus.GetTextExtent('XXXX YYYY-MM-DD HH:MM:SS +00:00 xx:000xxxx')
            lSz.append(tSz[0])
        if self.STATUS_LOG_POS<0:
            if self.STATUS_CLK_POS>=iCount:
                self.STATUS_CLK_POS=iCount-1
        if 0:
            iCount+=1
            lSz.append(38)
        self.sbStatus.SetFieldsCount(iCount)
        self.sbStatus.SetStatusText(number=0, text=u'')
        self.sbStatus.SetStatusText(number=1, text=u'')
        self.sbStatus.SetStatusText(number=2, text=u'')
        self._txtLog=None
        self._dlgLog=None
        if self.STATUS_LOG_POS>=0:
            rect = self.sbStatus.GetFieldRect(self.STATUS_LOG_POS)
            self._txtLog=wx.TextCtrl(self.sbStatus,
                    pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),
                    style=wx.TE_READONLY|wx.ALIGN_LEFT|wx.NO_BORDER|wx.NO_3D)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
            self._txtLog.SetBackgroundColour(bkgCol)
            self._txtLog.Bind(wx.EVT_RIGHT_DOWN, self.OnTextLogRightDown)
            self._txtLog.Bind(wx.EVT_MIDDLE_DOWN, self.OnTextLogMiddleDown)
            self._dlgLog=None
            #self.sbStatus.SetStatusText(number=3, text=u'')
            #if self.STATUS_CLK_POS>=0:
            #    self.sbStatus.SetStatusText(number=4, text=u'')
        #else:
        #    if self.STATUS_CLK_POS>=0:
        #        self.sbStatus.SetStatusText(number=3, text=u'')
        
        self.sbStatus.SetStatusWidths(lSz)
        self.SetStatusBar(self.sbStatus)
        self._zClk=None
        self._txtClk=None
        self._iClkStyle=0
        if self.STATUS_CLK_POS>=0:
            rect = self.sbStatus.GetFieldRect(self.STATUS_CLK_POS)
            self._zClk=vtDateTime(True)
            self._txtClk=wx.TextCtrl(self.sbStatus,
                    pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),
                    style=wx.TE_READONLY|wx.ALIGN_RIGHT|wx.NO_BORDER|wx.NO_3D)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
            self._txtClk.SetBackgroundColour(bkgCol)
            self._txtClk.Bind(wx.EVT_RIGHT_DOWN, self.OnTextClkRightDown)
            self._txtClk.Bind(wx.EVT_MIDDLE_DOWN, self.OnTextClkMiddleDown)
            self._dlgCalendar=None
    def OnNotify(self,evt):
        self.Notify()
    def OnTextLogRightDown(self,evt):
        try:
            pass
        except:
            self.__logTB__()
    def OnTextLogMiddleDown(self,evt):
        pass
    def OnTextClkRightDown(self,evt):
        try:
            if self._dlgCalendar is None:
                self._dlgCalendar=vtgPopup(self._txtClk,
                        vCalendarPanel,kind='popup',
                        widArrange=self._txtClk,iArrange=3)
            if self._dlgCalendar is None:
                return
            if self._dlgCalendar.IsShown():
                self._dlgCalendar.Show(False)
            else:
                self._dlgCalendar.Show(True)
        except:
            self.__logTB__()
    def OnTextClkMiddleDown(self,evt):
        self._iClkStyle+=1
        if self._iClkStyle>2:
            self._iClkStyle=0
    def Notify(self):
        # 061006 wro read out queue infos and display them in statusbar and related popup-window
        try:
            if wx.Thread_IsMain():
                pass
            else:
                vtLog.vtLngCur(vtLog.ERROR,'should not be called by thread','root')
                return
            if self.STATUS_CLK_POS>=0:
                # show time information
                #t = time.localtime(time.time())
                #st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
                #self.SetStatusText(st, self.STATUS_CLK_POS)
                self._zClk.Now()
                if self._iClkStyle==0:
                    st=self._zClk.GetDateTimeStr(' ')
                    sDy=self._zClk.GetDayName()
                    sWk=_(u'wk:%02d')%(self._zClk.GetWeek())
                    st=u''.join([sDy,u' ',st[:19],st[-6:],u' ',sWk])
                elif self._iClkStyle==1:
                    self._zClk.Convert2GM()
                    st=self._zClk.GetDateTimeStr(' ')
                    sDy=self._zClk.GetDayName()
                    sWk=_(u'wk:%02d')%(self._zClk.GetWeek())
                    st=u''.join([sDy,u' ',st[:19],st[-6:],u' ',sWk])
                else:
                    st=''
                iInsPos,iInsLast=self._txtClk.GetSelection()
                self._txtClk.SetValue(st)
                if iInsPos>=0:
                    self._txtClk.SetSelection(iInsPos,iInsLast)
            self.__makeTitle__()
            
            if self.STATUS_TEXT_POS>=0:
                # show message information
                tup=self.GetPrintMsgTup()
                if tup is not None:
                    bIsShown=self.IsShownPopupStatusBar()
                    while tup is not None:
                        if bIsShown:
                            self.AddMsg2PopupStatusBar(tup,0)
                        tupLast=tup
                        tup=self.GetPrintMsgTup()
                    self.sb.SetStatusText(tupLast[0], self.STATUS_TEXT_POS)
                    #self._txtLog.SetValue(tupLast[0])
                    self.zMsgLast=time.clock()
                else:
                    if self.zMsgLast>0:
                        if (time.clock()-self.zMsgLast)>5:
                            self.sb.SetStatusText('', self.STATUS_TEXT_POS)
                            #self._txtLog.SetValue('')
                            self.zMsgLast=-1
            
            if self.STATUS_LOG_POS>=0:
                # show log information
                s=vtLog.vtLngGetTrendOrderedStr()
                #self.SetStatusText(s, self.STATUS_LOG_POS)
                self._txtLog.SetValue(s)
        except:
            vtLog.vtLngTB(self.GetName())
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s="VIDARC "+self.appl
        fn=self.netDocMain.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            try:
                if fn.startswith(vcCust.USR_LOCAL_DN):
                    i=len(vcCust.USR_LOCAL_DN)
                    fn=u''.join([u'...',fn[i:]])
            except:
                pass
            s=s+" ("+fn
            if self.netMaster.IsModified():
            #if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        if sOldTitle!=s:
            self.SetTitle(s)
    def SetProcBarVal(self,iVal,iCount=-1,iRange=1000,iNum=0,bForce=False):
        try:
            #self.gProcess.SetRange(iCount())
            def getNormalized(iVal,iCount):
                f=iVal/float(iCount)
                if f>1.0:
                    f=1.0
                if f<0.0:
                    f=0.0
                return long(f*iRange)
            if iCount>0:
                self.iProcCount=iCount
            else:
                iCount=self.iProcCount
            self.gProcess.SetValue(getNormalized(iVal,iCount))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnExit(self,evt):
        self.Close()
        evt.Skip()
    def OnHelp(self,evt):
        evt.Skip()
    def __getPluginImage__(self):
        return None
    def __getPluginBitmap__(self):
        return None
    def OnAbout(self,evt):
        try:
            tup=self.pn.GetAboutData()
            dlg=vAboutDialog.create(self,self.__getPluginBitmap__(),
                    tup[0],tup[1],tup[2])
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
        #evt.Skip()
    def OnLog(self,evt):
        evt.Skip()
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
    def OnErrRep(self,evt):
        evt.Skip()
        try:
            vtLog.PrintMsg(_(u'collecting application status ...'))
            sLogFN=vtLog.vtLngGetFN()
            #sPicFN=self.ScreenShotTop()
            sDT=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
            sFN=''.join([sLogFN,'_',sDT,'.zip'])
            zipFile=zipfile.ZipFile(sFN,'w',zipfile.ZIP_DEFLATED)
            if type(sLogFN)==types.UnicodeType:
                    sLogFN=sLogFN.encode('ISO-8859-1')
            zipFile.write(str(sLogFN))
            for fn in self.lScreeShots:
                if type(fn)==types.UnicodeType:
                        fn=fn.encode('ISO-8859-1')
                zipFile.write(str(fn))
            zipFile.close()
            vtLog.PrintMsg(_(u'application status collected.'))
            dlg = wx.MessageDialog(self, _(u'Please send file\n%s\nto us (mailto:office@vidarc.com).\n\nDo you want to send it now?')%(sFN),
                                    _(u'VIDARC Error Report Notification'),
                                    wx.YES | wx.NO | wx.ICON_INFORMATION
                                    )
            if dlg.ShowModal()==wx.ID_YES:
                webbrowser.open("mailto:office@vidarc.com?subject=error report %s&body=do not forget to attach file:%s manually!!!"%(sDT,sFN))
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnScreenShotTop(self,evt):
        evt.Skip()
        try:
            app=wx.GetApp()
            frm=app.GetTopWindow()
            sPicFN=vtLogScreen.genScreenShot(frm,frm)
            self.lScreeShots.append(sPicFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnScreenShotMDI(self,evt):
        evt.Skip()
        try:
            sPicFN=vtLogScreen.genScreenShot(self,self,self.appl)
            self.lScreeShots.append(sPicFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetCfgData(self,func,l,d):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(d)
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg.update(d)
        try:
            self.pn.SetCfgData(func,l,self.dCfg)
        except:
            pass
        try:
            x=int(d['x'])
            y=int(d['y'])
            #self.GetParent().Move(x,y)
            #wx.CallAfter(self.Move,(x,y))
            #sz=map(int,self.dCfg['size'].split(','))
            #self.GetParent().SetSize(sz)
        except:
            pass
        try:
            self.SetSize((int(d['width']),int(d['height'])))
            self.Maximize(int(d['maximized']))
        except:
            pass#vtLog.vtLngTB(self.GetName())
        
    def GetCfgData(self):
        try:
            self.pn.GetCfgData()
        except:
            pass
        try:
            pos=self.GetPositionTuple()
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            #iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['x']=str(iX)
            self.dCfg['y']=str(iY)
            sz=self.GetSize()
            iWidth=sz[0]
            iHeight=sz[1]
            #iWidth-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            if self.IsMaximized():
                self.dCfg['maximized']='1'
            else:
                self.dCfg['width']=str(iWidth)
                self.dCfg['height']=str(iHeight)
                self.dCfg['maximized']='0'
        except:
            vtLog.vtLngTB(self.GetName())
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dCfg)
    def OnSetNode(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('set response, permission denied %s')%(evt.GetID()))
    def OnDelNode(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('del response, permission denied %s')%(evt.GetID()))
    def OnAddResponse(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('add response, permission denied %s')%(evt.GetID()))
    def OnMoveResponse(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('move response, permission denied %s')%(evt.GetID()))
    def OnAddElementsProgress(self,evt):
        evt.Skip()
        if self.gProcess is not None:
            self.SetProcBarVal(evt.GetValue(),evt.GetCount())
            self.gProcess.SetValue(evt.GetValue())
            #if evt.GetCount()>=0:
            #    self.gProcess.SetRange(evt.GetCount())
    def OnAddElementsFin(self,evt):
        evt.Skip()
        if self.gProcess is not None:
            self.SetProcBarVal(0)
            #self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
    def OnMasterStart(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.__makeTitle__()
        self.PrintMsg(_('master started ...'))
    def OnMasterFinish(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('master finished.'))
    def OnMasterShutDown(self,evt):
        evt.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        #self.Destroy()
        #vtLog.CallStack('')
        #self.Show(False)
        try:
            xdCfg=self.pn.xdCfg
            xdCfg.DelConsumer()
            xdCfg.ShutDown()
            self.pn.xdCfg=None
        except:
            vtLog.vtLngTB(self.GetName())
        import time
        #time.sleep(4)
        self.Close()
    def OnOpenStart(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file opened ...'))
    def OnOpenOk(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file opened and parsed.'))
        #self.__getXmlBaseNodes__()
    def OnOpenFault(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file open fault.'))
        #self.__getXmlBaseNodes__()
    def __getNetDocOk4Processbar__(self,netDoc):
        try:
            netMaster=self.GetNetMaster()
            appl=netDoc.GetAppl()
            appls=netMaster.GetAppls()
            idx=appls.index(appl)
            for a in appls[:idx]:
                tmp=netMaster.GetNetDoc(a)
                if tmp.IsSettled()==False:
                    return False
            return True
        except:
            vtLog.vtLngTB(self.GetName())
        return False
    def OnSynchStart(self,evt):
        evt.Skip()
        netDoc=evt.GetNetDoc()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
        self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
    def OnSynchProc(self,evt):
        evt.Skip()
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.INFO,'',self)
        netDoc=evt.GetNetDoc()
        if self.__getNetDocOk4Processbar__(netDoc):
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
            self.SetProcBarVal(evt.GetAct(),evt.GetCount())
            #self.gProcess.SetRange(evt.GetCount())
            #self.gProcess.SetValue(evt.GetAct())
            self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
    def OnSynchFinished(self,evt):
        evt.Skip()
        netDoc=evt.GetNetDoc()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
        self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch finished.'))
        if self.__getNetDocOk4Processbar__(netDoc):
            self.SetProcBarVal(0)
            self.gProcess.SetValue(0)
            self.__makeTitle__()
        #self.__getXmlBaseNodes__()
    def OnSynchAborted(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('synch aborted.'))
        #self.__getXmlBaseNodes__()
    def OnGotContent(self,evt):
        evt.Skip()
        self.PrintMsg(_('content got. generate ...'))
        #self.setDoc()
    def OnConnect(self,evt):
        evt.Skip()
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
    def OnDisConnect(self,evt):
        evt.Skip()
        self.PrintMsg(_('connection lost.'))
    def OnLoggedin(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('get content ... '))
    def OnChanged(self,evt):
        evt.Skip()
        self.__setModified__(True)
    def OnMnToolsMn_tools_req_lockMenu(self, event):
        event.Skip()
        node=self.trMain.GetSelected()
        self.netDocMain.reqLock(node)
    def OnMnToolsMn_tools_settings(self,evt):
        evt.Skip()
        
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            netMaster=self.pn.GetNetMaster()
            if netMaster is not None:
                if self.autoConn is not None:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'autoconn:%s'%(vtLog.pformat(self.autoConn)),self)
                    wx.CallAfter(netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                    self.autoConn=None
                    #wx.CallAfter(self.netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                    #self.autoConn=None
                    #self.autoConn=None
                    #self.bActivated=True
                else:
                    netMaster.ShowPopup()
                self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        if self.IsShown():
            if self.bActivated==False:
                if 0:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'autoconn:%s'%(vtLog.pformat(self.autoConn)),self)
                    if self.autoConn is not None:
                        netMaster=self.pn.GetNetMaster()
                        wx.CallAfter(netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                        self.autoConn=None
                        #self.netMaster.ShowPopup()
                        pass
                    else:
                        netMaster=self.pn.GetNetMaster()
                        netMaster.ShowPopup()
                self.bActivated=True
            self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
    def OnMainFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            #self.dCfg['x']=str(pos[0])
            #self.dCfg['y']=str(pos[1])
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['x']=str(iX)
            self.dCfg['y']=str(iY)
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(self.dCfg)
                print pos
                print iX,iY
        except:
            pass
    def OnMainFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            self.dCfg['width']=str(sz[0])
            self.dCfg['height']=str(sz[1])
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(self.dCfg)
                print sz
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pn)
            #self.pn.Refresh()
            if self._txtLog is not None:
                rect = self.sbStatus.GetFieldRect(self.STATUS_LOG_POS)
                pos=(rect.x+2, rect.y+2)
                size=(rect.width-4, rect.height-4)
                self._txtLog.Move(pos)
                self._txtLog.SetSize(size)
            if self._txtClk is not None:
                rect = self.sbStatus.GetFieldRect(self.STATUS_CLK_POS)
                pos=(rect.x+2, rect.y+2)
                size=(rect.width-4, rect.height-4)
                self._txtClk.Move(pos)
                self._txtClk.SetSize(size)
        except:
            pass
    def OnMainClose(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'CanVeto:%s'%(event.CanVeto()),self)
        try:
            if self.netMaster is not None:
                if self.netMaster.IsShutDownActive()==False:
                    if event.CanVeto():
                        self.GetCfgData()
                        if self.netMaster.IsModified()==True:
                            dlg = vtmMsgDialog(self,#wx.MessageDialog(widDlgPar,#self,
                                        _(u'Unsaved data present!\n\nDo you want to save data?'),
                                        u'VIDARC ' + self.appl + u' '+ _(u'Close'),
                                        wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                                        )
                            if dlg.ShowModal()==wx.ID_YES:
                                #self.SaveFile()
                                self.netMaster.Save()
                                pass
                        self.thdFrm.Stop()
                        self.thdFrm.SetPostWid(None)
                        self.netMaster.ShutDown()
                        try:
                            xdCfg=self.pn.xdCfg
                            xdCfg.Close()
                            xdCfg.StopConsumer()
                        except:
                            vtLog.vtLngTB(self.GetName())
                        event.Veto()
                        vtLog.vtLngCurWX(vtLog.INFO,'veto',self)
                    else:
                        event.Skip()
                else:
                    if self.netMaster.IsShutDownFinished()==False:
                        if event.CanVeto():
                            event.Veto()
                        else:
                            event.Skip()
                    else:
                        event.Skip()
            else:
                event.Skip()
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        except:
            vtLog.vtLngTB(self.GetName())
            event.Skip()
    def GetDocMain(self):
        try:
            return self.pn.GetDocMain()
        except:
            return None
    def GetNetMaster(self):
        try:
            return self.pn.GetNetMaster()
        except:
            return None
    def GetTreeMain(self):
        try:
            return self.pn.GetTreeMain()
        except:
            return None
    def BindEvents(self,lOrigEventFunc):
        import vidarc.tool.net.vNetXmlWxGuiEvents as vtNetEvents
        netMaster=self.GetNetMaster()
        if netMaster is None:
            return
        for origin,lEventFunc in lOrigEventFunc:
            try:
                if origin is None:
                    if hasattr(self.pn,'BindEvents'):
                        self.pn.BindEvents(t)
                else:
                    netDoc=netMaster.GetNetDoc(origin)
                    for evt,func in lEventFunc:
                        if hasattr(vtNetEvents,evt):
                            vtNetEvents.__dict__[evt](netDoc,func)
            except:
                vtLog.vtLngTB(self.GetName())
    def __bindNetDocEvents__(self,appl,lEvents=None):
        try:
            import vidarc.tool.net.vNetXmlWxGuiEvents as vtNetEvents
            netMaster=self.GetNetMaster()
            if netMaster is None:
                return
            netDoc=netMaster.GetNetDoc(appl)
            if netDoc is None:
                return
            if lEvents is None:
                lEvents=['synch']
            for evt in lEvents:
                if evt=='synch':
                    vtNetEvents.EVT_NET_XML_SYNCH_START(netDoc,self.OnSynchStart)
                    vtNetEvents.EVT_NET_XML_SYNCH_PROC(netDoc,self.OnSynchProc)
                    vtNetEvents.EVT_NET_XML_SYNCH_FINISHED(netDoc,self.OnSynchFinished)
                    vtNetEvents.EVT_NET_XML_SYNCH_ABORTED(netDoc,self.OnSynchAborted)
        except:
            vtLog.vtLngTB(self.GetName())
    def BindNetDocEvents(self,appl=None,lEvents=None):
        try:
            netMaster=self.GetNetMaster()
            if netMaster is None:
                return
            if appl is None:
                appls=netMaster.GetAppls()
                for appl in appls:
                    self.__bindNetDocEvents__(appl)
            else:
                self.__bindNetDocEvents__(appl)
        except:
            vtLog.vtLngTB(self.GetName())
    def DoAutoConnect(self,name,bOffline=False):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'name:%s;offline:%s'%(name,bOffline),self)
            if self.bActivated:
                #netMaster=self.pn.GetNetMaster()
                #self.netMaster.DoAutoConnect(name,bOffline)
                #wx.CallAfter(netMaster.DoAutoConnect,name,bOffline)
                self.autoConn=[name,bOffline]
            else:
                self.autoConn=[name,bOffline]
                #netMaster=self.pn.GetNetMaster()
                #wx.CallAfter(netMaster.DoAutoConnect,name,bOffline)
            #netMaster=self.pn.GetNetMaster()
            #self.bActivated=True
            #netMaster.DoAutoConnect(name,bOffline)
            vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSbMainRightUp(self, event):
        event.Skip()
        try:
            if self.rbMsg is not None:
                if self.ShowPopupStatusBar():
                    self.AddMsg2PopupStatusBar((None,None))
                    self.rbMsg.proc(self.AddMsg2PopupStatusBar)
        except:
            vtLog.vtLngTB(self.GetName())

    def __get_refcounts(self):
        d = {}
        # collect all classes
        for m in sys.modules.values():
            for sym in dir(m):
                o = getattr (m, sym)
                if type(o) is types.ClassType:
                    d[o] = sys.getrefcount (o)
        # sort by refcount
        pairs = map (lambda x: (x[1],x[0]), d.items())
        pairs.sort()
        pairs.reverse()
        return pairs
    
    def __print_top_100(self):
        for n, c in self.__get_refcounts()[:100]:
            print '%10d %s' % (n, c.__name__)
    #def Do(self,func,*args,**kwargs):
    #    self.thdFrm.Do(func,*args,**kwargs)
    #def CallBack(self,func,*args,**kwargs):
    #    self.thdFrm.CallBack(func,*args,**kwargs)
    #def CallBackBranch(self,funcCB,branchOkCB,branchFltCB):
    #    self.thdFrm.CallBackBranch(funcCB,branchOkCB,branchFltCB)
    #def CallBackDelayed(self,zSleep,func,*args,**kwargs):
    #    self.thdFrm.CallBackDelayed(zSleep,func,*args,**kwargs)
    #def DoCallBackBranch(self,funcCB,branchOkCB,branchFltCB):
    #    self.thdFrm.DoCallBackBranch(funcCB,branchOkCB,branchFltCB)
    #def DoCallBackBranchWX(self,funcCB,branchOkCB,branchFltCB):
    #    self.thdFrm.DoCallBackBranchWX(funcCB,branchOkCB,branchFltCB)
    #def DoCallBackMultiBranchWX(self,funcCB,dBranchCB):
    #    self.thdFrm.DoCallBackMultiBranchWX(funcCB,dBranchCB)
    #def DoCallBackTimeOutWX(self,zSleep,zTimeOut,funcCmp,funcOkCB,funcFltCB):
    #    self.thdFrm.DoCallBackTimeOutWX(zSleep,zTimeOut,funcCmp,funcOkCB,funcFltCB)

