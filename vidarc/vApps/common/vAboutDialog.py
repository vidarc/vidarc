#Boa:Dialog:vAboutDialog
#----------------------------------------------------------------------------
# Name:         vAboutDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20051210
# CVS-ID:       $Id: vAboutDialog.py,v 1.6 2008/02/05 17:08:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import __config__
#import images
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vApps.common.vSplashFrame import getLogoBitmap
import cStringIO

#----------------------------------------------------------------------
def getApplData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x96IDAT(\x91c\\~n\x07\x03)\x80\x05S\xa8}\xcf|8\xbb\xd2%\x11M\x96\tS\x03\
\\\x11\xa6j\x06\x06\x06F\xb8\x93\x90\r\xc6jJ\xfb\x9e\xf9\x95.\x89Xl\xc0\n\
\xe0\xc6\xa1k8_\xb2\xf2|\xc9J\\\\\xa8\x06d\xc7\x18\xf6\x84\xa3\x19\x01\x11\
\x81\xfb\x87\t\xd3\x03\x86=\xe1\x10S\xcf\x97\xacD\xd6\x0f\xd1C\xac\x1fP\x9c\
\xc4\x80\x11\x82\x10K\xd0\x9c\x07q\x05\x13\xa6j\xac\x00g(![\x82\xcfI\x04\x01\
\xdc\x15\x8cX\x13\x1f\xc4\x01X\x9d\x8a\xc5\x06\xb8s\xb1&\x16\xec6\xe0\x01\
\x00\x98\xff>\xd3\x83on\xdb\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplBitmap():
    return wx.BitmapFromImage(getApplImage())

def getApplImage():
    stream = cStringIO.StringIO(getApplData())
    return wx.ImageFromStream(stream)

def create(parent,bmp,appl=None,desc=None,version=None,bLog=True):
    return vAboutDialog(parent,bmp,appl=appl,desc=desc,version=version,bLog=bLog)

[wxID_VABOUTDIALOG, wxID_VABOUTDIALOGCBOK, wxID_VABOUTDIALOGCHCLIC, 
 wxID_VABOUTDIALOGLBBLOGO, wxID_VABOUTDIALOGLBICON, 
 wxID_VABOUTDIALOGLBLAPPLNAME, wxID_VABOUTDIALOGLBLDESC, 
 wxID_VABOUTDIALOGLBLFN, wxID_VABOUTDIALOGLBLLOGFN, wxID_VABOUTDIALOGLBLPORT, 
 wxID_VABOUTDIALOGLBLSOCKET, wxID_VABOUTDIALOGLBLVERSION, 
 wxID_VABOUTDIALOGNBABOUT, wxID_VABOUTDIALOGPNAPPL, wxID_VABOUTDIALOGPNLIC, 
 wxID_VABOUTDIALOGTXTLIC, 
] = [wx.NewId() for _init_ctrls in range(16)]

class vAboutDialog(wx.Dialog):
    def _init_coll_fgsAbout_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(1)

    def _init_coll_fgsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbAbout, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbOk, 0, border=4, flag=wx.ALL | wx.ALIGN_CENTER)

    def _init_coll_fgsAbout_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lbbLogo, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lbIcon, 0, border=8,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddWindow(self.lblApplName, 1, border=8,
              flag=wx.EXPAND | wx.TOP | wx.LEFT | wx.RIGHT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.TOP)
        parent.AddWindow(self.lblVersion, 1, border=8,
              flag=wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDesc, 1, border=8,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsLog, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsSocket, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)

    def _init_coll_fgsGen_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsLic_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsLic_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcLic, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddWindow(self.txtLic, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsSocket_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSocket, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblPort, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogFN, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFN, 2, border=0, flag=wx.EXPAND)

    def _init_coll_nbAbout_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnAppl, select=True,
              text=u'application')
        parent.AddPage(imageId=-1, page=self.pnLic, select=False,
              text=u'licences')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsAbout = wx.FlexGridSizer(cols=2, hgap=0, rows=6, vgap=0)

        self.bxsLog = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSocket = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsGen = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.fgsLic = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self._init_coll_fgsAbout_Items(self.fgsAbout)
        self._init_coll_fgsAbout_Growables(self.fgsAbout)
        self._init_coll_bxsLog_Items(self.bxsLog)
        self._init_coll_bxsSocket_Items(self.bxsSocket)
        self._init_coll_fgsGen_Items(self.fgsGen)
        self._init_coll_fgsGen_Growables(self.fgsGen)
        self._init_coll_fgsLic_Growables(self.fgsLic)
        self._init_coll_fgsLic_Items(self.fgsLic)

        self.SetSizer(self.fgsGen)
        self.pnAppl.SetSizer(self.fgsAbout)
        self.pnLic.SetSizer(self.fgsLic)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VABOUTDIALOG, name=u'vAboutDialog',
              parent=prnt, pos=wx.Point(309, 122), size=wx.Size(422, 411),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'About Dialog')
        self.SetClientSize(wx.Size(414, 384))
        self.Bind(wx.EVT_SIZE, self.OnVAboutDialogSize)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VABOUTDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Ok'), name=u'cbOk',
              parent=self, pos=wx.Point(151, 350), size=wx.Size(112, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VABOUTDIALOGCBOK)

        self.nbAbout = wx.Notebook(id=wxID_VABOUTDIALOGNBABOUT, name=u'nbAbout',
              parent=self, pos=wx.Point(0, 4), size=wx.Size(414, 342),
              style=wx.NO_3D | wx.NO_BORDER)
        self.nbAbout.SetMinSize(wx.Size(-1, -1))

        self.pnAppl = wx.Panel(id=wxID_VABOUTDIALOGPNAPPL, name=u'pnAppl',
              parent=self.nbAbout, pos=wx.Point(0, 0), size=wx.Size(406, 316),
              style=wx.TAB_TRAVERSAL)

        self.pnLic = wx.Panel(id=wxID_VABOUTDIALOGPNLIC, name=u'pnLic',
              parent=self.nbAbout, pos=wx.Point(0, 0), size=wx.Size(406, 316),
              style=wx.TAB_TRAVERSAL)

        self.lbIcon = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VABOUTDIALOGLBICON, name=u'lbIcon', parent=self.pnAppl,
              pos=wx.Point(8, 57), size=wx.Size(32, 32), style=0)

        self.lblApplName = wx.StaticText(id=wxID_VABOUTDIALOGLBLAPPLNAME,
              label=u'Application Name', name=u'lblApplName',
              parent=self.pnAppl, pos=wx.Point(56, 57), size=wx.Size(342, 32),
              style=0)
        self.lblApplName.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD,
              False, u'Tahoma'))
        self.lblApplName.SetMinSize(wx.Size(-1, -1))

        self.lblVersion = wx.StaticText(id=wxID_VABOUTDIALOGLBLVERSION,
              label=u'Version', name=u'lblVersion', parent=self.pnAppl,
              pos=wx.Point(56, 97), size=wx.Size(350, 23), style=0)
        self.lblVersion.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))
        self.lblVersion.SetMinSize(wx.Size(-1, -1))

        self.lblDesc = wx.StaticText(id=wxID_VABOUTDIALOGLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self.pnAppl,
              pos=wx.Point(56, 128), size=wx.Size(350, 150), style=0)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.lbbLogo = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VABOUTDIALOGLBBLOGO, name=u'lbbLogo', parent=self.pnAppl,
              pos=wx.Point(52, 0), size=wx.Size(350, 49), style=0)

        self.lblLogFN = wx.StaticText(id=wxID_VABOUTDIALOGLBLLOGFN,
              label=_(u'log filename')+':', name=u'lblLogFN',
              parent=self.pnAppl, pos=wx.Point(48, 282), size=wx.Size(115, 13),
              style=wx.ALIGN_RIGHT)
        self.lblLogFN.SetMinSize(wx.Size(-1, -1))

        self.lblFN = wx.StaticText(id=wxID_VABOUTDIALOGLBLFN, label=u'',
              name=u'lblFN', parent=self.pnAppl, pos=wx.Point(167, 282),
              size=wx.Size(238, 13), style=0)
        self.lblFN.SetMinSize(wx.Size(-1, -1))

        self.lblSocket = wx.StaticText(id=wxID_VABOUTDIALOGLBLSOCKET,
              label=_(u'port')+':', name=u'lblSocket', parent=self.pnAppl,
              pos=wx.Point(48, 299), size=wx.Size(115, 13),
              style=wx.ALIGN_RIGHT)
        self.lblSocket.SetMinSize(wx.Size(-1, -1))

        self.lblPort = wx.StaticText(id=wxID_VABOUTDIALOGLBLPORT, label=u'',
              name=u'lblPort', parent=self.pnAppl, pos=wx.Point(167, 299),
              size=wx.Size(238, 13), style=0)
        self.lblPort.SetMinSize(wx.Size(-1, -1))

        self.chcLic = wx.Choice(choices=[], id=wxID_VABOUTDIALOGCHCLIC,
              name=u'chcLic', parent=self.pnLic, pos=wx.Point(0, 0),
              size=wx.Size(406, 21), style=0)
        self.chcLic.SetMinSize(wx.Size(-1, -1))
        self.chcLic.Bind(wx.EVT_CHOICE, self.OnChcLicChoice,
              id=wxID_VABOUTDIALOGCHCLIC)

        self.txtLic = wx.TextCtrl(id=wxID_VABOUTDIALOGTXTLIC, name=u'txtLic',
              parent=self.pnLic, pos=wx.Point(0, 29), size=wx.Size(406, 287),
              style=wx.TE_READONLY | wx.TE_MULTILINE, value=u'')
        self.txtLic.SetMinSize(wx.Size(-1, -1))

        self._init_coll_nbAbout_Pages(self.nbAbout)

        self._init_sizers()

    def __init__(self, parent ,bmp,appl=None,desc=None,version=None,bLog=True):
        global _
        _=vtLgBase.assignPluginLang('common')
        self._init_ctrls(parent)
        self.lbbLogo.SetBitmap(getLogoBitmap())
        if appl is None:
            appl=__config__.DESCRIPTION
        self.lblApplName.SetLabel(appl)
        if version is None:
            version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        self.lblVersion.SetLabel(version)
        if desc is None:
            desc=__config__.LONG_DESCRIPTION
        self.lblDesc.SetLabel(desc)
        if bmp is not None:
            self.lbIcon.SetBitmap(bmp)
        else:
            self.lbIcon.SetBitmap(getApplBitmap())
        self.cbOk.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        if bLog:
            self.lblFN.SetLabel(vtLog.vtLngGetFN())
            self.lblPort.SetLabel(vtLog.vtLngGetPort())
        else:
            self.lblFN.SetLabel('---')
            self.lblPort.SetLabel('---')
        self.__defLic()
        keys=self.dLic.keys()
        keys.sort()
        for k in keys:
            self.chcLic.Append(k)
        try:
            self.chcLic.SetSelection(0)
            wx.CallAfter(self.OnChcLicChoice,None)
        except:
            pass
        #self.fgsAbout.Layout()
        #self.fgsAbout.Fit(self)
        self.fgsGen.Layout()
        self.fgsGen.Fit(self)
    def OnChcLicChoice(self,event):
        if event is not None:
            event.Skip()
        sKey=self.chcLic.GetStringSelection()
        if sKey in self.dLic:
            s=self.dLic[sKey]
        else:
            s=''
        self.txtLic.SetValue(s)
    def __defLic(self):
        self.dLic={
            'ElementTree':"""# --------------------------------------------------------------------
# The ElementTree toolkit is
#
# Copyright (c) 1999-2004 by Fredrik Lundh
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# Secret Labs AB or the author not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.
#
# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
# TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANT-
# ABILITY AND FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR
# BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
# DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
# ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
# OF THIS SOFTWARE.
# --------------------------------------------------------------------
""",
            'libxml2':"""MIT - licence
The MIT License

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.""",
            'wxWindows':"""
                wxWindows Library Licence, Version 3
                ====================================

  Copyright (c) 1998 Julian Smart, Robert Roebling et al

  Everyone is permitted to copy and distribute verbatim copies
  of this licence document, but changing it is not allowed.

                       WXWINDOWS LIBRARY LICENCE
     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
  
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public Licence as published by
  the Free Software Foundation; either version 2 of the Licence, or (at
  your option) any later version.
  
  This library is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
  General Public Licence for more details.

  You should have received a copy of the GNU Library General Public Licence
  along with this software, usually in a file named COPYING.LIB.  If not,
  write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA 02111-1307 USA.

  EXCEPTION NOTICE

  1. As a special exception, the copyright holders of this library give
  permission for additional uses of the text contained in this release of
  the library as licenced under the wxWindows Library Licence, applying
  either version 3 of the Licence, or (at your option) any later version of
  the Licence as published by the copyright holders of version 3 of the
  Licence document.

  2. The exception is that you may use, copy, link, modify and distribute
  under the user's own terms, binary object code versions of works based
  on the Library.

  3. If you copy code from files distributed under the terms of the GNU
  General Public Licence or the GNU Library General Public Licence into a
  copy of this library, as this licence permits, the exception does not
  apply to the code that you add in this way.  To avoid misleading anyone as
  to the status of such modified files, you must delete this exception
  notice from such code and/or adjust the licensing conditions notice
  accordingly.

  4. If you write modifications of your own for this library, it is your
  choice whether to permit this exception to apply to your modifications. 
  If you do not wish that, you must delete the exception notice from such
  code and/or adjust the licensing conditions notice accordingly.
""",
            'wxWindows preample':"""
Preamble
========

The licensing of the wxWidgets library is intended to protect the wxWidgets
library, its developers, and its users, so that the considerable investment
it represents is not abused.

Under the terms of the wxWidgets Licence, you as a user are not
obliged to distribute wxWidgets source code with your products, if you
distribute these products in binary form. However, you are prevented from
restricting use of the library in source code form, or denying others the
rights to use or distribute wxWidgets library source code in the way
intended.

The wxWidgets Licence establishes the copyright for the code and related
material, and it gives you legal permission to copy, distribute and/or
modify the library. It also asserts that no warranty is given by the authors
for this or derived code.

The core distribution of the wxWidgets library contains files
under two different licences:

- Most files are distributed under the GNU Library General Public
  Licence, version 2, with the special exception that you may create and
  distribute object code versions built from the source code or modified
  versions of it (even if these modified versions include code under a
  different licence), and distribute such binaries under your own
  terms.

- Most core wxWidgets manuals are made available under the "wxWidgets
  Free Documentation Licence", which allows you to distribute modified
  versions of the manuals, such as versions documenting any modifications
  made by you in your version of the library. However, you may not restrict
  any third party from reincorporating your changes into the original
  manuals.

Other relevant files:

- licence.txt: a statement that the wxWidgets library is
  covered by the GNU Library General Public Licence, with an
  exception notice for binary distribution.

- licendoc.txt: the wxWidgets Documentation Licence.

- lgpl.txt: the text of the GNU Library General Public Licence.

- gpl.txt: the text of the GNU General Public Licence, which is
  referenced by the LGPL.

""",
            'wxWindows LGPL':"""

	  GNU LIBRARY GENERAL PUBLIC LICENSE
	  ==================================
                Version 2, June 1991

 Copyright (C) 1991 Free Software Foundation, Inc.
                    675 Mass Ave, Cambridge, MA 02139, USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

[This is the first released version of the library GPL.  It is
 numbered 2 because it goes with version 2 of the ordinary GPL.]

                        Preamble

The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General
Public Licenses are intended to guarantee your freedom to share
and change free software--to make sure the software is free for
all its users.

This license, the Library General Public License, applies to
some specially designated Free Software Foundation software, and
to any other libraries whose authors decide to use it.  You can
use it for your libraries, too.

When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure
that you have the freedom to distribute copies of free software
(and charge for this service if you wish), that you receive
source code or can get it if you want it, that you can change
the software or use pieces of it in new free programs; and that
you know you can do these things.

To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the
rights. These restrictions translate to certain responsibilities
for you if you distribute copies of the library, or if you
modify it.

For example, if you distribute copies of the library, whether
gratis or for a fee, you must give the recipients all the rights
that we gave you.  You must make sure that they, too, receive or
can get the source code.  If you link a program with the
library, you must provide complete object files to the
recipients so that they can relink them with the library, after
making changes to the library and recompiling it.  And you must
show them these terms so they know their rights.

Our method of protecting your rights has two steps: (1)
copyright the library, and (2) offer you this license which
gives you legal permission to copy, distribute and/or modify the
library.

Also, for each distributor's protection, we want to make certain
that everyone understands that there is no warranty for this
free library.  If the library is modified by someone else and
passed on, we want its recipients to know that what they have is
not the original version, so that any problems introduced by
others will not reflect on the original authors' reputations.
 
Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that companies
distributing free software will individually obtain patent
licenses, thus in effect transforming the program into
proprietary software.  To prevent this, we have made it clear
that any patent must be licensed for everyone's free use or not
licensed at all.

Most GNU software, including some libraries, is covered by the
ordinary GNU General Public License, which was designed for
utility programs.  This license, the GNU Library General Public
License, applies to certain designated libraries.  This license
is quite different from the ordinary one; be sure to read it in
full, and don't assume that anything in it is the same as in the
ordinary license.

The reason we have a separate public license for some libraries
is that they blur the distinction we usually make between
modifying or adding to a program and simply using it.  Linking a
program with a library, without changing the library, is in some
sense simply using the library, and is analogous to running a
utility program or application program.  However, in a textual
and legal sense, the linked executable is a combined work, a
derivative of the original library, and the ordinary General
Public License treats it as such.

Because of this blurred distinction, using the ordinary General
Public License for libraries did not effectively promote
software sharing, because most developers did not use the
libraries.  We concluded that weaker conditions might promote
sharing better.

However, unrestricted linking of non-free programs would deprive
the users of those programs of all benefit from the free status
of the libraries themselves.  This Library General Public
License is intended to permit developers of non-free programs to
use free libraries, while preserving your freedom as a user of
such programs to change the free libraries that are incorporated
in them.  (We have not seen how to achieve this as regards
changes in header files, but we have achieved it as regards
changes in the actual functions of the Library.)  The hope is
that this will lead to faster development of free libraries.

The precise terms and conditions for copying, distribution and
modification follow.  Pay close attention to the difference
between a "work based on the library" and a "work that uses the
library".  The former contains code derived from the library,
while the latter only works together with the library.

Note that it is possible for a library to be covered by the
ordinary General Public License rather than by this special one.

                GNU LIBRARY GENERAL PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. This License Agreement applies to any software library which
contains a notice placed by the copyright holder or other
authorized party saying it may be distributed under the terms of
this Library General Public License (also called "this
License").  Each licensee is addressed as "you".

A "library" means a collection of software functions and/or data
prepared so as to be conveniently linked with application
programs (which use some of those functions and data) to form
executables.

The "Library", below, refers to any such software library or
work which has been distributed under these terms.  A "work
based on the Library" means either the Library or any derivative
work under copyright law: that is to say, a work containing the
Library or a portion of it, either verbatim or with
modifications and/or translated straightforwardly into another
language.  (Hereinafter, translation is included without
limitation in the term "modification".)

"Source code" for a work means the preferred form of the work
for making modifications to it.  For a library, complete source
code means all the source code for all modules it contains, plus
any associated interface definition files, plus the scripts used
to control compilation and installation of the library.

Activities other than copying, distribution and modification are
not covered by this License; they are outside its scope.  The
act of running a program using the Library is not restricted,
and output from such a program is covered only if its contents
constitute a work based on the Library (independent of the use
of the Library in a tool for writing it).  Whether that is true
depends on what the Library does and what the program that uses
the Library does.
  
1. You may copy and distribute verbatim copies of the Library's
complete source code as you receive it, in any medium, provided
that you conspicuously and appropriately publish on each copy an
appropriate copyright notice and disclaimer of warranty; keep
intact all the notices that refer to this License and to the
absence of any warranty; and distribute a copy of this License
along with the Library.

You may charge a fee for the physical act of transferring a
copy, and you may at your option offer warranty protection in
exchange for a fee.
 
2. You may modify your copy or copies of the Library or any
portion of it, thus forming a work based on the Library, and
copy and distribute such modifications or work under the terms
of Section 1 above, provided that you also meet all of these
conditions:

    a) The modified work must itself be a software library.

    b) You must cause the files modified to carry prominent notices
    stating that you changed the files and the date of any change.

    c) You must cause the whole of the work to be licensed at no
    charge to all third parties under the terms of this License.

    d) If a facility in the modified Library refers to a function or a
    table of data to be supplied by an application program that uses
    the facility, other than as an argument passed when the facility
    is invoked, then you must make a good faith effort to ensure that,
    in the event an application does not supply such function or
    table, the facility still operates, and performs whatever part of
    its purpose remains meaningful.

    (For example, a function in a library to compute square roots has
    a purpose that is entirely well-defined independent of the
    application.  Therefore, Subsection 2d requires that any
    application-supplied function or table used by this function must
    be optional: if the application does not supply it, the square
    root function must still compute square roots.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the
Library, and can be reasonably considered independent and
separate works in themselves, then this License, and its terms,
do not apply to those sections when you distribute them as
separate works.  But when you distribute the same sections as
part of a whole which is a work based on the Library, the
distribution of the whole must be on the terms of this License,
whose permissions for other licensees extend to the entire
whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or
contest your rights to work written entirely by you; rather, the
intent is to exercise the right to control the distribution of
derivative or collective works based on the Library.

In addition, mere aggregation of another work not based on the
Library with the Library (or with a work based on the Library)
on a volume of a storage or distribution medium does not bring
the other work under the scope of this License.

3. You may opt to apply the terms of the ordinary GNU General
Public License instead of this License to a given copy of the
Library.  To do this, you must alter all the notices that refer
to this License, so that they refer to the ordinary GNU General
Public License, version 2, instead of to this License.  (If a
newer version than version 2 of the ordinary GNU General Public
License has appeared, then you can specify that version instead
if you wish.)  Do not make any other change in these notices.
 
Once this change is made in a given copy, it is irreversible for
that copy, so the ordinary GNU General Public License applies to
all subsequent copies and derivative works made from that copy.

This option is useful when you wish to copy part of the code of
the Library into a program that is not a library.

4. You may copy and distribute the Library (or a portion or
derivative of it, under Section 2) in object code or executable
form under the terms of Sections 1 and 2 above provided that you
accompany it with the complete corresponding machine-readable
source code, which must be distributed under the terms of
Sections 1 and 2 above on a medium customarily used for software
interchange.

If distribution of object code is made by offering access to
copy from a designated place, then offering equivalent access to
copy the source code from the same place satisfies the
requirement to distribute the source code, even though third
parties are not compelled to copy the source along with the
object code.

5. A program that contains no derivative of any portion of the
Library, but is designed to work with the Library by being
compiled or linked with it, is called a "work that uses the
Library".  Such a work, in isolation, is not a derivative work
of the Library, and therefore falls outside the scope of this
License.

However, linking a "work that uses the Library" with the Library
creates an executable that is a derivative of the Library
(because it contains portions of the Library), rather than a
"work that uses the library".  The executable is therefore
covered by this License. Section 6 states terms for distribution
of such executables.

When a "work that uses the Library" uses material from a header
file that is part of the Library, the object code for the work
may be a derivative work of the Library even though the source
code is not. Whether this is true is especially significant if
the work can be linked without the Library, or if the work is
itself a library.  The threshold for this to be true is not
precisely defined by law.

If such an object file uses only numerical parameters, data
structure layouts and accessors, and small macros and small
inline functions (ten lines or less in length), then the use of
the object file is unrestricted, regardless of whether it is
legally a derivative work.  (Executables containing this object
code plus portions of the Library will still fall under Section
6.)

Otherwise, if the work is a derivative of the Library, you may
distribute the object code for the work under the terms of
Section 6. Any executables containing that work also fall under
Section 6, whether or not they are linked directly with the
Library itself.
 
6. As an exception to the Sections above, you may also compile
or link a "work that uses the Library" with the Library to
produce a work containing portions of the Library, and
distribute that work under terms of your choice, provided that
the terms permit modification of the work for the customer's own
use and reverse engineering for debugging such modifications.

You must give prominent notice with each copy of the work that
the Library is used in it and that the Library and its use are
covered by this License.  You must supply a copy of this
License.  If the work during execution displays copyright
notices, you must include the copyright notice for the Library
among them, as well as a reference directing the user to the
copy of this License.  Also, you must do one of these things:

    a) Accompany the work with the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work (which must be distributed under
    Sections 1 and 2 above); and, if the work is an executable linked
    with the Library, with the complete machine-readable "work that
    uses the Library", as object code and/or source code, so that the
    user can modify the Library and then relink to produce a modified
    executable containing the modified Library.  (It is understood
    that the user who changes the contents of definitions files in the
    Library will not necessarily be able to recompile the application
    to use the modified definitions.)

    b) Accompany the work with a written offer, valid for at
    least three years, to give the same user the materials
    specified in Subsection 6a, above, for a charge no more
    than the cost of performing this distribution.

    c) If distribution of the work is made by offering access to copy
    from a designated place, offer equivalent access to copy the above
    specified materials from the same place.

    d) Verify that the user has already received a copy of these
    materials or that you have already sent this user a copy.

For an executable, the required form of the "work that uses the
Library" must include any data and utility programs needed for
reproducing the executable from it.  However, as a special
exception, the source code distributed need not include anything
that is normally distributed (in either source or binary form)
with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that
component itself accompanies the executable.

It may happen that this requirement contradicts the license
restrictions of other proprietary libraries that do not normally
accompany the operating system.  Such a contradiction means you
cannot use both them and the Library together in an executable
that you distribute.
 
7. You may place library facilities that are a work based on the
Library side-by-side in a single library together with other
library facilities not covered by this License, and distribute
such a combined library, provided that the separate distribution
of the work based on the Library and of the other library
facilities is otherwise permitted, and provided that you do
these two things:

    a) Accompany the combined library with a copy of the same work
    based on the Library, uncombined with any other library
    facilities.  This must be distributed under the terms of the
    Sections above.

    b) Give prominent notice with the combined library of the fact
    that part of it is a work based on the Library, and explaining
    where to find the accompanying uncombined form of the same work.

8. You may not copy, modify, sublicense, link with, or
distribute the Library except as expressly provided under this
License.  Any attempt otherwise to copy, modify, sublicense,
link with, or distribute the Library is void, and will
automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you
under this License will not have their licenses terminated so
long as such parties remain in full compliance.

9. You are not required to accept this License, since you have
not signed it.  However, nothing else grants you permission to
modify or distribute the Library or its derivative works.  These
actions are prohibited by law if you do not accept this
License.  Therefore, by modifying or distributing the Library
(or any work based on the Library), you indicate your acceptance
of this License to do so, and all its terms and conditions for
copying, distributing or modifying the Library or works based on
it.

10. Each time you redistribute the Library (or any work based on
the Library), the recipient automatically receives a license
from the original licensor to copy, distribute, link with or
modify the Library subject to these terms and conditions.  You
may not impose any further restrictions on the recipients'
exercise of the rights granted herein. You are not responsible
for enforcing compliance by third parties to this License.
 
11. If, as a consequence of a court judgment or allegation of
patent infringement or for any other reason (not limited to
patent issues), conditions are imposed on you (whether by court
order, agreement or otherwise) that contradict the conditions of
this License, they do not excuse you from the conditions of this
License.  If you cannot distribute so as to satisfy
simultaneously your obligations under this License and any other
pertinent obligations, then as a consequence you may not
distribute the Library at all.  For example, if a patent license
would not permit royalty-free redistribution of the Library by
all those who receive copies directly or indirectly through you,
then the only way you could satisfy both it and this License
would be to refrain entirely from distribution of the Library.

If any portion of this section is held invalid or unenforceable
under any particular circumstance, the balance of the section is
intended to apply, and the section as a whole is intended to
apply in other circumstances.

It is not the purpose of this section to induce you to infringe
any patents or other property right claims or to contest
validity of any such claims; this section has the sole purpose
of protecting the integrity of the free software distribution
system which is implemented by public license practices.  Many
people have made generous contributions to the wide range of
software distributed through that system in reliance on
consistent application of that system; it is up to the
author/donor to decide if he or she is willing to distribute
software through any other system and a licensee cannot impose
that choice.

This section is intended to make thoroughly clear what is
believed to be a consequence of the rest of this License.

12. If the distribution and/or use of the Library is restricted
in certain countries either by patents or by copyrighted
interfaces, the original copyright holder who places the Library
under this License may add an explicit geographical distribution
limitation excluding those countries, so that distribution is
permitted only in or among countries not thus excluded.  In such
case, this License incorporates the limitation as if written in
the body of this License.

13. The Free Software Foundation may publish revised and/or new
versions of the Library General Public License from time to
time. Such new versions will be similar in spirit to the present
version, but may differ in detail to address new problems or
concerns.

Each version is given a distinguishing version number.  If the
Library specifies a version number of this License which applies
to it and "any later version", you have the option of following
the terms and conditions either of that version or of any later
version published by the Free Software Foundation.  If the
Library does not specify a license version number, you may
choose any version ever published by the Free Software
Foundation.

14. If you wish to incorporate parts of the Library into other
free programs whose distribution conditions are incompatible
with these, write to the author to ask for permission.  For
software which is copyrighted by the Free Software Foundation,
write to the Free Software Foundation; we sometimes make
exceptions for this.  Our decision will be guided by the two
goals of preserving the free status of all derivatives of our
free software and of promoting the sharing and reuse of software
generally.

                           NO WARRANTY

  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL
DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

                    END OF TERMS AND CONDITIONS

 Appendix: How to Apply These Terms to Your New Libraries

If you develop a new library, and you want it to be of the
greatest possible use to the public, we recommend making it free
software that everyone can redistribute and change.  You can do
so by permitting redistribution under these terms (or,
alternatively, under the terms of the ordinary General Public
License).

To apply these terms, attach the following notices to the
library.  It is safest to attach them to the start of each
source file to most effectively convey the exclusion of
warranty; and each file should have at least the "copyright"
line and a pointer to where the full notice is found.

    <one line to give the library's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Also add information on how to contact you by electronic and paper mail.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the library, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the
  library `Frob' (a library for tweaking knobs) written by James Random Hacker.

  <signature of Ty Coon>, 1 April 1990
  Ty Coon, President of Vice

That's all there is to it!

""",
            'pyCrypto':"""
===================================================================

Distribute and use freely; there are no restrictions on further
dissemination and usage except those imposed by the laws of your
country of residence.  This software is provided "as is" without
warranty of fitness for use or suitability for any purpose, express
or implied. Use at your own risk or not at all.
===================================================================

Incorporating the code into commercial products is permitted; you do
not have to make source available or contribute your changes back
(though that would be nice).

--amk                                                             (www.amk.ca)


""",
            'pyOpenSSL':"""
                  GNU LESSER GENERAL PUBLIC LICENSE
                       Version 2.1, February 1999

 Copyright (C) 1991, 1999 Free Software Foundation, Inc.
     59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

[This is the first released version of the Lesser GPL.  It also counts
 as the successor of the GNU Library Public License, version 2, hence
 the version number 2.1.]

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
Licenses are intended to guarantee your freedom to share and change
free software--to make sure the software is free for all its users.

  This license, the Lesser General Public License, applies to some
specially designated software packages--typically libraries--of the
Free Software Foundation and other authors who decide to use it.  You
can use it too, but we suggest you first think carefully about whether
this license or the ordinary General Public License is the better
strategy to use in any particular case, based on the explanations
below.

  When we speak of free software, we are referring to freedom of use,
not price.  Our General Public Licenses are designed to make sure that
you have the freedom to distribute copies of free software (and charge
for this service if you wish); that you receive source code or can get
it if you want it; that you can change the software and use pieces of
it in new free programs; and that you are informed that you can do
these things.

  To protect your rights, we need to make restrictions that forbid
distributors to deny you these rights or to ask you to surrender these
rights.  These restrictions translate to certain responsibilities for
you if you distribute copies of the library or if you modify it.

  For example, if you distribute copies of the library, whether gratis
or for a fee, you must give the recipients all the rights that we gave
you.  You must make sure that they, too, receive or can get the source
code.  If you link other code with the library, you must provide
complete object files to the recipients, so that they can relink them
with the library after making changes to the library and recompiling
it.  And you must show them these terms so they know their rights.

  We protect your rights with a two-step method: (1) we copyright the
library, and (2) we offer you this license, which gives you legal
permission to copy, distribute and/or modify the library.

  To protect each distributor, we want to make it very clear that
there is no warranty for the free library.  Also, if the library is
modified by someone else and passed on, the recipients should know
that what they have is not the original version, so that the original
author's reputation will not be affected by problems that might be
introduced by others.
^L
  Finally, software patents pose a constant threat to the existence of
any free program.  We wish to make sure that a company cannot
effectively restrict the users of a free program by obtaining a
restrictive license from a patent holder.  Therefore, we insist that
any patent license obtained for a version of the library must be
consistent with the full freedom of use specified in this license.

  Most GNU software, including some libraries, is covered by the
ordinary GNU General Public License.  This license, the GNU Lesser
General Public License, applies to certain designated libraries, and
is quite different from the ordinary General Public License.  We use
this license for certain libraries in order to permit linking those
libraries into non-free programs.

  When a program is linked with a library, whether statically or using
a shared library, the combination of the two is legally speaking a
combined work, a derivative of the original library.  The ordinary
General Public License therefore permits such linking only if the
entire combination fits its criteria of freedom.  The Lesser General
Public License permits more lax criteria for linking other code with
the library.

  We call this license the "Lesser" General Public License because it
does Less to protect the user's freedom than the ordinary General
Public License.  It also provides other free software developers Less
of an advantage over competing non-free programs.  These disadvantages
are the reason we use the ordinary General Public License for many
libraries.  However, the Lesser license provides advantages in certain
special circumstances.

  For example, on rare occasions, there may be a special need to
encourage the widest possible use of a certain library, so that it
becomes
a de-facto standard.  To achieve this, non-free programs must be
allowed to use the library.  A more frequent case is that a free
library does the same job as widely used non-free libraries.  In this
case, there is little to gain by limiting the free library to free
software only, so we use the Lesser General Public License.

  In other cases, permission to use a particular library in non-free
programs enables a greater number of people to use a large body of
free software.  For example, permission to use the GNU C Library in
non-free programs enables many more people to use the whole GNU
operating system, as well as its variant, the GNU/Linux operating
system.

  Although the Lesser General Public License is Less protective of the
users' freedom, it does ensure that the user of a program that is
linked with the Library has the freedom and the wherewithal to run
that program using a modified version of the Library.

  The precise terms and conditions for copying, distribution and
modification follow.  Pay close attention to the difference between a
"work based on the library" and a "work that uses the library".  The
former contains code derived from the library, whereas the latter must
be combined with the library in order to run.
^L
                  GNU LESSER GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License Agreement applies to any software library or other
program which contains a notice placed by the copyright holder or
other authorized party saying it may be distributed under the terms of
this Lesser General Public License (also called "this License").
Each licensee is addressed as "you".

  A "library" means a collection of software functions and/or data
prepared so as to be conveniently linked with application programs
(which use some of those functions and data) to form executables.

  The "Library", below, refers to any such software library or work
which has been distributed under these terms.  A "work based on the
Library" means either the Library or any derivative work under
copyright law: that is to say, a work containing the Library or a
portion of it, either verbatim or with modifications and/or translated
straightforwardly into another language.  (Hereinafter, translation is
included without limitation in the term "modification".)

  "Source code" for a work means the preferred form of the work for
making modifications to it.  For a library, complete source code means
all the source code for all modules it contains, plus any associated
interface definition files, plus the scripts used to control
compilation
and installation of the library.

  Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running a program using the Library is not restricted, and output from
such a program is covered only if its contents constitute a work based
on the Library (independent of the use of the Library in a tool for
writing it).  Whether that is true depends on what the Library does
and what the program that uses the Library does.

  1. You may copy and distribute verbatim copies of the Library's
complete source code as you receive it, in any medium, provided that
you conspicuously and appropriately publish on each copy an
appropriate copyright notice and disclaimer of warranty; keep intact
all the notices that refer to this License and to the absence of any
warranty; and distribute a copy of this License along with the
Library.

  You may charge a fee for the physical act of transferring a copy,
and you may at your option offer warranty protection in exchange for a
fee.

  2. You may modify your copy or copies of the Library or any portion
of it, thus forming a work based on the Library, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) The modified work must itself be a software library.

    b) You must cause the files modified to carry prominent notices
    stating that you changed the files and the date of any change.

    c) You must cause the whole of the work to be licensed at no
    charge to all third parties under the terms of this License.

    d) If a facility in the modified Library refers to a function or a
    table of data to be supplied by an application program that uses
    the facility, other than as an argument passed when the facility
    is invoked, then you must make a good faith effort to ensure that,
    in the event an application does not supply such function or
    table, the facility still operates, and performs whatever part of
    its purpose remains meaningful.

    (For example, a function in a library to compute square roots has
    a purpose that is entirely well-defined independent of the
    application.  Therefore, Subsection 2d requires that any
    application-supplied function or table used by this function must
    be optional: if the application does not supply it, the square
    root function must still compute square roots.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Library,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Library, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Library.

In addition, mere aggregation of another work not based on the Library
with the Library (or with a work based on the Library) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may opt to apply the terms of the ordinary GNU General Public
License instead of this License to a given copy of the Library.  To do
this, you must alter all the notices that refer to this License, so
that they refer to the ordinary GNU General Public License, version 2,
instead of to this License.  (If a newer version than version 2 of the
ordinary GNU General Public License has appeared, then you can specify
that version instead if you wish.)  Do not make any other change in
these notices.
^L
  Once this change is made in a given copy, it is irreversible for
that copy, so the ordinary GNU General Public License applies to all
subsequent copies and derivative works made from that copy.

  This option is useful when you wish to copy part of the code of
the Library into a program that is not a library.

  4. You may copy and distribute the Library (or a portion or
derivative of it, under Section 2) in object code or executable form
under the terms of Sections 1 and 2 above provided that you accompany
it with the complete corresponding machine-readable source code, which
must be distributed under the terms of Sections 1 and 2 above on a
medium customarily used for software interchange.

  If distribution of object code is made by offering access to copy
from a designated place, then offering equivalent access to copy the
source code from the same place satisfies the requirement to
distribute the source code, even though third parties are not
compelled to copy the source along with the object code.

  5. A program that contains no derivative of any portion of the
Library, but is designed to work with the Library by being compiled or
linked with it, is called a "work that uses the Library".  Such a
work, in isolation, is not a derivative work of the Library, and
therefore falls outside the scope of this License.

  However, linking a "work that uses the Library" with the Library
creates an executable that is a derivative of the Library (because it
contains portions of the Library), rather than a "work that uses the
library".  The executable is therefore covered by this License.
Section 6 states terms for distribution of such executables.

  When a "work that uses the Library" uses material from a header file
that is part of the Library, the object code for the work may be a
derivative work of the Library even though the source code is not.
Whether this is true is especially significant if the work can be
linked without the Library, or if the work is itself a library.  The
threshold for this to be true is not precisely defined by law.

  If such an object file uses only numerical parameters, data
structure layouts and accessors, and small macros and small inline
functions (ten lines or less in length), then the use of the object
file is unrestricted, regardless of whether it is legally a derivative
work.  (Executables containing this object code plus portions of the
Library will still fall under Section 6.)

  Otherwise, if the work is a derivative of the Library, you may
distribute the object code for the work under the terms of Section 6.
Any executables containing that work also fall under Section 6,
whether or not they are linked directly with the Library itself.
^L
  6. As an exception to the Sections above, you may also combine or
link a "work that uses the Library" with the Library to produce a
work containing portions of the Library, and distribute that work
under terms of your choice, provided that the terms permit
modification of the work for the customer's own use and reverse
engineering for debugging such modifications.

  You must give prominent notice with each copy of the work that the
Library is used in it and that the Library and its use are covered by
this License.  You must supply a copy of this License.  If the work
during execution displays copyright notices, you must include the
copyright notice for the Library among them, as well as a reference
directing the user to the copy of this License.  Also, you must do one
of these things:

    a) Accompany the work with the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work (which must be distributed under
    Sections 1 and 2 above); and, if the work is an executable linked
    with the Library, with the complete machine-readable "work that
    uses the Library", as object code and/or source code, so that the
    user can modify the Library and then relink to produce a modified
    executable containing the modified Library.  (It is understood
    that the user who changes the contents of definitions files in the
    Library will not necessarily be able to recompile the application
    to use the modified definitions.)

    b) Use a suitable shared library mechanism for linking with the
    Library.  A suitable mechanism is one that (1) uses at run time a
    copy of the library already present on the user's computer system,
    rather than copying library functions into the executable, and (2)
    will operate properly with a modified version of the library, if
    the user installs one, as long as the modified version is
    interface-compatible with the version that the work was made with.

    c) Accompany the work with a written offer, valid for at
    least three years, to give the same user the materials
    specified in Subsection 6a, above, for a charge no more
    than the cost of performing this distribution.

    d) If distribution of the work is made by offering access to copy
    from a designated place, offer equivalent access to copy the above
    specified materials from the same place.

    e) Verify that the user has already received a copy of these
    materials or that you have already sent this user a copy.

  For an executable, the required form of the "work that uses the
Library" must include any data and utility programs needed for
reproducing the executable from it.  However, as a special exception,
the materials to be distributed need not include anything that is
normally distributed (in either source or binary form) with the major
components (compiler, kernel, and so on) of the operating system on
which the executable runs, unless that component itself accompanies
the executable.

  It may happen that this requirement contradicts the license
restrictions of other proprietary libraries that do not normally
accompany the operating system.  Such a contradiction means you cannot
use both them and the Library together in an executable that you
distribute.
^L
  7. You may place library facilities that are a work based on the
Library side-by-side in a single library together with other library
facilities not covered by this License, and distribute such a combined
library, provided that the separate distribution of the work based on
the Library and of the other library facilities is otherwise
permitted, and provided that you do these two things:

    a) Accompany the combined library with a copy of the same work
    based on the Library, uncombined with any other library
    facilities.  This must be distributed under the terms of the
    Sections above.

    b) Give prominent notice with the combined library of the fact
    that part of it is a work based on the Library, and explaining
    where to find the accompanying uncombined form of the same work.

  8. You may not copy, modify, sublicense, link with, or distribute
the Library except as expressly provided under this License.  Any
attempt otherwise to copy, modify, sublicense, link with, or
distribute the Library is void, and will automatically terminate your
rights under this License.  However, parties who have received copies,
or rights, from you under this License will not have their licenses
terminated so long as such parties remain in full compliance.

  9. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Library or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Library (or any work based on the
Library), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Library or works based on it.

  10. Each time you redistribute the Library (or any work based on the
Library), the recipient automatically receives a license from the
original licensor to copy, distribute, link with or modify the Library
subject to these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties with
this License.
^L
  11. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Library at all.  For example, if a patent
license would not permit royalty-free redistribution of the Library by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Library.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply, and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  12. If the distribution and/or use of the Library is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Library under this License
may add an explicit geographical distribution limitation excluding those
countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  13. The Free Software Foundation may publish revised and/or new
versions of the Lesser General Public License from time to time.
Such new versions will be similar in spirit to the present version,
but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Library
specifies a version number of this License which applies to it and
"any later version", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Library does not specify a
license version number, you may choose any version ever published by
the Free Software Foundation.
^L
  14. If you wish to incorporate parts of the Library into other free
programs whose distribution conditions are incompatible with these,
write to the author to ask for permission.  For software which is
copyrighted by the Free Software Foundation, write to the Free
Software Foundation; we sometimes make exceptions for this.  Our
decision will be guided by the two goals of preserving the free status
of all derivatives of our free software and of promoting the sharing
and reuse of software generally.

                            NO WARRANTY

  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

                     END OF TERMS AND CONDITIONS
^L
           How to Apply These Terms to Your New Libraries

  If you develop a new library, and you want it to be of the greatest
possible use to the public, we recommend making it free software that
everyone can redistribute and change.  You can do so by permitting
redistribution under these terms (or, alternatively, under the terms
of the ordinary General Public License).

  To apply these terms, attach the following notices to the library.
It is safest to attach them to the start of each source file to most
effectively convey the exclusion of warranty; and each file should
have at least the "copyright" line and a pointer to where the full
notice is found.


    <one line to give the library's name and a brief idea of what it
does.>
    Copyright (C) <year>  <name of author>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

Also add information on how to contact you by electronic and paper
mail.

You should also get your employer (if you work as a programmer) or
your
school, if any, to sign a "copyright disclaimer" for the library, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the
  library `Frob' (a library for tweaking knobs) written by James
Random Hacker.

  <signature of Ty Coon>, 1 April 1990
  Ty Coon, President of Vice

That's all there is to it!


""",
        }


    def OnCbOkButton(self, event):
        self.EndModal(0)
        event.Skip()
    def OnVAboutDialogSize(self, event):
        event.Skip()
        self.lbbLogo.Refresh()
        self.Refresh()
