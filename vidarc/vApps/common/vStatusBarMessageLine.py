#----------------------------------------------------------------------------
# Name:         vStatusBarMessageLine.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060417
# CVS-ID:       $Id: vStatusBarMessageLine.py,v 1.7 2007/10/15 21:30:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import Queue,time,sys,threading
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.vtRingBuffer import vtRingBuffer
from vidarc.tool.vtThread import CallBack

class vStatusBarMessageLineTransientPopup(wx.Dialog):
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        id=wx.NewId()
        self.lstMsg = wx.ListCtrl(id=id, name=u'lstStatusMsg',
              parent=self, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=wx.LC_REPORT)
        self.lstMsg.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('message'), width=280)
        self.lstMsg.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('time'), width=120)
        self.selIdx=-1
        bxs.AddWindow(self.lstMsg, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.SetSizer(bxs)
        self.Layout()
        
        self.szOrig=(240,140)
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        iWCol=self.lstMsg.GetColumnWidth(1)
        self.lstMsg.SetColumnWidth(0,iW-iWCol-20)
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def AddMsg(self,sMsg,sTime,iPos=sys.maxint):
        if sMsg is None:
            self.lstMsg.DeleteAllItems()
        else:
            idx=self.lstMsg.InsertStringItem(iPos,sMsg)
            self.lstMsg.SetStringItem(idx,1,sTime)

class vStatusBarMessageLine:
    def __init__(self,par,iNum,zClrTime):
        self.sb=par
        self.popWinSb=None
        self.iNum=iNum
        self.qMsg=Queue.Queue()
        self.zClrTime=zClrTime
        if zClrTime>0:
            self.zMsgTimer = wx.Timer(self)#self.NotifyClrMsgLine)
            self.Bind(wx.EVT_TIMER,self.OnNotifyClrMsgLine)
            self.rbMsg=None
        else:
            self.rbMsg=vtRingBuffer(100)
            self.semStatusBar=threading.Lock()#threading.Semaphore()
        self.zDt=vtDateTime(True)
    def _PrintMsg(self,s):
        self.sb.SetStatusText(s, self.iNum)
    def PrintMsg(self,s,bForce=False):
        """ put a message string into queue for delayed display on status bar. this method is thread safe.
        """
        # 061009 wro
        # attention do only add information to queue, never ever set statusbar info here!!!
        # this method may be called by threads, therefore a possible wx-crash may be cased!!! 
        #return
        if bForce:
            CallBack(self._PrintMsg,s)
            #bMain=wx.Thread_IsMain()
            #if bMain==False:
            #    wx.MutexGuiEnter()
            #try:
            #    self.sb.SetStatusText(s, self.iNum)
            #except:
            #    pass
            #if bMain==False:
            #    wx.MutexGuiLeave()
        if self.zClrTime>0:
            self.zMsgTimer.Start(self.zClrTime,True)
            self.sb.SetStatusText(s, self.iNum)
        else:
            self.semStatusBar.acquire()
            try:
                self.zDt.Now()
                tup=(s,self.zDt.GetDateTimeStr(sep=' '))
                self.rbMsg.put(tup)
                self.qMsg.put(tup)
            except:
                pass
            self.semStatusBar.release()
    def GetPrintMsgTup(self):
        if self.qMsg.empty():
            return None
        else:
            return self.qMsg.get()
    def OnNotifyClrMsgLine(self,evt):
        self.sb.SetStatusText('', self.iNum)
    def NotifyClrMsgLine(self):
        self.sb.SetStatusText('', self.iNum)
    def __createPopupStatusBar__(self):
        if self.popWinSb is None:
            sz=self.sb.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWinSb=vStatusBarMessageLineTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                pass
        else:
            pass
    def ShowPopupStatusBar(self):
        self.__createPopupStatusBar__()
        if self.popWinSb.IsShown()==False:
            btn=self.sb
            rect = self.sb.GetFieldRect(self.iNum)
            iX,iY = btn.ClientToScreen( (0,0) )
            iDX,iDY = rect.x+2, rect.y+2
            iX+=iDX
            #iY+=iDY
            iW,iH =  rect.width-4, rect.height-4
            iPopW,iPopH=self.popWinSb.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY-=iPopH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWinSb.Move((iX,iY))
            self.popWinSb.SetSize((iW,50))
            self.popWinSb.Show(True)
            return True
        else:
            self.popWinSb.Show(False)
            return False
    def IsShownPopupStatusBar(self):
        if self.popWinSb is None:
            return False
        return self.popWinSb.IsShown()
    def AddMsg2PopupStatusBar(self,tup,iPos=sys.maxint):
        if self.popWinSb is not None:
            if tup is None:
                return
            sMsg,sTime=tup[0],tup[1]
            self.popWinSb.AddMsg(sMsg,sTime,iPos)

