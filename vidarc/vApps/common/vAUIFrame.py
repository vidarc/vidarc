#----------------------------------------------------------------------------
# Name:         vMDIFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060529
# CVS-ID:       $Id: vAUIFrame.py,v 1.1 2009/11/14 07:53:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.aui as wa

import types

from vMDIFrame import vMDIFrame

class vAUIFrame(vMDIFrame):
    def __init__(self,*args,**kwargs):
        vMDIFrame.__init__(self,*args,**kwargs)
        
        self._mng=None
        self._mng=wa.AuiManager()
        self._mng.SetManagedWindow(self)
        self.SetToolBar=self.__Mng_SetToolBar__
        wx.CallAfter(self._mng.Update)
    def __Mng_SetToolBar__(self,wid):
        wid.Realize()
        self._mng.AddPane(wid, wa.AuiPaneInfo().
                          Name("tbMain").#Caption("main toolbar").
                          ToolbarPane().Top().LeftDockable(False).RightDockable(False))
    def AddWidget(self,wid,caption,pos=0,dock=-1,layer=1,show=1,
                close=0,maximize=0,bUpdate=False):
        ai=wa.AuiPaneInfo().Name(wid.GetName())
        if caption is not None:
            ai.Caption(caption)
        else:
            ai.CaptionVisible(False)
        if dock==0:
            ai.Left()
        elif dock==1:
            ai.Right()
        elif dock==2:
            ai.Top()
        elif dock==3:
            ai.Bottom()
        elif dock==4:
            ai.CenterPane()
        elif dock==-1:
            ai.Float()
        ai.Layer(layer)
        ai.Position(pos)
        if close:
            ai.CloseButton(True)
        else:
            ai.CloseButton(False)
        if maximize:
            ai.MaximizeButton(True)
        else:
            ai.MaximizeButton(False)
        if show==0:
            ai.Hide()
        self._mng.AddPane(wid, ai)
        if bUpdate:
            self._mng.Update()
    def ShowWidget(self,wid,bFlag=True):
        if bFlag:
            self._mng.GetPane(wid.GetName()).Show()
        else:
            self._mng.GetPane(wid.GetName()).Hide()
        self._mng.Update()
    def AddToolBar(self,iOfs,l,sz=(20,20),sName='_tbMain'):
        try:
            bAddTB=False
            if hasattr(self,sName)==False:
                iId=wx.NewId()
                tb = wx.ToolBar(id=iId,
                    name=sName, parent=self, pos=wx.Point(0,0),
                    size=wx.DefaultSize, style=wx.TB_FLAT | wx.TB_NODIVIDER)
                tb.SetToolBitmapSize(sz)
                setattr(self,sName,tb)
                bAddTB=True
                #self.SetToolBar(self._tbMain)
                self._dToolBarId={}
                self._dToolBar={}
                #if self._bLayout==True:
                #    wx.CallAfter(self.gbsData.Layout)
                #    wx.CallAfter(self.gbsData.Fit,self)
                
            tb=getattr(self,sName)
            d=getattr(self,'_dToolBar')
            dId=getattr(self,'_dToolBarId')
            for tup in l:
                t=type(tup)
                if tup is None:
                    tb.InsertSeparator(iOfs)
                    iOfs+=1
                elif t==types.ListType:
                    pass
                elif t==types.TupleType:
                    if tup[0] in d:
                        self.__logError__('key:%s already in d;%s'%(tup[0],
                                self.__logFmt__(d)))
                        continue
                    if tup[1]=='txt':
                        iId=wx.NewId()
                        txtCtrl=wx.TextCtrl(tb, iId, tup[2], size=(150, -1))
                        if tup[3] is not None:
                            pass
                        tb.AddControl(txtCtrl)
                        tb.InsertControl(iOfs,txtCtrl)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
                    elif tup[1]=='bt':
                        iId=wx.NewId()
                        tb.InsertSimpleTool(iOfs,iId, tup[2],  
                                shortHelpString=tup[3])
                        self.Bind(wx.EVT_TOOL, self.OnAddToolBar, id=iId)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
            tb.Realize()
            if bAddTB:
                self._mng.AddPane(tb, wa.AuiPaneInfo().
                          Name(sName).#Caption(sCaption).
                          ToolbarPane().Top())
                
        except:
            self.__logTB__()
    def OnAddToolBar(self,evt):
        try:
            eId=evt.GetId()
            if eId in self._dToolBarId:
                t=self._dToolBarId[eId]
                t[0](*t[1],**t[2])
        except:
            self.__logTB__()
    def EnableTool(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dToolBar:
                        self._tbMain.EnableTool(self._dToolBar[k],bFlag)
            else:
                if v in self._dToolBar:
                    self._tbMain.EnableTool(self._dToolBar[v],bFlag)
        except:
            self.__logTB__()
    def AddMenu(self,iOfs,l,lHier=None):
        try:
            if hasattr(self,'mnbMain')==False:
                mnb=wx.MenuBar()
                setattr(self,'mnbMain',mnb)
                self.SetMenuBar(mnb)
                #if self._bLayout==True:
                #    wx.CallAfter(self.gbsData.Layout)
                #    wx.CallAfter(self.gbsData.Fit,self)
            if hasattr(self,'mnbMain'):
                mnb=getattr(self,'mnbMain')
                mn=None
                if lHier is not None:
                    iPos=lHier[0]
                    iCnt=mn.GetMenuCount()
                    if iPos>=iCnt:
                        self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                        mn=mn.GetMenu(iCnt-1)
                    else:
                        mn=mn.GetMenu(iPos)
                    for iPos in lHier[1:]:
                        iCnt=mn.GetMenuItemCount()
                        if iPos>=iCnt:
                            self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                            mn=mn.GetMenuItems(iCnt-1)
                        else:
                            mn=mn.GetMenuItems(iPos)
                #mn=getattr(self,sMnName)
                sAttr='_popIdMenu'
                if hasattr(self,sAttr):
                    dId=getattr(self,sAttr)
                else:
                    dId={}
                    setattr(self,sAttr,dId)
                sAttr='_dMenu'
                if hasattr(self,sAttr):
                    d=getattr(self,sAttr)
                else:
                    d={}
                    setattr(self,sAttr,d)
                    #wx.EVT_MENU(self,self.popupIdHelp,self.OnHelp)
                def addItems(mn,d,iOfs,l,mnb):
                    for tup in l:
                        t=type(tup)
                        if tup is None:
                            mn.InsertSeparator(iOfs)
                            iOfs+=1
                        elif tup[0] in d:
                            self.__logError__('key:%s already in d;%s'%(tup[0],
                                    self.__logFmt__(d)))
                            #return
                        elif t==types.ListType:
                            iId=wx.NewId()
                            mnSub=wx.Menu()
                            addItems(mnSub,d,0,tup[6],mnb)
                            if mn is None:
                                mnb.Insert(iOfs,menu=mnSub,title=tup[1])
                                d[tup[0]]=iId
                            else:
                                mnIt=wx.MenuItem(mn,iId,tup[1],subMenu=mnSub)
                                if tup[2] is not None:
                                    mnIt.SetBitmap(tup[2])
                                wx.EVT_MENU(self,iId,self.OnAddMenu)
                                d[tup[0]]=iId
                                dId[iId]=(tup[3],tup[4],tup[5])
                                mn.InsertItem(iOfs,mnIt)
                            iOfs+=1
                        else:
                            iId=wx.NewId()
                            mnIt=wx.MenuItem(mn,iId,tup[1])
                            if tup[2] is not None:
                                mnIt.SetBitmap(tup[2])
                            wx.EVT_MENU(self,iId,self.OnAddMenu)
                            d[tup[0]]=iId
                            dId[iId]=(tup[3],tup[4],tup[5])
                            mn.InsertItem(iOfs,mnIt)
                            iOfs+=1
                addItems(mn,d,iOfs,l,mnb)
                #if self.__isLogDebug__():
                #    self.__logDebug__('menu:%s;d:;%s'%(sMnName,
                #            vtLog.pformat(self._popIdAddMenuItems)))
            else:
                self.__logError__('menu:%s not found'%(sMnName))
        except:
            self.__logTB__()
    def OnAddMenu(self,evt):
        try:
            eId=evt.GetId()
            sLang=None
            if eId in self._popIdMenu:
                t=self._popIdMenu[eId]
                t[0](*t[1],**t[2])
            else:
                self.__logError__('eId:%d not found;%s'%(eId,
                        vtLog.pformat(self._popIdAddMenuItems)))
        except:
            self.__logTB__()
    def EnableMenu(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dMenu:
                        self._mnbMain.Enable(self._dMenu[k],bFlag)
            else:
                if v in self._dMenu:
                    self._mnbMain.Enable(self._dMenu[v],bFlag)
        except:
            self.__logTB__()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def OnExit(self,evt):
        self.Close()
        evt.Skip()
    def OnHelp(self,evt):
        evt.Skip()
    def SetCfgData(self,func,l,d):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(d)
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg.update(d)
        try:
            self.pn.SetCfgData(func,l,self.dCfg)
        except:
            pass
        try:
            x=int(d['x'])
            y=int(d['y'])
            #self.GetParent().Move(x,y)
            #wx.CallAfter(self.Move,(x,y))
            #sz=map(int,self.dCfg['size'].split(','))
            #self.GetParent().SetSize(sz)
        except:
            pass
        try:
            self.SetSize((int(d['width']),int(d['height'])))
            self.Maximize(int(d['maximized']))
        except:
            pass#vtLog.vtLngTB(self.GetName())
        
    def GetCfgData(self):
        try:
            self.pn.GetCfgData()
        except:
            pass
        try:
            pos=self.GetPositionTuple()
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            #iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['x']=str(iX)
            self.dCfg['y']=str(iY)
            sz=self.GetSize()
            iWidth=sz[0]
            iHeight=sz[1]
            #iWidth-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            if self.IsMaximized():
                self.dCfg['maximized']='1'
            else:
                self.dCfg['width']=str(iWidth)
                self.dCfg['height']=str(iHeight)
                self.dCfg['maximized']='0'
        except:
            vtLog.vtLngTB(self.GetName())
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dCfg)
        
    def OnMainClose(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'CanVeto:%s'%(event.CanVeto()),self)
        try:
            if self.netMaster is not None:
                if self.netMaster.IsShutDownActive()==False:
                    if event.CanVeto():
                        self.GetCfgData()
                        if self.netMaster.IsModified()==True:
                            dlg = vtmMsgDialog(self,#wx.MessageDialog(widDlgPar,#self,
                                        _(u'Unsaved data present!\n\nDo you want to save data?'),
                                        u'VIDARC ' + self.appl + u' '+ _(u'Close'),
                                        wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                                        )
                            if dlg.ShowModal()==wx.ID_YES:
                                #self.SaveFile()
                                self.netMaster.Save()
                                pass
                        self.thdFrm.Stop()
                        self.thdFrm.SetPostWid(None)
                        self.netMaster.ShutDown()
                        try:
                            xdCfg=self.pn.xdCfg
                            xdCfg.Close()
                            xdCfg.StopConsumer()
                        except:
                            vtLog.vtLngTB(self.GetName())
                        event.Veto()
                        vtLog.vtLngCurWX(vtLog.INFO,'veto',self)
                    else:
                        event.Skip()
                else:
                    if self.netMaster.IsShutDownFinished()==False:
                        if event.CanVeto():
                            event.Veto()
                        else:
                            event.Skip()
                    else:
                        event.Skip()
            else:
                event.Skip()
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        except:
            vtLog.vtLngTB(self.GetName())
            event.Skip()
    
    def __getattr__(self,name):
        if hasattr(self.pn,name):
            return getattr(self.pn,name)
        raise AttributeError(name)

