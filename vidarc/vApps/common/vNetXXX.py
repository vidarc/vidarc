#----------------------------------------------------------------------------
# Name:         vNetXXX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vNetXXX.py,v 1.3 2007/07/30 20:38:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vXXX.vXmlXXX import vXmlXXX
from vidarc.tool.net.vNetXmlWxGui import *

class vNetXXX(vXmlXXX,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlXXX.__init__(self,attr,skip,synch=synch,verbose=verbose)
        vXmlXXX.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlCustomer.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlCustomer.IsRunning(self):
    #        return True
    #    return False
    def New(self,root='XXXroot'):
        vNetXmlWxGui.New(self)
        vXmlXXX.New(self,root=root,bConnection=self.IsFlag(self.CONN_ACTIVE))
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlXXX.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'file:%s;slient:%d;thread:%d'%(repr(fn),silent,bThread),origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlXXX.New()
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlXXX.Open(self,fn,True)
        else:
            iRet=vXmlXXX.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        #sFN=vNetXmlWxGui.GetFN(self)
        #if sFN is not None:
        #    if fn is None:
        #        fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlXXX.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlXXX.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlXXX.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlXXX.delNode(self,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlXXX.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlXXX.__stop__(self)
    def NotifyContent(self):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        self.customers=self.getSortedNodeIds(['customers'],
                        'customer','id',[('name','%s')])
    def GetSortedIds(self):
        try:
            return self.customers
        except:
            self.GenerateSortedIds()
            return self.customers
            