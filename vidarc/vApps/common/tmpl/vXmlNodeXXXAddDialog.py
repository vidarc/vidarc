#Boa:Dialog:vXmlNodeXXXAddDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXAddDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeXXXAddDialog.py,v 1.4 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vXmlNodeXXXPanel import *

def create(parent):
    return vXmlNodeXXXAddDialog(parent)

[wxID_VXMLNODEXXXADDDIALOG, wxID_VXMLNODEXXXADDDIALOGCBAPPLY, 
 wxID_VXMLNODEXXXADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeXXXAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODEXXXADDDIALOG,
              name=u'vXmlNodeXXXAddDialog', parent=prnt, pos=wx.Point(221, 78),
              size=wx.Size(474, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vXXX Add Dialog')
        self.SetClientSize(wx.Size(466, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEXXXADDDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(136, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEXXXADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEXXXADDDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(248, 296), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODEXXXADDDIALOGCBCANCEL)

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('ZZZ')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnXXX=vXmlNodeXXXPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnLoc')
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
    def SetDoc(self,doc,bNet=False):
        self.pnXXX.SetDoc(doc,bNet)
    def SetNode(self,nodeObj,nodePar,nodeDft=None):
        self.nodeObj=nodeObj
        self.nodePar=nodePar
        self.pnXXX.SetNode(nodeDft)
    def GetNode(self,node):
        self.pnXXX.GetNode(node)
    def OnCbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
