#----------------------------------------------------------------------------
# Name:         vXmlXXX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlXXX.py,v 1.3 2006/04/11 17:40:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vXXX.vXmlNodeXXX import vXmlNodeXXX
#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
import traceback
VERBOSE=1

class vXmlXXX(vtXmlDomReg):
    NODE_ATTRS=[
            ('name',None,'name',None),
            ('country',None,'contry',None),
            ('region',None,'region',None),
            ('distance',None,'distance',None),
            ('coor',None,'coor',None),
            ]
    def __init__(self,attr='id',skip=[],synch=False,verbose=0):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr,skip,synch,verbose)
            self.verbose=verbose
            oXXX=vXmlNodeXXX()
            #oAddr=vtXmlNodeAddr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #oArea=vXmlNodeArea()
            self.RegisterNode(oXXX,True)
            #self.RegisterNode(oAddr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(oArea,False)
            self.LinkRegisteredNode(oXXX,oLog)
            #self.LinkRegisteredNode(oCust,oAddr)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
        except:
            traceback.print_exc()
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'XXX')
    def New(self,revision='1.0',root='XXXRoot',bConnection=False):
        vtXmlDomReg.New(self,revision,root)
        elem=self.createSubNode(self.getRoot(),'XXX')
        self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
