#Boa:Dialog:vXmlNodeXXXEditDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeXXXEditDialog.py,v 1.4 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vXmlNodeXXXPanel import *


def create(parent):
    return vXmlNodeXXXEditDialog(parent)

[wxID_VXMLNODEXXXEDITDIALOG, wxID_VXMLNODEXXXEDITDIALOGCBAPPLY, 
 wxID_VXMLNODEXXXEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeXXXEditDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODEXXXEDITDIALOG,
              name=u'vXmlNodeXXXEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(466, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vXXX Edit Dialog')
        self.SetClientSize(wx.Size(458, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEXXXEDITDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(136, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEXXXEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEXXXEDITDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(248, 296), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODEXXXEDITDIALOGCBCANCEL)

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('ZZZ')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnXXX=vXmlNodeXXXPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnLoc')
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
    def SetDoc(self,doc,bNet=False):
        self.pnXXX.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnXXX.SetNode(node)
    def GetNode(self):
        self.pnXXX.GetNode()
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
