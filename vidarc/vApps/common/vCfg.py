#----------------------------------------------------------------------------
# Name:         vCfg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061117
# CVS-ID:       $Id: vCfg.py,v 1.5 2008/02/02 16:10:52 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import types

import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog

class vCfg:
    def __init__(self):
        pass
    def __del__(self):
        pass
    def getCfgData(self,docCfg,l):
        try:
            vtLog.vtLngCur(vtLog.INFO,'l:%s'%(vtLog.pformat(l)),'vCfg')
            if docCfg is None:
                return {}
            d={}
            docCfg.acquire('dom')
            cfgNode=docCfg.getChildByLstForced(None,l)
            if cfgNode is not None:
                def __getCfgData__(node,dd):
                    for c in docCfg.getChilds(node):
                        if docCfg.hasChilds(c):
                            ddd={}
                            __getCfgData__(c,ddd)
                            dd[docCfg.getTagName(c)]=ddd
                        else:
                            dd[docCfg.getTagName(c)]=docCfg.getText(c)
                __getCfgData__(cfgNode,d)
            docCfg.release('dom')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'d:%s'%(vtLog.pformat(d)),'vCfg')
            return d
        except:
            vtLog.vtLngTB('vCfg')
            docCfg.release('dom')
            return {}
    def setCfgData(self,docCfg,l,d):
        try:
            vtLog.vtLngCur(vtLog.INFO,'l:%s'%(vtLog.pformat(l)),'vCfg')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'d:%s'%(vtLog.pformat(d)),'vCfg')
            if docCfg is None:
                return 0
            
            docCfg.acquire('dom')
            cfgNode=docCfg.getChildByLstForced(None,l)
            for c in docCfg.getChilds(cfgNode):
                docCfg.deleteNode(c,cfgNode)
            def __setCfgData__(cfgNode,d):
                keys=d.keys()
                keys.sort()
                for k in keys:
                    kr=fnUtil.replaceSuspectChars(k)
                    if type(d[k])==types.DictionaryType:
                        cc=docCfg.getChildForced(cfgNode,kr)
                        __setCfgData__(cc,d[k])
                    else:
                        docCfg.setNodeText(cfgNode,kr,unicode(d[k]))
            __setCfgData__(cfgNode,d)
        except:
            vtLog.vtLngTB('vCfg')
        docCfg.release('dom')
        try:
            docCfg.AlignNode(cfgNode)
            docCfg.Save()
            return 0
        except:
            vtLog.vtLngTB('vCfg')
            return -1
