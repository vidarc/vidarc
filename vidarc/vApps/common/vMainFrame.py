#----------------------------------------------------------------------------
# Name:         vMainFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060515
# CVS-ID:       $Id: vMainFrame.py,v 1.3 2009/01/25 17:56:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vMainFrame:
    def __init__(self):
        
        pass
    def AddMenus(self,frmMain,widTree=None,widNetMaster=None,
                    mnFile=None,mnView=None,mnTools=None,mnAnalyse=None):
        if widTree is not None:
            if mnTools is not None and frmMain is not None:
                widTree.AddMenuTool(mnTools,frmMain,0)
            iPos=0
            if mnView is not None and frmMain is not None:
                mn,iPos=widTree.AddMenuGroup(mnView,frmMain,iPos)
            if mnView is not None and frmMain is not None:
                widTree.AddMenuLang(mnView,frmMain,iPos)
            if mnView is not None and frmMain is not None:
                widTree.AddMenuNav(mnView,frmMain,-1)
            if mnAnalyse is not None and frmMain is not None:
                widTree.AddMenuAnalyse(mnAnalyse,frmMain,0)
        if widNetMaster is not None:
            if mnFile is not None and frmMain is not None:
                widNetMaster.AddMenuGeneral(mnFile,frmMain,0)
