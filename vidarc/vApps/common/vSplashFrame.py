#Boa:Frame:vSplashFrame
#----------------------------------------------------------------------------
# Name:         vSplashFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vSplashFrame.py,v 1.12 2010/02/26 00:59:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import cStringIO
import traceback

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import images
import vtLogDef

def getLogoData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\xff\x00\x00\x001\x08\x06\
\x00\x00\x00\x15\xea\x7f\x0e\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\
\x00\x00\x04\xdcIDATx\x9c\xed\x9dOn\xd3@\x14\x87\xc7I\x01\xd1\xb2Be\x87\x04b\
I\xa5J\xbd\t]\xe5"U\xb6\xa8\xdb\xaa\'@\\ \xb7\x80\x13T\xca\x01({$v\xb0"1\x8b\
hR\xcb\xb1\xe7\xefof\xde\xc4\xbfOb\x81\xecx>\xbf\xf8\xbd\x19{&n\xd3\xcc\xe6\
\nA\xbb\xdd\xb4\xa1\x9fmf\xf3\x06"a\x81\x8e\x18\xe8\x88\xa1\xb4\xe3I\xec\x01\
\xba\\\xde-\xbc?\xb3^\xae\x90\nV\xe8\x88\x81\x8e\x18J:\xce GQ\xbbJ\xe4+\xb5^\
\xae\xb2UY\xa5\xe8\x88\x82\x8e\x18J;\xc2\x92\x9f\x10R\x17\xd0\xe4\xf7\xa9d\
\xb9\xab\xac\x86\x8e\x18\xe8\x88\xa1\xa4#{~B&\n<\xf9]*Y\xa9*\xab\xa1#\x06:b(\
\xe5h}\xda\x1f3\x1d\x81>\xee\xd0\xc9\xa7\xf2\x0b=6\x1d\x0f\xa1#\x06\xb4cc\
\x9b\xe7o\xb7\x9b6d:\x02M\xbf\xf2\xe9\x13\xb8\xbc[\xa8\xf5r\x154e\x82\x86\
\x8e\x18\xe8\x88\xc1\xe6\x08\x9d\xe7\xcf\x85\x94\x82d\x82\x8e\x18\xe8\x88a\
\xc8\xb1\xaa\xe4\xefV.\xa9\xd0\x11\x03\x1d1\x98\x1c\xabJ~\xc9A\xd6\xd0\x11\
\x03\x1d1\x98\x1c\xab\x99\xea\xab=\xd0R\xa0#\x86cp\xac&\xf9\t!X\xa2\x86\xfd\
\x0f7n+\x93\xae\xee\x17\xce\xfb\xeb}S\xe2\xea\xad\x94<\x9f>9\xfc\x86\x08qN\
\xedZc\x1c56\xf7\xae_\x7f\xdfPw\xf6\xfc\x95\xf3p\xb3\x8a\xba\xe8\xc9\x8e\x92\
qti7\x85\x1f\x93\xffH`\x01\xc0\x903\x8e!\t\x8d\xf4\x9b)\x95vU\x12\x8a\xdc\
\xbf\xb3\x0e\xa1\xb4\xa3\xcb\x85Q\xda\xd1\x85)8\xe6(26\xc7\xfd=\xff~\xc9\x9f\
\xc7\xfaa\xd3}H\x7f{\xf7\xff!\xf7,g\xef\xcf\xf7\'\x13\xfb\xa4\xd5\xd7\xdb\
\x87\x10G\xdb3\x91!\x9f\xb1}\x1fnVV\x7fD\x1c]\x9f\xe3\x84\xc62&\x8ec^\xaeqt\
\x89a\xa8\xe3X\x9b\x1aS\xbb!\x05\xc3\xe4\xb8O~\xbdq\xbd\\y\x17\x811\\\x83\
\xe8B\xf3l\xdeuTJ\xc9\x9cn\xc9\xe5xu\xbf\x08\xee=\x18\xc7\'$\xc5\xd1\x96+!\
\xae:\x8fu^w\x1d\x0f\x9e\xf6\xf7\x8b\x80+\xaeb\x88\'\x95\xfd\xa0K$\x87\xe3X\
\xcc]\x8bn\n\xc7\xbeSl\x070\x95\xef\xda5F\xa1\xc5\xaa_\x04\x942<\xf0\xbb\xbc\
[\x88\xec\x11\xba\xd0\x11C\xa8#j\xca\xc9\x85TqD\xde{\xbb8"\xda\x8b\x89s3\x9b\
7\xba\x10X\xe7\xf9c\x03\x8e\x1c\xfa\x8f!=\xb9\x94\x929l\xed\x93\xca\x11y\r\
\xc48\xfa\xaeK\t\xc5\xd71\xa4\xbdX\xc7f6o\xa0S}6\xa1\x9c=\x05!!L\xe9\x9a\xcc\
2\xcf\xcf9\xe8\xe3\xc3T\xc8kM\xa0Z\xbdC\xa9\xeaW}D.\xb6\x02\x9f\xe3\xf6/\x06\
\xc9n\xa9\x80\xf7\xfc>O-\t\xc9\xc9\xd5\xfdb\xff\xaffP#ik\xf2\xa3\xa6XR\x0e\
\xfd%O\x03iR9"\xe3\xea\xeaX\xf26\xee\x98\xe2\x18\xd2\xb6\xde7v\xad\x7f\xbb\
\xdd\xb4\xa3\xc3\xfe)\'\x14\x92)8\x8e\xf5\xa4\xfd\x8b3f\xe8\xaf\x1dO\xdf\xbe\
\x0e\xfa|\x1f\xe4,\x89\xc6%\x8e\xe8v}c\xaaW\xf2\x9e\xbe;?\xbc\xe7\xd7\'\xa0\
\xe7\x02C\xd6\xfd\xdbN0v\xd8\xd5_Q%1\xc1r8\xfa,\x07\x1e\xa2\xc68\xfe\xf8\xf2\
-Y[\xa1\xc5\t\x11G\x97\xb6c{z\xa5\x9e\x1c\x1f\xbf~\x7fJ\xfe~\xd2K\xa3\xfd\
\xb7\x15\xbd\x1cU\x93\xcb1\xe6B\x88q\xcc5]\x9b#\x8eC\x9d\x94O\x01@;\x9a\xda\
\x0e-\xf4N\xef\xf0\x93\x9a\xf4\x9a?\x8f\xbf\xa0Av\xdd\xee{q#\x12\xcaw{\x17\
\x9fe\xbd>\x98~H\xd4o\xdb\xe4\xeb\x1a\xdb\xd4qt\xfd\x01\r\xdaQ\x1f\xd3%\x9e.\
\xc7\xb1a}\x87_\x8a\xc4\x1f\x13\x0b\xed)$\xf7\xf6\x9a\xd2\x8e\xa9\x12?79\x1d\
K]\x8f\xb1#&\xc4w\xcd\x97y\x1c\t\xb5O_M\x91\xd0\xef\x0c\xf5]3\xf9\x8f\x00&~\
\x1c%\xe3\xe7\xb3\xee\x00\xbdF!\xe9\n?\xa9\x17\xa54/i>C\xf88&=\x1f\xc3\rjL\
\xbb\xa5\xbf\x83\x12\xed\xb3\xe7\'d\xa2T\x93\xfc\x12\xe7\xa0\xfb\xd0\x11\x03\
\x1d1\xd8\x1c\xabI~\xa5v\'#=\xe8t\xc4@G\x0c&\xc7\xaa\x92_\xbf\x85Dr\xc0\xe9\
\x88\x81\x8e\x18L\x8eU%\xbfF\x9f\x8c\xf4\xa0\xd31\x1e:b\x18rl\x9a\xd9\xdc\
\xf8\xa1\x98w\xfa\x0f-2\x88\t\xd0\xd0b\xa4\xd8\xbf9@\xc7\x1dt\xf4G\xba\xe3\
\xd9\x877\xea\xef\xcf\xdf\xa3\x8e\xd6\xe4\x0f\xa1\xddnZ\xd3\xea\xa2\xf5rU|9q\
-\x8e\x17\xb7\xd7j\xfe\xf2\xf9\xe0v)\x8e5\xc4\x91\x8e\x87T9\xec\x9f\x12c\x89\
OH,\xf0\xe4\xb7U0\xa5vC\x9b\x92\x7f"\xac\x16\xc7\x8b\xdbk\xe3>\x12\x1ck\x88#\
\x1d\x87a\xcf/\x18\xf6\xfa$%\xd0\xe4w\xa9`\x9aR\xd5\xb6\x16\xc7\x8f\x9f?9\
\xed\xcb8\x8eCG3|{\xafPN^\xbd(\xad@\x8e\x1c\xd8\xd3\xfe\x98\x8a\x94\xebI+\
\x1d1\xd0\x11Ci\xc7\xff\xeey`\x1ae\x99*\xfc\x00\x00\x00\x00IEND\xaeB`\x82' 

def getLogoBitmap():
    return wx.BitmapFromImage(getLogoImage())
def getLogoImage():
    stream = cStringIO.StringIO(getLogoData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getLaunchData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00 \x00\x00\x00 \x08\x06\x00\
\x00\x00szz\xf4\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\x027ID\
ATX\x85\xbd\x96=r\xa30\x18\x86\x1fa\x9fB]\xc69\xc7\x96)\xe87\xb5\xdb\r\x07\
\xd8]\x0e\xe0$\x07\x08\xb9CR\x87\x99\xf8\x14>\x80\xdd\xfa\x16\xf0m\x01\x92?\
\xc4\x8f1\xc6\xfb\xceh@\x12\xf0|\x7f\x920&Z\xf0?$e!\xbao\xa2\x85\x01\x88n\ru\
\r\xc0ZK\x9a\xa6\x8dg\xcc\xad"\xa0=\xb6\xd6\xb2^\xaf\xd9\xedv\xe4y^\x81\xeb\
\x08`\xa2\xc5\xac\r\x10@\xbe\xebk\x1c\xc7\x92\xa6\xa9Xk\xc5\xcd\x01\xe2\xdf\
\x993\x02R\x16\xf2\r\xac\xea\xfeJ\x84\x831\xfc\xb0\x96\xe3\xf1\xe8\x9f\xf3\
\xde3c\nZp\xe0\xa0\xe6\xef\xd5\xbd6`\x96"\xec\x82#\xd2\xff\x82\xd2\xd5\x06\
\xf4\xc1\x0f\xc6;\xc9\x16\xc8\xea{\xed\xfd,\x06x\xa8\xb7\xa8\x82\xbb\xb1m8\
\x1f\xe8*\x03\xa4,d\xaf\rQ\xf0\x83\x82\x1f\x80\'\xda\xde_e\xc098\xb4\x0b\xb1\
K\xcb[\xc1\xb7j\xbe\xcf{\xb82\x05+\x05\'\x80\xbb\xbc\x0f\xc1\'\x19\xd0\xf0>(\
8\r\x1f\xca\xbbVo\n\xc2\xd3+T_\xd8/\x81\xf7\x1a e!\x92$\x8d\xb1\xf7\xb77\x9e\
\x80=\xb0J\x12\x8c1\xeckXXtc\xe1@\xfb0\x02d_\xedc\xb2\x07\xc9\xea\xe6\xc7\
\x93\xa4\xd1\xcf\xea\x83G?w\xd1\xe15\x15\x9e\x05\x8d\t\xf0\x86\x01\xa8\x8ft\
\xb5>\xf8T\xb0kK\x95\x8aV\xce\xa4,$\x8ec\xbe\xee\xee|\rdj\xde\xd5\xc4}\xf8b\
\x87\xfe\xfe\xf9\xddY\xd4g7"\rw\xd0L]\xa7@\xb5:\xff\x07\xdc\x12\x94\x0f0\x8f\
\xa7\xf18\x8e\xfd/\x95\x83?PE@Gp\x0c\x18\xe0\xf9\xe5\xd5\xb4" e!\xf2Qw~\x9e\
\x8eu\xd3\xb1\xa8~\xd1\xdc\xeb\xc7\x82\xb5\x1a;a\x08\xf7\xfa\xac.y\x9e{O\x1f\
\xea)\xbd\xe7_\xa2\xe7\x97W\x03c\x0e\xa3\xcf*\r\xeawZ6\x9b\r\x00\xefT\xb5P\
\xcf\x8d\x0e\xbb\xee{\x03Z\xde\x7f\x0e\x7fh\xdb\x84_\x0cv\xf2E\xe8\r\x08\xe0\
\xe6\xb1]`.\x02\xe7\xe0}P\xade\x1f\xdcU\xbf\x89\x16\xa6\xab\xb8\x86\xe0c\xc0\
N\xc6D\x8b\xca\x00\xc1\xe7{\x08<\xa4K\xc0N\xa7\x9d\xd0\x9c\xc00mIM\xd1RC\xc7\
\x80\xb5\x97s\x18\xe9#\xd0\xf7\xb1\xb1a\x9d\x12~\x80\x7f\xf0\xb3a\xfc\xec\
\xfcO]\x00\x00\x00\x00IEND\xaeB`\x82' 

def getLaunchBitmap():
    return wx.BitmapFromImage(getLaunchImage())

def getLaunchImage():
    stream = cStringIO.StringIO(getLaunchData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

def create(parent,sPrg,sAppl,img,actionList,actionListPost,sApplName,iLogLv,iSockPort):
    return vSplashFrame(parent,sPrg,sAppl,img,actionList,actionListPost,sApplName,iLogLv,iSockPort)

[wxID_VSPLASHFRAME, wxID_VSPLASHFRAMECBAPPLY, wxID_VSPLASHFRAMECBCANCEL, 
 wxID_VSPLASHFRAMECBLAUNCH, wxID_VSPLASHFRAMECHCLOGLEVEL, 
 wxID_VSPLASHFRAMEGAGPROCESS, wxID_VSPLASHFRAMELBITMAP, 
 wxID_VSPLASHFRAMELBLAPPL, wxID_VSPLASHFRAMELBLAPPLLOGNAME, 
 wxID_VSPLASHFRAMELBLCOMPANY, wxID_VSPLASHFRAMELBLDUMMY, 
 wxID_VSPLASHFRAMELBLPORT, wxID_VSPLASHFRAMELBLPRG, 
 wxID_VSPLASHFRAMELBLPROCESS, wxID_VSPLASHFRAMEPNMAIN, 
 wxID_VSPLASHFRAMESNBLOGNAME, wxID_VSPLASHFRAMESNBPORT, 
 wxID_VSPLASHFRAMETXTLOGNAME, wxID_VSPLASHFRAMETXTPORT, 
] = [wx.NewId() for _init_ctrls in range(19)]


#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
# defined event for vgpData item selected
wxEVT_SPLASH_PROCESSED=wx.NewEventType()
def EVT_SPLASH_PROCESSED(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_PROCESSED,func)
class vgaSplashProcessed(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_PROCESSED(<widget_name>, self.OnSplashProcessed)
    """

    def __init__(self,idx):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_PROCESSED)
        self.idx=idx
    def GetIdx(self):
        return self.idx
    
wxEVT_SPLASH_FINISHED=wx.NewEventType()
def EVT_SPLASH_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_FINISHED,func)
class vgaSplashFinished(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_FINISHED(<widget_name>, self.OnSplashFinished)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_FINISHED)

wxEVT_SPLASH_ABORTED=wx.NewEventType()
def EVT_SPLASH_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_ABORTED,func)
class vgaSplashAborted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_ABORTED(<widget_name>, self.OnSplashAborted)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_ABORTED)

wxEVT_SPLASH_ACTION=wx.NewEventType()
def EVT_SPLASH_ACTION(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_ACTION,func)
class vgaSplashAction(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_ACTION(<widget_name>, self.OnSplashAction)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_ACTION)
        
class vSplashFrame(wx.Frame):
    def _init_coll_bxsLogBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsFin_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsInfo, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.cbLaunch, 0, border=4, flag=wx.RIGHT | wx.BOTTOM)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCompany, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddWindow(self.lblPrg, 0, border=16,
              flag=wx.EXPAND | wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lblAppl, 0, border=16,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lBitmap, 0, border=0, flag=0)
        parent.AddSizer(self.bxsLogName, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLogPort, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLogBt, 0, border=5,
              flag=wx.RIGHT | wx.LEFT | wx.ALIGN_RIGHT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsFin, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsLogName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblApplLogName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtLogName, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.snbLogName, 0, border=0, flag=0)
        parent.AddWindow(self.chcLogLevel, 1, border=8,
              flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblProcess, 0, border=4,
              flag=wx.LEFT | wx.RIGHT | wx.EXPAND | wx.ALL)
        parent.AddWindow(self.gagProcess, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.BOTTOM)

    def _init_coll_bxsLogPort_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPort, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPort, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.snbPort, 0, border=0, flag=0)
        parent.AddWindow(self.lblDummy, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=10, vgap=0)

        self.bxsLogName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLogPort = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsFin = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLogBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsLogName_Items(self.bxsLogName)
        self._init_coll_bxsLogPort_Items(self.bxsLogPort)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_bxsFin_Items(self.bxsFin)
        self._init_coll_bxsLogBt_Items(self.bxsLogBt)

        self.pnMain.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VSPLASHFRAME, name=u'vSplashFrame',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(413, 468),
              style=wx.STAY_ON_TOP, title=u'VIDARC loader')
        self.SetClientSize(wx.Size(405, 441))
        self.Bind(wx.EVT_ACTIVATE, self.OnVgfSplashActivate)
        self.Bind(wx.EVT_IDLE, self.OnVgfSplashIdle)

        self.pnMain = wx.Panel(id=wxID_VSPLASHFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(405, 441),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.gagProcess = wx.Gauge(id=wxID_VSPLASHFRAMEGAGPROCESS,
              name=u'gagProcess', parent=self.pnMain, pos=wx.Point(4, 410),
              range=100, size=wx.Size(338, 20),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.gagProcess.SetMinSize(wx.Size(-1, 20))

        self.lblProcess = wx.StaticText(id=wxID_VSPLASHFRAMELBLPROCESS,
              label=u'', name=u'lblProcess', parent=self.pnMain, pos=wx.Point(4,
              393), size=wx.Size(338, 13), style=wx.ST_NO_AUTORESIZE)
        self.lblProcess.SetMinSize(wx.Size(-1, -1))

        self.lblAppl = wx.StaticText(id=wxID_VSPLASHFRAMELBLAPPL,
              label=_(u'Document'), name=u'lblAppl', parent=self.pnMain,
              pos=wx.Point(16, 74), size=wx.Size(113, 25),
              style=wx.ALIGN_CENTRE)
        self.lblAppl.SetFont(wx.Font(16, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              u'Swis721 Blk BT'))
        self.lblAppl.SetMinSize(wx.Size(-1, -1))
        self.lblAppl.SetForegroundColour(wx.Colour(46, 138, 87))

        self.lBitmap = wx.StaticBitmap(bitmap=wx.EmptyBitmap(400, 200),
              id=wxID_VSPLASHFRAMELBITMAP, name=u'lBitmap', parent=self.pnMain,
              pos=wx.Point(0, 107), size=wx.Size(400, 200), style=0)
        self.lBitmap.Bind(wx.EVT_LEFT_DOWN, self.OnLBitmapLeftDown)

        self.lblCompany = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VSPLASHFRAMELBLCOMPANY, name=u'lblCompany',
              parent=self.pnMain, pos=wx.Point(72, 0), size=wx.Size(255, 49),
              style=0)

        self.lblPrg = wx.StaticText(id=wxID_VSPLASHFRAMELBLPRG, label=u'VIDARC',
              name=u'lblPrg', parent=self.pnMain, pos=wx.Point(16, 49),
              size=wx.Size(368, 25), style=wx.ALIGN_CENTRE)
        self.lblPrg.SetFont(wx.Font(16, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              u'Swis721 Blk BT'))
        self.lblPrg.SetAutoLayout(True)
        self.lblPrg.SetMinSize(wx.Size(-1, -1))
        self.lblPrg.SetForegroundColour(wx.Colour(46, 138, 87))

        self.cbLaunch = wx.lib.buttons.GenBitmapButton(ID=wxID_VSPLASHFRAMECBLAUNCH,
              bitmap=wx.EmptyBitmap(32, 32), name=u'cbLaunch',
              parent=self.pnMain, pos=wx.Point(350, 389), size=wx.Size(46, 46),
              style=0)
        self.cbLaunch.Enable(False)
        self.cbLaunch.Bind(wx.EVT_BUTTON, self.OnCbLaunchButton,
              id=wxID_VSPLASHFRAMECBLAUNCH)

        self.lblApplLogName = wx.StaticText(id=wxID_VSPLASHFRAMELBLAPPLLOGNAME,
              label=_(u'Log Name'), name=u'lblApplLogName', parent=self.pnMain,
              pos=wx.Point(0, 307), size=wx.Size(118, 22),
              style=wx.ALIGN_RIGHT)
        self.lblApplLogName.Show(False)
        self.lblApplLogName.SetMinSize(wx.Size(-1, -1))

        self.cbCancel = wx.lib.buttons.GenBitmapButton(ID=wxID_VSPLASHFRAMECBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbCancel',
              parent=self.pnMain, pos=wx.Point(364, 351), size=wx.Size(31, 30),
              style=0)
        self.cbCancel.Show(False)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VSPLASHFRAMECBCANCEL)

        self.txtLogName = wx.TextCtrl(id=wxID_VSPLASHFRAMETXTLOGNAME,
              name=u'txtLogName', parent=self.pnMain, pos=wx.Point(122, 307),
              size=wx.Size(110, 22), style=0, value=u'')
        self.txtLogName.Show(False)
        self.txtLogName.SetMinSize(wx.Size(-1, -1))

        self.snbLogName = wx.SpinButton(id=wxID_VSPLASHFRAMESNBLOGNAME,
              name=u'snbLogName', parent=self.pnMain, pos=wx.Point(236, 307),
              size=wx.Size(45, 22), style=wx.SP_HORIZONTAL)
        self.snbLogName.Show(False)
        self.snbLogName.Bind(wx.EVT_SPIN_DOWN, self.OnSnbLogNameSpinDown,
              id=wxID_VSPLASHFRAMESNBLOGNAME)
        self.snbLogName.Bind(wx.EVT_SPIN_UP, self.OnSnbLogNameSpinUp,
              id=wxID_VSPLASHFRAMESNBLOGNAME)

        self.chcLogLevel = wx.Choice(choices=[u'debug', u'information',
              u'warning', u'error', u'critical', u'fatal'],
              id=wxID_VSPLASHFRAMECHCLOGLEVEL, name=u'chcLogLevel',
              parent=self.pnMain, pos=wx.Point(289, 307), size=wx.Size(110, 21),
              style=0)
        self.chcLogLevel.Show(False)
        self.chcLogLevel.SetMinSize(wx.Size(-1, -1))

        self.txtPort = wx.TextCtrl(id=wxID_VSPLASHFRAMETXTPORT, name=u'txtPort',
              parent=self.pnMain, pos=wx.Point(122, 329), size=wx.Size(110, 22),
              style=0, value=u'')
        self.txtPort.Show(False)
        self.txtPort.SetMinSize(wx.Size(-1, -1))

        self.lblPort = wx.StaticText(id=wxID_VSPLASHFRAMELBLPORT,
              label=_(u'Port'), name=u'lblPort', parent=self.pnMain,
              pos=wx.Point(0, 329), size=wx.Size(118, 22),
              style=wx.ALIGN_RIGHT)
        self.lblPort.Show(False)
        self.lblPort.SetMinSize(wx.Size(-1, -1))

        self.snbPort = wx.SpinButton(id=wxID_VSPLASHFRAMESNBPORT,
              name=u'snbPort', parent=self.pnMain, pos=wx.Point(236, 329),
              size=wx.Size(45, 22), style=wx.SP_HORIZONTAL)
        self.snbPort.Show(False)
        self.snbPort.Bind(wx.EVT_SPIN_DOWN, self.OnSnbPortSpinDown,
              id=wxID_VSPLASHFRAMESNBPORT)
        self.snbPort.Bind(wx.EVT_SPIN_UP, self.OnSnbPortSpinUp,
              id=wxID_VSPLASHFRAMESNBPORT)

        self.cbApply = wx.lib.buttons.GenBitmapButton(ID=wxID_VSPLASHFRAMECBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbApply',
              parent=self.pnMain, pos=wx.Point(325, 351), size=wx.Size(31, 30),
              style=0)
        self.cbApply.Show(False)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VSPLASHFRAMECBAPPLY)

        self.lblDummy = wx.StaticText(id=wxID_VSPLASHFRAMELBLDUMMY, label=u'',
              name=u'lblDummy', parent=self.pnMain, pos=wx.Point(281, 329),
              size=wx.Size(118, 22), style=0)

        self._init_sizers()

    def __init__(self, parent,sPrg,sAppl,img,actionList,actionListPost,
                sApplName,iLogLv,iSockPort,
                autoLaunch=10):
        self._init_ctrls(parent)
        self.SetTitle(u'VIDARC loader %s'%sApplName)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        self.cbLaunch.SetBitmapLabel(getLaunchBitmap())
        self.cbApply.SetBitmapLabel(getApplyBitmap())
        self.cbCancel.SetBitmapLabel(getCancelBitmap())
        self.lblCompany.SetBitmap(getLogoBitmap())
        self.lblPrg.SetLabel(sPrg)
        self.lblAppl.SetLabel(sAppl)
        if img is not None:
            self.lBitmap.SetBitmap(img)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
        self.Centre()
        
        self.gagProcess.SetRange(len(actionList)+len(actionListPost))
        self.res=[]
        self.actionList=actionList
        self.actionListPost=actionListPost
        self.iAction=-2
        self.iActionPost=-2
        self.bActivated=False
        self.sLogName=sApplName
        self.iLogName=-1
        self.SetLogLv(iLogLv)
        self.txtLogName.SetValue(sApplName)
        self.txtPort.SetValue(str(iSockPort))
        self.zAutoLaunch=autoLaunch
    def DoAction(self):
        try:
            if self.iAction>=-1:
                self.iAction=self.iAction+1
                if self.iAction<len(self.actionList):
                    self.gagProcess.SetValue(self.iAction+1)
                    dAction=self.actionList[self.iAction]
                    self.lblProcess.SetLabel(dAction['label'])
                    wx.PostEvent(self,vgaSplashAction())
                else:
                    #wx.PostEvent(self,vgaSplashFinished())
                    if self.zAutoLaunch>0:
                        self.timer = wx.PyTimer(self.Notify)
                        self.timer.Start(1000)
                        self.zAuto=0
                        self.Notify()
                        self.gagProcess.SetValue(0)
                        self.gagProcess.SetRange(self.zAutoLaunch)
                    elif self.zAutoLaunch==0:
                        self.__postFinished__()
                        return
                    else:
                        self.gagProcess.Show(False)
                    self.cbLaunch.Enable(True)
            elif self.iActionPost>=-1:
                self.iActionPost=self.iActionPost+1
                if self.iActionPost<len(self.actionListPost):
                    self.gagProcess.SetValue(self.iActionPost+1)
                    dAction=self.actionListPost[self.iActionPost]
                    self.lblProcess.SetLabel(dAction['label'])
                    wx.PostEvent(self,vgaSplashAction())
                else:
                    wx.PostEvent(self,vgaSplashFinished())
        except:
            vLogFallBack.logTB(__name__)
    def DoProcess(self):
        try:
            if self.iAction>=0:
                if self.iAction<len(self.actionList):
                    dAction=self.actionList[self.iAction]
                    if dAction.has_key('eval'):
                        self.res.append(eval(dAction['eval']))
                    else:
                        self.res.append('')
                    wx.PostEvent(self,vgaSplashProcessed(self.iAction))
            elif self.iActionPost>=0:
                if self.iActionPost<len(self.actionListPost):
                    dAction=self.actionListPost[self.iActionPost]
                    if dAction.has_key('eval'):
                        self.res.append(eval(dAction['eval']))
                    else:
                        self.res.append('')
                    wx.PostEvent(self,vgaSplashProcessed(self.iActionPost))
        except:
            vLogFallBack.logTB(__name__)
    def Notify(self):
        self.zAuto+=1
        if self.zAutoLaunch<0:
            self.timer.Stop()
            return
        if self.zAuto>self.zAutoLaunch:
            self.__postFinished__()
        else:
            self.gagProcess.SetValue(self.zAuto)
    def OnVgfSplashActivate(self, event):
        event.Skip()

    def OnVgfSplashIdle(self, event):
        event.Skip()
        if self.iAction==-2:
            if self.lBitmap.IsShown():
                if self.GetSockPort() is None:
                    self.__showBasicSettings__()
                    self.lblProcess.SetLabel(_(u'Please setup logging port (%s).')%'Port')
                    return
                try:
                    sLogName=self.txtLogName.GetValue()
                    import vidarc.tool.log.vtLog as vtLog
                    iRes=vtLog.vtLngInit(sLogName,sLogName+'.log',
                            self.GetLogLv(),
                            iSockPort=self.GetSockPort())
                    if iRes==-1:
                        self.__showBasicSettings__()
                        self.lblProcess.SetLabel(_(u'Please change logging name (%s).')%'Log Name')
                        return
                    if iRes==-2:
                        self.__showBasicSettings__()
                        self.lblProcess.SetLabel(_(u'Please change logging port (%s).')%'Port')
                        return
                except:
                    traceback.print_exc()
                    vLogFallBack.logTB(__name__)
                    return
            else:
                return
            self.iAction=-1
            wx.PostEvent(self,vgaSplashProcessed(-1))
        elif self.iAction==-3 and self.iActionPost==-2:
            self.iActionPost=-1
            wx.PostEvent(self,vgaSplashProcessed(-1))
        
    def __postFinished__(self):
        try:
            self.gagProcess.SetValue(0)
            self.gagProcess.Show(False)
            self.timer.Stop()
        except:
            pass
        if 0:
            if self.GetSockPort() is None:
                self.__showBasicSettings__()
                self.lblProcess.SetLabel(_(u'Please setup logging port (%s).')%'Port')
                return
            try:
                sLogName=self.txtLogName.GetValue()
                iRes=vtLog.vtLngInit(sLogName,sLogName+'.log',
                        self.GetLogLv(),
                        iSockPort=self.GetSockPort())
                if iRes==-1:
                    self.__showBasicSettings__()
                    self.lblProcess.SetLabel(_(u'Please change logging name (%s).')%'Log Name')
                    return
                if iRes==-2:
                    self.__showBasicSettings__()
                    self.lblProcess.SetLabel(_(u'Please change logging port (%s).')%'Port')
                    return
            except:
                traceback.print_exc()
                vLogFallBack.logTB(__name__)
                return
        try:
            if len(self.actionListPost)>0:
                self.iAction=-3
                self.cbLaunch.Enable(False)
                #self.iActionPost=-1
                return
        except:
            pass
        wx.PostEvent(self,vgaSplashFinished())
         
    def OnCbLaunchButton(self, event):
        self.__postFinished__()
        event.Skip()

    def OnLBitmapLeftDown(self, event):
        self.zAutoLaunch=-1
        self.gagProcess.Show(False)
        self.__showBasicSettings__()
        event.Skip()
    
    def __showBasicSettings__(self,flag=True):
        self.lBitmap.Show(not flag)
        self.lblPort.Show(flag)
        self.txtPort.Show(flag)
        self.chcLogLevel.Show(flag)
        self.txtLogName.Show(flag)
        self.lblApplLogName.Show(flag)
        self.cbCancel.Show(flag)
        self.cbApply.Show(flag)
        self.snbPort.Show(flag)
        self.snbLogName.Show(flag)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
        
    def GetLogLv(self):
        iSel=self.chcLogLevel.GetSelection()
        if iSel==0:
            iLogLv=vtLogDef.DEBUG
        elif iSel==1:
            iLogLv=vtLogDef.INFO
        elif iSel==2:
            iLogLv=vtLogDef.WARN
        elif iSel==3:
            iLogLv=vtLogDef.ERROR
        elif iSel==4:
            iLogLv=vtLogDef.CRITICAL
        elif iSel==5:
            iLogLv=vtLogDef.FATAL
        return iLogLv
    def SetLogLv(self,iLogLv):
        if iLogLv==vtLogDef.DEBUG:
            iSel=0
        elif iLogLv==vtLogDef.INFO:
            iSel=1
        elif iLogLv==vtLogDef.WARN:
            iSel=2
        elif iLogLv==vtLogDef.ERROR:
            iSel=3
        elif iLogLv==vtLogDef.CRITICAL:
            iSel=4
        elif iLogLv==vtLogDef.FATAL:
            iSel=5

        self.chcLogLevel.SetSelection(iSel)
        
    def GetSockPort(self):
        try:
            return int(self.txtPort.GetValue())
        except:
            return None

    def OnCbCancelButton(self, event):
        wx.PostEvent(self,vgaSplashAborted())
        event.Skip()

    def OnSnbPortSpinDown(self, event):
        iPort=self.GetSockPort()
        if iPort is not None:
            self.txtPort.SetValue(str(iPort-1))
        event.Skip()

    def OnSnbPortSpinUp(self, event):
        iPort=self.GetSockPort()
        if iPort is not None:
            self.txtPort.SetValue(str(iPort+1))
        event.Skip()
        
    def OnSnbLogNameSpinDown(self, event):
        self.iLogName-=1
        if self.iLogName<-1:
            self.iLogName=-1
        if self.iLogName<0:
            self.txtLogName.SetValue(self.sLogName)
        else:
            self.txtLogName.SetValue(self.sLogName+str(self.iLogName))
        event.Skip()

    def OnSnbLogNameSpinUp(self, event):
        self.iLogName+=1
        if self.iLogName<0:
            self.iLogName=0
        self.txtLogName.SetValue(self.sLogName+str(self.iLogName))
        event.Skip()

    def OnCbApplyButton(self, event):
        event.Skip()
        self.__showBasicSettings__(False)
        
