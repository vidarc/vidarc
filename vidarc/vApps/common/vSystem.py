#----------------------------------------------------------------------------
# Name:         vSystem.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060219
# CVS-ID:       $Id: vSystem.py,v 1.6 2009/11/03 21:35:37 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os
import sys

gUsrCfgDN=None

def LimitWindowToScreen(iX,iY,iWidth,iHeight):
    #try:
        iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
        iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
        iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
        iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
        #iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_WINDOWMIN_X)
        #iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_WINDOWMIN_Y)
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<0:
            iX=0
        if iY<0:
            iY=0
        if iX+iWidth>iMaxW:
            iX=iMaxW-iWidth
        if iY+iHeight>iMaxH:
            iY=iMaxH-iHeight
        if iX<0:
            iX=0
        if iY<0:
            iY=0
        if iX+iWidth>iMaxW:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    #except:
    #    iX=iY=0
    #    iWidth=100
    #    iHeight=100
        return iX,iY,iWidth,iHeight

def ArrangeWidget(widBase,widSub,how):
    """
    how = 0     ... bottom left of widBase
    """
    iBaseX,iBaseY = widBase.ClientToScreen( (0,0) )
    iBaseW,iBaseH =  widBase.GetSize()
    iSubX,iSubY=widSub.GetPosition()
    iSubW,iSubH=widSub.GetSize()
    if how==0:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    elif how==10:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iBaseH)
    else:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    #print 'base',iBaseX,iBaseY,iBaseW,iBaseH
    #print 'sub ',iSubX,iSubY,iSubW,iSubH
    #print 'res',iX,iY,iW,iH
    widSub.Move((iX,iY))
    widSub.SetSize((iW,iH))

def getDefaultLocalDN():
    try:
        sDN=getUsrCfgDN('VIDARC')
        return sDN
    except:
        pass
    return os.path.join(os.path.expanduser('~'),'VIDARC')

def setUsrCfgDN(sDN):
    global gUsrCfgDN
    gUsrCfgDN=sDN

def getUsrCfgDN(sAppl=None):
    global gUsrCfgDN
    if gUsrCfgDN is None:
        try:
            if sys.platform.startswith('win'):
                sCfgDN=os.getenv('APPDATA')
            else:
                sCfgDN=None
        except:
            sCfgDN=None
        if sCfgDN is None:
            sCfgDN=os.path.join(os.path.expanduser('~'),'VIDARC')
    else:
        sCfgDN=gUsrCfgDN
    if sAppl is not None:
        sDN=os.path.join(sCfgDN,sAppl)
        try:
            os.makedirs(sDN)
        except:
            pass
        return sDN
    return sCfgDN

def getPlugInDN():
    return getUsrCfgDN('VIDARCplugins')

def getPlugInLocaleDN():
    return os.path.join(getPlugInDN(),'locale')

def getPlugInHelpDN():
    return os.path.join(getPlugInDN(),'help')

def getLogDN():
    sDN=os.getenv('vidarcLogDN',None)
    if sDN is None:
        return getUsrCfgDN('VIDARClog')
    else:
        try:
            os.makedirs(sDN)
        except:
            pass
    return sDN

def getErrFN(sAppl):
    sDN=getLogDN()
    sLogFN=os.path.join(sDN,u'.'.join([sAppl,'err.log']))
    return sLogFN

def getHost():
    return wx.GetHostName()
    
def getHostFull():
    return wx.GetFullHostName()
