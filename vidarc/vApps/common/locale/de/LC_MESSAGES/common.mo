��    +      t  ;   �      �  	   �     �     �     �     �     �     �     �          
          #     (     ,  R   /     �     �     �     �  /   �      �  "        9  !   W  &   y     �     �  "   �     �     �       
   '     2     C     P     a  #   t     �  "   �     �     �     �  �  �  
   �     �     �     �  
   �     �     �     �  
   �     �     �     	     	     	  Z   	     p	     �	     �	     �	  (   �	  $   �	  .   
     >
     Z
  %   v
     �
     �
  +   �
     �
          +     K     T     e     s     �  /   �     �  *   �     �          &                    (           +          &                   '      %          $            *      
       )   	                                                  #   !                        "                 (undef*) %b-%d-%Y   %H:%M:%S wk:%W &Analyse &File &Tools &View ?	F1 About Close E&xit	ALT+X Error Report Help Log Ok Please send file
%s
to us (mailto:office@vidarc.com).

Do you want to send it now? Request Lock	F4 Screenshot full	F11 Screenshot plugin	F12 Settings Unsaved data present! Do you want to save data? VIDARC Error Report Notification add response, permission denied %s application status collected. collecting application status ... connection established ... please wait connection lost. content got. generate ... del response, permission denied %s file open fault. file opened ... file opened and parsed. generated. get content ...  log filename master finished. master started ... move response, permission denied %s port set response, permission denied %s synch aborted. synch finished. synch started ... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-05-30 16:07+0200
PO-Revision-Date: 2005-09-20 14:49+0100
Last-Translator: Walter Obweger <walter.obweger@vidarc.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
  (undef *) %d-%b-%Y   %H:%M:%S KW:%W &Analyse &Datei &Werkzeuge &Ansicht ?	F1 �ber Schliessen B&enden	ALT+X Fehlerreport Hilfe Log Ok Bitte senden Sie die Datei
%s
zu uns (mailto:office@vidarc.com).

Wollen Sie jetzt senden? Speere anfordern Lock	F4 Screenshot voll	F11 Screenshot plugin	F12 Einstellungen Nicht alle Daten gespeichert! Speichern? VIDARC Fehlerreport Benachrichtigung Antwort Hinzuf�gen, Berechtigung verweigert %s Anwendungsstatus gesammelt. sammle Anwendungsstatus ... Verbindung aufgebaut ... bitte warten Verbindung verloren. Inhalt erhalten. Erzeuge ... Antwort L�schen, Berechtigung verweigert %s Fehler beim �ffnen der Datei. Datei ge�ffnet ... Datei ge�ffnet und ausgewertet. erzeugt. hole Inhalt ...  Log Dateiname Master abgeschlossen. Master gestarted ... Antwort verschieben, Berechtigung verweigert %s Port Antwort Setzen, Berechtigung verweigert %s Synch abgebrochen. Synch abgeschlossen. Synch gestartet ... 