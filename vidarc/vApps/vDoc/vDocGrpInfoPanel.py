#Boa:FramePanel:vDocGrpInfoPanel
#----------------------------------------------------------------------------
# Name:         vDocGrpInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocGrpInfoPanel.py,v 1.9 2007/08/21 18:11:59 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.grid
import wx.lib.buttons
import cStringIO
import string,sys,os.path
import vidarc.tool.InOut.fnUtil as fnUtil
#import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGuiEvents import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
#from vidarc.vApps.vDoc.vNetDoc import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import images

[wxID_VDOCGRPINFOPANEL, wxID_VDOCGRPINFOPANELGCBBADD, 
 wxID_VDOCGRPINFOPANELGCBBBROWSEFN, wxID_VDOCGRPINFOPANELGCBBDEL, 
 wxID_VDOCGRPINFOPANELLBLDOCTITLE, wxID_VDOCGRPINFOPANELLBLFILENAME, 
 wxID_VDOCGRPINFOPANELLBLPRJDESC, wxID_VDOCGRPINFOPANELLBLTITLE, 
 wxID_VDOCGRPINFOPANELLSTDOCS, wxID_VDOCGRPINFOPANELTXTDESC, 
 wxID_VDOCGRPINFOPANELTXTDOCTITLE, wxID_VDOCGRPINFOPANELTXTFILENAME, 
 wxID_VDOCGRPINFOPANELTXTTITLE, 
] = [wx.NewId() for _init_ctrls in range(13)]

# defined event for vgpXmlTree item selected
wxEVT_VGPDOCUMENT_SELECTED=wx.NewEventType()
def EVT_VGPDOCUMENT_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOCUMENT_SELECTED,func)
class vgpDocumentSelected(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOCUMENT_SELECTED(<widget_name>, self.OnDocumentSelected)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOCUMENT_SELECTED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_CHANGED=wx.NewEventType()
def EVT_VGPDOC_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_CHANGED,func)
class vgpDocChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOC_CHANGED(<widget_name>, self.OnDocChanged)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_GRP_INFO_CHANGED=wx.NewEventType()
def EVT_VGPDOC_GRP_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_GRP_INFO_CHANGED,func)
class vgpDocGrpInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOC_GRP_INFO_CHANGED(<widget_name>, self.OnDocGrpInfoChanged)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_GRP_INFO_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_GRP_INFO_CANCELED=wx.NewEventType()
def EVT_VGPDOC_GRP_INFO_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_GRP_INFO_CANCELED,func)
class vgpDocGrpInfoCanceled(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOC_GRP_INFO_CANCELED(<widget_name>, self.OnDocGrpInfoCanceled)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_GRP_INFO_CANCELED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_GRP_INFO_DELETED=wx.NewEventType()
def EVT_VGPDOC_GRP_INFO_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_GRP_INFO_DELETED,func)
class vgpDocGrpInfoDeleted(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOC_GRP_INFO_DELETED(<widget_name>, self.OnDocGrpInfoDeleted)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_GRP_INFO_DELETED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_GRP_INFO_ADDED=wx.NewEventType()
def EVT_VGPDOC_GRP_INFO_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_GRP_INFO_ADDED,func)
class vgpDocGrpInfoAdded(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOC_GRP_INFO_ADDED(<widget_name>, self.OnDocGrpInfoAdded)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_GRP_INFO_ADDED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang


class vDocGrpInfoPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsTmplInputFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilename, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFilename, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsTmpl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstDocs, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsTmplBt, 0, border=0, flag=0)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtTitle, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsTmplInput_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTmplInputFN, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbBrowseFN, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTitle, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsTmplInputTitle, 0, border=4,
              flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsTmplInput, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsTmpl, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 3, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsTmplBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbDel, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsTmplInputTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDocTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDocTitle, 3, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=4)

        self.bxsTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplInputTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplInputFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsTmpl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTitle_Items(self.bxsTitle)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_bxsTmplInput_Items(self.bxsTmplInput)
        self._init_coll_bxsTmplInputTitle_Items(self.bxsTmplInputTitle)
        self._init_coll_bxsTmplInputFN_Items(self.bxsTmplInputFN)
        self._init_coll_bxsTmplBt_Items(self.bxsTmplBt)
        self._init_coll_bxsTmpl_Items(self.bxsTmpl)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCGRPINFOPANEL,
              name=u'vDocGrpInfoPanel', parent=prnt, pos=wx.Point(124, 23),
              size=wx.Size(302, 225), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(294, 198))

        self.lblTitle = wx.StaticText(id=wxID_VDOCGRPINFOPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self, pos=wx.Point(0,
              4), size=wx.Size(73, 21), style=wx.ALIGN_RIGHT)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.txtTitle = wx.TextCtrl(id=wxID_VDOCGRPINFOPANELTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(77, 4),
              size=wx.Size(216, 21), style=0, value=u'')
        self.txtTitle.SetMinSize(wx.Size(-1, -1))

        self.lblPrjDesc = wx.StaticText(id=wxID_VDOCGRPINFOPANELLBLPRJDESC,
              label=_(u'Description'), name=u'lblPrjDesc', parent=self,
              pos=wx.Point(0, 29), size=wx.Size(73, 34), style=wx.ALIGN_RIGHT)
        self.lblPrjDesc.SetMinSize(wx.Size(-1, -1))

        self.txtDesc = wx.TextCtrl(id=wxID_VDOCGRPINFOPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(77, 29),
              size=wx.Size(216, 34), style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetExtraStyle(0)
        self.txtDesc.SetMinSize(wx.Size(-1, -1))

        self.lstDocs = wx.ListCtrl(id=wxID_VDOCGRPINFOPANELLSTDOCS,
              name=u'lstDocs', parent=self, pos=wx.Point(0, 134),
              size=wx.Size(214, 64), style=wx.LC_REPORT)
        self.lstDocs.SetMinSize(wx.Size(-1, -1))
        self.lstDocs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VDOCGRPINFOPANELLSTDOCS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCGRPINFOPANELGCBBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'),
              name=u'gcbbAdd', parent=self, pos=wx.Point(218, 134),
              size=wx.Size(76, 30), style=0)
        self.gcbbAdd.SetMinSize(wx.Size(-1, -1))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VDOCGRPINFOPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCGRPINFOPANELGCBBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'),
              name=u'gcbbDel', parent=self, pos=wx.Point(218, 168),
              size=wx.Size(76, 30), style=0)
        self.gcbbDel.SetMinSize(wx.Size(-1, -1))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VDOCGRPINFOPANELGCBBDEL)

        self.lblDocTitle = wx.StaticText(id=wxID_VDOCGRPINFOPANELLBLDOCTITLE,
              label=_(u'Doc Title'), name=u'lblDocTitle', parent=self,
              pos=wx.Point(0, 71), size=wx.Size(73, 21), style=wx.ALIGN_RIGHT)
        self.lblDocTitle.SetMinSize(wx.Size(-1, -1))
        self.lblDocTitle.SetMaxSize(wx.Size(20, -1))

        self.txtDocTitle = wx.TextCtrl(id=wxID_VDOCGRPINFOPANELTXTDOCTITLE,
              name=u'txtDocTitle', parent=self, pos=wx.Point(77, 71),
              size=wx.Size(216, 21), style=0, value=u'')
        self.txtDocTitle.SetMinSize(wx.Size(-1, -1))

        self.lblFilename = wx.StaticText(id=wxID_VDOCGRPINFOPANELLBLFILENAME,
              label=_(u'Filename'), name=u'lblFilename', parent=self,
              pos=wx.Point(0, 96), size=wx.Size(72, 30), style=wx.ALIGN_RIGHT)
        self.lblFilename.SetMinSize(wx.Size(-1, -1))

        self.txtFilename = wx.TextCtrl(id=wxID_VDOCGRPINFOPANELTXTFILENAME,
              name=u'txtFilename', parent=self, pos=wx.Point(76, 96),
              size=wx.Size(141, 30), style=0, value=u'')
        self.txtFilename.SetMinSize(wx.Size(-1, -1))

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCGRPINFOPANELGCBBBROWSEFN,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=u'...',
              name=u'gcbbBrowseFN', parent=self, pos=wx.Point(218, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseFN.SetMinSize(wx.Size(-1, -1))
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VDOCGRPINFOPANELGCBBBROWSEFN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.language=None
        self.tmplDN=None
        
        self.selectedIdx=-1
        self.docs=[]
        
        # setup list
        self.lstDocs.InsertColumn(0,_(u'Doc'))
        self.lstDocs.InsertColumn(1,_(u'Filename'),wx.LIST_FORMAT_LEFT,270)
        self.SetupImageList()
        #self.Move(pos)
        #self.SetSize(size)
        #self.fgsData.Layout()
        #self.fgsData.FitInside(self)
    def SetTemplateBaseDir(self,dn):
        self.tmplDN=dn
    def SetLanguage(self,lang):
        self.lang=lang
    def GetLanguage(self):
        return self.lang
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        self.selectedIdx=-1
        self.docs=[]
        self.lstDocs.DeleteAllItems()
        self.txtTitle.SetValue('')
        self.txtDesc.SetValue('')
        self.node=None
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet=bNet)
        if doc is None:
            return
        # FIXME if called more than once
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            #EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        
    def SetNode(self,node):
        self.Clear()
        if node is None:
            self.Lock(True)
            return
        if self.doc is None:
            return
        self.node=node
        
        #sKey=vtXmlDomTree.getNodeText(node,'key')
        #sPath=vtXmlDomTree.getNodeText(node,'path')
        sTitle=self.doc.getNodeTextLang(node,'title',self.lang)
        sDesc=self.doc.getNodeTextLang(node,'description',self.lang)
        
        #self.txtPath.SetValue(sPath)
        self.txtTitle.SetValue(sTitle)
        self.txtDesc.SetValue(sDesc)
        self.selectedIdx=-1
        
        self.RefreshDocuments()
    def RefreshDocuments(self):
        # setup attributes
        self.lstDocs.DeleteAllItems()
        
        docNodes=self.doc.getChilds(self.node,'document')
        self.docs=[]
        for docNode in docNodes:
            l=self.doc.getAttribute(docNode,'language')
            bLang=False
            if len(l)<=0 and self.lang=='en':
                bLang=True
            elif self.lang==l:
                bLang=True
            if bLang==True:
                d={}
                for sAttr in ['title','type','filename','author','version','date']:
                    sVal=self.doc.getNodeText(docNode,sAttr)
                    d[sAttr]=sVal
                d['document']=docNode
                self.docs.append(d)
        i=0
        for doc in self.docs:
            #keys=doc.keys()
            #keys.sort()
            index = self.lstDocs.InsertImageStringItem(sys.maxint, doc['title'], -1)
            sFileName=fnUtil.getFilenameRel2BaseDir(doc['filename'],self.tmplDN)
            self.lstDocs.SetStringItem(index, 1, sFileName)
            self.lstDocs.SetItemData(index,i)
            if i==self.selectedIdx:
                self.lstDocs.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            i=i+1
            #for k in keys:
            #    try:
            #        imgId=self.imgDict[k]
            #    except:
            #        imgId=-1
            #    index = self.lstDocs.InsertImageStringItem(sys.maxint, k, imgId)
            #    self.lstDocs.SetStringItem(index, 1, docs[k])
        pass
    def GetNode(self,node):
        if node is None:
            return ''
        #sPath=self.txtPath.GetValue()
        sTitle=self.txtTitle.GetValue()
        sDesc=self.txtDesc.GetValue()
        self.doc.setNodeTextLang(node,'title',sTitle,self.lang)
        self.doc.setNodeTextLang(node,'description',sDesc,self.lang)
        self.doc.AlignNode(node,iRec=2)
        return sTitle
    def CreateNode(self):
        pass
    def GetSelectedDoc(self):
        if self.selectedIdx>=0:
            return self.docs[self.selectedIdx]
        else:
            return None
    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        doc=self.docs[idx]
        wx.PostEvent(self,vgpDocumentSelected(self,doc,self.lang))
        
        it=self.lstDocs.GetItem(idx,0)
        self.txtDocTitle.SetValue(it.m_text)
        self.selectedIdx=it.m_itemId
        it=self.lstDocs.GetItem(idx,1)
        self.txtFilename.SetValue(it.m_text)
        event.Skip()

    def OnGcbbAddButton(self, event):
        sDoc=self.txtDocTitle.GetValue()
        sFN=self.txtFilename.GetValue()
        if len(sDoc)<=0:
            return
        index = self.lstDocs.InsertImageStringItem(sys.maxint, sDoc, -1)
        self.lstDocs.SetStringItem(index, 1, sFN)
        
        docNode=self.doc.createSubNode(self.node,'document')
        self.doc.setAttribute(docNode,'language',self.lang)
        d={}
        for sAttr in ['title','type','filename','author','version','date']:
            sVal=''
            if sAttr=='title':
                sVal=sDoc
            elif sAttr=='filename':
                sVal=sFN
            self.doc.setNodeText(docNode,sAttr,sVal)
            d[sAttr]=sVal
        d['document']=docNode
        self.docs.append(d)
        self.txtDocTitle.SetValue('')
        self.txtFilename.SetValue('')
        wx.PostEvent(self,vgpDocumentSelected(self,None,self.lang))
        wx.PostEvent(self,vgpDocChanged(self,None,self.lang))
        #wx.PostEvent(self,vgpDocGrpInfoChanged(self,self.node,self.lang))
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedIdx>=0:
            self.lstDocs.DeleteItem(self.selectedIdx)
            d=self.docs[self.selectedIdx]['document']
            self.docs=self.docs[:self.selectedIdx]+self.docs[self.selectedIdx+1:]
            self.selectedIdx=-1
            self.txtDocTitle.SetValue('')
            self.txtFilename.SetValue('')
            
            parDoc=d.parentNode
            parDoc.removeChild(d)
            wx.PostEvent(self,vgpDocumentSelected(self,None,self.lang))
            wx.PostEvent(self,vgpDocChanged(self,None,self.lang))
            #wx.PostEvent(self,vgpDocGrpInfoChanged(self,self.node,self.lang))
        
        event.Skip()

    def OnGcbbApplyButton(self, event):
        wx.PostEvent(self,vgpDocGrpInfoChanged(self,self.node,None))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        wx.PostEvent(self,vgpDocGrpInfoCanceled(self,self.node,None))
        event.Skip()
    def OnGcbbBowsePathButton(self, event):
        event.Skip()

    def OnGcbbBrowseFNButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "all files (*.*)|*.*|XML files (*.xml)|*.xml", wx.OPEN)
        try:
            fn=self.txtFilename.GetValue()
            fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if fn is not None:
                dlg.SetFilename(fn)
            else:
                dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtFilename.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def Lock(self,flag,locker=''):
        if flag==False:
            self.txtTitle.Enable(True)
            self.txtDesc.Enable(True)
            self.txtDocTitle.Enable(True)
            self.txtFilename.Enable(True)
            self.gcbbBrowseFN.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDel.Enable(True)
        else:
            self.txtTitle.Enable(False)
            self.txtDesc.Enable(False)
            self.txtDocTitle.Enable(False)
            self.txtFilename.Enable(False)
            self.gcbbBrowseFN.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDel.Enable(False)
