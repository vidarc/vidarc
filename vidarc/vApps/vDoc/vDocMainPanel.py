#Boa:FramePanel:vDocMainPanel
#----------------------------------------------------------------------------
# Name:         vDocMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocMainPanel.py,v 1.9 2007/10/28 16:29:55 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors
import getpass,time

import vidarc.tool.log.vtLog as vtLog
try:
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.vApps.vDoc.vXmlDocTree  import *
    from vidarc.vApps.vDoc.vDocGrpInfoNoteBook  import *
    from vidarc.vApps.vDoc.vDocSettingsDialog  import *
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    import vidarc.tool.InOut.fnUtil as fnUtil
    import vidarc.tool.art.vtArt as vtArt
    
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND

    import images
    import images_lang
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vDocMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wx.NewId(), range(4))

[wxID_VDOCMAINPANEL, wxID_VDOCMAINPANELPNDATA, wxID_VDOCMAINPANELSLWNAV, 
 wxID_VDOCMAINPANELSLWTOP, wxID_VDOCMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(5)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

class vDocMainPanel(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCMAINPANEL, name=u'vDocMainFrame',
              parent=prnt, pos=wx.Point(115, 10), size=wx.Size(819, 508),
              style=wx.DEFAULT_FRAME_STYLE)
        self.SetClientSize(wx.Size(811, 481))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VDOCMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              480), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VDOCMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VDOCMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(u'information')
        self.slwTop.SetToolTipString(u'information')
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VDOCMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VDOCMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 216), size=wx.Size(592, 224),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)
        self.pnData.SetMinSize(wx.Size(-1, -1))

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VDOCMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.slwTop, pos=wx.Point(4, 4),
              size=wx.Size(596, 188), style=0)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_SELECTED,
              self.OnViRegSelToolXmlRegSelectorSelected,
              id=wxID_VDOCMAINPANELVIREGSEL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_CANCEL,
              self.OnViRegSelToolXmlRegSelectorCancel,
              id=wxID_VDOCMAINPANELVIREGSEL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_APPLY,
              self.OnViRegSelToolXmlRegSelectorApply,
              id=wxID_VDOCMAINPANELVIREGSEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.bActivated=False
        self.SetName(name)
        self.dCfg={}
        
        self.bModified=False
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.node=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='vDocCfg',audit_trail=False)
        
        self.langIds=[]
        self.languages=[u'English',u'Deutsch']
        self.languageIds=[u'en',u'de']
        
        appls=['vDoc','vHum']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0,
                                        audit_trail=True)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vgaDoc')
        
        self.bAutoConnect=True
        self.OpenCfgFile()
        
        self.vgpTree=vXmlDocTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(180,460),style=0,name="vgpTree",
                master=True,verbose=0)
        self.vgpTree.EnableCache()
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netDoc,True)
        #EVT_VGTRXMLDOC_GROUPING_CHANGED(self.vgpTree,self.OnDocGrpChanged)
        
        self.vglbDocGrpInfo=vDocGrpInfoNoteBook(self.viRegSel,wx.NewId(),
                    pos=(4,0),size=(458, 192),style=0,name="vDocGrpInfoNoteBook")
        #self.vglbDocGrpInfo.SetConstraints(
        #        anchors.LayoutAnchors(self.vglbDocGrpInfo, True, True, True, True)
        #        )
        EVT_VGPDOC_GRP_INFO_CHANGED(self.vglbDocGrpInfo,self.OnDocGrpInfoChanged)
        EVT_VGPDOC_CHANGED(self.vglbDocGrpInfo,self.OnDocChanged)
        #self.spwInfo.ReplaceWindow(self.pnInfo,self.vglbDocGrpInfo)
        #self.pnInfo.Destroy()
        
        oDocGrp=self.netDoc.GetReg('docgroup')
        oDocMain=self.netDoc.GetReg('maingroup')
        self.viRegSel.AddWidgetDependent(self.vglbDocGrpInfo,None)
        
        self.viRegSel.SetDoc(self.netDoc,True)
        #self.viRegSel.SetRegNode(oDocMain)
        #self.viRegSel.SetRegNode(oDocGrp)
        
        pn=self.vglbDocGrpInfo.AddDocumentPanel(self.pnData)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND|wx.ALL)
        self.pnData.Layout()
        #self.spwInfo.ReplaceWindow(self.pnItem,pn)
        #self.pnItem.Destroy()
        
        self.vglbDocGrpInfo.SetLanguages(self.languages,self.languageIds)
        self.vglbDocGrpInfo.SetNetDocHuman(self.netHum)
        self.vglbDocGrpInfo.SetDoc(self.netDoc,True)
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.SetSize(size)
    def GetDocMain(self):
        return self.netDoc
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        self.netDoc.endEdit(None)
        node=event.GetTreeNodeData()
        if event.GetTreeNodeData() is not None:
            self.netDoc.startEdit(node)
            self.vglbDocGrpInfo.SetNode(node)
        else:
            # grouping tree item selected
            self.vglbDocGrpInfo.SetNode(None)
        event.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        event.Skip()
    def OnDocGrpInfoChanged(self,event):
        oldSelItem=self.vgpTree.triSelected
        self.vgpTree.RefreshSelected()
        self.netDoc.doEdit(self.vgpTree.GetSelected())
    def OnDocChanged(self,event):
        vtLog.CallStack('')
        print self.vgpTree.GetSelected()
        self.netDoc.doEdit(self.vgpTree.GetSelected())
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netDoc.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netDoc.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netDoc.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netDoc.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netDoc.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netDoc.getNodeText(self.baseSettings,'treeviewgrp')
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netDoc.getChild(self.netDoc.getRoot(),'docnumbering')
        self.baseSettings=self.netDoc.getChildForced(self.netDoc.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netDoc.createSubNode(self.netDoc.getRoot(),'settings')
        #    self.netDoc.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netDoc.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        if self.netDoc.ClearAutoFN()>0:
            self.netDoc.Open(fn)
            self.netHum.Open(fn)
        
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
            self.vgpTree.SetNode(self.baseDoc)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.bBlockCfgEventHandling=False
    def CreateNew(self):
        self.netDoc.New()
        self.CreateSetup()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(self.baseDoc)
    def CreateSetup(self):
        node=self.netDoc.getRoot()
        elem=self.netDoc.createSubNode(node,'settings',False)
        # add project file
        elemPrjFN=self.netDoc.createSubNodeText(elem,'projectfn','---')
        # add project file
        elemTaskFN=self.netDoc.createSubNodeText(elem,'taskfn','---')
        # add human file
        elemHumanFN=self.netDoc.createSubNodeText(elem,'humanfn','---')
        # add template dir
        elemTmplDN=self.netDoc.createSubNodeText(elem,'templatedn','---')
        # add project dir
        elemPrjDN=self.netDoc.createSubNode(elem,'projectdn','---')
        self.netDoc.setNodeText(elem,'lastprj','---')
        self.netDoc.setNodeText(elem,'lasttask','---')
        self.netDoc.setNodeText(elem,'lastsubtask','---')
        self.netDoc.setNodeText(elem,'lastdesc','---')
        self.netDoc.setNodeText(elem,'lastloc','---')
        self.netDoc.setNodeText(elem,'lastperson','---')
        self.netDoc.setNodeText(elem,'treeviewgrp','usrdate')
        self.netDoc.setNodeText(elem,'doctreeview','normal')
        
        self.netDoc.AlignNode(elem,iRec=2)
    def OnDocGrpChanged(self,event):
        iGrp=event.GetGrouping()
        mnItems=self.mnViewTree.GetMenuItems()
        if iGrp==0:
            mnItems[0].Check(True)
            mnItems[1].Check(False)
            self.netDoc.setNodeText(self.baseSettings,'treeviewgrp','normal')
            self.netDoc.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        elif iGrp==1:
            mnItems[0].Check(False)
            mnItems[1].Check(True)
            self.netDoc.setNodeText(self.baseSettings,'treeviewgrp','iddocgrp')
            self.netDoc.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        
    def OnActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['top_sash']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Document module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Document'),desc,version

    def OnViRegSelToolXmlRegSelectorSelected(self, event):
        event.Skip()

    def OnViRegSelToolXmlRegSelectorCancel(self, event):
        event.Skip()
        self.vglbDocGrpInfo.Cancel()

    def OnViRegSelToolXmlRegSelectorApply(self, event):
        event.Skip()
        self.vglbDocGrpInfo.Apply()
        #self.vgpTree.RefreshSelected()
        #self.netDoc.doEdit(self.vgpTree.GetSelected())
