#----------------------------------------------------------------------------
# Name:         vXmlDocInputCombTree.py
# Purpose:      input widget for document system xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060423
# CVS-ID:       $Id: vXmlDocInputCombTree.py,v 1.11 2015/02/27 18:54:45 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vDoc.vXmlDocTree import vXmlDocTree
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *

class vXmlDocInputCombTree(vtInputTree):
    VERBOSE=0
    def __initObj__(self,*args,**kwargs):
        try:
            vtInputTree.__initObj__(self,*args,**kwargs)
            
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.txtName=None
            self.tagNameIntName='title'
            self.appl='vDoc'
            
            self.trClass=self.__createTree__
            self.trShow=self.__showNode__
        except:
            self.__logTB__()
    def __initCtrlVal__(self,*args,**kwargs):
        try:
            vtInputTree.__initCtrlVal__(self,*args,**kwargs)
            
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            mode=kwargs.get('modeName','txtRd')
            iProp=kwargs.get('iPropName',2)
            dW=self.__getClsDftArgs__(mode)
            cls,lKey,kw,flag=dW['CLS'],dW['key'],dW['kw'],dW['flag']
            kkw=kwargs.get('kwName',{'name':'txtName'})
            kw.update(kkw)
            kw['parent']=self.GetWid()
            _args,_kwargs=self.GetGuiArgs(kw,lKey,{
                    'pos':(0,0),'size':(-1,24),
                    'style':0,
                    'name':'txtName',
                    'value':'',
                    },
                    sNameSub='txtName')
            if bDbg:
                self.__logDebugAdd__('txtVal',
                        {'_args':_args,'_kwargs':_kwargs})
            self.txtName=cls(*_args,**_kwargs)
            
            self.AddSz(self.txtName,iProp, border=4, flag=flag)
        except:
            self.__logTB__()
    def __createTree__(*args,**kwargs):
        tr=vXmlDocTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id','country','region'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getKey(node),doc.getNodeTextLang(node,self.tagNameIntName,lang)
        #return doc.getNodeText(node,self.tagNameInt),doc.getNodeTextLang(node,self.tagNameIntName,lang)
    def __markModified__(self,flag=True):
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
            f=self.txtName.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtName.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
            f=self.txtName.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtName.SetFont(f)
        self.txtVal.Refresh()
        self.txtName.Refresh()
    def __apply__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;node:%s'%(node),
        #                origin=self.GetName())
        #if self.VERBOSE:
        #    vtLog.CallStack(node)
        try:
            self.fid=''
            self.nodeSel=node
            if self.nodeSel is not None:
                self.fid=self.docTreeTup[0].getKey(node)
            if self.trShow is not None:
                sVal,sName=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            else:
                sVal,sName='',''
            if self.bBlock==False:
                self.__markModified__(True)
            wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal,sName=_(u'fault'),_(u'fault')
            self.__logTB__()
        self.bBlock=True
        self.txtVal.SetValue(sVal.split('\n')[0])
        self.txtName.SetValue(sName.split('\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal

