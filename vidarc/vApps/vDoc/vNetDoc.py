#----------------------------------------------------------------------------
# Name:         vNetDoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetDoc.py,v 1.7 2014/06/20 08:01:50 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vDoc.vXmlDoc import vXmlDoc
from vidarc.tool.net.vNetXmlWxGui import *

class vNetDoc(vXmlDoc,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,
                audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlDoc.__init__(self,attr,skip,synch=synch,verbose=verbose,audit_trail=audit_trail)
        vXmlDoc.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlDoc.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlDoc.IsRunning(self):
    #        return True
    #    return False
    #def New(self,root='docs'):
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlDoc.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlDoc.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Open',origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlDoc.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlDoc.Open(self,fn,True)
        else:
            iRet=vXmlDoc.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
                if silent==False:
                    self.NotifyContent()

        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlDoc.Save(self,fn,encoding)
    def getChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return vXmlDoc.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        vXmlDoc.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'delNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.delNode(self,node)
        vXmlDoc.deleteNode(self,node)
    def moveNode(self,nodePar,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s;id:%s'%(self.getKey(nodePar),self.getKey(node)),self.appl)
        vNetXmlWxGui.moveNode(self,nodePar,node)
        vXmlDoc.moveNode(self,nodePar,node)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlDoc.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlDoc.__stop__(self)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyContent',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyGetNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyAddNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyRemoveNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedIds',origin=self.appl)
        self.docs=self.getSortedNodeIds(['users'],
                        'docgroup','id',[('key','%s'),('title','%s')])
    def GetSortedIds(self):
        try:
            return self.docs
        except:
            self.GenerateSortedIds()
            return self.docs
            