#----------------------------------------------------------------------------
# Name:         vXmlDoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlDoc.py,v 1.15 2014/10/01 09:03:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
#from vidarc.tool.sec.vtSecFileAcl import vtSecFileAcl

from vidarc.vApps.vDoc.vXmlNodeDocRoot import vXmlNodeDocRoot
from vidarc.vApps.vDoc.vXmlNodeDocGrp import vXmlNodeDocGrp
from vidarc.vApps.vDoc.vXmlNodeMainDocGrp import vXmlNodeMainDocGrp
from vidarc.vApps.vDoc.vXmlNodeDocGrpAcl import vXmlNodeDocGrpAcl
VERBOSE=0

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x94IDAT8\x8d\xadS\xb1\r\x800\x0cs\nW\xf0\x03\\S\x89W\xba!6\xc4-\xfd\
\xa6?\xf0\x06LEU\x13W \xc8\xe8\xc4\xae\x1b9"\xaeC]\xe3\xe6O\x05\x02H!J\x8d\
\xb9\xa7\x83\x16\x06\x00\x92\x1d\xb0WYeA\xd3\xc1\x9bj\n\xc4cA<\x96\xb6\x00\
\xb3^\x12\x99\xc8\xb8\xf9S\xa6}V\x02\x8c\xe0\x87U;xJf\xbd\xff\x97h\xd9l\xf5\
\x1c\xa0Cb\r\xd6\xd8\x9d\x03\x96\xb0\x92\xc0\\\xa5\x10\xa5\xa7~\x1b\xc4\xb2>\
/Q\xack\x04\xf4m\xb0\xaf\x9a\x0e\xact\xb2\xc4^`\x908\x1e\xdb\x83O \x00\x00\
\x00\x00IEND\xaeB`\x82' 

class vXmlDoc(vtXmlDomReg):
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='docs'
    APPL_REF=['vHum','vGlobals']
    def __init__(self,attr='id',skip=[],synch=False,appl='vDoc',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            oDocRoot=vXmlNodeDocRoot(tagName='root')
            self.RegisterNode(oDocRoot,False)
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #oAttrCfgML=vtXmlNodeAttrCfgML()
            #oCalc=vtXmlNodeCalc()
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(oAttrCfgML,False)
            #self.RegisterNode(oCalc,False)
            
            self.dDocGrpAcl={}
            
            #oAclFile=vtSecFileAcl()
            #self.RegisterNode(oAclFile,False)
            oDocGrpAcl=vXmlNodeDocGrpAcl()
            self.RegisterNode(oDocGrpAcl,False)
            
            oDocRoot=vXmlNodeDocRoot()
            oMainDocGrp=vXmlNodeMainDocGrp()
            oDocGrp=vXmlNodeDocGrp()
            self.RegisterNode(oDocRoot,False)
            self.RegisterNode(oMainDocGrp,True)
            self.RegisterNode(oDocGrp,False)
            self.LinkRegisteredNode(oMainDocGrp,oDocGrp)
            self.LinkRegisteredNode(oDocGrp,oDocGrp)
            #self.LinkRegisteredNode(oDocGrp,oDocGrpAcl)
            
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'docnumbering')
    #def New(self,revision='1.0',root='docs',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'docnumbering')
        if bConnection==False:
            self.CreateReq()
        #self.setAttribute(elem,self.attr,'')
        #self.checkId(elem)
        #self.processMissingId()
        #self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def CreateIds(self,attr='id'):
        vtXmlDomReg.CreateIds(self,attr)
        self.ids.SetFormat("%04d")
    def GetTranslationOld(self,kind,name):
        if name is None:
            if kind=='maingroup':
                return _(u'maingroup')
            elif kind=='docgroup':
                return _(u'docgroup')
        else:
            if name=='title':
                    return _(u'attr_title')
            try:
                return {'description':_(u'attr_description'),
                    'key':_(u'attr_key'),
                    'path':_(u'attr_path')}[name]
            except:
                return name
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL,
                'maingroup':self.ACL_MSK_NORMAL,
                'docgroup':self.ACL_MSK_NORMAL,},['all','maingroup','docgroup']
    def GetPossibleFilterOld(self):
        return {'maingroup':[('title',self.FILTER_TYPE_DATE),
                            ('description',self.FILTER_TYPE_STRING),
                            ('key',self.FILTER_TYPE_STRING),
                            ('path',self.FILTER_TYPE_STRING),],
                'docgroup':[('title',self.FILTER_TYPE_DATE),
                            ('description',self.FILTER_TYPE_STRING),
                            ('key',self.FILTER_TYPE_STRING),
                            ('path',self.FILTER_TYPE_STRING),],
                }
    def addNode(self,par,node):
        vtXmlDomReg.addNode(self,par,node)
        #oAclFile=self.GetReg('fileAcl')
        oAclFile=self.GetReg('docgroupACL')
        self.__procDocGrpAcl__(node,oAclFile)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(oAclFile.GetAclDict())),self.appl)
    def __procDocGrpAcl__(self,node,oAclFile):
        sTag=self.getTagName(node)
        if sTag=='docgroup':
            id=long(self.getKey(node))
            fp=self.GetFingerPrintAndStore(node)
            if id in self.dDocGrpAcl:
                vtLog.vtLngCur(vtLog.ERROR,'id:%08d already defined'%(id),self.appl)
                return 0
            oAclFile.BuildAcl(node)
            self.dDocGrpAcl[id]={
                        'fingerprint':fp,
                        'dAclDocGrp':oAclFile.GetAclDict(),
                    }
        return 0
    #def Build(self):
    #    vtXmlDomReg.Build(self)
    def CreateAcl(self):
        vtXmlDomReg.CreateAcl(self)
        #vtLog.CallStack('')
        self.dDocGrpAcl={}
        #oAclFile=self.GetReg('fileAcl')
        oAclFile=self.GetReg('docgroupACL',bLogFlt=False)
        if oAclFile is None:
            return
        self.procChildsKeysRec(1,self.getBaseNode(),self.__procDocGrpAcl__,
                    oAclFile)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dDocGrpAcl:%s'%(vtLog.pformat(self.dDocGrpAcl)),self.appl)
    def __checkDocGrpAclUpToDate__(self,id,oAclFile):
        try:
            if id in self.dDocGrpAcl:
                d=self.dDocGrpAcl[id]
                node=self.getNodeByIdNum(id)
                fp=self.GetFingerPrintAndStore(node)
                if fp!=d['fingerprint']:
                    oAclFile.BuildAcl(node)
                    d['dAclDocGrp']=oAclFile.GetAclDict()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(oAclFile.GetAclDict())),self.appl)
                return d['dAclDocGrp']
        except:
            vtLog.vtLngTB(self.appl)
            vtLog.vtLngCur(vtLog.ERROR,'fid:%d;%s'%(fid,vtLog.pformat(self.dDocGrpAcl)),self.appl)
        return None
    def IsAccessOkDocGrp(self,id,objLogin,iAcl,iSecLv):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%08d;objLogin:%s;iAcl:0x%08x;iSecLv:%d'%\
                    (id,`objLogin`,iAcl,iSecLv),self.appl)
        if id not in self.dDocGrpAcl:
            vtLog.vtLngCur(vtLog.INFO,'id not found',self.appl)
            return False
        #oAclFile=self.GetReg('fileAcl')
        oAclFile=self.GetReg('docgroupACL')
        dAclFile=self.__checkDocGrpAclUpToDate__(id,oAclFile)
        if dAclFile is None:
            vtLog.vtLngCur(vtLog.INFO,'no dAcl',self.appl)
            return False
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(dAclFile)),self.appl)
        if oAclFile.IsAccessOkDict(dAclFile,objLogin,iAcl,iSecLv):
            vtLog.vtLngCur(vtLog.INFO,'access ok',self.appl)
            return True
        vtLog.vtLngCur(vtLog.INFO,'no access',self.appl)
        return False
    def ConvertIdStr2Str(self,id):
        try:
            return '%04d'%(long(id))
        except:
            vtLog.vtLngTB(self.appl)
            return id
    def ConvertIdNum2Str(self,id):
        try:
            return '%04d'%(id)
        except:
            vtLog.vtLngTB(self.appl)
            return id
    def GetDocGrpDNs(self,id):
        try:
            node=self.getNodeByIdNum(id)
            if node is None:
                return None
            lvStrs=[]
            while id>=0:
                try:
                    sPath=self.getNodeText(node,'path')
                    sPath=None
                    oReg=self.GetRegByNode(node)
                    if oReg is not None:
                        if hasattr(oReg,'GetPath')==True:
                            sPath=oReg.GetPath(node)
                    if sPath is not None:
                        if len(sPath)>0:
                            lvStrs.append(sPath)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                node=self.getParent(node)
                id=self.getKeyNum(node)
            lvStrs.reverse()
            return lvStrs
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None
