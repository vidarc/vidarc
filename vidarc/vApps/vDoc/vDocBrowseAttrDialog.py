#Boa:Dialog:vDocBrowseAttrDialog
#----------------------------------------------------------------------------
# Name:         vDocBrowseAttrDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vDocBrowseAttrDialog.py,v 1.1 2006/04/19 11:57:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vDocBrowseAttrDialog(parent)

[wxID_VDOCBROWSEATTRDIALOG, wxID_VDOCBROWSEATTRDIALOGCBCANCEL, 
 wxID_VDOCBROWSEATTRDIALOGCBOK, wxID_VDOCBROWSEATTRDIALOGLBLATTR, 
 wxID_VDOCBROWSEATTRDIALOGLSTATTR, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vDocBrowseAttrDialog(wx.Dialog):
    ATTRS=[
        ('DocTitle',_('document title')),
        ('Revision',_('document revision')),
        ('DocDate',_('date of first issue')),
        ('DocDateTime',_('date and time of first issue')),
        ('RevDate',_('revision date')),
        ('RevDateTime',_('revision date and time')),
        ]
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttr, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lstAttr, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=8,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_lstAttr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'name',
              width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=u'description', width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VDOCBROWSEATTRDIALOG,
              name=u'vDocBrowseAttrDialog', parent=prnt, pos=wx.Point(409, 142),
              size=wx.Size(400, 250), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vDoc Drowse Attributes Dialog')
        self.SetClientSize(wx.Size(392, 223))

        self.lblAttr = wx.StaticText(id=wxID_VDOCBROWSEATTRDIALOGLBLATTR,
              label=u'Attributes', name=u'lblAttr', parent=self, pos=wx.Point(4,
              4), size=wx.Size(48, 13), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VDOCBROWSEATTRDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(4, 21),
              size=wx.Size(384, 156), style=wx.LC_REPORT)
        self._init_coll_lstAttr_Columns(self.lstAttr)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VDOCBROWSEATTRDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VDOCBROWSEATTRDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstAttrListColClick,
              id=wxID_VDOCBROWSEATTRDIALOGLSTATTR)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCBROWSEATTRDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Ok'), name=u'cbOk',
              parent=self, pos=wx.Point(104, 185), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VDOCBROWSEATTRDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCBROWSEATTRDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(212, 185),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VDOCBROWSEATTRDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.idxSelAttr=-1
        
        self.cbOk.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        
        self.__build__()
    def __build__(self):
        self.lstAttr.DeleteAllItems()
        for sName,sDesc in self.ATTRS:
            idx=self.lstAttr.InsertStringItem(sys.maxint,sName)
            self.lstAttr.SetStringItem(idx,1,sDesc)
    def GetSelAttr(self):
        if self.idxSelAttr<0:
            return None
        return self.ATTRS[self.idxSelAttr][0]
    def OnCbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.idxSelAttr=-1
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        self.idxSelAttr=event.GetIndex()
        event.Skip()

    def OnLstAttrListColClick(self, event):
        self.idxSelAttr=-1
        event.Skip()
            
