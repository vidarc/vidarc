#Boa:FramePanel:Panel1

import wx
#import ColorPanel
import lang_images
from boa.vApps.vDoc.vgpDocGrpInfo import *
from boa.vApps.vDoc.vgpDocGrpInfoControl import *
from boa.vApps.vDoc.vgpDocInfo import *

[wxID_PANEL1] = [wx.NewId() for _init_ctrls in range(1)]

class vglbDocGrpInfo(wx.Listbook):
    def _init_ctrls(self, prnt):
        wx.Listbook.__init__(self, style=wx.TAB_TRAVERSAL, name='', parent=prnt, pos=wx.DefaultPosition, id=wxID_PANEL1, size=wx.Size(100, 50))

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        #self.SetPadding(wx.Size(2,2))
        self.pnDoc=None
        self.tmplDN=None
        self.pnCtrl=None
        self.doc=None
        
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGING, self.OnPageChanging)

    def SetHuman(self,grp,usr):
        self.pnCtrl.SetHuman(grp,usr)
        pass
    def AddDocumentPanel(self,parent):
        self.pnDoc=vgpDocInfo(parent,0,wx.Point(0,0),wx.Size(200,100),0,u"")
        EVT_VGPDOCUMENT_CHANGED(self.pnDoc,self.OnDocumentChanged)
        self.pnDoc.SetTemplateBaseDir(self.tmplDN)
        return self.pnDoc
    def SetLanguages(self,languages,languageIds,pnCtrl=None):
        languages=[u'English',u'Deutsch']
        il = wx.ImageList(32, 32)
        for i in range(0,len(languages)):
            f=getattr(lang_images, 'getLang%02dBigBitmap' % i)
            bmp = f()
            il.Add(bmp)
        idx=il.Add(lang_images.getApplyMultipleBitmap())
        self.AssignImageList(il)
        
        if pnCtrl is None:
            sName="vgpDocGrpInfoControl"
            pnCtrl=vgpDocGrpInfoControl(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=sName)
            EVT_VGPDOC_GRP_INFO_CHANGED(pnCtrl,self.OnDocGrpChanged)
            EVT_VGPDOC_GRP_INFO_CANCELED(pnCtrl,self.OnDocGrpCanceled)
        self.AddPage(pnCtrl,_(u'General'),False,idx)
        self.pnCtrl=pnCtrl
        
        self.vpgDocGrpInfos=[]
        for i in range(0,len(languages)):
            sName="vgpDocGrpInfo%s"%languages[i]
            panel=vgpDocGrpInfo(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=sName)
            panel.SetLanguage(languageIds[i])
            self.AddPage(panel,languages[i],i==0,i)
            self.vpgDocGrpInfos.append(panel)
            EVT_VGPDOCUMENT_SELECTED(panel,self.OnDocumentSelected)
            EVT_VGPDOC_CHANGED(panel,self.OnDocChanged)
            EVT_VGPDOC_GRP_INFO_CHANGED(panel,self.OnDocGrpChanged)
            EVT_VGPDOC_GRP_INFO_CANCELED(panel,self.OnDocGrpCanceled)
        self.languageIds=[]
        for id in languageIds:
            self.languageIds.append(id)
    def SetTemplateBaseDir(self,dn):
        self.tmplDN=dn
        if self.pnDoc is not None:
            self.pnDoc.SetTemplateBaseDir(self.tmplDN)
        
        for panel in self.vpgDocGrpInfos:
            panel.SetTemplateBaseDir(self.tmplDN)
    def SetNode(self,node):
        #self.SetSelection(0)
        self.node=node
        #sel=self.GetSelection()
        #self.vgpDocGrpInfos[sel].SetNode(node)
        self.pnCtrl.SetNode(node)
        for p in self.vpgDocGrpInfos:
            p.SetNode(node)
    def SetNetDocHuman(self,netDoc):
        self.pnCtrl.SetNetDocHuman(netDoc)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        pass
    def SetDoc(self,doc,bNet=False):
        if doc is None:
            return
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        self.pnCtrl.SetDoc(doc,bNet)
        self.pnDoc.SetDoc(doc,bNet)
        for p in self.vpgDocGrpInfos:
            p.SetDoc(doc,bNet)
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.pnDoc.Lock(False)
            else:
                self.pnDoc.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.pnDoc.Lock(False)
        evt.Skip()
    
    def OnPageChanged(self, event):
        old = event.GetOldSelection() -1
        sel = self.GetSelection() -1
        if old >= 0:
            oldPanel=self.vpgDocGrpInfos[old]
            selPanel=self.vpgDocGrpInfos[sel]
            if self.pnDoc is not None:
                self.pnDoc.SetNode(selPanel.GetSelectedDoc(),
                        selPanel.GetLanguage())
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def OnDocGrpChanged(self,event):
        self.pnCtrl.GetNode(self.node)
        #if self.pnDoc is not None:
        #    self.pnDoc.GetNode(selPanel.GetSelectedDoc(),
        #            selPanel.GetLanguage())
        for panel in self.vpgDocGrpInfos:
            panel.GetNode(self.node)
        wxPostEvent(self,vgpDocGrpInfoChanged(self,self.node,None))
        event.Skip()
    def OnDocGrpCanceled(self,event):
        self.pnCtrl.SetNode(self.node)
        for panel in self.vpgDocGrpInfos:
            panel.SetNode(self.node)
        wxPostEvent(self,vgpDocGrpInfoCanceled(self,self.node,None))
        event.Skip()
    def OnDocumentSelected(self,event):
        if self.pnDoc is not None:
            self.pnDoc.SetNode(event.GetNode(),event.GetLanguage())
        event.Skip()
    def OnDocumentChanged(self,event):
        sel = self.GetSelection() -1
        selPanel=self.vpgDocGrpInfos[sel]
        selPanel.RefreshDocuments()
        wxPostEvent(self,vgpDocChanged(self,None,None))
        event.Skip()
    def OnDocChanged(self,event):
        wxPostEvent(self,vgpDocChanged(self,None,None))
        event.Skip()
        