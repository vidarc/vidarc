#----------------------------------------------------------------------------
# Name:         vXmlDocTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlDocTree.py,v 1.16 2014/06/20 08:02:20 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemEdited
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemAdded
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vDoc.vNetDoc import *
import wx
import images
import vidarc.tool.lang.images as images_lang
import vidarc.tool.art.vtArt as vtArt

ATTRS=['title','key','path']

wxEVT_VGTRXMLDOC_GROUPING_CHANGED=wx.NewEventType()
def EVT_VGTRXMLDOC_GROUPING_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGTRXMLDOC_GROUPING_CHANGED,func)
class vgtrXmlDocGroupingChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VGPXMLDOC_GROUPING_CHANGED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,grouping):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGTRXMLDOC_GROUPING_CHANGED)
        self.obj=obj
        self.grouping=grouping
    def GetVgpTree(self):
        return self.obj
    def GetGrouping(self):
        return self.grouping

class vXmlDocTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.doc=None
        self.prjAttr=ATTRS
        self.nodeKey='|id'
        self.nodeInfos=['title','|id','path']
        self.lang=None
        self.languages=[]
        self.langIds=[]
        self.bGrpMenu=True
        self.bLangMenu=True
        #self.SetLanguage('en')
        self.iGrp=0
        self.grpMenu=[
            ('normal',_(u'normal'),
                [],
                [('|id',''),('title','')],
                ),
            ('id grouping',_(u'ID gouping'),
                [('|id',',2',('','xx '),('title','# '))],[('|id',''),('title','')],
                [('|id',''),('title','')],
                ),
            ]
        #self.bManualAdd=False
        #EVT_THREAD_ADD_ELEMENTS_FINISHED(self,self.OnAddFinished)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlGrpTree.__del__(self)
    def SetLanguage(self,lang):
        self.language=lang
        vtXmlGrpTree.SetLanguage(self,lang)
        #self.SetNode(self.rootNode)
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[('|id',''),('title','')]
        #self.bGrouping=False
        self.SetNormalGrouping()
    def SetNormalGrouping(self):
        self.iGrp=0
        self.SetGrouping([],[('|id',''),('title','')])
    def SetIdGrouping(self):
        self.iGrp=1
        self.SetGrouping([('|id',',2',('','xx '),('title','# '))],[('|id',''),('title','')])
    def SetupImageList(self):
        self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        self.__addElemImage2ImageList__('docnumbering',
                            images.getPluginImage(),
                            images.getPluginImage(),True)
            
        img=images.getDocGrpBitmap()
        self.imgDict['elem']['maingroup']=[self.imgLstTyp.Add(img)]
        img=images.getDocGrpSelBitmap()
        self.imgDict['elem']['maingroup'].append(self.imgLstTyp.Add(img))
        
        img=images.getSubDocGrpBitmap()
        self.imgDict['elem']['docgroup']=[self.imgLstTyp.Add(img)]
        img=images.getSubDocGrpSelBitmap()
        self.imgDict['elem']['docgroup'].append(self.imgLstTyp.Add(img))
        
        img=images.getTextBitmap()
        #img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getYearBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['YYYY']=[self.imgLstTyp.Add(img)]
        
        img=images.getMonthBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['MM']=[self.imgLstTyp.Add(img)]
        
        img=images.getDayBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['DD']=[self.imgLstTyp.Add(img)]
        
        img=images.getDocGrpBitmap()
        self.imgDict['grp']['|id']=[self.imgLstTyp.Add(img)]
        img=images.getDocGrpSelBitmap()
        self.imgDict['grp']['|id'].append(self.imgLstTyp.Add(img))
        
        img=images.getUserBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['grp']['manager']=[self.imgLstTyp.Add(img)]
        
        img=images.getClientBitmap()
        self.imgDict['grp']['attributes:clientshort']=[self.imgLstTyp.Add(img)]
        
        img=images.getTextSelBitmap()
        #img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
        self.SetImageList(self.imgLstTyp)
    def RefreshSelectedOld(self):
        oldnode=self.triSelected
        tiRoot=self.GetItemParent(self.triSelected)
        if self.doc.getTagName(self.objTreeItemSel)=='maingroup':
            self.triSelected=self.__addMainGroup__(self.tiRoot,self.objTreeItemSel)
            bSort=False
        else:
            if self.GetPyData(tiRoot) is None:
                tiRoot=self.GetItemParent(tiRoot)
            self.triSelected=self.__addElement__(tiRoot,self.objTreeItemSel,bRet=True)
            bSort=True
            
        tid=wx.TreeItemData()
        tid.SetData(None)
        self.SetItemData(oldnode,tid)
        self.SelectItem(self.triSelected)
        self.Delete(oldnode)
        self.SelectItem(self.triSelected)
        if bSort:
            if self.TREE_DATA_ID:
                idPar=str(self.GetPyData(tiRoot))
            else:
                idPar=self.doc.getAttribute(self.GetPyData(tiRoot),self.doc.attr)
            if self.groupItems.has_key(idPar):
                self.__sortAllGrpItems__(self.groupItems[idPar])
            self.SortChildren(tiRoot)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
    def __sortAllGrpItemsO__(self,grpItem):
        for g in grpItem.values():
            self.SortChildren(g[0])
            self.__sortAllGrpItems__(g[1])
    def __applyFormat__(self,val,fmt):
        sPos=string.split(fmt,',')
        if len(sPos)==2:
            try:
                if len(sPos[0])>0:
                    i=int(sPos[0])
                    if len(sPos[1])>0:
                        j=int(sPos[1])
                        val=val[i:j]
                    else:
                        val=val[i:]
                else:
                    if len(sPos[1])>0:
                        j=int(sPos[1])
                        val=val[:j]
                        
            except:
                pass
            return val
        if fmt[0]=='#':
            i=string.find(val,fmt[1])
            if i>0:
                return val[:i]
            else:
                return val
            
    def __getValFormat__(self,g,val,infos=None):
        #vtLog.CallStack(val)
        #vtLog.pprint(g)
        #vtLog.pprint(infos)
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='|id':
                val=self.__applyFormat__(val,sFormat)
                for gTmp in g[2:]:
                    try:
                        newVal=infos[gTmp[0]]
                    except:
                        newVal=''
                    val=val+self.__getValFormat__(gTmp,newVal,infos)
                #if len(g)>2:
                #    val=val+g[-1]
                #if len(g)>3:
                #    for 
                #    if infos is not None:
                        
                return val
            elif g[0]=='title':
                return self.__applyFormat__(val,sFormat)
            elif g[0]=='':
                return g[1]
        # use parent format
        return vtXmlGrpTree.__getValFormat__(self,g,val)                
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='person':
            #if sFormat=='':
            img=self.imgDict['grp']['person'][0]
            imgSel=self.imgDict['grp']['person'][0]
            return (img,imgSel)
        elif g[0]=='|id':
            #if sFormat=='':
            img=self.imgDict['grp']['|id'][0]
            imgSel=self.imgDict['grp']['|id'][0]
            return (img,imgSel)
        elif g[0]=='attributes:clientshort':
            #if sFormat=='':
            img=self.imgDict['grp']['attributes:clientshort'][0]
            imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return (img,imgSel)
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][o][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][o][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)

    def __addMainGroup__(self,parent,o):
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        tid=wx.TreeItemData()
        if self.TREE_DATA_ID:
            tid.SetData(self.doc.getKeyNum(o))
        else:
            tid.SetData(o)
        #sKey=vtXmlDomTree.getNodeTextLang(o,'key',self.language)
        sKey=self.doc.getKey(o)
        sTitle=self.doc.getNodeTextLang(o,'title',self.lang)
        sLabel=sKey+" "+sTitle
        tn=self.AppendItem(parent,sLabel,-1,-1,tid)
        self.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,wx.TreeItemIcon_Selected)
        if self.tiCache is not None:
            self.tiCache.AddID(tn,self.doc.getKeyNum(o))
        #if o.tagName=='projtimes':
        #    self.tnProjTimes=tn
        #print self.tiRoot
        #self.tiRoot=tn
        self.__addElements__(tn,o)
        self.SortChildren(parent)
        return tn
    def __addElements__old(self,parent,obj):
        ids=self.doc.getIds()
        for o in self.doc.getChilds(obj):
            #print "addelements",parent
            #tn=self.treeCtrl1.AppendItem(parent,o.tagName,-1,-1,o)
            bBlockRec=False
            tagName=self.doc.getTagName(o)
            if tagName in ['docgroup']:
                #ids.CheckId(o)
                bBlockRec=True
                    
                self.__addElement__(parent,o)
            elif tagName=='maingroup':
                self.__addMainGroup__(parent,o)
            else:
                tid=wx.TreeItemData()
                tid.SetData(None)
        self.SortChildren(parent)
    def OnGotContent(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnGotContent;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if self.verbose:
            vtLog.vtLogCallDepth(self,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster))
        self.SetNode(self.doc.getChild(None,'docnumbering'))
    def SetNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'SetNode;%s;parent:%s master:%d'%(node,self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if self.doc is None:
            return
        if node is None:
            #node=self.doc.getChild(None,'docnumbering')
            node=self.doc.getBaseNode()
        ids=self.doc.getIds()#vtXmlDom.NodeIds(self.doc)
        ids.SetFormat("%04d")
        vtXmlGrpTree.SetNode(self,node)
    def __createMenuEditIds__(self):
        self.popupIdAddMainGrp=wx.NewId()
        self.popupIdAddDocGrp=wx.NewId()
        self.popupIdDel=wx.NewId()
        self.popupIdEdit=wx.NewId()
        self.popupIdMove=wx.NewId()
        wx.EVT_MENU(self,self.popupIdAddMainGrp,self.OnTreeAddMainGrp)
        wx.EVT_MENU(self,self.popupIdAddDocGrp,self.OnTreeAddDocGrp)
        wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
        wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        #wx.EVT_MENU(self,self.popupIdAdd,self.OnTreeAdd)
        #wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDel)
    def __addMenuEdit__(self,menu):
        if self.bMaster:
            mnIt=wx.MenuItem(menu,self.popupIdAddMainGrp,_(u'add main document group'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Add))
            menu.AppendItem(mnIt)
            nodePar=self.GetSelected()          # 080427 wro: use method
            if nodePar is not None:
                sTag=self.doc.getTagName(nodePar)
            else:
                sTag=None
            if sTag in ['docgroup']:
                mnIt.Enable(False)
            if self.doc.IsAclOk('maingroup',self.doc.ACL_MSK_ADD)==False:
                mnIt.Enable(False)
            mnIt=wx.MenuItem(menu,self.popupIdAddDocGrp,_(u'add document group'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Add))
            menu.AppendItem(mnIt)
            if self.doc is not None:
                if self.doc.getTagName(nodePar)!='maingroup':
                    mnIt.Enable(False)
                if self.doc.IsAclOk('docgroup',self.doc.ACL_MSK_ADD)==False:
                    mnIt.Enable(False)
            #    if not self.doc.hasPossibleNode(nodePar):
            #        mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdDel,_(u'Delete'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Del))
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            else:
                if self.doc.IsNodeAclOk(self.GetSelected(),self.doc.ACL_MSK_DEL)==False:
                    mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdEdit,_(u'Edit'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Edit))
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            else:
                if self.doc.IsNodeAclOk(self.GetSelected(),self.doc.ACL_MSK_WRITE)==False:
                    mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdMove,_(u'Move'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Move))
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            else:
                if self.doc.IsNodeAclOk(self.GetSelected(),self.doc.ACL_MSK_MOVE)==False:
                    mnIt.Enable(False)
            menu.AppendSeparator()
            return True
        return False

    def OnTcNodeRightDownO(self, event):
        if self.bGrpMenu==False and self.bLangMenu==False and self.bMaster==False:
            return
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdAddMainGrp'):
            # grouping menu item ids
            self.popupIdGrpId=wx.NewId()
            self.popupIdGrpNormal=wx.NewId()
            
            # language menu item ids
            self.popupIdLangEN=wx.NewId()
            self.popupIdLangDE=wx.NewId()
            
            # master menu item ids
            self.popupIdAddMainGrp=wx.NewId()
            self.popupIdAddDocGrp=wx.NewId()
            #self.popupIdAddUsr=wxNewId()
            self.popupIdDel=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            self.popupIdSort=wx.NewId()
            
            wx.EVT_MENU(self,self.popupIdGrpId,self.OnTreeGrpID)
            wx.EVT_MENU(self,self.popupIdGrpNormal,self.OnTreeGrpNormal)
            
            wx.EVT_MENU(self,self.popupIdLangEN,self.OnTreeLangEN)
            wx.EVT_MENU(self,self.popupIdLangDE,self.OnTreeLangDE)
            
            wx.EVT_MENU(self,self.popupIdAddMainGrp,self.OnTreeAddMainGrp)
            wx.EVT_MENU(self,self.popupIdAddDocGrp,self.OnTreeAddDocGrp)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
            wx.EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
        menu=wx.Menu()
        if self.bGrpMenu:
            mnIt=wx.MenuItem(menu,self.popupIdGrpNormal,_('Normal'),kind=wx.ITEM_CHECK)
            #mnIt.SetBitmap(images.getStoppedBitmap())
            menu.AppendItem(mnIt)
            
            mnIt=wx.MenuItem(menu,self.popupIdGrpId,_('ID grouping'),kind=wx.ITEM_CHECK)
            #mnIt.SetBitmap(images.getStoppedBitmap())
            menu.AppendItem(mnIt)
            
            menu.AppendSeparator()
            if self.iGrp==0:
                menu.Check(self.popupIdGrpNormal,True)
            elif self.iGrp==1:
                menu.Check(self.popupIdGrpId,True)
        if self.bLangMenu:
            mnIt=wx.MenuItem(menu,self.popupIdLangEN,u'English')
            mnIt.SetBitmap(images_lang.getLangEnBitmap())
            menu.AppendItem(mnIt)
            
            mnIt=wx.MenuItem(menu,self.popupIdLangDE,u'Deutsch')
            mnIt.SetBitmap(images_lang.getLangDeBitmap())
            menu.AppendItem(mnIt)
            menu.AppendSeparator()
            #if self.language=='en':
            #    menu.Check(self.popupIdLangEN,True)
            #elif self.language=='de':
            #    menu.Check(self.popupIdLangDE,True)
        if self.bMaster:
            menu.Append(self.popupIdAddMainGrp,'Add main document group')
            menu.Append(self.popupIdAddDocGrp,'Add document group')
            #menu.Append(self.popupIdAddUsr,'Add User')
            menu.Append(self.popupIdDel,'Delete')
            menu.Append(self.popupIdEdit,'Edit')
            menu.Append(self.popupIdMove,'Move Node')
            menu.Append(self.popupIdSort,'Sort')
        
        self.PopupMenu(menu,(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def OnTreeGrpID(self,evt):
        iOldGrp=self.iGrp
        self.SetIdGrouping()
        if iOldGrp!=self.iGrp:
            wx.PostEvent(self,vgtrXmlDocGroupingChanged(self,self.iGrp))
            self.SetNode(None)
        pass
    def OnTreeGrpNormal(self,evt):
        iOldGrp=self.iGrp
        self.SetNormalGrouping()
        if iOldGrp!=self.iGrp:
            wx.PostEvent(self,vgtrXmlDocGroupingChanged(self,self.iGrp))
            self.SetNode(None)
        pass
    def OnTreeLangEN(self,evt):
        self.SetLanguage('en')
        pass
    def OnTreeLangDE(self,evt):
        self.SetLanguage('de')
        pass
    def OnTreeAddMainGrp(self,event):
        if self.rootNode is None:
            return -1
        try:
            self.AddMainGrpNode(self.rootNode)
        except:
            self.__logTB__()
    def OnTreeAddDocGrp(self,event):
        if self.rootNode is None:
            return -1
        try:
            node=self.GetSelected()         # 080427 wro: use method
            if node is None:
                self.__logWarn__('node is None')
                return
            sTag=self.doc.getTagName(node)
            self.__logDebug__({'sTag':sTag,'id':self.doc.getKey(node)})
            if sTag=='docgroup':
                node=self.doc.getParent(node)
                tiRoot=self.FindTreeItem(node)
            else:
                tiRoot=self.FindTreeItem(node)
            self.AddDocGrpNode(node,tiRoot)
        except:
            self.__logTB__()
            return
    def OnTreeDelGrp(self,event):
        if self.rootNode is None:
            return -1
        try:
            node=self.GetSelected()         # 080427 wro: use method
            par=self.doc.getParent(self.GetSelected())         # 080427 wro: use method
            self.doc.delNode(self.GetSelected())
            self.Delete(self.triSelected)
        except:
            self.__logTB__()
    def OnTreeEdit(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        s=self.GetItemText(self.triSelected)
        
        if string.find(s,"project:")==0:
            # open group edit dialog
            try:
                d=self.dlgEditPrj
            except:
                self.dlgEditPrj=vgdXmlDlgEditPrj(self)
            self.dlgEditPrj.SetNode(self.doc,self.GetSelected())         # 080427 wro: use method
            ret=self.dlgEditPrj.ShowModal()
            if ret==1:
                # process dialog ok
                s=self.dlgEditPrj.GetNode(self.GetSelected())         # 080427 wro: use method
                self.SetItemText(self.triSelected,'project:'+s)
                wx.PostEvent(self,vtXmlTreeItemEdited(self,
                    self.triSelected,self.objTreeItemSel))  # forward double click event
        
            #dlg.Destroy()
    def SetLanguages(self,languages,languageIds):
        self.languages=languages
        self.languageIds=languageIds
    def AddMainGrpNode(self,node):
        child=self.doc.createSubNode(node,'maingroup')
        # append key
        #nodeKey=doc.createElement('key')
        #nodeKey.appendChild(doc.createTextNode('---'))
        #child.appendChild(nodeKey)
        # append title
        if self.languageIds is None:
            nodeTitle=self.doc.createSubNodeText(child,'title','---')
        else:
            for id in self.languageIds:
                nodeTitle=self.doc.createSubNodeTextAttr(child,'title','---',
                                        'language',id)
                
        # append description
        if self.languageIds is None:
            nodeDesc=self.doc.createSubNodeText(child,'description','---')
        else:
            for id in self.languageIds:
                nodeDesc=self.doc.createSubNodeTextAttr(child,
                        'description','---','language',id)
        #self.ids.AddNewNode(prj)
        self.doc.AlignNode(child,iRec=2)
        self.doc.addNode(node,child)
        
        #ti=self.__addMainGroup__(self.tiRoot,child)
        #self.SelectItem(ti)
        #self.__update_tree__()
        #self.triSelected=ti
        #self.objTreeItemSel=child
        #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,-1))
        
    def AddDocGrpNode(self,node,tiRoot):
        idPar=self.doc.getKey(node)
        child=self.doc.createSubNode(node,'docgroup')
        # append key
        nodeKey=self.doc.createSubNodeText(child,'key','---')
        # append title
        if self.languageIds is None:
            nodeTitle=self.doc.createSubNodeText(child,'title','---')
        else:
            for id in self.languageIds:
                nodeTitle=self.doc.createSubNodeTextAttr(child,
                            'title','---','language',id)
        # append description
        if self.languageIds is None:
            nodeDesc=self.doc.createSubNode(child,'description','---')
        else:
            for id in self.languageIds:
                nodeDesc=self.doc.createSubNodeTextAttr(child,
                            'description','---','language',id)
        #self.ids.AddNewNode(prj)
        self.doc.AlignNode(child,iRec=2)
        self.doc.addNode(node,child)
        #ti=self.__addElement__(tiRoot,child,True)
        #self.SelectItem(ti)
        #self.__update_tree__()
        #self.triSelected=ti
        #self.objTreeItemSel=child
        
        #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,idPar))
        
    def __update_tree__(self):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return
        # update tree
        triRoot=self.GetRootItem()
        triChild=self.GetFirstChild(triRoot)
        if self.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.GetNextChild(triChild[0],triChild[1])
            if self.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
        self.groupItems={}
        #self.DeleteChildren(triPar)
        self.DeleteChildren(triRoot)
        self.__addElements__(triRoot,self.rootNode)
        parTri=self.GetItemParent(triRoot)
        self.SortChildren(triRoot)
        #self.SelectItem(triPar)
        wx.PostEvent(self,vtXmlTreeItemAdded(self,
                            triPar,self.rootNode))
        return 0
