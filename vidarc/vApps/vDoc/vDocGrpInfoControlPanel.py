#Boa:FramePanel:vDocGrpInfoControlPanel
#----------------------------------------------------------------------------
# Name:         vDocGrpInfoControlPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocGrpInfoControlPanel.py,v 1.12 2010/03/29 08:54:42 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.grid
import wx.lib.buttons
import cStringIO
import string,sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.vApps.vDoc.vDocGrpInfoPanel import *
    from vidarc.tool.xml.vtXmlNodePanel import *
    #import vidarc.tool.xml.vtXmlDom as vtXmlDom
    #from vidarc.vApps.vHum.vNetHum import *
    #import vidarc.vApps.vPrj.vXmlPrjTree as vXmlPrjTree
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import images
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VDOCGRPINFOCONTROLPANEL, wxID_VDOCGRPINFOCONTROLPANELGCBBADD, 
 wxID_VDOCGRPINFOCONTROLPANELGCBBDELREF, wxID_VDOCGRPINFOCONTROLPANELLBLKEY, 
 wxID_VDOCGRPINFOCONTROLPANELLBLPATH, wxID_VDOCGRPINFOCONTROLPANELLBLREFCOMP, 
 wxID_VDOCGRPINFOCONTROLPANELLBLREFERENCES, 
 wxID_VDOCGRPINFOCONTROLPANELLBLREFNUM, wxID_VDOCGRPINFOCONTROLPANELLSTREF, 
 wxID_VDOCGRPINFOCONTROLPANELTXTID, wxID_VDOCGRPINFOCONTROLPANELTXTPATH, 
 wxID_VDOCGRPINFOCONTROLPANELTXTREFCOMP, 
 wxID_VDOCGRPINFOCONTROLPANELTXTREFNUMBER, 
] = [wx.NewId() for _init_ctrls in range(13)]


class vDocGrpInfoControlPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblKey, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtId, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblPath, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.txtPath, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsGen, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblReferences, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsRefInput, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.fgsRef, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsRefInputNum_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRefNum, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtRefNumber, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsRef_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRef, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsRefBt, 1, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsRef_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsRefBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbAdd, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbDelRef, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsRefInput_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsRefInputCust, 2, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsRefInputNum, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsRefInputCust_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRefComp, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtRefComp, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsGen = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRefBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsRefInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRefInputCust = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRefInputNum = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsRef = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsGen_Items(self.bxsGen)
        self._init_coll_bxsRefBt_Items(self.bxsRefBt)
        self._init_coll_bxsRefInput_Items(self.bxsRefInput)
        self._init_coll_bxsRefInputCust_Items(self.bxsRefInputCust)
        self._init_coll_bxsRefInputNum_Items(self.bxsRefInputNum)
        self._init_coll_fgsRef_Items(self.fgsRef)
        self._init_coll_fgsRef_Growables(self.fgsRef)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCGRPINFOCONTROLPANEL,
              name=u'vDocGrpInfoControlPanel', parent=prnt, pos=wx.Point(63,
              33), size=wx.Size(267, 182), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(259, 155))

        self.lblKey = wx.StaticText(id=wxID_VDOCGRPINFOCONTROLPANELLBLKEY,
              label=_(u'Key'), name=u'lblKey', parent=self, pos=wx.Point(0, 4),
              size=wx.Size(51, 21), style=wx.ALIGN_RIGHT)
        self.lblKey.SetMinSize(wx.Size(-1, -1))

        self.txtId = wx.TextCtrl(id=wxID_VDOCGRPINFOCONTROLPANELTXTID,
              name=u'txtId', parent=self, pos=wx.Point(55, 4), size=wx.Size(47,
              21), style=wx.TE_RICH2, value=u'')
        self.txtId.SetMinSize(wx.Size(-1, -1))
        self.txtId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VDOCGRPINFOCONTROLPANELTXTID)

        self.lblPath = wx.StaticText(id=wxID_VDOCGRPINFOCONTROLPANELLBLPATH,
              label=_(u'Path'), name=u'lblPath', parent=self, pos=wx.Point(106,
              4), size=wx.Size(47, 21), style=wx.ALIGN_RIGHT)
        self.lblPath.SetMinSize(wx.Size(-1, -1))

        self.txtPath = wx.TextCtrl(id=wxID_VDOCGRPINFOCONTROLPANELTXTPATH,
              name=u'txtPath', parent=self, pos=wx.Point(157, 4),
              size=wx.Size(99, 21), style=0, value=u'')
        self.txtPath.SetMinSize(wx.Size(-1, -1))

        self.lstRef = wx.ListCtrl(id=wxID_VDOCGRPINFOCONTROLPANELLSTREF,
              name=u'lstRef', parent=self, pos=wx.Point(0, 71),
              size=wx.Size(175, 84), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstRef.SetMinSize(wx.Size(-1, -1))
        self.lstRef.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRefListItemSelected,
              id=wxID_VDOCGRPINFOCONTROLPANELLSTREF)

        self.lblReferences = wx.StaticText(id=wxID_VDOCGRPINFOCONTROLPANELLBLREFERENCES,
              label=_(u'References'), name=u'lblReferences', parent=self,
              pos=wx.Point(0, 33), size=wx.Size(259, 13), style=0)
        self.lblReferences.SetMinSize(wx.Size(-1, -1))

        self.lblRefComp = wx.StaticText(id=wxID_VDOCGRPINFOCONTROLPANELLBLREFCOMP,
              label=_(u'Company'), name=u'lblRefComp', parent=self,
              pos=wx.Point(0, 46), size=wx.Size(51, 21), style=wx.ALIGN_RIGHT)
        self.lblRefComp.SetMinSize(wx.Size(-1, -1))

        self.txtRefComp = wx.TextCtrl(id=wxID_VDOCGRPINFOCONTROLPANELTXTREFCOMP,
              name=u'txtRefComp', parent=self, pos=wx.Point(55, 46),
              size=wx.Size(47, 21), style=wx.TE_RICH2, value=u'')
        self.txtRefComp.SetMinSize(wx.Size(-1, -1))

        self.lblRefNum = wx.StaticText(id=wxID_VDOCGRPINFOCONTROLPANELLBLREFNUM,
              label=_(u'Number'), name=u'lblRefNum', parent=self,
              pos=wx.Point(103, 46), size=wx.Size(51, 21),
              style=wx.ALIGN_RIGHT)
        self.lblRefNum.SetMinSize(wx.Size(-1, -1))

        self.txtRefNumber = wx.TextCtrl(id=wxID_VDOCGRPINFOCONTROLPANELTXTREFNUMBER,
              name=u'txtRefNumber', parent=self, pos=wx.Point(158, 46),
              size=wx.Size(99, 21), style=wx.TE_RICH2, value=u'')
        self.txtRefNumber.SetMinSize(wx.Size(-1, -1))

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCGRPINFOCONTROLPANELGCBBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'),
              name=u'gcbbAdd', parent=self, pos=wx.Point(179, 71),
              size=wx.Size(76, 34), style=0)
        self.gcbbAdd.SetMinSize(wx.Size(-1, -1))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VDOCGRPINFOCONTROLPANELGCBBADD)

        self.gcbbDelRef = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCGRPINFOCONTROLPANELGCBBDELREF,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'),
              name=u'gcbbDelRef', parent=self, pos=wx.Point(179, 109),
              size=wx.Size(76, 30), style=0)
        self.gcbbDelRef.SetMinSize(wx.Size(-1, -1))
        self.gcbbDelRef.Bind(wx.EVT_BUTTON, self.OnGcbbDelRefButton,
              id=wxID_VDOCGRPINFOCONTROLPANELGCBBDELREF)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vDoc Document Group'),
                lWidgets=[])
        self.netDocHuman=None
        self.selGrp={}
        self.selUsr={}
        self.selectedIdx=-1
        self.refs={}
        self.delNode=[]
        
        # setup list
        self.lstRef.InsertColumn(0,_(u'Company'))
        self.lstRef.InsertColumn(1,_(u'Number'),wx.LIST_FORMAT_LEFT,70)
        #self.Move(pos)
        #self.SetSize(size)
        self.fgsData.Layout()
        #self.fgsData.FitInside(self)
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.refs={}
        self.delNode=[]
        self.lstRef.DeleteAllItems()
        self.selectedIdx=-1
        self.txtId.SetValue('')
        self.txtPath.SetValue('')
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        return
        if doc is None:
            return
        if self.doc is not None:
            self.doc.DelConsumer(self)
        # FIXME if called more than once
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            #EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        
    def OnGotContentOld(self,evt):
        
        evt.Skip()
    def OnGetNodeOld(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNodeOld(self,evt):
        
        evt.Skip()
    def OnAddNodeOld(self,evt):
        
        evt.Skip()
    def OnDelNodeOld(self,evt):
        
        evt.Skip()
    def OnRemoveNodeOld(self,evt):
        
        evt.Skip()
    def OnLockOld(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLockOld(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()

    def SetHuman(self,grp,usr):
        return
        self.grpIds=grp
        self.usrIds=usr
        self.chcLstGrp.Clear()
        keys=self.grpIds.GetKeys()
        for k in keys:
            self.chcLstGrp.Append(k)
        self.chcLstUsr.Clear()
        keys=self.usrIds.GetKeys()
        for k in keys:
            self.chcLstUsr.Append(k)
        pass
    def SetNetDocHuman(self,netDoc):
        self.netDocHuman=netDoc
        EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnHumanChanged)
        self.__humanChanged__()
    def OnHumanContent(self,evt):
        self.__humanChanged__()
        evt.Skip()
    def OnHumanChanged(self,evt):
        self.__humanChanged__()
        evt.Skip()
    def __humanChanged__(self):
        return
        self.chcLstGrp.Clear()
        self.chcLstUsr.Clear()
        if self.netDocHuman is None:
            return
        self.grpIds=self.netDocHuman.GetSortedGrpIds()
        self.usrIds=self.netDocHuman.GetSortedUsrIds()
        
        keys=self.grpIds.GetKeys()
        for k in keys:
            self.chcLstGrp.Append(k)
        keys=self.usrIds.GetKeys()
        for k in keys:
            self.chcLstUsr.Append(k)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            sPath=self.doc.getNodeText(self.node,'path')
            sAttrId=self.doc.getAttribute(self.node,'id')
            
            self.txtPath.SetValue(sPath)
            self.txtId.SetValue(sAttrId)
            
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtRefComp.SetStyle(0, 100, wx.TextAttr("BLACK", bkgCol))
            self.txtRefNumber.SetStyle(0, 100, wx.TextAttr("BLACK", bkgCol))
            
            refNodes=self.doc.getChilds(self.node,'reference')
            for refNode in refNodes:
                sComp=self.doc.getAttribute(refNode,'company')
                sVal=self.doc.getText(refNode)
                try:
                    lst=self.refs[sComp]
                    lst.append(sVal)
                except:
                    self.refs[sComp]=[(sVal,refNode)]
                index = self.lstRef.InsertImageStringItem(sys.maxint, sComp, -1)
                self.lstRef.SetStringItem(index, 1, sVal)
        except:
            self.__logTB__()
        return
        try:
            grpNodes=self.doc.getChilds(self.node,'group')
            #self.chcLstGrp.Clear()
            iLen=len(self.grpIds.GetKeys())
            for i in range(0,iLen):
                self.chcLstGrp.Check(i,False)
            self.selGrp={}
            if self.grpIds is not None:
                for grp in grpNodes:
                    id=self.doc.getAttribute(grp,'fid')
                    idx=self.grpIds.GetIdxById(id)
                    if idx>=0 and idx<iLen:
                        self.selGrp[id]=grp
                        self.chcLstGrp.Check(idx)
        except:
            pass
        try:
            usrNodes=self.doc.getChilds(self.node,'user')
            iLen=len(self.usrIds.GetKeys())
            for i in range(0,iLen):
                self.chcLstUsr.Check(i,False)
            self.selUsr={}
            if self.usrIds is not None:
                for usr in usrNodes:
                    id=self.doc.getAttribute(usr,'fid')
                    idx=self.usrIds.GetIdxById(id)
                    if idx>=0 and idx<iLen:
                        self.selUsr[id]=usr
                        self.chcLstUsr.Check(idx)
        except:
            pass
        pass
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            idLock=self.doc.getKey(node)
            sPath=self.txtPath.GetValue()
            sAttrId=self.txtId.GetValue()
            ids=self.doc.getIds()
            if ids is not None:
                sOldId=self.doc.getAttribute(node,'id')
                
                ids.RemoveId(sOldId)
                ret,n=ids.GetIdStr(sAttrId)
                if ret<0:
                    bOk=False
                else:
                    if n is None:
                        bOk=True
                    else:
                        if n==node:
                            bOk=True
                        else:
                            bOk=False
                if bOk==False:
                    # fault id is not possible
                    ids.AddNewNode(node)
                    sAttr=ids.ConvertId2Str(self.node,self.doc)
                    self.txtId.SetValue(sAttr)
                    bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                    self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", bkgCol))
                else:
                    self.doc.setAttribute(node,'id',sAttrId)
                    sAttr=ids.ConvertId2Str(self.node,self.doc)
                    self.txtId.SetValue(sAttr)
                    self.doc.setAttribute(node,'id',sAttr)
                    self.doc.setNodeText(node,'key',sAttr)
                    self.doc.checkId(node)
                    self.doc.clearMissing()
            self.doc.setNodeText(node,'path',sPath)
            keys=self.refs.keys()
            keys.sort()
            for k in keys:
                vals=self.refs[k]
                for v,n in vals:
                    self.doc.setNodeTextAttr(node,'reference',v,'company',k)
            #return sLogin
        except:
            self.__logTB__()
        return
        if self.netDocHuman is not None:
            grpIds=self.netDocHuman.GetSortedGrpIds()
            if grpIds is not None:
                keys=grpIds.GetKeys()
                for i in range(0,len(keys)):
                    if self.chcLstGrp.IsChecked(i):
                        ids=grpIds.GetIdByIdx(i)
                        id=ids[0]
                        self.doc.setNodeTextAttr(node,'group',keys[i],'fid',id)
                        self.selGrp[id]=None
                for val in self.selGrp.values():
                    if val is not None:
                        self.doc.deleteNode(val)
            usrIds=self.netDocHuman.GetSortedUsrIds()
            if usrIds is not None:
                keys=usrIds.GetKeys()
                for i in range(0,len(keys)):
                    if self.chcLstUsr.IsChecked(i):
                        ids=usrIds.GetIdByIdx(i)
                        id=ids[0]
                        self.doc.setNodeTextAttr(node,'user',keys[i],'fid',ids[0])
                        self.selUsr[id]=None
                for val in self.selUsr.values():
                    if val is not None:
                        self.doc.deleteNode(val)
            for n in self.delNode:
                self.doc.deleteNode(n)
    def CreateNode(self):
        pass
    def OnGcbbApplyButton(self, event):
        #self.GetNode(self.node)
        #wx.PostEvent(self,vgpProjectInfoChanged(self,self.node))
        wx.PostEvent(self,vgpDocGrpInfoChanged(self,self.node,None))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        wx.PostEvent(self,vgpDocGrpInfoCanceled(self,self.node,None))
        event.Skip()

    def OnGcbbDelButton(self, event):
        wx.PostEvent(self,vgpDocGrpInfoDeleted(self,self.node,None))
        event.Skip()

    def OnGcbbDelRefButton(self, event):
        if self.selectedIdx>=0:
            it=self.lstRef.GetItem(self.selectedIdx,0)
            sComp=it.m_text
            it=self.lstRef.GetItem(self.selectedIdx,1)
            sVal=it.m_text
            vals=self.refs[sComp]
            for v,n in vals:
                if v==sVal:
                    self.delNode.append(n)
            self.lstRef.DeleteItem(self.selectedIdx)
        event.Skip()

    def OnGcbbNewButton(self, event):
        wx.PostEvent(self,vgpDocGrpInfoAdded(self,self.node,None))
        event.Skip()


    def OnGcbbAddButton(self, event):
        sComp=self.txtRefComp.GetValue()
        sVal=self.txtRefNumber.GetValue()
        i=self.lstRef.FindItem(-1,sComp,False)
        if i<0:
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtRefComp.SetStyle(0, len(sComp), wx.TextAttr("BLACK", bkgCol))
            try:
                lst=self.refs[sComp]
                lst.append((sVal,None))
            except:
                self.refs[sComp]=[(sVal,None)]
            index = self.lstRef.InsertImageStringItem(sys.maxint, sComp, -1)
            self.lstRef.SetStringItem(index, 1, sVal)
            self.txtRefComp.SetValue('')
            self.txtRefNumber.SetValue('')
        else:
            self.txtRefComp.SetStyle(0, len(sComp), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnTxtIdText(self, event):
        if self.doc is None:
            return
        ids=self.doc.getIds()
        if ids is None:
            return
        sAttr=self.txtId.GetValue()
        ret,n=ids.GetIdStr(sAttr)
        if ret<0:
            ids.AddNewNode(self.node)
            sId=ids.ConvertId2Str(self.node,self.doc)
            self.txtId.SetValue(sId)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", bkgCol))
            return
        if n is None:
            bOk=True
        else:
            if self.doc.isSame(n,self.node):
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", bkgCol))
        else:
            # id is USED -> fault
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnLstRefListItemSelected(self, event):
        idx=event.GetIndex()
        self.selectedIdx=idx
        event.Skip()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag==False:
            self.txtId.SetEditable(True)
            self.txtPath.Enable(True)
            self.txtRefComp.Enable(True)
            self.txtRefNumber.Enable(True)
            #self.gcbbApply.Enable(True)
            #self.gcbbCancel.Enable(True)
            #self.chcLstGrp.Enable(True)
            #self.chcLstUsr.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDelRef.Enable(True)
            self.lstRef.Enable(True)
        else:
            self.txtId.SetEditable(False)
            self.txtPath.Enable(False)
            self.txtRefComp.Enable(False)
            self.txtRefNumber.Enable(False)
            #self.gcbbApply.Enable(False)
            #self.gcbbCancel.Enable(False)
            #self.chcLstGrp.Enable(False)
            #self.chcLstUsr.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDelRef.Enable(False)
            self.lstRef.Enable(False)
            
    
