#Boa:Frame:vDocMainFrame
#----------------------------------------------------------------------------
# Name:         vDocMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocMainFrame.py,v 1.16 2007/08/21 18:12:00 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vDoc.images as images
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.vApps.common.vCfg import vCfg
    from vidarc.vApps.vDoc.vDocMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vLoc'):
    return vDocMainFrame(parent)

[wxID_VDOCMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vDocMainFrame(wx.Frame,vMDIFrame,vCfg):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=-1,
              name=u'vDocMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vDocument')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Document')
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vDocMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vDocPanel')
            self.pn.OpenCfgFile('vDocCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
            
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.dCfg.update(self._getCfgData(['vDocGui']))
        self.pn.SetCfgData(self._setCfgData,['vDocGui'],self.dCfg)
        self.__setCfg__()
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def OpenCfgFile(self,fn=None):
        self.pn.OpenCfgFile(fn)
        self.dCfg.update(self._getCfgData(['vDocGui']))
        self.pn.SetCfgData(self._setCfgData,['vDocGui'],self.dCfg)
        self.__setCfg__()
    def __setCfg__(self):
        try:
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def __getPluginImage__(self):
        return images.getPluginImage()
    def __getPluginBitmap__(self):
        return images.getPluginBitmap()
    def OnMainClose(self,event):
        if self.pn.xdCfg is not None:
            self._setCfgData(['vDocGui'],self.dCfg)
        vMDIFrame.OnMainClose(self,event)
