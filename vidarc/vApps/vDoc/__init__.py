#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: __init__.py,v 1.4 2007/11/17 10:39:50 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import images as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vDocMDIFrame'
PLUGABLE_FRAME=True
PLUGABLE_SRV='vXmlDoc@vXmlSrv'
DOMAINS=['core','vDoc']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','PLUGABLE_SRV',
        'getPluginData',]

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0

