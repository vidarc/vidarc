#----------------------------------------------------------------------------
# Name:         vXmlNodeDocRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vXmlNodeDocRoot.py,v 1.5 2007/10/28 16:29:55 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeDocRoot(vtXmlNodeRoot):
    def __init__(self,tagName='docnumbering'):
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'document numbering root')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x94IDAT8\x8d\xadS\xb1\r\x800\x0cs\nW\xf0\x03\\S\x89W\xba!6\xc4-\xfd\
\xa6?\xf0\x06LEU\x13W \xc8\xe8\xc4\xae\x1b9"\xaeC]\xe3\xe6O\x05\x02H!J\x8d\
\xb9\xa7\x83\x16\x06\x00\x92\x1d\xb0WYeA\xd3\xc1\x9bj\n\xc4cA<\x96\xb6\x00\
\xb3^\x12\x99\xc8\xb8\xf9S\xa6}V\x02\x8c\xe0\x87U;xJf\xbd\xff\x97h\xd9l\xf5\
\x1c\xa0Cb\r\xd6\xd8\x9d\x03\x96\xb0\x92\xc0\\\xa5\x10\xa5\xa7~\x1b\xc4\xb2>\
/Q\xack\x04\xf4m\xb0\xaf\x9a\x0e\xact\xb2\xc4^`\x908\x1e\xdb\x83O \x00\x00\
\x00\x00IEND\xaeB`\x82' 
