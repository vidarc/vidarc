#Boa:FramePanel:vDocGrpInfoSecPanel
#----------------------------------------------------------------------------
# Name:         vDocGrpInfoSecPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vDocGrpInfoSecPanel.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx
import vidarc.tool.sec.vtSecAclFileTree
import vidarc.tool.sec.vtSecAclFile
import vidarc.vApps.vHum.vXmlHumInputTreeUsers
import vidarc.vApps.vHum.vXmlHumInputTreeGroups

from vidarc.tool.xml.vtXmlDomConsumer import *

[wxID_VDOCGRPINFOSECPANEL, wxID_VDOCGRPINFOSECPANELVTSTREE, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vDocGrpInfoSecPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.vtsTree, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCGRPINFOSECPANEL,
              name=u'vDocGrpInfoSecPanel', parent=prnt, pos=wx.Point(428, 91),
              size=wx.Size(317, 253), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(309, 226))

        self.vtsTree = vidarc.tool.sec.vtSecAclFileTree.vtSecAclFileTree(id=wxID_VDOCGRPINFOSECPANELVTSTREE,
              name=u'vtsTree', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(301, 218), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        self.fgsData.Layout()
        #self.vtsTree.Enable(False)
    def SetNetDocHuman(self,doc,bNet=False):
        self.vtsTree.SetDocHum(doc,bNet)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.vtsTree.SetDoc(doc)
    def SetNode(self,node):
        vtXmlDomConsumer.SetNode(self,node)
        if node is not None:
            self.vtsTree.Enable(True)
        else:
            self.vtsTree.Enable(False)
        self.vtsTree.SetNode(node)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        self.vtsTree.GetNode(node)
