#----------------------------------------------------------------------------
# Name:         vDocReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060930
# CVS-ID:       $Id: vDocReg.py,v 1.1 2006/10/09 11:35:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust
if vcCust.is2Import(__name__):
    import vidarc.tool.log.vtLog as vtLog
    vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    import wx
    import vidarc.tool.lang.vtLgBase as vtLgBase
    def funcCreateLoc(obj,sSrc,parent):
        from vidarc.vApps.vDoc.vXmlDocInputTree import vXmlDocInputTree
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        lbl=wx.StaticText(id=-1,
                  label=_(u'document'), name='lblDoc', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlDocInputCombTree(id=-1,name=u'%s_vitrDoc'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vDoc')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
else:
    def funcCreateDoc(obj,sSrc,parent):
        return None,None
def funcSetDocDoc(obj,viTr,doc,dNetDocs):
    viTr.SetDoc(doc)
    viTr.SetDocTree(dNetDocs['vDoc']['doc'],dNetDocs['vDoc']['bNet'])
    viTr.SetNodeTree(None)
def funcGetSelDoc(obj,viTr,doc):
    sVal=viTr.GetValue()
    sID=viTr.GetForeignID()
    return sVal,sID
def funcCmpId(doc,node,val,lMatch):
    if lMatch is not None:
        if val in lMatch:
            return True
        else:
            return False
    return True
def Register4Collector(self,tagNode,tagAttr,translation,tagName='dataCollector'):
    oDataColl=self.GetReg(tagName)
    dSrcWid={tagAttr:{
                    'create':funcCreateDoc,
                    'setDoc':funcSetDocDoc,
                    'getSel':funcGetSelDoc,
                    'translation':translation,
                    'match':funcCmpId},
            }
    oDataColl.UpdateSrcWidDict(dSrcWid)
