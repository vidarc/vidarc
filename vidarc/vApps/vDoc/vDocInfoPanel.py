#Boa:FramePanel:vDocInfoPanel
#----------------------------------------------------------------------------
# Name:         vDocInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocInfoPanel.py,v 1.9 2007/10/13 16:29:02 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.numctrl
import wx.grid
import wx.lib.buttons
import cStringIO
import string,sys,os.path
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vDoc.vNetDoc import *
from vidarc.vApps.vDoc.vDocBrowseAttrDialog import *

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import images

[wxID_VDOCINFOPANEL, wxID_VDOCINFOPANELGCBBADD, wxID_VDOCINFOPANELGCBBAPPLY, 
 wxID_VDOCINFOPANELGCBBBROWSEFN, wxID_VDOCINFOPANELGCBBBROWSEVALUE, 
 wxID_VDOCINFOPANELGCBBCANCEL, wxID_VDOCINFOPANELGCBBDEL, 
 wxID_VDOCINFOPANELGCBBOPEN, wxID_VDOCINFOPANELGCBBSET, 
 wxID_VDOCINFOPANELLBLATTR, wxID_VDOCINFOPANELLBLAUTHOR, 
 wxID_VDOCINFOPANELLBLCELLS, wxID_VDOCINFOPANELLBLFILENAME, 
 wxID_VDOCINFOPANELLBLPLACE, wxID_VDOCINFOPANELLBLPRJDESC, 
 wxID_VDOCINFOPANELLBLTITLE, wxID_VDOCINFOPANELLBLVALUE, 
 wxID_VDOCINFOPANELLBLVERSION, wxID_VDOCINFOPANELLSTATTRS, 
 wxID_VDOCINFOPANELNBTMPL, wxID_VDOCINFOPANELNUMCTRLCELLHEIGHT, 
 wxID_VDOCINFOPANELNUMCTRLCELLWIDTH, wxID_VDOCINFOPANELPNATTR, 
 wxID_VDOCINFOPANELPNGEN, wxID_VDOCINFOPANELPNTMPL, wxID_VDOCINFOPANELTXTATTR, 
 wxID_VDOCINFOPANELTXTAUTHOR, wxID_VDOCINFOPANELTXTDESC, 
 wxID_VDOCINFOPANELTXTFILENAME, wxID_VDOCINFOPANELTXTPLACE, 
 wxID_VDOCINFOPANELTXTTITLE, wxID_VDOCINFOPANELTXTVAL, 
 wxID_VDOCINFOPANELTXTVERSION, 
] = [wx.NewId() for _init_ctrls in range(33)]

# defined event for vgpXmlTree item selected
wxEVT_VGPDOCUMENT_CHANGED=wx.NewEventType()
def EVT_VGPDOCUMENT_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOCUMENT_CHANGED,func)
class vgpDocumentChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPDOCUMENT_CHANGED(<widget_name>, self.OnDocumentChanged)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOCUMENT_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang


class vDocInfoPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_fgsTmpl_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFN, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbOpen, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddSizer(self.bxsCells, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsAttrVal_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsAttrVal_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblValue, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtVal, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblAttr, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtAttr, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblPlace, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPlace, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbApply, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtTitle, 3, border=0, flag=wx.EXPAND)

    def _init_coll_fgsDoc_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsAuthor_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAuthor, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtAuthor, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.lblVersion, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtVersion, 1, border=4, flag=wx.EXPAND)

    def _init_coll_fgsDoc_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTitle, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsAuthor, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsAttrValSet_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstAttrs, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsAttrBt, 0, border=0, flag=0)

    def _init_coll_fgsAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsAttrVal, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbBrowseValue, 0, border=0,
              flag=wx.ALIGN_CENTER)
        parent.AddSizer(self.fgsAttrValSet, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsCells_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCells, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.numCtrlCellWidth, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.numCtrlCellHeight, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsAttrBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbSet, 0, border=3, flag=wx.TOP)
        parent.AddWindow(self.gcbbDel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsAttr_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilename, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFilename, 4, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbBrowseFN, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsTmpl_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjDesc, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtDesc, 3, border=0, flag=wx.EXPAND)

    def _init_coll_fxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbTmpl, 0, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsAttrValSet_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fxsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_nbTmpl_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=True,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnTmpl, select=False,
              text=u'Template')
        parent.AddPage(imageId=-1, page=self.pnAttr, select=False,
              text=u'Attributes')

    def _init_sizers(self):
        # generated method, don't edit
        self.fxsData = wx.FlexGridSizer(cols=2, hgap=4, rows=1, vgap=0)

        self.fgsDoc = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=4)

        self.bxsCells = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsAttr = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=4)

        self.fgsAttrVal = wx.FlexGridSizer(cols=2, hgap=0, rows=3, vgap=0)

        self.fgsAttrValSet = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsAuthor = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAttrBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsTmpl = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=4)

        self._init_coll_fxsData_Items(self.fxsData)
        self._init_coll_fxsData_Growables(self.fxsData)
        self._init_coll_fgsDoc_Items(self.fgsDoc)
        self._init_coll_fgsDoc_Growables(self.fgsDoc)
        self._init_coll_bxsCells_Items(self.bxsCells)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_fgsAttr_Items(self.fgsAttr)
        self._init_coll_fgsAttr_Growables(self.fgsAttr)
        self._init_coll_fgsAttrVal_Items(self.fgsAttrVal)
        self._init_coll_fgsAttrVal_Growables(self.fgsAttrVal)
        self._init_coll_fgsAttrValSet_Items(self.fgsAttrValSet)
        self._init_coll_fgsAttrValSet_Growables(self.fgsAttrValSet)
        self._init_coll_bxsTitle_Items(self.bxsTitle)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsAuthor_Items(self.bxsAuthor)
        self._init_coll_bxsAttrBt_Items(self.bxsAttrBt)
        self._init_coll_fgsTmpl_Items(self.fgsTmpl)
        self._init_coll_fgsTmpl_Growables(self.fgsTmpl)

        self.SetSizer(self.fxsData)
        self.pnGen.SetSizer(self.fgsDoc)
        self.pnAttr.SetSizer(self.fgsAttr)
        self.pnTmpl.SetSizer(self.fgsTmpl)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCINFOPANEL, name=u'vDocInfoPanel',
              parent=prnt, pos=wx.Point(57, 21), size=wx.Size(400, 178),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(392, 151))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'gcbbApply', parent=self, pos=wx.Point(312, 4),
              size=wx.Size(76, 30), style=0)
        self.gcbbApply.SetMinSize(wx.Size(-1, -1))
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VDOCINFOPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(312, 38),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VDOCINFOPANELGCBBCANCEL)

        self.nbTmpl = wx.Notebook(id=wxID_VDOCINFOPANELNBTMPL, name=u'nbTmpl',
              parent=self, pos=wx.Point(4, 4), size=wx.Size(300, 143), style=0)
        self.nbTmpl.SetMinSize(wx.Size(-1, -1))

        self.pnGen = wx.Panel(id=wxID_VDOCINFOPANELPNGEN, name=u'pnGen',
              parent=self.nbTmpl, pos=wx.Point(0, 0), size=wx.Size(292, 117),
              style=wx.TAB_TRAVERSAL)

        self.lblTitle = wx.StaticText(id=wxID_VDOCINFOPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self.pnGen,
              pos=wx.Point(0, 0), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.txtTitle = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTTITLE,
              name=u'txtTitle', parent=self.pnGen, pos=wx.Point(73, 0),
              size=wx.Size(219, 21), style=0, value=u'')
        self.txtTitle.SetMinSize(wx.Size(-1, -1))

        self.lblAuthor = wx.StaticText(id=wxID_VDOCINFOPANELLBLAUTHOR,
              label=_(u'Author'), name=u'lblAuthor', parent=self.pnGen,
              pos=wx.Point(0, 25), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblAuthor.SetMinSize(wx.Size(-1, -1))

        self.txtAuthor = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTAUTHOR,
              name=u'txtAuthor', parent=self.pnGen, pos=wx.Point(73, 25),
              size=wx.Size(73, 21), style=0, value=u'')
        self.txtAuthor.SetMinSize(wx.Size(-1, -1))

        self.lblVersion = wx.StaticText(id=wxID_VDOCINFOPANELLBLVERSION,
              label=_(u'Version'), name=u'lblVersion', parent=self.pnGen,
              pos=wx.Point(146, 25), size=wx.Size(69, 21),
              style=wx.ALIGN_RIGHT)
        self.lblVersion.SetMinSize(wx.Size(-1, -1))

        self.txtVersion = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTVERSION,
              name=u'txtVersion', parent=self.pnGen, pos=wx.Point(219, 25),
              size=wx.Size(73, 21), style=0, value=u'')
        self.txtVersion.SetMinSize(wx.Size(-1, -1))

        self.lblPrjDesc = wx.StaticText(id=wxID_VDOCINFOPANELLBLPRJDESC,
              label=_(u'Description'), name=u'lblPrjDesc', parent=self.pnGen,
              pos=wx.Point(0, 50), size=wx.Size(69, 67), style=wx.ALIGN_RIGHT)
        self.lblPrjDesc.SetMinSize(wx.Size(-1, -1))

        self.txtDesc = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTDESC,
              name=u'txtDesc', parent=self.pnGen, pos=wx.Point(73, 50),
              size=wx.Size(219, 67), style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetExtraStyle(0)
        self.txtDesc.SetMinSize(wx.Size(-1, -1))

        self.pnTmpl = wx.Panel(id=wxID_VDOCINFOPANELPNTMPL, name=u'pnTmpl',
              parent=self.nbTmpl, pos=wx.Point(0, 0), size=wx.Size(292, 117),
              style=wx.TAB_TRAVERSAL)

        self.lblFilename = wx.StaticText(id=wxID_VDOCINFOPANELLBLFILENAME,
              label=_(u'Filename'), name=u'lblFilename', parent=self.pnTmpl,
              pos=wx.Point(0, 0), size=wx.Size(42, 30), style=wx.ALIGN_RIGHT)
        self.lblFilename.SetMinSize(wx.Size(-1, -1))

        self.txtFilename = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTFILENAME,
              name=u'txtFilename', parent=self.pnTmpl, pos=wx.Point(46, 0),
              size=wx.Size(165, 30), style=0, value=u'')
        self.txtFilename.SetMinSize(wx.Size(-1, -1))

        self.lblCells = wx.StaticText(id=wxID_VDOCINFOPANELLBLCELLS,
              label=_(u'Cells'), name=u'lblCells', parent=self.pnTmpl,
              pos=wx.Point(0, 68), size=wx.Size(97, 22), style=wx.ALIGN_RIGHT)
        self.lblCells.SetMinSize(wx.Size(-1, -1))

        self.numCtrlCellWidth = wx.lib.masked.numctrl.NumCtrl(id=wxID_VDOCINFOPANELNUMCTRLCELLWIDTH,
              name=u'numCtrlCellWidth', parent=self.pnTmpl, pos=wx.Point(97,
              68), size=wx.Size(97, 22), style=0, value=0)
        self.numCtrlCellWidth.SetMax(10)
        self.numCtrlCellWidth.SetBounds((0, 10))
        self.numCtrlCellWidth.SetMin(0)
        #self.numCtrlCellWidth.SetInitialSize(wx.Size(30, 22))
        self.numCtrlCellWidth.SetMinSize(wx.Size(-1, -1))

        self.numCtrlCellHeight = wx.lib.masked.numctrl.NumCtrl(id=wxID_VDOCINFOPANELNUMCTRLCELLHEIGHT,
              name=u'numCtrlCellHeight', parent=self.pnTmpl, pos=wx.Point(194,
              68), size=wx.Size(97, 22), style=0, value=0)
        self.numCtrlCellHeight.SetMax(10)
        self.numCtrlCellHeight.SetBounds((0, 10))
        self.numCtrlCellHeight.SetMin(0)
        #self.numCtrlCellHeight.SetInitialSize(wx.Size(30, 22))
        self.numCtrlCellHeight.SetMinSize(wx.Size(-1, -1))

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBBROWSEFN,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=_(u'Browse'),
              name=u'gcbbBrowseFN', parent=self.pnTmpl, pos=wx.Point(215, 0),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseFN.SetMinSize(wx.Size(-1, -1))
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VDOCINFOPANELGCBBBROWSEFN)

        self.gcbbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBOPEN,
              bitmap=vtArt.getBitmap(vtArt.Open), label=_(u'Open'),
              name=u'gcbbOpen', parent=self.pnTmpl, pos=wx.Point(108, 34),
              size=wx.Size(76, 30), style=0)
        self.gcbbOpen.SetMinSize(wx.Size(-1, -1))
        self.gcbbOpen.Bind(wx.EVT_BUTTON, self.OnGcbbOpenTmplButton,
              id=wxID_VDOCINFOPANELGCBBOPEN)

        self.pnAttr = wx.Panel(id=wxID_VDOCINFOPANELPNATTR, name=u'pnAttr',
              parent=self.nbTmpl, pos=wx.Point(0, 0), size=wx.Size(292, 117),
              style=wx.TAB_TRAVERSAL)

        self.lblValue = wx.StaticText(id=wxID_VDOCINFOPANELLBLVALUE,
              label=_(u'Value'), name=u'lblValue', parent=self.pnAttr,
              pos=wx.Point(4, 0), size=wx.Size(113, 21), style=wx.ALIGN_RIGHT)
        self.lblValue.SetMinSize(wx.Size(-1, -1))

        self.txtVal = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTVAL, name=u'txtVal',
              parent=self.pnAttr, pos=wx.Point(121, 0), size=wx.Size(170, 21),
              style=0, value=u'')
        self.txtVal.SetMinSize(wx.Size(-1, -1))

        self.lblAttr = wx.StaticText(id=wxID_VDOCINFOPANELLBLATTR,
              label=_(u'Attribute'), name=u'lblAttr', parent=self.pnAttr,
              pos=wx.Point(4, 21), size=wx.Size(113, 21), style=wx.ALIGN_RIGHT)
        self.lblAttr.SetMinSize(wx.Size(-1, -1))

        self.txtAttr = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTATTR,
              name=u'txtAttr', parent=self.pnAttr, pos=wx.Point(121, 21),
              size=wx.Size(170, 21), style=wx.TE_RICH2, value=u'')
        self.txtAttr.SetMinSize(wx.Size(-1, -1))

        self.lblPlace = wx.StaticText(id=wxID_VDOCINFOPANELLBLPLACE,
              label=_(u'Place'), name=u'lblPlace', parent=self.pnAttr,
              pos=wx.Point(4, 42), size=wx.Size(113, 21), style=wx.ALIGN_RIGHT)
        self.lblPlace.SetMinSize(wx.Size(-1, -1))

        self.txtPlace = wx.TextCtrl(id=wxID_VDOCINFOPANELTXTPLACE,
              name=u'txtPlace', parent=self.pnAttr, pos=wx.Point(121, 42),
              size=wx.Size(170, 21), style=0, value=u'')
        self.txtPlace.SetMinSize(wx.Size(-1, -1))

        self.lstAttrs = wx.ListCtrl(id=wxID_VDOCINFOPANELLSTATTRS,
              name=u'lstAttrs', parent=self.pnAttr, pos=wx.Point(0, 101),
              size=wx.Size(212, 16), style=wx.LC_REPORT)
        self.lstAttrs.SetMinSize(wx.Size(-1, -1))
        self.lstAttrs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VDOCINFOPANELLSTATTRS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'),
              name=u'gcbbAdd', parent=self.pnAttr, pos=wx.Point(216, 101),
              size=wx.Size(76, 30), style=0)
        self.gcbbAdd.SetMinSize(wx.Size(-1, -1))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VDOCINFOPANELGCBBADD)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBSET,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Set'),
              name=u'gcbbSet', parent=self.pnAttr, pos=wx.Point(216, 134),
              size=wx.Size(76, 30), style=0)
        self.gcbbSet.SetMinSize(wx.Size(-1, -1))
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VDOCINFOPANELGCBBSET)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'),
              name=u'gcbbDel', parent=self.pnAttr, pos=wx.Point(216, 168),
              size=wx.Size(76, 30), style=0)
        self.gcbbDel.SetMinSize(wx.Size(-1, -1))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VDOCINFOPANELGCBBDEL)

        self.gcbbBrowseValue = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCINFOPANELGCBBBROWSEVALUE,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=_(u'Browse'),
              name=u'gcbbBrowseValue', parent=self.pnAttr, pos=wx.Point(110,
              67), size=wx.Size(72, 30), style=0)
        self.gcbbBrowseValue.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseValButton,
              id=wxID_VDOCINFOPANELGCBBBROWSEVALUE)

        self._init_coll_nbTmpl_Pages(self.nbTmpl)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.ids=None
        self.lang=None
        self.attrNode=None
        self.tmplDN=None
        self.selectedIdx=-1
        self.docs=[]
        self.attrs={}
        self.dlgBrowseAttr=None
        
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        self.numCtrlCellWidth.SetCtrlParameters(emptyBackgroundColour=bkgCol,validBackgroundColour=bkgCol)
        self.numCtrlCellHeight.SetCtrlParameters(emptyBackgroundColour=bkgCol,validBackgroundColour=bkgCol)
        
        # setup list
        self.lstAttrs.InsertColumn(0,_(u'Attribute'))
        self.lstAttrs.InsertColumn(1,_(u'Value'),wx.LIST_FORMAT_LEFT,270)
        self.SetupImageList()
    def GetLanguage(self):
        return self.lang
    def SetTemplateBaseDir(self,dn):
        self.tmplDN=dn
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        self.txtTitle.SetValue('')
        self.txtDesc.SetValue('')
        self.txtVersion.SetValue('')
        self.txtFilename.SetValue('')
        self.txtAuthor.SetValue('')
        self.lstAttrs.DeleteAllItems()
        self.selectedIdx=-1
        self.attrNodes2Del=[]
        self.node=None
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet=bNet)
        if doc is None:
            return
        # FIXME if called more than once
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            #EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
    
    def SetNode(self,node,lang):
        self.Clear()
        #self.lstAttrs.DeleteAllItems()
        #self.selectedIdx=-1
        #self.attrNodes2Del=[]
        if node is None:
            #self.node=None
            #self.txtTitle.SetValue('')
            ##self.txtDesc.SetValue('')
            #self.txtVersion.SetValue('')
            #self.txtFilename.SetValue('')
            #self.txtAuthor.SetValue('')
            #self.lstAttrs.DeleteAllItems()
            #self.selectedIdx=-1
            #self.Lock(True)
            return
        #print node
        #self.node=self.doc.getChild(node,'document')
        self.node=node['document']
        self.lang=lang
        
        sTitle=self.doc.getNodeText(self.node,'title')
        sDesc=self.doc.getNodeText(self.node,'description')
        sVersion=self.doc.getNodeText(self.node,'version')
        sDate=self.doc.getNodeText(self.node,'date')
        sAuthor=self.doc.getNodeText(self.node,'author')
        sFileName=self.doc.getNodeText(self.node,'filename')
        sFileName=fnUtil.getFilenameRel2BaseDir(sFileName,self.tmplDN)
        
        self.txtTitle.SetValue(sTitle)
        self.txtDesc.SetValue(sDesc)
        self.txtVersion.SetValue(sVersion)
        self.txtFilename.SetValue(sFileName)
        self.txtAuthor.SetValue(sAuthor)
        
        self.txtAttr.SetValue('')
        self.txtVal.SetValue('')
        
        # setup attributes
        self.lstAttrs.DeleteAllItems()
        self.selectedIdx=-1
        
        attrNode=self.doc.getChild(self.node,'attributes')
        self.attrs={}
        if attrNode is not None:
            for o in self.doc.getChilds(attrNode):
                sTagName=self.doc.getTagName(o)
                sVal=self.doc.getNodeText(attrNode,sTagName)
                sPlace=self.doc.getAttribute(o,'place')
                k=sTagName+':'+sPlace
                self.attrs[k]=[sVal,sPlace,o]
        self.__refreshAttrLst__()
        #keys=self.attrs.keys()
        #keys.sort()
        #for k in keys:
        #    skeys=string.split(k,':')
        #    index = self.lstAttrs.InsertImageStringItem(sys.maxint, skeys[0], -1)
        #    self.lstAttrs.SetStringItem(index, 1, self.attrs[k][0])
        pass
    def GetNode(self,node):
        if node is None:
            return ''
        
        sTitle=self.txtTitle.GetValue()
        sDesc=self.txtDesc.GetValue()
        sVersion=self.txtVersion.GetValue()
        sFilename=self.txtFilename.GetValue()
        sAuthor=self.txtAuthor.GetValue()
                
        self.doc.setNodeText(node,'title',sTitle)
        self.doc.setNodeText(node,'description',sDesc)
        self.doc.setNodeText(node,'version',sVersion)
        self.doc.setNodeText(node,'filename',sFilename)
        self.doc.setNodeText(node,'author',sAuthor)
        
        attrsNode=self.doc.getChild(node,'attributes')
        if attrsNode is None:
            attrsNode=self.doc.createSubNode(node,'attributes')
        keys=self.attrs.keys()
        keys.sort()
        if attrsNode is not None:
            for o in self.attrNodes2Del:
                try:
                    self.doc.deleteNode(o)
                except:
                    pass
        for sAttrName in keys:
            obj=self.attrs[sAttrName]
            if obj is None:
                if attrsNode is not None:
                    sk=string.split(sAttrName,':')
                    if len(sk)>1:
                        o=self.doc.getChildAttr(attrsNode,sk[0],
                            'place',sk[1])
                    else:
                        o=self.doc.getChild(attrsNode,sAttrName)
                    self.doc.deleteNode(o)
            else:
                sk=string.split(sAttrName,':')
                if len(sk)>1:
                    self.doc.setNodeTextAttr(attrsNode,sk[0],obj[0],
                        'place',obj[1])
                else:
                    self.doc.setNodeTextAttr(attrsNode,sAttrName,obj[0],
                        'place',obj[1])
            
        self.doc.AlignNode(node,iRec=2)
        return sTitle
        
    def OnGcbbApplyButton(self, event):
        self.GetNode(self.node)
        wx.PostEvent(self,vgpDocumentChanged(self,self.node,self.lang))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode(self.doc,{'document':self.node},self.ids)
        event.Skip()

    def OnGcbbSetButton(self,event):
        #index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, -1)
        sAttr=self.txtAttr.GetValue()
        sAttrStrip=string.strip(sAttr)
        sAttrStrip=fnUtil.replaceSuspectChars(sAttrStrip)
        sAttrStrip=string.replace(sAttrStrip,'_','')
        if sAttrStrip!=sAttr:
            self.txtAttr.SetValue(sAttrStrip)
            self.txtAttr.SetStyle(0, len(sAttrStrip), wx.TextAttr("RED", "YELLOW"))
            return
        else:
            self.txtAttr.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
        it=self.lstAttrs.GetItem(self.selectedIdx,0)
        sAttr=it.m_text
        keys=self.attrs.keys()
        keys.sort()
        sAttrKey=keys[self.selectedIdx]
        
        sAttr=self.txtAttr.GetValue()
        sVal=self.txtVal.GetValue()
        sPlace=self.txtPlace.GetValue()
        k=sAttr+':'+sPlace
        if sAttrKey!=k:
            # delete old
            lst=self.attrs[sAttrKey]
            if lst[2] is not None:
                self.attrNodes2Del.append(lst[2])
                #self.attrNode.removeChild(lst[2])
                #wx.PostEvent(self,vgpDocumentChanged(self,None,self.lang))
                
            attrs={}
            for sk in keys:
                if sk!=sAttrKey:
                    attrs[sk]=self.attrs[sk]
            self.attrs=attrs
            self.attrs[k]=[sVal,sPlace,None]
        else:
            l=self.attrs[k]
            self.attrs[k]=[sVal,sPlace,l[2]]
            
        #try:
        #    lst=self.attrs[sAttr]
        #    self.attrs[sAttr]=None
            #if self.attrNode is not None:
            #    self.attrNode.removeChild(lst[2])
        #except:
        #    pass
        #self.attrs[sAttr]=None
        self.__refreshAttrLst__()
        #self.lstAttrs.SetStringItem(self.selectedIdx, 0, self.txtAttr.GetValue())
        #self.lstAttrs.SetStringItem(self.selectedIdx, 1, self.txtVal.GetValue())
        
            
        #event.skip()
    def __refreshAttrLst__(self):
        self.lstAttrs.DeleteAllItems()
        keys=self.attrs.keys()
        keys.sort()
        for k in keys:
            skeys=string.split(k,':')
            index = self.lstAttrs.InsertImageStringItem(sys.maxint, skeys[0], -1)
            self.lstAttrs.SetStringItem(index, 1, self.attrs[k][0])
        self.txtAttr.SetValue('')
        self.txtVal.SetValue('')
        self.txtPlace.SetValue('')
        
    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttrs.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttrs.GetItem(idx,1)
        sAttrVal=it.m_text
        self.txtAttr.SetValue(sAttrName)
        self.txtVal.SetValue(sAttrVal)
        keys=self.attrs.keys()
        keys.sort()
        k=keys[idx]
        lst=self.attrs[k]
        self.txtPlace.SetValue(lst[1])
        
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnGcbbAddButton(self, event):
        sAttr=self.txtAttr.GetValue()
        sAttrStrip=string.strip(sAttr)
        sAttrStrip=fnUtil.replaceSuspectChars(sAttrStrip)
        sAttrStrip=string.replace(sAttrStrip,'_','')
        if sAttrStrip!=sAttr:
            self.txtAttr.SetValue(sAttrStrip)
            self.txtAttr.SetStyle(0, len(sAttrStrip), wx.TextAttr("RED", "YELLOW"))
            return
        else:
            self.txtAttr.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
        sAttr=self.txtAttr.GetValue()
        sPlace=self.txtPlace.GetValue()
        k=sAttr+':'+sPlace
        #i=self.lstAttrs.FindItem(-1,sAttr,False)
        i=-1
        if i<0:
            self.txtAttr.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            sVal=self.txtVal.GetValue()
            #sPlace=self.txtPlace.GetValue()
            #k=sAttr+':'+sPlace
            
            self.attrs[k]=[sVal,sPlace,None]
            self.__refreshAttrLst__()
            #index = self.lstAttrs.InsertImageStringItem(sys.maxint, sAttr, -1)
            #self.lstAttrs.SetStringItem(index, 1, self.attrs[k][0])
            self.txtAttr.SetValue('')
            self.txtVal.SetValue('')
            self.txtPlace.SetValue('')
        else:
            self.txtAttr.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedIdx>=0:
            it=self.lstAttrs.GetItem(self.selectedIdx,0)
            sAttrName=it.m_text
            
            self.lstAttrs.DeleteItem(self.selectedIdx)
            keys=self.attrs.keys()
            keys.sort()
            k=keys[self.selectedIdx]
            lst=self.attrs[k]
            if lst[2] is not None:
                self.attrNodes2Del.append(lst[2])
                #self.attrNode.removeChild(lst[2])
                #wx.PostEvent(self,vgpDocumentChanged(self,self.node,self.lang))
            attrs={}
            for sk in keys:
                if sk!=k:
                    attrs[sk]=self.attrs[sk]
            self.attrs=attrs
            self.txtAttr.SetValue('')
            self.txtVal.SetValue('')
            self.txtPlace.SetValue('')
        event.Skip()

    def OnGcbbBrowseFNButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("all files (*.*)|*.*|XML files (*.xml)|*.xml"), wx.OPEN)
        try:
            fn=self.txtFilename.GetValue()
            fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if fn is not None:
                dlg.SetFilename(fn)
            else:
                dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtFilename.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def OnGcbbBrowseValButton(self,event):
        if self.dlgBrowseAttr is None:
            self.dlgBrowseAttr=vDocBrowseAttrDialog(self)
            self.dlgBrowseAttr.Centre()
        iRet=self.dlgBrowseAttr.ShowModal()
        if iRet>0:
            sAttr=self.dlgBrowseAttr.GetSelAttr()
            self.txtVal.SetValue('$'+sAttr+'$')
        #self.dlgBrowseAttr.Destroy()
        event.Skip()
    def OnGcbbOpenTmplButton(self,event):
        sFileName=self.txtFilename.GetValue()
        sFileName=fnUtil.getAbsoluteFilenameRel2BaseDir(sFileName,self.tmplDN)
        if len(sFileName)<=0:
            return
        sExts=string.split(sFileName,'.')
        sExt=sExts[-1]
        if len(sExt)<=0:
            return
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        mime = fileType.GetMimeType()
        cmd = fileType.GetOpenCommand(sFileName, mime)
        wx.Execute(cmd)
        event.Skip()
    def Lock(self,flag,locker=''):
        #vtLog.CallStack('')
        #print flag
        if flag==False:
            self.txtTitle.Enable(True)
            self.txtDesc.Enable(True)
            self.txtAttr.Enable(True)
            self.txtVal.Enable(True)
            self.txtPlace.Enable(True)
            self.txtAuthor.Enable(True)
            self.txtFilename.Enable(True)
            self.txtVersion.Enable(True)
            self.lstAttrs.Enable(True)
            self.gcbbSet.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDel.Enable(True)
            self.gcbbBrowseFN.Enable(True)
            self.gcbbBrowseValue.Enable(True)
            self.gcbbOpen.Enable(True)
            self.numCtrlCellWidth.Enable(True)
            self.numCtrlCellHeight.Enable(True)
        else:
            self.txtTitle.Enable(False)
            self.txtDesc.Enable(False)
            self.txtAttr.Enable(False)
            self.txtVal.Enable(False)
            self.txtPlace.Enable(False)
            self.txtAuthor.Enable(False)
            self.txtFilename.Enable(False)
            self.txtVersion.Enable(False)
            self.lstAttrs.Enable(False)
            self.gcbbSet.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDel.Enable(False)
            self.gcbbBrowseFN.Enable(False)
            self.gcbbBrowseValue.Enable(False)
            self.gcbbOpen.Enable(False)
            self.numCtrlCellWidth.Enable(False)
            self.numCtrlCellHeight.Enable(False)
            
