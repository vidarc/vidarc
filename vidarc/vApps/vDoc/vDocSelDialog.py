#Boa:Dialog:vDocSelDialog
#----------------------------------------------------------------------------
# Name:         vDocSelDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vDocSelDialog.py,v 1.4 2007/09/07 05:37:31 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

from vidarc.vApps.vDoc.vXmlDocTree import *

import images

def create(parent):
    return vDocSelDialog(parent)

[wxID_VDOCSELDIALOG, wxID_VDOCSELDIALOGGCBBAPPLY, 
 wxID_VDOCSELDIALOGGCBBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vDocSelDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VDOCSELDIALOG, name=u'vDocSelDialog',
              parent=prnt, pos=wx.Point(313, 70), size=wx.Size(294, 365),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Document Select')
        self.SetClientSize(wx.Size(286, 338))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(56, 304), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VDOCSELDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(152, 304),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VDOCSELDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.vgpTree=vXmlDocTree(self,wx.NewId(),
                pos=(4,4),size=(278, 220),style=0,name="vgpTree")
        self.vgpTree.SetConstraints(
            LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        
        self.chcLstLevel = wx.CheckListBox(choices=[],
              id=wx.NewId(), name=u'chcLstLevel', parent=self,
              pos=wx.Point(4, 230), size=wx.Size(278, 63), style=0)

        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        if self.doc is not None:
            self.doc.DelConsumer(self)
    def Destroy(self):
        if self.doc is not None:
            self.doc.DelConsumer(self)
            self.doc=None
        wx.Dialog.Destroy(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        #docs=node.getElementsByTagName('document')
        self.node=node
        self.docNode=self.vgpTree.GetRootNode()
        lvStrs=[]
        if node is not None:
            while not(self.doc.isSame(node,self.docNode)):
                sPath=self.doc.getNodeText(node,'path')
                lvStrs.append(sPath)
                node=self.doc.getParent(node)
            lvStrs.reverse()
        
        self.chcLstLevel.Clear()
        self.iLenLevel=len(lvStrs)
        self.lstLevel=[]
        for i in range(0,len(lvStrs)):
            self.chcLstLevel.Append(lvStrs[i])
            #if self.mode=='complete':
            self.chcLstLevel.Check(i)
        #if self.mode=='last':
        #    if len(lvStrs)>0:
        #        self.chcLstLevel.Check(len(lvStrs)-1)
        sels=self.chcLstLevel.GetSelections()
        
    def SetLanguage(self,lang):
        self.lang=lang
        self.vgpTree.SetLanguage(self.lang)
    def Stop(self):
        self.vgpTree.Stop()
    def IsBusy(self):
        return False
    def Clear(self):
        self.docNode=None
    def SetDoc(self,doc,bNet=False):
        self.chcLstLevel.Clear()
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        self.vgpTree.SetDoc(doc,bNet)
        self.vgpTree.SetNode(None)
    def GetSelNode(self):
        return self.node
    def GetDocPath(self):
        iCount=self.chcLstLevel.GetCount()
        lstDocPath=[]
        for idx in range(0,iCount):
            if self.chcLstLevel.IsChecked(idx):
                lstDocPath.append(self.chcLstLevel.GetString(idx))
        return lstDocPath
    def OnGcbbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
