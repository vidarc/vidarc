#Boa:FramePanel:Panel1
#----------------------------------------------------------------------------
# Name:         vDocGrpInfoNoteBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDocGrpInfoNoteBook.py,v 1.5 2014/06/20 08:01:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#import ColorPanel
import images_lang
from vidarc.vApps.vDoc.vDocGrpInfoPanel import *
from vidarc.vApps.vDoc.vDocGrpInfoControlPanel import *
from vidarc.vApps.vDoc.vDocGrpInfoSecPanel import *
from vidarc.vApps.vDoc.vDocInfoPanel import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_PANEL1] = [wx.NewId() for _init_ctrls in range(1)]

class vDocGrpInfoNoteBook(wx.Notebook,vtXmlDomConsumer,vtInputModifyFeedBack):
    def _init_ctrls(self, prnt):
        wx.Notebook.__init__(self, style=wx.TAB_TRAVERSAL, name='', parent=prnt, pos=wx.DefaultPosition, id=wxID_PANEL1, size=wx.Size(100, 50))

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vDoc')
        vtXmlDomConsumer.__init__(self)
        vtInputModifyFeedBack.__init__(self,lWidgets=[],
                    lEvent=[],bEnableMark=True,bEnableMod=True)
        self._init_ctrls(parent)
        #self.SetPadding(wx.Size(2,2))
        self.pnDoc=None
        self.tmplDN=None
        self.pnCtrl=None
        self.doc=None
        
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.Move(pos)
        self.SetSize(size)

    def SetHuman(self,grp,usr):
        self.pnCtrl.SetHuman(grp,usr)
        pass
    def AddDocumentPanel(self,parent):
        self.pnDoc=vDocInfoPanel(parent,0,wx.Point(0,0),wx.Size(200,100),0,u"")
        EVT_VGPDOCUMENT_CHANGED(self.pnDoc,self.OnDocumentChanged)
        self.pnDoc.SetTemplateBaseDir(self.tmplDN)
        return self.pnDoc
    def SetLanguages(self,languages,languageIds,pnCtrl=None):
        languages=[u'English',u'Deutsch']
        il = wx.ImageList(16, 16)
        for i in range(0,len(languages)):
            f=getattr(images_lang, 'getLang%02dBitmap' % i)
            bmp = f()
            il.Add(bmp)
        il.Add(vtArt.getBitmap(vtArt.Acl))
        idx=il.Add(images_lang.getApplyMultipleBitmap())
        self.AssignImageList(il)
        
        if pnCtrl is None:
            sName="vDocGrpInfoControlPanel"
            pnCtrl=vDocGrpInfoControlPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=sName)
            EVT_VGPDOC_GRP_INFO_CHANGED(pnCtrl,self.OnDocGrpChanged)
            EVT_VGPDOC_GRP_INFO_CANCELED(pnCtrl,self.OnDocGrpCanceled)
        self.AddPage(pnCtrl,_(u'General'),False,idx)
        self.pnCtrl=pnCtrl
        
        self.pnAcl=vDocGrpInfoSecPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name='vDocGrpInfoSec')
        self.AddPage(self.pnAcl,'ACL',False,idx-1)
        
        self.vpgDocGrpInfos=[]
        for i in range(0,len(languages)):
            sName="vDocGrpInfo%sPanel"%languages[i]
            panel=vDocGrpInfoPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=sName)
            panel.SetLanguage(languageIds[i])
            self.AddPage(panel,languages[i],i==0,i)
            self.vpgDocGrpInfos.append(panel)
            EVT_VGPDOCUMENT_SELECTED(panel,self.OnDocumentSelected)
            EVT_VGPDOC_CHANGED(panel,self.OnDocChanged)
            EVT_VGPDOC_GRP_INFO_CHANGED(panel,self.OnDocGrpChanged)
            EVT_VGPDOC_GRP_INFO_CANCELED(panel,self.OnDocGrpCanceled)
        
        self.languageIds=[]
        for id in languageIds:
            self.languageIds.append(id)
    def SetTemplateBaseDir(self,dn):
        self.tmplDN=dn
        if self.pnDoc is not None:
            self.pnDoc.SetTemplateBaseDir(self.tmplDN)
        
        for panel in self.vpgDocGrpInfos:
            panel.SetTemplateBaseDir(self.tmplDN)
    def SetNode(self,node):
        #self.SetSelection(0)
        self.node=node
        #sel=self.GetSelection()
        #self.vgpDocGrpInfos[sel].SetNode(node)
        self.pnCtrl.SetNode(node)
        oAcl=self.doc.GetReg('docgroupACL')
        bOk=self.doc.IsAclOk('docgroupACL',self.doc.ACL_MSK_WRITE)
        if bOk:
            self.pnAcl.SetNode(node)
        else:
            self.pnAcl.SetNode(None)
        bOkRd=self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_READ)
        bOkWr=self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE)
        for p in self.vpgDocGrpInfos:
            if bOkRd:
                p.SetNode(node)
                if bOkWr:
                    p.Enable(True)
                else:
                    p.Enable(False)
            else:
                p.SetNode(None)
        if self.pnDoc is not None:
            self.pnDoc.SetNode(None,'en')
    def SetNetDocHuman(self,netDoc):
        self.pnCtrl.SetNetDocHuman(netDoc)
        self.pnAcl.SetNetDocHuman(netDoc)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        pass
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet=bNet)
        if doc is None:
            return
        self.pnCtrl.SetDoc(doc,bNet)
        self.pnDoc.SetDoc(doc,bNet)
        self.pnAcl.SetDoc(doc,bNet)
        for p in self.vpgDocGrpInfos:
            p.SetDoc(doc,bNet)
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.pnDoc.Lock(False)
            else:
                self.pnDoc.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.pnDoc.Lock(False)
        evt.Skip()
    
    def OnPageChanged(self, event):
        old = event.GetOldSelection() -2
        sel = self.GetSelection() -2
        if old >= 0:
            oldPanel=self.vpgDocGrpInfos[old]
            selPanel=self.vpgDocGrpInfos[sel]
            bOkRd=self.doc.IsAclOk('document',self.doc.ACL_MSK_READ)
            bOkWr=self.doc.IsAclOk('document',self.doc.ACL_MSK_WRITE)
            if self.pnDoc is not None:
                if bOkRd:
                    self.pnDoc.SetNode(selPanel.GetSelectedDoc(),
                                    selPanel.GetLanguage())
                    if bOkWr:
                        self.pnDoc.Enable(True)
                    else:
                        self.pnDoc.Enable(False)
                else:
                    self.pnDoc.SetNode(None,
                        selPanel.GetLanguage())
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def OnDocGrpChanged(self,event):
        bOkWr=self.doc.IsNodeAclOk(self.node,self.doc.ACL_MSK_WRITE)
        if bOkWr==False:
            return
        self.pnCtrl.GetNode(self.node)
        self.pnAcl.GetNode(self.node)
        #if self.pnDoc is not None:
        #    self.pnDoc.GetNode(selPanel.GetSelectedDoc(),
        #            selPanel.GetLanguage())
        for panel in self.vpgDocGrpInfos:
            panel.GetNode(self.node)
        wxPostEvent(self,vgpDocGrpInfoChanged(self,self.node,None))
        event.Skip()
    def OnDocGrpCanceled(self,event):
        self.pnCtrl.SetNode(self.node)
        for panel in self.vpgDocGrpInfos:
            panel.SetNode(self.node)
        wxPostEvent(self,vgpDocGrpInfoCanceled(self,self.node,None))
        event.Skip()
    def Apply(self):
        vtLog.vtLngCurWX(vtLog.INFO,'key:%s'%(self.doc.getKey(self.node)),self)
        bOkWr=self.doc.IsNodeAclOk(self.node,self.doc.ACL_MSK_WRITE)
        if bOkWr==False:
            return
        self.pnCtrl.GetNode(self.node)
        self.pnAcl.GetNode(self.node)
        for panel in self.vpgDocGrpInfos:
            panel.GetNode(self.node)
    def Cancel(self):
        vtLog.vtLngCurWX(vtLog.INFO,'key:%s'%(self.doc.getKey(self.node)),self)
        self.pnCtrl.SetNode(self.node)
        self.pnAcl.SetNode(self.node)
        for panel in self.vpgDocGrpInfos:
            panel.SetNode(self.node)
    def OnDocumentSelected(self,event):
        if self.pnDoc is not None:
            self.pnDoc.SetNode(event.GetNode(),event.GetLanguage())
        event.Skip()
    def OnDocumentChanged(self,event):
        sel = self.GetSelection() -2
        selPanel=self.vpgDocGrpInfos[sel]
        selPanel.RefreshDocuments()
        wxPostEvent(self,vgpDocChanged(self,None,None))
        event.Skip()
    def OnDocChanged(self,event):
        wxPostEvent(self,vgpDocChanged(self,None,None))
        event.Skip()
        
