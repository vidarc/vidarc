#----------------------------------------------------------------------------
# Name:         vXmlNodeUserReferenced.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080413
# CVS-ID:       $Id: vXmlNodeUserReferenced.py,v 1.1 2008/04/15 23:53:53 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vHum.vUserNodeReferenced import vUserNodeReferenced
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeUserReferencedPanel import vXmlNodeUserReferencedPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeUserReferenced(vUserNodeReferenced):
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeUserReferencedPanel
        else:
            return None
