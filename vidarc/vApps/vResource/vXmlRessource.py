#----------------------------------------------------------------------------
# Name:         vXmlRessource.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlRessource.py,v 1.21 2008/04/15 23:54:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg

# add node import here
from vidarc.vApps.vRessource.vXmlNodeRessourceRoot import vXmlNodeRessourceRoot
import vidarc.vApps.vRessource.__register__

import types

from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
#from vidarc.ext.report.veRepDoc import veRepDoc
#from vidarc.ext.report.veRepDocTmpl import veRepDocTmpl
#from vidarc.ext.report.veRepDocTmplLaTex import veRepDocTmplLaTex

VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x026IDAT8\x8d\x95\x93\xdfKSq\x18\xc6?g;*\xd9\xc5\x0esns\xd3S\x93P\x02\
\x8fP\xd0\x8f\xbb\xa0\xa0p\x9c\x0b/\n!bf\x89\xce.L\xcbD\xa4\x0b\xbbP\xf3\x98\
\xb0 \x9a\t]\xe5\xc59 \xd1\xc5a\xfe\x03\xddD\x97Z\xf7M\xa5\xd6dt\xd1\x0f(\
\xdd\xdb\xd5\x16\x9be\xf5\xc2\x17\xbe<\xdf\xef\xfb\xf2\xf0<\xcf\xab(\x1e/\
\xd5e\xdc\xbf({@`mbE\xa9\xc6<\xff\xfa\xf1wX\xc5\x80\xde\xbeK\xd2\xd0\x1c\x13\
)\xb6\x8b57\xbb\x87\x8157+Rl\x97\x80~X\x06\x06\xfa~\xbd+\x1e/\x8a\xc7K\xe0\
\xd0Q9\x93\x10\xd1\xb4\x19\x89DC\x12\x8b\xb5\x8am\xdbb\xdb\xb6\xc4b\xad\x12\
\x89\x86D\xd3\xa6\xe5\xdc5\x11\xb5V\x95R\x9fR\xd2@\x8amR_\xd7\xcb\xf0\xe8\
\x1b\x1a\x1bO\xd0\xd2\xd2\xcc\xe2b\x1a\x80dr\x88\x8d\x8dM\xf2\xf9\xd7\xa4\
\x16\x8e\xb1+S\xec\x14\xbf(\x00\x9e\x92`\x96u\x19-p\x97`\xf0\x14\x91H\x13\
\xf9|\x0e\xc3\xe8\xc00:\xc8\xe7sD"M\x04\x83\xa79\xe8\xbb\xc5\xb3\xe5\'e\xb1=\
\xa5\xcb2\xeb\xfc\xf8\x1eB\xd7\xa3\x14\n\xdbd2.\xa6\xd9\x85iv\x91\xc9\xb8\
\x14\n\xdb\xe8z\x94o_5f\xb2/\xcaN)\x9dVOY\x90I\xbd\x9b\xa5\xa5\xa7\x18\x86\
\x81ivaY\xf3\x00\x8c\x8f\xdf\xc1uWY[[gp\xb0\x9f\xe9w\xcf\xf7\xb7\xf1\x7f\xaa\
\x82\xc1\xfb\xf9\x97\xa4\xd3\x8f\xc8\xe5>\x90\xc9\xb8\x8c\x8c\xdc\x04 \x95zH\
<n\x12\n5\x91H\\\xe5\xc8\xd4\x85J\x06k\x13+\xca\x15:\xa8\xa9\xcd\x91\xcdn\
\xe1\xf7\x07\x88\xc7M\\w\x15\xd7]%\x1e7\xf1\xfb\x03d\xb3[\x1c\xa8\xff\xc4\
\xa4\xde]\x0eV\x95\x8d\t\x86G\xdf\x12l<I\x8b\xdeL:\xfd\x18\x80\xa1\xa1\x1bld\
7\xf9\x98\x7fEj\xe18\xbb\xdccg\xf7\xb3R\x11$\xbf\xde&g\xaf\x8b\xf8|\x0f\xa4!\
\xa0I8\x1c\x16\xc7q\xc4q\x1c\t\x87\xc3\xd2\x10\xd0\xc4\xe7\x9b\x97\xf3I\x11\
\xc5K9H\xe5\x01\x83\xc9~Q\xebTQU\x9f8\x8e-\x9dVO\xc5q\x1c[T\xd5\'5u^\x19\x1b\
\xbb\xbd7\x89\xd5U\xbd\x91\x7f]\xa6\xfd\x9a\xff\x84\x01\xfc\x04n=\xec{]r\xb6\
\xe7\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlRessource(vtXmlDomReg):
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='ressourceroot'
    NODE_ATTRS=[
            #('name',None,'name',None),
            ]
    APPL_REF=['vPrjDoc','vDoc','vPrj','vHum','vGlobals','vMsg']
    def __init__(self,attr='id',skip=[],synch=False,appl='vRessource',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            
            #oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #oArea=vXmlNodeArea()
            #oUsr=vUserNode()
            #self.RegisterNode(oAddr,False)
            #self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            #self.RegisterNode(oArea,False)
            #self.RegisterNode(oUsr,False)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
            oRoot=vXmlNodeRessourceRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeRessourceRoot()
            self.RegisterNode(oRoot,True)
            
            vidarc.vApps.vRessource.__register__.RegisterNodes(self,bRegAsRoot=True)
            
            oRessource=self.GetReg('ressource')
            oRessourceHuman=self.GetReg('ressourceHuman')
            self.LinkRegisteredNode(oRoot,oRessource)
            self.LinkRegisteredNode(oRoot,oRessourceHuman)
            self.LinkRegisteredNode(oRessource,oRessource)
            
            #oAddr=vtXmlNodeAddr()
            #self.LinkRegisteredNode(oRessource,oRessource)
            #self.LinkRegisteredNode(oCust,oAddr)
            self.dResource={}
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
        #self.appl='ressource'
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'ressources')
    #def New(self,revision='1.0',root='ressourceroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'ressources')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
            #self.__checkReqBase__()
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        #    pass
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtXmlDomReg.Build(self)
        self.BuildStateMachine()
        self.dResource={}
        self.BuildResDict()
    def BuildStateMachine(self):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
    def BuildSrv(self):
        self.BuildStateMachine()
    def BuildResDict(self,node=None,bStore=True):
        dRes={}
        self.__buildResDict__(node,dRes)
        if bStore==True:
            self.SetResDict(dRes)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dResource:%s;dRes:%s'%(vtLog.pformat(self.dResource),vtLog.pformat(dRes)),self.appl)
        return dRes
    def SetResDict(self,dResource):
        self.dResource=dResource
    def __buildResDict__(self,node=None,dRes={}):
        if node is None:
            node=self.getBaseNode()
            self.procChildsKeys(node,self.__buildResDict__,dRes)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'resource:%s'%vtLog.pformat(self.dResource),self.appl)
            return 0
        else:
            sTag=self.getTagName(node)
            o=self.GetRegisteredNode(sTag)
            if o is None:
                return 0
            if o.IsSkip():
                return 0
            try:
                if hasattr(o,'IsResource2Draw'):
                    if o.IsResource2Draw():
                        #o.Add2ResourceDict(node,self.dResource)
                        #d={}
                        #self.dResource[o.GetDescription()]=d
                        dResChild=o.Add2ResourceDict(node,dRes)
                        if dResChild is not None:
                            #self.procChildsKeys(node,self.__buildResDict__,dResChild)
                            self.procChildsKeys(node,self.__buildResDict__,dRes)
                    else:
                        self.procChildsKeys(node,self.__buildResDict__,dRes)
                else:
                    self.procChildsKeys(node,self.__buildResDict__,dRes)
            except:
                vtLog.vtLngTB(self.appl)
                self.procChildsKeys(node,self.__buildResDict__,dRes)
            return 0
    def __getResourceData__(self,dResource,iYear,iMon,iMonCount):
        keys=dResource.keys()
        for k in keys:
            t=type(k)
            if t==types.UnicodeType or t==types.StringType:
                if k.startswith('__'):
                    continue
            lst=dResource[k]
            if type(lst)==types.ListType:
                #dResult[k]=lst
                
                dYear=lst[1]
                #lst[1]={}
                iId=lst[2]
                node=self.getNodeByIdNum(iId)
                if node is not None:
                    sTag=self.getTagName(node)
                    o=self.GetRegisteredNode(sTag)
                    iY=iYear
                    iM=iMon
                    for iMonRepeat in xrange(iMonCount):
                        bAdd=True
                        if iY in dYear:
                            if iM in dYear[iY]:
                                bAdd=False
                        if bAdd:
                            o.GetResourceData(node,iY,iM,1,lst[1],lst)
                        iM+=1
                        if iM>12:
                            iM=1
                            iY+=1
                #dResult[k]=lst
            elif type(lst)==types.DictionaryType:
                self.__getResourceData__(lst,iYear,iMon,iMonCount)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'resources year:%d month:%d;%s'%(iYear,iMon,vtLog.pformat(dResult)),self)
    def __getUnique__(self,d,tag):
        if tag in d:
            #tagUnique=''.join([tag,'[%d]'%0])
            #if  tagUnique not in d:
            #    d[tagUnique]=d[tag]
            #    lst=d[tag]
            #    d[tag]=None
                #return tagUnique,lst
            for i in xrange(1,1000):
                tagUnique=''.join([tag,'[%d]'%i])
                if tagUnique not in d:
                    return tagUnique
        #else:
        #    lst=self.__getResInfoLst__()
        #    d[tag]=lst
        return tag
    def GetResourceData(self,iYear,iMon,iMonCount,dRes=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'year:%d month:%d;dRes:%s'%(iYear,
                    iMon,vtLog.pformat(dRes)),self.GetOrigin())
        #dResult={}
        if dRes is None:
            dRes=self.dResource
        # check ressource data to build
        self.__getResourceData__(dRes,iYear,iMon,iMonCount)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'resources year:%d month:%d;%s'%
                        (iYear,iMon,vtLog.pformat(dRes)),self.GetOrigin())
        #vtLog.CallStack('')
        #print vtLog.pformat(dRes)
        iY=iYear
        iM=iMon
        docGlb=self.GetNetDoc('vGlobals')
        dHoliday={}
        dValid={}
        for iMonRepeat in xrange(iMonCount):
            if iY in dValid:
                ddValid=dValid[iY]
            else:
                ddValid={}
                dValid[iY]=ddValid
                if docGlb is not None:
                    docGlb.GetHolidaysDict('',iY,dHoliday)
            ddValid[iM]=True
            iM+=1
            if iM>12:
                iM=1
                iY+=1
        fLimOvLd=None
        try:
            nodeCfg=self.getChildByLst(self.getBaseNode(),['cfg','ResGenCfg'])
            if nodeCfg is not None:
                oReg=self.GetRegByNode(nodeCfg)
                fLimOvLd=oReg.GetLimitOverLoadFloat(nodeCfg)
        except:
            pass
        dVal={}
        dVal['__holidays']=dHoliday
        for k,d in dRes.iteritems():
            ddVal={}
            dVal[d['__label']]=ddVal
            lSum=d['__sum']
            lSum[1]={}
            dSum=lSum[1]
            ddVal['__sum']=lSum
            if fLimOvLd is not None:
                if '__limOvLd' not in ddVal:
                    ddVal['__limOvLd']=fLimOvLd
                    dAux={'__limOvLd':lSum[0]*(fLimOvLd/100.0)}
                    lSum.append(dAux)
            for kk,dd in d.iteritems():
                t=type(kk)
                if t==types.UnicodeType or t==types.StringType:
                    if kk.startswith('__'):
                        continue
                if -1 in dd:
                    dVac=dd[-1][1]
                    for year,dYear in dVac.iteritems():
                        if year not in dValid:
                            continue
                        if year in dSum:
                            dSumYear=dSum[year]
                        else:
                            dSumYear={}
                            dSum[year]=dSumYear
                        for mon,dMon in dYear.iteritems():
                            if mon not in dValid[year]:
                                continue
                            if mon in dSumYear:
                                dSumMon=dSumYear[mon]
                            else:
                                dSumMon={}
                                dSumYear[mon]=dSumMon
                            for day,l in dMon.iteritems():
                                dSumMon[day]=l
                dPrjSum={}
                if 0 in dd:
                    fLimit=dd[0][0]
                    dPrj=dd[0][1]
                    for fidPrj,lPrj in dPrj.iteritems():
                        bPrjAdd=False
                        dPrjData=lPrj[1]
                        for year,dYear in dPrjData.iteritems():
                            if year not in dValid:
                                continue
                            if year in dPrjSum:
                                dSumYear=dPrjSum[year]
                            else:
                                dSumYear={}
                                dPrjSum[year]=dSumYear
                            for mon,dMon in dYear.iteritems():
                                if mon not in dValid[year]:
                                    continue
                                if mon in dSumYear:
                                    dSumMon=dSumYear[mon]
                                else:
                                    dSumMon={}
                                    dSumYear[mon]=dSumMon
                                for day,v in dMon.iteritems():
                                    if day in dSumMon:
                                        dSumMon[day]=dSumMon[day]+v
                                    else:
                                        dSumMon[day]=v
                                    bPrjAdd=True
                        if bPrjAdd:
                            ddVal[lPrj[0]]=[fLimit,dPrjData,lPrj[2]]
                if kk==-2:
                    fLimit=dd[0]
                    dPrj=dd[1]
                    for fidPrj,lPrj in dPrj.iteritems():
                        bPrjAdd=False
                        dPrjData=lPrj[1]
                        if len(lPrj)>2:
                            try:
                                dInfo=lPrj[2]
                                fScale=100.0/dInfo['__lim']
                            except:
                                fScale=1.0
                        for year,dYear in dPrjData.iteritems():
                            if year not in dValid:
                                continue
                            if year in dPrjSum:
                                dSumYear=dPrjSum[year]
                            else:
                                dSumYear={}
                                dPrjSum[year]=dSumYear
                            for mon,dMon in dYear.iteritems():
                                if mon not in dValid[year]:
                                    continue
                                if mon in dSumYear:
                                    dSumMon=dSumYear[mon]
                                else:
                                    dSumMon={}
                                    dSumYear[mon]=dSumMon
                                for day,l in dMon.iteritems():
                                    if day in dSumMon:
                                        #l=dSumMon[day]
                                        v=0.0
                                        for t in l:
                                            #l.append(t)
                                            v+=t[-1]#*fScale
                                        dSumMon[day]=dSumMon[day]+v*fScale
                                    else:
                                        v=0.0
                                        for t in l:
                                            v+=t[-1]#*fScale
                                        dSumMon[day]=v*fScale#[t for t in v]
                                    bPrjAdd=True
                        if bPrjAdd:
                            sPrjLbl=self.__getUnique__(ddVal,lPrj[0])
                            
                            ddVal[sPrjLbl]=[fLimit,dPrjData,lPrj[2]]
                for year,dYear in dPrjSum.iteritems():
                    if year in dSum:
                        dSumYear=dSum[year]
                    else:
                        dSumYear={}
                        dSum[year]=dSumYear
                    for mon,dMon in dYear.iteritems():
                        if mon in dSumYear:
                            dSumMon=dSumYear[mon]
                        else:
                            dSumMon={}
                            dSumYear[mon]=dSumMon
                        
                        for day,v in dMon.iteritems():
                            if day not in dSumMon:
                                dSumMon[day]=v
        #print 'dVal'
        #print vtLog.pformat(dVal)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'resources year:%d month:%d;%s'%
                        (iYear,iMon,vtLog.pformat(dVal)),self.GetOrigin())
        return dVal
        return dRes#self.dResource
        keys=self.dResource.keys()
        #for k in keys:
        #    lst=self.dResource[k]
        #    lst[1]={}
        for k in keys:
            lst=self.dResource[k]
            if type(lst)!=types.ListType():
                continue
            lst[1]={}
            iId=lst[2]
            node=self.getNodeByIdNum(iId)
            if node is not None:
                sTag=self.getTagName(node)
                o=self.GetRegisteredNode(sTag)
                o.GetResourceData(node,iYear,iMon,lst[1],lst)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'resources year:%d month:%d;%s'%(iYear,iMon,vtLog.pformat(self.dResource)),self)
        return self.dResource
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
