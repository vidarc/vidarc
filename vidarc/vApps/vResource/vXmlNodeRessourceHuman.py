#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceHuman.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vXmlNodeRessourceHuman.py,v 1.8 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vRessource.vXmlNodeRessource import *

try:
    if vcCust.is2Import(__name__):
        from vXmlNodeRessourceHumanPanel import vXmlNodeRessourceHumanPanel
        #from vidarc.vApps.vRessource.vXmlNodeRessourceHumanEditDialog import *
        #from vidarc.vApps.vRessource.vXmlNodeRessourceHumanAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeRessourceHuman(vXmlNodeRessource):
    #NODE_ATTRS=[
    #        ('tag',None,'tag',None),
    #        ('name',None,'name',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    def __init__(self,tagName='ressourceHuman'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vXmlNodeRessource.__init__(self,tagName=tagName)
    def GetDescription(self):
        return _(u'ressource humans')
    # ---------------------------------------------------------
    # specific
    def IsResource2Draw(self):
        return False
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.GetConfigurableRoleTranslationLst()
    def GetGroupId(self,node):
        return self.GetChildForeignKey(node,'group','fid',appl='vHum')
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smVacation')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x98IDAT8\x8d\xa5\x931K\x03A\x10\x85\xbf\xbdhlTb\x11\x11A\xc4"E\n\
\xab\xeb\x04\xc1\xe8\x05\xf4\'\x08)\xd2hzQ\xb73\xe7\x0f\xc8\x0f\xc8U)\x04\
\xb1\x10-,O\x8d\xbd\xa7\x85E\x0c\xc4\xda\x88v\xa6M\xc6bs\x97\xc4C\x0b\x1dX\
\x96\x99y\xf3\xf6\xed\xf2V)+\xc1\x7f\xc2\x028<\xd8\x17\xe9uEz]\xd9X\xcfI\xd8\
\x9cH\x8eK*5\x1d\xe5\x1b\xeb\xb9\x08wx\xb0o\xea}\x05\xb2[\x15\x01\x11@\x94\
\x95`ksK\xde\xdaoR\xadV%=\x9b\x96\x10\x07\xd2\xc7\x1a\x9c\x15\xb2{%p\x9c<Zk\
\x002\x99\x0c\xc9\x89$K\x8bK\xa4fR\x00h\xadq\x9c<^\xe9\xdb\x15\x1c\xc7\x01\
\x14\xb6m\x93\xcd.\x03\xf0\xfarJ\xa5R\xe1\xec\xfc\x8c\x8f\xf7\x0f\x00\xb2\
\xd9el\xdb\x06T\x7f\x06\xc6\x00\xaeon\x95\x91g\xa2\xbc\xb7*\xe5\xed5\x8ew]\
\x16\x80\x8b\x15p\xdb\x83\xfe\xd0\x8c!\x08#<\x9dN\x1d\xac:\xe5+\xd7\xe4\x0f.\
kG\xdf0\xc3W\xe8?\xa6*\x16\x0b#M\xda\xaeY\x97\x83R\xb1X@Y\t\x15#\xf8kD\x04\
\xd2\xebJ\xadv\x02@\xfd>\x0e\xec|\x9a\xbdV;Az\xdd\xe8=F\x144\x1aO\x00\xdc=\
\x18\x89\x9d\xa6\xa9\xb7\x9aPi\x99Z\x88\x19!\x18v\xdf\xc8\xa9\xcf\xd0\xb9\
\x81\xb9\xf9x/\x9c\x19\x03\xf0}\x1f\x10\x82 \x1f\x03NN\x01S\x03\x85A\x10\x00\
\x82\xef+\xe3\xe2\x9f\xac,\x8f\xc8\xab\x8b\x88\x87\xc8\x0e\xbf[Yk\x8dWR\x0c;\
\xec.\x88\xcb\x0e\x1d\xeb\x95Tdy\xf5\xdf\xef\xfc\x05YU\xaf\x90\x89\xae\xf0\
\xbb\x00\x00\x00\x00IEND\xaeB`\x82'
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceHumanEditDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodeResHum',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceHumanAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodeResHum',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeRessourceHumanPanel
        else:
            return None
