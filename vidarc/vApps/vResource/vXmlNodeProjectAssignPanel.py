#Boa:FramePanel:vXmlNodeProjectAssignPanel
#----------------------------------------------------------------------------
# Name:         vNodeXXXPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vXmlNodeProjectAssignPanel.py,v 1.4 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.vApps.vPrj.vXmlPrjInputTree
import vidarc.tool.input.vtInputTree
import vidarc.ext.state.veStateInput
import vidarc.tool.time.vtTimeInputDateTime
import vidarc.tool.input.vtInputTextMultiLineML
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEPROJECTASSIGNPANEL, wxID_VXMLNODEPROJECTASSIGNPANELLBLDESC, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLEND, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLNAME, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLPRJ, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLSTART, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLSTATE, 
 wxID_VXMLNODEPROJECTASSIGNPANELLBLTAG, 
 wxID_VXMLNODEPROJECTASSIGNPANELTXTNAME, 
 wxID_VXMLNODEPROJECTASSIGNPANELTXTTAG, wxID_VXMLNODEPROJECTASSIGNPANELVIDESC, 
 wxID_VXMLNODEPROJECTASSIGNPANELVIEND, wxID_VXMLNODEPROJECTASSIGNPANELVIPER, 
 wxID_VXMLNODEPROJECTASSIGNPANELVIPRJ, wxID_VXMLNODEPROJECTASSIGNPANELVISTART, 
 wxID_VXMLNODEPROJECTASSIGNPANELVISTATE, 
] = [wx.NewId() for _init_ctrls in range(16)]

class vXmlNodeProjectAssignPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsName, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsPrj, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsStart, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsEnd, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsState, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDesc, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viDesc, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsState_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblState, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viState, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsStart_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblStart, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viStart, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsEnd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblEnd, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viEnd, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsPrj_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrj, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viPrj, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viPer, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtTag, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=4)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsStart = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsEnd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsState = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPrj = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_bxsStart_Items(self.bxsStart)
        self._init_coll_bxsEnd_Items(self.bxsEnd)
        self._init_coll_bxsState_Items(self.bxsState)
        self._init_coll_bxsPrj_Items(self.bxsPrj)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPROJECTASSIGNPANEL,
              name=u'vXmlNodeProjectAssignPanel', parent=prnt, pos=wx.Point(405,
              257), size=wx.Size(232, 227), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(224, 200))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLTAG,
              label=u'tag', name=u'lblTag', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(70, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.txtTag = wx.TextCtrl(id=wxID_VXMLNODEPROJECTASSIGNPANELTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(74, 0),
              size=wx.Size(149, 21), style=wx.NO_BORDER, value=u'')
        self.txtTag.SetMinSize(wx.Size(-1, -1))
        self.txtTag.Enable(False)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wx.Point(0, 25),
              size=wx.Size(70, 21), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.txtName = wx.TextCtrl(id=wxID_VXMLNODEPROJECTASSIGNPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(74, 25),
              size=wx.Size(149, 21), style=wx.NO_BORDER, value=u'')
        self.txtName.SetMinSize(wx.Size(-1, -1))
        self.txtName.Enable(False)

        self.lblPrj = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLPRJ,
              label=u'project', name=u'lblPrj', parent=self, pos=wx.Point(0,
              90), size=wx.Size(70, 26), style=wx.ALIGN_RIGHT)

        self.viPrj = vidarc.vApps.vPrj.vXmlPrjInputTree.vXmlPrjInputTree(id=wxID_VXMLNODEPROJECTASSIGNPANELVIPRJ,
              name=u'viPrj', parent=self, pos=wx.Point(74, 90), size=wx.Size(74,
              26), style=0)

        self.viPer = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEPROJECTASSIGNPANELVIPER,
              integer_width=4, maximum=100.0, minimum=0.0, name=u'viPer',
              parent=self, pos=wx.Point(152, 90), size=wx.Size(70, 26),
              style=0)

        self.lblDesc = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLDESC,
              label=u'description', name=u'lblDesc', parent=self,
              pos=wx.Point(0, 50), size=wx.Size(70, 36), style=wx.ALIGN_RIGHT)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VXMLNODEPROJECTASSIGNPANELVIDESC,
              name=u'viDesc', parent=self, pos=wx.Point(74, 50),
              size=wx.Size(149, 36), style=0)
        self.viDesc.SetMinSize(wx.Size(-1, -1))

        self.lblStart = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLSTART,
              label=u'start', name=u'lblStart', parent=self, pos=wx.Point(0,
              120), size=wx.Size(70, 24), style=wx.ALIGN_RIGHT)
        self.lblStart.SetMinSize(wx.Size(-1, -1))

        self.viStart = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEPROJECTASSIGNPANELVISTART,
              name=u'viStart', parent=self, pos=wx.Point(74, 120),
              size=wx.Size(149, 24), style=0)
        self.viStart.SetMinSize(wx.Size(-1, -1))

        self.lblEnd = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLEND,
              label=u'end', name=u'lblEnd', parent=self, pos=wx.Point(0, 148),
              size=wx.Size(70, 24), style=wx.ALIGN_RIGHT)
        self.lblEnd.SetMinSize(wx.Size(-1, -1))

        self.viEnd = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEPROJECTASSIGNPANELVIEND,
              name=u'viEnd', parent=self, pos=wx.Point(74, 148),
              size=wx.Size(149, 24), style=0)
        self.viEnd.SetMinSize(wx.Size(-1, -1))

        self.lblState = wx.StaticText(id=wxID_VXMLNODEPROJECTASSIGNPANELLBLSTATE,
              label=u'state', name=u'lblState', parent=self, pos=wx.Point(0,
              176), size=wx.Size(70, 24), style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize(wx.Size(-1, -1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VXMLNODEPROJECTASSIGNPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(74, 176),
              size=wx.Size(149, 24), style=0)
        self.viState.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        #self.docHum=None
        
        self.viPrj.SetAppl('vPrj')
        self.viPer.SetTagName('percentage')
        self.viDesc.SetTagName('description')
        self.viStart.SetTagName('startDt')
        self.viEnd.SetTagName('endDt')
        self.viState.SetNodeAttr('projectAssign','prjState')
        
        self.viStart.SetDftTime('00:00')
        self.viEnd.SetDftTime('23:55')
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.txtTag.SetValue('')
            self.txtName.SetValue('')
            self.viPrj.Clear()
            self.viPer.Clear()
            self.viDesc.Clear()
            self.viStart.Clear()
            self.viEnd.Clear()
            self.viState.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            #self.docHum=dd['doc']
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.viPrj.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viPrj.SetDoc(doc)
        #self.viPer.SetDoc(doc)
        self.viDesc.SetDoc(doc)
        self.viStart.SetDoc(doc)
        self.viEnd.SetDoc(doc)
        self.viState.SetDoc(doc)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            self.txtTag.SetValue(self.objRegNode.GetTag(self.node))
            self.txtName.SetValue(self.objRegNode.GetName(self.node))
            self.viPrj.SetNode(self.node)
            self.viPer.SetNode(self.node,self.doc)
            self.viDesc.SetNode(self.node)
            self.viStart.SetNode(self.node)
            self.viEnd.SetNode(self.node)
            self.viState.SetNode(self.node)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viPrj.GetNode(node)
            self.viPer.GetNode(node,self.doc)
            self.viDesc.GetNode(node)
            self.viStart.GetNode(node)
            self.viEnd.GetNode(node)
            self.viState.GetNode(node)
            
            self.viState.SetNode(node)  # Refresh
            self.objRegNode.SetDuration(node,'0.0')
            self.viStart.SetNode(node)
            self.viEnd.SetNode(node)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viPrj.Enable(False)
            self.viPer.Enable(False)
            self.viDesc.Enable(False)
            self.viStart.Enable(False)
            self.viEnd.Enable(False)
            self.viState.Enable(False)
        else:
            # add code here
            self.viPrj.Enable(True)
            self.viPer.Enable(True)
            self.viDesc.Enable(True)
            self.viStart.Enable(True)
            self.viEnd.Enable(True)
            self.viState.Enable(True)
