#Boa:FramePanel:vRessourceMainPanel
#----------------------------------------------------------------------------
# Name:         vRessourceMainPanel.py
# Purpose:       
#               dervied from vRessourceMainFrame.py
# Author:       Walter Obweger
#
# Created:      20060605
# CVS-ID:       $Id: vRessourceMainPanel.py,v 1.14 2008/04/15 23:53:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
import vidarc.tool.xml.vtXmlNodeRegListBook
import vidarc.tool.draw.vtDrawCanvasPanel
import vidarc.tool.draw.vtDrawCalendarPanel
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors
import getpass,time

import vidarc.vApps.vHum.impMin

try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.log.vtLogFileViewerFrame import *
    from vidarc.tool.xml.vtXmlNodeRegListBook import *
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.vRessource.vXmlRessourceTree  import *
    from vidarc.vApps.vRessource.vNetRessource import *
    from vidarc.vApps.vRessource.vRessourceListBook import *
    from vidarc.vApps.vRessource.vRessourceDiagramDialog import vRessourceDiagramDialog
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
    
    import vidarc.tool.InOut.fnUtil as fnUtil
    
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
    from vidarc.vApps.vPrj.vNetPrj import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    from vidarc.tool.vtThread import vtThread
    
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
    
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    from vXmlNodeRessourceResultPanel import vXmlNodeRessourceResultPanel
except:
    vtLog.vtLngTB('import')

import vidarc.vApps.vRessource.images as imgRessource

VERBOSE=0

def create(parent):
    return vRessourceMainPanel(parent)

[wxID_VRESSOURCEMAINPANEL, wxID_VRESSOURCEMAINPANELPNDATA, 
 wxID_VRESSOURCEMAINPANELSLWNAV, wxID_VRESSOURCEMAINPANELSLWTOP, 
 wxID_VRESSOURCEMAINPANELVIREGSEL, wxID_VRESSOURCEMAINPANELVTDRES, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VRESSOURCEMAINFRAMEMNIMPORTITEM_IMPORT_CSV, 
 wxID_VRESSOURCEMAINFRAMEMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VRESSOURCEMAINFRAMEMNEXPORTITEM_EXPORT_CSV, 
 wxID_VRESSOURCEMAINFRAMEMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(2)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgRessource.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VDOCMAINFRAMEMNVIEWITEM_LANG, wxID_VDOCMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VDOCMAINFRAMEMNLANGITEM_LANG_DE, wxID_VDOCMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VRESSOURCEMAINFRAMEMNHELPITEMS1, 
 wxID_VRESSOURCEMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VRESSOURCEMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VRESSOURCEMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

[wxID_VRESSOURCEMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK] = [wx.NewId() for _init_coll_mnTools_Items in range(1)]

class vRessourceMainPanel(wx.Panel,vtXmlDomConsumer):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VRESSOURCEMAINPANEL,
              name=u'vRessourceMainPanel', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 515), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(811, 488))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VRESSOURCEMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              489), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VRESSOURCEMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VRESSOURCEMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VRESSOURCEMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VRESSOURCEMAINPANELPNDATA,
              name=u'pnData', parent=self, pos=wx.Point(208, 208),
              size=wx.Size(592, 272),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.vtdRes = vidarc.tool.draw.vtDrawCalendarPanel.vtDrawCalendarPanel(draw_grid=False,
              flip=False, grid_x=20, grid_y=20,
              id=wxID_VRESSOURCEMAINPANELVTDRES, is_gui_wx=True, label_len=8,
              name=u'vtdRes', parent=self.pnData, pos=wx.Point(8, 8),
              size=wx.Size(576, 256), style=0)
        self.vtdRes.SetConstraints(LayoutAnchors(self.vtdRes, True, True, True,
              True))

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VRESSOURCEMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.slwTop, pos=wx.Point(4, 4),
              size=wx.Size(600, 189), style=0)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        self.dlgDiagram=None
        
        self.widLogging=vtLog.GetPrintMsgWid(self)
        self.thdDrawing=vtThread(self)
        
        self.xdCfg=vtXmlDom(appl='vRessourceCfg',audit_trail=False)
        
        appls=['vRessource','vPrjDoc','vDoc','vPrj','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        
        self.netRessource=self.netMaster.AddNetControl(vNetRessource,'vRessource',synch=True,verbose=0,
                                                audit_trail=True)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netRessource.SetDftLanguages()
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netRessource.SetDftLanguages()
        self.netRessource.SetLang(vtLgBase.getApplLang())
        
        EVT_NET_SEC_XML_MASTER_FINISHED(self.netMaster,self.OnMasterFin)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vRessourceMaster')
        self.bAutoConnect=True
        self.OpenCfgFile()
        
        self.vgpTree=vXmlRessourceTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,489),style=0,name="trRessource",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_NET_XML_GOT_CONTENT(self.netRessource,self.OnGotContent)
        
        self.vgpTree.SetDoc(self.netRessource,True)
        #self.vgpTree.EnableImportMenu(True)
        #self.vgpTree.EnableExportMenu(True)
        self.vgpTree.SetAnalyse([
                ( 'diagram', _(u'open diagram') , vtArt.getBitmap(vtArt.Draw) , (self.ShowDiagram ,)) ,
                ( 'refresh_diagram', _(u'refresh diagram') , vtArt.getBitmap(vtArt.Synch) , (self.RefreshDiagram ,)) ,
                ])
        self.vtdRes.SetDoc(self.netRessource,True)
        
        oDoc=self.netRessource.GetReg('Doc')
        oDoc.SetTreeClass(vXmlRessourceTree)
        
        self.nbRes=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRes")
        oRes=self.netRessource.GetRegisteredNode('ressource')
        self.nbRes.SetDoc(self.netRessource,True)
        self.viRegSel.AddWidgetDependent(self.nbRes,oRes)
        self.nbRes.Show(False)
        
        self.nbResHum=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbResHum")
        oResHum=self.netRessource.GetRegisteredNode('ressourceHuman')
        self.nbResHum.SetDoc(self.netRessource,True)
        self.viRegSel.AddWidgetDependent(self.nbResHum,oResHum)
        self.nbResHum.Show(False)
        
        oVac=self.netRessource.GetRegisteredNode('vacation')
        pnCls=oVac.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnVac")
        pn.SetDoc(self.netRessource,True)
        self.viRegSel.AddWidgetDependent(pn,oVac)
        pn.Show(False)
        
        pn=self.viRegSel.CreateRegisteredPanel('projectAssign',self.netRessource)
        pn.Show(False)
        
        pn=self.viRegSel.CreateRegisteredPanel('ressourceAssign',self.netRessource)
        pn.Show(False)
        
        pn=self.viRegSel.CreateRegisteredPanel('userReferenced',self.netRessource)
        pn.Show(False)
        
        #pn=self.viRegSel.CreateRegisteredPanel('dataCollector',self.netRessource)
        self.nbCollect=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbCollect")
        oCollect=self.netRessource.GetRegisteredNode('dataCollector')
        self.nbCollect.SetDoc(self.netRessource,True)
        self.nbCollect.SetRegNode(oCollect,bIncludeBase=True)
        self.viRegSel.AddWidgetDependent(self.nbCollect,oCollect)
        self.nbCollect.Show(False)
        
        self.nbDoc=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbDoc")
        oDoc=self.netRessource.GetRegisteredNode('Doc')
        self.nbDoc.SetRegNode(oDoc)
        self.nbDoc.SetDoc(self.netRessource,True)
        self.viRegSel.AddWidgetDependent(self.nbDoc,oDoc)
        self.nbDoc.Show(False)
        
        #pnCls=oDoc.GetPanelClass()
        #pn=pnCls(self.viRegSel,wx.NewId(),
        #        pos=(8,8),size=(576, 176),style=0,name="pnDoc")
        #pn.SetDoc(self.netRessource,True)
        #self.viRegSel.AddWidgetDependent(pn,oDoc)
        #pn.Show(False)
        pn=vXmlNodeRessourceResultPanel(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnResssourceRes")
        oUserMember=self.netRessource.GetRegisteredNode('ressourceHumanMember')
        pn.SetRegNode(oUserMember)
        pn.SetDoc(self.netRessource,True)
        self.viRegSel.AddWidgetDependent(pn,oUserMember)
        
        self.viRegSel.SetDoc(self.netRessource,True)
        self.viRegSel.SetRegNode(oRes,bIncludeBase=True)
        self.viRegSel.SetRegNode(oResHum,bIncludeBase=False)
        self.viRegSel.SetRegNode(oVac)
        self.viRegSel.SetRegNode(oDoc,bIncludeBase=True)
        self.viRegSel.SetNetDocs(self.netRessource.GetNetDocs())
        #self.nbRes.SetRegNode(oRes,bIncludeBase=True)
        #self.nbRes.Lock(True)
        #self.nbData.SetNetDocHuman(self.netHum,True)
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        #self.CreateNew()
        self.SetSize(size)
    def GetDocMain(self):
        return self.netRessource
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        try:
            self.tbMain.AddSeparator()
            id=wx.NewId()
            self.dToolBar['reqVacation']=id
            #self.tbMain.DoAddTool(id,_(u'request'),
            #        imgRessource.getVactionBitmap(),
            #        wx.NullBitmap,
            #        shortHelp=_(u'request vaction'))
            self.tbMain.AddLabelTool(id,_(u'request'),
                    imgRessource.getVactionBitmap(),
                    shortHelp=_(u'request vaction'))
            #self.tbMain.AddSimpleTool(id,
            #        imgRessource.getVactionBitmap(),
            #        _(u'request'),
            #        _(u'request vaction'))
            self.tbMain.Bind(wx.EVT_TOOL, self.OnReqVac, id=id)
        except:
            vtLog.vtLngTB(self.GetName())
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnReqVac(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            oLogin=self.netRessource.GetLogin()
            lGrp=oLogin.GetGrpIds()
            lRes=[]
            oReg=self.netRessource.GetReg('ressourceHuman')
            self.netRessource.procChildsKeys(self.netRessource.getBaseNode(),
                        self.__procResHumMember__,oReg,lGrp,lRes)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lRes:%s'%(vtLog.pformat(lRes)),self)
            iLen=len(lRes)
            if iLen==0:
                dlg=vtmMsgDialog(self,
                                _(u'Vacation could not be added.\n\n properly missing group assignment'),
                                u'VIDARC ' + _(u'Ressource') + u' '+ _(u'Vacation request'),
                                wx.OK | wx.YES_DEFAULT |  wx.ICON_ERROR)
                dlg.ShowModal()
                dlg.Destroy()
                return
            elif iLen==1:
                pass
            else:
                vtLog.vtLngCurWX(vtLog.WARN,'login:%s;lGrps:%s'%
                        (repr(oLogin),vtLog.pformat(lRes)),self)
                vtLog.PrintMsg(_(u'you are present in more than one ressources, first is picked.'),self)
            idRes=lRes[0]
            node=self.netRessource.getNodeByIdNum(idRes)
            if node is not None:
                nodeMember=self.netRessource.getChild(node,'ressourceHumanMember')
                if nodeMember is not None:
                    oReg=self.netRessource.GetRegByNode(nodeMember)
                    if oReg.DoAddVac(self,nodeMember,oLogin.GetUsrId())==False:
                        dlg=vtmMsgDialog(self,
                                _(u'Vacation could not be added.\n\n properly missing group assignment'),
                                u'VIDARC ' + _(u'Ressource') + u' '+ _(u'Vacation request'),
                                wx.OK | wx.YES_DEFAULT |  wx.ICON_ERROR)
                        dlg.ShowModal()
                        dlg.Destroy()
                else:
                    vtLog.vtLngCurWX(vtLog.ERROR,
                            'idRes:%08d node human member not found'%
                            (idRes),self)
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'idRes:%08d node not found'%
                        (idRes),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def __procResHumMember__(self,node,oReg,lGrp,lRes):
        sTagName=self.netRessource.getTagName(node)
        if sTagName=='ressourceHuman':
            grpId=oReg.GetGroupId(node)
            if grpId in lGrp:
                lRes.append(self.netRessource.getKeyNum(node))
        return 0
    def Clear(self):
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def IsBusy(self):
        return self.thdDrawing.IsRunning()
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        #if self.node is not None:
        #    self.netRessource.endEdit(self.node)
        #if event.GetTreeNodeData() is not None:
        #    self.netRessource.startEdit(node)
        #else:
            # grouping tree item selected
        #    self.nbData.SetNode(None)
        #    pass
        event.Skip()
    def OnGotContent(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.vtdRes.SetValue(self.netRessource.GetResourceData)
    def __getSettings__(self):
        return
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netRessource.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netRessource.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netRessource.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netRessource.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netRessource.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netRessource.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
        
        self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netRessource.getBaseNode()
        self.baseSettings=self.netRessource.getChildForced(self.netRessource.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netRessource.createSubNode(self.netRessource.getRoot(),'settings')
        #    self.netRessource.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netRessource.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netRessource.ClearAutoFN()>0:
            self.netRessource.Open(fn)
            
            self.__getXmlBaseNodes__()
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netRessource.New()
        self.__getXmlBaseNodes__()
        #self.vgpTree.SetNode(None)
    def __setCfg__(self):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['top_sash']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        #event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Ressource module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Ressource'),desc,version
    def __showDiagram__(self):
        self.vtdRes.SetValue(self.netRessource.GetResourceData)
    def ShowDiagram(self,doc,node,*args,**kwargs):
        #self.thdDrawing.Do(self.__showDiagram__)
        try:
            if VERBOSE:
                vtLog.CallStack('')
            node=self.vgpTree.GetSelected()
            if self.dlgDiagram is None:
                self.dlgDiagram=vRessourceDiagramDialog(self)
                sz=self.dlgDiagram.GetSize()
                iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
                iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
                iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)*2
                iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)*2
                self.dlgDiagram.SetSize((sz[0],iMaxH))
                self.dlgDiagram.Centre()
                self.dlgDiagram.SetDoc(self.netRessource,True)
                
            self.dlgDiagram.SetNode(node)
            self.dlgDiagram.Show(True)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.netRessource.BuildResDict(node)
        #self.vtdRes.SetValue(self.netRessource.GetResourceData)
    def __refreshDiagram__(self):
        try:
            node=self.vgpTree.GetSelected()
            self.netRessource.BuildResDict(node)
            self.widLogging.thdFrm.CallBack(self.vtdRes.SetValue,self.netRessource.GetResourceData)
        except:
            vtLog.vtLngTB(self.GetName())
    def RefreshDiagram(self,doc,node,*args,**kwargs):
        self.vtdRes.Clear()
        self.netRessource.DoSafe(self.__refreshDiagram__)
    def OnMasterFin(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.vtdRes.SetValue(self.netRessource.GetResourceData)
        
