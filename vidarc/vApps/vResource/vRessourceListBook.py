#----------------------------------------------------------------------------
# Name:         vRessourceListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vRessourceListBook.py,v 1.2 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import images

import vidarc.vApps.vRessource.vXmlNodeRessourcePanel as vXmlNodeRessourcePanel
import vidarc.tool.input.vtInputAttrPanel as vtInputAttrPanel
import vidarc.tool.xml.vtXmlNodeLogPanel as vtXmlNodeLogPanel
import types
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer

VERBOSE=0

class vRessourceListBook(wx.Notebook,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)

    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent,id, pos, size, style, name)
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.verbose=VERBOSE
        self.doc=None
        
        edit=[u'vXmlNodeRessource',u'vtInputAttr',u'vtXmlNodeLog']
        editName=[_(u'General'),_(u'Attr'),_(u'Log')]
        self.lstTagName=[None,None,None]
        il = wx.ImageList(16, 16)
        for e in edit:
            s='%sPanel'%e
            f=getattr(images, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='%sPanel'%e
            f=getattr(eval('%sPanel'%e),'%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
        
    def Clear(self):
        for pn in self.lstPanel:
            pn.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)

    def Lock(self,flag):
        for pn in self.lstPanel:
            pn.Lock(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    def SetNetDocHuman(self,doc,bNet=False):
        try:
            idx=self.lstName.index('vtXmlNodeLog')
            self.lstPanel[idx].SetNetDocHuman(doc,bNet)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
        
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        #self.doc=doc
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDoc(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
        
    def SetNode(self,node):
        if self.__isModified__():
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vCustomer',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode()
                #wx.PostEvent(self,vgpDataChanged(self,self.node))
        self.SetModified(False)
        sTagName=self.doc.getTagName(node)
        if sTagName in ['ressource']:
            dNode={}
            for sName in self.lstTagName:
                dNode[sName]=None
            if sTagName=='ressource':
                dNode[None]=node
                #dNode['address']=self.doc.getChild(node,'address')
            #elif sTagName=='address':
            #    dNode['address']=node
            #    dNode[None]=self.doc.getParent(node)
            self.node=node
            
            i=0
            for pn in self.lstPanel:
                try:
                    tmp=dNode[self.lstTagName[i]]
                except:
                    tmp=None
                pn.SetNode(tmp)
                i+=1
            return
        else:
            for pn in self.lstPanel:
                pn.SetNode(None)
            return
        
        self.node=node
        
        
        
        i=0
        for pn in self.lstPanel:
            if self.lstTagName[i] is None:
                tmp=node
            elif node is None:
                tmp=node
            else:
                tmp=self.doc.getChild(node,self.lstTagName[i])
                if tmp is None:
                    try:
                        vtLog.vtLngCS(vtLog.WARN,'attribute node %s not found in;%s'%(self.lstTagName[i],repr(node)),
                                origin=self.GetName())
                        oReg=self.doc.GetRegisteredNode(self.lstTagName[i])
                        if oReg is not None:
                            o,tmp=self.doc.CreateNode(node,self.lstTagName[i])
                        else:
                            tmp=self.doc.createSubNode(node,self.lstTagName[i],False)
                            vtLog.vtLngCS(vtLog.INFO,'no registered node %s'%(self.lstTagName[i]),
                                    origin=self.GetName())
                            
                    except:
                        vtLog.vtLngTB(self.GetName())
            pn.SetNode(tmp)
            i+=1
        
    def GetNode(self):
        for pn in self.lstPanel:
            pn.GetNode()
            
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = event.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
    