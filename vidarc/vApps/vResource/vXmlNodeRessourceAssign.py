#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceAssign.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060912
# CVS-ID:       $Id: vXmlNodeRessourceAssign.py,v 1.6 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtDateTimeStartEnd
import vidarc.tool.time.vtTimeLimit as vtTimeLimit
from vidarc.ext.state.veStateAttr import veStateAttr

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeRessourceAssignPanel import vXmlNodeRessourceAssignPanel
        #from vXmlNodeRessourceAssignEditDialog import *
        #from vXmlNodeRessourceAssignAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeRessourceAssign(vtXmlNodeBase,veStateAttr):
    VERBOSE=0
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
            ('StartDt',None,'startDt',None),
            ('EndDt',None,'endDt',None),
            ('Duration',None,'duration',None),
            ('ResState',None,'resState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag']
    FMT_GET_4_TREE=[('Tag','')]
    def __init__(self,tagName='ressourceAssign'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeBase.__init__(self,tagName)
        veStateAttr.__init__(self,'smResAsgn','resState')
        self.oSMlinker=None
        self.dt=vtDateTime(True)
        self.dtDiff=vtDateTimeStartEnd(True)
    def GetDescription(self):
        return _(u'ressource assignment')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'description')
    def GetStartDt(self,node):
        return self.Get(node,'startDt')
    def GetEndDt(self,node):
        return self.Get(node,'endDt')
    def GetDuration(self,node):
        return self.Get(node,'duration')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    def GetPercentage(self,node):
        return self.Get(node,'percentage')
    def GetPercentageFloat(self,node):
        try:
            s=self.GetPercentage(node)
            if len(s)==0:
                return 0.0
            return float(s)
        except:
            return 0.0
    def GetLogin(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetName(nodePar)
    def GetSurName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetSurName(nodePar)
    def GetFirstName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetFirstName(nodePar)
    def GetPrjFIDStr(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def GetPrjInfos(self,node):
        fid=self.GetChildAttrStr(node,'project','fid')
        netDoc,n=self.doc.GetNode(fid)
        if n is None:
            return None
        oPrj=netDoc.GetRegByNode(n)
        return oPrj.AddValuesToLstByLst(n,atts=['Name','PrjNumber'])
    
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'description',val)
    def SetStartDt(self,node,val):
        self.Set(node,'startDt',val)
    def SetEndDt(self,node,val):
        self.Set(node,'endDt',val)
    def GetDataDict(self,node,dData,bModify=False):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,self.smAttr,'owner')
        nodeUsr=self.doc.GetNode(idOwner)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print node
            sDt=self.GetStartDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrS=self.dt.GetYear()
            iMhS=self.dt.GetMonth()
            iDyS=self.dt.GetDay()
            iHrS=self.dt.GetHour()
            iMnS=self.dt.GetMinute()
            sDt=self.GetEndDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrE=self.dt.GetYear()
            iMhE=self.dt.GetMonth()
            iDyE=self.dt.GetDay()
            iHrE=self.dt.GetHour()
            iMnE=self.dt.GetMinute()
            if self.VERBOSE:
                print 'start',iYrS,iMhS,iDyS,iHrS,iMnS
                print '  end',iYrE,iMhE,iDyE,iHrE,iMnE
                print '  mod',bModify
            for iYear in xrange(iYrS,iYrE+1):
                if iYear in dData:
                    dYear=dData[iYear]
                else:
                    dYear={}
                    dData[iYear]=dYear
                if iYear==iYrS:
                    iMhST=iMhS
                else:
                    iMhST=1
                iMhET=12
                if iYear==iYrE:
                    iMhET=iMhE
                for iMon in xrange(iMhST,iMhET+1):
                    if iMon in dYear:
                        dMon=dYear[iMon]
                    else:
                        dMon={}
                        dYear[iMon]=dMon
                    if iYear==iYrS:
                        if iMon>iMhS:
                            iDyST=1
                        else:
                            iDyST=iDyS
                    else:
                        iDyST=1
                    if iYear==iYrE:
                        if iMon<iMhE:
                            iDyET=31
                        else:
                            iDyET=iDyE
                    else:
                        iDyET=31
                    #for iDy in xrange(iDyS,iDyE+1):
                    for iDy in xrange(1,32):
                    #for iDy in xrange(iDyST,iDyET+1):
                        if self.VERBOSE:
                            print iYear,iMon,iDy
                        iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(nodeUsr,iYear,iMon,iDy)
                        #if (iDy<iDyS) or (iDy>iDyE):
                        #    iHrNrS,iMnNrS,iHrNrE,iMnNrE=0,0,0,0
                        if iLmNr>0:
                            bCalcDiff=True
                            if self.VERBOSE:
                                print 'start',iYrS,iYear,iMhS,iMon,iDyS,iDy,iHrS,iMnS
                            if iYrS==iYear and iMhS==iMon:
                                if iDyS==iDy:
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToLower(iHrS,iMnS,iHrNrS,iMnNrS)
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToHigher(iHrScal,iMnScal,iHrNrE,iMnNrE)
                                    #FIXME handle out of limit
                                    if bModify:
                                    #    iHr=iMn=-1
                                        sDt=self.GetStartDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrScal)
                                        self.dt.SetMinute(iMnScal)
                                        self.SetStartDt(node,self.dt.GetDateTimeStr())
                                        if self.VERBOSE:
                                            print '   ',sDt,
                                            print self.dt.GetDateTimeStr()
                                elif iDy<iDyS:
                                    iHrScal,iMnScal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrScal=iHrNrS
                                    iMnScal=iMnNrS
                            else:
                                iHrScal=iHrNrS
                                iMnScal=iMnNrS
                            
                            if self.VERBOSE:
                                print '  end',iYrE,iYear,iMhE,iMon,iDyE,iDy,iHrE,iMnE
                            if iYrE==iYear and iMhE==iMon:
                                if iDyE==iDy:
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToHigher(iHrE,iMnE,iHrNrE,iMnNrE)
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToLower(iHrEcal,iMnEcal,iHrNrS,iMnNrS)
                                    #FIXME handle out of limit
                                    if bModify:
                                    #    iHr=iMn=-1
                                        sDt=self.GetEndDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrEcal)
                                        self.dt.SetMinute(iMnEcal)
                                        self.SetEndDt(node,self.dt.GetDateTimeStr())
                                        if self.VERBOSE:
                                            print '   ',sDt,
                                            print self.dt.GetDateTimeStr()
                                elif iDy>iDyE:
                                    iHrEcal,iMnEcal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrEcal=iHrNrE
                                    iMnEcal=iMnNrE
                            else:
                                iHrEcal=iHrNrE
                                iMnEcal=iMnNrE
                            #if (iDy<iDyS) or (iDy>iDyE):
                            #    iHrScal,iMnScal,iHrEcal,iMnEcal=0,0,0,0
                            if iHrNrS>=0 and iHrNrE>=0:
                                self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrNrS,iMnNrS))
                                if iMnNrE==60:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE+1,0))
                                else:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE,iMnNrE))
                                self.dtDiff.CalcDiff()
                                iNrMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                if self.VERBOSE:
                                    print self.dtDiff.GetTimeDiffStr(' '),iNrMn
                                
                                if bCalcDiff==True:
                                    if self.VERBOSE:
                                        print '  ',iHrScal,iMnScal,iHrEcal,iMnEcal,self.dtDiff.GetMinuteDiff()
                                    self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrScal,iMnScal))
                                    if iMnEcal==60:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal+1,0))
                                    else:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal,iMnEcal))
                                    self.dtDiff.CalcDiff()
                                    iCalMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                    fPer=self.GetPercentageFloat(node)
                                    if fPer>0.0:
                                        iCalMn=iCalMn*(fPer/100.0)
                                else:
                                    iCalMn=0.0
                                if iDy in dMon:
                                    dMon[iDy]['calMn']+=iCalMn
                                else:
                                    dMon[iDy]={'calMn':iCalMn,'limMn':iNrMn,'limPer':iLmNr}
                                #fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                                if self.VERBOSE:
                                    print '    ','date',iYear,iMon,iDy
                                    #print '    ','dur',fDur
                                    print '    ',' res',dMon[iDy]
                            #    vtTimeLimit.addRange(l,iHrScal,iMnScal,iHrEcal,iMnEcal)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetDuration(self,node,val):
        if self.VERBOSE:
            vtLog.CallStack('')
        dData={}
        self.GetDataDict(node,dData,bModify=True)
        fDur=0.0
        for dYear in dData.itervalues():
            for dMon in dYear.itervalues():
                for dDay in dMon.itervalues():
                    iCalMn=dDay['calMn']
                    iNrMn=dDay['limMn']
                    iLmNr=dDay['limPer']
                    fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                    if self.VERBOSE:
                        print '    ','dur',fDur
        self.Set(node,'duration','%06.4f'%fDur)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetSurName(self,node,val):
        pass
    def SetFirstName(self,node,val):
        pass
    
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        if 'Login' in dDef:
            dVal['Login']=self.GetLogin(node)
        if 'SurName' in dDef:
            dVal['SurName']=self.GetSurName(node)
        if 'FirstName' in dDef:
            dVal['FirstName']=self.GetFirstName(node)
        if 'Allocation' in dDef:
            dVal['Allocation']=self.GetAllocation(node)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    def GetAllocation(self,node,**kwargs):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,self.smAttr,'owner')
        nodeUsr=self.doc.GetNode(idOwner)
        dData={}
        self.GetDataDict(node,dData)
        if 'data' in kwargs:
            self.dt.SetDateSmallStr(kwargs['date'])
        else:
            self.dt.Now()
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iDay=self.dt.GetDay()
        if iYear in dData:
            dYear=dData[iYear]
            if iMon in dYear:
                dMon=dYear[iMon]
                dCalMn={}
                lLimMn=[]
                #dPer={}
                #for iDy in xrange(1,31):
                #    iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(nodeUsr,iYear,iMon,iDy)
                #    dPer[iDy]=iLmNr
                #if iLmNr>0:
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                #print dPer
                fAllocRel=((fAlloc/fSum)*100.0)
                return fAllocRel
        return 0.0
    def GetAllocationDate(self,node):
        dData={}
        self.GetDataDict(node,dData)
        lCalMn=[]
        for iYear,dYear in dData.iteritems():
            for iMon,dMon in dYear.iteritems():
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                fAllocRel=((fAlloc/fSum)*100.0)
                lCalMn.append((iYear,iMon,fAllocRel))
        lCalMn.sort()
        d={}
        l=[]
        for iYear,iMon,fAllocRel in lCalMn:
            l.append('%04d-%02d'%(iYear,iMon))
        return l
    def GetAllocationVal(self,node):
        dData={}
        self.GetDataDict(node,dData)
        lCalMn=[]
        for iYear,dYear in dData.iteritems():
            for iMon,dMon in dYear.iteritems():
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                fAllocRel=((fAlloc/fSum)*100.0)
                lCalMn.append((iYear,iMon,fAllocRel))
        lCalMn.sort()
        d={}
        l=[]
        for iYear,iMon,fAllocRel in lCalMn:
            l.append('%4.1f'%fAllocRel)
        return l
    # ---------------------------------------------------------
    # state machine related
    def GetResState(self,node):
        return veStateAttr.GetStateAttrState(self,node)
    def GetRolesDict(self,node):
        tmp=self.doc.getParent(node)
        if tmp is None:
            return {}
        #tmp=self.doc.getParent(tmp)
        #if tmp is None:
        #    return {}
        #tmp=self.doc.getParent(tmp)
        #if tmp is None:
        #    return {}
        obj=self.doc.GetRegisteredNode(self.doc.getTagName(tmp))
        if obj is None:
            return {}
        return obj.GetRolesDict(tmp,self.GetOwner(node))
    def SetResState(self,node,val):
        veStateAttr.SetStateAttrState(self,node,val)
    def GetResourceData(self,node,iYear,iMon,dData):
        if self.VERBOSE:
            vtLog.CallStack('year:%d month:%d'%(iYear,iMon))
            print node
        dtS=self.GetStartDt(node)
        dtE=self.GetEndDt(node)
    
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('go, assigned to ressource'),None),
            'NoGo':(_('do not go, not assigned to ressource'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetStr(self.GetStartDt(node))
        except:
            pass
        dtS=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        try:
            self.dt.SetStr(self.GetEndDt(node))
        except:
            pass
        dtE=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        return _(u'ressource assignment')+' '+_('from:')+dtS+' '+_('to:')+dtE
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,func=None,*args,**kwargs):
        if self.doc is None:
            return
        oLogin=self.doc.GetLogin()
        if oLogin is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
            return
        if oLogin.GetUsrId()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
            return
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        if self.oSMlinker is not None:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
        else:
            oRun=None
        if oRun is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,self.smAttr),self)
            return
        if oRun.GetInitID()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
            return
        if node is not None:
            oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [
            ('Login',vtXmlFilterType.FILTER_TYPE_STRING),
            ('SurName',vtXmlFilterType.FILTER_TYPE_STRING),
            ('FirstName',vtXmlFilterType.FILTER_TYPE_STRING),
            #('Allocation',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        return _(name)
    def IsNotContained(self):
        return True
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02jIDAT8\x8du\x93_H\x93Q\x18\x87\x9fo\xae\xb9\xec\x0fi\x9a\xcdO\x9c\
\xfayaHI\x84+M\xe7W\x81\x84Wet#\x1a\xd3!Q\x11\x11H\xe0\x85nsP\x10z\x15\xd4\
\x8d\x85\x051oJ\xba\x93\x155\x8d$fQ\x04\xa61\xb7$\xb7\xb1TV\xa4\xa9\x0c\xd9\
\xe9B\x9b\xae\xd9\x0b\xe7\xea\xbc\xcfs~\xbc\xbcG\x924i\xfc[\xb6v\x8b\x00P\
\xeb\x9b\x01P]*v\x83\x1dG\xb7SJi\x964iI\xc7~\xc3*\xc4D\x8f\x10\x13=\xc2\xf3\
\xe8\x928\xef<&r^\xe4\x08\xc6\x11\\D\xd8\xedv\x91\xc4lN`k\xb7\x08\xdb\x95:\
\xc8\xae\x82\xf9QX\x9a\x01\xe0\xe9H\x84;_\xdf1^:\xce\xdc\xc39\xa4\xe1\xb4D\
\x92\x84\xe0/\xac)jL\\\xc6\x17\xa6\x05\xf3\xa3\\s\x0e 2\xf2)\xdd\x1be\xee\
\xb7\x1e\xb5\xbe\x99\x13\'OI\x00\x9a\xad\xe0p((:\x9a\x1a\x84\xa3\xe1\x1c\x04\
}H\xbb\nH\xd7\xa5\xf3\xe5c\x94\xb6j\x99\xdb\xd7\xd5Dj\xad\xad\xabS\xd8\x8a\
\x16!\xe8#\xfe\xacC\x90]\x89F\xce\x97\xe2\xcf\xef\n\xf2\x16\x008\xbbs\x16\
\x8f\xd7O\x93I\xe6\xde\xfdnVb\x1b3\xd4\x00\xf4{\\\xf8\x87\xdeCt\x05\xe2\x1f\
\xd6D\xeb0@m\xe3\x11\xdan]e`r\x10\xbd\x0e\xf4\xba\r\x81\x16@\xc9\x94\x19\x9b\
\xf5\xe2\x0ex\xa9\x9b2\xa1\x94(k\xb2,=\x00\xe1\xe5\\\\\xbd\xcdI`\x92\xa0\xa6\
\xdc\x8c\x7f\xca\xcf\xeb\x98\x97\x81\xc9A\xe4\xa0L\xcd\xba(|\xb0\x1cW\xef\
\xda>\xfcW061\x02\xc0r\x0c*rMl\xd7A\xff\xa7A\x94\xa0\x0c\x1eW\n\xac\xd5i\x01\
\xb11\x83\xacm2\x15\xfb*\xf0GC\xb8\xfd^\x86>\x87(\xdaa\x82,=e\xc7\xcbR^^\x8d\
\xad\xe2\xb4;\xc4t  4\x00J\x89\x82\xffG\x18\x80`\x14\xbe\xe7\x9d\xe6\xed\xb7\
\x10u{\x0e\xa5D\xf6E\xc0p\xd8\x8a\xb1\xd0Haq\xb1\xa4ut;%\xba:\x05\xde\x10\
\xc1\x9f\x90q\xa0\x05c\x81\x91xH\x06 4\xe3O\x82\x97\xf2\xdb0WUs\xc1b\x91\x80\
\xe4U~\xd0\xd7\'\x14E\x01\xe0\xe5\xcdv*\x8f\xea\xc9,\xd0\xe3}\xe3\xc6\x17\
\x81E\x83\x15UU\x13p\x8a`s\xd5\x16\xef\x17\xaf.\x9fal\xb7\x8f\xc7O\xdc,\x1a\
\xac\x98\xcdf,\xad\xadI?R\xbb%\r\x0c\x07"\x92\xe3W\x8e\x08\r\x0f\xb1\x9a\xdb\
\x82\xba\x05\x0c\xf0\x07e[\xee>"\noL\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceAssignEditDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodePrjAssign',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceAssignAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodePrjAssign',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeRessourceAssignPanel
        else:
            return None
