#Boa:FramePanel:vXmlNodeRessourcePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessource.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlNodeRessourcePanel.py,v 1.10 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputAttrValue
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTree
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
from gettext import *
from vidarc.tool.xml.vtXmlTree import *

from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.ext.state.veRoleDialog import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
import vidarc.vApps.vRessource.images as imgRes

import vidarc.vApps.vRessource.__config__ as __config__

VERBOSE=0

[wxID_VXMLNODERESSOURCEPANEL, wxID_VXMLNODERESSOURCEPANELCBREQRES, 
 wxID_VXMLNODERESSOURCEPANELCBROLE, wxID_VXMLNODERESSOURCEPANELLBLAREA, 
 wxID_VXMLNODERESSOURCEPANELLBLDESC, wxID_VXMLNODERESSOURCEPANELLBLLIMIT, 
 wxID_VXMLNODERESSOURCEPANELLBLNAME, wxID_VXMLNODERESSOURCEPANELLBLROLE, 
 wxID_VXMLNODERESSOURCEPANELLBLTAG, wxID_VXMLNODERESSOURCEPANELTXTTAG, 
 wxID_VXMLNODERESSOURCEPANELVIDESC, wxID_VXMLNODERESSOURCEPANELVILIMIT, 
 wxID_VXMLNODERESSOURCEPANELVINAME, wxID_VXMLNODERESSOURCEPANELVITRAREA, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vXmlNodeRessourcePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.viName, 3, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        #parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsName, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsArea, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbReqRes, 0, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP | wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.vitrArea, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRole, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbRole, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDesc, 1, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT)
        parent.AddWindow(self.viDesc, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4,
              flag=wx.EXPAND | wx.SHRINK | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.txtTag, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblLimit, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.viLimit, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsDesc_Items(self.bxsDesc)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODERESSOURCEPANEL,
              name=u'vXmlNodeRessourcePanel', parent=prnt, pos=wx.Point(250,
              259), size=wx.Size(316, 183), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(308, 156))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              22), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.txtTag = wx.TextCtrl(id=wxID_VXMLNODERESSOURCEPANELTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(77, 0), size=wx.Size(77,
              22), style=0, value=u'')
        self.txtTag.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLTAG,
              label=u'tag', name=u'lblTag', parent=self, pos=wx.Point(4, 0),
              size=wx.Size(69, 22), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.lblArea = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLAREA,
              label=u'area', name=u'lblArea', parent=self, pos=wx.Point(4, 86),
              size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.lblLimit = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLLIMIT,
              label=u'limit', name=u'lblLimit', parent=self, pos=wx.Point(158,
              0), size=wx.Size(69, 22), style=wx.ALIGN_RIGHT)
        self.lblLimit.SetMinSize(wx.Size(-1, -1))

        self.viLimit = vidarc.tool.input.vtInputFloat.vtInputFloat(default=100,
              fraction_width=1, id=wxID_VXMLNODERESSOURCEPANELVILIMIT,
              integer_width=4, maximum=9999, minimum=0.0, name=u'viLimit',
              parent=self, pos=wx.Point(231, 0), size=wx.Size(77, 22), style=0)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODERESSOURCEPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(77, 22),
              size=wx.Size(231, 30), style=0)

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VXMLNODERESSOURCEPANELVIDESC,
              name=u'viDesc', parent=self, pos=wx.Point(77, 52),
              size=wx.Size(231, 34), style=0)

        self.vitrArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODERESSOURCEPANELVITRAREA,
              name=u'vitrArea', parent=self, pos=wx.Point(77, 86),
              size=wx.Size(77, 30), style=0)

        self.lblDesc = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLDESC,
              label=u'Desc', name=u'lblDesc', parent=self, pos=wx.Point(4, 52),
              size=wx.Size(69, 34), style=wx.ALIGN_RIGHT)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.lblRole = wx.StaticText(id=wxID_VXMLNODERESSOURCEPANELLBLROLE,
              label=_(u'role'), name=u'lblRole', parent=self, pos=wx.Point(158,
              86), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblRole.SetMinSize(wx.Size(-1, -1))

        self.cbRole = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Role),
              id=wxID_VXMLNODERESSOURCEPANELCBROLE, name=u'cbRole', parent=self,
              label=_(u'role'),pos=wx.Point(231, 86), size=wx.Size(77, 30), style=0)
        self.cbRole.Bind(wx.EVT_BUTTON, self.OnCbRoleButton,
              id=wxID_VXMLNODERESSOURCEPANELCBROLE)

        self.cbReqRes = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Desk),
              id=wxID_VXMLNODERESSOURCEPANELCBREQRES,
              label=_(u'Request Ressource'), name=u'cbReqRes', parent=self,
              pos=wx.Point(4, 120), size=wx.Size(300, 32), style=0)
        self.cbReqRes.Bind(wx.EVT_BUTTON, self.OnCbReqResButton,
              id=wxID_VXMLNODERESSOURCEPANELCBREQRES)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.txtTag,self.cbRole],
                lEvent=[(self.txtTag,wx.EVT_TEXT)],
                iSecLvEdit=__config__.SEC_LEVEL_LIMIT_ADMIN)
        self.dt=vtDateTime(True)
        
        self.dlgRole=veRoleDialog(self)
        self.bRoleShowOnce=False
        self.vitrArea.SetTagNames('mainArea','name')
        self.vitrArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.vitrArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.vitrArea.SetTagNames2Base(['customers','cfg'])
        self.viLimit.SetTagName('limit')
        #self.viMng.SetTagNames('manager','name')
        
        #self.cbReqRes.SetBitmapLabel(imgRes.getvXmlNodeRessourceBitmap())
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            #self.viMng.SetDocTree(dd['doc'],dd['bNet'])
            self.dlgRole.SetDocHum(dd['doc'],dd['bNet'])
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.viName.IsModified():
            return True
        if self.viDesc.IsModified():
            return True
        if self.vitrArea.IsModified():
            return True
        #if self.viMng.IsModified():
        #    return True
        return False
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viName.Close()
        self.viDesc.Close()
        self.vitrArea.Close()
        self.viLimit.Close()
        #self.viMng.Close()
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.Close()
            self.dRoles={}
            self.txtTag.SetValue('')
            self.viName.Clear()
            self.viDesc.Clear()
            self.vitrArea.Clear()
            self.viLimit.Clear()
            #self.viMng.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        self.viName.SetDoc(doc)
        self.viDesc.SetDoc(doc)
        self.vitrArea.SetDoc(doc,bNet)
        #self.viMng.SetDoc(doc)
        self.dlgRole.SetDoc(doc,bNet)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.txtTag.SetValue(self.doc.getNodeText(self.node,'tag'))
            self.viName.SetNode(self.node,'name')
            self.viDesc.SetNode(self.node,'description')
            self.viLimit.SetNode(self.node,self.doc)
            self.vitrArea.SetNode(self.node)
            self.dRoles=self.objRegNode.GetRolesDict(self.node)
            #self.viMng.SetNode(self.node)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            self.doc.setNodeText(node,'tag',self.txtTag.GetValue())
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            self.viLimit.GetNode(node,self.doc)
            self.vitrArea.GetNode(node)
            #self.viMng.GetNode(node)
            self.objRegNode.SetRolesDict(node,self.dRoles)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            self.txtTag.Enable(False)
            self.viName.Enable(False)
            self.viDesc.Enable(False)
            self.vitrArea.Enable(False)
            self.viLimit.Enable(False)
            #self.viMng.Enable(False)
            self.cbRole.Enable(False)
        else:
            self.txtTag.Enable(True)
            self.viName.Enable(True)
            self.viDesc.Enable(True)
            self.vitrArea.Enable(True)
            self.viLimit.Enable(True)
            self.cbRole.Enable(True)
            #self.viMng.Enable(True)
    def __setResAssignData__(self,oResAssign,node,*args,**kwargs):
        docHum=self.doc.GetNetDoc('vHum')
        nodeUsr=docHum.getNodeByIdNum(kwargs['idHum'])
        self.dt.Now()
        sDt=self.dt.GetDateSmallStr()
        sDtTm=self.dt.GetDateTimeStr(' ')
        oResAssign=self.doc.GetRegisteredNode('vacation')
        oHum=docHum.GetRegisteredNode('user')
        sTag=oHum.GetName(nodeUsr)
        oResAssign.SetTag(node,sTag+' '+sDt)
        oResAssign.SetName(node,sTag+' '+sDtTm)
        oResAssign.SetOwner(node,kwargs['idHum'])
    def OnCbReqResButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'you are not supposed to be here',self)
                return
            idHum=self.doc.GetLoginUsrId()
            oResAssign=self.doc.GetRegisteredNode('ressourceAssign')
            oResAssign.DoAdd(self,self.node,True,self.__setResAssignData__,idHum=idHum)
        except:
            self.__logTB__()
    def OnCbRoleButton(self, event):
        try:
            if self.bRoleShowOnce==False:
                self.dlgRole.Centre()
            self.dlgRole.SetRolePossible(self.objRegNode.GetConfigurableRoleTranslationLst())
            self.dlgRole.SetRoles(self.dRoles)
            iRet=self.dlgRole.ShowModal()
            if iRet>0:
                self.dRoles=self.dlgRole.GetRoles()
                self.SetModified(True,self.cbRole)
        except:
            self.__logTB__()
        event.Skip()
