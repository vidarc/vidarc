#Boa:Dialog:vXmlNodeRessourceHumanAddDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceHumanAddDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vXmlNodeRessourceHumanAddDialogOld.py,v 1.2 2006/10/09 11:11:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vXmlNodeRessourceHumanPanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vXmlNodeRessourceHumanAddDialog(parent)

[wxID_VXMLNODERESSOURCEHUMANADDDIALOG, 
 wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBAPPLY, 
 wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeRessourceHumanAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODERESSOURCEHUMANADDDIALOG,
              name=u'vXmlNodeRessourceHumanAddDialog', parent=prnt,
              pos=wx.Point(348, 174), size=wx.Size(508, 347),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNode Ressource Human Add Dialog')
        self.SetClientSize(wx.Size(500, 320))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(128, 288),
              size=wx.Size(96, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(256, 288),
              size=wx.Size(96, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODERESSOURCEHUMANADDDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnLoc=vXmlNodeRessourceHumanPanel(self,id=wx.NewId(),pos=(0,0),size=(500,280),
                        style=0,name=u'pnHuman')
    def SetRegNode(self,obj):
        self.pnLoc.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnLoc.SetDoc(doc,bNet)
    def SetNode(self,nodeObj,nodePar,nodeDft=None):
        self.nodeObj=nodeObj
        self.nodePar=nodePar
        #self.pnLoc.Clear()
        self.pnLoc.SetNode(nodeDft)
    def GetNode(self,node):
        self.pnLoc.GetNode(node)
    def OnCbApplyButtonOld(self, event):
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButtonOld(self, event):
        self.EndModal(0)
        event.Skip()
    def OnCbApplyButton(self, event):
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(1)
            else:
                node=self.objReg.Create(self.nodePar)
                self.pn.GetNode(node)
                self.pn.Clear()
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnCbCancelButton(self, event):
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

