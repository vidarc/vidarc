#Boa:FramePanel:vXmlNodeRessourceResultPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceResultPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080413
# CVS-ID:       $Id: vXmlNodeRessourceResultPanel.py,v 1.2 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.grid
import vidarc.tool.misc.vtmTreeListCtrl
import vidarc.tool.misc.vtmListCtrl
import wx.lib.buttons

import sys
import calendar

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.time.vtTime as vtTime

[wxID_VXMLNODERESSOURCERESULTPANEL, wxID_VXMLNODERESSOURCERESULTPANELCBDEC, 
 wxID_VXMLNODERESSOURCERESULTPANELCBINC, 
 wxID_VXMLNODERESSOURCERESULTPANELCHCMONTH, 
 wxID_VXMLNODERESSOURCERESULTPANELGRDALLOC, 
 wxID_VXMLNODERESSOURCERESULTPANELSPMONCOUNT, 
 wxID_VXMLNODERESSOURCERESULTPANELSPYEAR, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeRessourceResultPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsDate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spYear, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbDec, 0, border=0, flag=0)
        parent.AddWindow(self.chcMonth, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbInc, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.spMonCount, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDate, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.grdAlloc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsDate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsDate_Items(self.bxsDate)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODERESSOURCERESULTPANEL,
              name=u'vXmlNodeRessourceResultPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.spYear = wx.SpinCtrl(id=wxID_VXMLNODERESSOURCERESULTPANELSPYEAR,
              initial=2000, max=2100, min=0, name=u'spYear', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(79, 30), style=wx.SP_ARROW_KEYS)
        self.spYear.SetMinSize((-1,-1))
        self.spYear.Bind(wx.EVT_SPINCTRL, self.OnUpdateDate,
              id=wxID_VXMLNODERESSOURCERESULTPANELSPYEAR)
        self.spYear.Bind(wx.EVT_TEXT_ENTER, self.OnUpdateDate,
              id=wxID_VXMLNODERESSOURCERESULTPANELSPYEAR)

        self.cbDec = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Left),
              id=wxID_VXMLNODERESSOURCERESULTPANELCBDEC, name=u'cbDec',
              parent=self, pos=wx.Point(79, 0), size=wx.Size(31, 30), style=0)
        self.cbDec.Bind(wx.EVT_BUTTON, self.OnCbDecButton,
              id=wxID_VXMLNODERESSOURCERESULTPANELCBDEC)

        self.chcMonth = wx.Choice(choices=[],
              id=wxID_VXMLNODERESSOURCERESULTPANELCHCMONTH, name=u'chcMonth',
              parent=self, pos=wx.Point(110, 0), size=wx.Size(105, 21),
              style=0)
        self.chcMonth.SetMinSize(wx.Size(-1, -1))
        self.chcMonth.Bind(wx.EVT_CHOICE, self.OnUpdateDateChc,
              id=wxID_VXMLNODERESSOURCERESULTPANELCHCMONTH)

        self.cbInc = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Right),
              id=wxID_VXMLNODERESSOURCERESULTPANELCBINC, name=u'cbInc',
              parent=self, pos=wx.Point(215, 0), size=wx.Size(31, 30), style=0)
        self.cbInc.Bind(wx.EVT_BUTTON, self.OnCbIncButton,
              id=wxID_VXMLNODERESSOURCERESULTPANELCBINC)

        self.spMonCount = wx.SpinCtrl(id=wxID_VXMLNODERESSOURCERESULTPANELSPMONCOUNT,
              initial=1, max=48, min=1, name=u'spMonCount', parent=self,
              pos=wx.Point(254, 0), size=wx.Size(48, 30),
              style=wx.SP_ARROW_KEYS)
        self.spMonCount.SetMinSize((-1,-1))
        self.spMonCount.SetValue(1)
        self.spMonCount.Bind(wx.EVT_TEXT_ENTER, self.OnUpdateDate,
              id=wxID_VXMLNODERESSOURCERESULTPANELSPMONCOUNT)

        self.grdAlloc = wx.grid.Grid(id=wxID_VXMLNODERESSOURCERESULTPANELGRDALLOC,
              name=u'grdAlloc', parent=self, pos=wx.Point(0, 30),
              size=wx.Size(304, 150), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.grdAlloc.CreateGrid(0,0)
        mdc=wx.MemoryDC()
        fnt=self.grdAlloc.GetLabelFont()
        fnt.SetWeight(wx.FONTWEIGHT_NORMAL)
        mdc.SetFont(fnt)
        self.grdAlloc.SetLabelFont(fnt)
        te=mdc.GetTextExtent('Xy')
        self.grdAlloc.SetColLabelSize(te[1]*5)
        self.grdAlloc.SetDefaultCellAlignment(wx.ALIGN_RIGHT,wx.ALIGN_BOTTOM)
        self.iFntHeight=te[1]
        te=mdc.GetTextExtent('X')
        self.iFntWidth=te[0]
        
        self.dRes={}
        self.dDate={}
        self.dt=vtTime.vtDateTime(True)
        self.dt.SetDay(1)
        self.__updateLang__()
        self.__showDate__()
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __updateLang__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.chcMonth.Clear()
        for i in range(1,13):
            self.chcMonth.Append(calendar.month_name[i])
    def __showDate__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.spYear.SetValue(self.dt.GetYear())
        self.chcMonth.SetSelection(self.dt.GetMonth()-1)
    def __getDateCount__(self,iYear,iMon,iMonCount):
        iCount=0
        for iMonRep in xrange(iMonCount):
            mrng=calendar.monthrange(iYear,iMon)
            iDay,iDays=mrng
            iCount+=iDays
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
        return iCount
    def __showDateValues__(self,iYear,iMon,iMonCount):
        self.grdAlloc.SetColLabelAlignment(wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)
        iCol=0
        for iMonRep in xrange(iMonCount):
            mrng=calendar.monthrange(iYear,iMon)
            iDay,iDays=mrng
            iDays+=1
            for i in range(1,iDays):
                sTag=calendar.day_abbr[iDay]
                iDay+=1
                iDay%=7
                sDate='%04d\n%02d\n%02d\n%s'%(iYear,iMon,i,sTag)
                self.grdAlloc.SetColLabelValue(iCol,sDate)
                iCol+=1
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
    def ClearWid(self):
        vtXmlNodePanel.ClearWid(self)
        # add code here
        self.dRes={}
        self.dDate={}
        try:
            self.grdAlloc.BeginBatch()
            
            iCols=self.grdAlloc.GetNumberCols()
            if iCols>0:
                self.grdAlloc.DeleteCols(0,iCols)
            iRows=self.grdAlloc.GetNumberRows()
            if iRows>0:
                self.grdAlloc.DeleteRows(0,iRows)
        except:
            self.__logTB__()
        self.grdAlloc.EndBatch()
        self.grdAlloc.ClearGrid()
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.dRes={}
            if self.node is not None:
                self.dRes=self.doc.BuildResDict(self.node,bStore=False)
                #    par=self.doc.getParent(par)
                #    if par is not None:
                #        self.dRes=self.doc.BuildResDict(par,bStore=False)
            self.__showAll__()
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        # add code here
        
        vtXmlNodePanel.Close(self)
    def Cancel(self):
        # add code here
        
        vtXmlNodePanel.Cancel(self)
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def OnCbDecButton(self, event):
        try:
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon-=1
            if iMon<1:
                iYear-=1
                iMon=12
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnUpdateDate(self,evt):
        evt.Skip()
        try:
            iYear=int(self.spYear.GetValue())
            self.dt.SetYear(iYear)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnUpdateDateChc(self, event):
        event.Skip()
        try:
            self.dt.SetMonth(self.chcMonth.GetSelection()+1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnCbIncButton(self, event):
        try:
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def __showAll__(self):
        try:
            self.doc.CallBack(self.__startAllDly__)
        except:
            self.__logTB__()
    def __startAllDly__(self):
        try:
            iCols=self.grdAlloc.GetNumberCols()
            if iCols>0:
                self.grdAlloc.DeleteCols(0,iCols)
            iRows=self.grdAlloc.GetNumberRows()
            if iRows>0:
                self.grdAlloc.DeleteRows(0,iRows)
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            iMonCount=self.spMonCount.GetValue()
            self.doc.DoSafe(self.doc.GetResourceData,iYear,iMonth,iMonCount,dRes=self.dRes)
            self.doc.DoCallBack(self.__showAllDly__,iYear,iMonth,iMonCount)
        except:
            self.__logTB__()
    def __showAllDly__(self,iYear,iMonth,iMonCount):
        lKeys=[]
        iRowCount=0
        try:
            #iYear=self.dt.GetYear()
            #iMonth=self.dt.GetMonth()
            #iMonCount=self.spMonCount.GetValue()
            dVal=self.doc.GetResourceData(iYear,iMonth,iMonCount,dRes=self.dRes)
            if self.VERBOSE:
                vtLog.CallStack('')
                print 'year',iYear,'month',iMonth,'mon count',iMonCount
                print vtLog.pformat(dVal)
            keys=[k for k in dVal.keys() if k.startswith('__')==False]
            for k in keys:
                ddVal=dVal[k]
                kkeys=[kk for kk in ddVal.keys() if kk.startswith('__')==False]
                lKeys.append((k,kkeys))
                #kkeys.sort()
                iRowCount+=len(kkeys)+1
            iColCount=self.__getDateCount__(iYear,iMonth,iMonCount)
        except:
            self.__logTB__()
        iLblLen=0
        fLimOvLd=None
        try:
            nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),['cfg','ResGenCfg'])
            if nodeCfg is not None:
                oReg=self.doc.GetRegByNode(nodeCfg)
                fLimOvLd=oReg.GetLimitOverLoadFloat(nodeCfg)
        except:
            self.__logTB__()
        if fLimOvLd is None:
            fLimOvLd=100.0
        self.grdAlloc.BeginBatch()
        try:
            iCols=self.grdAlloc.GetNumberCols()
            if iCols>0:
                self.grdAlloc.DeleteCols(0,iCols)
            iRows=self.grdAlloc.GetNumberRows()
            if iRows>0:
                self.grdAlloc.DeleteRows(0,iRows)
            
            self.grdAlloc.AppendCols(iColCount)
            self.grdAlloc.AppendRows(iRowCount)
            self.__showDateValues__(iYear,iMonth,iMonCount)
            iCol=0
            iFmt=wx.LIST_FORMAT_RIGHT
            
            idx=0
            for k,kkeys in lKeys:
                lSum=[0.0]*(iColCount)
                iCol=0
                
                ddVal=dVal[k]
                for kk in kkeys:
                    sLbl=u' : '.join([k,kk])
                    iLblLen=max(iLblLen,len(sLbl))
                    self.grdAlloc.SetRowLabelValue(idx,sLbl)
                    fLimit=ddVal[kk][0]
                    dYear=ddVal[kk][1]
                    self.__showDict__(iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear,lSum)
                    #iCol+=1
                    idx+=1
                if '__sum_' in ddVal:
                    fLimit=dVal['__sum'][0]
                    dYear=dVal['__sum'][1]
                    if self.bTranspose:
                        idx=0
                        iCol+=1
                    self.__showDict__(iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear)
                sLbl=u' : '.join([k,_('sum')])
                iLblLen=max(iLblLen,len(sLbl))
                self.grdAlloc.SetRowLabelValue(idx,sLbl)
                for fVal in lSum:
                    fnt=self.grdAlloc.GetLabelFont()
                    fnt.SetWeight(wx.FONTWEIGHT_BOLD)
                    if fVal>0.0:
                        self.grdAlloc.SetCellValue(idx,iCol,
                                            vtLgBase.format('%4.1f',fVal))
                        if fVal>=fLimOvLd:
                            self.grdAlloc.SetCellTextColour(idx,iCol,wx.RED)
                        self.grdAlloc.SetCellFont(idx,iCol,fnt)
                    iCol+=1
                idx+=1
                
        except:
            self.__logTB__()
        for iCol in xrange(iColCount):
            self.grdAlloc.SetColSize(iCol,self.iFntWidth*6)
            for iRow in xrange(iRowCount):
                #grdAttr=wx.grid.GridCellAttr()
                #grdAttr.SetReadOnly()
                #self.grdAlloc.SetRowAttr(i,grdAttr)
                self.grdAlloc.SetReadOnly(iRow,iCol)
        #self.grdAlloc.SetDefaultEditor(None)
        self.grdAlloc.SetRowLabelSize(iLblLen*self.iFntWidth)
        self.grdAlloc.SetRowLabelAlignment(wx.ALIGN_LEFT,wx.ALIGN_BOTTOM)
        self.grdAlloc.EndBatch()
    def __showDict__(self,iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear,lSum):
        try:
            #if self.bTranspose:
            #    idx=0
                #iCol+=1
            for iMonRep in xrange(iMonCount):
                mrng=calendar.monthrange(iYear,iMonth)
                iDay,iDays=mrng
                iDays+=1
                for i in range(1,iDays):
                    sTag=calendar.day_abbr[iDay]
                    iDay+=1
                    iDay%=7
                    #sDate='%04d-%02d-%02d %s'%(iYear,iMon,i,sTag)
                    #print self.bTranspose,sDate
                    if iYear in dYear:
                        dMon=dYear[iYear]
                        if iMonth in dMon:
                            dDay=dMon[iMonth]
                            if i in dDay:
                                fVal=dDay[i]/fLimit*100.0
                                lSum[iCol]=lSum[iCol]+fVal
                                self.grdAlloc.SetCellValue(idx,iCol,
                                                vtLgBase.format('%4.1f',fVal))
                                        
                    iCol+=1
                iMonth+=1
                if iMonth>12:
                    iYear+=1
                    iMonth=1
        except:
            self.__logTB__()
