#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060912
# CVS-ID:       $Id: __register__.py,v 1.6 2008/04/15 23:54:54 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vRessource.vXmlNodeRessource import vXmlNodeRessource
from vidarc.vApps.vRessource.vXmlNodeRessourceAttrML import vXmlNodeRessourceAttrML
from vidarc.vApps.vRessource.vXmlNodeRessourceHuman import vXmlNodeRessourceHuman
from vidarc.vApps.vRessource.vXmlNodeRessourceHumanMember import vXmlNodeRessourceHumanMember
from vidarc.vApps.vRessource.vXmlNodeVacation import vXmlNodeVacation
from vidarc.vApps.vRessource.vXmlNodeProjectAssign import vXmlNodeProjectAssign
from vidarc.vApps.vRessource.vXmlNodeRessourceAssign import vXmlNodeRessourceAssign
from vidarc.vApps.vRessource.vXmlNodeRessourceCfg import vXmlNodeRessourceCfg
from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.vApps.vHum.vUserNode import vUserNode
#from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog

#from vidarc.tool.xml.vtXmlNodeTmpls import vtXmlNodeTmpls
#from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag

#from vidarc.vApps.vHum.vUserNodeReferenced import vUserNodeReferenced
from vidarc.vApps.vRessource.vXmlNodeUserReferenced import vXmlNodeUserReferenced

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition
from vidarc.tool.xml.vtXmlNodeFilter import vtXmlNodeFilter
#from vidarc.tool.xml.vtXmlNodeMessages import vtXmlNodeMessages
#from vidarc.tool.xml.vtXmlNodeMsg import vtXmlNodeMsg
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

import vidarc.ext.report.__register__
import vidarc.ext.data.__register__

import vidarc.config.vcCust as vcCust
if vcCust.is2Import(__name__):
    vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    import wx
    from vidarc.vApps.vPrj.vXmlPrjInputTree import vXmlPrjInputTree
    from vidarc.vApps.vHum.vXmlHumInputTree import vXmlHumInputTree
    from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
    from vidarc.tool.time.vtTimeDateGM import vtTimeDateGM
    from vidarc.tool.time.vtTime import vtDateTime
    from vidarc.tool.time.vtTime import vtTime
    def funcCmpId(doc,node,val,lMatch):
        if lMatch is not None:
            if val in lMatch:
                return True
            else:
                return False
        return True
    def funcCmpPrjId(node,doc,lMatch,lFound):
        fid,sAppl=doc.getForeignKeyNumAppl(node)
        if sAppl=='vPrj':
            if funcCmpId(doc,node,fid,lMatch):
                lFound[0]=True
                return -1
        return 0
    def funcCmpPrjsId(doc,node,val,lMatch):
        lFound=[False]
        doc.procChildsExt(node,funcCmpPrjId,doc,lMatch,lFound)
        return lFound[0]
    def funcCreateHum(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='human', name='lblHum', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlHumInputTree(id=-1,name=u'%s_vitrHum'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vHum')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
    def funcSetDocHum(obj,viTr,doc,dNetDocs):
        viTr.SetDoc(doc)
        viTr.SetDocTree(dNetDocs['vHum']['doc'],dNetDocs['vHum']['bNet'])
        viTr.SetNodeTree(None)
    def funcGetSelHum(obj,viTr,doc):
        sVal=viTr.GetValue()
        sID=viTr.GetForeignID()
        #sID='@'.join([sID,'vPrj'])
        return sVal,sID
    def funcCreatePrj(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='project', name='lblPrj', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlPrjInputTree(id=-1,name=u'%s_vitrPrj'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vPrj')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
    def funcSetDocPrj(obj,viTr,doc,dNetDocs):
        viTr.SetDoc(doc)
        viTr.SetDocTree(dNetDocs['vPrj']['doc'],dNetDocs['vPrj']['bNet'])
        viTr.SetNodeTree(None)
    def funcGetSelPrj(obj,viTr,doc):
        sVal=viTr.GetValue()
        sID=viTr.GetForeignID()
        #sID='@'.join([sID,'vPrj'])
        return sVal,sID
    def funcCreateDate(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='project', name='lblPrj', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viDtStart=vtTimeDateGM(id=-1,name=u'%s_vitrDtStart'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viDtStart.SetMinSize((-1,-1))
        bxs.AddWindow(viDtStart, 2, border=4, flag=wx.EXPAND)
        viDtEnd=vtTimeDateGM(id=-1,name=u'%s_vitrDtStart'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viDtEnd.SetMinSize((-1,-1))
        bxs.AddWindow(viDtEnd, 2, border=4, flag=wx.EXPAND)
        return bxs,(viDtStart,viDtEnd)
    def funcSetDocDate(obj,tupDt,doc,dNetDocs):
        viDtStart,viDtEnd=tupDt
    def funcGetSelDate(obj,tupDt,doc):
        viDtStart,viDtEnd=tupDt
        sVal=' '.join([viDtStart.GetValueStr(),viDtEnd.GetValueStr()])
        return sVal,''
    def funcCmpDate(doc,node,sVal,lMatch):
        for sMatch in lMatch:
            strs=sMatch.split(' ')
            z=vtDateTime()
            z.SetDateSmallStr(sVal)
            sDT=z.GetDateStr()
            if sDT>=strs[0]:
                if len(strs[1])>0:
                    if sDT<=strs[1]:
                        return True
                else:
                    return True
        return False
    def convDate(doc,node,sVal,dt):
        try:
            dt.SetDateSmallStr(sVal)
        except:
            pass
        return dt.GetYear(),dt.GetMonth(),dt.GetDay(),dt.GetWeek(),dt.GetDayName()

def convVacAlloc(doc,node,sVal,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,'kwargs:%s'%
                (vtLog.pformat(kwargs)),doc.GetOrigin())
        oReg=doc.GetRegByNode(node)
        l=oReg.GetAllocation(node,**kwargs)
        return l,

def convPrjAssignAlloc(doc,node,sVal,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,'kwargs:%s'%
                (vtLog.pformat(kwargs)),doc.GetOrigin())
        oReg=doc.GetRegByNode(node)
        l=oReg.GetAllocation(node,**kwargs)
        return l,

def convPrjAssignAllocDate(doc,node,sVal,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,'kwargs:%s'%
                (vtLog.pformat(kwargs)),doc.GetOrigin())
        oReg=doc.GetRegByNode(node)
        l=oReg.GetAllocationDate(node)
        return l,

def convPrjAssignAllocVal(doc,node,sVal,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,'kwargs:%s'%
                (vtLog.pformat(kwargs)),doc.GetOrigin())
        oReg=doc.GetRegByNode(node)
        l=oReg.GetAllocationVal(node)
        return l,

def RegisterNodes(self,bRegAsRoot=True):
    try:
        
        oLog=self.GetReg('log',bLogFlt=False)
        if oLog is None:
            oLog=vtXmlNodeLog()
            self.RegisterNode(oLog,False)
        oAttrML=self.GetReg('attrML',bLogFlt=False)
        if oAttrML is None:
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
        vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
        vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterNodes(self,'vRessource',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterDateTime(self,'vacation','startDt',_('start date time'))
        vidarc.ext.data.__register__.RegisterDateTime(self,'vacation','endDt',_('end date time'))
        vidarc.ext.data.__register__.RegisterDateTime(self,'projectAssign','startDt',_('start date time'))
        vidarc.ext.data.__register__.RegisterDateTime(self,'projectAssign','endDt',_('end date time'))
        vidarc.ext.data.__register__.RegisterDateTime(self,'ressourceAssign','startDt',_('start date time'))
        vidarc.ext.data.__register__.RegisterDateTime(self,'ressourceAssign','endDt',_('end date time'))
        
        oDataColl=self.GetReg('dataCollector')
        
        if vcCust.is2Import(__name__):
            d=oDataColl.GetSrcWidDict()
            #vtLog.CallStack('')
            #vtLog.pprint(d)
            dSrcWid={
                    'vPrj':{'create':funcCreatePrj,
                            'setDoc':funcSetDocPrj,
                            'getSel':funcGetSelPrj,
                            'translation':_('project module'),
                            'match':funcCmpId},
                    'vHum':{'create':funcCreateHum,
                            'setDoc':funcSetDocHum,
                            'getSel':funcGetSelHum,
                            'translation':_('human module'),
                            'match':funcCmpId},
                    }
            oDataColl.UpdateSrcWidDict(dSrcWid)
            d=oDataColl.GetSrcWidDict()
            #vtLog.CallStack('')
            #vtLog.pprint(d)
            d=oDataColl.GetDataProcDict()
            #vtLog.pprint(d)
        else:
            dSrcWid={}
        dataProc={'':[
                    {'conv':convPrjAssignAlloc,
                        'args':(),
                        'kwargs':{},
                        'values':['alloc'],
                    },
                    {'conv':convPrjAssignAllocVal,
                        'args':(),
                        'kwargs':{},
                        'values':['allocVal'],
                    },
                    {'conv':convPrjAssignAllocDate,
                        'args':(),
                        'kwargs':{},
                        'values':['allocDate'],
                    },]
                    }
        oDataColl.UpdateDataProcDict('projectAssign',dataProc)
        dataProc={'':[
                    {'conv':convPrjAssignAlloc,
                        'args':(),
                        'kwargs':{},
                        'values':['alloc'],
                    },]
                    }
        oDataColl.UpdateDataProcDict('vacation',dataProc)
        
        oRessource=vXmlNodeRessource()
        oRessourceAttr=vXmlNodeRessourceAttrML()
        
        oRessourceHuman=vXmlNodeRessourceHuman()
        #oUsrRef=vUserNodeReferenced()
        oUsrRef=vXmlNodeUserReferenced()
        oResHumMem=vXmlNodeRessourceHumanMember(tagNameUsr=oUsrRef.GetTagName())
        oResHumVac=vXmlNodeVacation()
        oResHumPrj=vXmlNodeProjectAssign()
        oResAssign=vXmlNodeRessourceAssign()
        
        self.RegisterNode(oRessourceHuman,True)
        self.RegisterNode(oResHumMem,False)
        self.RegisterNode(oResHumVac,False)
        self.RegisterNode(oResHumPrj,False)
        self.RegisterNode(oResAssign,False)
        self.RegisterNode(oUsrRef,False)
        self.RegisterNode(oRessource,True)
        self.RegisterNode(oRessourceAttr,True)
        self.LinkRegisteredNode(oRessource,oRessourceAttr)
        self.LinkRegisteredNode(oRessource,oLog)
        self.LinkRegisteredNode(oRessource,oResAssign)
        self.LinkRegisteredNode(oRessourceHuman,oResHumMem)
        self.LinkRegisteredNode(oRessourceHuman,oUsrRef)
        self.LinkRegisteredNode(oUsrRef,oResHumVac)
        self.LinkRegisteredNode(oUsrRef,oResHumPrj)
        self.LinkRegisteredNode(oRessourceHuman,oLog)
        
        flt={None:[('duration',vtXmlFilterType.FILTER_TYPE_FLOAT),
                    ('int',vtXmlFilterType.FILTER_TYPE_INT),
                    ('long',vtXmlFilterType.FILTER_TYPE_LONG),
                    ('date',vtXmlFilterType.FILTER_TYPE_DATE),
                    ('time',vtXmlFilterType.FILTER_TYPE_TIME),
                    ('date time',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ]}
        #oStateMachine=veStateStateMachine()
        oTransVac=self.GetReg('transVacation',bLogFlt=False)
        if oTransVac is None:
            oTransVac=veStateTransition(tagName='transVacation',filter=flt)
            self.RegisterNode(oTransVac,False)
        oSMvacation=self.GetReg('smVacation',bLogFlt=False)
        if oSMvacation is None:
            oSMvacation=veStateStateMachine(tagName='smVacation',
                    tagNameState='state',tagNameTrans='transVacation',
                    filter=flt,
                    trans={None:{None:_(u'vacation'),'duration':_(u'duraction')} },
                    cmd=oResHumVac.GetActionCmdDict())
            self.RegisterNode(oSMvacation,False)
        
        oTransPrj=self.GetReg('transPrjAsgn',bLogFlt=False)
        if oTransPrj is None:
            oTransPrj=veStateTransition(tagName='transPrjAsgn',filter=flt)
            self.RegisterNode(oTransPrj,False)
        oSMprjAsgn=self.GetReg('smPrjAsgn',bLogFlt=False)
        if oSMprjAsgn is None:
            oSMprjAsgn=veStateStateMachine(tagName='smPrjAsgn',
                    tagNameState='state',tagNameTrans='transPrjAsgn',
                    filter=flt,
                    trans={None:{None:_(u'project assign'),'duration':_(u'duration')} },
                    cmd=oResHumVac.GetActionCmdDict())
            self.RegisterNode(oSMprjAsgn,False)
        
        oTransRes=self.GetReg('transResAsgn',bLogFlt=False)
        if oTransRes is None:
            oTransRes=veStateTransition(tagName='transResAsgn',filter=flt)
            self.RegisterNode(oTransRes,False)
        oSMresAsgn=self.GetReg('smResAsgn',bLogFlt=False)
        if oSMresAsgn is None:
            oSMresAsgn=veStateStateMachine(tagName='smResAsgn',
                    tagNameState='state',tagNameTrans='transResAsgn',
                    filter=flt,
                    trans={None:{None:_(u'ressource assign'),'duration':_(u'duration')} },
                    cmd=oResAssign.GetActionCmdDict())
            self.RegisterNode(oSMresAsgn,False)
        #oState=veStateState()
        #oTrans=veStateTransition()
        #oFilter=vtXmlNodeFilter()
            
        #self.RegisterNode(oMsgs,True)
        #self.RegisterNode(oMsg,True)
        #self.RegisterNode(oStateMachineLinker,False)
        #self.RegisterNode(oStateMachine,False)
        
        #self.RegisterNode(oState,False)
        
        #self.RegisterNode(oTrans,False)
        #self.RegisterNode(oFilter,False)
                
        #self.LinkRegisteredNode(oCfg,oStateMachineLinker)
        #self.LinkRegisteredNode(oStateMachineLinker,oStateMachine)
        #self.LinkRegisteredNode(oStateMachineLinker,oSMvacation)
        #self.LinkRegisteredNode(oStateMachine,oState)
        #self.LinkRegisteredNode(oSMvacation,oState)
        
        if 1:
            pass
            #vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=False)
        else:
                oMsgs=vtXmlNodeMessages()
                oMsg=vtXmlNodeMsg()
                
                oTag=vtXmlNodeTag()
                oDoc=veRepDoc()
                oTmpls=vtXmlNodeTmpls()
                oRepTmpl=veRepDocTmpl()
                oRepTmplLaTex=veRepDocTmplLaTex()
                
                self.RegisterNode(oTag,True)
                self.RegisterNode(oDoc,True)
                self.RegisterNode(oTmpls,False)
                self.RegisterNode(oRepTmpl,False)
                self.RegisterNode(oRepTmplLaTex,False)
            
                self.LinkRegisteredNode(oMsgs,oMsg)
                self.LinkRegisteredNode(oCfg,oTmpls)
                self.LinkRegisteredNode(oTmpls,oRepTmpl)
                self.LinkRegisteredNode(oTmpls,oRepTmplLaTex)
                self.LinkRegisteredNode(oDoc,oLog)
        oStateMachineLinker=self.GetReg('stateMachineLinker',bLogFlt=False)
        if oStateMachineLinker is not None:
            oStateMachineLinker.AddPossible({
                    'vacation':[('vacState','smVacation')],
                    'projectAssign':[('prjState','smPrjAsgn')],
                    'ressourceAssign':[('resState','smResAsgn')],
                    #'offer':[('offState','smOffer')],
                    })
            self.RegisterNode(oSMvacation,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMvacation)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMvacation,oState)
            
            self.RegisterNode(oSMprjAsgn,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMprjAsgn)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMprjAsgn,oState)
            
            self.RegisterNode(oSMresAsgn,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMresAsgn)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMresAsgn,oState)
        else:
            vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)
        #oStateMachineLinker=veStateStateMachineLinker({
        #        'vacation':[('vacState','smVacation')],
        #        'input02':[('attribute01','stateMachine'),
        #                    ('attribute02','stateMachine')],
        #        })
        
        oResCfg=vXmlNodeRessourceCfg()
        self.RegisterNode(oResCfg,False)
        oCfg=self.GetRegisteredNode('cfg')
        self.LinkRegisteredNode(oCfg,oResCfg)
    except:
        vtLog.vtLngTB(__name__)
