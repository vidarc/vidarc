#Boa:Dialog:vRessourceDiagramDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070207
# CVS-ID:       $Id: vRessourceDiagramDialog.py,v 1.7 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.draw.vtDrawCanvasPanel
import vidarc.tool.draw.vtDrawCalendarPanel
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodePanel import *

def create(parent):
    return vRessourceDiagramDialog(parent)

[wxID_VRESSOURCEDIAGRAMDIALOG, wxID_VRESSOURCEDIAGRAMDIALOGCBAPPLY, 
 wxID_VRESSOURCEDIAGRAMDIALOGCBCANCEL, wxID_VRESSOURCEDIAGRAMDIALOGVTDRES, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vRessourceDiagramDialog(wx.Dialog,vtXmlNodePanel):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.vtdRes, (0, 0), border=4,
              flag=wx.TOP | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, span=(1,
              1))
        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VRESSOURCEDIAGRAMDIALOG,
              name=u'vRessourceDiagramDialog', parent=prnt, pos=wx.Point(16,
              231), size=wx.Size(882, 633),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vRessource Diagram Dialog')
        self.SetClientSize(wx.Size(874, 606))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VRESSOURCEDIAGRAMDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(342, 568),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize((-1,-1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VRESSOURCEDIAGRAMDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VRESSOURCEDIAGRAMDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(450, 568),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize((-1,-1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VRESSOURCEDIAGRAMDIALOGCBCANCEL)

        self.vtdRes = vidarc.tool.draw.vtDrawCalendarPanel.vtDrawCalendarPanel(draw_grid=False,
              flip=False, grid_x=20, grid_y=20,
              id=wxID_VRESSOURCEDIAGRAMDIALOGVTDRES, is_gui_wx=True,
              label_len=8, name='vtdRes', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(860, 556), style=0)
        self.vtdRes.SetMinSize((-1,-1))

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            #self.gbsData.Layout()
            self.gbsData.Fit(self)
            #self.gbsData.FitInside(self)
            
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            #self.pn.Close()
            #self.pn.GetNode(None)
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            #self.pn.Close()
            #self.pn.Cancel()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDoc(self,doc,bNet=True):
        vtXmlNodePanel.SetDoc(self,doc,bNet=bNet)
        self.vtdRes.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        dRes=self.doc.BuildResDict(node,bStore=False)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.INFO,'dRes:%s'%(vtLog.pformat(dRes)),self)
        self.doc.CallBack(self.vtdRes.SetValue,self.doc.GetResourceData,dRes=dRes)
    def SetNode(self,node):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'id:%d'%(self.doc.getKeyNum(node)),self)
            self.doc.DoSafe(self.__SetNode__,node)
        except:
            vtLog.vtLngTB(self.GetName())
