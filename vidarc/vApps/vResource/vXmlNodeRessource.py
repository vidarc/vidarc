#----------------------------------------------------------------------------
# Name:         vXmlNodeRessource.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlNodeRessource.py,v 1.15 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.time.vtTimeLimit as vtTimeLimit
#from vidarc.tool.xml.vtXmlNodeBase import *
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeRessourcePanel import vXmlNodeRessourcePanel
        #from vidarc.vApps.vRessource.vXmlNodeRessourceEditDialog import *
        #from vidarc.vApps.vRessource.vXmlNodeRessourceAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeRessource(vtXmlNodeTag):
    VERBOSE=0
    #NODE_ATTRS=[
    #        ('tag',None,'tag',None),
    #        ('name',None,'name',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='ressource'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeTag.__init__(self,tagName)
        self.dt=vtDateTime(True)
    def GetDescription(self):
        return _(u'ressource')
    # ---------------------------------------------------------
    # specific
    def IsResource2Draw(self):
        return True
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smResAsgn')
        return oSM.SetRolesDict(node,d)
    
    def GetLabel2Draw(self,node):
        l=[]
        n=node
        while n is not None:
            o=self.doc.GetRegByNode(n)
            if o is not None:
                if hasattr(o,'IsResource2Draw'):
                    if o.IsResource2Draw():
                        l.append(self.GetTag(n))
            n=self.doc.getParent(n)
            if self.doc.IsNodeKeyValid(n)==False:
                break
        l.reverse()
        return u'.'.join(l)
    def GetLimitFloat(self,node):
        try:
            return float(self.Get(node,'limit'))
        except:
            return 100.0
    def Add2ResourceDict(self,node,dResource):
        sLbl=self.GetLabel2Draw(node)
        if dResource.has_key(sLbl)==False:
            #dResource[sLbl]=[self.GetLimitFloat(node),{},long(self.doc.getKey(node))]
            id=self.doc.getKeyNum(node)
            fLimit=self.GetLimitFloat(node)
            dResource[id]={
                        '__label':  sLbl,
                        '__sum':    [24.0,{},fLimit],
                        -2:         [24.0,{},id,id,fLimit]
                        }
            return dResource[id]
    def __addResAssign__(self,node,iYear,iMon,iMonCount,dYear,llRes):
        try:
            sTag=self.doc.getTagName(node)
            if sTag=='ressourceAssign':
                id=self.doc.getKeyNum(node)
                if self.VERBOSE:
                    print llRes,id
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'llRes:%s;id:%08d'%
                                    (vtLog.pformat(llRes),id),self)
                oAsgn=self.doc.GetRegisteredNode(sTag)
                iResult=oAsgn.GetResultInt(node)
                if iResult<0:
                    if self.VERBOSE:
                        print '   ','skip'
                    return 0
                #fid=oAsgn.GetPrjFIDStr(node)
                if 1:
                    fLimit=llRes[-1]
                    dRes=llRes[1]
                    if id in dRes:
                        lRes=dRes[id]
                    else:
                        idPar=-1
                        id=self.doc.getKeyNum(node)
                        l=oAsgn.GetPrjInfos(node)
                        if l is None:
                            l=[oAsgn.GetTag(node)]
                        
                        lRes=[u', '.join(l),{},{
                                    'id':id,
                                    '__res':iResult,
                                    '__lim':fLimit,
                                    '__per':oAsgn.GetPercentageFloat(node)}]
                        dRes[id]=lRes
                dYear=lRes[1]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'dYear:%s;id:%s'%
                                (vtLog.pformat(dYear),id),self)
                if self.VERBOSE:
                    print '   ',iYear,iMon,dYear,lRes
                fPer=oAsgn.GetPercentageFloat(node)/100.0
                sDt=oAsgn.GetStartDt(node)
                self.dt.SetStr(sDt)
                iYrS=self.dt.GetYear()
                iMhS=self.dt.GetMonth()
                iDyS=self.dt.GetDay()
                iHrS=self.dt.GetHour()
                iMnS=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                sDt=oAsgn.GetEndDt(node)
                self.dt.SetStr(sDt)
                iYrE=self.dt.GetYear()
                iMhE=self.dt.GetMonth()
                iDyE=self.dt.GetDay()
                iHrE=self.dt.GetHour()
                iMnE=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                    print '   ','year',iYear,iYrS,iYrE
                for iMonRepeat in xrange(iMonCount):
                    if self.VERBOSE:
                        print '   rep:',iMonRepeat,iYear,iMon
                    if iYear>=iYrS and iYear<=iYrE:
                        if iYear not in dYear:
                            dMon={}
                            dYear[iYear]=dMon
                        else:
                            dMon=dYear[iYear]
                        if iYear==iYrS:
                            iMhST=iMhS
                        else:
                            iMhST=1
                        iMhET=12
                        if iYear==iYrE:
                            iMhET=iMhE
                        #if iMon>=iMhS and iMon<=iMhE:
                        if self.VERBOSE:
                            print '   ','mon',iMhST,iMhET,iMon
                        if iMon>=iMhST and iMon<=iMhET:
                            if iMon in dMon:
                                dData=dMon[iMon]
                                bMonCalc=False
                            else:
                                dData={}
                                dMon[iMon]=dData
                                bMonCalc=True
                            if bMonCalc:
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s;id:%s'%
                                                (vtLog.pformat(dData),id),self)
                                if iYear==iYrS:
                                    if iMon>iMhS:
                                        iDyST=1
                                    else:
                                        iDyST=iDyS
                                else:
                                    iDyST=1
                                if iYear==iYrE:
                                    if iMon<iMhE:
                                        iDyET=31
                                    else:
                                        iDyET=iDyE
                                else:
                                    iDyET=31
                                #for iDy in xrange(iDyS,iDyE+1):
                                if self.VERBOSE:
                                    print '   ','min',iMhST,iMhET
                                    print '   ','day',iDyST,iDyET
                                for iDy in xrange(iDyST,iDyET+1):
                                    if iYrS==iYear and iMhS==iMon and iDyS==iDy:
                                        iHrScal=iHrS
                                        iMnScal=iMnS
                                    else:
                                        iHrScal=0
                                        iMnScal=0
                                    if iYrE==iYear and iMhE==iMon and iDyE==iDy:
                                        iHrEcal=iHrE
                                        iMnEcal=iMnE
                                    else:
                                        iHrEcal=24
                                        iMnEcal=0
                                        if iHrS>=0 and iHrE>=0:
                                            fVal=(iHrEcal-iHrScal)+(iMnEcal-iMnScal)/60.0
                                            fVal*=fPer
                                            if self.VERBOSE:
                                                print fVal
                                    if iDy in dData:
                                        l=dData[iDy]
                                    else:
                                        l=[]
                                        dData[iDy]=l
                                    if iHrS>=0 and iHrE>=0:
                                        fVal=(iHrEcal-iHrScal)+(iMnEcal-iMnScal)/60.0
                                        fVal*=fPer
                                    #    if self.VERBOSE:
                                    #        print fVal
                                    #    if iDy in dData:
                                    #        dData[iDy]=dData[iDy]+fVal
                                    #    else:
                                    #        dData[iDy]=fVal
                                    ll=[]
                                    vtTimeLimit.addRange(ll,iHrScal,iMnScal,iHrEcal,iMnEcal)
                                    for t in ll:
                                        l.append((t[0],t[1],iResult,id,fVal))
                    iMon+=1
                    if iMon>12:
                        iMon=1
                        iYear+=1
                if self.VERBOSE:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'llRes:%s;id:%s'%
                                    (vtLog.pformat(llRes),id),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def GetResourceData(self,node,iYear,iMon,iMonCount,dData,lRes):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'dData:%s;lRes:%s'%
                    (vtLog.pformat(dData),vtLog.pformat(lRes)),self)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(dData)
            vtLog.pprint(lRes)
        self.doc.procChildsKeys(node,self.__addResAssign__,iYear,iMon,iMonCount,dData,lRes)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'dData:%s'%
                    (vtLog.pformat(dData)),self)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('description',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x017IDAT8\x8d\xa5\x93\xb1q\xc30\x0cE\x1fH/\xa3\xc2\x15\'\x90\x04o\x91\
\xc2U&p\xa2Y\xd2\xb1\xd0\x16\xa1\xed\tT\xb9\xd046RH\xa4%\xc7\xae\xfc\xefp\
\xc7\x13>>\xa1\x0fP\xc4y\xde\x81\x03\xf8\xfe:\x98\xdd\xaef\xb7\xab\xb5Mm\xaf\
\xc8mS\x17\xde\xf7\xd7a\xe2\xcd\x1d\xd8\xe7\x8f\x19\x98\x01%\xc4\xf9\x92\xbf\
\x87\xcd\xdc9/\xcec\xb7\xab\x81\xa1\xba#\x84\x00@Um\xd9\xef?\x00\x88\xb1g\
\x1c/\x00\x0c\xc3@J\xbf\x80 \xce\xcb\x06@UII\x08\xa1\xa3\xaa\xb6\xa5\xe5\x18\
\xfbr^~OIPU\x8e\xa73\x1b\x80\xe3\xe9,s\x8b\x05\xe3xa\x18\x06\x00B\x08+\x81E\
\xcd$\xf0xK.\xce\xa4,^U\xdb\x7fBd\xa3\xb2Y1\xf6\xa6\xaa\xb64O\x9cGU-\xc6~e\
\xae8?\x8d\xf1\x1d\x14\x01\xbb]-\x9b\x16B\xa0mj\x13\xe7E\x9c\x97\xb6\xa9-O\'\
\xc6~\x9e\xda\x84\x95\x07\xe3xy\xfcO\xcb\x82K\x7f\x96\xd8\xc0\xb4a)\xa5U\xe2\
\xa9a\x0b\xb4Mm\xc7\xd3y\xda\x83\xa9\xd8\x18\x86\xddJ\xe0\xd5"\x81\x91\x92L\
\xc6\xbf\xbb\xca\x88\xf3t]W\x08y\x84\xcfBU\x0b\xaf\xeb\xba\xfb[x\x07\x7f\ty\
\xb8\x9c\xf3\x06\x92\xf7\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(508, 347),
                    'pnName':'pnLoc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeRessourceAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(508, 347),
                    'pnName':'pnLoc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeRessourcePanel
        else:
            return None
_(u'tag'),_(u'name'),_(u'description')
