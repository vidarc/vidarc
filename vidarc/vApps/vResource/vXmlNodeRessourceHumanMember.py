#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceHumanMember.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vXmlNodeRessourceHumanMember.py,v 1.14 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.time.vtTimeLimit as vtTimeLimit
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeRessourceHumanMemberPanel import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeRessourceHumanMember(vtXmlNodeBase):
    VERBOSE=0
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['DescTree']
    FMT_GET_4_TREE=[('DescTree','')]
    def __init__(self,tagName='ressourceHumanMember',tagNameUsr=None):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeBase.__init__(self,tagName)
        self.tagNameUsr=tagNameUsr
        self.dt=vtDateTime(True)
        #self.LST_TYPES=[(_(u'project'),0),(_(u'vacation'),-1)]
        self.LST_TYPES=[(0,0),(-1,-1)]
        #self.LST_TYPES=[('__vac',-1),('__prj',0)]
    def GetDescription(self):
        return _(u'human members')
    # ---------------------------------------------------------
    # specific
    def __setVacData__(self,oVac,node,*args,**kwargs):
        docHum=self.doc.GetNetDoc('vHum')
        nodeUsr=docHum.getNodeByIdNum(kwargs['idHum'])
        self.dt.Now()
        sDt=self.dt.GetDateSmallStr()
        sDtTm=self.dt.GetDateTimeStr(' ')
        oVac=self.doc.GetRegisteredNode('vacation')
        oHum=docHum.GetRegisteredNode('user')
        sTag=oHum.GetName(nodeUsr)
        oVac.SetTag(node,sTag+' '+sDt)
        oVac.SetName(node,sTag+' '+sDtTm)
        oVac.SetOwner(node,kwargs['idHum'])
        #self.doc.AlignNode(node)
    def DoAddVac(self,wid,node,idHum):
        oVac=self.doc.GetRegisteredNode('vacation')
        nodeTmp=None
        oUsrRef=self.doc.GetRegisteredNode('userReferenced')
        for c in self.doc.getChilds(node,oUsrRef.GetTagName()):
            if idHum==oUsrRef.GetUsrId(c):
                nodeTmp=c
                break
        if nodeTmp is not None:
            oVac.DoAdd(wid,nodeTmp,bModal=True,
                    func=self.__setVacData__,idHum=idHum)
            return True
        else:
            print 'false'
            return False
    def GetDescTree(self,node):
        return self.GetDescription()
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'description')
    
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'description',val)
    
    def IsResource2Draw(self):
        return True
    def __addHum__(self,node,dResource):
        try:
            sTag=self.doc.getTagName(node)
            if sTag==self.tagNameUsr:
                oUsrRef=self.doc.GetRegisteredNode(self.tagNameUsr)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
                #sLbl=''.join([oUsrRef.GetSurName(node),' ',oUsrRef.GetFirstName(node)])
                fid=oUsrRef.GetUsrId(node)
                fid=self.doc.getAttribute(node,'fid')
                if 0:
                    for tup in self.LST_TYPES:
                        d=dResource[tup[0]]
                        if sLbl not in d:
                            d[sLbl]=[8.0,{},
                                    self.doc.getKeyNum(self.doc.getParent(node)),
                                    self.doc.getKeyNum(node),tup[1]]
                if fid not in dResource:
                    if oUsrRef.GetInActive(node)==False:
                        sLbl=''.join([oUsrRef.GetSurName(node),' ',oUsrRef.GetFirstName(node)])
                        dResource[fid]={'__label':sLbl,'__sum':[8.0,{}]}
                    else:
                        return 0
                        dResource[fid]=None
                d=dResource[fid]
                #if d is None:
                #    return
                idPar=self.doc.getKeyNum(self.doc.getParent(node))
                id=self.doc.getKeyNum(node)
                d[idPar]={}
                dd=d[idPar]
                #dd[-1]=[8.0,{},idPar,id,-1]
                #dd[0]={}
                for tup in self.LST_TYPES:
                    dd[tup[0]]=[8.0,{},idPar,id,tup[1],{
                            'idPar':idPar,
                            'id':id,
                            'type':tup[1],
                            '__per':100.0,
                            '__lim':100.0
                            }]
                    #d[]=[8.0,{},
                    #                self.doc.getKeyNum(self.doc.getParent(node)),
                    #                self.doc.getKeyNum(node),sLbl]
                #sLbl=''.join([' ',_('human'),': ',oUsrRef.GetSurName(node),' ',oUsrRef.GetFirstName(node)])
                #if dResource.has_key(sLbl)==False:
                #    dResource[' '.join([sLbl,_(u'(vac)')])]=[8.0,{},long(self.doc.getKey(self.doc.getParent(node))),long(self.doc.getKey(node)),0]
                #    dResource[sLbl]=[8.0,{},long(self.doc.getKey(self.doc.getParent(node))),long(self.doc.getKey(node)),1]
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def Add2ResourceDict(self,node,dResource):
        if 0:
            for tup in self.LST_TYPES:
                if  tup[0] not in dResource:
                    dResource[tup[0]]={}
        
        self.doc.procChildsKeys(node,self.__addHum__,dResource)
    def __addVacPrj__(self,node,iYear,iMon,iMonCount,dYear,lRes,sTag2Match):
        try:
            if self.VERBOSE:
                print
                #vtLog.CallStack('')
            sTag=self.doc.getTagName(node)
            if sTag==sTag2Match:
                oVac=self.doc.GetRegisteredNode(sTag2Match)
                #oVac.GetResourceData(iYear,iMon,dData)
                iResult=oVac.GetResultInt(node)
                if iResult<0:
                    if self.VERBOSE:
                        print '   ','skip'
                    return 0
                if self.VERBOSE:
                    print '   ',iYear,iMon,dYear,lRes,sTag2Match
                id=self.doc.getKeyNum(node)
                sDt=oVac.GetStartDt(node)
                self.dt.SetStr(sDt)
                iYrS=self.dt.GetYear()
                iMhS=self.dt.GetMonth()
                iDyS=self.dt.GetDay()
                iHrS=self.dt.GetHour()
                iMnS=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                sDt=oVac.GetEndDt(node)
                self.dt.SetStr(sDt)
                iYrE=self.dt.GetYear()
                iMhE=self.dt.GetMonth()
                iDyE=self.dt.GetDay()
                iHrE=self.dt.GetHour()
                iMnE=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                    print '   ','year',iYear,iYrS,iYrE
                for iMonRepeat in xrange(iMonCount):
                    if self.VERBOSE:
                        print '   rep:',iMonRepeat,iYear,iMon
                    if iYear>=iYrS and iYear<=iYrE:
                        if iYear in dYear:
                            dMon=dYear[iYear]
                        else:
                            dMon={}
                            dYear[iYear]=dMon
                        if iYear==iYrS:
                            iMhST=iMhS
                        else:
                            iMhST=1
                        iMhET=12
                        if iYear==iYrE:
                            iMhET=iMhE
                        #if iMon>=iMhS and iMon<=iMhE:
                        if self.VERBOSE:
                            print '   ','mon',iMhST,iMhET,iMon
                        if iMon>=iMhST and iMon<=iMhET:
                            if iMon in dMon:
                                dData=dMon[iMon]
                                bMonCalc=False
                            else:
                                dData={}
                                dMon[iMon]=dData
                                bMonCalc=True
                            if bMonCalc:
                                oUsr=self.doc.GetRegisteredNode('userReferenced')
                                #if iMon>iMhS:
                                #    iDyS=1
                                #if iMon<iMhE:
                                #    iDyE=31
                                if iYear==iYrS:
                                    if iMon>iMhS:
                                        iDyST=1
                                    else:
                                        iDyST=iDyS
                                else:
                                    iDyST=1
                                if iYear==iYrE:
                                    if iMon<iMhE:
                                        iDyET=31
                                    else:
                                        iDyET=iDyE
                                else:
                                    iDyET=31
                                #for iDy in xrange(iDyS,iDyE+1):
                                if self.VERBOSE:
                                    print '   ','min',iMhST,iMhET
                                    print '   ','day',iDyST,iDyET
                                for iDy in xrange(iDyST,iDyET+1):
                                    iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(node,iYear,iMon,iDy)
                                    if iLmNr>0:
                                        try:
                                            l=dData[iDy]
                                        except:
                                            l=[]
                                            dData[iDy]=l
                                        if iYrS==iYear and iMhS==iMon and iDyS==iDy:
                                            iHrScal,iMnScal,bMod=vtTimeLimit.boundToLower(iHrS,iMnS,iHrNrS,iMnNrS)
                                            iHrScal,iMnScal,bMod=vtTimeLimit.boundToHigher(iHrScal,iMnScal,iHrNrE,iMnNrE)
                                            #FIXME handle out of limit
                                            #if bMod:
                                            #    iHr=iMn=-1
                                        else:
                                            iHrScal=iHrNrS
                                            iMnScal=iMnNrS
                                        if iYrE==iYear and iMhE==iMon and iDyE==iDy:
                                            iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToHigher(iHrE,iMnE,iHrNrE,iMnNrE)
                                            iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToLower(iHrEcal,iMnEcal,iHrNrS,iMnNrS)
                                            #FIXME handle out of limit
                                            #if bMod:
                                            #    iHr=iMn=-1
                                        else:
                                            iHrEcal=iHrNrE
                                            iMnEcal=iMnNrE
                                        if iHrS>=0 and iHrE>=0:
                                            ll=[]
                                            vtTimeLimit.addRange(ll,iHrScal,iMnScal,iHrEcal,iMnEcal)
                                            for t in ll:
                                                l.append((t[0],t[1],iResult,id))
                    iMon+=1
                    if iMon>12:
                        iMon=1
                        iYear+=1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __addVacPrjPer__(self,node,iYear,iMon,iMonCount,dYear,llRes,sTag2Match):
        try:
            if self.VERBOSE:
                print
                #vtLog.CallStack('')
            sTag=self.doc.getTagName(node)
            if sTag==sTag2Match:
                oVac=self.doc.GetRegisteredNode(sTag2Match)
                #id=self.getKey(node)
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCurCls(vtLog.DEBUG,'lRes:%s;id:%s'%
                #                (vtLog.pformat(lRes),id),self)
                #oVac.GetResourceData(iYear,iMon,dData)
                iResult=oVac.GetResultInt(node)
                if iResult<0:
                    if self.VERBOSE:
                        print '   ','skip'
                    return 0
                fid=oVac.GetPrjFIDStr(node)
                dRes=llRes[1]
                if fid in dRes:
                    lRes=dRes[fid]
                else:
                    idPar=-1
                    id=self.doc.getKeyNum(node)
                    l=oVac.GetPrjInfos(node)
                    lRes=[u', '.join(l),{},{
                                'id':id,
                                'fid':fid,
                                '__res':iResult,
                                '__per':100.0,#oVac.GetPercentageFloat(node),
                                '__lim':100.0,
                                },
                                ]
                    dRes[fid]=lRes
                #dData=lRes[1]
                dYear=lRes[1]
                iYearS=iYear
                iMonS=iMon
                iMonE=iMon+iMonCount-2
                iYearE=iYear+(iMon/12)
                iMonE=(iMonE%12)+1
                if self.VERBOSE:
                    print '   ',iYear,iMon,dYear,lRes,sTag2Match
                    print '   ',iYearS,iMonS,iYearE,iMonE
                fPer=oVac.GetPercentageFloat(node)/100.0
                sDt=oVac.GetStartDt(node)
                self.dt.SetStr(sDt)
                
                iYrS=self.dt.GetYear()
                iMhS=self.dt.GetMonth()
                iDyS=self.dt.GetDay()
                iHrS=self.dt.GetHour()
                iMnS=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                sDt=oVac.GetEndDt(node)
                self.dt.SetStr(sDt)
                iYrE=self.dt.GetYear()
                iMhE=self.dt.GetMonth()
                iDyE=self.dt.GetDay()
                iHrE=self.dt.GetHour()
                iMnE=self.dt.GetMinute()
                if self.VERBOSE:
                    print '   ',sDt
                    print '   ','year',iYear,iYrS,iYrE
                for iMonRepeat in xrange(iMonCount):
                    if self.VERBOSE:
                        print '   rep:',iMonRepeat,iYear,iMon
                    if iYear>=iYrS and iYear<=iYrE:
                        if iYear not in dYear:
                            dMon={}
                            dYear[iYear]=dMon
                        else:
                            dMon=dYear[iYear]
                        if iYear==iYrS:
                            iMhST=iMhS
                        else:
                            iMhST=1
                        iMhET=12
                        if iYear==iYrE:
                            iMhET=iMhE
                        #if iMon>=iMhS and iMon<=iMhE:
                        if self.VERBOSE:
                            print '   ','mon',iMhST,iMhET,iMon
                        if iMon>=iMhST and iMon<=iMhET:
                            if iMon in dMon:
                                dData=dMon[iMon]
                                bMonCalc=False
                            else:
                                dData={}
                                dMon[iMon]=dData
                                bMonCalc=True
                            if bMonCalc:
                                oUsr=self.doc.GetRegisteredNode('userReferenced')
                                #if iMon>iMhS:
                                #    iDyS=1
                                #if iMon<iMhE:
                                #    iDyE=31
                                if iYear==iYrS:
                                    if iMon>iMhS:
                                        iDyST=1
                                    else:
                                        iDyST=iDyS
                                else:
                                    iDyST=1
                                if iYear==iYrE:
                                    if iMon<iMhE:
                                        iDyET=31
                                    else:
                                        iDyET=iDyE
                                else:
                                    iDyET=31
                                #for iDy in xrange(iDyS,iDyE+1):
                                if self.VERBOSE:
                                    print '   ','min',iMhST,iMhET
                                    print '   ','day',iDyST,iDyET
                                for iDy in xrange(iDyST,iDyET+1):
                                    iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(node,iYear,iMon,iDy)
                                    if iLmNr>0:
                                        if iYrS==iYear and iMhS==iMon and iDyS==iDy:
                                            iHrScal,iMnScal,bMod=vtTimeLimit.boundToLower(iHrS,iMnS,iHrNrS,iMnNrS)
                                            iHrScal,iMnScal,bMod=vtTimeLimit.boundToHigher(iHrScal,iMnScal,iHrNrE,iMnNrE)
                                            #FIXME handle out of limit
                                            #if bMod:
                                            #    iHr=iMn=-1
                                        else:
                                            iHrScal=iHrNrS
                                            iMnScal=iMnNrS
                                        if iYrE==iYear and iMhE==iMon and iDyE==iDy:
                                            iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToHigher(iHrE,iMnE,iHrNrE,iMnNrE)
                                            iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToLower(iHrEcal,iMnEcal,iHrNrS,iMnNrS)
                                            #FIXME handle out of limit
                                            #if bMod:
                                            #    iHr=iMn=-1
                                        else:
                                            iHrEcal=iHrNrE
                                            iMnEcal=iMnNrE
                                        if iHrS>=0 and iHrE>=0:
                                            fVal=(iHrEcal-iHrScal)+(iMnEcal-iMnScal)/60.0
                                            fVal*=fPer
                                            if self.VERBOSE:
                                                print fVal
                                            if iDy in dData:
                                                dData[iDy]=dData[iDy]+fVal
                                            else:
                                                dData[iDy]=fVal
                                            #vtTimeLimit.addRange(l,iHrScal,iMnScal,iHrEcal,iMnEcal)
                    iMon+=1
                    if iMon>12:
                        iMon=1
                        iYear+=1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __addHumVac__(self,node,iYear,iMon,iMonCount,dData,lRes):
        try:
            sTag=self.doc.getTagName(node)
            if sTag=='userReferenced':
                #o=self.doc.GetRegisteredNode(sTag)
                #o.Get
                id=self.doc.getKey(node)
                if self.VERBOSE:
                    print lRes,id
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'lRes:%s;id:%s'%
                                (vtLog.pformat(lRes),id),self)
                if (lRes[3]==long(id)) and (lRes[4]==-1):
                    if self.VERBOSE:
                        vtLog.CallStack('')
                        vtLog.pprint(dData)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self)
                    self.doc.procChildsKeys(node,self.__addVacPrj__,iYear,iMon,iMonCount,dData,lRes,'vacation')
                    if self.VERBOSE:
                        vtLog.pprint(dData)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self)
                if (lRes[3]==long(id)) and (lRes[4]==0):
                    if self.VERBOSE:
                        vtLog.CallStack('')
                        vtLog.pprint(dData)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self)
                    self.doc.procChildsKeys(node,self.__addVacPrjPer__,iYear,iMon,iMonCount,dData,lRes,'projectAssign')
                    if 0:
                        for dy in dData.keys():
                            fVal=dData[dy]
                            
                            l=[((0,0),(int(fVal),int(round((fVal%1)*60))))]
                            dData[dy]=l
                    if self.VERBOSE:
                        vtLog.pprint(dData)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def GetResourceData(self,node,iYear,iMon,iMonCount,dData,lRes):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'dData:%s;lRes:%s'%
                    (vtLog.pformat(dData),vtLog.pformat(lRes)),self)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(dData)
            vtLog.pprint(lRes)
        self.doc.procChildsKeys(node,self.__addHumVac__,iYear,iMon,iMonCount,dData,lRes)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'dData:%s'%
                    (vtLog.pformat(dData)),self)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        #return True
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01%IDAT8\x8d\xa5\x93=K\xc3P\x14\x86\x9f\xa4\x9a.\xb6D\xa9 ]\xa4C\xaa7\
\xa3uU3\x15\x07A\xc4\xbf\xe2\xac\xd9\xec\xd2\xdf\xa1\xa3\x83? U\xdc\xac\x9d4\
E\x11\xcc Q\x9a\xad]\xcbq\xa8\x15\x9a/\xac\x1e8p?\x0e\xcf}\xcf{\xef\xd54\xbd\
\xc0\x7fB\xcf\xda(\x1a\x8bb\x9ae\xf9\x13`\xbf\xd9\x94\xe0-\xa0u\xde\xa2RY\
\xc9\x85\xa4\x02,\xcb\xc2(\x1a\xd4\xd6k\x98\xcbf\xae\x82\x85\xb4\xc5\xf0\xf5\
\x82v\xbbL\xf8\x19\x12\r\xa2\\\x00\x9a^\x98I\xf7\xc4\x11\xe9\xba\xe26\x10\
\xb7\x81x\x07\x88\xb3\x8d\xc4\xeb\xa6\x99T0\xf2@\xf78\xbbv'\xf3\x07\x17\xe7\
\x14:\xf3\xb4\x00\xc0\xc77\xe0*\xbf\x83\xcck\xfcm$\x00\xde}\xb2h4\xcc!\xa4\
\x19#=dx\x89H\x0fy\xd9\xcd60\xdd\xc4\xe9\xa9}\xe0\x1d\xd6\xaa\xb0\xa9\xea\
\x02\xd0\xf7\x9f\xb5x]\xb6\x89\xc0R\t(M\xc6\xcaV([\x89\xff\xe4\xcf\x80\xe66Q\
\xd9\x8a\xa3\xe3\xc3\x9f\xe7\x9d\xaa\xa0\xd3\x85\x8d\x0c@\\\x81\x96\xf5\x9d\
\xf7\xb6\xc6\xe2T\xe11Xe\\\xdf!\x1aD\xdc\xde\xdc%<\xf8\x02;\x99Y\xc7\xf9\xc9\
\x06\xfb\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeRessourceHumanMemberPanel
        else:
            return None

