#Boa:FramePanel:vXmlNodeUserReferencedPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeUserReferencedPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080413
# CVS-ID:       $Id: vXmlNodeUserReferencedPanel.py,v 1.2 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmListCtrl
import wx.lib.buttons

import sys
import calendar

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.time.vtTime as vtTime

[wxID_VXMLNODEUSERREFERENCEDPANEL, wxID_VXMLNODEUSERREFERENCEDPANELCBDEC, 
 wxID_VXMLNODEUSERREFERENCEDPANELCBINC, 
 wxID_VXMLNODEUSERREFERENCEDPANELCHCMONTH, 
 wxID_VXMLNODEUSERREFERENCEDPANELLSTALLOC, 
 wxID_VXMLNODEUSERREFERENCEDPANELSPMONCOUNT, 
 wxID_VXMLNODEUSERREFERENCEDPANELSPYEAR, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeUserReferencedPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsDate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spYear, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbDec, 0, border=0, flag=0)
        parent.AddWindow(self.chcMonth, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbInc, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.spMonCount, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDate, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lstAlloc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_lstAlloc_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'name'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsDate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsDate_Items(self.bxsDate)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vXmlNodeUserReferencedPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.spYear = wx.SpinCtrl(id=wxID_VXMLNODEUSERREFERENCEDPANELSPYEAR,
              initial=2000, max=2100, min=0, name=u'spYear', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(79, 30), style=wx.SP_ARROW_KEYS)
        self.spYear.SetMinSize((-1,-1))
        self.spYear.Bind(wx.EVT_SPINCTRL, self.OnUpdateDate,
              id=wxID_VXMLNODEUSERREFERENCEDPANELSPYEAR)
        self.spYear.Bind(wx.EVT_TEXT_ENTER, self.OnUpdateDate,
              id=wxID_VXMLNODEUSERREFERENCEDPANELSPYEAR)

        self.cbDec = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Left),
              id=wxID_VXMLNODEUSERREFERENCEDPANELCBDEC, name=u'cbDec',
              parent=self, pos=wx.Point(79, 0), size=wx.Size(31, 30), style=0)
        self.cbDec.Bind(wx.EVT_BUTTON, self.OnCbDecButton,
              id=wxID_VXMLNODEUSERREFERENCEDPANELCBDEC)

        self.chcMonth = wx.Choice(choices=[],
              id=wxID_VXMLNODEUSERREFERENCEDPANELCHCMONTH, name=u'chcMonth',
              parent=self, pos=wx.Point(110, 0), size=wx.Size(105, 21),
              style=0)
        self.chcMonth.SetMinSize(wx.Size(-1, -1))
        self.chcMonth.Bind(wx.EVT_CHOICE, self.OnUpdateDateChc,
              id=wxID_VXMLNODEUSERREFERENCEDPANELCHCMONTH)

        self.cbInc = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Right),
              id=wxID_VXMLNODEUSERREFERENCEDPANELCBINC, name=u'cbInc',
              parent=self, pos=wx.Point(215, 0), size=wx.Size(31, 30), style=0)
        self.cbInc.Bind(wx.EVT_BUTTON, self.OnCbIncButton,
              id=wxID_VXMLNODEUSERREFERENCEDPANELCBINC)

        self.spMonCount = wx.SpinCtrl(id=wxID_VXMLNODEUSERREFERENCEDPANELSPMONCOUNT,
              initial=1, max=48, min=1, name=u'spMonCount', parent=self,
              pos=wx.Point(254, 0), size=wx.Size(48, 30),
              style=wx.SP_ARROW_KEYS)
        self.spMonCount.SetMinSize((-1,-1))
        self.spMonCount.SetValue(1)
        self.spMonCount.Bind(wx.EVT_TEXT_ENTER, self.OnUpdateDate,
              id=wxID_VXMLNODEUSERREFERENCEDPANELSPMONCOUNT)

        self.lstAlloc = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VXMLNODEUSERREFERENCEDPANELLSTALLOC,
              name=u'lstAlloc', parent=self, pos=wx.Point(0, 30),
              size=wx.Size(304, 150), style=wx.LC_REPORT)
        self._init_coll_lstAlloc_Columns(self.lstAlloc)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.lstAlloc.SetStretchLst([])
        self.dRes={}
        self.dDate={}
        self.bTranspose=True
        self.dt=vtTime.vtDateTime(True)
        self.dt.SetDay(1)
        self.__updateLang__()
        self.__showDate__()
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __updateLang__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.chcMonth.Clear()
        for i in range(1,13):
            self.chcMonth.Append(calendar.month_name[i])
    def __showDate__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.spYear.SetValue(self.dt.GetYear())
        self.chcMonth.SetSelection(self.dt.GetMonth()-1)
        
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iYearStart,iMonStart=iYear,iMon
        iMonCount=self.spMonCount.GetValue()
        iCount=self.lstAlloc.GetColumnCount()-1
        for i in xrange(iCount):
            self.lstAlloc.DeleteColumn(1)
        iFmt=wx.LIST_FORMAT_RIGHT
        if self.bTranspose:
            self.lstAlloc.DeleteAllItems()
            self.lstAlloc.DeleteColumn(0)
            self.lstAlloc.InsertColumn(0,heading=_('date'),format=iFmt,
                            width=180)
        iCol=1
        for iMonRep in xrange(iMonCount):
            mrng=calendar.monthrange(iYear,iMon)
            iDay,iDays=mrng
            iDays+=1
            for i in range(1,iDays):
                sTag=calendar.day_abbr[iDay]
                iDay+=1
                iDay%=7
                sDate='%04d-%02d-%02d %s'%(iYear,iMon,i,sTag)
                #print self.bTranspose,sDate
                if self.bTranspose:
                    idx=self.lstAlloc.InsertStringItem(sys.maxint,sDate)
                    #print '  ',idx
                else:
                    self.lstAlloc.InsertColumn(iCol,heading=sDate,format=iFmt,width=40)
                    iCol+=1
                    #self.dData[(iYear,iMon,i)]
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.dRes={}
            self.dDate={}
            if self.bTranspose:
                iCount=self.lstAlloc.GetColumnCount()-1
                for i in xrange(iCount):
                    self.lstAlloc.DeleteColumn(1)
                
            else:
                self.lstAlloc.DeleteAllItems()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.dRes={}
            dRes={}
            if self.node is not None:
                par=self.doc.getParent(self.node)
                if par is not None:
                    dRes=self.doc.BuildResDict(par,bStore=False)
                #    par=self.doc.getParent(par)
                #    if par is not None:
                #        self.dRes=self.doc.BuildResDict(par,bStore=False)
            fid=self.doc.getAttribute(self.node,'fid')
            if fid in dRes:
                self.dRes[fid]=dRes[fid]
            self.__showAll__()
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        # add code here
        
        vtXmlNodePanel.Close(self)
    def Cancel(self):
        # add code here
        
        vtXmlNodePanel.Cancel(self)
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def OnCbDecButton(self, event):
        try:
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon-=1
            if iMon<1:
                iYear-=1
                iMon=12
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnUpdateDate(self,evt):
        evt.Skip()
        try:
            iYear=int(self.spYear.GetValue())
            self.dt.SetYear(iYear)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnUpdateDateChc(self, event):
        event.Skip()
        try:
            self.dt.SetMonth(self.chcMonth.GetSelection()+1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def OnCbIncButton(self, event):
        try:
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.__showAll__()
        except:
            self.__logTB__()
    def __showAll__(self):
        try:
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            iMonCount=self.spMonCount.GetValue()
            dVal=self.doc.GetResourceData(iYear,iMonth,iMonCount,dRes=self.dRes)
            if self.VERBOSE:
                vtLog.CallStack('')
                print 'year',iYear,'month',iMonth,'mon count',iMonCount
                print vtLog.pformat(dVal)
            keys=[k for k in dVal.keys() if k.startswith('__')==False]
            dVal=dVal[keys[0]]
            keys=[k for k in dVal.keys() if k.startswith('__')==False]
            keys.sort()
            iCol=1
            iFmt=wx.LIST_FORMAT_RIGHT
            self.lstAlloc.Freeze()
            if self.bTranspose:
                iL=self.lstAlloc.GetItemCount()
                lSum=[0.0]*iL
                lStretch=[]
                for k in keys:
                    self.lstAlloc.InsertColumn(iCol,heading=k,format=iFmt,width=120)
                    lStretch.append((iCol,-1))
                    iCol+=1
                self.lstAlloc.InsertColumn(iCol,heading=_('sum'),format=iFmt,width=120)
                lStretch.append((iCol,-1))
                self.lstAlloc.SetStretchLst(lStretch)
                self.lstAlloc.__setup__()
                wx.CallAfter(self.lstAlloc.OnSizeStretch,None)
                iCol=0
            else:
                iL=self.lstAlloc.GetColumnCount()
                lSum=[0.0]*iL
            wx.CallAfter(self.lstAlloc.Thaw)
            idx=0
            for k in keys:
                fLimit=dVal[k][0]
                dYear=dVal[k][1]
                if self.bTranspose:
                    idx=0
                    iCol+=1
                self.__showDict__(iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear,lSum)
            if '__sum_' in dVal:
                fLimit=dVal['__sum'][0]
                dYear=dVal['__sum'][1]
                if self.bTranspose:
                    idx=0
                    iCol+=1
                self.__showDict__(iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear)
            if self.bTranspose:
                idx=0
                iCol+=1
                for fVal in lSum:
                    if fVal>0.0:
                        self.lstAlloc.SetStringItem(idx,iCol,
                                        vtLgBase.format('%4.1f',fVal),-1)
                    idx+=1
                
        except:
            self.__logTB__()
    def __showDict__(self,iYear,iMonth,iMonCount,idx,iCol,fLimit,dYear,lSum):
        try:
            #if self.bTranspose:
            #    idx=0
                #iCol+=1
            for iMonRep in xrange(iMonCount):
                mrng=calendar.monthrange(iYear,iMonth)
                iDay,iDays=mrng
                iDays+=1
                for i in range(1,iDays):
                    sTag=calendar.day_abbr[iDay]
                    iDay+=1
                    iDay%=7
                    #sDate='%04d-%02d-%02d %s'%(iYear,iMon,i,sTag)
                    #print self.bTranspose,sDate
                    if self.bTranspose:
                        if iYear in dYear:
                            dMon=dYear[iYear]
                            if iMonth in dMon:
                                dDay=dMon[iMonth]
                                if i in dDay:
                                    fVal=dDay[i]/fLimit*100.0
                                    lSum[idx]=lSum[idx]+fVal
                                    self.lstAlloc.SetStringItem(idx,iCol,
                                                    vtLgBase.format('%4.1f',fVal),-1)
                                        
                        idx+=1
                        #idx=self.lstAlloc.InsertStringItem(sys.maxint,sDate)
                        #print '  ',idx
                    else:
                        #self.lstAlloc.InsertColumn(iCol,heading=sDate,format=iFmt,width=40)
                        iCol+=1
                        
                        #self.dData[(iYear,iMon,i)]
                iMonth+=1
                if iMonth>12:
                    iYear+=1
                    iMonth=1
        except:
            self.__logTB__()
