#Boa:FramePanel:vXmlNodeRessourceHumanMemberPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceHumanMemberPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vXmlNodeRessourceHumanMemberPanel.py,v 1.8 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime

import vidarc.vApps.vRessource.images as imgRes

[wxID_VXMLNODERESSOURCEHUMANMEMBERPANEL, 
 wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQPRJ, 
 wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQVAC, 
 wxID_VXMLNODERESSOURCEHUMANMEMBERPANELLSTHUM, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vXmlNodeRessourceHumanMemberPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstHum, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsReq, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsReq_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbReqPrj, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbReqVac, 1, border=0, flag=wx.EXPAND)

    def _init_coll_lstHum_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'surname'), width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'firstname'), width=120)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'name'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=4)

        self.bxsReq = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsReq_Items(self.bxsReq)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANEL,
              name=u'vXmlNodeRessourceHumanMemberPanel', parent=prnt,
              pos=wx.Point(405, 257), size=wx.Size(301, 181),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(293, 154))

        self.lstHum = wx.ListCtrl(id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELLSTHUM,
              name=u'lstHum', parent=self, pos=wx.Point(0, 0), size=wx.Size(293,
              120), style=wx.LC_REPORT)
        self.lstHum.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstHum_Columns(self.lstHum)
        self.lstHum.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstHumListColClick,
              id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELLSTHUM)
        self.lstHum.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHumListItemDeselected,
              id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELLSTHUM)
        self.lstHum.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHumListItemSelected,
              id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELLSTHUM)

        self.cbReqPrj = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQPRJ,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Request for Project'),
              name=u'cbReqPrj', parent=self, pos=wx.Point(0, 124),
              size=wx.Size(142, 30), style=0)
        self.cbReqPrj.SetMinSize(wx.Size(-1, -1))
        self.cbReqPrj.Bind(wx.EVT_BUTTON, self.OnCbReqPrjButton,
              id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQPRJ)

        self.cbReqVac = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQVAC,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Request Vacation'),
              name=u'cbReqVac', parent=self, pos=wx.Point(150, 124),
              size=wx.Size(142, 30), style=0)
        self.cbReqVac.SetMinSize(wx.Size(-1, -1))
        self.cbReqVac.Bind(wx.EVT_BUTTON, self.OnCbReqVacButton,
              id=wxID_VXMLNODERESSOURCEHUMANMEMBERPANELCBREQVAC)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        self.lHum=[]
        self.dt=vtDateTime(True)
        
        self.cbReqPrj.SetBitmapLabel(imgRes.getPrjBitmap())
        self.cbReqVac.SetBitmapLabel(imgRes.getVactionBitmap())
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.lHum=[]
            self.lstHum.DeleteAllItems()
            self.selHum=-1
            self.cbReqPrj.Enable(False)
            self.cbReqVac.Enable(False)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
    def __setUsrRef__(self,oUsrRef,node,idHum=-1):
        oUsrRef.SetUsrId(node,idHum)
    def __showHum__(self):
        try:
            self.lstHum.DeleteAllItems()
            l=[]
            oUsr=self.docHum.GetRegisteredNode('user')
            for id in self.lHum:
                tmp=self.docHum.getNodeById(id)
                if tmp is not None:
                    l.append(oUsr.AddValuesToLst(tmp)+[long(id)])
                #print tmp
            l.sort()
            lUsrId=[]
            for tup in l:
                idx=self.lstHum.InsertStringItem(sys.maxint,tup[0])
                self.lstHum.SetItemData(idx,tup[-1])
                lUsrId.append(tup[-1])
                for i in xrange(1,3):
                    self.lstHum.SetStringItem(idx,i,tup[i])
            lUsrId.sort()
            lUsrIdRef=[]
            oUsrRef=self.doc.GetRegisteredNode('userReferenced')
            for c in self.doc.getChilds(self.node,oUsrRef.GetTagName()):
                lUsrIdRef.append(oUsrRef.GetUsrId(c))
            for id in lUsrId:
                try:
                    i=lUsrIdRef.index(id)
                except:
                    c=oUsrRef.Create(self.node,self.__setUsrRef__,idHum=id)
                    self.doc.AlignNode(c)
                    pass
        except:
            self.__logTB__()
    def __getAndShowHum__(self,fid):
        try:
            self.lHum=self.docHum.GetGroupMemberLst(fid)
            self.docHum.CallBack(self.__showHum__)
        except:
            self.__logTB__()
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            tmp=self.doc.getParent(self.node)
            nodeGrp=self.doc.getChild(tmp,'group')
            fid=self.objRegNode.GetForeignKey(nodeGrp,'fid','vHum')
            #self.lHum=self.docHum.GetGroupMemberLst(fid)
            #self.__showHum__()
            self.docHum.DoSafe(self.__getAndShowHum__,fid)
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            try:
                # add code here
                pass
            except:
                self.__logTB__()
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnLstHumListColClick(self, event):
        event.Skip()

    def OnLstHumListItemDeselected(self, event):
        self.selHum=-1
        self.cbReqPrj.Enable(False)
        self.cbReqVac.Enable(False)
        event.Skip()

    def OnLstHumListItemSelected(self, event):
        self.selHum=event.GetIndex()
        self.cbReqPrj.Enable(True)
        self.cbReqVac.Enable(True)
        event.Skip()
    def __setPrjData__(self,oPrjAsgn,node,*args,**kwargs):
        nodeUsr=self.docHum.getNodeByIdNum(kwargs['idHum'])
        self.dt.Now()
        sDt=self.dt.GetDateSmallStr()
        sDtTm=self.dt.GetDateTimeStr(' ')
        #oVac=self.doc.GetRegisteredNode('vacation')
        oHum=self.docHum.GetRegisteredNode('user')
        sTag=oHum.GetName(nodeUsr)
        oPrjAsgn.SetTag(node,sTag+' '+sDt)
        oPrjAsgn.SetName(node,sTag+' '+sDtTm)
        oPrjAsgn.SetOwner(node,kwargs['idHum'])
        #self.doc.AlignNode(node)
    def OnCbReqPrjButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'you are not supposed to be here',self)
                return
            if self.selHum>=0:
                idHum=self.lstHum.GetItemData(self.selHum)
                oPrjAsgn=self.doc.GetRegisteredNode('projectAssign')
                nodeTmp=None
                oUsrRef=self.doc.GetRegisteredNode('userReferenced')
                for c in self.doc.getChilds(self.node,oUsrRef.GetTagName()):
                    if idHum==oUsrRef.GetUsrId(c):
                        nodeTmp=c
                        break
                if nodeTmp is not None:
                    #c=oVac.Create(nodeTmp,self.__setVacData__,idHum=idHum)
                    oPrjAsgn.DoAdd(self,nodeTmp,True,self.__setPrjData__,idHum=idHum)
                    #self.doc.AlignNode(c)
        except:
            self.__logTB__()
    def __setVacData__(self,oVac,node,*args,**kwargs):
        nodeUsr=self.docHum.getNodeByIdNum(kwargs['idHum'])
        self.dt.Now()
        sDt=self.dt.GetDateSmallStr()
        sDtTm=self.dt.GetDateTimeStr(' ')
        oVac=self.doc.GetRegisteredNode('vacation')
        oHum=self.docHum.GetRegisteredNode('user')
        sTag=oHum.GetName(nodeUsr)
        oVac.SetTag(node,sTag+' '+sDt)
        oVac.SetName(node,sTag+' '+sDtTm)
        oVac.SetOwner(node,kwargs['idHum'])
        #self.doc.AlignNode(node)
    def OnCbReqVacButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'you are not supposed to be here',self)
                return
            if self.selHum>=0:
                idHum=self.lstHum.GetItemData(self.selHum)
                self.objRegNode.DoAddVac(self,self.node,idHum)
                return
                oVac=self.doc.GetRegisteredNode('vacation')
                nodeTmp=None
                oUsrRef=self.doc.GetRegisteredNode('userReferenced')
                for c in self.doc.getChilds(self.node,oUsrRef.GetTagName()):
                    if idHum==oUsrRef.GetUsrId(c):
                        nodeTmp=c
                        break
                if nodeTmp is not None:
                    #c=oVac.Create(nodeTmp,self.__setVacData__,idHum=idHum)
                    oVac.DoAdd(self,nodeTmp,True,self.__setVacData__,idHum=idHum)
                    #self.doc.AlignNode(c)
        except:
            self.__logTB__()
