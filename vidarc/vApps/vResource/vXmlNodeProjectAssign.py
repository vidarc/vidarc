#----------------------------------------------------------------------------
# Name:         vXmlNodeProjectAssign.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060912
# CVS-ID:       $Id: vXmlNodeProjectAssign.py,v 1.5 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtDateTimeStartEnd
import vidarc.tool.time.vtTimeLimit as vtTimeLimit
from vidarc.ext.state.veStateAttr import veStateAttr

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeProjectAssignPanel import vXmlNodeProjectAssignPanel
        #from vXmlNodeProjectAssignEditDialog import *
        #from vXmlNodeProjectAssignAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeProjectAssign(vtXmlNodeBase,veStateAttr):
    VERBOSE=0
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
            ('StartDt',None,'startDt',None),
            ('EndDt',None,'endDt',None),
            ('Duration',None,'duration',None),
            ('PrjState',None,'prjState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag']
    FMT_GET_4_TREE=[('Tag','')]
    def __init__(self,tagName='projectAssign'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeBase.__init__(self,tagName)
        veStateAttr.__init__(self,'smPrjAsgn','prjState')
        self.oSMlinker=None
        self.dt=vtDateTime(True)
        self.dtDiff=vtDateTimeStartEnd(True)
    def GetDescription(self):
        return _(u'project assignment')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'description')
    def GetStartDt(self,node):
        return self.Get(node,'startDt')
    def GetEndDt(self,node):
        return self.Get(node,'endDt')
    def GetDuration(self,node):
        return self.Get(node,'duration')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    def GetPercentage(self,node):
        return self.Get(node,'percentage')
    def GetPercentageFloat(self,node):
        try:
            s=self.GetPercentage(node)
            if len(s)==0:
                return 0.0
            return float(s)
        except:
            return 0.0
    def GetLogin(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetName(nodePar)
    def GetSurName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetSurName(nodePar)
    def GetFirstName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetFirstName(nodePar)
    def GetPrjFIDStr(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def GetPrjInfos(self,node):
        fid=self.GetChildAttrStr(node,'project','fid')
        netDoc,n=self.doc.GetNode(fid)
        if n is None:
            return None
        oPrj=netDoc.GetRegByNode(n)
        return oPrj.AddValuesToLstByLst(n,atts=['Name','PrjNumber'])

    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'description',val)
    def SetStartDt(self,node,val):
        self.Set(node,'startDt',val)
    def SetEndDt(self,node,val):
        self.Set(node,'endDt',val)
    def GetDataDict(self,node,dData,bModify=False):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,self.smAttr,'owner')
        nodeUsr=self.doc.GetNode(idOwner)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print node
            sDt=self.GetStartDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrS=self.dt.GetYear()
            iMhS=self.dt.GetMonth()
            iDyS=self.dt.GetDay()
            iHrS=self.dt.GetHour()
            iMnS=self.dt.GetMinute()
            sDt=self.GetEndDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrE=self.dt.GetYear()
            iMhE=self.dt.GetMonth()
            iDyE=self.dt.GetDay()
            iHrE=self.dt.GetHour()
            iMnE=self.dt.GetMinute()
            if self.VERBOSE:
                print 'start',iYrS,iMhS,iDyS,iHrS,iMnS
                print '  end',iYrE,iMhE,iDyE,iHrE,iMnE
                print '  mod',bModify
            for iYear in xrange(iYrS,iYrE+1):
                if iYear in dData:
                    dYear=dData[iYear]
                else:
                    dYear={}
                    dData[iYear]=dYear
                if iYear==iYrS:
                    iMhST=iMhS
                else:
                    iMhST=1
                iMhET=12
                if iYear==iYrE:
                    iMhET=iMhE
                for iMon in xrange(iMhST,iMhET+1):
                    if iMon in dYear:
                        dMon=dYear[iMon]
                    else:
                        dMon={}
                        dYear[iMon]=dMon
                    if iYear==iYrS:
                        if iMon>iMhS:
                            iDyST=1
                        else:
                            iDyST=iDyS
                    else:
                        iDyST=1
                    if iYear==iYrE:
                        if iMon<iMhE:
                            iDyET=31
                        else:
                            iDyET=iDyE
                    else:
                        iDyET=31
                    #for iDy in xrange(iDyS,iDyE+1):
                    for iDy in xrange(1,32):
                    #for iDy in xrange(iDyST,iDyET+1):
                        if self.VERBOSE:
                            print iYear,iMon,iDy
                        iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(nodeUsr,iYear,iMon,iDy)
                        #if (iDy<iDyS) or (iDy>iDyE):
                        #    iHrNrS,iMnNrS,iHrNrE,iMnNrE=0,0,0,0
                        if iLmNr>0:
                            bCalcDiff=True
                            if self.VERBOSE:
                                print 'start',iYrS,iYear,iMhS,iMon,iDyS,iDy,iHrS,iMnS
                            if iYrS==iYear and iMhS==iMon:
                                if iDyS==iDy:
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToLower(iHrS,iMnS,iHrNrS,iMnNrS)
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToHigher(iHrScal,iMnScal,iHrNrE,iMnNrE)
                                    #FIXME handle out of limit
                                    if bModify:
                                    #    iHr=iMn=-1
                                        sDt=self.GetStartDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrScal)
                                        self.dt.SetMinute(iMnScal)
                                        self.SetStartDt(node,self.dt.GetDateTimeStr())
                                        if self.VERBOSE:
                                            print '   ',sDt,
                                            print self.dt.GetDateTimeStr()
                                elif iDy<iDyS:
                                    iHrScal,iMnScal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrScal=iHrNrS
                                    iMnScal=iMnNrS
                            else:
                                iHrScal=iHrNrS
                                iMnScal=iMnNrS
                            
                            if self.VERBOSE:
                                print '  end',iYrE,iYear,iMhE,iMon,iDyE,iDy,iHrE,iMnE
                            if iYrE==iYear and iMhE==iMon:
                                if iDyE==iDy:
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToHigher(iHrE,iMnE,iHrNrE,iMnNrE)
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToLower(iHrEcal,iMnEcal,iHrNrS,iMnNrS)
                                    #FIXME handle out of limit
                                    if bModify:
                                    #    iHr=iMn=-1
                                        sDt=self.GetEndDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrEcal)
                                        self.dt.SetMinute(iMnEcal)
                                        self.SetEndDt(node,self.dt.GetDateTimeStr())
                                        if self.VERBOSE:
                                            print '   ',sDt,
                                            print self.dt.GetDateTimeStr()
                                elif iDy>iDyE:
                                    iHrEcal,iMnEcal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrEcal=iHrNrE
                                    iMnEcal=iMnNrE
                            else:
                                iHrEcal=iHrNrE
                                iMnEcal=iMnNrE
                            #if (iDy<iDyS) or (iDy>iDyE):
                            #    iHrScal,iMnScal,iHrEcal,iMnEcal=0,0,0,0
                            if iHrNrS>=0 and iHrNrE>=0:
                                self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrNrS,iMnNrS))
                                if iMnNrE==60:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE+1,0))
                                else:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE,iMnNrE))
                                self.dtDiff.CalcDiff()
                                iNrMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                if self.VERBOSE:
                                    print self.dtDiff.GetTimeDiffStr(' '),iNrMn
                                
                                if bCalcDiff==True:
                                    if self.VERBOSE:
                                        print '  ',iHrScal,iMnScal,iHrEcal,iMnEcal,self.dtDiff.GetMinuteDiff()
                                    self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrScal,iMnScal))
                                    if iMnEcal==60:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal+1,0))
                                    else:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal,iMnEcal))
                                    self.dtDiff.CalcDiff()
                                    iCalMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                    fPer=self.GetPercentageFloat(node)
                                    if fPer>0.0:
                                        iCalMn=iCalMn*(fPer/100.0)
                                else:
                                    iCalMn=0.0
                                if iDy in dMon:
                                    dMon[iDy]['calMn']+=iCalMn
                                else:
                                    dMon[iDy]={'calMn':iCalMn,'limMn':iNrMn,'limPer':iLmNr}
                                #fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                                if self.VERBOSE:
                                    print '    ','date',iYear,iMon,iDy
                                    #print '    ','dur',fDur
                                    print '    ',' res',dMon[iDy]
                            #    vtTimeLimit.addRange(l,iHrScal,iMnScal,iHrEcal,iMnEcal)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetDuration(self,node,val):
        if self.VERBOSE:
            vtLog.CallStack('')
        dData={}
        self.GetDataDict(node,dData,bModify=True)
        fDur=0.0
        for dYear in dData.itervalues():
            for dMon in dYear.itervalues():
                for dDay in dMon.itervalues():
                    iCalMn=dDay['calMn']
                    iNrMn=dDay['limMn']
                    iLmNr=dDay['limPer']
                    fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                    if self.VERBOSE:
                        print '    ','dur',fDur
        self.Set(node,'duration','%06.4f'%fDur)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetSurName(self,node,val):
        pass
    def SetFirstName(self,node,val):
        pass
    
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        if 'Login' in dDef:
            dVal['Login']=self.GetLogin(node)
        if 'SurName' in dDef:
            dVal['SurName']=self.GetSurName(node)
        if 'FirstName' in dDef:
            dVal['FirstName']=self.GetFirstName(node)
        if 'Allocation' in dDef:
            dVal['Allocation']=self.GetAllocation(node)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    def GetAllocation(self,node,**kwargs):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,self.smAttr,'owner')
        nodeUsr=self.doc.GetNode(idOwner)
        dData={}
        self.GetDataDict(node,dData)
        if 'data' in kwargs:
            self.dt.SetDateSmallStr(kwargs['date'])
        else:
            self.dt.Now()
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iDay=self.dt.GetDay()
        if iYear in dData:
            dYear=dData[iYear]
            if iMon in dYear:
                dMon=dYear[iMon]
                dCalMn={}
                lLimMn=[]
                #dPer={}
                #for iDy in xrange(1,31):
                #    iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(nodeUsr,iYear,iMon,iDy)
                #    dPer[iDy]=iLmNr
                #if iLmNr>0:
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                #print dPer
                fAllocRel=((fAlloc/fSum)*100.0)
                return fAllocRel
        return 0.0
    def GetAllocationDate(self,node):
        dData={}
        self.GetDataDict(node,dData)
        lCalMn=[]
        for iYear,dYear in dData.iteritems():
            for iMon,dMon in dYear.iteritems():
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                fAllocRel=((fAlloc/fSum)*100.0)
                lCalMn.append((iYear,iMon,fAllocRel))
        lCalMn.sort()
        d={}
        l=[]
        for iYear,iMon,fAllocRel in lCalMn:
            l.append('%04d-%02d'%(iYear,iMon))
        return l
    def GetAllocationVal(self,node):
        dData={}
        self.GetDataDict(node,dData)
        lCalMn=[]
        for iYear,dYear in dData.iteritems():
            for iMon,dMon in dYear.iteritems():
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                fAllocRel=((fAlloc/fSum)*100.0)
                lCalMn.append((iYear,iMon,fAllocRel))
        lCalMn.sort()
        d={}
        l=[]
        for iYear,iMon,fAllocRel in lCalMn:
            l.append('%4.1f'%fAllocRel)
        return l
    # ---------------------------------------------------------
    # state machine related
    def GetPrjState(self,node):
        return veStateAttr.GetStateAttrState(self,node)
    def _GetPrjState_Old(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            id=self.GetChildAttr(node,self.smAttr,'fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetRolesDict(self,node):
        tmp=self.doc.getParent(node)
        if tmp is None:
            return {}
        tmp=self.doc.getParent(tmp)
        if tmp is None:
            return {}
        tmp=self.doc.getParent(tmp)
        if tmp is None:
            return {}
        obj=self.doc.GetRegisteredNode(self.doc.getTagName(tmp))
        if obj is None:
            return {}
        return obj.GetRolesDict(tmp,self.GetOwner(node))
    def SetPrjState(self,node,val):
        veStateAttr.SetStateAttrState(self,node,val)
    def _SetPrjState_Old(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            c=self.GetChildForced(node,self.smAttr)
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetResourceData(self,node,iYear,iMon,dData):
        if self.VERBOSE:
            vtLog.CallStack('year:%d month:%d'%(iYear,iMon))
            print node
        dtS=self.GetStartDt(node)
        dtE=self.GetEndDt(node)
    
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('go, assigned to project'),None),
            'NoGo':(_('do not go, not assigned to project'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetStr(self.GetStartDt(node))
        except:
            pass
        dtS=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        try:
            self.dt.SetStr(self.GetEndDt(node))
        except:
            pass
        dtE=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        return _(u'project assignment')+' '+_('from:')+dtS+' '+_('to:')+dtE
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,func=None,*args,**kwargs):
        if self.doc is None:
            return
        oLogin=self.doc.GetLogin()
        if oLogin is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
            return
        if oLogin.GetUsrId()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
            return
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        if self.oSMlinker is not None:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
        else:
            oRun=None
        if oRun is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,self.smAttr),self)
            return
        if oRun.GetInitID()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
            return
        if node is not None:
            oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [
            ('Login',vtXmlFilterType.FILTER_TYPE_STRING),
            ('SurName',vtXmlFilterType.FILTER_TYPE_STRING),
            ('FirstName',vtXmlFilterType.FILTER_TYPE_STRING),
            #('Allocation',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        return _(name)
    def IsNotContained(self):
        return True
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd3IDAT8\x8d}\x92\xcbk\x13Q\x18\xc5\x7f3\xed*\x05\x11\x8cJ\x9bi@\
\xe8\xac\x94.\x84\x0c(\xd8\\\xe8B\x8c\xe0\x03|\xadJ]\x97R\xdc\xd4U\xc9\xcc\
\x9f\xe0\x03Z\x08\x85V\\\xf8\xd8\'d\x11\xbd\xa1(\x92\x16DT\x04\'"\xa5D$YX\
\xf0\x81\xb50\x9f\x8bI\xc6\xd8Iz\xe0\xc0\xe5~\xf7\xfc\xee\xe1r1\xcc\x01z\x19\
\xb4(\xa5\x04\xb4\xf4;c\x98\x03\xf4\rk\x8d\x88 Z\xb3/h\xdfp\xb7\xb5F\xd4q\
\x15\x83\xc4\x00\xae\xabDD\xc5\x00\xa2\x11\xf7*1\x80\xc9^\xbd\xd7P\x05Z.\xa0\
\xc2\xbd*\xd0\x8c\x9d\x04`\xb0\xb3\x90\xa0"\x8e\xa3\x01\x17\x9a:\xf4\x11\x15\
B\x9a\xbaw\x1a\xc2\x06\x12Td\xf5\x01\xac\xac\xe6\xff\x9fv@]\xca\x0e\xdf\x10\
\t*\x12\x01:\xe1L&\xdb\xf7\x96H\xdb)\x9e_8\x88\xbex\x87\xb1\xe0\x8c\x00\x98\
\xf3\xf3\x1a\x95\xfd\x17n\x1du\xf1\x9e\xc6\xb3^!E~\xec\x12\x1c:\xc6\xceF\x8d\
\xd3\x84\xcd\xcc\xcd7\xb0Y\x83!\x13\x86\x1203\x93\xe7\xad\xdc\xc2\xb86\x11\
\x81\xbc\x87\'\xc8\x8f\xdf\x04\xeb$/\x1e\xad\xf0\xa7\xd1\xe0W\x1b<\x08\xd0\
\xda\n}x\x14^~X\xe6\xcb\xd7W\xc00\xb558\xbf\x06\x85I\x1b\xac$\x9f\xd7\x9f\
\xd1\xa8\xbf#\xb1\xf7\x11\xa3\xfam\x10\xc0h\xfa\x07\x00\x85\xc9\xcb\x8c\x1c\
\x18\x81\x8f>\xf8>\x00\t\xe0gw\x83^\xb2\xd2\xdfqv\x9c0\xdc\xa5\x04\x84\xf5m\
\x9b{\xb3sb>.\xc3\x93\xb2\x17\x03\xec\xfe\xde\xa5\x96L\xe2m\xac\xc7f\x8b\xb6\
Mnv\x0e\xbf\x9e\xc2\x08\xff?H\xb0 \xd7\xcf\x82\x95\xb6\xa8\xbe^bzj\x1a\x80b\
\xc9\xa1X*\xe2f\x8aL\x01\xb7\xb7\xbf\x91m\x87\xef\xde\xbfbD\x80\x8e$X\x90\
\xdc\xb9\x1c\x00~=E\xfdS\xda\xe85+\x95O\x19\x00\x7f\x01V\xe6\xd7\xed\xe5\x9e\
R\x08\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeProjectAssignEditDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodePrjAssign',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeProjectAssignAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodePrjAssign',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeProjectAssignPanel
        else:
            return None

