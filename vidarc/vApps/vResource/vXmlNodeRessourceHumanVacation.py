#----------------------------------------------------------------------------
# Name:         vXmlNodeVacation.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vXmlNodeRessourceHumanVacation.py,v 1.6 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        #from vNodeXXXPanel import *
        #from vNodeXXXEditDialog import *
        #from vNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')

class vXmlNodeVacation(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'desc',None),
            ('StartDt',None,'startDt',None),
            ('EndDt',None,'endDt',None),
            ('Duration',None,'duration',None),
            ('VacState',None,'vacState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='vacation'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'vacation')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'desc')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'desc',val)
    def GetActionCmdDict(self):
        return {
            'Go':(_('go on vacation'),None),
            'NoGo':(_('do not go on vacation'),None),
            }
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.CallStack('')
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.CallStack('')
        self.Set(node,'result','-1')
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vNodeXXXEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vNodeXXXAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vNodeXXXPanel
        else:
            return None

