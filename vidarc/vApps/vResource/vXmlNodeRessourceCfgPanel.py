#Boa:FramePanel:vXmlNodeRessourceCfgPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceCfgPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080413
# CVS-ID:       $Id: vXmlNodeRessourceCfgPanel.py,v 1.2 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODERESSOURCECFGPANEL, wxID_VXMLNODERESSOURCECFGPANELLBLLIMOVLD, 
 wxID_VXMLNODERESSOURCECFGPANELVILIMOVLD, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeRessourceCfgPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLimOvLd, 0, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viLimOvLd, 0, border=0, flag=0)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vXmlNodeRessourceCfgPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblLimOvLd = wx.StaticText(id=wxID_VXMLNODERESSOURCECFGPANELLBLLIMOVLD,
              label=_(u'limit overload'), name=u'lblLimOvLd', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(162, 22), style=wx.ALIGN_RIGHT)

        self.viLimOvLd = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=1, id=wxID_VXMLNODERESSOURCECFGPANELVILIMOVLD,
              integer_width=4, maximum=100.0, minimum=0.0, name=u'viLimOvLd',
              parent=self, pos=wx.Point(166, 0), size=wx.Size(106, 22),
              style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viLimOvLd.SetTagName('limOvLd')
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viLimOvLd.Clear()
            self.viLimOvLd.SetValue(100.0)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.viLimOvLd.SetNode(self.node,self.doc)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viLimOvLd.GetNode(node,self.doc)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
