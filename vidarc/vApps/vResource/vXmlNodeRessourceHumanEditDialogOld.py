#Boa:Dialog:vXmlNodeRessourceHumanEditDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceHumanEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vXmlNodeRessourceHumanEditDialogOld.py,v 1.1 2006/05/19 10:54:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vXmlNodeRessourceHumanPanel import *

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vXmlNodeRessourceHumanEditDialog(parent)

[wxID_VXMLNODERESSOURCEHUMANEDITDIALOG, 
 wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBAPPLY, 
 wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeRessourceHumanEditDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODERESSOURCEHUMANEDITDIALOG,
              name=u'vXmlNodeRessourceHumanEditDialog', parent=prnt,
              pos=wx.Point(348, 174), size=wx.Size(508, 347),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNode Ressource Human Edit Dialog')
        self.SetClientSize(wx.Size(500, 320))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(128, 288),
              size=wx.Size(96, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(256, 288),
              size=wx.Size(96, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODERESSOURCEHUMANEDITDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnLoc=vXmlNodeRessourceHumanPanel(self,id=wx.NewId(),pos=(0,0),size=(500,280),
                        style=0,name=u'pnHuman')
    def SetRegNode(self,obj):
        self.pnLoc.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnLoc.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnLoc.SetNode(node)
    def GetNode(self):
        self.pnLoc.GetNode()
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
