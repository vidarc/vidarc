#Boa:Dialog:vXmlNodeRessourceEditDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeRessourceEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlNodeRessourceEditDialog.py,v 1.4 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vXmlNodeRessourcePanel import *

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vXmlNodeRessourceEditDialog(parent)

[wxID_VXMLNODERESSOURCEEDITDIALOG, wxID_VXMLNODERESSOURCEEDITDIALOGCBAPPLY, 
 wxID_VXMLNODERESSOURCEEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeRessourceEditDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODERESSOURCEEDITDIALOG,
              name=u'vXmlNodeRessourceEditDialog', parent=prnt,
              pos=wx.Point(348, 174), size=wx.Size(508, 347),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNode Ressource Edit Dialog')
        self.SetClientSize(wx.Size(500, 320))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(128, 288), size=wx.Size(96, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODERESSOURCEEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(256, 288), size=wx.Size(96, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODERESSOURCEEDITDIALOGCBCANCEL)

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnLoc=vXmlNodeRessourcePanel(self,id=wx.NewId(),pos=(0,0),size=(500,280),
                        style=0,name=u'pnLoc')
    def SetRegNode(self,obj):
        self.pnLoc.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnLoc.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnLoc.SetNode(node)
    def GetNode(self):
        self.pnLoc.GetNode()
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
