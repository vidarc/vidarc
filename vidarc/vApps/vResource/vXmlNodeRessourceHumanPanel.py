#Boa:FramePanel:vXmlNodeRessourceHumanPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeHumanRessource.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vXmlNodeRessourceHumanPanel.py,v 1.6 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vHum.vXmlHumInputTreeGrp
import vidarc.vApps.vHum.vXmlHumInputTree
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputAttrValue
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTree
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
from gettext import *
from vidarc.tool.xml.vtXmlTree import *

from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.ext.state.veRoleDialog import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.vApps.vRessource.__config__ as __config__

VERBOSE=0

[wxID_VXMLNODERESSOURCEHUMANPANEL, wxID_VXMLNODERESSOURCEHUMANPANELCBROLE, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLAREA, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLDESC, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLGROUP, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLNAME, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLROLE, 
 wxID_VXMLNODERESSOURCEHUMANPANELLBLTAG, 
 wxID_VXMLNODERESSOURCEHUMANPANELTXTTAG, 
 wxID_VXMLNODERESSOURCEHUMANPANELVIDESC, 
 wxID_VXMLNODERESSOURCEHUMANPANELVIGRP, 
 wxID_VXMLNODERESSOURCEHUMANPANELVINAME, 
 wxID_VXMLNODERESSOURCEHUMANPANELVITRAREA, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vXmlNodeRessourceHumanPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.viName, 3, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsName, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsArea, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.vitrArea, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRole, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbRole, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDesc, 1, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT)
        parent.AddWindow(self.viDesc, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4,
              flag=wx.EXPAND | wx.SHRINK | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.txtTag, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblGroup, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.viGrp, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsDesc_Items(self.bxsDesc)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODERESSOURCEHUMANPANEL,
              name=u'vXmlNodeRessourceHumanPanel', parent=prnt,
              pos=wx.Point(319, 211), size=wx.Size(316, 160),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(308, 133))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              24), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.txtTag = wx.TextCtrl(id=wxID_VXMLNODERESSOURCEHUMANPANELTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(77, 0), size=wx.Size(77,
              24), style=0, value=u'')
        self.txtTag.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLTAG,
              label=_(u'tag'), name=u'lblTag', parent=self, pos=wx.Point(4, 0),
              size=wx.Size(69, 24), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.lblArea = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLAREA,
              label=_(u'area'), name=u'lblArea', parent=self, pos=wx.Point(4,
              103), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.lblGroup = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLGROUP,
              label=_(u'group'), name=u'lblGroup', parent=self,
              pos=wx.Point(158, 0), size=wx.Size(69, 24), style=wx.ALIGN_RIGHT)
        self.lblGroup.SetMinSize(wx.Size(-1, -1))

        self.viGrp = vidarc.vApps.vHum.vXmlHumInputTreeGrp.vXmlHumInputTreeGrp(id=wxID_VXMLNODERESSOURCEHUMANPANELVIGRP,
              name=u'viGrp', parent=self, pos=wx.Point(231, 0), size=wx.Size(77,
              24), style=0)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODERESSOURCEHUMANPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(77, 24),
              size=wx.Size(231, 30), style=0)

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VXMLNODERESSOURCEHUMANPANELVIDESC,
              name=u'viDesc', parent=self, pos=wx.Point(77, 54),
              size=wx.Size(231, 49), style=0)

        self.vitrArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODERESSOURCEHUMANPANELVITRAREA,
              name=u'vitrArea', parent=self, pos=wx.Point(77, 103),
              size=wx.Size(77, 30), style=0)

        self.lblDesc = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLDESC,
              label=_(u'description'), name=u'lblDesc', parent=self,
              pos=wx.Point(4, 54), size=wx.Size(69, 49), style=wx.ALIGN_RIGHT)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.lblRole = wx.StaticText(id=wxID_VXMLNODERESSOURCEHUMANPANELLBLROLE,
              label=_(u'role'), name=u'lblRole', parent=self, pos=wx.Point(158,
              103), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblRole.SetMinSize(wx.Size(-1, -1))

        self.cbRole = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODERESSOURCEHUMANPANELCBROLE,
              bitmap=vtArt.getBitmap(vtArt.Role), label=_(u'role'),
              name=u'cbRole', parent=self, pos=wx.Point(231, 103),
              size=wx.Size(77, 30), style=0)
        self.cbRole.SetMinSize(wx.Size(-1, -1))
        self.cbRole.Bind(wx.EVT_BUTTON, self.OnCbRoleButton,
              id=wxID_VXMLNODERESSOURCEHUMANPANELCBROLE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.txtTag,self.cbRole],
                lEvent=[(self.txtTag,wx.EVT_TEXT)],
                iSecLvEdit=__config__.SEC_LEVEL_LIMIT_ADMIN)
                
        self.dlgRole=veRoleDialog(self)
        self.bRoleShowOnce=False
        self.vitrArea.SetTagNames('mainArea','name')
        self.vitrArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.vitrArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.vitrArea.SetTagNames2Base(['customers','cfg'])
        self.viGrp.SetTagNames('group','name')
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viGrp.SetDocTree(dd['doc'],dd['bNet'])
            self.dlgRole.SetDocHum(dd['doc'],dd['bNet'])
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.viName.IsModified():
            return True
        if self.viDesc.IsModified():
            return True
        if self.vitrArea.IsModified():
            return True
        return False
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viName.Close()
        self.viDesc.Close()
        self.vitrArea.Close()
        self.viGrp.Close()
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.Close()
            self.dRoles={}
            self.txtTag.SetValue('')
            self.viName.Clear()
            self.viDesc.Clear()
            self.vitrArea.Clear()
            self.viGrp.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        self.viName.SetDoc(doc)
        self.viDesc.SetDoc(doc)
        self.vitrArea.SetDoc(doc,bNet)
        self.viGrp.SetDoc(doc)
        self.dlgRole.SetDoc(doc,bNet)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.txtTag.SetValue(self.doc.getNodeText(self.node,'tag'))
            self.viName.SetNode(self.node,'name')
            self.viDesc.SetNode(self.node,'desc')
            self.vitrArea.SetNode(self.node)
            self.viGrp.SetNode(self.node)
            self.dRoles=self.objRegNode.GetRolesDict(self.node)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            self.doc.setNodeText(node,'tag',self.txtTag.GetValue())
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            self.vitrArea.GetNode(node)
            self.viGrp.GetNode(node)
            self.objRegNode.SetRolesDict(node,self.dRoles)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            self.txtTag.Enable(False)
            self.viName.Enable(False)
            self.viDesc.Enable(False)
            self.vitrArea.Enable(False)
            self.viGrp.Enable(False)
            self.cbRole.Enable(False)
        else:
            self.txtTag.Enable(True)
            self.viName.Enable(True)
            self.viDesc.Enable(True)
            self.vitrArea.Enable(True)
            self.viGrp.Enable(True)
            self.cbRole.Enable(True)

    def OnCbRoleButton(self, event):
        try:
            if self.bRoleShowOnce==False:
                self.dlgRole.Centre()
            self.dlgRole.SetRolePossible(self.objRegNode.GetConfigurableRoleTranslationLst())
            self.dlgRole.SetRoles(self.dRoles)
            iRet=self.dlgRole.ShowModal()
            if iRet>0:
                self.dRoles=self.dlgRole.GetRoles()
                self.SetModified(True,self.cbRole)
        except:
            self.__logTB__()
        event.Skip()
