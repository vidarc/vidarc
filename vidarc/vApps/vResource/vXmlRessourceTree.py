#----------------------------------------------------------------------------
# Name:         vXmlRessourceTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlRessourceTree.py,v 1.3 2006/06/07 12:20:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlGrpTreeReg import *
from vidarc.vApps.vRessource.vNetRessource import *
import vidarc.vApps.vRessource.images as images

class vXmlRessourceTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        #self.bGrpMenu=True
        #self.grpMenu=[('normal',_(u'normal')),('address:country',_(u'country')),('address:region',_(u'region'))]
    def SetDftGrouping(self):
        self.grouping=[]
        self.bGrouping=False
        self.label=[('name','')]
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['name','|id','address:country','address:region'])
    def SetGroupingByName(self,sGrp):
        if sGrp=='normal':
            return self.SetNormalGrouping()
        elif sGrp=='address:country':
            return self.SetCountryGrouping()
        elif sGrp=='address:region':
            return self.SetRegionGrouping()
        return False
    def SetNormalGrouping(self):
        if self.iGrp==0:
            return False
        self.iGrp=0
        self.SetGrouping([],[('name','')])
        return True
    def SetCountryGrouping(self):
        if self.iGrp==1:
            return False
        self.iGrp=1
        self.SetGrouping({'customer':[('address:country','')]},[('name','')])
        return True
    def SetRegionGrouping(self):
        if self.iGrp==2:
            return False
        self.iGrp=2
        self.SetGrouping({'customer':[('address:country',''),('address:region','')]},[('name','')])
        return True
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            #self.__addElemImage2ImageList__('root',
            #                images.getRessourcesImage(),
            #                images.getRessourcesImage(),True)
            self.__addElemImage2ImageList__('ressources',
                            images.getPluginImage(),
                            images.getPluginImage(),True)
            
            self.SetImageList(self.imgLstTyp)
