#----------------------------------------------------------------------------
# Name:         vXmlNodeVacation.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vXmlNodeVacation.py,v 1.10 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtDateTimeStartEnd
import vidarc.tool.time.vtTimeLimit as vtTimeLimit
from vidarc.ext.state.veStateAttr import veStateAttr

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeVacationPanel import *
        from vXmlNodeVacationEditDialog import *
        from vXmlNodeVacationAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeVacation(vtXmlNodeBase,veStateAttr):
    VERBOSE=0
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
            ('StartDt',None,'startDt',None),
            ('EndDt',None,'endDt',None),
            ('Duration',None,'duration',None),
            ('VacState',None,'vacState',None),
            ('Result',None,'result',None),
            #('SurName',None,None,None),
            #('FirstName',None,None,None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag']
    FMT_GET_4_TREE=[('Tag','')]
    def __init__(self,tagName='vacation'):
        global _
        _=vtLgBase.assignPluginLang('vRessource')
        vtXmlNodeBase.__init__(self,tagName)
        veStateAttr.__init__(self,'smVacation','vacState')
        self.oSMlinker=None
        self.dt=vtDateTime(True)
        self.dtDiff=vtDateTimeStartEnd(True)
    def GetDescription(self):
        return _(u'vacation')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetDesc(self,node,lang=None):
        return self.GetML(node,'description',lang=lang)
    def GetStartDt(self,node):
        return self.Get(node,'startDt')
    def GetEndDt(self,node):
        return self.Get(node,'endDt')
    def GetDuration(self,node):
        return self.Get(node,'duration')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    def GetLogin(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetName(nodePar)
    def GetSurName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetSurName(nodePar)
    def GetFirstName(self,node):
        nodePar=self.doc.getParent(node)
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetFirstName(nodePar)
    def GetVacType(self,node,lang=None):
        return self.GetML(node,'vacType',lang=lang)
    def GetVacTypeId(self,node):
        #return self.GetChildAttrStr(node,'vacType','fid')
        return self.GetChildForeignKeyStr(node,'vacType','fid')
    def GetVacTypeIdNum(self,node):
        return self.GetChildForeignKey(node,'vacType','fid','vGlobals')
    
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val,lang=None):
        self.SetML(node,'description',val,lang=lang)
    def SetStartDt(self,node,val):
        self.Set(node,'startDt',val)
    def SetEndDt(self,node,val):
        self.Set(node,'endDt',val)
    def SetVacType(self,node,val,lang=None):
        self.SetML(node,'vacType',lang=lang)
    def SetVacTypeId(self,node,id):
        self.SetChildForeignKeyStr(node,'vacType','fid',id,'vGlobals')
    def SetVacTypeIdNum(self,node,id):
        self.SetChildForeignKey(node,'vacType','fid',id,'vGlobals')
    
    def GetDataDict(self,node,dData,bModify=False):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,'vacState','owner')
        nodeUsr=self.doc.GetNode(idOwner)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
            sDt=self.GetStartDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrS=self.dt.GetYear()
            iMhS=self.dt.GetMonth()
            iDyS=self.dt.GetDay()
            iHrS=self.dt.GetHour()
            iMnS=self.dt.GetMinute()
            sDt=self.GetEndDt(node)
            if len(sDt)==0:
                return
            self.dt.SetStr(sDt)
            iYrE=self.dt.GetYear()
            iMhE=self.dt.GetMonth()
            iDyE=self.dt.GetDay()
            iHrE=self.dt.GetHour()
            iMnE=self.dt.GetMinute()
            for iYear in xrange(iYrS,iYrE+1):
                if iYear in dData:
                    dYear=dData[iYear]
                else:
                    dYear={}
                    dData[iYear]=dYear
                if iYear==iYrS:
                    iMhST=iMhS
                else:
                    iMhST=1
                iMhET=12
                if iYear==iYrE:
                    iMhET=iMhE
                for iMon in xrange(iMhST,iMhET+1):
                    if iMon in dYear:
                        dMon=dYear[iMon]
                    else:
                        dMon={}
                        dYear[iMon]=dMon
                    if iYear==iYrS:
                        if iMon>iMhS:
                            iDyST=1
                        else:
                            iDyST=iDyS
                    else:
                        iDyST=1
                    if iYear==iYrE:
                        if iMon<iMhE:
                            iDyET=31
                        else:
                            iDyET=iDyE
                    else:
                        iDyET=31
                    for iDy in xrange(1,32):
                        if self.VERBOSE:
                            print iYear,iMon,iDy
                        iHrNrS,iMnNrS,iHrNrE,iMnNrE,iLmNr=oUsr.GetTimeNormal(nodeUsr,iYear,iMon,iDy)
                        if iLmNr>0:
                            bCalcDiff=True
                            if iYrS==iYear and iMhS==iMon:
                                if iDyS==iDy:
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToLower(iHrS,iMnS,iHrNrS,iMnNrS)
                                    iHrScal,iMnScal,bMod=vtTimeLimit.boundToHigher(iHrScal,iMnScal,iHrNrE,iMnNrE)
                                    #FIXME handle out of limit
                                    if bModify:
                                        sDt=self.GetStartDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrScal)
                                        self.dt.SetMinute(iMnScal)
                                        self.SetStartDt(node,self.dt.GetDateTimeStr())
                                elif iDy<iDyS:
                                    iHrScal,iMnScal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrScal=iHrNrS
                                    iMnScal=iMnNrS
                            else:
                                iHrScal=iHrNrS
                                iMnScal=iMnNrS
                            if iYrE==iYear and iMhE==iMon:
                                if iDyE==iDy:
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToHigher(iHrE,iMnE,iHrNrE,iMnNrE)
                                    iHrEcal,iMnEcal,bMod=vtTimeLimit.boundToLower(iHrEcal,iMnEcal,iHrNrS,iMnNrS)
                                    #FIXME handle out of limit
                                    if bModify:
                                        sDt=self.GetEndDt(node)
                                        self.dt.SetStr(sDt)
                                        self.dt.SetHour(iHrEcal)
                                        self.dt.SetMinute(iMnEcal)
                                        self.SetEndDt(node,self.dt.GetDateTimeStr())
                                elif iDy>iDyE:
                                    iHrEcal,iMnEcal=0,0
                                    bCalcDiff=False
                                else:
                                    iHrEcal=iHrNrE
                                    iMnEcal=iMnNrE
                            else:
                                iHrEcal=iHrNrE
                                iMnEcal=iMnNrE
                            #if (iDy<iDyS) or (iDy>iDyE):
                            #    iHrScal,iMnScal,iHrEcal,iMnEcal=0,0,0,0
                            if iHrNrS>=0 and iHrNrE>=0:
                                self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrNrS,iMnNrS))
                                if iMnNrE==60:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE+1,0))
                                else:
                                    self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrNrE,iMnNrE))
                                self.dtDiff.CalcDiff()
                                iNrMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                if self.VERBOSE:
                                    print self.dtDiff.GetTimeDiffStr(' '),iNrMn
                                
                                if bCalcDiff==True:
                                    if self.VERBOSE:
                                        print '  ',iHrScal,iMnScal,iHrEcal,iMnEcal,self.dtDiff.GetMinuteDiff()
                                    self.dtDiff.SetTimeStr('%02d:%02d:00'%(iHrScal,iMnScal))
                                    if iMnEcal==60:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal+1,0))
                                    else:
                                        self.dtDiff.SetEndTimeStr('%02d%02d00'%(iHrEcal,iMnEcal))
                                    self.dtDiff.CalcDiff()
                                    iCalMn=self.dtDiff.GetHourDiff()*60+self.dtDiff.GetMinuteDiff()
                                else:
                                    iCalMn=0.0
                                if iDy in dMon:
                                    dMon[iDy]['calMn']+=iCalMn
                                    # bound holiday request to limit
                                    if dMon[iDy]['calMn']>dMon[iDy]['limMn']:
                                        dMon[iDy]['calMn']=dMon[iDy]['limMn']
                                else:
                                    dMon[iDy]={'calMn':iCalMn,'limMn':iNrMn,'limPer':iLmNr}
                                #fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                                if self.VERBOSE:
                                    #print '    ','dur',fDur
                                    print '    ','res',dMon[iDy]
                            #    vtTimeLimit.addRange(l,iHrScal,iMnScal,iHrEcal,iMnEcal)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self)
    def SetDuration(self,node,val):
        if self.VERBOSE:
            vtLog.CallStack('')
        dData={}
        self.GetDataDict(node,dData,bModify=True)
        fDur=0.0
        for dYear in dData.itervalues():
            for dMon in dYear.itervalues():
                for dDay in dMon.itervalues():
                    iCalMn=dDay['calMn']
                    iNrMn=dDay['limMn']
                    iLmNr=dDay['limPer']
                    fDur+=(iCalMn/float(iNrMn))*(iLmNr/100.0)
                    if self.VERBOSE:
                        print '    ','dur',fDur
        self.Set(node,'duration','%06.4f'%fDur)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetSurName(self,node,val):
        pass
    def SetFirstName(self,node,val):
        pass
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        if 'Login' in dDef:
            dVal['Login']=self.GetLogin(node)
        if 'SurName' in dDef:
            dVal['SurName']=self.GetSurName(node)
        if 'FirstName' in dDef:
            dVal['FirstName']=self.GetFirstName(node)
        if 'Allocation' in dDef:
            dVal['Allocation']=self.GetAllocation(node)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    def GetAllocation(self,node,**kwargs):
        oUsr=self.doc.GetRegisteredNode('userReferenced')
        idOwner=self.GetChildAttrStr(node,self.smAttr,'owner')
        nodeUsr=self.doc.GetNode(idOwner)
        dData={}
        self.GetDataDict(node,dData)
        if 'data' in kwargs:
            self.dt.SetDateSmallStr(kwargs['date'])
        else:
            self.dt.Now()
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iDay=self.dt.GetDay()
        if iYear in dData:
            dYear=dData[iYear]
            if iMon in dYear:
                dMon=dYear[iMon]
                dCalMn={}
                lLimMn=[]
                fSum=0.0
                iCount=0
                fAlloc=0.0
                for iDy,d in dMon.iteritems():
                    fAqual=d['limPer']/100.0
                    fAlloc+=(d['calMn']/float(d['limMn']))*fAqual
                    iCount+=1
                    fSum+=fAqual
                #print dPer
                fAllocRel=((fAlloc/fSum)*100.0)
                return fAllocRel
        return 0.0
    # ---------------------------------------------------------
    # state machine related
    def GetVacState(self,node):
        return veStateAttr.GetStateAttrState(self,node)
    def GetRolesDict(self,node):
        tmp=self.doc.getParent(node)
        if tmp is None:
            return {}
        tmp=self.doc.getParent(tmp)
        if tmp is None:
            return {}
        tmp=self.doc.getParent(tmp)
        if tmp is None:
            return {}
        obj=self.doc.GetRegisteredNode(self.doc.getTagName(tmp))
        if obj is None:
            return {}
        return obj.GetRolesDict(tmp,self.GetOwner(node))
    def SetVacState(self,node,val):
        veStateAttr.SetStateAttrState(self,node,val)
    def GetResourceData(self,node,iYear,iMon,dData):
        if self.VERBOSE:
            vtLog.CallStack('year:%d month:%d'%(iYear,iMon))
            print node
        dtS=self.GetStartDt(node)
        dtE=self.GetEndDt(node)
    
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('go on vacation'),None),
            'NoGo':(_('do not go on vacation'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetStr(self.GetStartDt(node))
        except:
            pass
        dtS=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()[:5]
        try:
            self.dt.SetStr(self.GetEndDt(node))
        except:
            pass
        dtE=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()[:5]
        return _(u'vacation')+' '+_('from:')+dtS+' '+_('to:')+dtE
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,func=None,*args,**kwargs):
        if self.doc is None:
            return
        oLogin=self.doc.GetLogin()
        if oLogin is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
            return
        if oLogin.GetUsrId()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
            return
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        if self.oSMlinker is not None:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
        else:
            oRun=None
        if oRun is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,self.smAttr),self)
            return
        if oRun.GetInitID()<0:
            vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
            return
        if node is not None:
            oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [
            ('Login',vtXmlFilterType.FILTER_TYPE_STRING),
            ('SurName',vtXmlFilterType.FILTER_TYPE_STRING),
            ('FirstName',vtXmlFilterType.FILTER_TYPE_STRING),
            #('Allocation',vtXmlFilterType.FILTER_TYPE_DATE),
            #('AllocationAllVal',vtXmlFilterType.FILTER_TYPE_DATE),
            #('AllocationAllDate',vtXmlFilterType.FILTER_TYPE_DATE),
            ]
    def GetTranslation(self,name):
        return _(name)
    def IsNotContained(self):
        return True
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x02\xffIDAT(\x91\x05\xc1\xddk[e\x18\x00\xf0\'O\x9e\x9c\x9e\xbc\xf9h\xf3q\
\xd2\x84$\xcd\x0ei\x9b\xc6~\xe8\xd6\xa8c\x94Y\xc2\xec\x85\x1fha"\x08\xb2\x8d\
\xe1\x85\x17\xa2\x85\xea\xc5.\xbc\xf0\xce?\xa0\x14-\xc3\xa1\x14\x99"\xc5!2\
\x8b\x94\xb2\x85Q\xd7\xd2V\xe2\xd6f\xb5\x0b\xa19\xa6[\x9b4Kr^\x8f\xa7\'o\xfd\
\xfdlS\x8f4\xcb\xa0N\xa7`\x12\x1e\x1aV\xd8\x06V\x07)\x16\xe5\xd6g\n\xd5\xfa\
\xc4P\xd6\x9fx\x99\x0b\x11-\xad\x8cy\xca\x00\x80\x8a\x83F\x02\xc4\x08\x15\
\xa2\x98,w:\x99\xb61ws>\x13\x87\xe5lo1\x97\x9b\xba17\xc9\x10\xcd\xb5\xe5]\rK\
Z\x9b\xf4\x13\xa1W6\xf6W?[iCk\xbf\xa1\xc4.2\xc7RJ\xed\x9e/\xf0\x91xF\xab\xdd\
\x89\x87\x80\xefmX#o\xe5\xf3\xf9.f\'\xd7?[-\xed\x8bZ\xb3^\xd8z\xecQ|i\xdb\
\xe2j\xa1Tmr\xc1\xb1\x19\x1e\x9b\xfe\xf8\xd1\xad\xc5/\raB\xf2\xf9\x94W*7,\
\xdb\xcc\x9d\xaffo^S3W\x93\x14,\xc2\xe1v\xfe\xfaXZ\xad\x9a\x02%\x9f\xebYew\
\xaf\xf2\xce\xe4\x8f\xa1\xe8(\x97\x84\x0cT0\xb8\xfd\xfc\xf4t\xfdDy\xf5\xc2T\
\xe4\xd4\x8b\xda\xfa7\xc9\x18.-\xef\x1c\xdc><\x83j-\xd4\xfe\xe4\xf2\xb5o\x7f\
\xfety\xf1vd\xe4M\x9dN\x86\xc1\x85\xb2wp\xe2\xc2G\x8aE;\xc5\xbb\xbcy\xff\xa7\
\xfbk/%\'~\x98\xfa\xd0Q\xae\r\x9d\xfe\xfc\xbb\x99\xd9\xb1s\xd9\xa0\xaf\x90\
\xffc\x1e\x04Hn\x89\x00\x80\x0b\xf1\x04\x05\xf1\x87k\xacq&>\x99}\xff\xeb\xc7\
KsQ\xbf\xdbQ\xac\xbc\x91}\x1b\x8d\xc6\xc4k\x1f\xfc\xc2#\xa5\x7fM\xc0:\xbaLd\
\x88\xdd\x02\xab\xd5\x06<\x90\xaf\\\xba\xe1&\x18\x18\x1e\x8dD\xbb\xfe[\xfc\
\xdejV\x00\xe0\xa8(\x8e+\x00\x00\x03\x82\x11\xc8\xf0\xb4%z\xbc4>\xfe\xde\xc0\
\xd9l\x00\xa5\x90$6;3jT!\x0bZ\x9cz\xd5\x9e\xf9\xdfV\xeb\xe9\xc4\xbb.\xff\xdd\
v\x03\xf7\x8a[Hb\xe1\xa0\xb6\xa6\xfb\xc28\xaa\t\x9e\xe3\xdc\xb59\xdb\xb2\x0c\
\x83\xe1\xe6\xbd\x82ip\x9f\x92\x90O\xf5-\xe85\xc5At\xd1\xfc}\xe9\xde\x8a|\
\xeeR7\xa0[\x16\t\x895L\xf3ho\'\xa6v\x11\x91\x9a\xee\x7f\xda"\xf1B&\xd0\x97\
\xf2\xa3\xd4O\x0c}\x91D\x1f\xd6\x0b:o"\xf4\xcb^\'\xd0_\xb9[j\\FGG\xea\xfc\
\xb8:\xd4\xc7\xb9=\x00^Fh\x80\xa5\x99\xa6}0\xdd\xbb\xfd\xf7~\xe8\xec\xeb\xcd\
\x93\xe3J\xdb\xd4\xdbb(`\x04\xb1\xe5\x8b$\xc8)\xe7\xfe\xac.\xe4vm\xe9\xc1]r3\
\t\x93r\x07\x95k\xbe\x8a7\xa2\x80\x15\x94(`J\xc8D\xb3d^\xff\xb5\xac\xeb\x0f<\
\x12\xeb\xf6xN\'\x07[AeTf=N\xf9\xc0\xb2(\xda\xfb\xdc\xb3X\x98["\xce\xa4\xf5c\
\xfe\n\xb0\x87\xe1x"5\xac\xbaQ+\x1e\xf9#\x81\x1d%~\x88,e\x87m\x93\x03\xc0\
\xff\teT\xf6\x13\x12\x8f\x04\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeVacationEditDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodeVacation',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeVacationAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnNodeVacation',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeVacationPanel
        else:
            return None

