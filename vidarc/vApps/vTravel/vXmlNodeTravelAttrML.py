#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelAttrML.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071126
# CVS-ID:       $Id: vXmlNodeTravelAttrML.py,v 1.1 2007/12/15 12:23:16 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import __config__
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    #if __config__.NODE_GUI:
    if vcCust.is2Import(__name__):
        from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeTravelAttrML(vtXmlNodeAttrML):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='travelAttr',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'travel attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def IsRequired(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xeeIDAT8\x8d\xb5\x93An\x83@\x0cE\x9fgz\x91\\#R\x0e\x90\xe4(!9\x0c\
\x98SDY\xa2*\xcb\n\x95ez\x04\x96\x9c\x02\xdc\x05\x1d\xc4@\xa0R\xa3~\xc9\xd2\
\xd8c\x7f\xfd\xf1\xd7\x888\xcf+x\x03\xb0\xae\xb5\xbf\x0c\x8b\xf3\xe2BbV\x0c\
\xa1\xa9\x0eM\x9ajt\x17"R0F\x9e\xd5$\x97\x84\xaa\xac\x00\xd8\xee\xb6\x80r:o\
\x16e\xf4\x02\xac0\xb3\xc2\x00\xab\xca\xca\x00\x03l|\x1fj\xe1,\xce\xe3\x9e\
\xd3\xf68\xec\x0fQ>\x95\x0f\xcc\t4\xd5\x1f\xd9\xf0~\xbf\x8bu\xad\x8dw2\xc5\
\x8c\xe0t\xde\x0cK\x0c\xc3\x8b\xef\x07D\x9c\xc7\xba\xd6\xa6\xd2~\x83\xc81\
\xb6q\x8c<\xab\xfb\x069\x92g\xf5*\xd1\xff\xd9(\xce\xd34\x8di\xaa\x91uLl\x9c)\
\x08\xf8\xfc(\xedv\xbdE\xb5g{Z\xb4\xf1\xf1\xf5\x00 \xb9$\xac\xd98\xb8\xb0\
\xd8\xb1\x02q^\xe4\xd5\xef\xfc\r\x8fB\x9f\x10\xf9\xdb\xd4\xbc\x00\x00\x00\
\x00IEND\xaeB`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtInputAttrCfgMLPanel
        else:
            return None

