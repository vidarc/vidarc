#Boa:FramePanel:vXmlNodeTravelPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelPanel.py,v 1.7 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputUnique
import vidarc.tool.input.vtInputMultipleTree
import vidarc.tool.input.vtInputDistributeTree
import vidarc.vApps.vPrj.vXmlPrjInputTreePrjsPercent
import vidarc.vApps.vContact.vXmlContactInputTree
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputMultipledValues
import vidarc.vApps.vPrj.vXmlPrjInputTree
import vidarc.ext.state.veStateInput
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeInputDateTime
import vidarc.vApps.vHum.vXmlHumInputTreeUsr

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODETRAVELPANEL, wxID_VXMLNODETRAVELPANELLBLCURRENCY, 
 wxID_VXMLNODETRAVELPANELLBLENDDTTM, wxID_VXMLNODETRAVELPANELLBLID, 
 wxID_VXMLNODETRAVELPANELLBLSTARTDTTM, wxID_VXMLNODETRAVELPANELLBLPRJ, 
 wxID_VXMLNODETRAVELPANELLBLSTATE, wxID_VXMLNODETRAVELPANELLBLSUM, 
 wxID_VXMLNODETRAVELPANELLBLVIS, wxID_VXMLNODETRAVELPANELLBLVISADR, 
 wxID_VXMLNODETRAVELPANELLBLVISCONTACT, wxID_VXMLNODETRAVELPANELLBLUSR, 
 wxID_VXMLNODETRAVELPANELTXTSUM, wxID_VXMLNODETRAVELPANELVICONTACT, 
 wxID_VXMLNODETRAVELPANELVICURRENCY, wxID_VXMLNODETRAVELPANELVIENDDTTM, 
 wxID_VXMLNODETRAVELPANELVISTARTDTTM, wxID_VXMLNODETRAVELPANELVITRAVELLING, 
 wxID_VXMLNODETRAVELPANELVIPRJ, wxID_VXMLNODETRAVELPANELVITRVID, 
 wxID_VXMLNODETRAVELPANELVISTATE, wxID_VXMLNODETRAVELPANELVIVISDEP, 
 wxID_VXMLNODETRAVELPANELVIVISITED, 
] = [wx.NewId() for _init_ctrls in range(23)]

class vXmlNodeTravelPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsNum_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblId, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTrvId, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsState_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblState, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viState, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTravelling, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblStartDtTm, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viStartDtTm, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblEndDtTm, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viEndDtTm, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsPrj_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrj, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viPrj, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsVis_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblVis, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viVisited, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblVisAdr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viVisDep, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblVisContact, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viContact, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrj, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsUsr, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsVis, 0, border=8, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsCurrency, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsNum, 0, border=4, flag=wx.EXPAND | wx.BOTTOM)
        parent.AddSizer(self.bxsState, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsCurrency_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSum, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtSum, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCurrency, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viCurrency, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=8, vgap=0)

        self.bxsUsr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsVis = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsState = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPrj = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCurrency = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsNum = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsUsr_Items(self.bxsUsr)
        self._init_coll_bxsVis_Items(self.bxsVis)
        self._init_coll_bxsState_Items(self.bxsState)
        self._init_coll_bxsPrj_Items(self.bxsPrj)
        self._init_coll_bxsCurrency_Items(self.bxsCurrency)
        self._init_coll_bxsNum_Items(self.bxsNum)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vXmlNodeTravelPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(456, 287),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(448, 260))
        self.SetAutoLayout(True)

        self.lblPrj = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLPRJ,
              label=_(u'project'), name=u'lblPrj', parent=self, pos=wx.Point(0,
              0), size=wx.Size(52, 110), style=wx.ALIGN_RIGHT)

        self.viPrj = vidarc.vApps.vPrj.vXmlPrjInputTreePrjsPercent.vXmlPrjInputTreePrjsPercent(id=wxID_VXMLNODETRAVELPANELVIPRJ,
              name=u'viPrj', parent=self, pos=wx.Point(56, 0), size=wx.Size(392,
              110), style=0)

        self.lblUsr = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLUSR,
              label=_(u'traveler'), name=u'lblUsr', parent=self,
              pos=wx.Point(0, 114), size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize((-1,-1))

        self.viTravelling = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODETRAVELPANELVITRAVELLING,
              name=u'viTravelling', parent=self, pos=wx.Point(56, 114),
              size=wx.Size(56, 24), style=0)

        self.lblVis = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLVIS,
              label=_(u'visited'), name=u'lblVis', parent=self, pos=wx.Point(0,
              146), size=wx.Size(52, 26), style=wx.ALIGN_RIGHT)
        self.lblVis.SetMinSize((-1,-1))

        self.lblVisAdr = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLVISADR,
              label=_(u'address'), name=u'lblVisAdr', parent=self,
              pos=wx.Point(112, 146), size=wx.Size(52, 26),
              style=wx.ALIGN_RIGHT)

        self.viVisited = vidarc.vApps.vContact.vXmlContactInputTree.vXmlContactInputTree(id=wxID_VXMLNODETRAVELPANELVIVISITED,
              name=u'viVisited', parent=self, pos=wx.Point(56, 146),
              size=wx.Size(56, 26), style=0)
        self.viVisited.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViVisitedVtinputTreeChanged,
              id=wxID_VXMLNODETRAVELPANELVIVISITED)

        self.viVisDep = vidarc.vApps.vContact.vXmlContactInputTree.vXmlContactInputTree(id=wxID_VXMLNODETRAVELPANELVIVISDEP,
              parent=self, pos=wx.Point(168, 146), size=wx.Size(112, 26),
              style=0)
        self.viVisDep.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViVisDepVtinputTreeChanged,
              id=wxID_VXMLNODETRAVELPANELVIVISDEP)

        self.viContact = vidarc.vApps.vContact.vXmlContactInputTree.vXmlContactInputTree(id=wxID_VXMLNODETRAVELPANELVICONTACT,
              name=u'viContact', parent=self, pos=wx.Point(336, 146),
              size=wx.Size(112, 26), style=0)

        self.lblVisContact = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLVISCONTACT,
              label=_(u'contact'), name=u'lblSupContact', parent=self,
              pos=wx.Point(280, 146), size=wx.Size(52, 26),
              style=wx.ALIGN_RIGHT)

        self.lblStartDtTm = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLSTARTDTTM,
              label=_(u'start date'), name=u'lblStartDt', parent=self,
              pos=wx.Point(112, 114), size=wx.Size(52, 24),
              style=wx.ALIGN_RIGHT)
        self.lblStartDtTm.SetMinSize((-1,-1))

        self.viStartDtTm = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODETRAVELPANELVISTARTDTTM,
              name=u'viStartDtTm', parent=self, pos=wx.Point(168, 114),
              size=wx.Size(112, 24), style=0)

        self.lblEndDtTm = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLENDDTTM,
              label=_(u'end date'), name=u'lblEndDtTm', parent=self,
              pos=wx.Point(280, 114), size=wx.Size(52, 24),
              style=wx.ALIGN_RIGHT)
        self.lblEndDtTm.SetMinSize((-1,-1))

        self.viEndDtTm = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODETRAVELPANELVIENDDTTM,
              name=u'viEndDtTm', parent=self, pos=wx.Point(336, 114),
              size=wx.Size(112, 24), style=0)

        self.lblCurrency = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLCURRENCY,
              label=_(u'currency'), name=u'lblCurrency', parent=self,
              pos=wx.Point(280, 176), size=wx.Size(52, 21),
              style=wx.ALIGN_RIGHT)
        self.lblCurrency.SetMinSize(wx.Size(-1, -1))

        self.viCurrency = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODETRAVELPANELVICURRENCY,
              name=u'viCurrency', parent=self, pos=wx.Point(336, 176),
              size=wx.Size(112, 21), style=0)

        self.lblId = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLID,
              label=_(u'travel nr'), name=u'lblId', parent=self, pos=wx.Point(0,
              209), size=wx.Size(52, 21), style=wx.ALIGN_RIGHT)

        self.viTrvId = vidarc.tool.input.vtInputUnique.vtInputUnique(id=wxID_VXMLNODETRAVELPANELVITRVID,
              name=u'viTrvId', parent=self, pos=wx.Point(56, 209),
              size=wx.Size(392, 21), style=0)

        self.lblState = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLSTATE,
              label=_(u'state'), name=u'lblState', parent=self, pos=wx.Point(0,
              238), size=wx.Size(52, 22), style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize((-1,-1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VXMLNODETRAVELPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(56, 238),
              size=wx.Size(392, 22), style=0)

        self.lblSum = wx.StaticText(id=wxID_VXMLNODETRAVELPANELLBLSUM,
              label=_(u'sum'), name=u'lblSum', parent=self, pos=wx.Point(0,
              176), size=wx.Size(52, 21), style=wx.ALIGN_RIGHT)

        self.txtSum = wx.TextCtrl(id=wxID_VXMLNODETRAVELPANELTXTSUM,
              name=u'txtSum', parent=self, pos=wx.Point(56, 176),
              size=wx.Size(224, 21), style=wx.TE_READONLY | wx.TE_RIGHT,
              value=u'')
        self.txtSum.Enable(False)
        self.txtSum.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        self._init_ctrls(parent, id)
        self.bSupFirst=False
        
        vtXmlNodePanel.__init__(self,applName=_(u'vTravel')+' '+_(u'travel'),
                lWidgets=[],
                lEvent=[])
        self.viTravelling.SetTagNames('traveler','name')
        self.viVisited.SetTagNames('visited','tag')
        self.viVisDep.SetTagNames('visitedDepart','tag')
        self.viContact.SetTagNames('contact','tag')
        self.viStartDtTm.SetTagName('startDtTm')
        self.viEndDtTm.SetTagName('endDtTm')
        self.viCurrency.SetTagName('currency')
        self.viState.SetNodeAttr('travel','trvState')
        self.viTrvId.SetTagName('travelID')
        #self.viSupAdr.EnableEdit(False)
        #self.viSupContact.EnableEdit(False)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.bSupFirst=False
            self.viPrj.Clear()
            self.viTravelling.Clear()
            self.viVisited.Clear()
            self.viVisDep.Clear()
            self.viContact.Clear()
            #self.viSupAdr.Clear()
            #self.viSupContact.Clear()
            self.viStartDtTm.Clear()
            self.viEndDtTm.Clear()
            self.viTrvId.Clear()
            try:
                docGlb=self.doc.GetNetDoc('vGlobals')
                sCur=docGlb.GetGlobal(['vTravel','travel'],'globalsTravel','currency')
                self.viCurrency.SetValue(sCur)
            except:
                self.viCurrency.SetValue('')
            self.viCurrency.SetModified(False)
            self.viState.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viTravelling.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vContact'):
            dd=d['vContact']
            self.viVisited.SetDocTree(dd['doc'],dd['bNet'])
            self.viVisDep.SetDocTree(dd['doc'],dd['bNet'])
            #self.viSupAdr.SetDoc(dd['doc'])
            self.viContact.SetDocTree(dd['doc'],dd['bNet'])
            #self.viSupContact.SetDoc(dd['doc'])
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.viPrj.SetDocTree(dd['doc'],dd['bNet'])
        
    def SetDependentItemsPanel(self,pn):
        pn.BindItemsChanged(self.OnItemsChanged)
    def OnItemsChanged(self,evt):
        evt.Skip()
        self.viState.SetNode(self.node)
        self.viState.Enable(False)
        pn=self.GetParent().GetPanelByName('travelItems')
        #print pn.GetSum()
        self.txtSum.SetValue(pn.GetSum())
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            t=(self.viPrj.IsModified(),self.viTravelling.IsModified(),
                    self.viVisited.IsModified(),
                    self.viVisDep.IsModified(),
                    self.viStartDtTm.IsModified(),
                    self.viEndDtTm.IsModified(),
                    self.viCurrency.IsModified(),
                    self.viTrvId.IsModified())
            vtLog.vtLngCurWX(vtLog.DEBUG,'mod viPrj:  %d;viTravelling:%d;viVisited:%d;viVisDep:%d;viStartDtTm:%d;'\
                        'viEndDtTm:%d;viCurrency:%d;viTrvId:%d'%t,self)
        if self.viPrj.IsModified():
            return True
        if self.viTravelling.IsModified():
            return True
        if self.viVisited.IsModified():
            return True
        if self.viVisDep.IsModified():
            return True
        if self.viStartDtTm.IsModified():
            return True
        if self.viEndDtTm.IsModified():
            return True
        if self.viCurrency.IsModified():
            return True
        if self.viTrvId.IsModified():
            return True
        #if self.viState.IsModified():
        #    return True
        return False
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viPrj.SetDoc(self.doc)
        self.viTravelling.SetDoc(self.doc)
        self.viVisited.SetDoc(self.doc)
        self.viVisDep.SetDoc(self.doc)
        self.viContact.SetDoc(self.doc)
        #self.viSupAdr.SetDoc(self.doc)
        #self.viSupContact.SetDoc(dd['doc'])
        self.viStartDtTm.SetDoc(self.doc)
        self.viEndDtTm.SetDoc(self.doc)
        self.viCurrency.SetDoc(self.doc)
        self.viTrvId.SetDoc(self.doc)
        self.viTrvId.SetUniqueFunc(self.doc.GetSortedTravelNrIds)
        self.viState.SetDoc(self.doc)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.bSupFirst=True
            self.viPrj.SetNode(self.node)
            self.viTravelling.SetNode(self.node)
            self.viVisited.SetNode(self.node)
            suppFid=self.objRegNode.GetVisited(self.node)
            id,appl=self.doc.convForeign(suppFid)
            netSup,nodeSup=self.doc.GetNode(suppFid)
            self.viVisDep.SetNodeTree(nodeSup)
            self.viVisDep.SetNode(self.node)
            self.viContact.SetNodeTree(nodeSup)
            self.viContact.SetNode(self.node)
            self.viStartDtTm.SetNode(self.node)
            self.viEndDtTm.SetNode(self.node)
            self.viCurrency.SetNode(self.node)
            self.viTrvId.SetNode(self.node)
            self.viState.SetNode(self.node)
            self.txtSum.SetValue(self.objRegNode.GetSum(self.node))
            
            sCur=self.viCurrency.GetValue()
            if len(sCur)==0:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCur=docGlb.GetGlobal(['vTravel','travel'],'globalsTravel','currency')
                    self.viCurrency.SetValue(sCur)
                    #self.viCurrency.__markModified__()
                    self.viCurrency.SetModified(True)
                except:
                    pass
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viPrj.GetNode(node)
            self.viTravelling.GetNode(node)
            self.viVisited.GetNode(node)
            self.viVisDep.GetNode(node)
            self.viContact.GetNode(node)
            #self.objRegNode.SetVisitedAddress(node,self.viSupAdr.GetValue())
            #self.objRegNode.SetVisitedContact(node,self.viContact.GetForeignID())
            #self.objRegNode.SetVisitedContact(node,self.viSupContact.GetValue())
            #self.viContact.__markModified__(False)
            
            self.viStartDtTm.GetNode(node)
            self.viEndDtTm.GetNode(node)
            self.viCurrency.GetNode(node)
            self.viState.GetNode(node)
            self.viTrvId.GetNode(node)
            self.viState.Enable(True)
            self.viState.SetNode(node)      # Refresh
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viPrj.Close()
        self.viTravelling.Close()
        self.viVisited.Close()
        self.viVisDep.Close()
        self.viContact.Close()
        self.viStartDtTm.Close()
        self.viEndDtTm.Close()
        self.viState.Close()
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viPrj.Enable(False)
            self.viTravelling.Enable(False)
            self.viVisited.Enable(False)
            self.viVisDep.Enable(False)
            self.viContact.Enable(False)
            self.viStartDtTm.Enable(False)
            self.viEndDtTm.Enable(False)
            self.viCurrency.Enable(False)
            self.viTrvId.Enable(False)
            self.viState.Enable(False)
        else:
            # add code here
            self.viPrj.Enable(True)
            self.viTravelling.Enable(True)
            self.viVisited.Enable(True)
            self.viVisDep.Enable(True)
            self.viContact.Enable(True)
            self.viStartDtTm.Enable(True)
            self.viEndDtTm.Enable(True)
            self.viCurrency.Enable(True)
            self.viTrvId.Enable(True)
            self.viState.Enable(True)
    
    def __refreshVisited__(self,fid):
        self.viVisDep.Clear()
        self.viContact.Clear()
        netSup,nodeSup=self.doc.GetNode(fid)
        if nodeSup is not None:
            self.viVisDep.SetNodeTree(nodeSup)
            self.viContact.SetNodeTree(nodeSup)
            #self.viCustomer.Enable(True)
    def OnViVisitedVtinputTreeChanged(self, event):
        event.Skip()
        try:
            #fid='%s@vContact'%event.GetId()
            fid='@'.join([event.GetId(),'vContact'])
            suppFid=self.objRegNode.GetVisited(self.node)
            if fid!=suppFid:
                self.__refreshVisited__(fid)
            return
            netDoc,nodeSup=self.doc.GetNode('%s@vContact'%fid)
            self.viContact.SetNodeTree(nodeSup)
            if self.bSupFirst:
                sSupAdr=self.objRegNode.GetVisitedAddress(self.node)
                sSupCont=self.objRegNode.GetVisitedContact(self.node)
            else:
                sSupAdr=None
                sSupCont=None
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s;blocked:%d;first:%d;adr:%s;contact:%s'%(fid,
                        self.IsBlocked(),self.bSupFirst,sSupAdr,sSupCont),self)
            self.viSupAdr.SetNode(netDoc.getChild(nodeSup,'addresses'),sSupAdr)
            if sSupCont is not None:
                id,appl=self.doc.convForeign(sSupCont)
                self.viContact.SetValueID('',id)
            #self.viSupContact.SetNode(netDoc.getChild(nodeSup,'contacts'),sSupCont)
            
            self.bSupFirst=False
        except:
            self.__logTB__()
    def OnViVisDepVtinputTreeChanged(self, event):
        event.Skip()
        try:
            sId=event.GetId()
            if len(sId)>0:
                fid='@'.join([sId,'vContact'])
                if fid!=self.objRegNode.GetVisitedDepartment(self.node):
                    self.viContact.Clear()
                    netSupDep,nodeSupDep=self.doc.GetNode(fid)
                    if nodeSupDep is not None:
                        self.viContact.SetNodeTree(nodeSupDep)
            else:
                self.viContact.Clear()
        except:
            self.__logTB__()
