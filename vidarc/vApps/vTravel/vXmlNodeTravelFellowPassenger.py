#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelFellowPassenger.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelFellowPassenger.py,v 1.2 2008/02/02 16:10:55 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeTravelFellowPassengerPanel import *
        #from vXmlNodeTravelFellowPassengerEditDialog import *
        #from vXmlNodeTravelFellowPassengerAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeTravelFellowPassenger(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Sum',None,'sum',None),
            #('Count',None,'Count',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='travelFellowPassenger'):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'travel fellow passenger')
    # ---------------------------------------------------------
    # specific
    def GetSetup(self):
        return [
            {'tag':'surname'    ,'trans':'surname','label':_('surname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.4},
            {'tag':'firstname'  ,'trans':'firstname','label':_('firstname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.4},
            ]
    def GetLaTexTableFmt(self):
        return '{|@{\\extracolsep\\fill}r|p{0.4\\linewidth}|p{0.4\\linewidth}|}'
    def GetValues(self,node):
        try:
            lVal=[]
            lSum=None
            if node is None:
                return lVal,lSum
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                for cRow in self.doc.getChilds(nodeData,'row'):
                    lCol=['' for i in lMap]
                    for cCol in self.doc.getChilds(cRow):
                        sTag=self.doc.getTagName(cCol)
                        if sTag in lMap:
                            iNum=lMap.index(sTag)
                            lCol[iNum]=self.doc.getText(cCol)
                    lVal.append(lCol)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return lVal,lSum
    def SetValues(self,node,lVal,lSum):
        if node is None:
            return
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                self.doc.deleteNode(nodeData,node)
            nodeData=self.doc.getChildForced(node,'data')
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            for lRow in lVal:
                if len(lRow[0])==0:
                    continue
                nodeRow=self.doc.createSubNode(nodeData,'row')
                for sTag,sVal in zip(lMap,lRow):
                    self.doc.createSubNodeText(nodeRow,sTag,unicode(sVal))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        if name=='sum':
            return _(u'sum')
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xdeIDAT8\x8d\x85\x91?hSQ\x14\xc6\x7f\xef\xe5\x91\xa6\x92'\x08\r\x9a\
`\x90\x08\xf9g'\xc5 \x08b\x17\xc3SIC\x88\xab\x9b\x18\xcd\x96=C;9\x88\x938(\
\xee.\xba\xd8v\xb55A(>\xd3-&7(4b\xf2\x14C@\xfa\x94\x0e/\\\x87\xa4\xadm^\xd2\
\x0f\x0e\x9c\xfb\xdds\xbes\xeewQT\x0f\x8a\xeaa\xd67+K\xa5\x924n\x1aR\xd34y\
\x1c\xbf\x17\xfbI\xb9\\\x96RJYy_\x91\xd1xT*\xaa\x07\x9f\xcf's\xb9\xdc\x18\
\xff\x7f\xa8\x8c`~4\xa9V\xaa\xd4\xebu\x1c\xc7\xc1H\xa7e{\xbb\x8da\x18d\xb2\
\x99}\xfe(\xb4\xbd$u%C\xa1X\xc0q\x1cz?w\x89\xde\x8a\xe2\x9d\xf1\x129\x17A\
\x08\x81\x10\x02\xef\xd7\x1e\x07#\x87\xf0(\x8aJ\xe6\xf6=\xf9\xf0A\x91\xf0\
\xd9$\xe7#if\xbc:\xbb\xfdw\xb4\xb6\xfbl\x9a\x9b\xe8~\x9d\xa7\xa7np'~\x15\x90\
K\xb5\xfe\xb7\xe5\xb1\r\xb6j\x16\x9a\x1af1\x13dc5\xcb\xa3gox]\xcc\x13\x06\
\xee\x06\xe1m\xc2Gj\xfd$\x96\x10\x87\xb6\xd08\x82\xad\x9a\xc5\xc2e\x98\xf7\
\xe6\x99_]\x02.\xc2\xca2\x1b\xcf_\x01\x85\xc9\x1e\xb8\xe2\xc7\xd2\xc8\xe1\
\xc9%\xea\xe4+w\x08\xecq\x81\x95\xb5\x97\n\x80i\x9at\xbb]\xd7F{\x07j\x96\xa0\
\xa5\xfeU\xa6n\xd0\xe9t\xa85\x83\xc3&1\xe4\xbe\x08\xd8\xd1\x17\tu\x7f\x8f\t\
\xbbz\xf0\xa9\x19\x04,\xec&\xd0\x01\xbf\x0e\xfcq\x7f\xd2\xb1\x1e\xf8u8\x13:8\
'\x921\x99H\xc6\xe4\x98\xc0\xe3'\xf7\xb1mk\xaa\x98\xc0&\x10\x08\x90\xbc\x90$\
\x97\xcfJ\x00EQ=\xae\xc5\xeb/\x062\xfe}4\xdd\x84\xd4Z\x9c\x13\xb1\xd3\xcc\
\x05\xe6h|n\xd0l\xb4\x94\xa9\x02\x00\xd7/\r\xe4B\x08\xea\xed\x00\x83\xd85z\
\xbfzT+\x1f\x0e\xfd\xc2?\xb2#\xb7\xa3}\xc3\xb6\xb4\x00\x00\x00\x00IEND\xaeB`\
\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            return vXmlNodeTravelFellowPassengerEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            return vXmlNodeTravelFellowPassengerAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelFellowPassengerPanel
        else:
            return None
