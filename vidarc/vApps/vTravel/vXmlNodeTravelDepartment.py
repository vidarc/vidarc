#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelDepartment.py,v 1.3 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vTravel.vXmlNodeTravelDepartmentPanel import *
        #from vidarc.vApps.vTravel.vXmlNodeTravelDepartmentEditDialog import *
        #from vidarc.vApps.vTravel.vXmlNodeTravelDepartmentAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeTravelDepartment(vtXmlNodeTag):
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='travelDepartment'):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'travel department')
    # ---------------------------------------------------------
    # specific
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smTravel')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x83IDAT(\x91c<p\xe0\x00\x03)\x80\x85\x81\x81\x81$=,\x10J\xdd\xcf\x82\
\xa0R\xc9O\x1c\x07\x0e\x1c`\x81\xf3\xdb\xf7\xce\xc7TT\xe9\x9c\x88&\xc2\x84\
\xdfTLSpj\xd8\x1e\xb5\x12\xab\x1e,\x1a\xb6G\xad\x84\xa8\xc6\xaa\x07\x8b\x06\
\xcfe\xe1x\x1c\x89\xddI\x9e\xcb\xc2qi\xc3\xe7i\xacz\x08\x84\x12&@\xc4\x03r\
\x90\xa3\x85\x0c\xb2\x14v\x1b\x90U\xa0\xc5\x1d\x0b\x86b\xec\xea\xd05H~\xe2\
\xc0\xa5\x13\xbb\x06\xe2\x13,\x00\x8c\x1c(\x1eErT\xcc\x00\x00\x00\x00IEND\
\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClassOld(self):
        if GUI:
            #return vXmlNodeTravelDepartmentEditDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnTravelDepartment',
                }
        else:
            return None
    def GetAddDialogClassOld(self):
        if GUI:
            #return vXmlNodeTravelDepartmentAddDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnTravelDepartment',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelDepartmentPanel
        else:
            return None

