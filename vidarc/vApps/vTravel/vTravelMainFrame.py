#Boa:Frame:vTravelMainFrame
#----------------------------------------------------------------------------
# Name:         vTravelMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vTravelMainFrame.py,v 1.1 2007/11/22 08:53:09 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    
    import vidarc.vApps.vTravel.images as imgTravel
    from vidarc.vApps.vTravel.vTravelMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgTravel.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgTravel.getApplicationBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vXXX'):
    return vTravelMainFrame(parent,id,pos,size,style,name)

[wxID_VTRAVELMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vTravelMainFrame(wx.Frame,vMDIFrame,vCfg):
    STATUS_CLK_POS=4
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTRAVELMAINFRAME,
              name=u'vTravelMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vTravel')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Travel',iNotifyTime=250)
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':800,'height':540})
        
        try:
            self.pn=vTravelMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vTravelPlugIn')
            self.pn.OpenCfgFile('vTravelCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            EVT_NET_XML_SYNCH_START(self.netDocMain,self.OnSynchStart)
            EVT_NET_XML_SYNCH_PROC(self.netDocMain,self.OnSynchProc)
            EVT_NET_XML_SYNCH_FINISHED(self.netDocMain,self.OnSynchFinished)
            EVT_NET_XML_SYNCH_ABORTED(self.netDocMain,self.OnSynchAborted)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
            
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools , self.mnAnalyse)
            
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        self.dCfg.update(self._getCfgData(['vTravelGui']))
        self.pn.SetCfgData(self._setCfgData,['vTravelGui'],self.dCfg)
        self.__setCfg__()
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def OpenCfgFile(self,fn=None):
        self.pn.OpenCfgFile(fn)
    def __setCfg__(self):
        #sys.stderr.write('__setCfg__\n')
        try:
            if 0:
                node=self.docCfg.getChildForced(None,'size')
                iX=self.docCfg.GetValue(node,'x',int,20)
                iY=self.docCfg.GetValue(node,'y',int,20)
                iWidth=self.docCfg.GetValue(node,'width',int,600)
                iHeight=self.docCfg.GetValue(node,'height',int,400)
            else:
                iX=self.dCfg['x']
                iY=self.dCfg['y']
                iWidth=self.dCfg['width']
                iHeight=self.dCfg['height']
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            #self.dCfg.update({'x':iX,'y':iY,'width':iWidth,'height':iHeight})
            
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def OnMainClose(self,event):
        if self.pn.xdCfg is not None:
            self._setCfgData(['vTravelGui'],self.dCfg)
        vMDIFrame.OnMainClose(self,event)
