#----------------------------------------------------------------------------
# Name:         vXmlTravelTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlTravelTree.py,v 1.1 2007/11/22 08:53:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vTravel.vNetTravel import *

import vidarc.vApps.vTravel.images as imgTravel

class vXmlTravelTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','|id'])
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgTravel.getPluginImage(),
                            imgTravel.getPluginImage(),True)
            self.__addElemImage2ImageList__('travels',
                            imgTravel.getPluginImage(),
                            imgTravel.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)
