#----------------------------------------------------------------------------
# Name:         setup_inno.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: __setup__.py,v 1.1 2007/11/22 12:36:40 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys
from __init__ import *
from __config__ import *
from vidarc.build import *

version = '%s.%s.%s'%(VER_MAJOR,VER_MINOR,VER_RELEASE)
setup_args = {
    'name': DESCRIPTION,
    'version': version,
    'description': "%s %s " % (DESCRIPTION,version),
    'author': AUTHOR,
    'author_email': AUTHOR_EMAIL,
    'maintainer': MAINTAINER,
    'maintainer_email': MAINTAINER_EMAIL,
    'url': URL,
    'license': LICENSE,
    'long_description': LONG_DESCRIPTION,
    'zipfile':'vidarc.vApps.vTravel.%s.zip'%version,
    'vidarcfile':'vidarc.vApps.vTravel.%s.vidarc'%version,
    'releasefile':'vidarc.vApps.vTravel.%s.zip'%version,
    'options':{'vidarc':{'pubkey':'../../install/step08/default.pub',
                        'licencedir':'../../../licences',
                        'mid':'test phrase'},
        },
    'packages': [
#        "mod01",
        ],
    'cmdclass': {
        'vidarc': build_vidarc_plugin,
        },
    }


if __name__ == '__main__':
    print sys.argv
    setup(**setup_args)
