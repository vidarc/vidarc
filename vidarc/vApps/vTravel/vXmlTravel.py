#----------------------------------------------------------------------------
# Name:         vXmlTravel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlTravel.py,v 1.3 2008/01/06 12:16:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vTravel.vXmlNodeTravelRoot import vXmlNodeTravelRoot

from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

import vidarc.vApps.vTravel.__register__

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xacIDAT(\x91c\\~n\x07\x03\x12h\xdf3\x9f\x01\x15T\xba$"s\x99\xf0H\x9f+Z\
\x8a\xa6\x9a\x81\x81\x81\x05n\xf0\xb9\xa2\xa5F}\xd1\xc8\xaa\x97.]\xfa\xe7\
\xcf\x9f\xf8\xf8x\x888D3\x13\\\xfa\xf0\xe1\xc3\xe7\x8a\x96\xc2\xb9\xdbw\xecx\
v\xf6\xac\xc0\x9f?\x10.\\\n\xaaa\xf6\xec\xd9\xff\xff\xff\xbfp\xf1\x12\\b\xf7\
\xae]\xcf\xbf\x7f\xbf\xfe\xf8\xf1\xb9s\xe7._\xbe\x0c\xb7\x99\xf9\xb6\xf2o[%\
\xc3X\xaf\xd0\xd3\xa7\xcf\x98\x8a\xfc\xbdz\xe3ZCd\xe5\xd5\xabW\x19\x19\x19\
\xd9\xb8\xb9Y88\xde\xbe}\xfb\xf3\xe7O\r\r\x8dk/\xef\xb5\xef\x99\xcf\xa8\xd7\
\x15\x0ew\xc6\xde\xd5st\x8cm\xb7\x1e<\xa6\xab\xad\xad\xa2\xa2\xb8x\xc92>>~\
\x0b\x0b\x0b\r\r\r\xb8\xf7\x10\xa1\xf4\xe7\xcf\x9f[o\xff\xcd_\xb5O^NI^^f\xcd\
\xda\xf5L\xcc\xcc\x8e\x8eN***\x10\xe3PB\x89\x81\x81\x81\x85\x85EP@\xfd\xff?\
\xc1S\xa7\x8e12\xfds\xd3\x97\xfe\xfc\xfa\xe9\xe3\xc7\x8f\xaf_\xbf\xf6\xef\
\xdf?NNNH0\xb2\xc0\xdd\xb3|\xf9\xae\x9f?T\x9e=\xdd\xf5\xeb\xf7\x83\xd7\xaf\
\x85\xfe}de\xf8+\xc8\xc8\xf1\xe3\xdf\xbf\x7f/_\xbe\x94\x91\x91A\xd8P\xe9\x92\
\xb8b\xf5\xaa\x07\x8f>1\xfc:+\xc8\xf5\xe8\x1f\xbbJxX\x18\x03\x06X}i\x0f\x03\
\x03\x03#$i<)\xeeT\xb2\xb4\xbc\xc9\xc3\xa5\xa1\xae\xee\xe7\x1f\x88\x1c\x83p\
\x00\x898\xa8\x93>\xdc\xbf\xffVR\xaal\xd1B\x06\x06\x06\xac\xaa\x11^\x85P\x8d\
w\xee\x10T\n\x01\x8ch\xa9\x95\x015\xc1b&>\xf4\xd4\x8a\x96\xbc1S;\x00\x8b\x93\
\xc8K\xd1\xf5\xf2O\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlTravel(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='travelroot'
    APPL_REF=['vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
    def __init__(self,appl='vTravel',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                                audit_trail=audit_trail)
            oRoot=vXmlNodeTravelRoot('root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeTravelRoot()
            self.RegisterNode(oRoot,False)
            
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            
            #vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
            vidarc.vApps.vTravel.__register__.RegisterNodes(self,bRegAsRoot=True)
            
            oDataColl=self.GetReg('dataCollector')
            self.LinkRegisteredNode(oRoot,oDataColl)
            
            oTravel=self.GetReg('travel')
            self.LinkRegisteredNode(oRoot,oTravel)
            oTravelDepartment=self.GetReg('travelDepartment')
            self.LinkRegisteredNode(oRoot,oTravelDepartment)
            self.LinkRegisteredNode(oTravelDepartment,oDataColl)
            
            oDocs=self.GetReg('Documents')
            self.LinkRegisteredNode(oRoot,oDocs)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'travels')
    #def New(self,revision='1.0',root='travelroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'travels')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtXmlDomReg.Build(self)
        self.BuildStateMachine()
        self.GenerateSortedTravelNrIds()
    def BuildStateMachine(self):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
    def BuildSrv(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.BuildStateMachine()
        self.GenerateSortedTravelNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def GenerateSortedTravelNrIds(self):
        self.travelNr=self.getSortedNodeIdsRec([],
                        'travel','travelID',None)
    def GetSortedTravelNrIds(self):
        try:
            return self.travelNr
        except:
            self.GenerateSortedTravelNrIds()
            return self.travelNr
    def AddNodeToSortedTravelNrIds(self,node):
        try:
            self.travelNr.AddNodeSingle(node,'travelID',None,self)
        except:
            vtLog.vtLngTB(self.GetOrigin())
