#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelDestinations.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelDestinations.py,v 1.5 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeTravelDestinationsPanel import *
        #from vXmlNodeTravelDestinationsEditDialog import *
        #from vXmlNodeTravelDestinationsAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeTravelDestinations(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Sum',None,'sum',None),
    #        ('Count',None,'Count',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='travelDestinations'):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'travel destinations')
    # ---------------------------------------------------------
    # specific
    def GetSetup(self):
        return [
            {'tag':'country'    ,'trans':'country','label':_('country'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.2},
            {'tag':'location'   ,'trans':'location','label':_('location'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.2},
            {'tag':'company'    ,'trans':'company','label':_('company'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.2},
            {'tag':'contact'    ,'trans':'contact','label':_('contact'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.2},
            ]
    def GetLaTexTableFmt(self):
        return '{|@{\\extracolsep\\fill}r|p{0.2\\linewidth}|p{0.2\\linewidth}||p{0.2\\linewidth}|p{0.2\\linewidth}|}'
    def GetValues(self,node):
        try:
            lVal=[]
            if node is None:
                return lVal
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                for cRow in self.doc.getChilds(nodeData,'row'):
                    lCol=['' for i in lMap]
                    for cCol in self.doc.getChilds(cRow):
                        sTag=self.doc.getTagName(cCol)
                        if sTag in lMap:
                            iNum=lMap.index(sTag)
                            lCol[iNum]=self.doc.getText(cCol)
                    lVal.append(lCol)
            nodeSum=self.doc.getChild(nodeData,'sum')
            if nodeSum is None:
                lSum=None
            else:
                lSum=['' for i in lMap]
                for cCol in self.doc.getChilds(nodeSum):
                    sTag=self.doc.getTagName(cCol)
                    if sTag in lMap:
                        iNum=lMap.index(sTag)
                        lSum[iNum]=self.doc.getText(cCol)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return lVal,lSum
    def GetSum(self,node):
        return self.Get(node,'sum')
    def SetValues(self,node,lVal,lSum):
        if node is None:
            return
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                self.doc.deleteNode(nodeData,node)
            nodeData=self.doc.getChildForced(node,'data')
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            for lRow in lVal:
                if len(lRow[0])==0:
                    continue
                nodeRow=self.doc.createSubNode(nodeData,'row')
                for sTag,sVal in zip(lMap,lRow):
                    self.doc.createSubNodeText(nodeRow,sTag,unicode(sVal))
            if lSum is not None:
                nodeRow=self.doc.createSubNode(nodeData,'sum')
                for sTag,sVal in zip(lMap,lSum):
                    self.doc.createSubNodeText(nodeRow,sTag,unicode(sVal))
                self.SetSum(node,lSum[-1])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        if name=='sum':
            return _(u'sum')
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe1IDAT(\x91}\x92Av\xc3@\x08C\xbf\x92\x1e\x8c\x9c\xc4G\x198Y\x9d\x93\
\xa9\x0b\xec\xf14i\xea\x05\x0fd=\x10\x1a\x04d\xe6K|/\'(\xc06\xa8*\x811\x06\
\x08\xfcg\xac\xca\x1bPU\xe01\xba\xd9G6\xb8\x7f\x1f\x13\xfe\xe5-\x132\xb3\xeb\
\x9e\xb32\x1a\x99xk\xbe\x01\'\x83\x17\xf6\xc8<\xa2ti\xceL\xfb\xf2\xc4\xc6v#\
\x99i0ti{\x9d0\x1d\xbc\xf6\x1b\xa7\xa7U\xd3\xbd\xa5k\xf7X\xf3\x04\xdb\xfe\
\xad\xe2k\xe9\x9as\x871\x0c\x1a\xed\x9e=\x0e\xbcl\xb7$ID\x84\xc4\'\xc7Z\x8f$\
\xcew8\xbe\xa9\xea=^g\xd2\xfc}\xdf;\x7f\xd9dE\x80; eDl\xdb&)\xe2{j\xe8\xbc*#\
\x1e\x8dHy\x07"\xa2][\xd9\x93\x17\xf1\x90\x90\xf4|\xc6t\xa9\xef\xef8^P\xcb\
\xadJ\xa8\xf5\xc03\xf3\x07\xcf\xbe\xeaG@2\x15e\x00\x00\x00\x00IEND\xaeB`\x82\
' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        #return None
        if GUI:
            #return vXmlNodeTravelDestinationsEditDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnTravelDestination',
                }
        else:
            return None
    def GetAddDialogClass(self):
        #return None
        if GUI:
            #return vXmlNodeTravelDestinationsAddDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnTravelDestination',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelDestinationsPanel
        else:
            return None
