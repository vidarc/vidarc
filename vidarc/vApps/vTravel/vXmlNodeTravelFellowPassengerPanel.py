#Boa:FramePanel:vXmlNodeTravelFellowPassengerPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelFellowPassengerPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelFellowPassengerPanel.py,v 1.2 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODETRAVELFELLOWPASSENGERPANEL, wxID_VXMLNODETRAVELFELLOWPASSENGERPANELVIFELLOWPASSENGER, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vXmlNodeTravelFellowPassengerPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFellowPassenger, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODETRAVELFELLOWPASSENGERPANEL,
              name=u'vXmlNodeTravelFellowPassengerPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.viFellowPassenger = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODETRAVELFELLOWPASSENGERPANELVIFELLOWPASSENGER,
              name=u'viFellowPassenger', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(304, 180))
        self.viFellowPassenger.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              self.OnViFellowPassengerVtinputGridsetupInfoChanged,
              id=wxID_VXMLNODETRAVELFELLOWPASSENGERPANELVIFELLOWPASSENGER)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viFellowPassenger.SetAutoStretch(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viFellowPassenger.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viFellowPassenger.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            tup=self.objRegNode.GetValues(node)
            self.viFellowPassenger.SetValue(tup[0])
            self.viFellowPassenger.SetModified(False)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            tup=self.viFellowPassenger.GetValueStr()
            self.objRegNode.SetValues(node,tup[0],tup[1])
            self.viFellowPassenger.SetModified(False)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def GetSum(self):
        tup=self.viFellowPassenger.GetValueStr()
        return tup[1][-1]
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viFellowPassenger.Enable(False)
        else:
            # add code here
            self.viFellowPassenger.Enable(True)
    def BindFellowPassengerChanged(self,func):
        self.viFellowPassenger.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              func,self.viFellowPassenger)

    def OnViFellowPassengerVtinputGridsetupInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
