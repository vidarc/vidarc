#----------------------------------------------------------------------------
# Name:         vXmlNodeTravel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravel.py,v 1.10 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

import os,copy
import vidarc.tool.report.vRepLatex as latex

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.state.veStateAttr import veStateAttr
from vidarc.ext.report.veRepDoc import veRepDoc
from vidarc.tool.lang.vtLgDictML import vtLgDictML
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        from vXmlNodeTravelPanel import *
        from vXmlNodeTravelEditDialog import *
        from vXmlNodeTravelAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeTravel(vtXmlNodeTag,veStateAttr,veDataCollectorCfg):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            #('Sum',None,'sum',None),
            ('Currency',None,'currency',None),
            ('TrvState',None,'trvState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='travel',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vTravel')
        vtXmlNodeTag.__init__(self,tagName)
        veStateAttr.__init__(self,'smTravel','trvState')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
        self.oSMlinker=None
        self.oRep=veRepDoc()
        self.dt=vtDateTime(True)
        self.lgDict=None
        if GUI:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            
            #domain=vtLgBase.getApplDomain()
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def GetDescription(self):
        return _(u'travel')
    # ---------------------------------------------------------
    # specific
    def GetSum(self,node):
        c=self.doc.getChild(node,'travelItems')
        if c is not None:
            sVal=self.doc.GetNodeAttribute(c,'GetSum')
            if len(sVal)==0:
                return '0.00'
            return sVal
        return '0.00'
        #return self.Get(node,'sum')
    def GetSumFloat(self,node):
        try:
            return float(self.GetSum(node))
        except:
            return 0.0
    def GetProject(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def GetPrjId(self,node):
        return self.GetChildForeignKeyStr(node,'project','fid','vPrj')
    def GetPrjIdNum(self,node):
        return self.GetChildForeignKey(node,'project','fid','vPrj')
    def GetVisited(self,node):
        return self.GetChildAttrStr(node,'visited','fid')
    def GetVisitedDepartment(self,node):
        return self.GetChildAttrStr(node,'visitedDepart','fid')
    def GetVisitedAddress(self,node):
        return self.GetChildAttrStr(node,'visited','address')
    def GetVisitedContact(self,node):
        return self.GetChildAttrStr(node,'visited','contact')
    def GetStartDtTm(self,node):
        return self.Get(node,'startDtTm')
    def GetEndDtTm(self,node):
        return self.Get(node,'endDtTm')
    def GetTraveler(self,node):
        return self.GetChildAttrStr(node,'traveler','fid')
    def GetTravelerId(self,node):
        return self.GetChildForeignKeyStr(node,'traveler','fid','vHum')
    def GetTravelerIdNum(self,node):
        return self.GetChildForeignKey(node,'traveler','fid','vHum')
    def GetCurrency(self,node):
        return self.Get(node,'currency')
    def GetTravelID(self,node):
        return self.GetAttrStr(node,'travelID')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    def SetSumFloat(self,node,val):
        self.SetSum(node,str(val))
    def SetVisitedStr(self,node,val):
        self.Set(node,'visited',val)
    def SetVisitedContactStr(self,node,val):
        self.Set(node,'contact',val)
    def SetVisitedAddress(self,node,val):
        self.SetChildAttrStr(node,'visited','address',val)
    def SetVisitedContact(self,node,val):
        self.SetChildAttrStr(node,'visited','contact',val)
    def SetStartDtTm(self,node,val):
        self.Set(node,'startDtTm',val)
    def SetEndDtTm(self,node,val):
        self.Set(node,'endDtTm',val)
    def SetTraveler(self,node,id):
        return self.SetChildAttrStr(node,'traveler','fid',id)
    def SetTravelerId(self,node,id):
        return self.SetChildForeignKeyStr(node,'traveler','fid',id,'vHum')
    def SetTravelerIdNum(self,node,id):
        return self.SetChildForeignKey(node,'traveler','fid',id,'vHum')
    def SetCurrency(self,node,val):
        self.Set(node,'currency',val)
    def SetTravelID(self,node,val):
        self.SetAttrStr(node,'travelID',val)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetPrjId(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKeyStr(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    def SetPrjIdNum(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKey(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    #def SetName(self,node,val):
    #    self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # state machine related
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,self.smAttr,'owner','vHum')
    def GetTrvState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            id=self.GetChildAttr(node,self.smAttr,'fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetTrvState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            c=self.GetChildForced(node,self.smAttr)
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,self.smAttr,'owner',val,'vHum')
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('travel on go'),None),
            'NoGo':(_('travel on no go'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetDateSmallStr(self.GetOrderDt(node))
        except:
            pass
        dtO=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        try:
            self.dt.SetDateSmallStr(self.GetDeliveryDt(node))
        except:
            pass
        dtD=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        return u'\n'.join([_(u'travel'),
                ' '+_('start date:')+dtO,
                ' '+_('end date:')+dtD])
    # ---------------------------------------------------------
    # document machine related
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            lang=kwargs['lang']
            self.oRep.__setLang__(lang)
            iLevel=kwargs['level']
            sName=self.GetName(node,lang)
            sDesc=self.GetDesc(node,lang)
            sCurrency=self.GetCurrency(node)
            
            f.write(os.linesep)
            f.write(self.oRep.__getHeadLine__(iLevel,sName))
            f.write(os.linesep)
            
            kkwargs=copy.copy(kwargs)
            kkwargs.update({'bSkipHeadLine':True,'bSkipSummary':True})
            veDataCollectorCfg.Process(self,f,*args,**kkwargs)
            
            
            self.__getTranslation__()
            if 0:
                oSupAdr=self.doc.GetReg('addresses')
                #oSupCont=self.doc.GetReg('contacts')
                fid=self.GetVisited(node)
                netDoc,nodeSup=self.doc.GetNode(fid)
                oSup=netDoc.GetRegByNode(nodeSup)
                if oSup is not None:
                    sVisited=oSup.GetML(nodeSup,'name',lang)
                else:
                    sVisited=''
                sAdr=self.GetVisitedAddress(node)
                #sCont=self.GetVisitedContact(node)
                fidCont=self.GetVisitedContact(node)
                fidOrderBy=self.GetOrderBy(node)
                oSupAdr.SetDoc(netDoc)
                #oSupCont.SetDoc(netDoc)
                if len(fidCont)>0:
                    netDocCont,nodeCont=self.doc.GetNode(fidCont)
                    oSupCont=netDocCont.GetRegByNode(nodeCont)
                    lCont=[]
                    for a in ['title','firstname','surname']:
                        lCont.append(oSupCont.GetML(nodeCont,a,lang=lang))
                else:
                    #netDocCont,nodeCont=None,None
                    lCont=None
                if len(fidOrderBy)>0:
                    netDocOrderBy,nodeOrderBy=self.doc.GetNode(fidOrderBy)
                    oOrderBy=netDocOrderBy.GetRegByNode(nodeOrderBy)
                    nodePersonal=netDocOrderBy.getChild(nodeOrderBy,'personal')
                    oPersonal=netDocOrderBy.GetRegByNode(nodePersonal)
                    lOrderBy=[]
                    for a in ['frm','title']:
                        lOrderBy.append(oPersonal.GetML(nodePersonal,a,lang=lang))
                    lOrderBy=[' '.join(lOrderBy)]
                    for a in ['firstname','surname']:
                        lOrderBy.append(oOrderBy.Get(nodeOrderBy,a))
                else:
                    lOrderBy=None
                lAdr=oSupAdr.GetValuesFromParent(nodeSup,sAdr)
                #lCont=oSupCont.GetValuesFromParent(nodeSup,sCont)
                _(u'address'),_(u'city'),_(u'zip'),_(u'street')
                _(u'firstname'),_(u'surname'),_(u'title'),_(u'contact')
                f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
                f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                f.write(''.join([self.lgDict.Get(u'visited',lang),' & \\\\ & ',os.linesep,]))
                self.oRep.__tableSimplePseudo__(f,'ll',[(sVisited,'')])
                f.write(''.join(['\\\\',os.linesep,]))
                if lAdr is not None:
                    f.write(''.join([self.lgDict.Get(u'address',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'street',lang),
                            self.lgDict.Get(u'city',lang),self.lgDict.Get(u'zip',lang)],lAdr))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lCont is not None:
                    f.write(''.join([self.lgDict.Get(u'contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    #f.write(''.join([self.lgDict.Get(u'contact',lang),os.linesep,os.linesep]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lCont))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lOrderBy is not None:
                    #f.write(''.join([self.lgDict.Get(u'our contact',lang),os.linesep,os.linesep]))
                    f.write(''.join([' & \\\\ ',os.linesep,self.lgDict.Get(u'our contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lOrderBy))
                f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
                f.write(os.linesep.join([latex.texReplace(s) for s in sDesc.split('\n')]))
                
            def convBool(s):
                if s=='1':
                    return 'X'
                else:
                    return ''
            def convOrig(s):
                return s
            def convFloat(s):
                try:
                    return vtLgBase.format('%6.2f',float(s))
                    #return '%6.2f'%float(s)
                except:
                    return ''
            def convEmpty(s):
                return ''
            def getConvCurrency(lSetup,lTrans,lFmt,lConv,lCurrency):
                for d in lSetup:
                    if 'is2Report' in d:
                        if d['is2Report']==0:
                            lConv.append(None)
                            #lFmt.append('c')
                            continue
                    if 'trans' in d:
                        lTrans.append(self.lgDict.Get(d['trans'],lang))
                    else:
                        lTrans.append('')
                    if 'isCurrency' in d:
                        if d['isCurrency']==1:
                            lCurrency.append('[ '+sCurrency+' ]')
                        else:
                            lCurrency.append('')
                    else:
                        lCurrency.append('')
                    if 'typedef' in d:
                        dType=d['typedef']
                        sType=dType['type']
                        if sType=='bool':
                            func=convBool
                            sFmt='c'
                        elif sType=='string':
                            func=convOrig
                            sFmt='l'
                        elif sType=='int':
                            func=convOrig
                            sFmt='r'
                        elif sType=='float':
                            func=convFloat
                            sFmt='r'
                        else:
                            func=convEmpty
                            sFmt='c'
                        lConv.append(func)
                        lFmt.append(sFmt)
                    else:
                        lConv.append(convEmpty)
                        lFmt.append('c')
            oDestinations=self.doc.GetReg('travelDestinations')
            lSetup=oDestinations.GetSetup()
            lTrans=[self.lgDict.Get('pos',lang)]
            lFmt=[]
            lConv=[]
            lCurrency=['']
            getConvCurrency(lSetup,lTrans,lFmt,lConv,lCurrency)
            nodeDestinations=self.doc.getChild(node,'travelDestinations')
            lVal,lSum=oDestinations.GetValues(nodeDestinations)
            sFmt=oDestinations.GetLaTexTableFmt()
            sCurrencyTable=' & '.join(lCurrency)
            strs=[]
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{%s}'%sFmt
            strs.append(s)
            s='\\hline '+' & '.join(lTrans)+'\\\\ %s \\\\ \\hline'%sCurrencyTable
            strs.append(s)
            strs.append('  \\endfirsthead')
            strs.append(s)
            strs.append('  \\endhead')
            iNum=1
            for lRow in lVal:
                lVal=[str(iNum)]
                for func,val in zip(lConv,lRow):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lRow)])+'\\\\'
                strs.append(s)
                iNum+=1
            if lSum is not None:
                strs.append('\\hline \\hline')
                lVal=['']
                for func,val in zip(lConv,lSum):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lSum)])+'\\\\'
                strs.append(s)
            strs.append('\\hline')
            sPrefix=' '.join([self.lgDict.Get('destination',lang),sName])
            strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
            strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            f.write(os.linesep.join(strs))
            f.write(os.linesep)
            
            oFellowPassenger=self.doc.GetReg('travelFellowPassenger')
            lSetup=oFellowPassenger.GetSetup()
            lTrans=[self.lgDict.Get('pos',lang)]
            lFmt=[]
            lConv=[]
            lCurrency=['']
            getConvCurrency(lSetup,lTrans,lFmt,lConv,lCurrency)
            nodeFellowPassenger=self.doc.getChild(node,'travelFellowPassenger')
            lVal,lSum=oFellowPassenger.GetValues(nodeFellowPassenger)
            sFmt=oFellowPassenger.GetLaTexTableFmt()
            strs=[]
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{%s}'%sFmt
            strs.append(s)
            s='\\hline '+' & '.join(lTrans)+'\\\\ \\hline'
            strs.append(s)
            strs.append('  \\endfirsthead')
            strs.append(s)
            strs.append('  \\endhead')
            iNum=1
            for lRow in lVal:
                lVal=[str(iNum)]
                for func,val in zip(lConv,lRow):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lRow)])+'\\\\'
                strs.append(s)
                iNum+=1
            strs.append('\\hline')
            sPrefix=' '.join([self.lgDict.Get('fellow passenger',lang),sName])
            strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
            strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            f.write(os.linesep.join(strs))
            f.write(os.linesep)
            
            #f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                #f.write(''.join([os.linesep,'\\vspace{1cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
            f.write(os.linesep)
            
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        
    # ---------------------------------------------------------
    # inheritance
    def GetAttrValueToDict(self,d,node,*args,**kwargs):
        if node is None:
            return d
        if d is None:
            d={}
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
        if netPrj is not None:
            if nodePrj is not None:
                dPrj=netPrj.GetAttrValueToDict(nodePrj,*args,**kwargs)
                for k,v in dPrj.items():
                    d[':'.join(['vPrj',k])]=v
        return d
    def GetAttributeValFullLang(self,node,tagName,lang=None):
        if tagName[:5]=='vPrj:':
            netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
            if netPrj is not None:
                if nodePrj is not None:
                    return netPrj.getNodeAttributeValFullLang(nodePrj,tagName[5:],lang=lang)
        return ''
    def SetDoc(self,doc,bNet=False):
        vtXmlNodeBase.SetDoc(self,doc)
        if self.doc is None:
            self.lgDict=None
            return
        if vcCust.is2Import(__name__):
            l=[]
            for tup in self.doc.GetLanguages():
                l.append(tup[1])
            if len(l)==0:
                l=['en','de','fr']
            try:
                self.lgDict=vtLgDictML('vTravel',langs=l)
            except:
                try:
                    self.lgDict=vtLgDictML('vTravel',dn=os.path.split(__file__)[0],langs=l)
                except:
                    self.lgDict=None
                    vtLog.vtLngCurCls(vtLog.ERROR,'translation veReportDoc not found',self)
                    vtLog.vtLngTB(self.__class__.__name__)
    def __createPost__(self,node,func=None,*args,**kwargs):
        try:
            if self.doc is None:
                return
            oLogin=self.doc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
                return
            if oLogin.GetUsrId()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
                return
            if self.oSMlinker is None:
                self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
            if self.oSMlinker is not None:
                oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            else:
                oRun=None
            if oRun is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,self.smAttr),self)
                return
            if oRun.GetInitID()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
                return
            if node is not None:
                self.dt.Now()
                if len(self.GetTag(node))==0:
                    try:
                        sTag=u' '.join([oLogin.GetUsr(),self.dt.GetDateStr()])
                    except:
                        vtLog.vtLngTB(self.GetName())
                        sTag=self.dt.GetDateStr()
                    self.SetTag(node,sTag)
                fid=self.GetProject(node)
                netPrj,nodePrj=self.doc.GetNode(fid)
                if nodePrj is not None:
                    sPrj=netPrj.GetPrjName(nodePrj)
                else:
                    sPrj=''
                fid=self.GetVisited(node)
                netSup,nodeSup=self.doc.GetNode(fid)
                if nodeSup is not None:
                    langs=self.doc.GetLanguageIds()
                    if langs is not None:
                        sDt=self.dt.GetDateStr()
                        for lang in langs:
                            sSup=netSup.GetNodeAttribute(nodeSup,'GetName',lang)
                            self.SetName(node,' '.join([sPrj,sDt,sSup]),lang)
                oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
                if func is not None:
                    func(self,node,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return vtXmlNodeTag.GetAttrFilterTypes(self)+[
                ('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('visited',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('contact',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('|travelID',vtXmlFilterType.FILTER_TYPE_LONG),
                #('projects',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x92IDAT8\x8d\x9d\x92]l\x8ba\x18\x86\xaf\xf7\xeb\xc8\xda\x8alY\xbaZ\
\xb3\x881v\xb0\xaf\x19\xeb:6\x12\xe6o\xd2D\xcc\x1c\xad\xce\x18F8u\x80\x08\
\xc1!N\xec\xd0\x82 \x98\x9f,$\xec`\xdd\xda\x92\xe8\x8c\x15\xc5\x0c\x89\xdf\
\xd5h2\xabm\xdf:\xbe>\x0e\xfc\x84\x8cdq\'\xef\xc9\xf3\xe6\xbar\'\xcf\xa3\x94\
fa"\xd9\xb6c\xbb\xe4\xe6\xe4\xb0o\xdf~\xf5\xfb\\\x03x\xf3\xfa\x95\xdc\xe9\
\x8c\xc8\xbf\xe0S\'O\xc8t\xd3\xe4S\x7f\xff\xb8?\xedf8$\x81@\x80\xbe\xbe>B\
\xc1\x8e?$\xed\x816\xd9\xda\xb0EZ[[\xb1~\xfc\xc8\xd7\x91\x91\xf1\x82\xde\xde\
^\x0c\xc3\xa0\xba\xba\x1aS\x14\xc1\xdf$UK\x97)\x97\xcb\x85a\x18$\x8a\x8ax78\
\xc8\xd93\xa7\xe5b\xf3\x05\xe9h\x0f\x08\x80\xb6ac\xbd\x1a\x1b\x1b\xa3\'\xf6\
\x88\n{\x02s8N8\x14\x12\x80;\x9d\x11\xc9\xca\xca\xa2\xbc\xbc\x9c\x82\x82\x02\
jkk\xb1\xd9ltuu\x11\x0e\x87\x01PJ\xb3\x10\xed\xbe\'\xe1`\x1b\xfe\xc2\x14SK\
\xeb\xb8r\xeb.\xfd\xf18N\xa7\x93\xca\x8a\xf9\x0c&\x93\xec\xda\xb5\x87T*\x85\
\xc3\xe1\xc0j\xb5\xe2\xf3\xf9\xa8Y[\xab2\x00\xe6\xce+U\x8d\x8d\xc7$\xf8\xa5\
\x10\xfd\xf3\x17>\xbc\x8fSRRB\x99g\x1eCCC<~\xdc\x83\xa6ix\xbd^<\x1e\x0f\xd9\
\xd9\xd9,^R\xa5~5\xf8\x99\xfa\x8d\xf5b\xb7\xe7\xe3r\xe5\xd0\xd0\xe0\'\xfe\
\xee-\xcd\x97[\xe8\x8e>\xc0\xeb)\xc5\xeb-c\xf9\x8a\x95\x7f\xac\xf1\x97 r\xbb\
S"\x91\xe7\xe4\xe7Wq\xfdz3\xabV9\x19\x1eIQ\xe6.\x80\x17\x17\xb9?\\L*\x9d\x81\
i\x9a\x88\x08\xba\xae\xb3\xa0\xa2R)\xa5Yxp?*\xc1\xe0s\x8a\x8bW\xd3\xd6\x16b`\
\xe0\x06EE\xf9\xac\xa9\xa9\xc1i\xcf\xc4\x0c^e\xd2\xf2u\x90i#\x91H\x90L&ijjbt\
t\xf4{\x83\xc6c\xc7%3\xb3\x9c\x9e\'\xcf\xd0\xa4\x1d\xdb\xd4lb\xb1G8\x1c\x0e\
\xd2\xe94X,`\x9a?*+D\x04\xc30\xd0u\x1du\xee\xc2y\x89\xc5^\x90\x1c\xd4\x981\
\xed\r\xb6IC\\\xeb\x18\xc0n\xb7\xa2\xeb:\x9a\xa6\x8d;\x1e\x11!//\x8f\xfaM\
\x9b\x95\xda\xe9\xf7K\xa5\xc7\x836{\x0e\xd1\xeeN\xa2\xd1\x87\xcc\x9cU\xc8\
\xe1#G\xd58\xf2/Q\xbb\x95\x92\xc5\xb9\xb9\\Z\xb8\x88\xf7\x92\xa6\xc4\xed\xe6\
\xc0\xc1C\x13\x82\x012\x9e\xba\xdd\xbcT\x8a\xc9S\xec\xd4\xf9|\xd4\xf9\xd7O\
\x18\x06\xe0JK\x8b\xec\xde\xbbW\x94f\xe1\x7f\xde7\x80\xe3\x0c~o\xf9\x1a5\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        #return veDataCollectorCfg.GetEditDialogClass(self)
        if GUI:
            #return vXmlNodeTravelEditDialog
            d=veDataCollectorCfg.GetEditDialogClass(self).copy()
            d['pnCls']=veDataCollectorCfg.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeTravelAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(400, 420),
                    'pnName':'pnTravel',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelPanel
        else:
            return None
    def GetCollectorPanelClass(self):
        return veDataCollectorCfg.GetPanelClass(self)

_('sum'),_('projects'),_('visited'),_('contact'),_(u'|travelID')
