#----------------------------------------------------------------------------
# Name:         vNetTravel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vNetTravel.py,v 1.1 2007/11/22 08:53:09 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vTravel.vXmlTravel import vXmlTravel
from vidarc.tool.net.vNetXmlWxGui import *

class vNetTravel(vXmlTravel,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlTravel.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                            audit_trail=audit_trail)
        vXmlTravel.SetFN(self,fn)
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlTravel.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlTravel.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'file:%s;slient:%d;thread:%d'%(repr(fn),silent,bThread),origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlTravel.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlTravel.Open(self,fn,True)
        else:
            iRet=vXmlTravel.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlTravel.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlTravel.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlTravel.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlTravel.delNode(self,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlTravel.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlTravel.__stop__(self)
