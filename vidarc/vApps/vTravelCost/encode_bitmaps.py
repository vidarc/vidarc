#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2008/01/14 07:07:45 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application           img/VidMod_TravelCost01_16.ico    images.py",
    "-a -u -n Plugin                img/VidMod_TravelCost01_16.png    images.py",
    "-a -u -n TravelCost            img/PlaneCost01_16.png            images.py",
    "-a -u -n CostItemsPriv         img/CashMoney_01_16.png           images.py",
    "-a -u -n CostItemsComp         img/CashReceipt_01_16.png         images.py",
    "-a -u -n Milage                img/Measure01_16.png              images.py",
    "-a -u -n TravelCostDepart      img/AreaCost03_16.png             images.py",
    
    "-u -i -n Splash img/splashPurchase01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

