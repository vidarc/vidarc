��    E      D  a   l      �  %   �            	   $     .     @     H     O     X     ]     c     k     w          �  	   �  
   �     �     �     �  	   �     �  
   �  	   �     �     �     �                    !     (  ,   E  
   r     }     �     �     �     �     �  
   �     �     �     �     �     �     �     �          
          -     G     ^     {     �     �     �     �     �     �     	     	     	     )	     6	     >	     B	  �  P	     �
       	              )     F     N  	   U     _     e     m     s          �     �  
   �  	   �  
   �     �     �  	   �     �     �     �     �     �                         *     1  7   Q  
   �     �     �     �     �     �     �  
   �  
   �     �     �                    !     *     1     =     R     q     �     �     �     �     �  &        )     ;  	   N     X     \     h     z     �     �         '      8   3       D   <      B      !             @      +       ,   )                     ;   ?      >   5                      -                     2            E       =                 *           A   "                  (   0   /          6   	          #   &   C   $       .   4       %                  1                 7   
          :   9    --lang <language id according ISO639> Apply Cancel Travel Nr VIDARC TravelCost address amount category city clear company company car contact country currency date time department distance document end date end date: end location end milage firstname help information item license number location milage normal open the <filename> at start open the <filename> to configure application passengers private car project projects receipt role show this screen start date start date: start location start milage state street sum surname title travel cost travel cost attributes travel cost company items travel cost department travel cost fellow passenger travel cost items travel cost mileage allowance travel cost on go travel cost on no go travel cost private items travel cost root travel cost times traveler type vTravelCost valid flags: visited zip |travelCostID Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-01-14 09:38+0100
PO-Revision-Date: 2005-09-20 14:49+0100
Last-Translator: Walter Obweger <walter.obweger@vidarc.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
 --lang <Sprach id nach ISO639> Anwenden Abbrechen Reise Nr VIDARC Reisekostenabrechnung Adresse Betrag Kategorie Stadt S�ubern Firma Firmenwagen Kontakt Land W�hrung Datum Zeit Abteilung Entfernung Dokument Enddatum Enddatum: Endort Endkilometerstand Vorname Hilfe Information Eintrag Kennzeichen Ort Kilometerstand Normal �ffnet die <Dateiname> am Start �ffnet die <filename> als Konfiguration def Applikation Passagiere Privatwagen Projekt Projekte Rechnung Rolle gibt diese Meldungen aus Startdatum Startdatum Startort Startklometerstand Zustand Strasse Summe Nachname Anrede Reisekosten Reisekostenattribute Reisekosten Ausgaben von Firma Reisekosten Abteilung Reisekosten Passagiere Reisekosten Ausgaben Reisekosten Kilometergeld Reisekosten wenn bewilligt Reisekosten wenn verwehrt Reisekosten Ausgaben von Dienstnehmner Reisekostenwurzel Reisekosten Zeiten Reisender Typ vTravelCost g�ltige Optionen: besucht PLZ |travelCostID 