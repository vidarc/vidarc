#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelCost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelCost.py,v 1.7 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

import os,copy
import vidarc.tool.report.vRepLatex as latex

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.state.veStateAttr import veStateAttr
from vidarc.ext.report.veRepDoc import veRepDoc
from vidarc.tool.lang.vtLgDictML import vtLgDictML
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        from vXmlNodeTravelCostPanel import vXmlNodeTravelCostPanel
        #from vXmlNodeTravelCostEditDialog import *
        #from vXmlNodeTravelCostAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeTravelCost(vtXmlNodeTag,veStateAttr,veDataCollectorCfg):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            #('Sum',None,'sum',None),
            ('Currency',None,'currency',None),
            ('TrvCostState',None,'trvCostState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='travelCost',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vTravelCost')
        vtXmlNodeTag.__init__(self,tagName)
        veStateAttr.__init__(self,'smTravelCost','trvCostState')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
        self.oSMlinker=None
        self.oRep=veRepDoc()
        self.dt=vtDateTime(True)
        self.lgDict=None
        if GUI:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            
            #domain=vtLgBase.getApplDomain()
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def GetDescription(self):
        return _(u'travel cost')
    # ---------------------------------------------------------
    # specific
    def GetSum(self,node):
        val=0
        c=self.doc.getChild(node,'travelCostItemsComp')
        if c is not None:
            sVal=self.doc.GetNodeAttribute(c,'GetSum')
            if len(sVal)!=0:
                try:
                    val+=float(sVal)
                except:
                    pass
        c=self.doc.getChild(node,'travelCostItemsPriv')
        if c is not None:
            sVal=self.doc.GetNodeAttribute(c,'GetSum')
            if len(sVal)!=0:
                try:
                    val+=float(sVal)
                except:
                    pass
        return '%6.2f'%val
    def GetSumFloat(self,node):
        try:
            return float(self.GetSum(node))
        except:
            return 0.0
    def GetProject(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def GetPrjId(self,node):
        return self.GetChildForeignKeyStr(node,'project','fid','vPrj')
    def GetPrjIdNum(self,node):
        return self.GetChildForeignKey(node,'project','fid','vPrj')
    def GetVisited(self,node):
        return self.GetChildAttrStr(node,'visited','fid')
    def GetVisitedDepartment(self,node):
        return self.GetChildAttrStr(node,'visitedDepart','fid')
    def GetVisitedAddress(self,node):
        return self.GetChildAttrStr(node,'visited','address')
    def GetVisitedContact(self,node):
        return self.GetChildAttrStr(node,'visited','contact')
    def GetStartDtTm(self,node):
        return self.Get(node,'startDtTm')
    def GetEndDtTm(self,node):
        return self.Get(node,'endDtTm')
    def GetTraveler(self,node):
        return self.GetChildAttrStr(node,'traveler','fid')
    def GetTravelerId(self,node):
        return self.GetChildForeignKeyStr(node,'traveler','fid','vHum')
    def GetTravelerIdNum(self,node):
        return self.GetChildForeignKey(node,'traveler','fid','vHum')
    def GetCurrency(self,node):
        return self.Get(node,'currency')
    def GetTravelCostID(self,node):
        return self.GetAttrStr(node,'travelCostID')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    def SetSumFloat(self,node,val):
        self.SetSum(node,str(val))
    def SetVisitedStr(self,node,val):
        self.Set(node,'visited',val)
    def SetVisitedContactStr(self,node,val):
        self.Set(node,'contact',val)
    def SetVisitedAddress(self,node,val):
        self.SetChildAttrStr(node,'visited','address',val)
    def SetVisitedContact(self,node,val):
        self.SetChildAttrStr(node,'visited','contact',val)
    def SetStartDtTm(self,node,val):
        self.Set(node,'startDtTm',val)
    def SetEndDtTm(self,node,val):
        self.Set(node,'endDtTm',val)
    def SetTraveler(self,node,id):
        return self.SetChildAttrStr(node,'traveler','fid',id)
    def SetTravelerId(self,node,id):
        return self.SetChildForeignKeyStr(node,'traveler','fid',id,'vHum')
    def SetTravelerIdNum(self,node,id):
        return self.SetChildForeignKey(node,'traveler','fid',id,'vHum')
    def SetCurrency(self,node,val):
        self.Set(node,'currency',val)
    def SetTravelCostID(self,node,val):
        self.SetAttrStr(node,'travelCostID',val)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetPrjId(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKeyStr(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    def SetPrjIdNum(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKey(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    #def SetName(self,node,val):
    #    self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # state machine related
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,self.smAttr,'owner','vHum')
    def GetTrvCostState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            id=self.GetChildAttr(node,self.smAttr,'fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetTrvCostState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            c=self.GetChildForced(node,self.smAttr)
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,self.smAttr,'owner',val,'vHum')
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('travel cost on go'),None),
            'NoGo':(_('travel cost on no go'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetDateSmallStr(self.GetOrderDt(node))
        except:
            pass
        dtO=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        try:
            self.dt.SetDateSmallStr(self.GetDeliveryDt(node))
        except:
            pass
        dtD=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        return u'\n'.join([_(u'travel cost'),
                ' '+_('start date:')+dtO,
                ' '+_('end date:')+dtD])
    # ---------------------------------------------------------
    # document machine related
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            lang=kwargs['lang']
            self.oRep.__setLang__(lang)
            iLevel=kwargs['level']
            sName=self.GetName(node,lang)
            sDesc=self.GetDesc(node,lang)
            sCurrency=self.GetCurrency(node)
            
            f.write(os.linesep)
            f.write(self.oRep.__getHeadLine__(iLevel,sName))
            f.write(os.linesep)
            
            kkwargs=copy.copy(kwargs)
            kkwargs.update({'bSkipHeadLine':True,'bSkipSummary':True})
            veDataCollectorCfg.Process(self,f,*args,**kkwargs)
            
            
            self.__getTranslation__()
            if 0:
                oSupAdr=self.doc.GetReg('addresses')
                #oSupCont=self.doc.GetReg('contacts')
                fid=self.GetVisited(node)
                netDoc,nodeSup=self.doc.GetNode(fid)
                oSup=netDoc.GetRegByNode(nodeSup)
                if oSup is not None:
                    sVisited=oSup.GetML(nodeSup,'name',lang)
                else:
                    sVisited=''
                sAdr=self.GetVisitedAddress(node)
                #sCont=self.GetVisitedContact(node)
                fidCont=self.GetVisitedContact(node)
                fidOrderBy=self.GetOrderBy(node)
                oSupAdr.SetDoc(netDoc)
                #oSupCont.SetDoc(netDoc)
                if len(fidCont)>0:
                    netDocCont,nodeCont=self.doc.GetNode(fidCont)
                    oSupCont=netDocCont.GetRegByNode(nodeCont)
                    lCont=[]
                    for a in ['title','firstname','surname']:
                        lCont.append(oSupCont.GetML(nodeCont,a,lang=lang))
                else:
                    #netDocCont,nodeCont=None,None
                    lCont=None
                if len(fidOrderBy)>0:
                    netDocOrderBy,nodeOrderBy=self.doc.GetNode(fidOrderBy)
                    oOrderBy=netDocOrderBy.GetRegByNode(nodeOrderBy)
                    nodePersonal=netDocOrderBy.getChild(nodeOrderBy,'personal')
                    oPersonal=netDocOrderBy.GetRegByNode(nodePersonal)
                    lOrderBy=[]
                    for a in ['frm','title']:
                        lOrderBy.append(oPersonal.GetML(nodePersonal,a,lang=lang))
                    lOrderBy=[' '.join(lOrderBy)]
                    for a in ['firstname','surname']:
                        lOrderBy.append(oOrderBy.Get(nodeOrderBy,a))
                else:
                    lOrderBy=None
                lAdr=oSupAdr.GetValuesFromParent(nodeSup,sAdr)
                #lCont=oSupCont.GetValuesFromParent(nodeSup,sCont)
                _(u'address'),_(u'city'),_(u'zip'),_(u'street')
                _(u'firstname'),_(u'surname'),_(u'title'),_(u'contact')
                f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
                f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                f.write(''.join([self.lgDict.Get(u'visited',lang),' & \\\\ & ',os.linesep,]))
                self.oRep.__tableSimplePseudo__(f,'ll',[(sVisited,'')])
                f.write(''.join(['\\\\',os.linesep,]))
                if lAdr is not None:
                    f.write(''.join([self.lgDict.Get(u'address',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'street',lang),
                            self.lgDict.Get(u'city',lang),self.lgDict.Get(u'zip',lang)],lAdr))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lCont is not None:
                    f.write(''.join([self.lgDict.Get(u'contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    #f.write(''.join([self.lgDict.Get(u'contact',lang),os.linesep,os.linesep]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lCont))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lOrderBy is not None:
                    #f.write(''.join([self.lgDict.Get(u'our contact',lang),os.linesep,os.linesep]))
                    f.write(''.join([' & \\\\ ',os.linesep,self.lgDict.Get(u'our contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lOrderBy))
                f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
                f.write(os.linesep.join([latex.texReplace(s) for s in sDesc.split('\n')]))
                
            oItems=self.doc.GetReg('travelCostItems')
            lSetup=oItems.GetSetup()
            lTrans=[self.lgDict.Get('pos',lang)]
            lFmt=[]
            def convBool(s):
                if s=='1':
                    return 'X'
                else:
                    return ''
            def convOrig(s):
                return s
            def convFloat(s):
                try:
                    return vtLgBase.format('%6.2f',float(s))
                    #return '%6.2f'%float(s)
                except:
                    return ''
            def convEmpty(s):
                return ''
            lConv=[]
            lCurrency=['']
            for d in lSetup:
                if 'is2Report' in d:
                    if d['is2Report']==0:
                        lConv.append(None)
                        #lFmt.append('c')
                        continue
                if 'trans' in d:
                    lTrans.append(self.lgDict.Get(d['trans'],lang))
                else:
                    lTrans.append('')
                if 'isCurrency' in d:
                    if d['isCurrency']==1:
                        lCurrency.append('[ '+sCurrency+' ]')
                    else:
                        lCurrency.append('')
                else:
                    lCurrency.append('')
                if 'typedef' in d:
                    dType=d['typedef']
                    sType=dType['type']
                    if sType=='bool':
                        func=convBool
                        sFmt='c'
                    elif sType=='string':
                        func=convOrig
                        sFmt='l'
                    elif sType=='int':
                        func=convOrig
                        sFmt='r'
                    elif sType=='float':
                        func=convFloat
                        sFmt='r'
                    else:
                        func=convEmpty
                        sFmt='c'
                    lConv.append(func)
                    lFmt.append(sFmt)
                else:
                    lConv.append(convEmpty)
                    lFmt.append('c')
            nodeItems=self.doc.getChild(node,'travelCostItems')
            lVal,lSum=oItems.GetValues(nodeItems)
            sFmt=oItems.GetLaTexTableFmt()
            sCurrencyTable=' & '.join(lCurrency)
            strs=[]
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{%s}'%sFmt
            strs.append(s)
            s='\\hline '+' & '.join(lTrans)+'\\\\ %s \\\\ \\hline'%sCurrencyTable
            strs.append(s)
            strs.append('  \\endfirsthead')
            strs.append(s)
            strs.append('  \\endhead')
            iNum=1
            for lRow in lVal:
                lVal=[str(iNum)]
                for func,val in zip(lConv,lRow):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lRow)])+'\\\\'
                strs.append(s)
                iNum+=1
            if lSum is not None:
                strs.append('\\hline \\hline')
                lVal=['']
                for func,val in zip(lConv,lSum):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lSum)])+'\\\\'
                strs.append(s)
            strs.append('\\hline')
            sPrefix=' '.join([self.lgDict.Get('destination',lang),sName])
            strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
            strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            f.write(os.linesep.join(strs))
            f.write(os.linesep)
            
            #f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                #f.write(''.join([os.linesep,'\\vspace{1cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
            f.write(os.linesep)
            
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        
    # ---------------------------------------------------------
    # inheritance
    def GetAttrValueToDict(self,d,node,*args,**kwargs):
        if node is None:
            return d
        if d is None:
            d={}
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
        if netPrj is not None:
            if nodePrj is not None:
                dPrj=netPrj.GetAttrValueToDict(nodePrj,*args,**kwargs)
                for k,v in dPrj.items():
                    d[':'.join(['vPrj',k])]=v
        return d
    def GetAttributeValFullLang(self,node,tagName,lang=None):
        if tagName[:5]=='vPrj:':
            netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
            if netPrj is not None:
                if nodePrj is not None:
                    return netPrj.getNodeAttributeValFullLang(nodePrj,tagName[5:],lang=lang)
        return ''
    def SetDoc(self,doc,bNet=False):
        vtXmlNodeBase.SetDoc(self,doc)
        if self.doc is None:
            self.lgDict=None
            return
        if vcCust.is2Import(__name__):
            l=[]
            for tup in self.doc.GetLanguages():
                l.append(tup[1])
            if len(l)==0:
                l=['en','de','fr']
            try:
                self.lgDict=vtLgDictML('vTravelCost',langs=l)
            except:
                try:
                    self.lgDict=vtLgDictML('vTravelCost',dn=os.path.split(__file__)[0],langs=l)
                except:
                    self.lgDict=None
                    vtLog.vtLngCurCls(vtLog.ERROR,'translation veReportDoc not found',self)
                    vtLog.vtLngTB(self.__class__.__name__)
    def __createPost__(self,node,func=None,*args,**kwargs):
        try:
            if self.doc is None:
                return
            oLogin=self.doc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
                return
            if oLogin.GetUsrId()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
                return
            if self.oSMlinker is None:
                self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
            if self.oSMlinker is not None:
                oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            else:
                oRun=None
            if oRun is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,self.smAttr),self)
                return
            if oRun.GetInitID()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
                return
            if node is not None:
                self.dt.Now()
                if len(self.GetTag(node))==0:
                    try:
                        sTag=u' '.join([oLogin.GetUsr(),self.dt.GetDateStr()])
                    except:
                        vtLog.vtLngTB(self.GetName())
                        sTag=self.dt.GetDateStr()
                    self.SetTag(node,sTag)
                fid=self.GetProject(node)
                netPrj,nodePrj=self.doc.GetNode(fid)
                if nodePrj is not None:
                    sPrj=netPrj.GetPrjName(nodePrj)
                else:
                    sPrj=''
                fid=self.GetVisited(node)
                netSup,nodeSup=self.doc.GetNode(fid)
                if nodeSup is not None:
                    langs=self.doc.GetLanguageIds()
                    if langs is not None:
                        sDt=self.dt.GetDateStr()
                        for lang in langs:
                            sSup=netSup.GetNodeAttribute(nodeSup,'GetName',lang)
                            self.SetName(node,' '.join([sPrj,sDt,sSup]),lang)
                oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return vtXmlNodeTag.GetAttrFilterTypes(self)+[
                ('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('visited',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('contact',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('|travelCostID',vtXmlFilterType.FILTER_TYPE_LONG),
                #('projects',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02cIDAT8\x8d\x8d\x91OH\xd3a\x18\xc7?\xefoY:A\x8c\x81\x8b1$\xcb\xf2\xe0\
t\xa5\xb9\xd2\x82\xd0\xfehVX\xc3\x04\xd7-\xad,\xec\x12\xe1\xa5\x8b\x91E\x1d\
\xaaK\x1d\x85\x8c\x8a\x9a\x95\xec$\x1e\x9cn\xabhSs\xd6$1%J\xf2OzHK\x7f:\xfb\
\xf9vHMqR\xdf\xe3\xfb\xbc\xcf\xe7\xfd<\xcf+\x84\xa2\xe3\x7fr\xfeB\xa5L0\x18\
\xa8\xae\xbe"\x96\x9e+\x00\x03_>\xcb\xb6\x80_\xae\xd6\xfc\xa0\xee\xbeL\xd44\
\xbe\x8f\x8c\xac\xa8)/}^\xe9v\xbb\x19\x1c\x1c\xc4\xebi]\x06iq7\xcbs\x15geSS\
\x131\xa3\xa3\xfc\x9a\x9aZ\t\xe8\xed\xedEUU\xf2\xf3\xf3\xd1\xa4\xc0\xb3\x04\
\x92\x9b\xb7O\x98L&TUe,%\x85\xaf\xe3\xe3<~\xf4P>\xabw\xca\xd6\x16\xb7\x04PN\
\x95\x95\x8bp8LO\xa8\x9b\xec\xd81\xb4\xc9!|^\xaf\x04h\x0b\xf8e||<6\x9b\x8d\
\xa4\xa4$\xecv;z\xbd\x9e\xf6\xf6v|>\x1f\x00B(:\x82\x9do\xa5\xcf\xd3\x8c#y\
\x86\xb8\x8cR\x1a^u024\x84\xd1h$\'{\'\xaf\xcd\x89+\xd4\xd7\xba\\\x1c;n\x17\n\
\xc0\xb6\xed\x19B\x89\x8a\xc13\x9b\xca\xa7\x1f\xb3|\x1b\x1e\xc2j\xb5r\xe4\
\xf0\xa1\x88\xcd\x00\xe1\xa2\xa2\xbf\x06\x0b)/+\x97\xb1\xb1fL&\x03\x15\x15\
\x0e\x9a\xe2\xe2\x17k\xfd7n\x92\x95\xb5\x83\xfd\x07\x0e\n\x00\xe7\x9c&\x97\
\x01\xfco\x02\xd2\xef\xef\xc3l\xce\xa5\xb1\xb1\x9e\x82\x02#\x9a\xbd\x18\x00\
\xcb\x8b\x8btM\xa623\xb7\x06M\xd3\x90Rb\xb1X\xd8\x95\x9d#\x84Pt\xbc\xeb\nJ\
\x8f\xa7\x8f\xd4\xd4\xa347{\xb1\xd6\xe4E\xd4^\x9a\xb6\xaa*\xa6\xa7\xa7\xff\
\x18\xdc\xbb[+\xa3\xa3m\xf4|\xf8\x88"[\xd0\xc7\xad\'\x14\xea\xe6\x84\xd3\t\
\x80\xbb\xb2\x124m~f\x81\x94\x12UU\xb1X,\x88\'\xce\xa72\x14\xeagb\\a\xe3\x86\
\x01\xf4Q?1\\\xaa\xfb\xa7\xc1Dm-\xe5\xa7\xcf\x08Q\xe5p\xc8\x9c\xccL\x94-[\tv\
\x06\x08\x06\xdf\xb3is2\xb7n\xdf\x11\x0b\x8b*Qtb5\x90\xb8,\x84\xdc\x9b\x90\
\xc0\xf3\xdd{\x18\x96sX\xd3\xd2\xb8Zsm\xd9\xa6#A\x16\x7f\xa18=]\xae\x13\x82\
\xb5V+\xf9\x85\x85\x94:NF\xbc\x18)%\x8aN\x88\x06\x97K\xb6wtp}\xfe\xd5H\x89\
\x04Y0\xfa\r\xc3\xd3\x02!=/^B\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=veDataCollectorCfg.GetEditDialogClass(self).copy()
            d['pnCls']=veDataCollectorCfg.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnName':'pn%s'%self.tagName[8:]}
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelCostPanel
        else:
            return None
    def GetCollectorPanelClass(self):
        return veDataCollectorCfg.GetPanelClass(self)

_('sum'),_('projects'),_('visited'),_('contact'),_(u'|travelCostID')
