#Boa:FramePanel:vXmlNodeTravelCostMilagePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelCostMilagePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080109
# CVS-ID:       $Id: vXmlNodeTravelCostMilagePanel.py,v 1.3 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODETRAVELCOSTMILAGEPANEL, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELCHCCOMP, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELCHCPRIV, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLCATEGORY, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLDISTANCE, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLLICENSENR, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLMILAGEEND, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLMILAGESTART, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLPASS, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELVIDISTANCE, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELVILICENSENR, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGE, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGEEND, 
 wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGESTART, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vXmlNodeTravelCostMilagePanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsFen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLicenseNr, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viLicenseNr, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblCategory, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcComp, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.chcPriv, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblMilageStart, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viMilageStart, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblMilageEnd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viMilageEnd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDistance, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDistance, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsFen, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblPass, 0, border=0, flag=0)
        parent.AddWindow(self.viMilage, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsFen_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)
        parent.AddGrowableCol(3)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.fgsFen = wx.FlexGridSizer(cols=4, hgap=4, rows=4, vgap=4)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_fgsFen_Items(self.fgsFen)
        self._init_coll_fgsFen_Growables(self.fgsFen)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODETRAVELCOSTMILAGEPANEL,
              name=u'vXmlNodeTravelCostMilagePanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(375, 233),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(367, 206))
        self.SetAutoLayout(True)

        self.lblLicenseNr = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLLICENSENR,
              label=_(u'license number'), name=u'lblLicenseNr', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)

        self.viLicenseNr = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVILICENSENR,
              name=u'viLicenseNr', parent=self, pos=wx.Point(60, 0),
              size=wx.Size(124, 21), style=0)

        self.lblCategory = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLCATEGORY,
              label=_(u'category'), name=u'lblCategory', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(56, 13), style=wx.ALIGN_RIGHT)

        self.chcPriv = wx.CheckBox(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELCHCPRIV,
              label=_(u'private car'), name=u'chcPriv', parent=self,
              pos=wx.Point(243, 25), size=wx.Size(124, 13), style=0)
        self.chcPriv.SetValue(True)

        self.chcComp = wx.CheckBox(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELCHCCOMP,
              label=_(u'company car'), name=u'chcComp', parent=self,
              pos=wx.Point(60, 25), size=wx.Size(124, 13), style=0)
        self.chcComp.SetValue(False)

        self.lblMilageStart = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLMILAGESTART,
              label=_(u'start milage'), name=u'lblMilageStart', parent=self,
              pos=wx.Point(0, 42), size=wx.Size(56, 22), style=wx.ALIGN_RIGHT)

        self.lblMilageEnd = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLMILAGEEND,
              label=_(u'end milage'), name=u'lblMilageEnd', parent=self,
              pos=wx.Point(188, 42), size=wx.Size(51, 22),
              style=wx.ALIGN_RIGHT)

        self.viMilageStart = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=0,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGESTART,
              integer_width=6, maximum=1000000, minimum=0.0,
              name=u'viMilageStart', parent=self, pos=wx.Point(60, 42),
              size=wx.Size(124, 22), style=0)
        self.viMilageStart.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViMilageStartVtinputFloatChanged,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGESTART)

        self.lblDistance = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLDISTANCE,
              label=_(u'distance'), name=u'lblDistance', parent=self,
              pos=wx.Point(0, 68), size=wx.Size(56, 22), style=wx.ALIGN_RIGHT)

        self.viMilageEnd = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=0,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGEEND, integer_width=6,
              maximum=1000000, minimum=0.0, name=u'viMilageEnd', parent=self,
              pos=wx.Point(243, 42), size=wx.Size(124, 22), style=0)
        self.viMilageEnd.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViMilageEndVtinputFloatChanged,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGEEND)

        self.lblPass = wx.StaticText(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELLBLPASS,
              label=_(u'passengers'), name=u'lblPass', parent=self,
              pos=wx.Point(0, 98), size=wx.Size(55, 13), style=0)

        self.viDistance = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=0, id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIDISTANCE,
              integer_width=6, maximum=10000, minimum=0.0, name=u'viDistance',
              parent=self, pos=wx.Point(60, 68), size=wx.Size(124, 22),
              style=0)
        self.viDistance.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViDistanceVtinputFloatChanged,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIDISTANCE)

        self.viMilage = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGE,
              name=u'viMilage', parent=self, pos=wx.Point(0, 111),
              size=wx.Size(367, 95))
        self.viMilage.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              self.OnViMilageVtinputGridsetupInfoChanged,
              id=wxID_VXMLNODETRAVELCOSTMILAGEPANELVIMILAGE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vTravelCost')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viLicenseNr.SetTagName('licenseNr')
        self.viMilage.SetAutoStretch(True)
        self.viMilageStart.SetTagName('milageStart')
        self.viMilageEnd.SetTagName('milageEnd')
        self.viDistance.SetTagName('distance')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __calcLimit__(self):
        try:
            fStart=self.viMilageStart.GetValue()
            fEnd=self.viMilageEnd.GetValue()
            self.viDistance.SetLimit(0,min(10000,fEnd-fStart))
        except:
            pass
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viLicenseNr.Clear()
            self.viMilageStart.Clear()
            self.viMilageEnd.Clear()
            self.viDistance.Clear()
            
            self.viMilage.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viMilage.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viLicenseNr.SetDoc(self.doc)
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.viLicenseNr.SetNode(self.node)
            self.viMilageStart.SetNode(self.node,self.doc)
            self.viMilageEnd.SetNode(self.node,self.doc)
            self.viDistance.SetNode(self.node,self.doc)
            
            tup=self.objRegNode.GetValues(node)
            self.viMilage.SetValue(tup[0])
            self.viMilage.SetModified(False)
            
            self.__calcLimit__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viLicenseNr.GetNode(node)
            self.viMilageStart.GetNode(node,self.doc)
            self.viMilageEnd.GetNode(node,self.doc)
            self.viDistance.GetNode(node,self.doc)
            
            tup=self.viMilage.GetValueStr()
            self.objRegNode.SetValues(node,tup[0],tup[1])
            self.viMilage.SetModified(False)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def GetSum(self):
        tup=self.viMilage.GetValueStr()
        return tup[1][-1]
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viLicenseNr.Enable(False)
            self.viMilageStart.Enable(False)
            self.viMilageEnd.Enable(False)
            self.viDistance.Enable(False)
            self.viMilage.Enable(False)
        else:
            # add code here
            self.viLicenseNr.Enable(True)
            self.viMilageStart.Enable(True)
            self.viMilageEnd.Enable(True)
            self.viDistance.Enable(True)
            self.viMilage.Enable(True)
    def BindMilageChanged(self,func):
        self.viMilage.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              func,self.viMilage)

    def OnViMilageVtinputGridsetupInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')

    def OnViMilageStartVtinputFloatChanged(self, event):
        event.Skip()
        try:
            self.__calcLimit__()
        except:
            self.__logTB__()
    def OnViMilageEndVtinputFloatChanged(self, event):
        event.Skip()
        try:
            self.__calcLimit__()
        except:
            self.__logTB__()
    def OnViDistanceVtinputFloatChanged(self, event):
        event.Skip()
