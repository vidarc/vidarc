#----------------------------------------------------------------------------
# Name:         vXmlTravelCost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlTravelCost.py,v 1.2 2007/11/22 12:44:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostRoot import vXmlNodeTravelCostRoot

from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

import vidarc.vApps.vTravelCost.__register__

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xb2IDAT(\x91}R=HBQ\x14\xfe\x94\x0ba\x05\xd5h\xd2\x12Z\xd1\xd0\xf0\xa0\
\xa1\x96\x16\xa1\x86 \xa2\xa2\xc1\xc1\x1cZ\x1a\x1a\x0c^\x199DC\xd4\xe0\xd8"$\
\x05\x0f\xb3\x1f\xa2\xa9\x87I\x8b\xd0`x\xad\xc4\xa4\xff\xe8EMR\x04\xfdry6\
\xdc\xba\xbe^\xd4\xd9\xce\xf9\xcew\xce\xf9\x0e\x9f%JU\x18b6\x11\xc1\xcf\x08\
\xb8}\xc6\xd4\xfa\x0fL\xfd\x8a\xa9\x1b\x00\x11\x83\xa9_\x91B\x1ec\xb7\xa2(\
\x8c1\xaf\xd7\xcb\xeb\x9cl\x15p2\x99\xa4~E\xa4\xdb\xaaz\x97NW3\xc6S\x01}\x11\
\xc2\xe1p\xb1X<8<\x12\xc0N<~\xff\xfa\x9a\xd74Ji6\x9b\x15\x9b--\xf3\x83\x01\
\xb7o\xa0\xc5\xbd\xbe\xbe\xd1\xd9Pu\xf6\xf0&ut\xe7r\xb9L&S14d\xbc\x9eQu6\x11\
\xb1r\x01R\xc8\xd3\xdf\xdf\x97:\xb9v\xd4\xb9\x16#\x91\x97\xe7gS7\x00"u\x95D\
\x03`\x8c\x9d\x16\xf4\xfd\xd5\xdd\xd6\xd6\xe6\xdb\xf6v^\x94..\x1d\x8eZB\x08\
\x80MB\x82r\xacD \x84\xd4T7\x16\xf5\x9aTj\xaf\t\x00\xe0\xdaZ\xd04-\x9f?\xd6u\
\xddf\xb3\xf52&\x85<D\xbc%\x1a\x8d\xbf\xbf9\xed\xa3\r\xf6\xef\x11g=#b\xdc;\
\x00\xc6\xbeN\n\xb8}+k\xab\xd77O\xf8H\x17\xa6\x87\xf52\xa7sb\x1c@/cF\rkG\t\
\x00\x16n\x8d\xdb\xb1\xb9\xfa\xb6\xb6\x93\xcar\xd7\xe4\x14\xfe\x08F\xd5\x92\
\xe8\xc7\xab\xab\x82\xbdV^^\x82<!\x85<A9\x06`f~\xd0H\x08p\xa9<\x99>?\x07`\
\xb4\x06\x80\xa0\x1c3r\xf8[-&\xb7\xe2\xdb\xb0|\xc9\xef\xab\xccn\x15\xf66\xdd\
#4|\x02IP\xc5[\x8cw\xb7\xa2\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlTravelCost(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='travelCostroot'
    APPL_REF=['vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
    def __init__(self,appl='vTravelCost',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                                audit_trail=audit_trail)
            oRoot=vXmlNodeTravelCostRoot('root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeTravelCostRoot()
            self.RegisterNode(oRoot,False)
            
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            
            #vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
            vidarc.vApps.vTravelCost.__register__.RegisterNodes(self,bRegAsRoot=True)
            
            oDataColl=self.GetReg('dataCollector')
            self.LinkRegisteredNode(oRoot,oDataColl)
            
            oTravelCost=self.GetReg('travelCost')
            self.LinkRegisteredNode(oRoot,oTravelCost)
            oTravelCostDepartment=self.GetReg('travelCostDepartment')
            self.LinkRegisteredNode(oRoot,oTravelCostDepartment)
            self.LinkRegisteredNode(oTravelCostDepartment,oDataColl)
            
            oDocs=self.GetReg('Documents')
            self.LinkRegisteredNode(oRoot,oDocs)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'travelCosts')
    #def New(self,revision='1.0',root='travelroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'travelCosts')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtXmlDomReg.Build(self)
        self.BuildStateMachine()
        self.GenerateSortedTravelCostNrIds()
    def BuildStateMachine(self):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
    def BuildSrv(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.BuildStateMachine()
        self.GenerateSortedTravelCostNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def GenerateSortedTravelCostNrIds(self):
        self.travelCostNr=self.getSortedNodeIdsRec([],
                        'travelCost','travelCostID',None)
    def GetSortedTravelCostNrIds(self):
        try:
            return self.travelCostNr
        except:
            self.GenerateSortedTravelCostNrIds()
            return self.travelCostNr
