#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: __register__.py,v 1.5 2008/01/09 09:26:11 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vTravelCost.vXmlNodeTravelCost import vXmlNodeTravelCost
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostDepartment import vXmlNodeTravelCostDepartment
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostItemsPrivate import vXmlNodeTravelCostItemsPrivate
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostItemsCompany import vXmlNodeTravelCostItemsCompany
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostTimes import vXmlNodeTravelCostTimes
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostFellowPassenger import vXmlNodeTravelCostFellowPassenger
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostMilage import vXmlNodeTravelCostMilage
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostAttrML import vXmlNodeTravelCostAttrML
from vidarc.vApps.vTravelCost.vXmlNodeTravelCostDoc import vXmlNodeTravelCostDoc

from vidarc.vApps.vSupplier.vXmlNodeSupplierContact import vXmlNodeSupplierContact
from vidarc.vApps.vSupplier.vXmlNodeSupplierAddr import vXmlNodeSupplierAddr

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

import vidarc.ext.report.__register__
import vidarc.ext.data.__register__

import vidarc.config.vcCust as vcCust
if vcCust.is2Import(__name__):
    vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    import wx
    from vidarc.vApps.vPrj.vXmlPrjInputTree import vXmlPrjInputTree
    from vidarc.vApps.vHum.vXmlHumInputTree import vXmlHumInputTree
    from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
    from vidarc.tool.time.vtTimeDateGM import vtTimeDateGM
    from vidarc.tool.time.vtTime import vtDateTime
    from vidarc.tool.time.vtTime import vtTime
    def funcCmpId(doc,node,val,lMatch):
        if lMatch is not None:
            if val in lMatch:
                return True
            else:
                return False
        return True
    def funcCmpPrjId(node,doc,lMatch,lFound):
        fid,sAppl=doc.getForeignKeyNumAppl(node)
        if sAppl=='vPrj':
            if funcCmpId(doc,node,fid,lMatch):
                lFound[0]=True
                return -1
        return 0
    def funcCmpPrjsId(doc,node,val,lMatch):
        lFound=[False]
        doc.procChildsExt(node,funcCmpPrjId,doc,lMatch,lFound)
        return lFound[0]
    def funcCreateHum(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='human', name='lblHum', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlHumInputTree(id=-1,name=u'%s_viTrHum'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vHum')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
    def funcSetDocHum(obj,viTr,doc,dNetDocs):
        viTr.SetDoc(doc)
        viTr.SetDocTree(dNetDocs['vHum']['doc'],dNetDocs['vHum']['bNet'])
        viTr.SetNodeTree(None)
    def funcGetSelHum(obj,viTr,doc):
        sVal=viTr.GetValue()
        sID=viTr.GetForeignID()
        #sID='@'.join([sID,'vPrj'])
        return sVal,sID
    def funcCreatePrj(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='project', name='lblPrj', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlPrjInputTree(id=-1,name=u'%s_vitrPrj'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vPrj')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
    def funcSetDocPrj(obj,viTr,doc,dNetDocs):
        viTr.SetDoc(doc)
        viTr.SetDocTree(dNetDocs['vPrj']['doc'],dNetDocs['vPrj']['bNet'])
        viTr.SetNodeTree(None)
    def funcGetSelPrj(obj,viTr,doc):
        sVal=viTr.GetValue()
        sID=viTr.GetForeignID()
        #sID='@'.join([sID,'vPrj'])
        return sVal,sID
    def funcCreateDate(obj,sSrc,parent):
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label='project', name='lblPrj', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viDtStart=vtTimeDateGM(id=-1,name=u'%s_viTrDtStart'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viDtStart.SetMinSize((-1,-1))
        bxs.AddWindow(viDtStart, 2, border=4, flag=wx.EXPAND)
        viDtEnd=vtTimeDateGM(id=-1,name=u'%s_viTrDtStart'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viDtEnd.SetMinSize((-1,-1))
        bxs.AddWindow(viDtEnd, 2, border=4, flag=wx.EXPAND)
        return bxs,(viDtStart,viDtEnd)
    def funcSetDocDate(obj,tupDt,doc,dNetDocs):
        viDtStart,viDtEnd=tupDt
    def funcGetSelDate(obj,tupDt,doc):
        viDtStart,viDtEnd=tupDt
        sVal=' '.join([viDtStart.GetValueStr(),viDtEnd.GetValueStr()])
        return sVal,''
    def funcCmpDate(doc,node,sVal,lMatch):
        for sMatch in lMatch:
            strs=sMatch.split(' ')
            z=vtDateTime()
            z.SetDateSmallStr(sVal)
            sDT=z.GetDateStr()
            if sDT>=strs[0]:
                if len(strs[1])>0:
                    if sDT<=strs[1]:
                        return True
                else:
                    return True
        return False
    def convDate(doc,node,sVal,dt,**kwargs):
        try:
            dt.SetDateSmallStr(sVal)
        except:
            pass
        return dt.GetYear(),dt.GetMonth(),dt.GetDay(),dt.GetWeek(),dt.GetDayName()

def funcCalcSumPerPrj(node,doc,fSum,lPer):
        fid,sAppl=doc.getForeignKeyNumAppl(node)
        if sAppl=='vPrj':
            fPer=doc.GetValue(node,'percent',float,0.0)
        else:
            fPer=0.0
        lPer.append('%6.2f'%(fSum*fPer/100.0))
        return 0
def convSumPer(doc,node,sVal,**kwargs):
        lPer=[]
        oReg=doc.GetRegByNode(node)
        fSum=oReg.GetSumFloat(node)
        nodePrjs=doc.getChild(node,'projects')
        doc.procChildsExt(nodePrjs,funcCalcSumPerPrj,doc,fSum,lPer)
        return lPer,
    

def RegisterNodes(self,bRegAsRoot=True):
    try:
        oTravelCostDoc=vXmlNodeTravelCostDoc()
        self.RegisterNode(oTravelCostDoc,False)
        
        oLog=self.GetReg('log',bLogFlt=False)
        if oLog is None:
            oLog=vtXmlNodeLog()
            self.RegisterNode(oLog,False)
        oAttrML=self.GetReg('attrML',bLogFlt=False)
        if oAttrML is None:
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
        vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
        vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterNodes(self,'vTravelCost',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterDateSmall(self,'travelCost','startDtTm',_('start date time'))
        vidarc.ext.data.__register__.RegisterDateSmall(self,'travelCost','endDtTm',_('end date time'))
        oDataColl=self.GetReg('dataCollector')
        
        if vcCust.is2Import(__name__):
            d=oDataColl.GetSrcWidDict()
            #vtLog.CallStack('')
            #vtLog.pprint(d)
            dSrcWid={
                'projects':{'create':funcCreatePrj,
                        'setDoc':funcSetDocPrj,
                        'getSel':funcGetSelPrj,
                        'translation':_('project module'),
                        'match':funcCmpPrjsId},
                'vHum':{'create':funcCreateHum,
                        'setDoc':funcSetDocHum,
                        'getSel':funcGetSelHum,
                        'translation':_('human module'),
                        'match':funcCmpId},
                }
            oDataColl.UpdateSrcWidDict(dSrcWid)
            d=oDataColl.GetSrcWidDict()
            #vtLog.CallStack('')
            #vtLog.pprint(d)
        else:
            dSrcWid={}
        dataProc={'':({'conv':convSumPer,
                        'args':(),
                        'kwargs':{},
                        'values':['sumPercentage'],
                        },)
                    }
        oDataColl.UpdateDataProcDict('travelCost',dataProc)
        
        oTravelCost=vXmlNodeTravelCost(dSrcWid=dSrcWid,srcAppl='vTravelCost',dataProc=dataProc)
        oTravelCostDepartment=vXmlNodeTravelCostDepartment()
        oTravelCostItemsPriv=vXmlNodeTravelCostItemsPrivate()
        oTravelCostItemsComp=vXmlNodeTravelCostItemsCompany()
        oTravelCostTimes=vXmlNodeTravelCostTimes()
        oTravelCostFellowPassenger=vXmlNodeTravelCostFellowPassenger()
        oTravelCostMilage=vXmlNodeTravelCostMilage()
        oTravelCostAttr=vXmlNodeTravelCostAttrML()
        oContacts=vXmlNodeSupplierContact()
        oAddrs=vXmlNodeSupplierAddr()
        self.RegisterNode(oTravelCost,True)
        self.RegisterNode(oTravelCostDepartment,True)
        self.RegisterNode(oTravelCostItemsPriv,False)
        self.RegisterNode(oTravelCostItemsComp,False)
        self.RegisterNode(oTravelCostTimes,False)
        self.RegisterNode(oTravelCostFellowPassenger,False)
        self.RegisterNode(oTravelCostMilage,False)
        self.RegisterNode(oTravelCostAttr,False)
        self.RegisterNode(oContacts,False)
        self.RegisterNode(oAddrs,False)
        
        self.LinkRegisteredNode(oTravelCostDepartment,oTravelCost)
        self.LinkRegisteredNode(oTravelCost,oTravelCostItemsPriv)
        self.LinkRegisteredNode(oTravelCost,oTravelCostItemsComp)
        self.LinkRegisteredNode(oTravelCost,oTravelCostTimes)
        self.LinkRegisteredNode(oTravelCost,oTravelCostFellowPassenger)
        self.LinkRegisteredNode(oTravelCost,oTravelCostMilage)
        self.LinkRegisteredNode(oTravelCost,oTravelCostAttr)
        self.LinkRegisteredNode(oTravelCost,oLog)
        self.LinkRegisteredNode(oTravelCost,oTravelCostDoc)
        
        oDocs=self.GetReg('Documents',bLogFlt=False)
        self.LinkRegisteredNode(oTravelCostDepartment,oDocs)
        
        flt={None:[('GetSum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                    ('startDtTm',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ('endDtTm',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ]}
        oTransPur=self.GetReg('transTravelCost',bLogFlt=False)
        if oTransPur is None:
            oTransPur=veStateTransition(tagName='transTravelCost',filter=flt)
            self.RegisterNode(oTransPur,False)
        oSMtravelCost=self.GetReg('smTravelCost',bLogFlt=False)
        if oSMtravelCost is None:
            oSMtravelCost=veStateStateMachine(tagName='smTravelCost',
                    tagNameState='state',tagNameTrans='transTravelCost',
                    filter=flt,
                    trans={None:{None:_(u'travel cost'),
                            'GetSum':_(u'sum'),
                            'startDtTm':_(u'start date time'),
                            'endDtTm':_(u'end date time'),
                            } },
                    cmd=oTravelCost.GetActionCmdDict())
        oStateMachineLinker=self.GetReg('stateMachineLinker',bLogFlt=False)
        if oStateMachineLinker is not None:
            oStateMachineLinker.AddPossible({
                    'travelCost':[('trvCostState','smTravelCost')],
                    })
            self.RegisterNode(oSMtravelCost,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMtravelCost)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMtravelCost,oState)
        else:
            vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)
    except:
        vtLog.vtLngTB(__name__)
