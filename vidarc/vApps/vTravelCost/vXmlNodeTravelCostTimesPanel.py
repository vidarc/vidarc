#Boa:FramePanel:vXmlNodeTravelCostTimesPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelCostTimesPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20071126
# CVS-ID:       $Id: vXmlNodeTravelCostTimesPanel.py,v 1.2 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODETRAVELCOSTTIMESPANEL, wxID_VXMLNODETRAVELCOSTTIMESPANELVIITEMS, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vXmlNodeTravelCostTimesPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viItems, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODETRAVELCOSTTIMESPANEL,
              name=u'vXmlNodeTravelCostItemsPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.viItems = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODETRAVELCOSTTIMESPANELVIITEMS,
              name=u'viItems', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(304, 180))
        self.viItems.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              self.OnViItemsVtinputGridsetupInfoChanged,
              id=wxID_VXMLNODETRAVELCOSTTIMESPANELVIITEMS)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vTravelCost')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viItems.SetAutoStretch(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viItems.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viItems.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            #self.docHum=dd['doc']
        # add code here
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            tup=self.objRegNode.GetValues(node)
            self.viItems.SetValue(tup[0])
            self.viItems.SetModified(False)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            tup=self.viItems.GetValueStr()
            self.objRegNode.SetValues(node,tup[0],tup[1])
            self.viItems.SetModified(False)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def GetSum(self):
        tup=self.viItems.GetValueStr()
        return tup[1][-1]
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        pass
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        pass
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viItems.Enable(False)
        else:
            # add code here
            self.viItems.Enable(True)
    def BindItemsChanged(self,func):
        self.viItems.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              func,self.viItems)

    def OnViItemsVtinputGridsetupInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
