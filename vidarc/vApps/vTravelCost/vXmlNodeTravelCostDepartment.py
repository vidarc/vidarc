#----------------------------------------------------------------------------
# Name:         vXmlNodeTravelCostDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeTravelCostDepartment.py,v 1.2 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeTravelCostDepartmentPanel import vXmlNodeTravelCostDepartmentPanel
        #from vidarc.vApps.vTravelCost.vXmlNodeTravelCostDepartmentEditDialog import *
        #from vidarc.vApps.vTravelCost.vXmlNodeTravelCostDepartmentAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeTravelCostDepartment(vtXmlNodeTag):
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='travelCostDepartment'):
        global _
        _=vtLgBase.assignPluginLang('vTravelCost')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'travel cost department')
    # ---------------------------------------------------------
    # specific
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smTravelCost')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe4IDAT(\x91\x95R!\x0e\x83@\x10\x1c\x9a\x13|\xa0\x82\xf0\x80\n\x1c\xaa\
\xa9"\xc1PSK\x82\xe3\x11\x08\x04\xc9%\x15\x15}D\x1d\xc9\xbd\x80\x9a&\xab\x90\
u}B\x15\x0e]Q\xb1\xe4\x80\x03\xda0\xe6n/3;s{g\x11\x11\xd6@\x00X\xa5\x11\xbc\
\xecN\xfb\xbfT\xa7\xb5\x89H\xe8\xfa\xf2\xb8MIy\x98\x1a\'\x9b\xdf]\xa7]\xc4,\
\x0f@\x95\xa8\xda\xe5\xad\xea\x8e\x88\xe6\x1d\xaaD\r\xd8=\x9a \x98w8\x96q\
\x91u]\xcf\xd7\x987y\x98\n?\xf2\xa4\\\x8c\x04\xe0\xf0\x06\xca\xbe\xdc\x12\
\x8d\xa6\xc4\xd0\xbd\x01\xd4.\n}\x01(\xbe\x83)\xe0\x0c,\xd3y8\x12Z\x8c\x04<r\
\xe1G\xb3n\x8b\x0e\x9f\xe7]\xcb\xd8\xa1\x7f;\xc3a\x8a"S\xacg4A\xe0i\x81\xd3\
\xda#.Q7\xf5AB\x00/)-"Z\xfa\xad\x9e\x94\x06\x1b\xc0\x17s\xa1PZ(\x9bv/\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClassOld(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnName':'pn%s'%self.tagName[8:]}
        else:
            return None
    def GetAddDialogClassOld(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnName':'pn%s'%self.tagName[8:]}
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeTravelCostDepartmentPanel
        else:
            return None
