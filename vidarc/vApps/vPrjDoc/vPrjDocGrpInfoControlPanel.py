#Boa:FramePanel:vPrjDocGrpInfoControlPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocGrpInfoControlPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocGrpInfoControlPanel.py,v 1.4 2007/01/14 14:31:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.grid
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import string,sys

from vidarc.vApps.vPrjDoc.vPrjDocGrpInfoPanel import *
#import vidarc.vApps.vPrj.vtXmlPrjTree as vtXmlPrjTree

[wxID_VPRJDOCGRPINFOCONTROLPANEL, wxID_VPRJDOCGRPINFOCONTROLPANELLBLKEY, 
 wxID_VPRJDOCGRPINFOCONTROLPANELLBLPATH, 
 wxID_VPRJDOCGRPINFOCONTROLPANELLBLREFERENCES, 
 wxID_VPRJDOCGRPINFOCONTROLPANELLSTREF, wxID_VPRJDOCGRPINFOCONTROLPANELTXTID, 
 wxID_VPRJDOCGRPINFOCONTROLPANELTXTPATH, 
] = [wx.NewId() for _init_ctrls in range(7)]

VERBOSE=0

class vPrjDocGrpInfoControlPanel(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsKey = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_bxsKey_Items(self.bxsKey)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)

        self.SetSizer(self.fgsData)


    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsKey, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblReferences, 0, border=4,
              flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lstRef, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsKey_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblKey, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtId, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblPath, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.txtPath, 4, border=4, flag=wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCGRPINFOCONTROLPANEL,
              name=u'vPrjDocGrpInfoControlPanel', parent=prnt, pos=wx.Point(75,
              13), size=wx.Size(366, 189), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(358, 162))

        self.lblKey = wx.StaticText(id=wxID_VPRJDOCGRPINFOCONTROLPANELLBLKEY,
              label=u'Key', name=u'lblKey', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(47, 21), style=wx.ALIGN_RIGHT)
        self.lblKey.SetMinSize(wx.Size(-1, -1))

        self.txtId = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOCONTROLPANELTXTID,
              name=u'txtId', parent=self, pos=wx.Point(51, 0), size=wx.Size(47,
              21), style=wx.TE_RICH2, value=u'')
        self.txtId.Enable(False)
        self.txtId.SetEditable(False)
        self.txtId.SetMinSize(wx.Size(-1, -1))
        self.txtId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VPRJDOCGRPINFOCONTROLPANELTXTID)

        self.lblPath = wx.StaticText(id=wxID_VPRJDOCGRPINFOCONTROLPANELLBLPATH,
              label=u'Path', name=u'lblPath', parent=self, pos=wx.Point(106, 0),
              size=wx.Size(43, 21), style=wx.ALIGN_RIGHT)
        self.lblPath.SetMinSize(wx.Size(-1, -1))

        self.txtPath = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOCONTROLPANELTXTPATH,
              name=u'txtPath', parent=self, pos=wx.Point(153, 0),
              size=wx.Size(204, 21), style=0, value=u'')
        self.txtPath.Enable(False)
        self.txtPath.SetMinSize(wx.Size(-1, -1))

        self.lstRef = wx.ListCtrl(id=wxID_VPRJDOCGRPINFOCONTROLPANELLSTREF,
              name=u'lstRef', parent=self, pos=wx.Point(0, 38),
              size=wx.Size(358, 124), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstRef.SetMinSize(wx.Size(-1, -1))
        self.lstRef.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRefListItemSelected,
              id=wxID_VPRJDOCGRPINFOCONTROLPANELLSTREF)

        self.lblReferences = wx.StaticText(id=wxID_VPRJDOCGRPINFOCONTROLPANELLBLREFERENCES,
              label=u'References', name=u'lblReferences', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(358, 13), style=0)
        self.lblReferences.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.ids=None
        self.selectedIdx=-1
        self.refs={}
        self.delNode=[]
        self.verbose=VERBOSE
        
        # setup list
        self.lstRef.InsertColumn(0,u'Company')
        self.lstRef.InsertColumn(1,u'Number',wx.LIST_FORMAT_LEFT,70)
    def Clear(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.txtId.SetValue('')
        self.txtPath.SetValue('')
        self.lstRef.DeleteAllItems()
        self.node=None
        self.selectedIdx=-1
        self.refs={}
        self.delNode=[]
        
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.refs={}
        self.delNode=[]
        self.lstRef.DeleteAllItems()
        self.selectedIdx=-1
        if node is None:
            self.txtId.SetValue('')
            self.txtPath.SetValue('')
            return
        self.node=node
        
        sPath=self.doc.getNodeText(node,'path')
        #sAttrId=self.doc.getAttribute(node,'fid')
        oReg=self.doc.GetRegByNode(node)
        sAttrId=oReg.GetKey(node)
        
        self.txtPath.SetValue(sPath)
        self.txtId.SetValue(sAttrId)
        
        refNodes=self.doc.getChilds(node,'reference')
        for refNode in refNodes:
            sComp=self.doc.getAttribute(refNode,'company')
            sVal=self.doc.getText(refNode)
            try:
                lst=self.refs[sComp]
                lst.append(sVal)
            except:
                self.refs[sComp]=[(sVal,refNode)]
            index = self.lstRef.InsertImageStringItem(sys.maxint, sComp, -1)
            self.lstRef.SetStringItem(index, 1, sVal)
        pass
    def GetNode(self,node):
        if self.doc is None:
            return
        if node is None:
            return ''
        return
        sPath=self.txtPath.GetValue()
        sAttrId=self.txtId.GetValue()
        ids=self.doc.getIds()
        if ids is not None:
            sOldId=self.doc.getAttribute(node,'id')
            ids.RemoveId(sOldId)
            ret,n=self.doc.GetIdStr(sAttrId)
            if ret<0:
                bOk=False
            else:
                if n is None:
                    bOk=True
                else:
                    if n==node:
                        bOk=True
                    else:
                        bOk=False
            if bOk==False:
                # fault id is not possible
                ids.AddNewNode(node)
                sAttr=ids.ConvertId2Str(self.node)
                self.txtId.SetValue(sAttr)
                self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            else:
                self.doc.setAttribute(node,'id',sAttrId)
                sAttr=ids.ConvertId2Str(self.node)
                self.txtId.SetValue(sAttr)
                self.doc.setAttribute(node,'id',sAttr)
                self.doc.checkId(node)
                self.doc.clearMissing()
        
        self.doc.setNodeText(node,'path',sPath)
        keys=self.refs.keys()
        keys.sort()
        for k in keys:
            vals=self.refs[k]
            for v,n in vals:
                self.doc.setNodeTextAttr(node,'reference',v,'company',k)
        for n in self.delNode:
            self.doc.deleteNode(n)
    def CreateNode(self):
        pass
    def OnGcbbApplyButton(self, event):
        #self.GetNode(self.node)
        #wx.PostEvent(self,vgpProjectInfoChanged(self,self.node))
        wx.PostEvent(self,vgpDocGrpInfoChanged(self,self.node,None))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        #self.SetNode(self.node,self.ids)
        wx.PostEvent(self,vgpDocGrpInfoCanceled(self,self.node,None))
        event.Skip()

    def OnGcbbDelButton(self, event):
        #self.SetNode(self.node,self.ids)
        wx.PostEvent(self,vgpDocGrpInfoDeleted(self,self.node,None))
        event.Skip()

    def OnGcbbDelRefButton(self, event):
        #self.SetNode(self.node,self.ids)
        if self.selectedIdx>=0:
            it=self.lstRef.GetItem(self.selectedIdx,0)
            sComp=it.m_text
            it=self.lstRef.GetItem(self.selectedIdx,1)
            sVal=it.m_text
            vals=self.refs[sComp]
            for v,n in vals:
                if v==sVal:
                    self.delNode.append(n)
            self.lstRef.DeleteItem(self.selectedIdx)
        #wx.PostEvent(self,vgpDocGrpInfoDeleted(self,self.node,None))
        event.Skip()

    def OnGcbbNewButton(self, event):
        #self.SetNode(self.node,self.ids)
        wx.PostEvent(self,vgpDocGrpInfoAdded(self,self.node,None))
        event.Skip()

    def OnGcbbAddButton(self, event):
        sComp=self.txtRefComp.GetValue()
        sVal=self.txtRefNumber.GetValue()
        i=self.lstRef.FindItem(-1,sComp,False)
        if i<0:
            self.txtRefComp.SetStyle(0, len(sComp), wx.TextAttr("BLACK", "WHITE"))
            try:
                lst=self.refs[sComp]
                lst.append((sVal,None))
            except:
                self.refs[sComp]=[(sVal,None)]
            index = self.lstRef.InsertImageStringItem(sys.maxint, sComp, -1)
            self.lstRef.SetStringItem(index, 1, sVal)
            self.txtRefComp.SetValue('')
            self.txtRefNumber.SetValue('')
        else:
            self.txtRefComp.SetStyle(0, len(sComp), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnTxtIdText(self, event):
        if self.ids is None:
            return
        ids=self.doc.getIds()
        sAttr=self.txtId.GetValue()
        ret,n=self.doc.GetIdStr(sAttr)
        if ret<0:
            ids.AddNewNode(self.node)
            sId=ids.ConvertId2Str(self.node)
            self.txtId.SetValue(sId)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            return
        if n is None:
            bOk=True
        else:
            if n==self.node:
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnLstRefListItemSelected(self, event):
        idx=event.GetIndex()
        self.selectedIdx=idx
        event.Skip()
