#Boa:Dialog:vGenerateArchTmplDialog
#----------------------------------------------------------------------------
# Name:         vGenerateArchTmplDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vGenerateArchTmplDialog.py,v 1.4 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string,os,os.path,shutil
import vidarc.tool.InOut.fnUtil as fnUtil
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
from vidarc.tool.office.vtOfficeSCalc import *
from vidarc.tool.office.vtOfficeSWriter import *

#from boa.vApps.vPrjDoc.vgpAddDoc import *
#from boa.vApps.vPrjDoc.vglbPrjDocGrpInfo import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase


def create(parent):
    return vGenerateArchTmplDialog(parent)

[wxID_VGENERATEARCHTMPLDIALOG, wxID_VGENERATEARCHTMPLDIALOGGCBBBROWSEDIR, 
 wxID_VGENERATEARCHTMPLDIALOGGCBBCANCEL, wxID_VGENERATEARCHTMPLDIALOGGCBBOK, 
 wxID_VGENERATEARCHTMPLDIALOGLBLDIR, wxID_VGENERATEARCHTMPLDIALOGLBLFN, 
 wxID_VGENERATEARCHTMPLDIALOGLBLSIZE, wxID_VGENERATEARCHTMPLDIALOGLSTFILE, 
 wxID_VGENERATEARCHTMPLDIALOGTXTDIR, wxID_VGENERATEARCHTMPLDIALOGTXTFN, 
 wxID_VGENERATEARCHTMPLDIALOGTXTSIZE, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vGenerateArchTmplDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFN, 7, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFN, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsDir, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lstFile, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=8,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDir, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtDir, 3, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbBrowseDir, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblSize, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtSize, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_bxsDir_Items(self.bxsDir)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGENERATEARCHTMPLDIALOG,
              name=u'vGenerateArchTmplDialog', parent=prnt, pos=wx.Point(222,
              118), size=wx.Size(493, 356),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vPrjDoc Archive Template Generate')
        self.SetClientSize(wx.Size(485, 329))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHTMPLDIALOGGCBBOK,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Ok'),
              name=u'gcbbOk', parent=self, pos=wx.Point(150, 291),
              size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGENERATEARCHTMPLDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHTMPLDIALOGGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(258, 291),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGENERATEARCHTMPLDIALOGGCBBCANCEL)

        self.lstFile = wx.ListCtrl(id=wxID_VGENERATEARCHTMPLDIALOGLSTFILE,
              name=u'lstFile', parent=self, pos=wx.Point(4, 63),
              size=wx.Size(477, 220), style=wx.LC_REPORT)

        self.lblDir = wx.StaticText(id=wxID_VGENERATEARCHTMPLDIALOGLBLDIR,
              label=_(u'Directory'), name=u'lblDir', parent=self,
              pos=wx.Point(4, 29), size=wx.Size(59, 30), style=wx.ALIGN_RIGHT)

        self.txtDir = wx.TextCtrl(id=wxID_VGENERATEARCHTMPLDIALOGTXTDIR,
              name=u'txtDir', parent=self, pos=wx.Point(67, 29),
              size=wx.Size(174, 30), style=0, value=u'')

        self.gcbbBrowseDir = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHTMPLDIALOGGCBBBROWSEDIR,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=_(u'Browse Dir'),
              name=u'gcbbBrowseDir', parent=self, pos=wx.Point(245, 29),
              size=wx.Size(115, 30), style=0)
        self.gcbbBrowseDir.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseDirButton,
              id=wxID_VGENERATEARCHTMPLDIALOGGCBBBROWSEDIR)

        self.lblFN = wx.StaticText(id=wxID_VGENERATEARCHTMPLDIALOGLBLFN,
              label=_(u'Filename'), name=u'lblFN', parent=self, pos=wx.Point(4,
              4), size=wx.Size(59, 21), style=wx.ALIGN_RIGHT)

        self.txtFN = wx.TextCtrl(id=wxID_VGENERATEARCHTMPLDIALOGTXTFN,
              name=u'txtFN', parent=self, pos=wx.Point(67, 4), size=wx.Size(413,
              21), style=0, value=u'')
        self.txtFN.Enable(False)

        self.lblSize = wx.StaticText(id=wxID_VGENERATEARCHTMPLDIALOGLBLSIZE,
              label=_(u'Size'), name=u'lblSize', parent=self, pos=wx.Point(364,
              29), size=wx.Size(55, 30), style=wx.ALIGN_RIGHT)

        self.txtSize = wx.TextCtrl(id=wxID_VGENERATEARCHTMPLDIALOGTXTSIZE,
              name=u'txtSize', parent=self, pos=wx.Point(423, 29),
              size=wx.Size(55, 30), style=0, value=u'')

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.attr={}
        self.docinfos={}
        self.prjinfos={}
        self.doc=None
        self.tmplFN=None
        self.sFullName=None
        self.sDoc=''
        self.sRev=''
        self.selectedIdx=-1
        
        #img=images.getBuildBitmap()
        #self.gcbbOk.SetBitmapLabel(img)
        
        #img=images.getBrowseBitmap()
        #self.gcbbBrowseDir.SetBitmapLabel(img)
        
        #img=images.getCancelBitmap()
        #self.gcbbCancel.SetBitmapLabel(img)
        
        # setup list
        self.lstFile.InsertColumn(0,_(u'Filename'),wx.LIST_FORMAT_LEFT,240)
        self.lstFile.InsertColumn(1,_(u'Size'),wx.LIST_FORMAT_RIGHT,90)
        self.lstFile.InsertColumn(2,_(u'Date'),wx.LIST_FORMAT_LEFT,80)
        
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def SetFileNames(self,tmplFN,FN):
        self.tmplFN=tmplFN
        self.sFullName=FN
    def __getInfo__(self,info):
        s=string.split(info,':')
        val='---'
        if len(s)>1:
            if s[0]==u'prj':
                try:
                    sInfo=string.split(s[1],'|')
                    if len(sInfo)>1:
                        k=sInfo[0]
                        f=sInfo[1]
                    else:
                        k=s[1]
                        f=None
                    val=self.prjinfos[k]
                    if f is not None:
                        pass
                except:
                    val='---'
        else:
            try:
                sInfo=string.split(info,'|')
                if len(sInfo)>1:
                    k=sInfo[0]
                    f=sInfo[1]
                else:
                    k=info
                    f=None
                val=self.docinfos[k]
                if f is not None:
                    val=val.Format(f)
            except:
                val='---'
            #elif info==u'Date':
            #    dt=wx.DateTime.ToDay()
            #    val=dt.Format('%Y%m%D')
            
        return val
    def SetDoc(self,doc):
        self.doc=doc
    def Set(self,archNode,fn,prjDN):
        self.archDN=''
        self.sFN=fn
        self.prjDN=prjDN
        fn=fnUtil.getFilenameRel2BaseDir(fn,prjDN)
        self.txtFN.SetValue(fn)
        #self.sFullName=fn
        self.lstFile.DeleteAllItems()
        self.archNode=archNode
        self.txtSize.SetValue('0.00 MB')
    def SetDir(self,dn):
        self.archDN=dn
        self.txtDir.SetValue(dn)
        lstFiles=[]
        self.subDN=os.path.split(dn)[0]
        self.lstFile.DeleteAllItems()
        iOfs=len(self.subDN)
        iSize=0
        for root,dirs,files in os.walk(dn):
            for sfn in files:
                fn=root+os.path.sep+sfn
                index = self.lstFile.InsertImageStringItem(sys.maxint, '.'+fn[iOfs:], -1)
                mode=os.stat(fn)
                iSize=iSize+mode.st_size
                size="%10.2f kB"%(mode.st_size/1024.0)
                self.lstFile.SetStringItem(index, 1, size)
        size="%10.2f MB"%(iSize/(1024.0*1024.0))
        self.txtSize.SetValue(size)
            #print root, "consumes",
            #print dirs
            #print files
            #print sum(getsize(join(root, name)) for name in files),
            #print "bytes in", len(files), "non-directory files"
    
        pass
            
    def Generate(self):
        self.doc.setNodeText(self.archNode,'directory',self.txtDir.GetValue())
        
        pass
    def OnGcbbOkButton(self, event):
        self.Generate()
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnGcbbBrowseDirButton(self,event):
        dlg = wx.DirDialog(self, _(u"Choose the directory to archive:"),
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.archDN is not None:
                dlg.SetPath(self.archDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetDir(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        
        event.Skip()
