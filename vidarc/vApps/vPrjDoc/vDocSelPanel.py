#Boa:FramePanel:vDocSelPanel
#----------------------------------------------------------------------------
# Name:         vDocSelPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDocSelPanel.py,v 1.12 2014/10/01 09:02:28 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.xml.vtXmlTree import *
import vidarc.vApps.vDoc.vXmlDocTree
import vidarc.vApps.vDoc.vXmlDocInputTree
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import wx.lib.anchors as anchors
from vidarc.vApps.vDoc.vXmlDocTree import *
import sys,string,os,os.path,shutil
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjDoc.vGenerateDocDialog import *
from vidarc.vApps.vPrjDoc.vGenerateArchTmplDialog import *
from vidarc.vApps.vPrjDoc.vAddFileDialog import vAddFileDialog
import vidarc.tool.sec.vtSecFileAcl as vtSecFileAcl
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import images

MODES=['complete','last','flat','major']

[wxID_VDOCSELPANEL, wxID_VDOCSELPANELCBADDBROWSE, wxID_VDOCSELPANELCBOPENDIR, 
 wxID_VDOCSELPANELCHCLSTLEVEL, wxID_VDOCSELPANELGCBBADD, 
 wxID_VDOCSELPANELGCBBARC, wxID_VDOCSELPANELGCBBGENERATE, 
 wxID_VDOCSELPANELLBLDOCGROUP, wxID_VDOCSELPANELLBLEXT, 
 wxID_VDOCSELPANELLBLLEVELS, wxID_VDOCSELPANELLBLREV, 
 wxID_VDOCSELPANELLBLTITLE, wxID_VDOCSELPANELLBLTMPL, 
 wxID_VDOCSELPANELLSTDOCS, wxID_VDOCSELPANELTXTEXT, wxID_VDOCSELPANELTXTREV, 
 wxID_VDOCSELPANELTXTTITLE, wxID_VDOCSELPANELVGPTREE, 
] = [wx.NewId() for _init_ctrls in range(18)]

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_SEL_DOC_ADDED=wx.NewEventType()
def EVT_VGPDOC_SEL_DOC_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDOC_SEL_DOC_ADDED,func)
class vgpDocSelDocAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPDOC_SEL_DOC_ADDED(<widget_name>, self.OnDocAdded)
    """

    def __init__(self,obj,node,docNode,fn):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDOC_SEL_DOC_ADDED)
        self.obj=obj
        self.node=node
        self.docNode=docNode
        self.fn=fn
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetDocNode(self):
        return self.docNode
    def GetFN(self):
        return self.fn

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

class vDocSelPanel(wx.Panel):
    def _init_coll_bxsRev_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRev, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtRev, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTmpl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstDocs, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsTitleFull, 0, border=0, flag=0)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)

    def _init_coll_bxsTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=0,
              flag=wx.ALIGN_BOTTOM | wx.EXPAND)
        parent.AddWindow(self.txtTitle, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsExt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblExt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtExt, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDocGroup, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblTmpl, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblLevels, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.vgpTree, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsTmpl, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsLv, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsLv_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcLstLevel, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.ALIGN_CENTER)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOpenDir, 0, border=4, flag=wx.BOTTOM | wx.TOP)
        parent.AddWindow(self.gcbbAdd, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbAddBrowse, 0, border=4, flag=wx.BOTTOM | wx.TOP)
        parent.AddWindow(self.gcbbArc, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.gcbbGenerate, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsTitleFull_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTitle, 3, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsRev, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsExt, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=3, hgap=0, rows=2, vgap=0)

        self.bxsTitleFull = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTitle = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsRev = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsExt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsTmpl = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsLv = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTitleFull_Items(self.bxsTitleFull)
        self._init_coll_bxsTitle_Items(self.bxsTitle)
        self._init_coll_bxsRev_Items(self.bxsRev)
        self._init_coll_bxsExt_Items(self.bxsExt)
        self._init_coll_bxsTmpl_Items(self.bxsTmpl)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLv_Items(self.bxsLv)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCSELPANEL, name=u'vDocSelPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(590, 280),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(582, 253))
        self.SetAutoLayout(True)

        self.lblDocGroup = wx.StaticText(id=wxID_VDOCSELPANELLBLDOCGROUP,
              label=_(u'Document Group'), name=u'lblDocGroup', parent=self,
              pos=wx.Point(4, 0), size=wx.Size(192, 13), style=0)

        self.lstDocs = wx.ListCtrl(id=wxID_VDOCSELPANELLSTDOCS, name=u'lstDocs',
              parent=self, pos=wx.Point(200, 13), size=wx.Size(264, 198),
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstDocs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstDocsListItemSelected, id=wxID_VDOCSELPANELLSTDOCS)

        self.lblTitle = wx.StaticText(id=wxID_VDOCSELPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self,
              pos=wx.Point(200, 211), size=wx.Size(156, 21), style=0)

        self.txtTitle = wx.TextCtrl(id=wxID_VDOCSELPANELTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(200, 232),
              size=wx.Size(156, 21), style=wx.TE_RICH2, value=u'')

        self.lblRev = wx.StaticText(id=wxID_VDOCSELPANELLBLREV,
              label=_(u'Revision'), name=u'lblRev', parent=self,
              pos=wx.Point(356, 211), size=wx.Size(52, 21), style=0)

        self.txtRev = wx.TextCtrl(id=wxID_VDOCSELPANELTXTREV, name=u'txtRev',
              parent=self, pos=wx.Point(356, 232), size=wx.Size(52, 21),
              style=wx.TE_RICH2, value=u'')

        self.lblExt = wx.StaticText(id=wxID_VDOCSELPANELLBLEXT,
              label=_(u'Extension'), name=u'lblExt', parent=self,
              pos=wx.Point(408, 211), size=wx.Size(52, 21), style=0)

        self.txtExt = wx.TextCtrl(id=wxID_VDOCSELPANELTXTEXT, name=u'txtExt',
              parent=self, pos=wx.Point(408, 232), size=wx.Size(52, 21),
              style=wx.TE_RICH2, value=u'')

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELPANELGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Generate'),
              name=u'gcbbGenerate', parent=self, pos=wx.Point(485, 223),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.SetMinSize(wx.Size(-1, -1))
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VDOCSELPANELGCBBGENERATE)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Add'), name=u'gcbbAdd',
              parent=self, pos=wx.Point(485, 117), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.SetMinSize(wx.Size(-1, -1))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VDOCSELPANELGCBBADD)

        self.gcbbArc = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELPANELGCBBARC,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Archive'),
              name=u'gcbbArc', parent=self, pos=wx.Point(485, 189),
              size=wx.Size(76, 30), style=0)
        self.gcbbArc.SetMinSize(wx.Size(-1, -1))
        self.gcbbArc.Bind(wx.EVT_BUTTON, self.OnGcbbArchButton,
              id=wxID_VDOCSELPANELGCBBARC)

        self.lblTmpl = wx.StaticText(id=wxID_VDOCSELPANELLBLTMPL,
              label=_(u'Templates'), name=u'lblTmpl', parent=self,
              pos=wx.Point(200, 0), size=wx.Size(264, 13), style=0)

        self.lblLevels = wx.StaticText(id=wxID_VDOCSELPANELLBLLEVELS,
              label=_(u'Levels'), name=u'lblLevels', parent=self,
              pos=wx.Point(468, 0), size=wx.Size(114, 13), style=0)

        self.chcLstLevel = wx.CheckListBox(choices=[],
              id=wxID_VDOCSELPANELCHCLSTLEVEL, name=u'chcLstLevel', parent=self,
              pos=wx.Point(468, 13), size=wx.Size(110, 62), style=0)

        self.vgpTree = vidarc.vApps.vDoc.vXmlDocTree.vXmlDocTree(controller=True,
              id=wxID_VDOCSELPANELVGPTREE, master=False, name=u'vgpTree',
              parent=self, pos=wx.Point(4, 13), size=wx.Size(192, 240),
              style=0)
        self.vgpTree.Bind(vEVT_VTXMLTREE_ITEM_SELECTED, self.OnTreeItemSel,
              id=wxID_VDOCSELPANELVGPTREE)

        self.cbOpenDir = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELPANELCBOPENDIR,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Directory'),
              name=u'cbOpenDir', parent=self, pos=wx.Point(485, 79),
              size=wx.Size(76, 30), style=0)
        self.cbOpenDir.SetMinSize(wx.Size(-1, -1))
        self.cbOpenDir.Bind(wx.EVT_BUTTON, self.OnCbOpenDirButton,
              id=wxID_VDOCSELPANELCBOPENDIR)

        self.cbAddBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCSELPANELCBADDBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Browse'),
              name=u'cbAddBrowse', parent=self, pos=wx.Point(485, 151),
              size=wx.Size(76, 30), style=0)
        self.cbAddBrowse.SetMinSize(wx.Size(-1, -1))
        self.cbAddBrowse.Bind(wx.EVT_BUTTON, self.OnCbAddBrowseButton,
              id=wxID_VDOCSELPANELCBADDBROWSE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.lang='en'
        self.docs=[]
        self.selectedIdx=-1
        self.docNode=None
        self.docPrj=None
        self.sFN=None
        self.dlgGen=None
        self.dlgGenArchTmpl=None
        self.dlgAddFile=None
        self.baseTmplDN=''
        self.prjDN=''
        self.bReplBaseDN=False
        self.baseDN='.'
        self.prjDN='.'
        self.bReplPrjDN=False
        self.sPrjShortCut=''
        self.sPrjClient=''
        self.prjinfos={}
        self.docinfos={}
        self.SetMode('complete')
        
        #self.vgpTree=vXmlDocTree(self,wx.NewId(),
        #        pos=(0,20),size=(200,202),style=0,name="vgpTree",
        #        controller=True,verbose=0)
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        #EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #self.vgpTree.SetLanguages(self.languages,self.languageIds)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getArchBitmap()
        self.gcbbArc.SetBitmapLabel(img)
        
        self.cbOpenDir.SetBitmapLabel(vtArt.getBitmap(vtArt.Browse))
        self.cbAddBrowse.SetBitmapLabel(vtArt.getBitmap(vtArt.Drop))
        # setup list
        self.lstDocs.InsertColumn(0,_(u'Doc'))
        self.lstDocs.InsertColumn(1,_(u'Filename'),wx.LIST_FORMAT_LEFT,270)
        
        dt = vFileDropTarget(self)
        self.cbAddBrowse.SetDropTarget(dt)
        
    def Lock(self,flag):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if flag:
            self.vgpTree.Enable(False)
            self.lstDocs.Enable(False)
            self.txtTitle.Enable(False)
            self.txtRev.Enable(False)
            self.txtExt.Enable(False)
            self.gcbbGenerate.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbArc.Enable(False)
            self.cbOpenDir.Enable(False)
            self.cbAddBrowse.Enable(False)
            self.chcLstLevel.Enable(False)
        else:
            self.vgpTree.Enable(True)
            self.lstDocs.Enable(True)
            self.txtTitle.Enable(True)
            self.txtRev.Enable(True)
            self.txtExt.Enable(True)
            self.gcbbGenerate.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbArc.Enable(True)
            self.cbOpenDir.Enable(True)
            self.cbAddBrowse.Enable(True)
            self.chcLstLevel.Enable(True)
    def SetLanguage(self,lang):
        self.lang=lang
        self.vgpTree.SetLanguage(self.lang)
    def SetNormalGrouping(self):
        self.vgpTree.SetNormalGrouping()
        self.SetNode(self.docNode)
    def SetIdGrouping(self):
        self.vgpTree.SetIdGrouping()
        self.SetNode(self.docNode)
    def SetBaseTmplDN(self,baseTmplDN):
        if len(baseTmplDN)>0:
            self.bReplBaseDN=True
            self.baseDN=baseTmplDN
            pass
    def SetProjectDN(self,prjDN):
        if len(prjDN)>0:
            self.bReplPrjDN=True
            self.prjDN=prjDN
            pass
    def SetPrjInfo(self,sPrj,sClient):
        self.sPrjShortCut=sPrj
        self.sPrjClient=sClient
    def SetPrjInfos(self,prjinfos):
        self.prjinfos=prjinfos
    def SetDocPrjDoc(self,doc,bNet=False):
        #if bNet:
        #    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.docPrj=doc
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        self.vgpTree.SetDoc(self.doc,bNet)
    def OnGotContent(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            #if self.verbose:
            #    vtLog.vtLogCallDepth(self,'name:%s'%(self.GetName()))
            #self.docNode=self.doc.getChild(self.doc.getRoot(),'docnumbering')
            self.docNode=self.doc.getBaseNode()
            self.lstDocs.DeleteAllItems()
            self.txtTitle.SetValue('')
            self.txtRev.SetValue('')
            self.txtExt.SetValue('')
            self.chcLstLevel.Clear()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node):
        vtLog.CallStack('')
        self.docNode=node
        self.vgpTree.SetNode(node)
        self.lstDocs.DeleteAllItems()
        self.txtTitle.SetValue('')
        self.txtRev.SetValue('')
        self.txtExt.SetValue('')
        self.chcLstLevel.Clear()
    def SetMode(self,mode):
        self.mode=mode
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        #docs=node.getElementsByTagName('document')
        self.node=node
        self.selectedIdx=-1
        self.sFN=None
        lvStrs=[]
        self.mode=self.docPrj.GetMode()
        if (node is not None) and (self.docNode is not None):
            while not(self.doc.isSame(node,self.docNode)):
                sPath=self.doc.getNodeText(node,'path')
                lvStrs.append(sPath)
                node=self.doc.getParent(node)
                if node is None:
                    vtLog.vtLngCurWX(vtLog.ERROR,'you are not supposed to be here',self)
                    break
            lvStrs.reverse()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lvStrs:%s'%(vtLog.pformat(lvStrs)),self)
        
        self.chcLstLevel.Clear()
        self.iLenLevel=len(lvStrs)
        self.lstLevel=[]
        for i in range(0,len(lvStrs)):
            self.chcLstLevel.Append(lvStrs[i])
            if self.mode=='complete':
                self.chcLstLevel.Check(i)
        if self.mode=='last':
            if len(lvStrs)>0:
                self.chcLstLevel.Check(len(lvStrs)-1)
        sels=self.chcLstLevel.GetSelections()
        # handle permissions
        if self.node is not None:
            try:
                bDocGrpCreate=self.doc.IsAccessOkDocGrp(long(self.doc.getKey(self.node)),
                                self.docPrj.GetLogin(),
                                vtSecFileAcl.ACL_FILE_CREATE,-1)
                vtLog.vtLngCurWX(vtLog.DEBUG,'key:%s;access:%d'%(self.doc.getKey(self.node),bDocGrpCreate),self)
            except:
                vtLog.vtLngTB(self.GetName())
        else:
            bDocGrpCreate=False
        bAddDoc=self.docPrj.IsAclOk('docgroup',self.doc.ACL_MSK_ADD)
        if bDocGrpCreate==False:
            bAddDoc=False
        bAddArchTmpl=self.docPrj.IsAclOk('archivetmpl',self.doc.ACL_MSK_ADD)
        if bDocGrpCreate==False:
            bAddArchTmpl=False
        bAdd=bAddDoc or bAddArchTmpl
        self.lstDocs.Enable(bAddDoc)
        self.txtTitle.Enable(bAdd)
        self.txtRev.Enable(bAdd)
        self.txtExt.Enable(bAdd)
        self.gcbbGenerate.Enable(bAddDoc)
        self.gcbbAdd.Enable(bAddDoc)
        self.gcbbArc.Enable(bAddArchTmpl)
        self.cbAddBrowse.Enable(bAddDoc)
        self.chcLstLevel.Enable(bAdd)
        
        #self.cbOpenDir.Enable(False)
        
        self.RefreshDocuments()
    def RefreshDocuments(self):
        # setup attributes
        self.lstDocs.DeleteAllItems()
        
        #docNodes=self.node.getElementsByTagName('document')
        self.docs=[]
        if self.node is None:
            return
        docNodes=self.doc.getChilds(self.node,'document')
        for docNode in docNodes:
            l=self.doc.getAttribute(docNode,'language')
            bLang=False
            if len(l)<=0 and self.lang=='en':
                bLang=True
            elif self.lang==l:
                bLang=True
            if bLang==True:
                d={}
                for sAttr in ['title','type','filename','author','version','date']:
                    sVal=self.doc.getNodeText(docNode,sAttr)
                    d[sAttr]=sVal
                d['document']=docNode
                self.docs.append(d)
        i=0
        for doc in self.docs:
            #keys=doc.keys()
            #keys.sort()
            index = self.lstDocs.InsertImageStringItem(sys.maxint, doc['title'], -1)
            fn=fnUtil.getFilenameRel2BaseDir(doc['filename'],self.baseDN)
            self.lstDocs.SetStringItem(index, 1, fn)
            self.lstDocs.SetItemData(index,i)
            if i==self.selectedIdx:
                self.lstDocs.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            i=i+1
            #for k in keys:
            #    try:
            #        imgId=self.imgDict[k]
            #    except:
            #        imgId=-1
            #    index = self.lstDocs.InsertImageStringItem(sys.maxint, k, imgId)
            #    self.lstDocs.SetStringItem(index, 1, docs[k])
        pass
    def OnLstDocsListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstDocs.GetItem(idx,0)
        #self.txtDocTitle.SetValue(it.m_text)
        self.selectedIdx=it.m_itemId
        it=self.lstDocs.GetItem(idx,1)
        #self.txtFilename.SetValue(it.m_text)
        self.sFN=it.m_text
        sExts=string.split(self.sFN,'.')
        sExt=sExts[-1]
        self.txtExt.SetValue(sExt)
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        self.txtExt.SetStyle(0, 100, wx.TextAttr("BLACK", bkgCol))
        event.Skip()
    def OnGcbbGenerateButton(self, event):
        if self.__addDoc2Prj__()>=0:
            #wx.PostEvent(self,vgpDocSelDocAdded(self,node,docNode,sFN))
            pass
        else:
            return
        if self.sFullName is None:
            return
        #vtLog.CallStack('')
        #vtLog.pprint(self.docinfos)
        self.dlgGen.Set(self.doc,self.docinfos,self.documentNode,self.prjinfos)
        ret=self.dlgGen.ShowModal()
        if ret==1:
            wx.PostEvent(self,vgpDocSelDocAdded(self,self.node,
                    self.documentNode,self.sFullName))
        
        event.Skip()
    def __addDoc2Prj__(self,bChkFileExist=True,sNewFN=None):
        self.docinfos={}
        self.lang=self.docPrj.GetLang()
        if self.dlgGen is None:
            self.dlgGen=vGenerateDocDialog(self)
        sExt=self.txtExt.GetValue()
        sTitle=self.txtTitle.GetValue()
        bFault=False
        if len(sTitle)<=0:
            sTitle='???'
            self.txtTitle.SetValue(sTitle)
            self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("RED", "YELLOW"))
            bFault=True
        else:
            sTitleRepl=fnUtil.replaceSuspectChars(sTitle,' ')
            #sTitle=sRepl
            #if sRepl!=sTitle:
            #    self.txtTitle.SetValue(sRepl)
            #    self.txtTitle.SetStyle(0, len(sRepl), wx.TextAttr("RED", "YELLOW"))
            #    bFault=True
            #else:
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("BLACK", bkgCol))
        sRev=self.txtRev.GetValue()
        if len(sRev)<=0:
            sRev='??'
            self.txtRev.SetValue(sRev)
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
            bFault=True
        else:
            sRepl=fnUtil.replaceSuspectChars(sRev)
            if sRepl!=sRev:
                self.txtRev.SetValue(sRepl)
                self.txtRev.SetStyle(0, len(sRepl), wx.TextAttr("RED", "YELLOW"))
                bFault=True
            else:
                bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", bkgCol))
        self.prjDN=self.docPrj.GetPrjDN()
        if len(self.prjDN)<=0:
            return
        if len(self.baseDN)<=0:
            return
        if bFault:
            return -1
        #dt=wx.DateTime.Now()
        #sDate=dt.Format('%Y%m%D_%H%M%S')
        dt=vtDateTime(True)
        sDate=dt.GetDateStr()
        
        self.docinfos['DocTitle']=sTitle
        self.docinfos['Revision']=sRev
        self.docinfos['DocDate']=sDate
        self.docinfos['RevDate']=sDate
        self.docinfos['DocDateTime']=dt.GetDateTimeStr()
        self.docinfos['RevDateTime']=dt.GetDateTimeStr()
        if self.vgpTree.objTreeItemSel is None:
            return
        else:
            if self.doc.getTagName(self.vgpTree.objTreeItemSel)!='docgroup':
                return
            node=self.vgpTree.objTreeItemSel
            #doc=vtXmlDomTree.__getDocument__(node)
            fn=self.prjDN
            sDocGrp=self.doc.getAttribute(node,'id')
            sDocPath=self.doc.getNodeText(node,'path')
            iCount=self.chcLstLevel.GetCount()
            lstDocPath=[]
            for idx in range(0,iCount):
                if self.chcLstLevel.IsChecked(idx):
                    lstDocPath.append(self.chcLstLevel.GetString(idx))
            
            sDocPath=string.join(lstDocPath,os.path.sep)
            
            fn=string.capitalize(self.sPrjClient)
            fn=fn+string.capitalize(self.sPrjShortCut)+'_'+sDocGrp+'_'
            #fn=fn+''.join(map(string.capitalize,sTitleRepl.split(' ')))+'_'+sRev+'.'+sExt
            fn=fn+''.join(sTitleRepl.split(' '))+'_'+sRev+'.'+sExt
            if sNewFN is None:
                sFN=os.path.join(self.prjDN,sDocPath,fn)
                sRelFN=os.path.join('.',sDocPath,fn)
            else:
                sFN=os.path.join(self.prjDN,sDocPath,sNewFN)
                sRelFN=os.path.join('.',sDocPath,sNewFN)
                
            if bChkFileExist:
                try:
                    os.stat(sFN)
                    self.txtTitle.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                    self.txtRev.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                    self.txtExt.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                    return -2
                except:
                    bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                    self.txtExt.SetStyle(0, 100, wx.TextAttr("BLACK", bkgCol))
                    pass
            if self.selectedIdx==-1:
                docNode=self.docPrj.createNode('document')
                self.docPrj.setNodeText(docNode,'title','')
                self.docPrj.setNodeText(docNode,'filename','')
                self.docPrj.setNodeText(docNode,'author','---')
                self.docPrj.setNodeText(docNode,'version','---')
                self.docPrj.setNodeText(docNode,'date','---')
                self.docPrj.setNodeText(docNode,'description','---')
                self.sFullName=sFN
                self.documentNode=docNode
                self.dlgGen.SetFileNames(None,sFN)
            else:
                it=self.lstDocs.GetItem(self.selectedIdx,1)
                sTmpl=it.m_text
                for doc in self.docs:
                    if doc['filename']==sTmpl:
                        break
                sTmplFN=doc['filename']
                sFullTmplFN=os.path.join(self.baseDN,sTmplFN)
                docNode=self.docPrj.cloneNode(doc['document'],True)
                self.sFullName=sFN
                self.documentNode=docNode
                self.dlgGen.SetFileNames(sFullTmplFN,sFN)
            self.docPrj.setNodeText(docNode,'title',sTitle)
            if sys.platform=='win32':
                sRelFN=sRelFN.replace('\\','/')
            try:
                if len(self.lang)>0:
                    self.docPrj.setAttribute(docNode,'language',self.lang)
            except:
                vtLog.vtLngTB(self.GetName())
            self.docPrj.setNodeText(docNode,'filename',sRelFN)
            self.docPrj.setNodeText(docNode,'version',sRev)
            #dt=vtDateTime(True)
            #sDate=dt.GetDateTimeStr(' ')
            dt=wx.DateTime.Now()
            sDate=dt.Format('%Y%m%D_%H%M%S')
            
            self.docPrj.setNodeText(docNode,'date',sDate)
            keys=self.docinfos.keys()
            keys.sort()
            for k in keys:
                self.docPrj.setNodeText(docNode,k,self.docinfos[k])
            return 0
    def OnGcbbAddButton(self, event):
        self.prjDN=self.docPrj.GetPrjDN()
        nodeSel=self.vgpTree.GetSelected()
        if nodeSel is not None:
            if self.selectedIdx==-1:
                #vtLog.CallStack('')
                #print nodeSel
                #print self.selectedIdx
                sDocPath=self.__getDocPath__()
                sDN=os.path.join(self.prjDN,sDocPath)
                try:
                    os.makedirs(sDN)
                except:
                    #vtLog.vtLngTB(self.GetName())
                    pass
                #self.doc.SetDocument(,nodeSel,None,None,None)
                wx.PostEvent(self,vgpDocSelDocAdded(self,nodeSel,
                                None,sDN))
                return
        
        if self.__addDoc2Prj__()>=0:
            if self.dlgGen.tmplFN is None:
                try:
                    sDir=os.path.split(self.sFullName)
                    os.makedirs(sDir[0])
                except:
                    pass
                f=open(self.sFullName,'w')
                f.close()
            else:
                try:
                    sDir=os.path.split(self.sFullName)
                    os.makedirs(sDir[0])
                except:
                    pass
                shutil.copy(self.dlgGen.tmplFN,self.sFullName)
            wx.PostEvent(self,vgpDocSelDocAdded(self,self.node,
                self.documentNode,self.sFullName))
        event.Skip()
    def __getDocPath__(self):
        iCount=self.chcLstLevel.GetCount()
        lstDocPath=[]
        for idx in range(0,iCount):
            if self.chcLstLevel.IsChecked(idx):
                lstDocPath.append(self.chcLstLevel.GetString(idx))
        
        sDocPath=string.join(lstDocPath,os.path.sep)
        return sDocPath
    def OnGcbbArchButton(self,event):
        self.prjDN=self.docPrj.GetPrjDN()
        self.docinfos={}
        if self.dlgGenArchTmpl is None:
            self.dlgGenArchTmpl=vGenerateArchTmplDialog(self)
            self.dlgGenArchTmpl.SetDoc(self.doc)
        sExt=self.txtExt.GetValue()
        if len(sExt)<=0:
            sExt='zip'
            self.txtExt.SetValue('zip')
        bFound=False
        if sExt=='zip':
            bFound=True
        elif sExt=='bz2':
            bFound=True
        else:
            sExt='zip'
            self.txtExt.SetValue('zip')
            
        sTitle=self.txtTitle.GetValue()
        bFault=False
        if len(sTitle)<=0:
            sTitle='???'
            self.txtTitle.SetValue(sTitle)
            self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("RED", "YELLOW"))
            bFault=True
        else:
            sRepl=fnUtil.replaceSuspectChars(sTitle)
            if sRepl!=sTitle:
                self.txtTitle.SetValue(sRepl)
                self.txtTitle.SetStyle(0, len(sRepl), wx.TextAttr("RED", "YELLOW"))
                bFault=True
            else:
                bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("BLACK", bkgCol))
        sRev=self.txtRev.GetValue()
        if len(sRev)<=0:
            sRev='??'
            self.txtRev.SetValue(sRev)
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
            bFault=True
        else:
            sRepl=fnUtil.replaceSuspectChars(sRev)
            if sRepl!=sRev:
                self.txtRev.SetValue(sRepl)
                self.txtRev.SetStyle(0, len(sRepl), wx.TextAttr("RED", "YELLOW"))
                bFault=True
            else:
                bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", bkgCol))
        if len(self.prjDN)<=0:
            return
        if len(self.baseDN)<=0:
            return
        if bFault:
            return -1
        #dt=vtDateTime(True)
        #sDate=dt.GetDateTimeStr(' ')
        dt=wx.DateTime.Now()
        sDate=dt.Format('%Y%m%D_%H%M%S')
            
        self.docinfos['DocTitle']=sTitle
        self.docinfos['Revision']=sRev
        self.docinfos['DocDate']=dt
        self.docinfos['RevDate']=dt
        if self.vgpTree.objTreeItemSel is None:
            return
        else:
            node=self.vgpTree.objTreeItemSel
            if self.doc.getTagName(node)!='docgroup':
                return
            
            fn=self.prjDN
            sDocGrp=self.doc.getAttribute(node,'id')
            sDocPath=self.doc.getNodeText(node,'path')
            
            sDocPath=self.__getDocPath__()
            
            fn=string.capitalize(self.sPrjClient)
            fn=fn+string.capitalize(self.sPrjShortCut)+'_'+sDocGrp+'_'
            fn=fn+string.capitalize(sTitle)+'_'+sRev+'.'+sExt
            sFN=os.path.join(self.prjDN,sDocPath,fn)
            sRelFN=os.path.join('.',sDocPath,fn)
            try:
                os.stat(sFN)
                self.txtTitle.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                self.txtRev.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                self.txtExt.SetStyle(0, 100, wx.TextAttr("RED", "YELLOW"))
                return -2
            except:
                bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                self.txtExt.SetStyle(0, 100, wx.TextAttr("BLACK", bkgCol))
                pass
            dt=wx.DateTime.Now()
            sDate=dt.Format('%Y%m%D_%H%M%S')
            docNode=self.doc.createSubNode(None,'archivetmpl',False)
            self.doc.setNodeText(docNode,'title',sTitle)
            self.doc.setNodeText(docNode,'filename',sRelFN)
            self.doc.setNodeText(docNode,'directory',sRelFN)
            self.doc.setNodeText(docNode,'author','---')
            self.doc.setNodeText(docNode,'version',sRev)
            self.doc.setNodeText(docNode,'date',sDate)
            self.doc.setNodeText(docNode,'description','---')
            self.sFullName=sFN
            self.documentNode=docNode
            self.dlgGenArchTmpl.Set(docNode,sFN,self.prjDN)
            self.dlgGenArchTmpl.Centre()
            ret=self.dlgGenArchTmpl.ShowModal()
            if ret==1:
                
                wx.PostEvent(self,vgpDocSelDocAdded(self,self.node,
                    self.documentNode,self.sFullName))
                pass
            #vtXmlDomTree.setNodeText(docNode,'title',sTitle,doc)
            #vtXmlDomTree.setNodeText(docNode,'filename',sRelFN,doc)
            #vtXmlDomTree.setNodeText(docNode,'version',sRev,doc)
            #vtXmlDomTree.setNodeText(docNode,'date',sDate,doc)
        event.Skip()

    def OnVgpTreeVtxmltreeItemSelected(self, event):
        event.Skip()

    def OnCbOpenDirButton(self, event):
        self.prjDN=self.docPrj.GetPrjDN()
        sDocPath=self.__getDocPath__()
        sDN=os.path.join(self.prjDN,sDocPath)
        
        if sys.platform=='win32':
            os.popen('explorer %s'%sDN)
        event.Skip()
        
    def AddFile(self,sFullFN,bNew=True):
        try:
            if self.dlgAddFile is None:
                self.dlgAddFile=vAddFileDialog(self)
                self.dlgAddFile.Centre()
            self.prjDN=self.docPrj.GetPrjDN()
            sDocDN=self.__getDocPath__()
            node=self.node
            sDocGrp=self.doc.getAttribute(node,'id')
            if self.dlgAddFile.Set(self.prjDN,sDocDN,sDocGrp,
                    self.sPrjClient,self.sPrjShortCut,sFullFN):
                # file is 2 import
                iRet=self.dlgAddFile.ShowModal()
                if iRet==1:
                    # copy
                    sNewFN,sNewFullFN=self.dlgAddFile.GetFN()
                    try:
                        sDir=os.path.split(sNewFullFN)
                        sNewFN=sDir[1]
                        os.makedirs(sDir[0])
                    except:
                        pass
                    shutil.copy(sFullFN,sNewFullFN)
                elif iRet==2:
                    # move
                    sNewFN,sNewFullFN=self.dlgAddFile.GetFN()
                    try:
                        sDir=os.path.split(sNewFullFN)
                        sNewFN=sDir[1]
                        os.makedirs(sDir[0])
                    except:
                        pass
                    shutil.move(sFullFN,sNewFullFN)
                else:
                    return
            else:
                sNewFN,sNewFullFN=self.dlgAddFile.GetFN()
            sTitle,sRev,sExt=self.dlgAddFile.GetInfos()
            self.txtTitle.SetValue(sTitle)
            self.txtRev.SetValue(sRev)
            self.txtExt.SetValue(sExt)
            if self.__addDoc2Prj__(False,sNewFN=sNewFN)>=0:
                wx.PostEvent(self,vgpDocSelDocAdded(self,self.node,
                    self.documentNode,sNewFN))
        except:
            vtLog.vtLngTB(self.GetName())
        
    def OnCbAddBrowseButton(self, event):
        dlg = wx.FileDialog(self, _(u"Open"), "", "", _(u"all files (*.*)|*.*"), wx.OPEN)
        try:
            self.prjDN=self.docPrj.GetPrjDN()
            fn=self.prjDN
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                sFN = dlg.GetPath()
                self.AddFile(sFN)
            return
        finally:
            dlg.Destroy()
        event.Skip()
     
        
