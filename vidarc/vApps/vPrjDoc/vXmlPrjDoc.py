#----------------------------------------------------------------------------
# Name:         vXmlPrjDoc.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlPrjDoc.py,v 1.15 2014/10/01 09:02:28 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.sec.vtSecFileAcl import vtSecFileAcl

from vidarc.vApps.vPrjDoc.vXmlNodePrjDocRoot import vXmlNodePrjDocRoot
from vidarc.vApps.vPrjDoc.vXmlNodeDocGrp import vXmlNodeDocGrp
from vidarc.vApps.vPrjDoc.vXmlNodeDocument import vXmlNodeDocument
from vidarc.vApps.vPrjDoc.vXmlNodeArchive import vXmlNodeArchive
from vidarc.vApps.vPrjDoc.vXmlNodeArchiveTmpl import vXmlNodeArchiveTmpl
from vidarc.vApps.vPrjDoc.vXmlNodeCfgLocal import vXmlNodeCfgLocal

from vidarc.vApps.vDoc.vXmlNodeDocGrpAcl import vXmlNodeDocGrpAcl

from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
import vidarc.config.vcCust as vcCust

import vidarc.tool.InOut.fnUtil as fnUtil

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xcaIDAT8\x8d\x8d\x93\xbfk\x14A\x14\xc7?3\xb3\x93\xdd\xbd$\x97\xbb\
\xa8\x04\xd3\x88QD\x0c\x11\x14\r\x88\x8a\x85XYz\xff@\n;\x05\x0b\x0b\xc5\xc6F\
H\x99"\xfe\xc0\xc6\xd2&\x85\x08\x96V\xa6Ja\xa1\x16IZ\r\x06\xa3\xe1\xb8\xbb\
\xdd=wgw,\xe4\xce\xdc\xde\x1c\xf8\xaa\x99/\xdf\xf7\xe6\xbd\xef\xbc\xaf\x10RQ\
\x8e\x85\xe5\x86\x1d\x02\x81\xcf\x0f\xd6D\x19\x93\xffKta\x00\xa2\xd7\xc1\xc1\
W\x9f\x9c\xbe\xc5\xb7\xaf\xdb\x9c\x99\xbf\xc0\xdd\x8dW\xae\xbc~Ag\x07\x95@q\
\xea\xc4,i\xfc\x9d\x95s\r^\\^r\x16\x01\xf0\\\xa0\xb4\x96\x10\x0fc\xba\x8cW3\
\xa4\xbf\xcf\xb3\x8b\xd7H\xba\x16+\xa6\xb9\xff\xe9\xcd?nY\xb0\xe7\x97n\xe3\
\xd9\x82\xce~\x9b\xa8\x19#2\x98\n+\xcc\xd4\x03\xa6k\x1d\xd6\xd7\xdf\xf6\xb9\
\x0b\xcb\r+\xcb\xf3\x17\xa6Cu\xc2g,\x18\xc3\x9f\x08\xf1\xc7C\xbaQB\x9e\x19L\
\x0e\xc7\xe7\xce\x0eh6\xa0\xc1\xcb\xabK\xd4\xab\x12\xad-\xb5#\x93\x1c\x9e\
\xa9\xa3<\x891)\xca\xb3\xc4I\xc6\xd1\xd9\x93\xa35\xf0\xf8\x85\xaf|\xd24\xc5\
\x0f$\xb6\xc8H\x7f\'(\x9d\xa3\xb4!N\n\x1en\xae\xb9\x0b<]\xbc\xc1\xa1\x9a!\
\x8b2\x8a\xc2\x90\x1bI\x9e\x1b\x90\x19A\xa8\x88\xd3\x16VL\x0e\x0b\x0e\x7f\
\xff\xb4\xdd\xde%N\xf6\xb0\xaa\x85\xd6\tBEX:@\x8c\xd4\t\xcd\xa8Mu\xeaX?\xb1\
\xb7\x07\x03\x8b\xb4\xbax\x93<\xdb\xa5\xe2\x1b\xc2\x10\xb4\x04\xa5\nth\xf8\
\xb2\xd9\xe2\xd1\xd6\xceP\x81\x01\r\xeel\xbc\xeb\x9fW\xce_G\xd8\x1fT\xc2\x88\
x\xaf\x89\xa7\xe7\x81\x1d\xca\xe1\\$\x80{\x1f\xdf\x03\xf0x\xee\n[\xdb?y\xed}\
p\xf2\x84\xcb\x8d\xbd\x91\x0e\xdeG\x99\xc9\xe9\x05\x97\x9dGY\xfc\x0f(\n\xb17\
\xac\xcb\xdf\x17\x00\x00\x00\x00IEND\xaeB`\x82'

class vXmlPrjDoc(vtXmlDomReg):
    TAGNAME_ROOT='projectdocuments'#'prjdocs'
    APPL_REF=['vHum','vDoc','vGlobals','vFileSrv']
    def __init__(self,attr='id',skip=[],synch=False,appl='vPrjDoc',verbose=0,
                    audit_trail=True):
        for s in ['prjinfo','config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            self.nodes2synch.append('prjinfo')
            oDocRoot=vXmlNodePrjDocRoot()
            oDocGrp=vXmlNodeDocGrp()
            oDoc=vXmlNodeDocument()
            oArch=vXmlNodeArchive()
            oArchTmpl=vXmlNodeArchiveTmpl()
            
            self.dDocGrpAcl={}
            
            #oAclFile=vtSecFileAcl()
            #self.RegisterNode(oAclFile,False)
            oDocGrpAcl=vXmlNodeDocGrpAcl()
            self.RegisterNode(oDocGrpAcl,False)
            
            self.RegisterNode(oDocRoot,False)
            self.RegisterNode(oDocGrp,True)
            self.RegisterNode(oDoc,False)
            self.RegisterNode(oArch,False)
            self.RegisterNode(oArchTmpl,False)
            self.LinkRegisteredNode(oDocGrp,oDoc)
            self.LinkRegisteredNode(oDocGrp,oDocGrpAcl)
            self.LinkRegisteredNode(oDocGrp,oArch)
            self.LinkRegisteredNode(oDocGrp,oArchTmpl)
            
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            
            oLocalCfg=vXmlNodeCfgLocal()
            self.RegisterNode(oLocalCfg,False)
            self.LinkRegisteredNode(oCfg,oLocalCfg)
            
        except:
            vtLog.vtLngTB(self.appl)
        
        self.SetDftLanguages()
    def IsSkip(self,node):
        if vtXmlDomReg.IsSkip(self,node):
            return True
        #if self.getTagName(node) not in ['docgroup','document']:
        #    return True
        return False
    def __genIds__(self,c,node,ids):
        sTag=self.getTagName(c)
        if sTag in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        elif sTag in ['docgroup','document','archivetmpl','archive']:
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'prjdocs')
    #def New(self,revision='1.0',root='projectdocuments',bConnection=False):
    #    if self.verbose>0:
    #        vtLog.vtLogCallDepth(self,'root:%s'%root)
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'prjdocs')
        if bConnection==False:
            self.CreateReq()
        #self.setAttribute(elem,self.attr,'')
        #self.checkId(elem)
        #self.processMissingId()
        #self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL,
                #'prjdocs':self.ACL_MSK_NORMAL,
                'docgroup':self.ACL_MSK_NORMAL,
                'archivetmpl':self.ACL_MSK_NORMAL,
                'archive':self.ACL_MSK_NORMAL,
                'document':self.ACL_MSK_NORMAL,
                },['all','docgroup','archivetmpl','archive','document']
    def GetPossibleFilter(self):
        return {'docgroup':[
                            ('title',self.FILTER_TYPE_STRING),
                            ],
                'document':[
                            ('title',self.FILTER_TYPE_STRING),
                            ('filename',self.FILTER_TYPE_STRING),
                            ('author',self.FILTER_TYPE_STRING),
                            ('version',self.FILTER_TYPE_STRING),
                            ('description',self.FILTER_TYPE_STRING),
                            ('date',self.FILTER_TYPE_DATE_TIME),
                            ('DocDate',self.FILTER_TYPE_DATE),
                            ('DocDateTime',self.FILTER_TYPE_DATE_TIME),
                            ('RevDate',self.FILTER_TYPE_DATE),
                            ('RevDateTime',self.FILTER_TYPE_DATE_TIME),
                            ('Revision',self.FILTER_TYPE_STRING),
                            ],
                'archivetmpl':[
                            ('title',self.FILTER_TYPE_STRING),
                            ('filename',self.FILTER_TYPE_STRING),
                            ('directory',self.FILTER_TYPE_STRING),
                            ('author',self.FILTER_TYPE_STRING),
                            ('version',self.FILTER_TYPE_STRING),
                            ('description',self.FILTER_TYPE_STRING),
                            ('date',self.FILTER_TYPE_DATE_TIME),
                            ],
                'archive':[
                            ('title',self.FILTER_TYPE_STRING),
                            ('filename',self.FILTER_TYPE_STRING),
                            ('directory',self.FILTER_TYPE_STRING),
                            ('author',self.FILTER_TYPE_STRING),
                            ('version',self.FILTER_TYPE_STRING),
                            ('description',self.FILTER_TYPE_STRING),
                            ('date',self.FILTER_TYPE_DATE_TIME),
                            ],
                            }
    def __procDocGrpAcl__(self,node,oDocGrp,oAclFile):
        sTag=self.getTagName(node)
        if sTag=='docgroup':
            fid=oDocGrp.GetDocGrpID(node)
            fp=self.GetFingerPrintAndStore(node)
            if fid in self.dDocGrpAcl:
                vtLog.vtLngCur(vtLog.ERROR,'fid:%08d already defined;id:%s ignored'%(fid,self.getKey(node)),self.appl)
                return 0
            oAclFile.BuildAcl(node)
            id=long(self.getKey(node))
            #vtLog.CallStack('')
            #print id
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'node:%s'%(node),self.appl)
            self.dDocGrpAcl[fid]={'id':id,
                        'fingerprint':fp,
                        'dAclDocGrp':oAclFile.GetAclDict(),
                    }
        return 0
    def addNode(self,par,node):
        vtXmlDomReg.addNode(self,par,node)
        oDocGrp=self.GetReg('docgroup')
        #oAclFile=self.GetReg('fileAcl')
        oAclFile=self.GetReg('docgroupACL')
        self.__procDocGrpAcl__(node,oDocGrp,oAclFile)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(oAclFile.GetAclDict())),self.appl)
    #def Build(self):
    #    vtXmlDomReg.Build(self)
    def CreateAcl(self):
        vtXmlDomReg.CreateAcl(self)
        #vtLog.CallStack('')
        self.dDocGrpAcl={}
        oDocGrp=self.GetReg('docgroup',bLogFlt=False)
        if oDocGrp is None:
            return
        #oAclFile=self.GetReg('fileAcl')
        oAclFile=self.GetReg('docgroupACL',bLogFlt=False)
        if oAclFile is None:
            return
        self.procChildsKeys(self.getBaseNode(),self.__procDocGrpAcl__,
                    oDocGrp,oAclFile)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dDocGrpAcl:%s'%(vtLog.pformat(self.dDocGrpAcl)),self.appl)
    def __checkDocGrpAclUpToDate__(self,fid,oAclFile):
        try:
            if fid in self.dDocGrpAcl:
                d=self.dDocGrpAcl[fid]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fid:%08d;d:%s'%(fid,vtLog.pformat(d)),self.appl)
                node=self.getNodeByIdNum(d['id'])
                fp=self.GetFingerPrintAndStore(node)
                if fp!=d['fingerprint']:
                    oAclFile.BuildAcl(node)
                    d['dAclDocGrp']=oAclFile.GetAclDict()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(oAclFile.GetAclDict())),self.appl)
                return d['dAclDocGrp']
        except:
            vtLog.vtLngTB(self.appl)
            vtLog.vtLngCur(vtLog.ERROR,'fid:%d;%s'%(fid,vtLog.pformat(self.dDocGrpAcl)),self.appl)
        return None
    def IsAccessOkDocGrp(self,fid,objLogin,iAcl,iSecLv):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'fid:%08d;objLogin:%s;iAcl:0x%08x;iSecLv:%d'%\
                    (fid,`objLogin`,iAcl,iSecLv),self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dDocGrpAcl:%s'%(vtLog.pformat(self.dDocGrpAcl)),self.appl)
        if fid in self.dDocGrpAcl:
            #oAclFile=self.GetReg('fileAcl')
            oAclFile=self.GetReg('docgroupACL')
            dAclFile=self.__checkDocGrpAclUpToDate__(fid,oAclFile)
            if dAclFile is None:
                vtLog.vtLngCur(vtLog.INFO,'no dAcl',self.appl)
                return False
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(dAclFile)),self.appl)
            if oAclFile.IsAccessOkDict(dAclFile,objLogin,iAcl,iSecLv):
                vtLog.vtLngCur(vtLog.INFO,'access ok',self.appl)
                return True
        try:
            d=self.GetNetDocs()
            if d is None:
                vtLog.vtLngCur(vtLog.ERROR,'net docs is none',self.appl)
                return False
            if 'vDoc' in d:
                return d['vDoc']['doc'].IsAccessOkDocGrp(fid,objLogin,iAcl,iSecLv)
        except:
            vtLog.vtLngTB(self.appl)
        vtLog.vtLngCur(vtLog.INFO,'no access',self.appl)
        return False
    def GetPrjDN(self):
        node=self.getChildByLst(self.getBaseNode(),['cfg','cfgLocal'])
        if node is None:
            return vcCust.USR_LOCAL_DN
            #return os.path.expanduser('~')
        else:
            oReg=self.GetRegByNode(node)
            return oReg.GetPrjDN(node)
        oReg=self.GetReg('prjdocs')
        return oReg.GetPrjDN(self.getRoot())
    def GetMode(self):
        node=self.getChildByLst(self.getBaseNode(),['cfg','cfgLocal'])
        if node is None:
            return 'complete'
        else:
            oReg=self.GetRegByNode(node)
            return oReg.GetMode(node)
        oReg=self.GetReg('prjdocs')
        return oReg.GetMode(self.getRoot())
    def _buildPrjInfoDict(self):
        self.dPrjInfo={}
        try:
            node=self.getChild(self.getRoot(),'prjinfo')
            
            self.sPrjShortCut=self.getNodeText(node,'name').strip()
            self.sPrjClt=self.getNodeText(node,'clientshort').strip()
            
            sID=self.getAttribute(node,'id')
            self.dPrjInfo[u'id']=sID
            nodes=self.getChilds(node)
            for n in nodes:
                tagName=self.getTagName(n)
                val=self.getText(n)
                self.dPrjInfo[tagName]=val
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'prjShortCut:%s;sPrjClt:%s;dPrjInfo:%s'%
                    (self.sPrjShortCut,self.sPrjClt,vtLog.pformat(self.dPrjInfo)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GenDocGrpDN(self,iDocGrpID,mode=None):
        try:
            #doc=self.GetNetDoc('vDoc')
            netMaster=self.GetNetMaster()
            doc=netMaster.GetNetDoc('vDoc')
            if doc is None:
                vtLog.vtLngCur(vtLog.WARN,'doc is None',self.GetOrigin())
                return None
            
            if mode is None:
                mode=self.GetMode()
            lvStrs=doc.GetDocGrpDNs(iDocGrpID)
            if lvStrs is None:
                vtLog.vtLngCur(vtLog.WARN,'lvStrs is None',self.GetOrigin())
                return None
            if mode=='complete':
                return '/'.join(['.','/'.join(lvStrs)])
            elif mode=='flat':
                return '.'
            elif mode=='last':
                return '/'.join(['.',lvStrs[-1]])
            elif mode=='major':
                if len(lvStrs)>1:
                    return '/'.join(['.',lvStrs[0],lvStrs[-1]])
                else:
                    return '/'.join(['.',lvStrs[-1]])
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return None
    def _genPrefix(self,iDocGrpID,style=0):
        try:
            netMaster=self.GetNetMaster()
            doc=netMaster.GetNetDoc('vDoc')
            if doc is None:
                vtLog.vtLngCur(vtLog.WARN,'doc is None',self.GetOrigin())
                return None
            if doc.IsKeyValid(iDocGrpID)==False:
                return None
            node=doc.getNodeByIdNum(iDocGrpID)
            if node is None:
                vtLog.vtLngCur(vtLog.WARN,'node is None',self.GetOrigin())
                return None
            o=doc.GetRegByNode(node)
            if o is None:
                vtLog.vtLngCur(vtLog.WARN,'node is not registered',self.GetOrigin())
                return None
            sDocGrp=o.GetKey(node)
            if style==0:
                return ''.join([self.sPrjClt.capitalize(),self.sPrjShortCut.capitalize(),'_',
                        sDocGrp,'_'])
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return None
    def GenDocGrpFN(self,iDocGrpID,sTitle,sRev,sExt,style=0):
        sDocGrpDN=self.GenDocGrpDN(iDocGrpID)
        if sDocGrpDN is None:
            vtLog.vtLngCur(vtLog.WARN,'docGrp is None',self.GetOrigin())
            return None,None
        sPrefix=self._genPrefix(iDocGrpID,style=style)
        if sPrefix is None:
            vtLog.vtLngCur(vtLog.WARN,'prefix is None',self.GetOrigin())
            return None,None
        if style==0:
            sTitle=fnUtil.replaceSuspectChars(sTitle)
            sRev=fnUtil.replaceSuspectChars(sRev)
            sExt=fnUtil.replaceSuspectChars(sExt)
            l=[sDocGrpDN,'/',
                sPrefix,
                sTitle,'_',sRev,'.',sExt]
        else:
            vtLog.vtLngCur(vtLog.ERROR,'undefined style',self.GetOrigin())
            return None,None
        sDN=self.GetPrjDN().replace('\\','/')
        return sDN,''.join(l)
    def GenDocGrpExternalFN(self,iDocGrpID,sFN,style=0):
        sDocGrpDN=self.GenDocGrpDN(iDocGrpID)
        if sDocGrpDN is None:
            vtLog.vtLngCur(vtLog.WARN,'docGrp is None',self.GetOrigin())
            return None,None
        sPrefix=self._genPrefix(iDocGrpID,style=style)
        if sPrefix is None:
            vtLog.vtLngCur(vtLog.WARN,'prefix is None',self.GetOrigin())
            return None,None
        if style==0:
            sFN=fnUtil.replaceSuspectChars(sFN)
            l=[sDocGrpDN,'/',sPrefix,sFN]
        else:
            vtLog.vtLngCur(vtLog.ERROR,'undefined style',self.GetOrigin())
            return None,None
        sDN=self.GetPrjDN().replace('\\','/')
        return sDN,''.join(l)
    def SplitFN(self,iDocGrpID,fn,style=0):
        sDN,sFNext=os.path.split(fn)
        sPrefix=self._genPrefix(iDocGrpID,style=style)
        if sPrefix is None:
            return None
        sPrjDocDN=self.GetPrjDN()
        if sPrjDocDN==sDN:
            bIs2Import=True
            if sFNext.startswith(sPrefix):
                bIs2Import=False
        else:
            bIs2Import=True
        i=sFNext.rfind('.')
        if i>0:
            sExt=sFNext[i+1:]
            sFNrev=sFNext[:i]
        else:
            sFNrev=sFNext
            sExt=''
        if style==0:
            #print sFNrev
            i=sFNrev.rfind('_')
            if i>0:
                sRev=sFNrev[i+1:]
                sFN=sFNrev[:i]
            else:
                sFN=sFNrev
                sRev='0'
            if sFN.startswith(sPrefix):
                sTitle=sFN[len(sPrefix):]
            else:
                sTitle=sFN
        else:
            return None
        return bIs2Import,sTitle,sRev,sExt
    def Build(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        vtXmlDomReg.Build(self)
        self._buildPrjInfoDict()
    def GePrjInfoDict(self):
        return self.dPrjInfo
    def DoEditCfg(self,par):
        node=self.getChildByLst(self.getBaseNode(),['cfg','cfgLocal'])
        if node is not None:
            oReg=self.GetRegByNode(node)
            oReg.DoEdit(par,self,node)
