#----------------------------------------------------------------------------
# Name:         vXmlNodeArchive.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060729
# CVS-ID:       $Id: vXmlNodeArchive.py,v 1.2 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        #from vXmlNodeXXXPanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeArchive(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Title',None,'title',None),
            ('Desc',None,'description',None),
            #('Type',None,'type',None),
            ('Author',None,'author',None),
            #('Version',None,'version',None),
            ('FN',None,'filename',None),
            ('DN',None,'directory',None),
            ('Date',None,'date',None),
        ]
    FUNCS_GET_4_TREE=['Date','Title']
    FMT_GET_4_TREE=[('Date',''),('Title','')]
    GRP_GET_4_TREE=[]#[('Key',''),('Title','')]
    def __init__(self,tagName='archive'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'archive')
    # ---------------------------------------------------------
    # specific
    def GetTitle(self,node):
        return self.Get(node,'title')
    def GetDesc(self,node):
        return self.Get(node,'description')
    def GetAuthor(self,node):
        return self.Get(node,'author')
    def GetFN(self,node):
        return self.Get(node,'filename')
    def GetDN(self,node):
        return self.Get(node,'directory')
    def GetDate(self,node):
        return self.Get(node,'date')
    def SetTitle(self,node,val):
        self.Set(node,'title',val)
    def SetDesc(self,node,val):
        self.Set(node,'description',val)
    def SetAuthor(self,node,val):
        self.Set(node,'author',val)
    def SetFN(self,node,val):
        self.Set(node,'filename',val)
    def SetDN(self,node,val):
        self.Set(node,'directory',val)
    def SetDate(self,node,val):
        self.Set(node,'date',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x028IDAT8\x8d\x95\x92\xcfK\x14a\x1c\xc6?3z\xdc]!\x16t\xb6.\x12\xb9P\x87\
\x98\x15\xbc\xd9\xb8\xf4\x17t\xd9\x1a3\x88\x0eAy\xc9D:\xb6\x06\x9ej\x15\xa1"\
\xfb\xa1\xa4]\x12\x0fK\xbbn\x10\x98\xa3x)\xdb]\x7f\xb0\xe2\x8cA`\xad[\x14\
\x143K\x94\x90o\x87\xd9\xd9M\xda\x8b\xefe^x\xe7\xf3<\x0f\xcf\xf7+Ir\x03\x079\
\xc1\xe0\xaa\xb8ru\x9a\xafk\x87x\x90\xec\x97\xa4\x83\x08\x04\x83\xab\xe2\xee\
\xa5)\x88D\x88\xfdp\x90.\x7fB>\x08<\x9f\x18\x04E\xa1\xc92\x99\xdcX\x07\xa0\
\xb1\xde\xcfboN\x00H\xf2i\xe9_x\xf7\xf8E~\x15f\xf8\xb0d3\xf1r\x17I\xceH\xff\
\t\x88\xbd9a\x18\xee=\x1e\xef\x13\x85\x8dV<Xn|\xcc\xc9\x8fE&\xd6\x9a\xb1~f$\
\x80}\x1dx\xb0\xa6\xb5\x03P\xd8(@.Q\x85I\x17\xb96\xd9\xcc\xc2\x96\x0b\xe3\
\xc6l\xc0\x151\x84a\x18B\x08G\x08\xe1\x08\xc7\xb1\xc4\xf3\xd1\x0b"\x97\x9d\
\x15+\xabg\xc4\xcaP\x87\xe8\n\xfb\x05\x18\xc2c$\xb9\xc1-\xb1\xe6\x1c\x06\xca\
\x98\xd6\x02#\xa3#\x14\xbe\xb7V\x9d\x93\xdb\xcd\xccoZ\x18F\x19\xedT\x9f\xa8\
\xf6\xe4:{0\x98V\x96&\xbf\x8f\xc5\xa5/\x9c\xf8\xfd\x8d\x99\xe9\'\x18N\x80Tj\
\x18\x9fO\xa9\xe4.\x13\x8d\x8e\xb1\xb08"5\xc6\xe3i4\xad\x1b(U\x1e\x1d\xde\
\xbe\xdb\xe4\xe9\xd4k:\xf8LV\x92\xb9\x938O:\xfd\x8cp\x9bkr\xbd\xff\x11G\x8f\
\xfd\xc1\xb4\xde\x08)\xd2\xae\x8b\x80\x7f\x8bx\xbc\xbb\x9a\xe2\xde\xfdW\x0c\
\r\x8d\xa3(>\x14\xc5\x07@K\xe5\x9b\xcf}F\x8d\xb4\x90\x99}\x8f\xa2\xdcv\xa7\
\xa0\xaa1q$d\xa2\x84\x02\xe8z\x18M\xf3\xa2\x82i\x15\t\xb7\x1d\x06`` \x87S\
\x86\xe5\xe5u\xe0\x06\xf9|om\x95U5&zz\x02\x94m\x9b\xe2N\x16]w\xd0\xb4\xda~D\
\xa3el\xc7\x07\xdc$\x9f\xef\xad\x8eq\xdf\x1e\xa8jL\xdc\x1a\x84P(L:eR\xdc1\
\xd0\xf5\x12c\x0f\xfd\xd8\xf692\x99\xf1\xda\xfc\xeb\tx"\xc3\t\x1b\xc3(\xf1"U\
\xaat\xd0U\x17\xae+\xe0\x89\x80Fg\xe7Y\xb6\xb7\x0b$\x93Z]\x18\xe0/\xb4b\x016\
\xae\x135c\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            return vXmlNodeXXXEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            return vXmlNodeXXXAddDialog
        else:
            return None
    def GetPanelClass(self):
        return None
        if GUI:
            return vXmlNodeXXXPanel
        else:
            return None
