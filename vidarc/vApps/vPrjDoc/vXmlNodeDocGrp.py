#----------------------------------------------------------------------------
# Name:         vXmlNodeDoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060729
# CVS-ID:       $Id: vXmlNodeDocGrp.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        #from vDocGrpInfoControlPanel import *
        #from vXmlNodeDocEditDialog import *
        #from vXmlNodeDocAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeDocGrp(vtXmlNodeBase):
    NODE_ATTRS=[
            #('key',None,'key',None),
            ('Title',None,'title',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Key','Title','Desc']
    FUNCS_GET_4_TREE=['Key','Title']
    FMT_GET_4_TREE=[('Key',''),('Title','')]
    GRP_GET_4_TREE=[]#[('Key',''),('Title','')]
    
    def __init__(self,tagName='docgroup'):
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'document group')
    # ---------------------------------------------------------
    # specific
    def GetDocGrpID(self,node):
        return self.GetForeignKey(node,'fid','vDoc')
    def GetDocGrpFID(self,node):
        return self.GetAttrStr(node,'fid')
    def __getDocNode__(self,node):
        return self.doc.GetNode(self.GetDocGrpFID(node))
    def __getDocInfo__(self,node,attr,lang=None,bLang=False):
        netDoc,nodeDoc=self.__getDocNode__(node)
        if netDoc is None:
            vtLog.vtLngCurCls(vtLog.WARN,'net doc not found',self)
            return '???'
        if nodeDoc is None:
            vtLog.vtLngCurCls(vtLog.WARN,'net doc node not found',self)
            return '???'
        oReg=netDoc.GetRegByNode(nodeDoc)
        if bLang:
            if lang is None:
                lang=self.doc.GetLang()
        if lang is None:
            return oReg.GetValueByAttr(nodeDoc,attr)
        else:
            return oReg.GetValueByAttrML(nodeDoc,attr,lang)
    def GetKey(self,node):
        return self.__getDocInfo__(node,'Key')
    def GetPath(self,node):
        return self.__getDocInfo__(node,'Path')
    def GetTitleDoc(self,node,lang=None):
        return self.__getDocInfo__(node,'Title',lang,True)
    def GetDescDoc(self,node,lang=None):
        return self.__getDocInfo__(node,'Title',lang,True)
    def GetTitle(self,node,lang=None):
        return self.GetML(node,'title',lang)
    def GetDesc(self,node,lang=None):
        return self.GetML(node,'description',lang)
    def SetDocGrpID(self,node,val):
        self.SetForeignKeyStr(node,'fid',val,'vDoc')
    def SetDocGrpFID(self,node,val):
        return self.SetAttrStr(node,'fid',val)
    def SetTitle(self,node,val,lang=None):
        self.SetML(node,'title',val,lang)
    def SetDesc(self,node,val,lang=None):
        self.SetML(node,'description',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [
                (u'key',vtXmlFilterType.FILTER_TYPE_INT),
                (u'path',vtXmlFilterType.FILTER_TYPE_STRING),
                (u'title',vtXmlFilterType.FILTER_TYPE_STRING),
                (u'description',vtXmlFilterType.FILTER_TYPE_STRING),
                (u'user',vtXmlFilterType.FILTER_TYPE_STRING),
                (u'group',vtXmlFilterType.FILTER_TYPE_STRING),
                (u'reference',vtXmlFilterType.FILTER_TYPE_STRING),
                ]
    def GetTranslation(self,name):
        _(u'key'),_(u'path'),_(u'title'),_(u'description'),_(u'reference|company')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd8IDAT8\x8d\x85\xd3?h\x13a\x18\xc7\xf1\xef\xdd\xb5\xe0p\xa5\xb5\
\x93R+\xd1\x1a"upjB\xb1\x83\xa6P%C\xa5\x08\xa5\x7f\xbcN\xba8H\x07]\x8azI\x0b\
.\xe7h\'W\xbb\t\x92\x1bD\xa3\xb9H\xa2\x08\xd1\xa1"E0m\xc0d9\x8d\xbd\x06sC\
\x88\xc3\xeb\x10IL\x9a\\^x\xa7\xf7\xf9\xfc\xde\x97\xf7y_$Y\xa1\xdbLg\xd2b\
\xeb\xe9\x96\xf0\xaa\x91$Y\xa1\xd3\x88D\xae\x88\xd1\x13\xa3\xe8\xba\x8e\x197\
QU\x95\xeb++R{]\xc7\x80v\x0c0{u\x163nR\xab\xd5\xb8\xbd\xba*u\r\xf0\xc2\x8e\
\xe3\xb0\xef\xec\xa3\xca\x07\\\x8ch\\\nOK}\xff\xe3\x85\xc5y180\xe8\x89\x8b\
\xdf\x8b\x04\xfa?\xf1\xf6Y\x11\x00\xb9\x1d\x1b\x86\x81\x95\xb4\xba\xe2\x8a[a\
*8F\xf6}\xa2\x19\x90\xb2\x92"\x14\na\x18\x06f\xdc\xc4u]\x96\x96\x97\xb0\x92\
\xd6!\xbc\xb68\xc2\xb7\xcf\x89\xc6\xa9\xfbRVR\xec\xbc|\xc8\xaf\xfe \xb6m\x13\
\x9e\x0e\xa3\xaa*f\xdc\xa4P(Pq+-x;\xf5\xa4\x81\xdf$\x12B\xf1\xf9|\xd1\xbb\
\xa1\xe3\xfc\xfc\xf3\x9b/\xb92\x92$\xa1(\n\x81\xb3\x012\xef2\xec\xe5\xf6\x0e\
\xe1\x9c\r\xd2I\x8d\xb9ks\xd4/1w\xc0\x02\x80\xdf\xc6*\x95y\xb53\xc4\xf8\xb9q\
\\\xd7\xed\x88\x9da\x8d\xd8F\x8c\xb13\xfef\x17n<\xdfd\xe6t\x90\xf9\xf3\x13\
\x84\xfdU\xacR\x99\xdd\xdc\xae\'n\xe9\xc2\x90\n%9\xcb\x9d\xd7\x9b\xe0T\x19\
\xd86{\xe2\x96\x80G\xb7\xee\x11\xbc0\xc3\xa9c\xc0\xf0\x11\x80\x9e\xb8\xf1\
\x12\xf5\x07\xf7\x05\x1f^\xa0_\x9e\x00\xffQp\xaa|\xfc\x9a&\xfb#\xeb\x89\x81\
\xfa%\xc6\xd67\xea\x0b\x93\x11\xc1\xe3\x7fA=vn\x1e\xa1\xc3\x17\x8dF\xa3\xe2\
\xe6\xd4\x88\xd0\x965\x91\xcf\xe7=\xbf\xf3_\x10\x96\x0c:\xb9\x15\x9br\x00\
\x00\x00\x00IEND\xaeB`\x82' 

    def getSelImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd8IDAT8\x8d\x85\xd3?h\x13a\x18\xc7\xf1\xef\xdd\xb5\xe0p\xa5\xb5\
\x93R+\xd1\x1a"upjB\xb1\x83\xa6P%C\xa5\x08\xa5\x7f\xbcN\xba8H\x07]\x8azI\x0b\
.\xe7h\'W\xbb\t\x92\x1bD\xa3\xb9H\xa2\x08\xd1\xa1"E0m\xc0d9\x8d\xbd\x06sC\
\x88\xc3\xeb\x10IL\x9a\\^x\xa7\xf7\xf9\xfc\xde\x97\xf7y_$Y\xa1\xdbLg\xd2b\
\xeb\xe9\x96\xf0\xaa\x91$Y\xa1\xd3\x88D\xae\x88\xd1\x13\xa3\xe8\xba\x8e\x197\
QU\x95\xeb++R{]\xc7\x80v\x0c0{u\x163nR\xab\xd5\xb8\xbd\xba*u\r\xf0\xc2\x8e\
\xe3\xb0\xef\xec\xa3\xca\x07\\\x8ch\\\nOK}\xff\xe3\x85\xc5y180\xe8\x89\x8b\
\xdf\x8b\x04\xfa?\xf1\xf6Y\x11\x00\xb9\x1d\x1b\x86\x81\x95\xb4\xba\xe2\x8a[a\
*8F\xf6}\xa2\x19\x90\xb2\x92"\x14\na\x18\x06f\xdc\xc4u]\x96\x96\x97\xb0\x92\
\xd6!\xbc\xb68\xc2\xb7\xcf\x89\xc6\xa9\xfbRVR\xec\xbc|\xc8\xaf\xfe \xb6m\x13\
\x9e\x0e\xa3\xaa*f\xdc\xa4P(Pq+-x;\xf5\xa4\x81\xdf$\x12B\xf1\xf9|\xd1\xbb\
\xa1\xe3\xfc\xfc\xf3\x9b/\xb92\x92$\xa1(\n\x81\xb3\x012\xef2\xec\xe5\xf6\x0e\
\xe1\x9c\r\xd2I\x8d\xb9ks\xd4/1w\xc0\x02\x80\xdf\xc6*\x95y\xb53\xc4\xf8\xb9q\
\\\xd7\xed\x88\x9da\x8d\xd8F\x8c\xb13\xfef\x17n<\xdfd\xe6t\x90\xf9\xf3\x13\
\x84\xfdU\xacR\x99\xdd\xdc\xae\'n\xe9\xc2\x90\n%9\xcb\x9d\xd7\x9b\xe0T\x19\
\xd86{\xe2\x96\x80G\xb7\xee\x11\xbc0\xc3\xa9c\xc0\xf0\x11\x80\x9e\xb8\xf1\
\x12\xf5\x07\xf7\x05\x1f^\xa0_\x9e\x00\xffQp\xaa|\xfc\x9a&\xfb#\xeb\x89\x81\
\xfa%\xc6\xd67\xea\x0b\x93\x11\xc1\xe3\x7fA=vn\x1e\xa1\xc3\x17\x8dF\xa3\xe2\
\xe6\xd4\x88\xd0\x965\x91\xcf\xe7=\xbf\xf3_\x10\x96\x0c:\xb9\x15\x9br\x00\
\x00\x00\x00IEND\xaeB`\x82' 

    def GetEditDialogClass(self):
        return None
        if GUI:
            return vXmlNodeDocEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            return vXmlNodeDocAddDialog
        else:
            return None
    def GetPanelClass(self):
        return None
        if GUI:
            return vDocGrpInfoControlPanel
        else:
            return None

