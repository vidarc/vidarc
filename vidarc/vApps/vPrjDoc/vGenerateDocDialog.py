#Boa:Dialog:vGenerateDocDialog
#----------------------------------------------------------------------------
# Name:         vGenerateDocDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vGenerateDocDialog.py,v 1.4 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string,os,os.path,shutil
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.office.vtOfficeSCalc import *
from vidarc.tool.office.vtOfficeSWriter import *
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
#from boa.vApps.vPrjDoc.vgpAddDoc import *
#from boa.vApps.vPrjDoc.vglbPrjDocGrpInfo import *

import images

def create(parent):
    return vGenerateDocDialog(parent)

[wxID_VGENERATEDOCDIALOG, wxID_VGENERATEDOCDIALOGGCBBCANCEL, 
 wxID_VGENERATEDOCDIALOGGCBBOK, wxID_VGENERATEDOCDIALOGGCBBSET, 
 wxID_VGENERATEDOCDIALOGLBLATTR, wxID_VGENERATEDOCDIALOGLSTATTR, 
 wxID_VGENERATEDOCDIALOGTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vGenerateDocDialog(wx.Dialog):
    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsAttr_Items(self.bxsAttr)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)


    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_bxsAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttr, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtVal, 3, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbSet, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsAttr, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lstAttr, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 1, border=8,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGENERATEDOCDIALOG,
              name=u'vGenerateDocDialog', parent=prnt, pos=wx.Point(222, 118),
              size=wx.Size(493, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Document Generate')
        self.SetClientSize(wx.Size(485, 329))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEDOCDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Ok'), name=u'gcbbOk',
              parent=self, pos=wx.Point(148, 291), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGENERATEDOCDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEDOCDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(256, 291),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGENERATEDOCDIALOGGCBBCANCEL)

        self.lstAttr = wx.ListCtrl(id=wxID_VGENERATEDOCDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(4, 38),
              size=wx.Size(472, 245), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VGENERATEDOCDIALOGLSTATTR)

        self.lblAttr = wx.StaticText(id=wxID_VGENERATEDOCDIALOGLBLATTR,
              label=_(u'Attributes'), name=u'lblAttr', parent=self, pos=wx.Point(4,
              4), size=wx.Size(98, 30), style=wx.ALIGN_RIGHT)

        self.txtVal = wx.TextCtrl(id=wxID_VGENERATEDOCDIALOGTXTVAL,
              name=u'txtVal', parent=self, pos=wx.Point(106, 4),
              size=wx.Size(290, 30), style=0, value=u'')

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEDOCDIALOGGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Set'), name=u'gcbbSet',
              parent=self, pos=wx.Point(400, 4), size=wx.Size(76, 30), style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGENERATEDOCDIALOGGCBBSET)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.attr={}
        self.docinfos={}
        self.prjinfos={}
        self.tmplFN=None
        self.sFullName=None
        self.sDoc=''
        self.sRev=''
        self.selectedIdx=-1
        
        img=images.getBuildBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getApplyBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)
        
        # setup list
        self.lstAttr.InsertColumn(0,_(u'Title'))
        self.lstAttr.InsertColumn(1,_(u'Value'),wx.LIST_FORMAT_LEFT,100)
        self.lstAttr.InsertColumn(2,_(u'Place'),wx.LIST_FORMAT_LEFT,80)
        
    def SetTemplateBaseDir(self,dn):
        self.vglbDocGrpInfo.SetTemplateBaseDir(dn)
    def SetFileNames(self,tmplFN,FN):
        self.tmplFN=tmplFN
        self.sFullName=FN
    def __getInfo__(self,info):
        #vtLog.CallStack('')
        #print info
        s=string.split(info,':')
        #print s
        val='---'
        if len(s)>1:
            if s[0]==u'prj':
                try:
                    sInfo=string.split(s[1],'|')
                    if len(sInfo)>1:
                        k=sInfo[0]
                        f=sInfo[1]
                    else:
                        k=s[1]
                        f=None
                    val=self.prjinfos[k]
                    if f is not None:
                        pass
                except:
                    val='---'
        else:
            try:
                sInfo=string.split(info,'|')
                if len(sInfo)>1:
                    k=sInfo[0]
                    f=sInfo[1]
                else:
                    k=info
                    f=None
                val=self.docinfos[k]
                if f is not None:
                    val=val.Format(f)
            except:
                val='---'
            #elif info==u'Date':
            #    dt=wx.DateTime.ToDay()
            #    val=dt.Format('%Y%m%D')
            
        return val
    def Set(self,doc,docinfos,docNode,prjinfos):
        #vtLog.CallStack('')
        #vtLog.pprint(prjinfos)
        #self.sFullName=fn
        self.lstAttr.DeleteAllItems()
        self.doc=doc
        self.docinfos=docinfos
        self.documentNode=docNode
        self.prjinfos=prjinfos
        
        attrNode=self.doc.getChild(self.documentNode,'attributes')
        self.attrs={}
        if attrNode is not None:
            self.attrNode=attrNode
            for o in self.doc.getChilds(attrNode):
                sTagName=self.doc.getTagName(o)
                sPlace=self.doc.getAttribute(o,'place')
                sVal=self.doc.getNodeText(attrNode,sTagName)
                self.attrs[sTagName]=[sVal,sPlace,o]
        keys=self.attrs.keys()
        keys.sort()
        for k in keys:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            try:
                vInfo=self.attrs[k][0]
                if vInfo[0]=='$' and vInfo[-1]=='$':
                    v=self.__getInfo__(vInfo[1:-1])
                else:
                    v=vInfo
                self.lstAttr.SetStringItem(index, 1, v)
                self.lstAttr.SetStringItem(index, 2, self.attrs[k][1])
            except:
                pass
            #self.lstAttr.SetItemData(index,i)
            
    def Generate(self):
        if self.tmplFN is None:
            try:
                sDir=os.path.split(self.sFullName)
                os.makedirs(sDir[0])
            except:
                pass
            f=open(self.sFullName,'w')
            f.close()
            return
        else:
            try:
                sDir=os.path.split(self.sFullName)
                os.makedirs(sDir[0])
            except:
                pass
            shutil.copy(self.tmplFN,self.sFullName)
                    
        sExts=string.split(self.sFullName,'.')
        sExt=sExts[-1]
        if sExt=='sxc':
            appl=vtOfficeSCalc()
            appl.openFile(self.sFullName)
            
            attrNode=self.doc.getChild(self.documentNode,'attributes')
            self.attrs={}
            if attrNode is not None:
                self.attrNode=attrNode
                
                for i in range(0,self.lstAttr.GetItemCount()):
                    it=self.lstAttr.GetItem(i,0)
                    sAttrName=it.m_text
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    self.doc.setNodeText(attrNode,sAttrName,sAttrVal)
                
                for o in self.doc.getChilds(attrNode):
                        sPlace=self.doc.getAttribute(o,'place')
                        sVal=self.doc.getText(o)
                        
                        celladr=string.split(sPlace,'.')
                        if len(celladr)==2:
                            appl.selSheet(celladr[0])
                            appl.setCellByName(celladr[1],sVal)
            if 1==0:
              self.attrs[o.tagName]=[sVal,sPlace,attrNode,o]
              for k in self.attrs.keys():
                vLst=self.attrs[k]
                v=vLst[0]
                sPlace=string.strip(vLst[1])
                if len(sPlace)>0:
                    vInfo=vLst[0]
                    if vInfo[0]=='$' and vInfo[-1]=='$':
                        val=self.__getInfo__(vInfo[1:-1])
                    else:
                        val='---' # fixme
                    o=vLst[-2]
                    self.doc.setNodeTextAttr(o,k,val,'place',vLst[1])
                    celladr=string.split(sPlace,'.')
                    if len(celladr)==2:
                        appl.selSheet(celladr[0])
                        appl.setCellByName(celladr[1],val)
            appl.save()
            pass
        elif sExt=='sxw':
            appl=vtOfficeSWriter()
            appl.openFile(self.sFullName)
            attrNodes=self.doc.getChild(self.documentNode,'attributes')
            self.attrs={}
            if attrNode is not None:
                self.attrNode=attrNode
                
                for i in range(0,self.lstAttr.GetItemCount()):
                    it=self.lstAttr.GetItem(i,0)
                    sAttrName=it.m_text
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    self.doc.setChildsNodeText(attrNode,sAttrName,sAttrVal)
                
                for o in self.doc.getChilds(attrNode):
                        sPlace=self.doc.getAttribute(o,'place')
                        sVal=self.doc.getText(o)
                        
                        appl.replaceAll(sPlace,sVal)
            appl.save()
            pass
        #fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        #mime = fileType.GetMimeType()
        #cmd = fileType.GetOpenCommand(filename, mime)
        #wx.Execute(cmd)
 
    def SetNode(self,node,ids):
        self.vglbDocGrpInfo.SetNode(node,ids)
    def OnGcbbOkButton(self, event):
        self.Generate()
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        self.selectedIdx=idx
        it=self.lstAttr.GetItem(idx,1)
        self.txtVal.SetValue(it.m_text)
        
        event.Skip()
    def OnGcbbSetButton(self,event):
        if self.selectedIdx>=0:
            sVal=self.txtVal.GetValue()
            self.lstAttr.SetStringItem(self.selectedIdx,1,sVal,-1)
            it=self.lstAttr.GetItem(self.selectedIdx,0)
            sAttr=it.m_text
            self.doc.setNodeText(self.attrNode,sAttr,sVal)
