#----------------------------------------------------------------------------
# Name:         vXmlNodeCfgLocal.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061117
# CVS-ID:       $Id: vXmlNodeCfgLocal.py,v 1.2 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vSystem
import platform

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
from vidarc.vApps.vPrjDoc.vDocSelPanel import MODES
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeCfgLocalPanel import vXmlNodeCfgLocalPanel
        #from vXmlNodeCfgLocalEditDialog import *
        #from vXmlNodeCfgLocalAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeCfgLocal(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=[]#[('Key',''),('Title','')]
    def __init__(self,tagName='cfgLocal'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'local configuration')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return 'host'#self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetPrjDN(self,node):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.GetAttrVal(node,'prjDN','host',sHost)
        if len(sDN)>0:
            return sDN
        else:
            return self.GetVal(node,'prjDN',unicode,'')
    def GetMode(self,node):
        return self.Get(node,'mode')
    def SetTag(self,node,val):
        self.Set(node,'tag','host')
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetPrjDN(self,node,sDN):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.SetAttrVal(node,'prjDN',sDN,'host',sHost)
    def SetMode(self,node,val):
        self.Set(node,'mode',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x02\xcdIDAT(\x91-\xceMhTW\x14\xc0\xf1\xf3\xee\xbbs\xf3\xfa\x9c\x19\xf3A\x08\
\xb5\xa4S\x1bj\x13$`\xb4\x91\x18c?t\xa2]\xb8\tAD,\x8a\xa5\x12\x88 \x120\x1b\
\rt\x95\xeeZ\xe96 \xb4n*-*%\x95\x98E\tjCD\x8d\x83F\xe2Pb\xd0I&q\xdeLF3\xef\
\xe3\xcey\xe7\x9d\xb8\xd0\xfd\x1f~\x7f#\x95Je\x9e\xcc)\xa5\x04\x00o\x00\x0b\
\x16 \x04\x08b"f\x0e\x89\x889"\'\xf3\xb4\xef\xf8\x91\x85\xa2#[>m\xd1\x06/\
\xcc\xcd/,-\x08\x96l\x80\x88\x80M\xa2\x88\x890\xd0H\xcc\x9f\xb8N+\xbd\xfa\
\xb7\xbf\xbe\xfbrY\x02\x80\x8a\xe0\xda\x8d\xf1[\x7fL\x99\xcaV\xb1\x84\xaa\
\xb1Hk\xe4 \xd4\xae\xde\xf0\xdbc\xfa\xe0\xa9\x8eM\xcfoO\xce\xe6R\xa6\x90\x00\
\x80&\xd8\xa08\xd1\xd0X\xdbl\xdbI0P\xe3\x1b\x1dP\xe0\xa9VtGN\xeeM,N\x8e\xdd\
\xcd=]\x12\xd3\x1a%\x00(f\x92\xdc`Heml2UU\xb0\x15*09\x11\x04#\'\xda6?\x9f\
\x18\xbb\x93[\\M\x8ck\x1f\x00\x04\x00\x90)\x80\xa4gJ\xf4\xb1\x149\x18T4c\xac\
R\xb8t\xac&\x7f\xeb\xcf\xbff\x96s\x95\xba\xc2\x97\xdd\xc9d-\x00H\x00\x90\x91\
`\x81\xe8\x95\xd6C\xb4<\xe9+%4\xe9\xa8<ug\xde/\xd1\xd5\xac\xaa\xff\xb6\xb3\
\xb7k_\xe6^\xe6\xbd\x80\x06\x85\x0c~\xe4\x07\xae\xe3\xa0\xe3\xaf\x97\xca\xa2\
\xdc?p\xf2\xc7l\xfcJ\xce~\x16o\xe8\xda\xf9\x8dL\xc4\xdf\xc5\x02\x00X3\x87\
\xbe\nA\x93\'\\\xc4\x8d\xb5\x1f\xbe\xeb\xff\xf5\xe7_\x84\x0f\xcb5MM\xc9&\xa5\
Lt}&|\xbf\xe4E\xee\xa9\xd3\xdf\x0f\x9e;C\x1a\x9d\xe2Za\xb54\xfa\xd3h\xdf\xf1\
\xc3_\xf7\xf4\xd8\xa6\x8d\xcc\x93\xc6t\xfc!0\x10\x00\x18_\xec\xda\xbd\xa7{\
\xaf\xaf\x89\x08\x95\xf5\xc1\xfe\xaf\x0e\\\xbc0lY\xb2iKJIIL@\x8c\x11\x01\xd2\
\xfd\xcc\xfd\xc0\xaf\x98\xf9\x95e+n74&A\x18\x8d\xf5\xcd\xbf\xfd>\xf6y[\xdb\
\xc7-\xad\xab/\x9d\xc0+m\xae\xab\xd5U],\xbc\xf2\xb4\xbf\x92\xcf\x01\x80L\xa7\
\xd3\xc3C\xc3\xb3\x8f\xe6\xd3\x07\x0e\x9d\x1d\x1a\xdc\xfaas\xc7\xf6v\xdb\xb6\
\xdb\xb7}&\x01\x18@\x02\xfc7\xf3\xb0\xa3\xb5\xed\x9aR\xb3\x99\x07F\xb9\xec\
\xe5\x9d\xc2\xc4\xdf7\xaf\xdf\xbc\xde\xb9\xa3\xe3\xe8\xb1\x13VM\x9c\x8d\x80|\
x]Y\xb7\xa4EB\x93f\rz)\x9b\x1b:? \xcb\xe5\xb5\xbb\xc5\x99*z\xe9\xce\x9e\xee\
\xde\x83\xb6\x8c\x11V\xc8#7\xd0"\x84\xc7/f\x8a/\xf2%\xedn\xfbh\xabg\x84X%9\
\xf1\xcfTv\xf1\x81\xeb\xfa\x0c\xc2\x19\xbfJ\xc4LDH\x18\x92m\xd7\xbd^[!\xc2*\
\x86\xffg\x9f\x11\x93\x88\x89\xb7\x8f0\x9c\xcb\x96\xf0\xa5\xc9\x00\x00\x00\
\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeCfgLocalEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnCfgLocal',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeCfgLocalAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnCfgLocal',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeCfgLocalPanel
        else:
            return None
