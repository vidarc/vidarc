#Boa:FramePanel:vPrjDocInfoPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocInfoPanel.py,v 1.3 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.numctrl
import wx.grid
import wx.lib.buttons
import sys,string,os,os.path,shutil
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjDoc.vGenerateDocDialog import *
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import images

[wxID_VPRJDOCINFOPANEL, wxID_VPRJDOCINFOPANELGCBBAPPLY, 
 wxID_VPRJDOCINFOPANELGCBBCANCEL, wxID_VPRJDOCINFOPANELGCBBNEWREV, 
 wxID_VPRJDOCINFOPANELLBLAUTHOR, wxID_VPRJDOCINFOPANELLBLCURDATE, 
 wxID_VPRJDOCINFOPANELLBLDATE, wxID_VPRJDOCINFOPANELLBLPRJDESC, 
 wxID_VPRJDOCINFOPANELLBLREV, wxID_VPRJDOCINFOPANELLBLTITLE, 
 wxID_VPRJDOCINFOPANELLBLVERSION, wxID_VPRJDOCINFOPANELLSTATTRS, 
 wxID_VPRJDOCINFOPANELTXTAUTHOR, wxID_VPRJDOCINFOPANELTXTDATE, 
 wxID_VPRJDOCINFOPANELTXTDESC, wxID_VPRJDOCINFOPANELTXTREV, 
 wxID_VPRJDOCINFOPANELTXTTITLE, wxID_VPRJDOCINFOPANELTXTVERSION, 
] = [wx.NewId() for _init_ctrls in range(18)]

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOCUMENT_ADDED=wx.NewEventType()
def EVT_VGPPRJDOCUMENT_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOCUMENT_ADDED,func)
class vgpPrjDocumentAdded(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOCUMENT_ADDED(<widget_name>, self.OnPrjDocumentAdded)
    """

    def __init__(self,obj,node,fn):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOCUMENT_ADDED)
        self.obj=obj
        self.node=node
        self.fn=fn
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetFN(self):
        return self.fn

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOCUMENT_CHANGED=wx.NewEventType()
def EVT_VGPPRJDOCUMENT_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOCUMENT_CHANGED,func)
class vgpPrjDocumentChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOCUMENT_ADDED(<widget_name>, self.OnPrjDocumentAdded)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOCUMENT_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLang(self):
        return self.lang


class vPrjDocInfoPanel(wx.Panel):
    def _init_coll_bxsRev_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRev, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtRev, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.gcbbNewRev, 0, border=0, flag=0)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtTitle, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblAuthor, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtAuthor, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblVersion, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtVersion, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTitle, 1, border=4, flag=wx.EXPAND | wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.fgsInfo, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lstAttrs, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsRev, 0, border=0, flag=0)
        parent.AddSizer(self.bxsDate, 0, border=0, flag=0)

    def _init_coll_bxsDate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.txtDate, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblCurDate, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)

    def _init_coll_bxBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbApply, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=4, flag=wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsInfo = wx.FlexGridSizer(cols=3, hgap=0, rows=2, vgap=0)

        self.bxsRev = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTitle_Items(self.bxsTitle)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)
        self._init_coll_bxsRev_Items(self.bxsRev)
        self._init_coll_bxsDate_Items(self.bxsDate)
        self._init_coll_bxBt_Items(self.bxBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCINFOPANEL,
              name=u'vPrjDocInfoPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(736, 187), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(728, 160))

        self.lblTitle = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self, pos=wx.Point(0, 4),
              size=wx.Size(93, 21), style=wx.ALIGN_RIGHT)

        self.txtTitle = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(97, 4),
              size=wx.Size(182, 21), style=0, value=u'')
        self.txtTitle.Enable(False)

        self.lstAttrs = wx.ListCtrl(id=wxID_VPRJDOCINFOPANELLSTATTRS,
              name=u'lstAttrs', parent=self, pos=wx.Point(412, 29),
              size=wx.Size(240, 101), style=wx.LC_REPORT)
        self.lstAttrs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VPRJDOCINFOPANELLSTATTRS)

        self.lblAuthor = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLAUTHOR,
              label=_(u'Author'), name=u'lblAuthor', parent=self, pos=wx.Point(283,
              4), size=wx.Size(89, 21), style=wx.ALIGN_RIGHT)

        self.txtAuthor = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTAUTHOR,
              name=u'txtAuthor', parent=self, pos=wx.Point(376, 4),
              size=wx.Size(89, 21), style=0, value=u'')
        self.txtAuthor.Enable(False)

        self.lblPrjDesc = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLPRJDESC,
              label=_(u'Description'), name=u'lblPrjDesc', parent=self,
              pos=wx.Point(0, 29), size=wx.Size(53, 101), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(57, 29),
              size=wx.Size(351, 101), style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetExtraStyle(0)

        self.lblVersion = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLVERSION,
              label=_(u'Version'), name=u'lblVersion', parent=self,
              pos=wx.Point(469, 4), size=wx.Size(89, 21), style=wx.ALIGN_RIGHT)

        self.txtVersion = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTVERSION,
              name=u'txtVersion', parent=self, pos=wx.Point(562, 4),
              size=wx.Size(89, 21), style=0, value=u'')
        self.txtVersion.Enable(False)

        self.lblRev = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLREV,
              label=_(u'New Revision'), name=u'lblRev', parent=self,
              pos=wx.Point(53, 130), size=wx.Size(72, 30),
              style=wx.ALIGN_RIGHT)

        self.txtRev = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTREV,
              name=u'txtRev', parent=self, pos=wx.Point(129, 130),
              size=wx.Size(68, 30), style=wx.TE_RICH2, value=u'')

        self.lblDate = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLDATE,
              label=_(u'Date'), name=u'lblDate', parent=self, pos=wx.Point(412,
              130), size=wx.Size(46, 21), style=wx.ALIGN_RIGHT)

        self.lblCurDate = wx.StaticText(id=wxID_VPRJDOCINFOPANELLBLCURDATE,
              label=u'', name=u'lblCurDate', parent=self, pos=wx.Point(558,
              130), size=wx.Size(50, 21), style=0)

        self.txtDate = wx.TextCtrl(id=wxID_VPRJDOCINFOPANELTXTDATE,
              name=u'txtDate', parent=self, pos=wx.Point(462, 130),
              size=wx.Size(96, 21), style=wx.TE_RICH2, value=u'')

        self.gcbbNewRev = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCINFOPANELGCBBNEWREV,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'New Rev'),
              name=u'gcbbNewRev', parent=self, pos=wx.Point(197, 130),
              size=wx.Size(82, 30), style=0)
        self.gcbbNewRev.Bind(wx.EVT_BUTTON, self.OnGcbbNewRevButton,
              id=wxID_VPRJDOCINFOPANELGCBBNEWREV)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCINFOPANELGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'gcbbApply',
              parent=self, pos=wx.Point(656, 25), size=wx.Size(72, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VPRJDOCINFOPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCINFOPANELGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(656, 59),
              size=wx.Size(72, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJDOCINFOPANELGCBBCANCEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.ids=None
        self.lang=None
        self.attrNode=None
        self.prjDN=None
        self.dlgGen=None
        self.selectedIdx=-1
        self.docs=[]
        self.attrs={}
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbNewRev.SetBitmapLabel(img)
        
        # setup list
        self.lstAttrs.InsertColumn(0,_(u'Attribute'))
        self.lstAttrs.InsertColumn(1,_(u'Value'),wx.LIST_FORMAT_LEFT,100)
        self.SetupImageList()
    def Clear(self):
        self.txtTitle.SetValue('')
        self.txtAuthor.SetValue('')
        self.txtVersion.SetValue('')
        self.txtRev.SetValue('')
        self.txtDesc.SetValue('')
        self.txtDate.SetValue('')
        self.lstAttrs.DeleteAllItems()
        self.node=None
        self.selectedIdx=-1
        self.docs=[]
        self.attrs={}
        
    def Lock(self,flag):
        if flag:
            self.txtRev.Enable(False)
            self.txtDesc.Enable(False)
            self.txtDate.Enable(False)
            self.gcbbNewRev.Enable(False)
            self.gcbbApply.Enable(False)
            self.gcbbCancel.Enable(False)
        else:
            self.txtRev.Enable(True)
            self.txtDesc.Enable(True)
            self.txtDate.Enable(True)
            self.gcbbNewRev.Enable(True)
            self.gcbbApply.Enable(True)
            self.gcbbCancel.Enable(True)
    def GetLanguage(self):
        return self.lang
    def SetPrjDir(self,dn):
        self.prjDN=dn
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node,lang):
        self.lstAttrs.DeleteAllItems()
        self.selectedIdx=-1
        
        if node is None:
            self.txtTitle.SetValue('')
            self.txtDesc.SetValue('')
            self.txtVersion.SetValue('')
            self.txtAuthor.SetValue('')
            self.lstAttrs.DeleteAllItems()
            self.selectedIdx=-1
            return
        self.node=node['document']
        self.lang=lang
        
        sTitle=self.doc.getNodeText(self.node,'title')
        sDesc=self.doc.getNodeText(self.node,'description')
        sVersion=self.doc.getNodeText(self.node,'version')
        sDate=self.doc.getNodeText(self.node,'date')
        sAuthor=self.doc.getNodeText(self.node,'author')
        
        self.txtTitle.SetValue(sTitle)
        self.txtDesc.SetValue(sDesc)
        self.txtVersion.SetValue(sVersion)
        self.txtAuthor.SetValue(sAuthor)
        
        dt=wx.DateTime.Now()
        sDate=dt.FormatISODate()
        self.lblCurDate.SetLabel(sDate)
        self.txtDate.SetLabel(sDate)
        # setup attributes
        self.lstAttrs.DeleteAllItems()
        self.selectedIdx=-1
        
        attrNodes=self.doc.getChilds(self.node,'attributes')
        self.attrs={}
        if len(attrNodes)>0:
            attrNode=attrNodes[0]
            self.attrNode=attrNode
            for o in self.doc.getChilds(attrNode):
                sTag=self.doc.getTagName(o)
                self.attrs[sTag]=[self.doc.getNodeText(attrNode,sTag),o]
        keys=self.attrs.keys()
        keys.sort()
        for k in keys:
            index = self.lstAttrs.InsertImageStringItem(sys.maxint, k, -1)
            self.lstAttrs.SetStringItem(index, 1, self.attrs[k][0])
        pass
    def GetNode(self,node):
        if node is None:
            return ''
        sTitle=self.txtTitle.GetValue()
        sDesc=self.txtDesc.GetValue()
        sVersion=self.txtVersion.GetValue()
        sAuthor=self.txtAuthor.GetValue()
                
        self.doc.setNodeText(node,'title',sTitle)
        self.doc.setNodeText(node,'description',sDesc)
        self.doc.setNodeText(node,'version',sVersion)
        self.doc.setNodeText(node,'author',sAuthor)
        
        attrNode=self.doc.getChild(node,'attributes')
        if attrNode is None:
            attrNode=self.doc.createSubNode(node,'attributes',False)
        for i in range(0,self.lstAttrs.GetItemCount()):
            it=self.lstAttrs.GetItem(i)
            sAttrName=it.m_text
            it=self.lstAttrs.GetItem(i,1)
            sAttrVal=it.m_text
            self.doc.setNodeText(attrNode,sAttrName,sAttrVal)
        self.doc.AlignNode(node,iRec=1)
        self.doc.doEdit(node)
        return sTitle
        
    def OnGcbbApplyButton(self, event):
        self.GetNode(self.node)
        wx.PostEvent(self,vgpPrjDocumentChanged(self,self.node,self.lang))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode({'document':self.node},self.ids)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        
        it=self.lstAttrs.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttrs.GetItem(idx,1)
        sAttrVal=it.m_text
        lst=self.attrs[sAttrName]
        self.attrs[sAttrName]=[sAttrVal,lst[1]]
        
        self.selectedIdx=it.m_itemId
        event.Skip()
    def OnGcbbNewRevButton(self,event):
        if self.dlgGen is None:
            self.dlgGen=vGenerateDocDialog(self)
        bFault=False
        sRev=self.txtRev.GetValue()
        if len(sRev)<=0:
            sRev='??'
            bFault=True
        else:
            sRepl=fnUtil.replaceSuspectChars(sRev)
            if sRepl!=sRev:
                sRev=sRepl
                bFault=True
        sFN=self.doc.getNodeText(self.node,'filename')
        j=string.rfind(sFN,'.')
        if j<0:
            bFault=True
        else:
            i=string.rfind(sFN[:j],'_')
            if i<0:
                bFault=True
        if bFault==False:
            #so far so good
            sNewFN=sFN[:i+1]+sRev+sFN[j:]
            fn=fnUtil.getAbsoluteFilenameRel2BaseDir(sNewFN,self.prjDN)
            try:
                mode=os.stat(fn)
                bFault=True
            except:
                filename=fnUtil.getFilenameRel2BaseDir(fn,self.prjDN)
        if bFault:
            # revision is not possible
            self.txtRev.SetValue(sRev)
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
        else:
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
            docNew=self.doc.cloneNode(self.node,True)
            par=self.doc.getParent(self.node)
            try:
                sOldRev=self.attrs['Revision'][0]
            except:
                sOldRev=self.doc.getNodeText(self.node,'version')
                #vtXmlDomTree.setNodeText(docNew,'Revision',sRev)
                #vtXmlDomTree.setNodeText(docNew,'RevDate',self.txtDate.GetValue())
            #try:
            #    sDir=os.path.split(self.sFullName)
            #    os.makedirs(sDir[0])
            #except:
            #    pass
            oldFN=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
            #shutil.copy(oldFN,fn)

            self.dlgGen.SetFileNames(oldFN,fn)

            self.docinfos={}
            attrNode=self.doc.getChild(docNew,'attributes')
            #if attrNode is not None:
                #for o in self.doc.getChilds(attrNode):
                #    self.docinfos[self.doc.getTagName(o)]=self.doc.getText(o)
                ##self.docinfos['DocTitle']=sTitle
                #self.doc.setNodeText(attrNode,'Revision',sRev)
                #self.doc.setNodeText(attrNode,'PrevRevision',sOldRev)
                ##vtXmlDomTree.setChildsNodeText(attrNode,'DocDate',vtXmlDomTree.getNodeText(docNew,'DocDate'),doc)
                #self.doc.setNodeText(attrNode,'RevDate',self.txtDate.GetValue())
            self.doc.setNodeText(docNew,'filename',sNewFN)
            self.doc.setNodeText(docNew,'version',sRev)
            dt=wx.DateTime.Now()
            sDate=dt.Format("%Y%m%D_%H%M%S")
            self.doc.setNodeText(docNew,'date',sDate) #childs??
            self.doc.setNodeText(docNew,'description',self.txtDesc.GetValue())
            
            dt=vtDateTime(True)
            self.docinfos['Revision']=sRev
            self.docinfos['PrevRevision']=sOldRev
            self.docinfos['DocDate']=self.doc.getNodeText(docNew,'DocDate')
            self.docinfos['RevDate']=self.txtDate.GetValue()
            self.docinfos['DocDateTime']=self.doc.getNodeText(docNew,'DocDateTime')
            self.docinfos['RevDateTime']=dt.GetDateTimeStr()
            
            keys=self.docinfos.keys()
            keys.sort()
            for k in keys:
                self.doc.setNodeText(docNew,k,self.docinfos[k])
                
            self.dlgGen.Set(self.doc,self.docinfos,docNew,{})
            ret=self.dlgGen.ShowModal()
            if ret==1:
                
                self.doc.appendChild(par,docNew)
                self.doc.AlignNode(docNew,iRec=2)
                wx.PostEvent(self,vgpPrjDocumentAdded(self,docNew,fn))
            
            #wxPostEvent(self,vgpDocSelDocAdded(self,self.node,
            #    self.documentNode,self.sFullName))

    
