#Boa:FramePanel:vPrjDocPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocPanel.py,v 1.7 2007/01/14 14:31:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.lang.vtLgSelector
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import wx.lib.anchors as anchors
from vidarc.vApps.vDoc.vXmlDocTree  import *
import sys,string,os,os.path,platform
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.vApps.vPrjDoc.vEditDocDialog import *
from vidarc.vApps.vPrjDoc.vGenerateArchDialog import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *

import images

[wxID_VPRJDOCPANEL, wxID_VPRJDOCPANELGCBBDEL, wxID_VPRJDOCPANELGCBBEDIT, 
 wxID_VPRJDOCPANELGCBBGENARCH, wxID_VPRJDOCPANELGCBBOPEN, 
 wxID_VPRJDOCPANELLBLDESC, wxID_VPRJDOCPANELLBLTITLE, 
 wxID_VPRJDOCPANELLSTARCH, wxID_VPRJDOCPANELLSTDOCS, 
 wxID_VPRJDOCPANELNBDOCARCH, wxID_VPRJDOCPANELPNARCH, wxID_VPRJDOCPANELPNDOC, 
 wxID_VPRJDOCPANELTGSHOWALL, wxID_VPRJDOCPANELTXTDESC, 
 wxID_VPRJDOCPANELTXTTITLE, wxID_VPRJDOCPANELVILANG, 
] = [wx.NewId() for _init_ctrls in range(16)]

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOC_DELETED=wx.NewEventType()
def EVT_VGPPRJDOC_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_DELETED,func)
class vgpPrjDocDelted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPRJDOC_DELETED(<widget_name>, self.OnDocDeleted)
    """

    def __init__(self,obj,node,fn):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_DELETED)
        self.obj=obj
        self.node=node
        self.fn=fn
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetFN(self):
        return self.fn


class vPrjDocPanel(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def _init_coll_bxsArchBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbGenArch, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbDel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsDoc_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsDocTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtTitle, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDoc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstDocs, 1, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsDocBt, 0, border=4, flag=wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsDocTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsDocDesc, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDocBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbEdit, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.gcbbOpen, 0, border=4,
              flag=wx.LEFT | wx.RIGHT | wx.TOP)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.tgShowAll, 0, border=4,
              flag=wx.LEFT | wx.RIGHT | wx.TOP)
        parent.AddWindow(self.viLang, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP)

    def _init_coll_fxsArch_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsDocDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fxsArch_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstArch, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsArchBt, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.EXPAND)

    def _init_coll_nbDocArch_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnDoc, select=True,
              text=_(u'Documents'))
        parent.AddPage(imageId=-1, page=self.pnArch, select=False,
              text=_(u'Archives'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDoc = wx.FlexGridSizer(cols=2, hgap=0, rows=5, vgap=4)

        self.bxsDocBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsDocTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDocDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fxsArch = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsArchBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsDoc_Items(self.fgsDoc)
        self._init_coll_fgsDoc_Growables(self.fgsDoc)
        self._init_coll_bxsDocBt_Items(self.bxsDocBt)
        self._init_coll_bxsDocTitle_Items(self.bxsDocTitle)
        self._init_coll_bxsDocDesc_Items(self.bxsDocDesc)
        self._init_coll_fxsArch_Items(self.fxsArch)
        self._init_coll_fxsArch_Growables(self.fxsArch)
        self._init_coll_bxsArchBt_Items(self.bxsArchBt)

        self.pnArch.SetSizer(self.fxsArch)
        self.pnDoc.SetSizer(self.fgsDoc)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCPANEL, name=u'vPrjDocPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(477, 262),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(469, 235))
        self.SetAutoLayout(True)

        self.nbDocArch = wx.Notebook(id=wxID_VPRJDOCPANELNBDOCARCH,
              name=u'nbDocArch', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(464, 229), style=0)
        self.nbDocArch.SetConstraints(LayoutAnchors(self.nbDocArch, True, True,
              True, True))
        self.nbDocArch.SetAutoLayout(True)

        self.pnDoc = wx.Panel(id=wxID_VPRJDOCPANELPNDOC, name=u'pnDoc',
              parent=self.nbDocArch, pos=wx.Point(0, 0), size=wx.Size(456, 203),
              style=wx.TAB_TRAVERSAL)

        self.pnArch = wx.Panel(id=wxID_VPRJDOCPANELPNARCH, name=u'pnArch',
              parent=self.nbDocArch, pos=wx.Point(0, 0), size=wx.Size(456, 203),
              style=wx.TAB_TRAVERSAL)
        self.pnArch.SetAutoLayout(True)

        self.lstArch = wx.ListCtrl(id=wxID_VPRJDOCPANELLSTARCH, name=u'lstArch',
              parent=self.pnArch, pos=wx.Point(4, 4), size=wx.Size(368, 195),
              style=wx.LC_REPORT)
        self.lstArch.SetMinSize(wx.Size(-1, -1))
        self.lstArch.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstArchListItemSelected, id=wxID_VPRJDOCPANELLSTARCH)

        self.gcbbGenArch = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCPANELGCBBGENARCH,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Generate'),
              name=u'gcbbGenArch', parent=self.pnArch, pos=wx.Point(376, 4),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenArch.SetMinSize(wx.Size(-1, -1))
        self.gcbbGenArch.Bind(wx.EVT_BUTTON, self.OnGcbbGenArchButton,
              id=wxID_VPRJDOCPANELGCBBGENARCH)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Delete'),
              name=u'gcbbDel', parent=self.pnArch, pos=wx.Point(376, 38),
              size=wx.Size(76, 30), style=0)
        self.gcbbDel.SetMinSize(wx.Size(-1, -1))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VPRJDOCPANELGCBBDEL)

        self.lstDocs = wx.ListCtrl(id=wxID_VPRJDOCPANELLSTDOCS, name=u'lstDocs',
              parent=self.pnDoc, pos=wx.Point(4, 4), size=wx.Size(364, 148),
              style=wx.LC_REPORT)
        self.lstDocs.SetMinSize(wx.Size(-1, -1))
        self.lstDocs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstDocsListItemSelected, id=wxID_VPRJDOCPANELLSTDOCS)

        self.gcbbEdit = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCPANELGCBBEDIT,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Edit'), name=u'gcbbEdit',
              parent=self.pnDoc, pos=wx.Point(376, 4), size=wx.Size(76, 30),
              style=0)
        self.gcbbEdit.SetMinSize(wx.Size(-1, -1))
        self.gcbbEdit.Bind(wx.EVT_BUTTON, self.OnGcbbEditButton,
              id=wxID_VPRJDOCPANELGCBBEDIT)

        self.lblTitle = wx.StaticText(id=wxID_VPRJDOCPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self.pnDoc,
              pos=wx.Point(0, 152), size=wx.Size(122, 21),
              style=wx.ALIGN_RIGHT)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.txtTitle = wx.TextCtrl(id=wxID_VPRJDOCPANELTXTTITLE,
              name=u'txtTitle', parent=self.pnDoc, pos=wx.Point(126, 152),
              size=wx.Size(241, 21), style=0, value=u'')
        self.txtTitle.SetMinSize(wx.Size(-1, -1))

        self.lblDesc = wx.StaticText(id=wxID_VPRJDOCPANELLBLDESC,
              label=_(u'Description'), name=u'lblDesc', parent=self.pnDoc,
              pos=wx.Point(0, 173), size=wx.Size(122, 30),
              style=wx.ALIGN_RIGHT)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.txtDesc = wx.TextCtrl(id=wxID_VPRJDOCPANELTXTDESC, name=u'txtDesc',
              parent=self.pnDoc, pos=wx.Point(126, 173), size=wx.Size(241, 30),
              style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetMinSize(wx.Size(-1, -1))

        self.gcbbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCPANELGCBBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Open'), name=u'gcbbOpen',
              parent=self.pnDoc, pos=wx.Point(376, 38), size=wx.Size(76, 30),
              style=0)
        self.gcbbOpen.SetMinSize(wx.Size(-1, -1))
        self.gcbbOpen.Bind(wx.EVT_BUTTON, self.OnGcbbOpenButton,
              id=wxID_VPRJDOCPANELGCBBOPEN)

        self.tgShowAll = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VPRJDOCPANELTGSHOWALL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'All'), name=u'tgShowAll',
              parent=self.pnDoc, pos=wx.Point(376, 88), size=wx.Size(76, 30),
              style=0)
        self.tgShowAll.SetToggle(True)
        self.tgShowAll.SetMinSize(wx.Size(-1, -1))
        self.tgShowAll.Bind(wx.EVT_BUTTON, self.OnTgShowAllButton,
              id=wxID_VPRJDOCPANELTGSHOWALL)

        self.viLang = vidarc.tool.lang.vtLgSelector.vtLgSelector(id=wxID_VPRJDOCPANELVILANG,
              name=u'viLang', parent=self.pnDoc, pos=wx.Point(399, 122),
              size=wx.Size(30, 30), style=0)
        self.viLang.Bind(vidarc.tool.lang.vtLgSelector.vEVT_VTLANG_SELECTION_CHANGED,
              self.OnViLangVtlangSelectionChanged)

        self._init_coll_nbDocArch_Pages(self.nbDocArch)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        
        self.node=None
        self.lang='en'
        self.netDoc=None
        self.docs=[]
        self.archs=[]
        self.selectedIdx=-1
        self.selArchIdx=-1
        self.sFN=None
        self.prjDN=''
        self.dlgEdit=None
        self.dlgArch=None
        
        img=images.getDocEditBitmap()
        self.gcbbOpen.SetBitmapLabel(img)
        
        img=images.getEditDocBitmap()
        self.gcbbEdit.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenArch.SetBitmapLabel(img)

        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getViewAllBitmap()
        self.tgShowAll.SetBitmapLabel(img)
        self.tgShowAll.SetBitmapSelected(img)
        
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getArchTmplBitmap()
        self.imgLstTyp.Add(img)
        img=images.getArchBitmap()
        self.imgLstTyp.Add(img)
        self.lstArch.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.il = wx.ImageList(16, 16)
        self.il.Add(vtArt.getBitmap(vtArt.Open))
        self.il.Add(vtArt.getBitmap(vtArt.Zip))
        self.nbDocArch.SetImageList(self.il)
        self.nbDocArch.SetPageImage(0,0)
        self.nbDocArch.SetPageImage(1,1)
        # setup list
        #self.lstDocs.InsertColumn(0,u'Doc')
        #self.lstDocs.InsertColumn(0,u'nr',wx.LIST_FORMAT_RIGHT,30)
        self.lstDocs.InsertColumn(0,_(u'Filename'),wx.LIST_FORMAT_RIGHT,430)

        self.lstArch.InsertColumn(0,_(u'Filename'),wx.LIST_FORMAT_LEFT,430)
        
        self.widLogging=vtLog.GetPrintMsgWid(self)
        
        self.Move(pos)
        self.SetSize(size)
    def Stop(self):
        pass
    def IsBusy(self):
        return False
    def UpdateLang(self):
        if self.doc is not None:
            self.SetLanguage(self.doc.GetLang())
            self.RefreshDocuments()
        pass
    def Clear(self):
        self.txtTitle.SetValue('')
        self.txtDesc.SetValue('')
        self.lstDocs.DeleteAllItems()
        self.lstArch.DeleteAllItems()
        self.docs=[]
        self.archs=[]
        self.selectedIdx=-1
        self.selArchIdx=-1
        self.node=None
        self.sFN=None
    def Lock(self,flag):
        if flag:
            self.txtTitle.Enable(False)
            self.txtDesc.Enable(False)
            #self.gcbbOpen.Enable(False)
            #self.gcbbEdit.Enable(False)
            #self.gcbbGenArch.Enable(False)
            #self.gcbbDel.Enable(False)
            self.lstDocs.Enable(False)
            self.lstArch.Enable(False)
        else:
            self.txtTitle.Enable(True)
            self.txtDesc.Enable(True)
            #self.gcbbOpen.Enable(True)
            #self.gcbbEdit.Enable(True)
            #self.gcbbGenArch.Enable(True)
            #self.gcbbDel.Enable(True)
            self.lstDocs.Enable(True)
            self.lstArch.Enable(True)
    def SetLanguage(self,lang):
        self.lang=lang
    def SetProjectDN(self,prjDN):
        vtLog.vtLngCurWX(vtLog.DEBUG,'prjDN:%s'%(prjDN),self)
        self.prjDN=prjDN
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        self.viLang.SetDoc(self.doc)
    def SetDocNetDoc(self,doc,bNet=False):
        self.netDoc=doc
    def OnGotContent(self,evt):
        self.Clear()
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        try:
            id=self.doc.getKey(self.node)
            if self.doc.isSameKey(self.node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    self.Lock(False)
                else:
                    self.Lock(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnUnLock(self,evt):
        evt.Skip()
        try:
            if self.doc.isSameKey(self.node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['released']:
                    self.Lock(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        try:
            self.Clear()
            self.node=node
            
            if node is not None:
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
                sTag=self.doc.getTagName(node)
                self.gcbbOpen.Enable(self.doc.IsAclOk('document',self.doc.ACL_MSK_READ))
                self.gcbbEdit.Enable(self.doc.IsAclOk(sTag,self.doc.ACL_MSK_READ))
                self.gcbbGenArch.Enable(self.doc.IsAclOk('archivetmpl',self.doc.ACL_MSK_BUILD))
                self.gcbbDel.Enable(self.doc.IsAclOk('archivetmpl',self.doc.ACL_MSK_DEL))
            else:
                vtLog.vtLngCurWX(vtLog.DEBUG,'node is None',self)
                self.gcbbOpen.Enable(False)
                self.gcbbEdit.Enable(False)
                self.gcbbGenArch.Enable(False)
                self.gcbbDel.Enable(False)
            
            self.RefreshDocuments()
            self.RefreshArchs()
        except:
            vtLog.vtLngTB(self.GetName())
    def __addArch__(self,archNode):
        d={}
        for sAttr in ['title','filename','directory','author','version','date']:
            sVal=self.doc.getNodeText(archNode,sAttr)
            d[sAttr]=sVal
        d['archive']=archNode
        self.archs.append(d)
    def RefreshArchs(self):
        self.lstArch.DeleteAllItems()
        
        self.archs=[]
        if self.doc is None:
            return
        if self.doc.IsAclOk('archivetmpl',self.doc.ACL_MSK_READ):
            archNodes=self.doc.getChilds(self.node,'archivetmpl')
            for archNode in archNodes:
                self.__addArch__(archNode)
        iTmpl=len(self.archs)
        if self.doc.IsAclOk('archive',self.doc.ACL_MSK_READ):
            archNodes=self.doc.getChilds(self.node,'archive')
            for archNode in archNodes:
                self.__addArch__(archNode)
        i=0
        for arch in self.archs:
            if i<iTmpl:
                iImgIdx=0
            else:
                iImgIdx=1
            index = self.lstArch.InsertImageStringItem(sys.maxint, arch['filename'], iImgIdx)
            self.lstArch.SetItemData(index,i)
            #if i==self.selectedIdx:
            #    self.lstArch.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            i=i+1
        
    def __addDoc__(self,docNode):
        if self.tgShowAll.GetValue():
            bLang=True
        else:
            l=self.doc.getAttribute(docNode,'language')
            bLang=False
            if len(l)<=0:# and self.lang=='en':
                bLang=True
            elif self.lang==l:
                bLang=True
        if bLang==True:
            d={}
            for sAttr in ['title','type','filename','author','version','date']:
                sVal=self.doc.getNodeText(docNode,sAttr)
                d[sAttr]=sVal
            d['document']=docNode
            self.docs.append(d)
    def __getDocGrp__(self,node):
        if self.doc is None:
            return None
        if node is None:
            return None
        sTag=self.doc.getTagName(node)
        while sTag!='docgroup':
            node=self.doc.getParent(node)
            if node is None:
                return None
            sTag=self.doc.getTagName(node)
        return node
    def RefreshDocuments(self):
        # setup attributes
        self.lstDocs.DeleteAllItems()
        self.docs=[]
        if self.doc is None:
            return
        if self.doc.IsAclOk('document',self.doc.ACL_MSK_READ)==False:
            return
        id=self.doc.getKey(self.node)
        nodeDocGrp=self.__getDocGrp__(self.node)
        docNodes=self.doc.getChilds(nodeDocGrp,'document')
        for docNode in docNodes:
            self.__addDoc__(docNode)
        def cmpFunc(da,db):
            iRet=cmp(da['filename'],db['filename'])
            if iRet==0:
                return cmp(da['date'],db['date'])
            return iRet
        self.docs.sort(cmpFunc)
        i=0
        for doc in self.docs:
            index = self.lstDocs.InsertImageStringItem(sys.maxint, doc['filename'], -1)
            self.lstDocs.SetItemData(index,i)
            #if i==self.selectedIdx:
            #    self.lstDocs.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            if self.doc.isSameKey(doc['document'],id):
                self.selectedIdx=index
                self.lstDocs.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                self.lstDocs.EnsureVisible(self.selectedIdx)
            i=i+1
        pass
    def OnDocGrpChanged(self,event):
        self.RefreshDocuments()
        wxPostEvent(self,vgpPrjDocGrpInfoChanged(self,self.node,None))
    def OnDocAdded(self,event):
        docNode=event.GetNode()
        self.__addDoc__(docNode)
        doc=self.docs[-1]
        index = self.lstDocs.InsertImageStringItem(sys.maxint, doc['filename'], -1)
        i=len(self.docs)-1
        self.lstDocs.SetItemData(index,i)
        wx.PostEvent(self,vgpPrjDocumentAdded(self,event.GetNode(),event.GetFN()))
    def OnLstDocsListItemSelected(self, event):
        idx=event.GetIndex()
        try:
            i=self.lstDocs.GetItemData(idx)
            doc=self.docs[i]
            self.sFN=doc['filename']
        except:
            pass
        d=doc['document']
        self.txtTitle.SetValue(self.doc.getNodeText(d,'title'))
        self.txtDesc.SetValue(self.doc.getNodeText(d,'description'))
        event.Skip()
    def OnGcbbOpenButton(self, event):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.sFN is None:
            return
        #oPrjDocs=self.doc.GetReg('prjdocs')
        #self.prjDN=oPrjDocs.GetPrjDN(self.node)
        self.prjDN=self.doc.GetPrjDN()
        sExts=self.sFN.split('.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        if self.sFN[0]=='.':
            filename=fnUtil.getAbsoluteFilenameRel2BaseDir(self.sFN,self.prjDN)
        else:
            filename=self.sFN
        try:
            mime = fileType.GetMimeType()
            if sys.platform=='win32':
                filename=filename.replace('/','\\')
            cmd = fileType.GetOpenCommand(filename, mime)
            wx.Execute(cmd)
            vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
            try:
                vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
            except:
                pass
            #os.popen(cmd)
        except:
            vtLog.vtLngTB(self.GetName())
            sMsg=u"Cann't open %s!"%(filename)
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjDoc',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            
        event.Skip()
    
    def OnGcbbEditButton(self,event):
        if self.dlgEdit is None:
            self.dlgEdit=vEditDocDialog(self)
            EVT_VGPPRJDOCUMENT_ADDED(self.dlgEdit,self.OnDocAdded)
            EVT_VGPPRJDOC_GRP_INFO_CHANGED(self.dlgEdit,self.OnDocGrpChanged)
            self.dlgEdit.SetDoc(self.doc,True)
        self.prjDN=self.doc.GetPrjDN()
        self.dlgEdit.SetPrjDir(self.prjDN)
        if self.doc.getTagName(self.node)!='docgroup':
            node=self.doc.getParent(self.node)
        else:
            node=self.node
        #self.dlgEdit.SetNode(self.node)
        #self.doc.startEdit(self.node)
        self.lstDocs.DeleteAllItems()
        self.docs=[]
        self.dlgEdit.SetNode(node)
        self.doc.startEdit(node)
        try:
            ret=self.dlgEdit.ShowModal()
        except:
            vtLog.vtLngTB(self.GetName())
        #if ret==1:
        #    self.doc.doEdit(self.node)
        #else:
        self.doc.endEdit(node)
        self.RefreshDocuments()
        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
        event.Skip()

    def OnGcbbGenArchButton(self,event):
        if self.selArchIdx<0:
            return
        if self.selArchIdx<len(self.archs):
            # this is a archive template
            if self.dlgArch is None:
                self.dlgArch=vGenerateArchDialog(self)
            
            oPrjDocs=self.doc.GetReg('prjdocs')
            self.prjDN=oPrjDocs.GetPrjDN(self.node)
            self.dlgArch.Centre()
            archNode=self.archs[self.selArchIdx]['archive']
            oArchTmpl=self.doc.GetRegByNode(archNode)
            o=self.doc.getChild(archNode,'filename')
            sFN=self.doc.getText(o)
            #o=self.doc.getChild(archNode,'directory')
            #sDN=self.doc.getText(o)
            sDN=oArchTmpl.GetDN(archNode)
            sFN=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
            
            dt=wx.DateTime.Today()
            sDate=dt.Format("%Y%m%d")
            
            iExtPos=string.rfind(sFN,'.')
            sExt=sFN[iExtPos:]
            sNode=platform.node()
            for i in range(0,26):
                c=chr(97+i)
                s=sFN[:iExtPos]+'_'+sDate+c+'_'+sNode+sExt
                try:
                    os.stat(s)
                except:
                    break;
            sFN=s
            self.dlgArch.Set(archNode,sFN,self.prjDN)
            self.dlgArch.SetDir(sDN)
            self.doc.startEdit(archNode)
            ret=self.dlgArch.ShowModal()
            sDNnew=self.dlgArch.GetDir()
            if sDN!=sDNnew:
                vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;new dn:%s'%(sDN,sDNnew),self)
                if self.doc.IsLocked(archNode):
                    oArchTmpl.SetDN(archNode,sDNnew)
                    self.doc.doEdit(archNode)
            self.doc.endEdit(archNode)
            
            #self.dlgArch.Show(True)
            #ret=0
            if ret==1:
                newNode=self.doc.cloneNode(archNode,True)
                self.doc.setTagName(newNode,'archive')
                o=self.doc.getChild(newNode,'filename')
                sFN=fnUtil.getFilenameRel2BaseDir(sFN,self.prjDN)
                self.doc.setText(o,sFN)
                o=self.doc.getChild(newNode,'date')
                #dt=vtDateTime(True)
                #sDate=dt.GetDateTimeStr(' ')
                self.doc.setText(o,sDate)
                self.doc.appendChild(self.node,newNode)
                self.doc.addNode(self.node,newNode)
                self.__addArch__(newNode)
                self.doc.AlignNode(newNode)
                index = self.lstArch.InsertImageStringItem(sys.maxint, sFN, 1)
                i=len(self.archs)-1
                self.lstArch.SetItemData(index,i)
                wx.PostEvent(self,vgpPrjDocumentAdded(self,newNode,sFN))
                pass
            
        event.Skip()
    def OnGcbbDelButton(self,event):
        event.Skip()
    def OnLstArchListItemSelected(self, event):
        self.selArchIdx=event.GetIndex()
        event.Skip()

    def OnTgShowAllButton(self, event):
        event.Skip()
        wx.CallAfter(self.RefreshDocuments)

    def OnViLangVtlangSelectionChanged(self, event):
        event.Skip()
        self.lang=self.viLang.GetValue()
        wx.CallAfter(self.RefreshDocuments)
