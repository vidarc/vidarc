#Boa:FramePanel:vPrjDocGrpInfoPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocGrpInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocGrpInfoPanel.py,v 1.6 2007/01/14 14:31:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.grid
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import string,sys,os.path
import vidarc.tool.InOut.fnUtil as fnUtil
#import vidarc.vApps.vPrj.vXmlPrjTree as vXmlPrjTree
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.lang.vtLgBase as vtLgBase

import images

[wxID_VPRJDOCGRPINFOPANEL, wxID_VPRJDOCGRPINFOPANELGCBBADD, 
 wxID_VPRJDOCGRPINFOPANELGCBBBROWSEFN, wxID_VPRJDOCGRPINFOPANELGCBBDEL, 
 wxID_VPRJDOCGRPINFOPANELLBLDOCTITLE, wxID_VPRJDOCGRPINFOPANELLBLFILENAME, 
 wxID_VPRJDOCGRPINFOPANELLBLPRJDESC, wxID_VPRJDOCGRPINFOPANELLBLTITLE, 
 wxID_VPRJDOCGRPINFOPANELLSTDOCS, wxID_VPRJDOCGRPINFOPANELTXTDESC, 
 wxID_VPRJDOCGRPINFOPANELTXTDOCTITLE, wxID_VPRJDOCGRPINFOPANELTXTFILENAME, 
 wxID_VPRJDOCGRPINFOPANELTXTTITLE, 
] = [wx.NewId() for _init_ctrls in range(13)]

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOCUMENT_SELECTED=wx.NewEventType()
def EVT_VGPPRJDOCUMENT_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOCUMENT_SELECTED,func)
class vgpPrjDocumentSelected(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOCUMENT_SELECTED(<widget_name>, self.OnPrjDocumentSelected)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOCUMENT_SELECTED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOC_CHANGED=wx.NewEventType()
def EVT_VGPPRJDOC_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_CHANGED,func)
class vgpPrjDocChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOC_CHANGED(<widget_name>, self.OnPrjDocChanged)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOC_GRP_INFO_CHANGED=wx.NewEventType()
def EVT_VGPPRJDOC_GRP_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_GRP_INFO_CHANGED,func)
class vgpPrjDocGrpInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOC_GRP_INFO_CHANGED(<widget_name>, self.OnPrjDocGrpInfoChanged)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_GRP_INFO_CHANGED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOC_GRP_INFO_CANCELED=wx.NewEventType()
def EVT_VGPPRJDOC_GRP_INFO_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_GRP_INFO_CANCELED,func)
class vgpPrjDocGrpInfoCanceled(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOC_GRP_INFO_CANCELED(<widget_name>, self.OnPrjDocGrpInfoCanceled)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_GRP_INFO_CANCELED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPDOC_GRP_INFO_DELETED=wx.NewEventType()
def EVT_VGPPRJDOC_GRP_INFO_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_GRP_INFO_DELETED,func)
class vgpPrjDocGrpInfoDeleted(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOC_GRP_INFO_DELETED(<widget_name>, self.OnPrjDocGrpInfoDeleted)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_GRP_INFO_DELETED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang

# defined event for vgpXmlTree item selected
wxEVT_VGPPRJDOC_GRP_INFO_ADDED=wx.NewEventType()
def EVT_VGPPRJDOC_GRP_INFO_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPRJDOC_GRP_INFO_ADDED,func)
class vgpPrjDocGrpInfoAdded(wx.PyEvent):
    """
    Posted Events:
        Document selected event
            EVT_VGPPRJDOC_GRP_INFO_ADDED(<widget_name>, self.OnPrjDocGrpInfoAdded)
    """

    def __init__(self,obj,node,lang):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPRJDOC_GRP_INFO_ADDED)
        self.obj=obj
        self.node=node
        self.lang=lang
    def GetVgpDocGrpInfo(self):
        return self.obj
    def GetNode(self):
        return self.node
    def GetLanguage(self):
        return self.lang


class vPrjDocGrpInfoPanel(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=3, vgap=0)

        self.bxsTitle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsDesc = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsDoc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTitle_Items(self.bxsTitle)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_fgsDesc_Items(self.fgsDesc)
        self._init_coll_fgsDesc_Growables(self.fgsDesc)
        self._init_coll_bxsDoc_Items(self.bxsDoc)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)


    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsTitle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTitle, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtTitle, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 4, border=0, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsDoc, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.gcbbBrowseFN, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lstDocs, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.LEFT | wx.TOP)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsDesc, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbDel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsDesc_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsDoc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDocTitle, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDocTitle, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFilename, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtFilename, 3, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCGRPINFOPANEL,
              name=u'vPrjDocGrpInfoPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(629, 187), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(621, 160))

        self.lblTitle = wx.StaticText(id=wxID_VPRJDOCGRPINFOPANELLBLTITLE,
              label=_(u'Title'), name=u'lblTitle', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(92, 38), style=wx.ALIGN_RIGHT)

        self.txtTitle = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOPANELTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(96, 0),
              size=wx.Size(88, 38), style=0, value=u'')
        self.txtTitle.Enable(False)

        self.lblPrjDesc = wx.StaticText(id=wxID_VPRJDOCGRPINFOPANELLBLPRJDESC,
              label=_(u'Description'), name=u'lblPrjDesc', parent=self,
              pos=wx.Point(184, 0), size=wx.Size(116, 38),
              style=wx.ALIGN_RIGHT)

        self.txtDesc = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(304, 0),
              size=wx.Size(248, 38), style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetExtraStyle(0)
        self.txtDesc.Enable(False)

        self.lstDocs = wx.ListCtrl(id=wxID_VPRJDOCGRPINFOPANELLSTDOCS,
              name=u'lstDocs', parent=self, pos=wx.Point(0, 68),
              size=wx.Size(552, 92),
              style=wx.LC_REPORT | wx.LC_SORT_DESCENDING)
        self.lstDocs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VPRJDOCGRPINFOPANELLSTDOCS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCGRPINFOPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Add'), name=u'gcbbAdd',
              parent=self, pos=wx.Point(556, 72), size=wx.Size(72, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VPRJDOCGRPINFOPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCGRPINFOPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Delete'), name=u'gcbbDel',
              parent=self, pos=wx.Point(556, 106), size=wx.Size(72, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VPRJDOCGRPINFOPANELGCBBDEL)

        self.lblDocTitle = wx.StaticText(id=wxID_VPRJDOCGRPINFOPANELLBLDOCTITLE,
              label=_(u'Doc Title'), name=u'lblDocTitle', parent=self,
              pos=wx.Point(0, 42), size=wx.Size(92, 26), style=wx.ALIGN_RIGHT)

        self.txtDocTitle = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOPANELTXTDOCTITLE,
              name=u'txtDocTitle', parent=self, pos=wx.Point(96, 42),
              size=wx.Size(88, 26), style=wx.TE_RICH2, value=u'')

        self.lblFilename = wx.StaticText(id=wxID_VPRJDOCGRPINFOPANELLBLFILENAME,
              label=_(u'Filename'), name=u'lblFilename', parent=self,
              pos=wx.Point(188, 42), size=wx.Size(88, 26),
              style=wx.ALIGN_RIGHT)

        self.txtFilename = wx.TextCtrl(id=wxID_VPRJDOCGRPINFOPANELTXTFILENAME,
              name=u'txtFilename', parent=self, pos=wx.Point(280, 42),
              size=wx.Size(272, 26), style=wx.TE_RICH2, value=u'')

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCGRPINFOPANELGCBBBROWSEFN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'...', name=u'gcbbBrowseFN',
              parent=self, pos=wx.Point(556, 38), size=wx.Size(32, 30),
              style=0)
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VPRJDOCGRPINFOPANELGCBBBROWSEFN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.language=None
        self.prjDN=None
        
        self.selDocIdx=-1
        self.selectedIdx=-1
        self.docs=[]
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)

        img=images.getBrowseBitmap()
        self.gcbbBrowseFN.SetBitmapLabel(img)
        
        # setup list
        self.lstDocs.InsertColumn(0,_(u'Filename'),wx.LIST_FORMAT_RIGHT,410)
        self.lstDocs.InsertColumn(1,_(u'Doc'))
        self.SetupImageList()
    def Lock(self,flag):
        if flag:
            self.txtTitle.Enable(False)
            self.txtDesc.Enable(False)
            self.txtDocTitle.Enable(False)
            self.txtFilename.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDel.Enable(False)
            self.gcbbBrowseFN.Enable(False)
            self.lstDocs.Enable(False)
        else:
            self.txtTitle.Enable(True)
            self.txtDesc.Enable(True)
            self.txtDocTitle.Enable(True)
            self.txtFilename.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDel.Enable(True)
            self.gcbbBrowseFN.Enable(True)
            self.lstDocs.Enable(True)
    def Clear(self):
        self.txtTitle.SetValue('')
        self.txtDesc.SetValue('')
        self.txtDocTitle.SetValue('')
        self.txtFilename.SetValue('')
        self.lstDocs.DeleteAllItems()
        self.node=None
        
    def SetPrjDir(self,dn):
        self.prjDN=dn
    def SetLanguage(self,lang):
        self.lang=lang
    def GetLanguage(self):
        return self.lang
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
    def OnLock(self,evt):
        evt.Skip()
        try:
            id=self.doc.getKey(self.node)
            if self.doc.isSameKey(self.node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    self.Lock(False)
                else:
                    self.Lock(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnUnLock(self,evt):
        evt.Skip()
        try:
            if self.doc.isSameKey(self.node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['released']:
                    self.Lock(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if (node is None) or (self.doc is None):
            self.txtTitle.SetValue('')
            self.txtDesc.SetValue('')
            self.lstDocs.DeleteAllItems()
            self.selectedIdx=-1
            return
        self.node=node
        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
        #sKey=vtXmlDomTree.getNodeText(node,'key')
        #sPath=vtXmlDomTree.getNodeText(node,'path')
        sTitle=self.doc.getNodeTextLang(node,'title',self.lang)
        sDesc=self.doc.getNodeTextLang(node,'description',self.lang)
        
        #self.txtPath.SetValue(sPath)
        self.txtTitle.SetValue(sTitle)
        self.txtDesc.SetValue(sDesc)
        self.selectedIdx=-1
        
        self.RefreshDocuments()
    def RefreshDocuments(self):
        # setup attributes
        self.lstDocs.DeleteAllItems()
        
        docNodes=self.doc.getChilds(self.node,'document')
        self.docs=[]
        for docNode in docNodes:
            l=self.doc.getAttribute(docNode,'language')
            bLang=False
            if len(l)<=0 and self.lang=='en':
                bLang=True
            elif self.lang==l:
                bLang=True
            if bLang==True:
                d={}
                for sAttr in ['title','type','filename','author','version','date']:
                    sVal=self.doc.getNodeText(docNode,sAttr)
                    d[sAttr]=sVal
                d['document']=docNode
                d['documentID']=self.doc.getKeyNum(docNode)
                self.docs.append(d)
        i=0
        for doc in self.docs:
            #keys=doc.keys()
            #keys.sort()
            fn=fnUtil.getFilenameRel2BaseDir(doc['filename'],self.prjDN)
            index = self.lstDocs.InsertImageStringItem(sys.maxint, fn, -1)
            
            
            self.lstDocs.SetStringItem(index, 1, doc['title'])
            #self.lstDocs.SetItemData(index,i)
            self.lstDocs.SetItemData(index,doc['documentID'])
            if i==self.selectedIdx:
                self.lstDocs.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            i=i+1
            #for k in keys:
            #    try:
            #        imgId=self.imgDict[k]
            #    except:
            #        imgId=-1
            #    index = self.lstDocs.InsertImageStringItem(sys.maxint, k, imgId)
            #    self.lstDocs.SetStringItem(index, 1, docs[k])
        pass
    def GetNode(self,node):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if node is None:
            return ''
        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
        #sPath=self.txtPath.GetValue()
        sTitle=self.txtTitle.GetValue()
        sDesc=self.txtDesc.GetValue()
        self.doc.setNodeTextLang(node,'title',sTitle,self.lang)
        self.doc.setNodeTextLang(node,'description',sDesc,self.lang)
        
        return sTitle
    def CreateNode(self):
        pass
    def GetSelectedDoc(self):
        if self.selDocIdx>=0:
            return self.docs[self.selDocIdx]
        else:
            return None
    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        
        it=self.lstDocs.GetItem(idx,0)
        self.selectedIdx=it.m_itemId
        self.txtFilename.SetValue(it.m_text)
        sDocFN=it.m_text
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        self.txtFilename.SetStyle(0, len(it.m_text), wx.TextAttr("BLACK", bkgCol))
        it=self.lstDocs.GetItem(idx,1)
        self.txtDocTitle.SetValue(it.m_text)
        self.txtDocTitle.SetStyle(0, len(it.m_text), wx.TextAttr("BLACK", bkgCol))
        id=self.lstDocs.GetItemData(self.selectedIdx)
        
        self.selDocIdx=0
        for doc in self.docs:
            if doc['documentID']==id:
                wx.PostEvent(self,vgpPrjDocumentSelected(self,doc,self.lang))
                return
            #if doc['filename']==sDocFN:
            #    wx.PostEvent(self,vgpPrjDocumentSelected(self,doc,self.lang))
            #    return
            self.selDocIdx+=1
        event.Skip()

    def OnGcbbAddButton(self, event):
        sDoc=self.txtDocTitle.GetValue()
        sFN=self.txtFilename.GetValue()
        if len(sDoc)<=0:
            self.txtDocTitle.SetValue('???')
            self.txtDocTitle.SetStyle(0, 3, wx.TextAttr("RED", "YELLOW"))
            
            return
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        self.txtDocTitle.SetStyle(0, len(sDoc), wx.TextAttr("BLACK", bkgCol))
        if len(sFN)<=0:
            self.txtFilename.SetValue('???')
            self.txtFilename.SetStyle(0, 3, wx.TextAttr("RED", "YELLOW"))
            
            return
        self.txtFilename.SetStyle(0, len(sFN), wx.TextAttr("BLACK", bkgCol))
        index = self.lstDocs.InsertImageStringItem(sys.maxint, sFN, -1)
        self.lstDocs.SetStringItem(index, 1, sDoc)
        
        docNode=self.doc.createSubNodeTextAttr(self.node,'document',sDoc,
                            'language',self.lang)
        d={}
        for sAttr in ['title','type','filename','author','version','date']:
            sVal=''
            if sAttr=='title':
                sVal=sDoc
            elif sAttr=='filename':
                sVal=sFN
            self.doc.setNodeText(docNode,sAttr,sVal)
            d[sAttr]=sVal
        d['document']=docNode
        self.docs.append(d)
        self.txtDocTitle.SetValue('')
        self.txtFilename.SetValue('')
        wx.PostEvent(self,vgpPrjDocumentSelected(self,None,self.lang))
        wx.PostEvent(self,vgpPrjDocChanged(self,None,self.lang))
        wx.PostEvent(self,vgpPrjDocGrpInfoChanged(self,self.node,self.lang))
        self.doc.addNode(self.node,docNode)
        d['documentID']=self.doc.getKeyNum(docNode)
        self.doc.AlignNode(docNode,iRec=2)
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedIdx>=0:
            id=self.lstDocs.GetItemData(self.selectedIdx)
            it=self.lstDocs.GetItem(self.selectedIdx,0)
            fn=it.m_text
            self.lstDocs.DeleteItem(self.selectedIdx)
            docFound=None
            for i in range(0,len(self.docs)):
                doc=self.docs[i]
                if doc['documentID']==id:
                #if doc['filename']==fn:
                    docFound=doc
                    break
            if docFound is None:
                return
            #idx=self.docs.index(fn)
            #d=self.docs[self.selectedIdx]['document']
            #self.docs=self.docs[:self.selectedIdx]+self.docs[self.selectedIdx+1:]
            filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
            if filename is not None:
                dlg = wx.MessageDialog(self,
                            _(u'Do you want to delete file?')+'\n'+filename,
                            _(u'vPrjDoc Close'),
                            wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION
                            )
                if dlg.ShowModal()==wx.ID_YES:
                    try:
                        filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
                        os.remove(filename)
                    except:
                        vtLog.vtLngTB(self.GetName())
                        pass
            d=doc['document']
            self.docs.remove(doc)
            
            self.selectedIdx=-1
            self.txtDocTitle.SetValue('')
            self.txtFilename.SetValue('')
            
            parDoc=self.doc.getParent(d)
            self.doc.delNode(d)
            #self.doc.deleteNode(d)#parDoc.removeChild(d)
            wx.PostEvent(self,vgpPrjDocumentSelected(self,None,self.lang))
            #wx.PostEvent(self,vgpPrjDocChanged(self,None,self.lang))
            wx.PostEvent(self,vgpPrjDocGrpInfoChanged(self,self.node,self.lang))
        
        event.Skip()

    def OnGcbbBowsePathButton(self, event):
        event.Skip()

    def OnGcbbBrowseFNButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", "", "", "all files (*.*)|*.*|XML files (*.xml)|*.xml", wx.OPEN)
        try:
            fn=self.txtFilename.GetValue()
            fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
            if fn is not None:
                sDN,sFN=os.path.split(fn)
                dlg.SetDirectory(sDN)
                dlg.SetFilename(sFN)
                #dlg.SetFilename(fn)
            else:
                dlg.SetDirectory(self.prjDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                filename=fnUtil.getFilenameRel2BaseDir(filename,self.prjDN)
                self.txtFilename.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
    
