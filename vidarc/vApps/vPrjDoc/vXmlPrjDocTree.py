#----------------------------------------------------------------------------
# Name:         vXmlPrjDocTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlPrjDocTree.py,v 1.9 2008/04/28 11:47:03 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTreeReg import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.tool.net.vtNetFileSynchDialog import vtNetFileSynchDialog

import images

ATTRS=['title','key','path']

class vXmlPrjDocTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,master=False,
                    controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.prjAttr=ATTRS
        self.nodeInfos=['Title','|id','|fid','Key','path','RevDate']
        self.nodeKey='|id'
        #self.lang=None
        #self.languages=[]
        #self.langIds=[]
        self.bLangMenu=True
        
    def SetLanguageO(self,lang):
        #self.lang=lang
        vtXmlGrpTree.SetLanguage(self,lang)
        self.SetNode(self.rootNode)
    def SetDftGrouping(self):
        self.grouping={}
        self.label={None:[('Key',''),('Title','')],
            'document':[('RevDate',''),('Title','')],}
        self.bGrouping=False
        #vtLog.CallStack('')
        #print self.label
    def SetupImageList(self):
        if vtXmlGrpTreeReg.SetupImageList(self):
        
            img=images.getPluginImage()
            imgSel=images.getPluginImage()
            self.__addElemImage2ImageList__('root',img,imgSel,True)
            img=images.getPluginImage()
            imgSel=images.getPluginImage()
            self.__addElemImage2ImageList__('prjdocs',img,imgSel,True)
            
            img=images.getDocGrpImage()
            imgSel=images.getDocGrpSelImage()
            self.__addElemImage2ImageList__('maingroup',img,imgSel,True)
            
            img=images.getSubDocGrpImage()
            imgSel=images.getSubDocGrpSelImage()
            self.__addElemImage2ImageList__('docgroup',img,imgSel,True)
            
            img=images.getDocEditImage()
            imgSel=images.getDocEditImage()
            self.__addElemImage2ImageList__('document',img,imgSel,True)
            
            self.SetImageList(self.imgLstTyp)
    def RefreshSelected(self):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        #if self.objTreeItemSel is not None:
        #    return
        oldnode=self.triSelected
        #self.triSelected=self.GetSelection()
        tiRoot=self.GetItemParent(self.triSelected)
        #parNode=self.objTreeItemSel.parentNode
        #self.__addElements__(self.tiRoot,parNode)
        tagName=self.doc.getTagName(self.objTreeItemSel)
        if tagName=='maingroup':
            self.triSelected=self.__addMainGroup__(self.tiRoot,self.objTreeItemSel)
            #self.SortChildren(self.tiRoot)
        else:
            self.triSelected=self.__addElement__(tiRoot,self.objTreeItemSel,bRet=True)
            self.SortChildren(tiRoot)
        tid=wxTreeItemData()
        tid.SetData(None)
        self.SetItemData(oldnode,tid)
        self.Delete(oldnode)
        self.SelectItem(self.triSelected)
        wx.PostEvent(self,vgtrXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
    def __getValFormat__Old(self,g,val,infos=None):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    val=val[:4]
                elif sFormat=='MM':
                    val=val[4:6]
                elif sFormat=='DD':
                    val=val[6:8]
                elif sFormat=='YYYY-MM-DD':
                    val=val[:4]+'-'+val[4:6]+'-'+val[6:8]
                elif sFormat=='MM-DD':
                    val=val[4:6]+'-'+val[6:8]
            if g[0]=='starttime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
            if g[0]=='endtime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
                
        if len(val)<=0:
            val="---"
        return val
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='person':
            #if sFormat=='':
            img=self.imgDict['grp']['person'][0]
            imgSel=self.imgDict['grp']['person'][0]
            return (img,imgSel)
        elif g[0]=='attributes:clientshort':
            #if sFormat=='':
            img=self.imgDict['grp']['attributes:clientshort'][0]
            imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return (img,imgSel)
        try:
            img=self.imgDict['elem'][o.tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][o.tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)

    def __addMainGroup__(self,parent,o):
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        tid=wxTreeItemData()
        tid.SetData(o)
        sKey=self.doc.getNodeTextLang(o,'key',self.language)
        sTitle=self.doc.getNodeTextLang(o,'title',self.language)
        sLabel=sKey+" "+sTitle
        tn=self.AppendItem(parent,sLabel,-1,-1,tid)
        self.SetItemImage(tn,img,wxTreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,
                                wxTreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,
                                wxTreeItemIcon_Selected)
        if self.tiCache is not None:
            self.tiCache.AddID(tn,self.doc.getKey(o))
        self.__addElements__(tn,o)
        self.SortChildren(parent)
        print 'sorted',self.doc.getKey(self.GetPyData(parent))
    def __addElementsOld__(self,parent,obj):
        for o in self.doc.getChilds(obj):
                bBlockRec=False
                if self.doc.getTagName(o) in ['docgroup']:
                    bBlockRec=True
                    
                    self.__addElement__(parent,o)
                elif self.doc.getTagName(o)=='maingroup':
                    self.__addMainGroup__(parent,o)
                else:
                    tid=wxTreeItemData()
                    tid.SetData(None)
        self.SortChildren(parent)
    def OnGotContent(self,evt):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnGetContent;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        self.SetNode(None)
        evt.Skip()
    def OnAddNode(self,evt):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnAddNode;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                        self)
        idPar=evt.GetID()
        idNew=evt.GetNewID()
        child=self.doc.getNodeById(idNew)
        tagName=self.doc.getTagName(child)
        tup=self.__findNodeByID__(None,idPar)
        if (tup is not None) and (child is not None):
            ti=self.__addElement__(tup[0],child,True)
            self.SortChildren(tup[0])
        evt.Skip()
    def __createMenuEditIds__(self):
        return 
        self.popupIdAddDocGrp=wx.NewId()
        self.popupIdDel=wx.NewId()
        self.popupIdEdit=wx.NewId()
        #self.popupIdMove=wx.NewId()
        wx.EVT_MENU(self,self.popupIdAddDocGrp,self.OnTreeAddDocGrp)
        #wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
        #wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        #wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDel)
    def __addMenuEdit__(self,menu):
        return False
        if self.bMaster:
            mnIt=wx.MenuItem(menu,self.popupIdAddDocGrp,_(u'add document group'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Add))
            menu.AppendItem(mnIt)
            nodePar=self.objTreeItemSel
            if self.doc is not None:
                if self.doc.getTagName(nodePar)!='maingroup':
                    mnIt.Enable(False)
            #    if not self.doc.hasPossibleNode(nodePar):
            #        mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdDel,_(u'Delete'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Del))
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdEdit,_(u'Edit'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Edit))
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            
            menu.AppendSeparator()
            return True
        return False
    def __createMenuToolsIds__(self):
        vtXmlGrpTree.__createMenuToolsIds__(self)
        if not hasattr(self,'popupIdFileSynch'):
            self.popupIdFileSynch=wx.NewId()
            self.popupIdConfigDir=wx.NewId()
    def AddMenuTool(self,menuTools,obj,iPos=-1):
        menuTools,iPos=vtXmlGrpTree.AddMenuTool(self,menuTools,obj,iPos)
        if menuTools is None:
            return menuTools,iPos
        try:
            #mnIt=wx.MenuItem(menuTools,wx.ID_SEPARATOR)
            #menuTools.InsertItem(iPos,mnIt)
            #iPos+=1
            mnIt=wx.MenuItem(menuTools,self.popupIdConfigDir,_(u'local config'))
            wx.EVT_MENU(obj,self.popupIdConfigDir,self.OnTreeConfigDir)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Config))
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menuTools,self.popupIdFileSynch,_(u'Synch Files'))
            wx.EVT_MENU(obj,self.popupIdFileSynch,self.OnTreeFileSynch)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Synch))
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
        except:
            vtLog.vtLngTB(self.GetName())
        return menuTools,iPos
    def GetFileSynchDlg(self):
        try:
            if self.dlgFileSynch is None:
                self.dlgFileSynch=vtNetFileSynchDialog(self)
                self.dlgFileSynch.SetDoc(self.doc,self.bNetResponse)
                self.dlgFileSynch.Centre()
            return self.dlgFileSynch
        except:
            self.dlgFileSynch=vtNetFileSynchDialog(self)
            self.dlgFileSynch.SetDoc(self.doc,self.bNetResponse)
            self.dlgFileSynch.Centre()
            return self.dlgFileSynch
    def OnTreeConfigDir(self,evt):
        try:
            self.doc.DoEditCfg(self)
            return
            node=self.doc.getBaseNode()
            oReg=self.doc.GetRegByNode(node)
            dlgCls=oReg.GetEditDialogClass()
            if dlgCls is not None:
                dlg=dlgCls(self)
                dlg.Centre()
                dlg.SetDoc(self.doc)
                dlg.SetNode(node)
                dlg.ShowModal()
                dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeFileSynch(self,evt):
        try:
            dlg=self.GetFileSynchDlg()
            if dlg is not None:
                dlg.SetDoc(self.doc)
                nodeSettings=self.doc.getChildForced(self.doc.getRoot(),'settings')
                dlg.SetNode(nodeSettings)
                dlg.Show(True)
                return
            dlg=wx.MessageDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vXmlPrjDocTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
        
    def OnTcNodeRightDownO(self, event):
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdAddMainGrp'):
            self.popupIdAddMainGrp=wx.NewId()
            self.popupIdAddDocGrp=wx.NewId()
            #self.popupIdAddUsr=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            self.popupIdSort=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddMainGrp,self.OnTreeAddMainGrp)
            wx.EVT_MENU(self,self.popupIdAddDocGrp,self.OnTreeAddDocGrp)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
            wx.EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
        menu=wx.Menu()
        menu.Append(self.popupIdAddMainGrp,'Add main document group')
        menu.Append(self.popupIdAddDocGrp,'Add document group')
        #menu.Append(self.popupIdAddUsr,'Add User')
        menu.Append(self.popupIdDel,'Delete')
        menu.Append(self.popupIdEdit,'Edit')
        menu.Append(self.popupIdMove,'Move Node')
        menu.Append(self.popupIdSort,'Sort')
        
        self.PopupMenu(menu,(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def OnTreeAddMainGrp(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        if self.rootNode is None:
            return -1
        #doc=vtXmlDomTree.__getDocument__(self.rootNode)
        self.AddMainGrpNode(self.rootNode)
        #nodes=self.rootNode.getElementsByTagName('docnumbering')
        #print len(nodes)
        #if len(nodes)>0:
        #    self.AddMainGrpNode(nodes[0])
        pass
    def OnTreeAddDocGrp(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        if self.rootNode is None:
            return -1
        doc=vtXmlDomTree.__getDocument__(self.rootNode)
        try:
            if self.objTreeItemSel.tagName=='docgroup':
                node=self.objTreeItemSel.parentNode
                tiRoot=self.triSelected.GetItemParent()
            else:
                node=self.objTreeItemSel
                tiRoot=self.triSelected
            self.AddDocGrpNode(node,tiRoot)
        except:
            return
        #prjs=node.getElementsByTagName('maingroup')
        #if len(prjs)>0:
        #    self.AddProjectNode(prjs[0])
        pass
    def OnTreeDelGrp(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        if self.rootNode is None:
            return -1
        doc=vtXmlDomTree.__getDocument__(self.rootNode)
        if self.objTreeItemSel is None:
            return
        par=self.objTreeItemSel.parentNode
        par.removeChild(self.objTreeItemSel)
        self.objTreeItemSel.unlink()
        self.Delete(self.triSelected)
        #prjs=self.rootNode.getElementsByTagName('projects')
        #if len(prjs)>0:
        #    self.AddProjectNode(prjs[0])
        
        #self.__update_tree__()
        pass
        
    def OnTreeEdit(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        s=self.GetItemText(self.triSelected)
        
        if string.find(s,"project:")==0:
            # open group edit dialog
            try:
                d=self.dlgEditPrj
            except:
                self.dlgEditPrj=vgdXmlDlgEditPrj(self)
            self.dlgEditPrj.SetNode(self.doc,self.objTreeItemSel)
            ret=self.dlgEditPrj.ShowModal()
            if ret==1:
                # process dialog ok
                s=self.dlgEditPrj.GetNode(self.objTreeItemSel)
                self.SetItemText(self.triSelected,'project:'+s)
                wx.PostEvent(self,vgpXmlTreeItemEdited(self,
                    self.triSelected,self.objTreeItemSel))  # forward double click event
        
            #dlg.Destroy()
    #def SetLanguages(self,languages,languageIds):
    #    self.languages=languages
    #    self.languageIds=languageIds
    def AddMainGrpNode(self,node):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        ti=self.__addElement__(self.tiRoot,node,True)
        self.SelectItem(ti)
        #self.__update_tree__()
        self.triSelected=ti
        self.objTreeItemSel=node
        self.SortChildren(self.tiRoot)
        #wx.PostEvent(self,vgtrXmlTreeItemAdded(self,ti,child))
        
    def AddDocGrpNode(self,node,tiRoot):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        doc=vtXmlDomTree.__getDocument__(node)
        child=doc.createElement('docgroup')
        node.appendChild(child)
        # append key
        nodeKey=doc.createElement('key')
        nodeKey.appendChild(doc.createTextNode('---'))
        child.appendChild(nodeKey)
        # append title
        if self.languageIds is None:
            nodeTitle=doc.createElement('title')
            nodeTitle.appendChild(doc.createTextNode('---'))
            child.appendChild(nodeTitle)
        else:
            for id in self.languageIds:
                nodeTitle=doc.createElement('title')
                nodeTitle.setAttribute('language',id)
                nodeTitle.appendChild(doc.createTextNode('---'))
                child.appendChild(nodeTitle)
        # append description
        if self.languageIds is None:
            nodeDesc=doc.createElement('description')
            nodeDesc.appendChild(doc.createTextNode('---'))
            child.appendChild(nodeDesc)
        else:
            for id in self.languageIds:
                nodeDesc=doc.createElement('description')
                nodeDesc.setAttribute('language',id)
                nodeDesc.appendChild(doc.createTextNode('---'))
                child.appendChild(nodeDesc)
        #self.ids.AddNewNode(prj)
        vtXmlDomTree.vtXmlDomAlignNode(doc,child)
        
        ti=self.__addElement__(tiRoot,child,True)
        self.SelectItem(ti)
        #self.__update_tree__()
        self.triSelected=ti
        self.objTreeItemSel=child
        wx.PostEvent(self,vgtrXmlTreeItemAdded(self,ti,child))
        
    def __update_tree__(self):
        # update tree
        triRoot=self.GetRootItem()
        triChild=self.GetFirstChild(triRoot)
        if self.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.GetNextChild(triChild[0],triChild[1])
            if self.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
        self.groupItems={}
        #self.DeleteChildren(triPar)
        self.DeleteChildren(triRoot)
        self.__addElements__(triRoot,self.rootNode)
        parTri=self.GetItemParent(triRoot)
        #self.SortChildren(triRoot)
        #self.SelectItem(triPar)
        wx.PostEvent(self,vgtrXmlTreeItemAdded(self,
                            triPar,self.rootNode))
        return 0
