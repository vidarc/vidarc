#Boa:FramePanel:vXmlNodeCfgLocalPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeCfgLocalPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061117
# CVS-ID:       $Id: vXmlNodeCfgLocalPanel.py,v 1.5 2014/10/01 09:02:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.input.vtInputTextML
    import wx.lib.filebrowsebutton

    import sys

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.vApps.vPrjDoc.vDocSelPanel import MODES
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODECFGLOCALPANEL, wxID_VXMLNODECFGLOCALPANELCHCMODES, 
 wxID_VXMLNODECFGLOCALPANELDBBPRJDN, wxID_VXMLNODECFGLOCALPANELLBLMODES, 
 wxID_VXMLNODECFGLOCALPANELLBLNAME, wxID_VXMLNODECFGLOCALPANELVINAME, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vXmlNodeCfgLocalPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=0, flag=0)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.LEFT)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsModes, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.dbbPrjDN, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsModes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblModes, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcModes, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsModes = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsModes_Items(self.bxsModes)
        self._init_coll_bxsName_Items(self.bxsName)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECFGLOCALPANEL,
              name=u'vXmlNodeCfgLocalPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.dbbPrjDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VXMLNODECFGLOCALPANELDBBPRJDN,
              labelText=_(u'project directory'), parent=self, pos=wx.Point(4,
              63), size=wx.Size(296, 29), startDirectory='', 
              newDirectory=True , style=0)
        self.dbbPrjDN.SetMinSize(wx.Size(-1, -1))

        self.lblName = wx.StaticText(id=wxID_VXMLNODECFGLOCALPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              4), size=wx.Size(98, 13), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODECFGLOCALPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(106, 4),
              size=wx.Size(193, 30), style=0)

        self.lblModes = wx.StaticText(id=wxID_VXMLNODECFGLOCALPANELLBLMODES,
              label=_(u'modes'), name=u'lblModes', parent=self, pos=wx.Point(4,
              38), size=wx.Size(98, 21), style=wx.ALIGN_RIGHT)
        self.lblModes.SetMinSize(wx.Size(-1, -1))

        self.chcModes = wx.Choice(choices=[],
              id=wxID_VXMLNODECFGLOCALPANELCHCMODES, name=u'chcModes',
              parent=self, pos=wx.Point(106, 38), size=wx.Size(193, 21),
              style=0)
        self.chcModes.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent, id)
        self.MODES_TRANS={
                'complete':_('complete'),
                'last':_('last'),
                'flat':_('flat'),
                'major':_('major'),
                }
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viName.SetTagName('name')
        for m in MODES:
            self.chcModes.Append(self.MODES_TRANS[m])
        self.chcModes.SetSelection(0)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        try:
            # add code here
            self.viName.Clear()
            self.chcModes.SetSelection(0)
            self.dbbPrjDN.SetValue('')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viName.SetNode(node)
            sMode=self.objRegNode.GetMode(node)
            try:
                self.__logDebug__({'sMode':sMode,'TRANS':self.MODES_TRANS})
                self.chcModes.SetStringSelection(self.MODES_TRANS[sMode])
            except:
                self.chcModes.SetSelection(0)
            sPrjDN=self.objRegNode.GetPrjDN(node)
            self.dbbPrjDN.SetValue(sPrjDN)
        except:
            self.__logTB__()
    def __GetNode__(self,node=None):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viName.GetNode(node)
            iMode=self.chcModes.GetSelection()
            self.objRegNode.SetMode(node,MODES[iMode])
            sPrjDN=self.dbbPrjDN.GetValue()
            self.objRegNode.SetPrjDN(node,sPrjDN)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Clear()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viName.Enable(False)
            pass
        else:
            # add code here
            self.viName.Enable(True)
            pass
