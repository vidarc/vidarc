#Boa:Dialog:vAddFileDialog
#----------------------------------------------------------------------------
# Name:         vAddFileDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vAddFileDialog.py,v 1.2 2007/01/14 14:31:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import os,os.path,string

import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vAddFileDialog(parent)

[wxID_VADDFILEDIALOG, wxID_VADDFILEDIALOGCBCANCEL, wxID_VADDFILEDIALOGCBIMPCP, 
 wxID_VADDFILEDIALOGCBIMPMV, wxID_VADDFILEDIALOGLBLNEWFN, 
 wxID_VADDFILEDIALOGLBLORIGFN, wxID_VADDFILEDIALOGLBLREV, 
 wxID_VADDFILEDIALOGLBLTITLE, wxID_VADDFILEDIALOGTXTNEWFN, 
 wxID_VADDFILEDIALOGTXTORIGFN, wxID_VADDFILEDIALOGTXTREV, 
 wxID_VADDFILEDIALOGTXTTITLE, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vAddFileDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbImpCp, 0, border=0, flag=0)
        parent.AddWindow(self.cbImpMv, 0, border=16, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblOrigFN, (0, 0), border=0,
              flag=wx.ALIGN_BOTTOM | wx.EXPAND, span=(1, 2))
        parent.AddWindow(self.txtOrigFN, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblTitle, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtTitle, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblRev, (2, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.txtRev, (3, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblNewFN, (4, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.txtNewFN, (5, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddSpacer(wx.Size(8, 8), (6, 0), border=0, flag=0, span=(1, 1))
        parent.AddSizer(self.bxsBt, (7, 0), border=8,
              flag=wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT, span=(1, 2))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VADDFILEDIALOG, name=u'vAddFileDialog',
              parent=prnt, pos=wx.Point(44, 44), size=wx.Size(300, 236),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vPrjDoc Add File Dialog')
        self.SetClientSize(wx.Size(292, 209))

        self.lblOrigFN = wx.StaticText(id=wxID_VADDFILEDIALOGLBLORIGFN,
              label=_(u'original filename'), name=u'lblOrigFN', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(280, 20), style=0)
        self.lblOrigFN.SetMinSize(wx.Size(-1, -1))

        self.txtOrigFN = wx.TextCtrl(id=wxID_VADDFILEDIALOGTXTORIGFN,
              name=u'txtOrigFN', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(280, 21), style=wx.TE_READONLY, value=u'')
        self.txtOrigFN.SetMinSize(wx.Size(-1, -1))

        self.lblTitle = wx.StaticText(id=wxID_VADDFILEDIALOGLBLTITLE,
              label=_(u'title'), name=u'lblTitle', parent=self, pos=wx.Point(0,
              49), size=wx.Size(138, 20), style=0)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.txtTitle = wx.TextCtrl(id=wxID_VADDFILEDIALOGTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(0, 73),
              size=wx.Size(138, 21), style=0, value=u'')
        self.txtTitle.SetMinSize(wx.Size(-1, -1))
        self.txtTitle.Bind(wx.EVT_TEXT, self.OnTxtTitleText,
              id=wxID_VADDFILEDIALOGTXTTITLE)

        self.lblRev = wx.StaticText(id=wxID_VADDFILEDIALOGLBLREV,
              label=_(u'revision'), name=u'lblRev', parent=self,
              pos=wx.Point(142, 49), size=wx.Size(138, 20), style=0)
        self.lblRev.SetMinSize(wx.Size(-1, -1))

        self.txtRev = wx.TextCtrl(id=wxID_VADDFILEDIALOGTXTREV, name=u'txtRev',
              parent=self, pos=wx.Point(142, 73), size=wx.Size(138, 21),
              style=0, value=u'')
        self.txtRev.SetMinSize(wx.Size(-1, -1))
        self.txtRev.Bind(wx.EVT_TEXT, self.OnTxtRevText,
              id=wxID_VADDFILEDIALOGTXTREV)

        self.lblNewFN = wx.StaticText(id=wxID_VADDFILEDIALOGLBLNEWFN,
              label=_(u'new filename'), name=u'lblNewFN', parent=self,
              pos=wx.Point(0, 98), size=wx.Size(280, 20), style=0)
        self.lblNewFN.SetMinSize(wx.Size(-1, -1))

        self.txtNewFN = wx.TextCtrl(id=wxID_VADDFILEDIALOGTXTNEWFN,
              name=u'txtNewFN', parent=self, pos=wx.Point(0, 122),
              size=wx.Size(280, 21), style=wx.TE_READONLY, value=u'')
        self.txtNewFN.SetMinSize(wx.Size(-1, -1))

        self.cbImpCp = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VADDFILEDIALOGCBIMPCP,
              bitmap=vtArt.getBitmap(vtArt.Save), label=_(u'Copy'),
              name=u'cbImpCp', parent=self, pos=wx.Point(10, 171),
              size=wx.Size(76, 30), style=0)
        self.cbImpCp.SetMinSize(wx.Size(-1, -1))
        self.cbImpCp.Bind(wx.EVT_BUTTON, self.OnCbImpCpButton,
              id=wxID_VADDFILEDIALOGCBIMPCP)

        self.cbImpMv = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VADDFILEDIALOGCBIMPMV,
              bitmap=vtArt.getBitmap(vtArt.Move), label=_(u'Move'),
              name=u'cbImpMv', parent=self, pos=wx.Point(102, 171),
              size=wx.Size(76, 30), style=0)
        self.cbImpMv.SetMinSize(wx.Size(-1, -1))
        self.cbImpMv.Bind(wx.EVT_BUTTON, self.OnCbImpMvButton,
              id=wxID_VADDFILEDIALOGCBIMPMV)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VADDFILEDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(194, 171),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VADDFILEDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        
        self.gbsData.AddGrowableCol(0,2)
        self.gbsData.AddGrowableCol(1,1)
        self.Layout()
        self.Clear()
    def Clear(self):
        self.prjDN=None
        self.sDocDN=None
        self.sFullFN=None
    def Set(self,prjDN,sDocDN,sDocGrp,sPrjClient,sPrjShortCut,sFullFN):
        self.prjDN=prjDN
        self.sDocDN=sDocDN
        self.sDocGrp=sDocGrp
        self.sFullFN=sFullFN
        self.sPrjClient=sPrjClient
        self.sPrjShortCut=sPrjShortCut
        self.sPrjDocDN=os.path.join(self.prjDN,sDocDN)
        #print sFullFN
        sDN,sFNext=os.path.split(sFullFN)
        sPrefix=''.join([sPrjClient.capitalize(),
                        sPrjShortCut.capitalize(),'_',sDocGrp,'_'])
        #vtLog.CallStack('')
        #print self.sPrjDocDN,sDN
        #print sPrefix
        #print sFullFN
        if self.sPrjDocDN==sDN:
            bIs2Import=True
            if sFNext.startswith(sPrefix):
                bIs2Import=False
        else:
            bIs2Import=True
        i=sFNext.rfind('.')
        if i>0:
            self.sExt=sFNext[i+1:]
            sFNrev=sFNext[:i]
        else:
            sFNrev=sFNext
            self.sExt=''
        #print sFNrev
        i=sFNrev.rfind('_')
        if i>0:
            sRev=sFNrev[i+1:]
            sFN=sFNrev[:i]
        else:
            sFN=sFNrev
            sRev='0'
        if sFN.startswith(sPrefix):
            sTitle=sFN[len(sPrefix):]
        else:
            sTitle=sFN
        #sTitle=sFN
        #print sDN,sFNext
        #print 'fn',sFN,'rev',sRev,'ext',self.sExt
        #print 'docDN',sDocDN
        #print 'title',sTitle
        self.txtOrigFN.SetValue(sFullFN)
        self.txtTitle.SetValue(sTitle)
        self.txtRev.SetValue(sRev)
        #self.txtExt.SetValue(sExt)
        self.__updateFN()
        return bIs2Import
    def __updateFN(self):
        fn=string.capitalize(self.sPrjClient)
        fn=fn+string.capitalize(self.sPrjShortCut)+'_'+self.sDocGrp+'_'
        fn=fnUtil.replaceSuspectChars(fn)
        #fn=fn+''.join(map(string.capitalize,sTitleRepl.split(' ')))+'_'+sRev+'.'+sExt
        #sFN=os.path.join(self.prjDN,sDocPath,fn)
        sTitle=self.txtTitle.GetValue()
        sRev=self.txtRev.GetValue()
        sExt=self.sExt
        sNewFN=fn+sTitle+'_'+sRev+'.'+sExt
        #filename=string.replace(filename,'\\','/')
        #prjDN=string.replace(self.prjDN,'\\','/')
        #tupVal=fnUtil.getFilenameRel2BaseDir(sFullFN,self.prjDN)
        sNewFN=fnUtil.replaceSuspectChars(sNewFN)
        sNewFullFN=os.path.join(self.sPrjDocDN,sNewFN)
        self.txtNewFN.SetValue(sNewFullFN)
    def GetFN(self):
        sFullFN=self.txtNewFN.GetValue()
        #sFN=fnUtil.getFilenameRel2BaseDir(sFullFN,self.prjDN)
        sFullFN=sFullFN.replace('\\','/')
        sFN=sFullFN[len(self.sPrjDocDN)+1:]
        return sFN,sFullFN
    def GetInfos(self):
        return self.txtTitle.GetValue(),self.txtRev.GetValue(),self.sExt
    def OnCbImpCpButton(self, event):
        event.Skip()
        self.EndModal(1)
    def OnCbImpMvButton(self, event):
        event.Skip()
        self.EndModal(2)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.EndModal(0)

    def OnTxtTitleText(self, event):
        event.Skip()
        self.__updateFN()
    def OnTxtRevText(self, event):
        event.Skip()
        self.__updateFN()
