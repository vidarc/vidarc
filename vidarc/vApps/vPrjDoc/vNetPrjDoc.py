#----------------------------------------------------------------------------
# Name:         vNetPrjDoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetPrjDoc.py,v 1.11 2011/08/25 21:59:57 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from wx import DateTime
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPrjDoc.vXmlPrjDoc import vXmlPrjDoc
from vidarc.tool.net.vNetXmlWxGui import *

class vNetPrjDoc(vXmlPrjDoc,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlPrjDoc.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
        vXmlPrjDoc.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlPrjDoc.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlPrjDoc.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlPrjDoc.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCur(vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlPrjDoc.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'Open',origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlPrjDoc.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlPrjDoc.Open(self,fn,True)
        else:
            iRet=vXmlPrjDoc.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlPrjDoc.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlPrjDoc.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.appl)
        vXmlPrjDoc.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def addNodeCorrectId(self,par,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'addNodeCorretId;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.addNode(self,par,node)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlPrjDoc.delNode(self,node)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlPrjDoc.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlPrjDoc.__stop__(self)
    def GetConfig(self):
        vNetXmlWxGui.GetConfig(self)
        if self.cltSock is not None:
            self.cltSock.GetSpecial('prjinfo')
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'NotifyContent',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'NotifyGetNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'NotifyAddNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'NotifyRemoveNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'GenerateSortedIds',origin=self.appl)
        self.docs=self.getSortedNodeIds(['prjdocs'],
                        'docgroup','fid',[('key','%s'),('title','%s')])
    def GetSortedIds(self):
        try:
            return self.docs
        except:
            self.GenerateSortedIds()
            return self.docs
    def FindDocument(self,netDoc,docGrp,title,rev):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'FindDocument',origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'docgrp:%s;title:%s;rev:%s'%(docGrp,title,rev),origin=self.appl)
        ids=self.GetSortedIds()
        docGrpId=netDoc.getAttribute(docGrp,'id')
        docGrpNode=None
        try:
            docGrpId=long(docGrpId)
        except:
            return None,None
        try:
            for c in self.getChilds(self.getBaseNode(),'docgroup'):
                fid,appl=self.convForeign(self.getAttribute(c,'fid'))
                if fid==docGrpId:
                    docGrpNode=c
                    break
            #docGrpNode=ids.GetInfoById(docGrpId)[0]
            if docGrpNode is None:
                return None,None
            for c in self.getChilds(docGrpNode,'document'):
                if self.getNodeText(c,'title')==title:
                    if self.getNodeText(c,'version')==rev:
                        return docGrpNode,c
        except:
            vtLog.vtLngTB(self.appl)
        return docGrpNode,None
    def SetDocument(self,netDoc,docGrp,title,rev,fn,func=None,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'SetDocument',origin=self.appl)
            if docGrp is None:
                vtLog.vtLngCur(vtLog.ERROR,'docgrp is no None',self)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'docgrp:%s;title:%s;rev:%s;fn:%s'%(docGrp,title,rev,fn),origin=self.appl)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'title:%s;rev:%s;fn:%s'%(title,rev,fn),origin=self.appl)
        except:
            vtLog.vtLngTB(self.appl)
        docGrpNode,docNode=self.FindDocument(netDoc,docGrp,title,rev)
        if docGrpNode is None:
            baseNode=self.getBaseNode()
            docGrpNode=self.createSubNode(baseNode,'docgroup')
            docGrpId=netDoc.getAttribute(docGrp,'id')
            #self.setAttribute(docGrpNode,'fid',docGrpId)   # 060806
            self.setForeignKey(docGrpNode,'fid',docGrpId,'vDoc')
            for c in netDoc.getChilds(docGrp,'title'):
                nc=netDoc.cloneNode(c,True)
                self.appendChild(docGrpNode,nc)
            self.AlignNode(docGrpNode,iRec=2)
            self.addNode(baseNode,docGrpNode)
        if docNode is None:
            docNode=self.createSubNode(docGrpNode,'document')
            self.createSubNodeText(docNode,'title',title)
            self.createSubNodeText(docNode,'filename',fn)
            self.createSubNodeText(docNode,'version',rev)
            self.createSubNodeText(docNode,'author',self.GetUsr())
            dt=DateTime.Now()
            self.createSubNodeText(docNode,'date',dt.Format('%Y%m%d_%H%M%S'))
            if func is not None:
                func(docNode,*args,**kwargs)
            self.AlignNode(docNode,iRec=2)
            self.addNode(docGrpNode,docNode)
        else:
            if self.getNodeText(docNode,'locked')=='1':
                return -1
            self.setNodeText(docNode,'author',self.GetUsr())
            self.setNodeText(docNode,'filename',fn)
            dt=DateTime.Now()
            self.setNodeText(docNode,'date',dt.Format('%Y%m%d_%H%M%S'))
            if func is not None:
                func(docNode,*args,**kwargs)
            self.doEdit(docNode)
        return 0
    def GetProjectPath(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'GetProjectPath',origin=self.appl)
        nodeSettings=self.getChild(self.getRoot(),'settings')
        sPrjDN=self.getNodeText(nodeSettings,'projectdn')
        if self.verbose:
            vtLog.vtLogCallDepth(self,'prjDN:%s'%sPrjDN,callstack=False)
        if len(sPrjDN)<=0:
            vtLog.vtLngCur(vtLog.ERROR,'project path not found.',origin=self.appl)
            iRes=-1
        else:
            iRes=0
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'res:%d;dn:%s'%(iRes,sPrjDN),origin=self.appl)
        return iRes,sPrjDN
    def GetPrjInfo(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'GetPrjInfo',origin=self.appl)
        nodePrjInf=self.getChild(self.getRoot(),'prjinfo')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s'%(nodePrjInf),origin=self.appl)
        return nodePrjInf
    def GetDocumentPathPrefixFN(self,netDoc,selDocNode):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'GetDocumentPathPrefixFN',origin=self.appl)
        iRes,sPrjDN=self.GetProjectPath()
        if iRes<0:
            vtLog.vtLngCur(vtLog.ERROR,'project path not found',origin=self.appl)
            return -1,'',''
        nodeSettings=self.getChild(self.getRoot(),'settings')
        nodePrjInf=self.getChild(self.getRoot(),'prjinfo')
        
        sMode=self.getNodeText(nodeSettings,'mode')
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'prjinfo:%s'%nodePrjInf,callstack=False)
        iMode=100
        if sMode=='complete':
            iMode=100
        elif sMode=='last':
            iMode=1
        elif sMode=='flat':
            iMode=0
        lstPath=[]
        docBase=netDoc.getBaseNode()
        #selDocNode=netDoc.getNodeById(long(docID))
        if selDocNode is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc id:%s not found.'%docID,origin=self.appl)
            return -1,'',''
        tmpNode=selDocNode
        while tmpNode is not None:
            sDN=netDoc.getNodeText(tmpNode,'path')
            if len(sDN)>0:
                lstPath.append(sDN)
            tmpNode=netDoc.getParent(tmpNode)
            if docBase==tmpNode:
                tmpNode=None
        
        lstPath.reverse()
        sPrj=self.getNodeText(nodePrjInf,'name')
        sClt=self.getNodeText(nodePrjInf,'clientshort')
        sFac=self.getNodeText(nodePrjInf,'facility')
        #sDocGrp=self.netDoc.getNodeText(self.selDocNode,'key')
        sDocGrp=netDoc.getKey(selDocNode)
        if len(sDocGrp)<=0:
            iRes=-1
            return -1,'',''
        lstFN=[string.join(map(string.capitalize,[sClt,sPrj]),'')]
        lstFN.append(sDocGrp)
        
        sFN=string.join(lstFN,'_')
        sDocPath=string.join(lstPath,os.path.sep)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'docPath:%s;fn:%s'%(sDocPath,sFN),origin=self.appl)
        return 0,sDocPath,sFN+u'_'
    
