#Boa:FramePanel:vPrjDocMainPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocMainPanel.py,v 1.8 2007/08/21 18:14:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.anchors as anchors
import getpass

#import vidarc.vApps.vHum.impMin
try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.tool.time.vTimeTimeEdit import *
    
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
    from vidarc.tool.net.vtNetFileWxGui import *
    
    from vidarc.vApps.vPrjDoc.vXmlPrjDocTree  import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    from vidarc.vApps.vPrjDoc.vDocSelPanel  import *
    from vidarc.vApps.vPrjDoc.vPrjDocPanel  import *
    from vidarc.vApps.vPrjDoc.vPrjDocSettingsDialog  import *
    import vidarc.tool.InOut.fnUtil as fnUtil
    
    import vidarc.vApps.vPrjDoc.images as imgPrjDoc
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vPrjDocMainPanel(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VPRJDOCMAINPANEL, wxID_VPRJDOCMAINPANELPNINFO, 
 wxID_VPRJDOCMAINPANELPNITEM, wxID_VPRJDOCMAINPANELSLWNAV, 
 wxID_VPRJDOCMAINPANELSLWTOP, wxID_VPRJDOCMAINPANELSPWMAIN, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VPRJDOCMAINFRAMEMNFILEITEM_EXIT, 
 wxID_VPRJDOCMAINFRAMEMNFILEITEM_SETTING, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(2)]

[wxID_VPRJDOCMAINFRAMEMNIMPORTITEM_IMPORT_CSV, 
 wxID_VPRJDOCMAINFRAMEMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VPRJDOCMAINFRAMEMNEXPORTITEM_EXPORT_CSV, 
 wxID_VPRJDOCMAINFRAMEMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VPRJDOCMAINFRAMEMNTOOLSITEM_ALIGN, 
 wxID_VPRJDOCMAINFRAMEMNTOOLSITEM_TASK, 
 wxID_VPRJDOCMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(3)]

[wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
 wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_DOC_ID_DOCGRP, 
 wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_DOC_NORMAL, 
 wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_USR_DATE_GRP, 
 wxID_VPRJDOCMAINFRAMEMNVIEWTREEITEM_USR_DATE_SHORT_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(6)]

def getPluginImage():
    return imgPrjDoc.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPrjDoc.getPluginBitmap())
    return icon

class vPrjDocMainPanel(wx.Panel):
    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spwMain, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCMAINPANEL,
              name=u'vPrjDocMainPanel', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(771, 393), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(763, 366))
        #self.Bind(wx.EVT_CLOSE, self.OnVgfPrjTimerMainClose)
        #self.Bind(wx.EVT_ACTIVATE, self.OnVgfMainActivate)
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPRJDOCMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPRJDOCMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VPRJDOCMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(440, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VPRJDOCMAINPANELSLWTOP)
        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        self.bActivated=False
        self.dCfg={}
        
        self.bModified=False
        self.selNode=None
        
        self.xdCfg=vtXmlDom.vtXmlDom('vPrjDocCfg',audit_trail=False)
        
        self.baseDoc=None
        self.baseSettings=None
        self.basePrjInfo=None
        self.baseTasks=None
        self.tasks=[]
        self.subtasks=[]
        
        appls=['vPrjDoc','vDoc','vHum']#,'vFileSrv']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0,
                                            audit_trail=True)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        #self.netFileSrv=self.netMaster.AddNetControl(vtNetFileWxGui,'vFileSrv',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        EVT_NET_XML_SETUP_CHANGED(self.netHum,self.OnSetupChanged)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjDocNetMaster')
        #EVT_NET_XML_MASTER_START(self.netMaster,self.OnMasterStart)
        self.netPrjDoc.SetDftLanguages()
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        #EVT_NET_XML_OPEN_START(self.netPrjDoc,self.OnOpenStart)
        #EVT_NET_XML_OPEN_OK(self.netPrjDoc,self.OnOpenOk)
        #EVT_NET_XML_OPEN_FAULT(self.netPrjDoc,self.OnOpenFault)
        
        #EVT_NET_XML_SYNCH_START(self.netPrjDoc,self.OnSynchStart)
        #EVT_NET_XML_SYNCH_PROC(self.netPrjDoc,self.OnSynchProc)
        #EVT_NET_XML_SYNCH_FINISHED(self.netPrjDoc,self.OnSynchFinished)
        #EVT_NET_XML_SYNCH_ABORTED(self.netPrjDoc,self.OnSynchAborted)
        
        #EVT_NET_XML_CONNECT(self.netPrjDoc,self.OnConnect)
        #EVT_NET_XML_DISCONNECT(self.netPrjDoc,self.OnDisConnect)
        #EVT_NET_XML_LOGGEDIN(self.netPrjDoc,self.OnLoggedin)
        #EVT_NET_XML_CONTENT_CHANGED(self.netPrjDoc,self.OnChanged)
        EVT_NET_XML_GOT_CONTENT(self.netPrjDoc,self.OnGotContent)
        #EVT_NET_XML_SETUP_CHANGED(self.netPrjDoc,self.OnSetupChanged)
        
        self.vgpTree=vXmlPrjDocTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=wx.DefaultSize,style=0,name="trPrjDoc",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        self.vgpTree.SetDoc(self.netPrjDoc,True)
        #self.actLanguage='en'
        #self.vgpTree.SetLanguage(self.actLanguage)
        #self.vgpTree.SetLanguages(self.languages,self.languageIds)

        self.vgpDocSel=vDocSelPanel(self.slwTop,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name="vgpDocSel")
        #self.vgpDocSel.SetConstraints(
        #        anchors.LayoutAnchors(self.vgpDocSel, True, True, True, True)
        #        )
        EVT_VGPDOC_SEL_DOC_ADDED(self.vgpDocSel,self.OnDocAdded)
        self.vgpDocSel.SetDocPrjDoc(self.netPrjDoc,True)
        self.vgpDocSel.SetDoc(self.netDoc,True)
        self.vgpDocSel.SetLanguage(vtLgBase.getPluginLang())
        
        #EVT_VGPDOC_CHANGED(self.vglbDocGrpInfo,self.OnDocChanged)
        
        self.vgpPrjDoc=vPrjDocPanel(self,wx.NewId(),
                    pos=(0,0),size=(598, 243),style=0,name="vgpPrjDoc")
        #self.vgpPrjDoc.SetConstraints(
        #        anchors.LayoutAnchors(self.vgpPrjDoc, True, True, True, True)
        #        )
        self.vgpPrjDoc.SetDoc(self.netPrjDoc,True)
        self.pnData=self.vgpPrjDoc
        
        EVT_VGPPRJDOCUMENT_ADDED(self.vgpPrjDoc,self.OnPrjDocAdded)
        EVT_VGPPRJDOC_GRP_INFO_CHANGED(self.vgpPrjDoc,self.OnDocGrpChanged)
        
        #self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netPrjDoc
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
        
    def __setModified__(self,state):
        self.bModified=state
        #self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        evt.Skip()
    def OnAddElementsFin(self,evt):
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnTreeItemSel(self,event):
        #print event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
            self.vgpPrjDoc.SetNode(event.GetTreeNodeData())
            #,self.vgpTree.GetIds())
            
        else:
            # grouping tree item selected
            #self.vglbDocGrpInfo.SetNode(None,None)
            pass
        pass
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        
    def OnPrjDocAdded(self,event):
        prjDoc=event.GetNode()
        if prjDoc==self.selNode:
            self.vgpPrjDoc.SetNode(self.selNode)
        
        self.__setModified__(True)
    def OnDocGrpChanged(self,event):
        self.vgpPrjDoc.RefreshDocuments()
        self.__setModified__(True)
    def OnDocAdded(self,event):
        fn=event.GetFN()
        docNode=event.GetNode()
        docGrpId=long(self.netDoc.getAttribute(docNode,'id'))
        fn=fnUtil.getFilenameRel2BaseDir(fn,self.xmlProjectDN)
        prjDocs=self.netPrjDoc.getChilds(self.baseDoc,'docgroup')
        bFound=False
        for prjDoc in prjDocs:
            id,appl=self.netPrjDoc.getForeignKeyNumAppl(prjDoc,attr='fid',appl='vDoc')
            if id==docGrpId:
                bFound=True
                break
        if bFound==False:
            prjDoc=self.netPrjDoc.createSubNode(self.baseDoc,'docgroup',False)
            self.netPrjDoc.setForeignKey(prjDoc,attr='fid',attrVal=docGrpId,appl='vDoc')
            titles=self.netDoc.getChilds(docNode,'title')
            for t in titles:
                lang=self.netDoc.getAttribute(t,'language')
                self.netPrjDoc.setNodeTextLang(prjDoc,'title',self.netDoc.getText(t),lang)
            self.netPrjDoc.addNode(self.baseDoc,prjDoc)
            self.netPrjDoc.AlignNode(prjDoc)
        if event.GetDocNode() is not None:
            self.netPrjDoc.appendChild(prjDoc,event.GetDocNode())
            self.netPrjDoc.addNode(prjDoc,event.GetDocNode())
        self.netPrjDoc.AlignNode(prjDoc,iRec=2)
        if bFound==True:
            if self.netPrjDoc.isSame(prjDoc,self.selNode):
                self.vgpPrjDoc.SetNode(self.selNode)
        self.__setModified__(True)
    def OnDocChanged(self,event):
        self.__setModified__(True)
    def OnSetupChanged(self,evt):
        if self.netPrjDoc.ReConnect2Srv()==0:
            self.netHum.ReConnect2Srv()
            self.netDoc.ReConnect2Srv()
        
    def OnMasterStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.vgpTree.Clear()
        self.vgpPrjDoc.Clear()
        self.PrintMsg(_(u'master started ...'))
        evt.Skip()
    
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'file opened, building ...'))
        self.vgpPrjDoc.Clear()
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'synch started ... %07d - %07d')%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_(u'synch started ... %07d - %07d')%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'synch finished.'))
        self.gProcess.SetValue(0)
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnGotContent(self,evt):
        evt.Skip()
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        #self.vgpPrjDoc.Clear()
        self.__getXmlBaseNodes__()
        #cfgNode=self.netPrjDoc.getChild(None,'config')
        #self.netPrjDoc.SetCfgNode(cfgNode)
        #self.netHum.SetCfgNode(cfgNode)
        #self.netDoc.SetCfgNode(cfgNode)
        #self.netHum.AutoConnect2Srv()
        #self.netDoc.AutoConnect2Srv()
    def __getSettings__(self):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        self.xmlDocFN=self.netPrjDoc.getNodeText(self.baseSettings,'docfn')
        self.xmlHumanFN=self.netPrjDoc.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netPrjDoc.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netPrjDoc.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netPrjDoc.getNodeText(self.baseSettings,'projectdn')
        self.xmlMode=self.netPrjDoc.getNodeText(self.baseSettings,'mode')
        
        self.xmlBaseTmplDN=string.strip(self.xmlBaseTmplDN)
        self.xmlProjectDN=string.strip(self.xmlProjectDN)
        self.xmlMode=string.strip(self.xmlMode)
        
        self.vgpDocSel.SetBaseTmplDN(self.xmlBaseTmplDN)
        self.vgpDocSel.SetProjectDN(self.xmlProjectDN)
        self.vgpDocSel.SetMode(self.xmlMode)
        self.vgpPrjDoc.SetProjectDN(self.xmlProjectDN)
        # get tree view settings
        sTreeViewGrp=self.netPrjDoc.getNodeText(self.baseSettings,'treeviewgrp')
        sDocTreeView=self.netPrjDoc.getNodeText(self.baseSettings,'doctreeview')
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        #self.__makeTitle__()
        self.PrintMsg(_(u'connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'connection lost.'))
        evt.Skip()
    def OnLoggedin(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_(u'get content ... '))
        evt.Skip()
    def OnChanged(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__setModified__(True)
        evt.Skip()
    def __getPrjInfo__(self):
        self.xmlProjectShortcut=self.netPrjDoc.getNodeText(self.basePrjInfo,'name')
        self.xmlProjectClient=self.netPrjDoc.getNodeText(self.basePrjInfo,'clientshort')
        self.xmlProjectShortcut=string.strip(self.xmlProjectShortcut)
        self.xmlProjectClient=string.strip(self.xmlProjectClient)
        self.vgpDocSel.SetPrjInfo(self.xmlProjectShortcut,self.xmlProjectClient)
        
        self.prjinfos={}
        sID=self.netPrjDoc.getAttribute(self.basePrjInfo,'id')
        self.prjinfos[u'id']=sID
        nodes=self.netPrjDoc.getChilds(self.basePrjInfo)
        for n in nodes:
            tagName=self.netPrjDoc.getTagName(n)
            val=self.netPrjDoc.getText(n)
            self.prjinfos[tagName]=val
        self.vgpDocSel.SetPrjInfos(self.prjinfos)
    def __getXmlBaseNodes__(self):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.baseDoc=None
        self.baseSettings=None
        self.baseTasks=None
        self.tasks=[]
        self.subtasks=[]
        
        self.baseDoc=self.netPrjDoc.getChild(self.netPrjDoc.getRoot(),'prjdocs')
        self.baseSettings=self.netPrjDoc.getChild(self.netPrjDoc.getRoot(),'settings')
        if self.baseSettings is None:
            self.baseSettings=self.netPrjDoc.createSubNode(self.netPrjDoc.getRoot(),'settings')
        self.__getSettings__()
            
        self.basePrjInfo=self.netPrjDoc.getChild(self.netPrjDoc.getRoot(),'prjinfo')
        if self.basePrjInfo is None:
            self.basePrjInfo=self.netPrjDoc.createSubNode(self.netPrjDoc.getRoot(),'prjinfo')
        self.__getPrjInfo__()
            
        #self.vgpPrjTimeInfo.SetTasks(self.tasks)
        #self.vgpPrjTimeInfo.SetSubTasks(self.subtasks)
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
    def OpenFile(self,fn):
        if self.netPrjDoc.ClearAutoFN()>0:
            self.netPrjDoc.Open(fn)
            #self.netDoc.Open()
            #self.netHum.Open()
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
            
            #self.vgpTree.SetLanguage(self.actLanguage)
            #self.vgpTree.SetNode(self.baseDoc)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def CreateNew(self):
        self.netPrjDoc.New()
        self.CreateSetup()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(self.baseDoc)
        #self.vgpPrjTimeInfo.SetRootNode(self.baseDoc)
        #self.vgpPrjTimeInfo.SetStarted(None)
        self.__setModified__(True)
    def CreateSetup(self):
        #if self.dom is None:
        #    self.dom.New(root="projectdocuments")
        elem=self.netPrjDoc.createSubNode(self.netPrjDoc.getRoot(),'settings',False)
        # add project file
        elemPrjFN=self.netPrjDoc.createSubNodeText(elem,'projectfn','---',False)
        # add project file
        elemTaskFN=self.netPrjDoc.createSubNodeText(elem,'taskfn','---',False)
        # add human file
        elemHumanFN=self.netPrjDoc.createSubNodeText(elem,'humanfn','---',False)
        # add template dir
        elemTmplDN=self.netPrjDoc.createSubNodeText(elem,'templatedn','---',False)
        # add project dir
        elemPrjDN=self.netPrjDoc.createSubNodeText(elem,'projectdn','---',False)
        self.netPrjDoc.setNodeText(elem,'lastprj','---')
        self.netPrjDoc.setNodeText(elem,'lasttask','---')
        self.netPrjDoc.setNodeText(elem,'lastsubtask','---')
        self.netPrjDoc.setNodeText(elem,'lastdesc','---')
        self.netPrjDoc.setNodeText(elem,'lastloc','---')
        self.netPrjDoc.setNodeText(elem,'lastperson','---')
        self.netPrjDoc.setNodeText(elem,'treeviewgrp','usrdate')
        self.netPrjDoc.setNodeText(elem,'doctreeview','normal')
        
        self.netPrjDoc.AlignNode(elem,iRec=2)

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netPrjDoc.getChildForced(self.netPrjDoc.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vPrjDocSettingsDialog(self)
            dlg.SetXml(self.netPrjDoc,nodeSetting)
            dlg.Centre()
            ret=dlg.ShowModal()
            if ret>0:
                checkInfos=vtXmlDom.NodeInfo(self.netPrjDoc,nodeSetting)
                dlg.GetXml(self.netPrjDoc,nodeSetting)
                res=checkInfos.checkDiff(nodeSetting)
                if len(res.keys())>0:
                    self.__setModified__(True)
                self.__getSettings__()
                self.netPrjDoc.AlignNode(nodeSetting,iRec=2)
            dlg.Destroy()
        event.Skip()

    def OnMnToolsItem_taskMenu(self, event):
        node=self.dom.getElementsByTagName('tasks_subtasks')
        if len(node)>0:
            dlg=vgdDlgPrjTask(self)
            dlg.Centre()
            nodeTaskSubTask=node[0]
            tasks=node[0].getElementsByTagName('tasks')
            if len(tasks)>0:
                task=tasks[0]
            else:
                task=None
            dlg.SetXmlTask(task)
            subTasks=node[0].getElementsByTagName('subtasks')
            if len(subTasks)>0:
                subTask=subTasks[0]
            else:
                subTask=None
            dlg.SetXmlSubTask(subTask)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXmlTask(task)
                dlg.GetXmlSubTask(subTask)
                self.__setModified__(True)
                self.tasks=vtXmlDomTree.getNodeTextList(self.baseTasks,'task')
                self.subtasks=vtXmlDomTree.getNodeTextList(self.baseTasks,'subtask')
                self.vgpPrjTimeInfo.SetTasks(self.tasks)
                self.vgpPrjTimeInfo.SetSubTasks(self.subtasks)
                if self.baseTasks is not None:
                    vtXmlDomTree.vtXmlDomAlignNode(self.dom,nodeTaskSubTask)
                pass
            dlg.Destroy()
        event.Skip()
    def SetPrjs(self,prjs):
        self.cmbPrj.Clear()
        for t in prjs:
            self.cmbPrj.Append(t)
    def SetPersons(self,persons):
        self.cmbPerson.Clear()
        for t in persons:
            self.cmbPerson.Append(t)
    def SetLocs(self,locs):
        self.cmbLoc.Clear()
        for t in locs:
            self.cmbLoc.Append(t)
    def SetTasks(self,tasks):
        self.cmbTask.Clear()
        for t in tasks:
            self.cmbTask.Append(t)
    def SetSubTasks(self,subtasks):
        self.cmbSubTask.Clear()
        for t in subtasks:
            self.cmbSubTask.Append(t)
    
    def __validEmptyTextNode__(self,txt):
        if len(string.strip(txt))==0:
            return '---'
        else:
            return txt
    def __EnsureOneCheckMenuItem__(self,mnItems,iAct,iStart,iCount):
        if mnItems[iAct].IsChecked():
            # uncheck all other
            for i in range(iStart,iStart+iCount):
                if i!=iAct:
                    mnItems[i].Check(False)
            return False
        else:
            bFound=False
            for i in range(iStart,iCount):
                if mnItems[i].IsChecked()==True:
                    bFound=True
            if bFound==False:
                mnItems[iAct].Check(True)
                return True
            return False
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        #print 'SetCfgData',self.dCfg
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        #try:
        #    pos=map(int,self.dCfg['pos'].split(','))
        #    self.GetParent().Move(pos)
        #except:
        #    pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Project documents module.

    def OnVgfPrjTimerMainClose(self, event):
        event.Skip()

    def OnVgfMainActivate(self, event):
        event.Skip()

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Project Document'),desc,version

    def OnVgfPrjTimerMainClose(self, event):
        event.Skip()

    def OnVgfMainActivate(self, event):
        event.Skip()
