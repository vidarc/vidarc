#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDocRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vXmlNodePrjDocRoot.py,v 1.4 2007/01/14 21:44:52 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
#from vidarc.vApps.vPrjDoc.vXmlNodeCfgLocal import vXmlNodeCfgLocal
try:
    if vcCust.is2Import(__name__):
        GUI=1
        from vidarc.vApps.vPrjDoc.vPrjDocSettingsDialog import vPrjDocSettingsDialog
        
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrjDocRoot(vtXmlNodeRoot):#,vXmlNodeCfgLocal):
    #FUNCS_GET_SET_4_LST=[]
    #FMT_GET_4_TREE=[('Name','')]
    def __init__(self,tagName='prjdocs'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        vtXmlNodeBase.__init__(self,tagName)
        #vXmlNodeCfgLocal.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project documents root')
    # ---------------------------------------------------------
    # specific
    def GetPrjDN(self,node):
        #return vXmlNodeCfgLocal.GetPrjDN(node)
        nodeTmp=self.doc.getChild(self.doc.getRoot(),'settings')
        return self.doc.getNodeText(nodeTmp,'projectdn')
    def GetMode(self,node):
        #return vXmlNodeCfgLocal.GetPrjDN(node)
        nodeTmp=self.doc.getChild(self.doc.getRoot(),'settings')
        return self.doc.getNodeText(nodeTmp,'mode')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xcaIDAT8\x8d\x8d\x93\xbfk\x14A\x14\xc7?3\xb3\x93\xdd\xbd$\x97\xbb\
\xa8\x04\xd3\x88QD\x0c\x11\x14\r\x88\x8a\x85XYz\xff@\n;\x05\x0b\x0b\xc5\xc6F\
H\x99"\xfe\xc0\xc6\xd2&\x85\x08\x96V\xa6Ja\xa1\x16IZ\r\x06\xa3\xe1\xb8\xbb\
\xdd=wgw,\xe4\xce\xdc\xde\x1c\xf8\xaa\x99/\xdf\xf7\xe6\xbd\xef\xbc\xaf\x10RQ\
\x8e\x85\xe5\x86\x1d\x02\x81\xcf\x0f\xd6D\x19\x93\xffKta\x00\xa2\xd7\xc1\xc1\
W\x9f\x9c\xbe\xc5\xb7\xaf\xdb\x9c\x99\xbf\xc0\xdd\x8dW\xae\xbc~Ag\x07\x95@q\
\xea\xc4,i\xfc\x9d\x95s\r^\\^r\x16\x01\xf0\\\xa0\xb4\x96\x10\x0fc\xba\x8cW3\
\xa4\xbf\xcf\xb3\x8b\xd7H\xba\x16+\xa6\xb9\xff\xe9\xcd?nY\xb0\xe7\x97n\xe3\
\xd9\x82\xce~\x9b\xa8\x19#2\x98\n+\xcc\xd4\x03\xa6k\x1d\xd6\xd7\xdf\xf6\xb9\
\x0b\xcb\r+\xcb\xf3\x17\xa6Cu\xc2g,\x18\xc3\x9f\x08\xf1\xc7C\xbaQB\x9e\x19L\
\x0e\xc7\xe7\xce\x0eh6\xa0\xc1\xcb\xabK\xd4\xab\x12\xad-\xb5#\x93\x1c\x9e\
\xa9\xa3<\x891)\xca\xb3\xc4I\xc6\xd1\xd9\x93\xa35\xf0\xf8\x85\xaf|\xd24\xc5\
\x0f$\xb6\xc8H\x7f\'(\x9d\xa3\xb4!N\n\x1en\xae\xb9\x0b<]\xbc\xc1\xa1\x9a!\
\x8b2\x8a\xc2\x90\x1bI\x9e\x1b\x90\x19A\xa8\x88\xd3\x16VL\x0e\x0b\x0e\x7f\
\xff\xb4\xdd\xde%N\xf6\xb0\xaa\x85\xd6\tBEX:@\x8c\xd4\t\xcd\xa8Mu\xeaX?\xb1\
\xb7\x07\x03\x8b\xb4\xbax\x93<\xdb\xa5\xe2\x1b\xc2\x10\xb4\x04\xa5\nth\xf8\
\xb2\xd9\xe2\xd1\xd6\xceP\x81\x01\r\xeel\xbc\xeb\x9fW\xce_G\xd8\x1fT\xc2\x88\
x\xaf\x89\xa7\xe7\x81\x1d\xca\xe1\\$\x80{\x1f\xdf\x03\xf0x\xee\n[\xdb?y\xed}\
p\xf2\x84\xcb\x8d\xbd\x91\x0e\xdeG\x99\xc9\xe9\x05\x97\x9dGY\xfc\x0f(\n\xb17\
\xac\xcb\xdf\x17\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        #return vXmlNodeCfgLocal.GetEditDialogClass(self)
        if GUI:
            return vPrjDocSettingsDialog
        else:
            return None
