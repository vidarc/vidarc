#Boa:Frame:vPrjDocMainFrame
#----------------------------------------------------------------------------
# Name:         vPrjDocMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vPrjDocMainFrame.py,v 1.17 2007/09/02 14:56:55 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vPrjDoc.images as images
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.vApps.common.vCfg import vCfg
    from vidarc.vApps.vPrjDoc.vPrjDocMainPanel import *#vPrjDocMainPanel
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vLoc'):
    return vDocMainFrame(parent)

[wxID_VPRJDOCMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vPrjDocMainFrame(wx.Frame,vMDIFrame,vCfg):
    STATUS_CLK_POS=4
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VPRJDOCMAINFRAME,
              name=u'vPrjDocMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'VIDARC Project Document')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'PrjDoc',iNotifyTime=250)
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vPrjDocMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vDocPanel')
            self.pn.OpenCfgFile('vPrjDocCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
            
            #self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.dCfg.update(self._getCfgData(['vPrjDocGui']))
        self.pn.SetCfgData(self._setCfgData,['vPrjDocGui'],self.dCfg)
        self.__setCfg__()
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def OpenCfgFile(self,fn=None):
        self.pn.OpenCfgFile(fn)
        self.dCfg.update(self._getCfgData(['vPrjDocGui']))
        self.pn.SetCfgData(self._setCfgData,['vPrjDocGui'],self.dCfg)
        self.__setCfg__()
    def __setCfg__(self):
        try:
            #print self.dCfg
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def __getPluginImage__(self):
        return images.getPluginImage()
    def __getPluginBitmap__(self):
        return images.getPluginBitmap()
    def OnMainClose(self,event):
        if self.pn.xdCfg is not None:
            self._setCfgData(['vPrjDocGui'],self.dCfg)
        vMDIFrame.OnMainClose(self,event)
