#Boa:FramePanel:Panel1
#----------------------------------------------------------------------------
# Name:         vPrjDocGrpNoteBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocGrpInfoNoteBook.py,v 1.4 2007/08/21 18:14:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#import ColorPanel
import images_lang
from vidarc.vApps.vDoc.vDocGrpInfoSecPanel import *
from vidarc.vApps.vPrjDoc.vPrjDocGrpInfoPanel import *
from vidarc.vApps.vPrjDoc.vPrjDocGrpInfoControlPanel import *
from vidarc.vApps.vPrjDoc.vPrjDocInfoPanel import *
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_PANEL1] = [wx.NewId() for _init_ctrls in range(1)]

class vPrjDocGrpInfoNoteBook(wx.Notebook):
    def _init_ctrls(self, prnt):
        wx.Notebook.__init__(self, style=wx.TAB_TRAVERSAL, name='', parent=prnt, pos=wx.DefaultPosition, id=wxID_PANEL1, size=wx.Size(100, 50))

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        #self.SetPadding(wx.Size(2,2))
        self.pnDoc=None
        self.prjDN=None
        self.node=None
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)

    def AddDocumentPanel(self,parent):
        self.pnDoc=vPrjDocInfoPanel(parent,0,wx.Point(0,0),wx.Size(200,100),0,u"")
        EVT_VGPPRJDOCUMENT_CHANGED(self.pnDoc,self.OnDocumentChanged)
        self.pnDoc.SetTemplateBaseDir(self.tmplDN)
        return self.pnDoc
    def SetLanguages(self,languages,languageIds,pnCtrl=None,pnAcl=None):
        languages=[u'English',u'Deutsch']
        il = wx.ImageList(16, 16)
        for i in range(0,len(languages)):
            f=getattr(images_lang, 'getLang%02dBitmap' % i)
            bmp = f()
            il.Add(bmp)
        idx=il.Add(images_lang.getApplyMultipleBitmap())
        self.AssignImageList(il)
        
        if pnCtrl is None:
            sName="vPrjDocGrpInfoControlPanel"
            pnCtrl=vPrjDocGrpInfoControlPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,240),style=0,name=sName)
            EVT_VGPPRJDOC_GRP_INFO_CHANGED(pnCtrl,self.OnDocGrpChanged)
            EVT_VGPPRJDOC_GRP_INFO_CANCELED(pnCtrl,self.OnDocGrpCanceled)
        self.AddPage(pnCtrl,_(u'General'),False,idx)
        #pnCtrl=None
        self.pnCtrl=pnCtrl
        if pnAcl is None:
            sName="vDocGrpAclPanel"
            pnAcl=vDocGrpInfoSecPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,240),style=0,name=sName)
            EVT_VGPPRJDOC_GRP_INFO_CHANGED(pnCtrl,self.OnDocGrpChanged)
            EVT_VGPPRJDOC_GRP_INFO_CANCELED(pnCtrl,self.OnDocGrpCanceled)
        self.AddPage(pnAcl,_(u'ACL'),False,idx)
        #pnCtrl=None
        self.pnAcl=pnAcl
        
        self.vpgDocGrpInfos=[]
        for i in range(0,len(languages)):
            sName="vPrjDocGrpInfo%sPanel"%languages[i]
            panel=vPrjDocGrpInfoPanel(self,wx.NewId(),
                    pos=(0,0),size=(190,240),style=0,name=sName)
            panel.SetLanguage(languageIds[i])
            self.AddPage(panel,languages[i],i==0,i)
            self.vpgDocGrpInfos.append(panel)
            EVT_VGPPRJDOCUMENT_SELECTED(panel,self.OnDocumentSelected)
            EVT_VGPPRJDOC_CHANGED(panel,self.OnDocChanged)
            EVT_VGPPRJDOC_GRP_INFO_CHANGED(panel,self.OnDocGrpChanged)
            EVT_VGPPRJDOC_GRP_INFO_CANCELED(panel,self.OnDocGrpCanceled)
        self.languageIds=[]
        for id in languageIds:
            self.languageIds.append(id)
    def SetPrjDir(self,dn):
        self.prjDN=dn
        if self.pnDoc is not None:
            self.pnDoc.SetTemplateBaseDir(self.prjDN)
        
        for panel in self.vpgDocGrpInfos:
            panel.SetPrjDir(self.prjDN)
    def SetDoc(self,doc,bNet):
        if bNet:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            
        self.doc=doc
        if self.pnCtrl is not None:
            self.pnCtrl.SetDoc(doc,bNet)
        if self.pnAcl is not None:
            self.pnAcl.SetDoc(doc,bNet)
            dNet=doc.GetNetDocs()
            try:
                if 'vHum' in dNet:
                    d=dNet['vHum']
                    self.pnAcl.SetNetDocHuman(d['doc'],d['bNet'])
            except:
                pass
        for p in self.vpgDocGrpInfos:
            p.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.SetSelection(0)
        if self.node is not None:
            self.doc.endEdit(self.node)
        self.node=node
        if self.pnCtrl is not None:
            self.pnCtrl.SetNode(node)
        if self.pnAcl is not None:
            oAcl=self.doc.GetReg('docgroupACL')
            bOk=self.doc.IsAclOk('docgroupACL',self.doc.ACL_MSK_WRITE)
            if bOk:
                self.pnAcl.SetNode(node)
            else:
                self.pnAcl.SetNode(None)
        bOkRd=self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_READ)
        bOkWr=self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE)
        for p in self.vpgDocGrpInfos:
            if bOkRd:
                p.SetNode(node)
                if bOkWr:
                    p.Lock(False)
                else:
                    p.Lock(True)
            else:
                p.SetNode(None)
        if self.node is not None:
            self.doc.startEdit(self.node)

    def Clear(self):
        self.SetSelection(0)
        if self.node is not None:
            self.doc.endEdit(self.node)
        if self.pnCtrl is not None:
            self.pnCtrl.Clear()
        if self.pnAcl is not None:
            self.pnAcl.Clear()
        for p in self.vpgDocGrpInfos:
            p.Clear()
    def OnGotContent(self,evt):
        self.Clear()
        evt.Skip()
    def OnPageChanged(self, event):
        old = event.GetOldSelection() -2
        sel = event.GetSelection() -2
        vtLog.vtLngCurWX(vtLog.DEBUG,'old:%d;new:%d'%(old,sel),self)
        if self.pnDoc is not None:
            self.pnDoc.Clear()
        #if old >= 0:
        #    oldPanel=self.vpgDocGrpInfos[old]
        if sel >= 0:
            try:
                selPanel=self.vpgDocGrpInfos[sel]
                #bOkRd=self.doc.IsAclOk('document',self.doc.ACL_MSK_READ)
                #bOkWr=self.doc.IsAclOk('document',self.doc.ACL_MSK_WRITE)
                bOkRd=True
                bOkWr=True
                if self.pnDoc is not None:
                    if bOkRd:
                        self.pnDoc.SetNode(selPanel.GetSelectedDoc(),
                                selPanel.GetLanguage())
                        if bOkWr:
                            self.pnDoc.Lock(False)
                        else:
                            self.pnDoc.Lock(True)
                wx.PostEvent(self,vgpPrjDocumentSelected(self,
                            selPanel.GetSelectedDoc(),selPanel.GetLanguage()))
            except:
                wx.PostEvent(self,vgpPrjDocumentSelected(self,
                            None,None))
                pass
        #elif sel==-1:
        #    if self.pnAcl is not None:
        #        self.pnAcl.SetNode
        else:
            wx.PostEvent(self,vgpPrjDocumentSelected(self,
                    None,None))
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def OnDocGrpChanged(self,event):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bOkWr=self.doc.IsNodeAclOk(self.node,self.doc.ACL_MSK_WRITE)
        if bOkWr==False:
            return
        if self.pnCtrl is not None:
            self.pnCtrl.GetNode(self.node)
        if self.pnAcl is not None:
            self.pnAcl.GetNode(self.node)
        #if self.pnDoc is not None:
        #    self.pnDoc.GetNode(selPanel.GetSelectedDoc(),
        #            selPanel.GetLanguage())
        for panel in self.vpgDocGrpInfos:
            panel.GetNode(self.node)
        wx.PostEvent(self,vgpPrjDocGrpInfoChanged(self,self.node,None))
    def OnDocGrpCanceled(self,event):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.pnCtrl is not None:
            self.pnCtrl.SetNode(self.node,self.ids)
        if self.pnAcl is not None:
            self.pnAcl.SetNode(self.node,self.ids)
        for panel in self.vpgDocGrpInfos:
            panel.SetNode(self.node,self.ids)
        wx.PostEvent(self,vgpPrjDocGrpInfoCanceled(self,self.node,None))
        pass
    def OnDocumentSelected(self,event):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        wx.PostEvent(self,
            vgpPrjDocumentSelected(self,event.GetNode(),event.GetLanguage()))
        d=event.GetNode()
        if d is not None:
            self.doc.startEdit(d['document'])
        if self.pnDoc is not None:
            self.pnDoc.SetNode(event.GetNode(),event.GetLanguage())
            pass
        pass
    def OnDocumentChanged(self,event):
        sel = self.GetSelection() -2
        vtLog.vtLngCurWX(vtLog.DEBUG,'sel:%d'%(sel),self)
        if sel<0:
            return
        selPanel=self.vpgDocGrpInfos[sel]
        selPanel.RefreshDocuments()
        wx.PostEvent(self,vgpPrjDocChanged(self,None,self.lang))
        pass
    def OnDocChanged(self,event):
        wx.PostEvent(self,vgpPrjDocChanged(self,None,None))
        pass
    def Apply(self):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.node is None:
            return
        if self.pnAcl is not None:
            self.pnAcl.GetNode()
        
        bOkWr=self.doc.IsNodeAclOk(self.node,self.doc.ACL_MSK_WRITE)
        for p in self.vpgDocGrpInfos:
            if bOkWr:
                p.GetNode(self.node)
        self.doc.doEdit(self.node)
