#Boa:Dialog:vAddDocDialog
#----------------------------------------------------------------------------
# Name:         vAddDocDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vAddDocDialog.py,v 1.3 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import cStringIO
#from boa.vApps.vPrjDoc.vgpAddDoc import *
from vidarc.vApps.vPrjDoc.vPrjDocGrpInfoNoteBook import *
from vidarc.vApps.vPrjDoc.vPrjDocInfoPanel import *

import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vAddDocDialog(parent)

[wxID_VADDDOCDIALOG, wxID_VADDDOCDIALOGGCBBCANCEL, wxID_VADDDOCDIALOGGCBBOK, 
 wxID_VADDDOCDIALOGPANEL1, wxID_VADDDOCDIALOGPANEL2, 
 wxID_VADDDOCDIALOGSPWINFO, 
] = [wx.NewId() for _init_ctrls in range(6)]

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)


class vAddDocDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spwInfo, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VADDDOCDIALOG, name=u'vAddDocDialog',
              parent=prnt, pos=wx.Point(138, 47), size=wx.Size(792, 443),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vPrjDoc Document Add'))
        self.SetClientSize(wx.Size(784, 416))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VADDDOCDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Ok'), name=u'gcbbOk',
              parent=self, pos=wx.Point(300, 382), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.SetMinSize(wx.Size(-1, -1))
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VADDDOCDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VADDDOCDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(408, 382),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VADDDOCDIALOGGCBBCANCEL)

        self.spwInfo = wx.SplitterWindow(id=wxID_VADDDOCDIALOGSPWINFO,
              name=u'spwInfo', parent=self, point=wx.Point(2, 2),
              size=wx.Size(780, 370), style=wx.SP_3D)
        self.spwInfo.SetMinimumPaneSize(20)

        self.panel1 = wx.Panel(id=wxID_VADDDOCDIALOGPANEL1, name='panel1',
              parent=self.spwInfo, pos=wx.Point(2, 2), size=wx.Size(780, 98),
              style=wx.TAB_TRAVERSAL)

        self.panel2 = wx.Panel(id=wxID_VADDDOCDIALOGPANEL2, name='panel2',
              parent=self.spwInfo, pos=wx.Point(2, 107), size=wx.Size(780, 261),
              style=wx.TAB_TRAVERSAL)
        self.spwInfo.SplitHorizontally(self.panel1, self.panel2, 200)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        #self.panel=vgdPrjTimerSettings(parent=self , id=wx.NewId(),
        #      pos=wx.Point(0, 0), size=wx.Size(240, 140), 
        #      style=wx.TAB_TRAVERSAL, name=u'vgpGroup')
        #self.panel=vgpAddDoc(self , wx.NewId(),
        #      wx.Point(0, 0), wx.Size(240, 140), 
        #      wx.TAB_TRAVERSAL, u'vgpGroup')
        
        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)
        
        self.vglbDocGrpInfo=vPrjDocGrpInfoNoteBook(self.spwInfo,wx.NewId(),
                pos=(0,0),size=(190,200),style=0,name="vglbDocGrpInfo")
        #self.vglbDocGrpInfo.SetConstraints(
        #        anchors.LayoutAnchors(self.vglbDocGrpInfo, True, True, True, True)
        #        )
        #EVT_VGPDOC_GRP_INFO_CHANGED(self.vglbDocGrpInfo,self.OnDocGrpInfoChanged)
        EVT_VGPPRJDOC_CHANGED(self.vglbDocGrpInfo,self.OnDocChanged)
        EVT_VGPPRJDOCUMENT_SELECTED(self.vglbDocGrpInfo,self.OnDocumentSel)
        self.spwInfo.ReplaceWindow(self.panel1,self.vglbDocGrpInfo)
        self.panel1.Destroy()
        self.languages=[u'English',u'Deutsch']
        self.languageIds=[u'en',u'de']
        self.vglbDocGrpInfo.SetLanguages(self.languages,self.languageIds)
        
        self.vgpDocInfo=vPrjDocInfoPanel(self.spwInfo,wx.NewId(),
            pos=(0,0),size=(300,200),style=0,name="vgpDocInfo")
        self.spwInfo.ReplaceWindow(self.panel2,self.vgpDocInfo)
        self.panel2.Destroy()
        
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def SetTemplateBaseDir(self,dn):
        self.vglbDocGrpInfo.SetTemplateBaseDir(dn)
    def SetNode(self,node,ids):
        self.vglbDocGrpInfo.SetNode(node,ids)
    def OnDocChanged(self,event):
        wxPostEvent(self,vgpPrjDocChanged(self,event.GetNode(),event.GetLanguage()))
    def OnDocumentSel(self,event):
        node=event.GetNode()
        if node is not None:
            self.vgpDocInfo.SetNode(node,event.GetLanguage())
    def OnGcbbOkButton(self, event):
        self.vglbDocGrpInfo.Apply()
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
