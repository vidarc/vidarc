#----------------------------------------------------------------------------
# Name:         vXmlNodeArchiveTmpl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060730
# CVS-ID:       $Id: vXmlNodeArchiveTmpl.py,v 1.2 2006/11/21 21:16:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import platform

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
from vidarc.vApps.vPrjDoc.vXmlNodeArchive import vXmlNodeArchive
try:
    if vcCust.is2Import(__name__):
        #from vXmlNodeXXXPanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeArchiveTmpl(vXmlNodeArchive):
    def __init__(self,tagName='archivetmpl'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        vXmlNodeArchive.__init__(self,tagName)
    def GetDescription(self):
        return _(u'archive template')
    # ---------------------------------------------------------
    # specific
    def GetDN(self,node):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.GetAttrVal(node,'directory','host',sHost)
        if len(sDN)>0:
            return sDN
        else:
            return self.GetVal(node,'directory',unicode,'')
    def SetDN(self,node,sDN):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.SetAttrVal(node,'directory',sDN,'host',sHost)
        
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL | vtSecXmlAclDom.ACL_MSK_BUILD
