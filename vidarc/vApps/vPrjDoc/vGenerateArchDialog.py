#Boa:Dialog:vGenerateArchDialog
#----------------------------------------------------------------------------
# Name:         vGenerateArchDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vGenerateArchDialog.py,v 1.6 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string,os,os.path,shutil,zipfile,bz2,tarfile
import thread,types
import vidarc.tool.InOut.fnUtil as fnUtil
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
from vidarc.tool.office.vtOfficeSCalc import *
from vidarc.tool.office.vtOfficeSWriter import *


import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

#from boa.vApps.vPrjDoc.vgpAddDoc import *
#from boa.vApps.vPrjDoc.vglbPrjDocGrpInfo import *

#import images

def create(parent):
    return vGenerateArchDialog(parent)

wxEVT_THREAD_ARCH=wx.NewEventType()
def EVT_THREAD_ARCH(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_ARCH,func)
class wxThreadArch(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_ARCH(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_ARCH)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_ARCH_FINISHED=wx.NewEventType()
def EVT_THREAD_ARCH_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_ARCH_FINISHED,func)
class wxThreadArchFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_ARCH_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_ARCH_FINISHED)

class thdArch:
    def __init__(self,par):
        self.par=par
        self.sFN=None
        self.archDN=None
        self.running = False
    def Arch(self,archDN,fn):
        self.sFN=fn
        self.archDN=archDN
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __add2CompressFile__(self,zipFile,sDir):
        iSize=0
        for root,dirs,files in os.walk(sDir):
            for sfn in files:
                if self.keepGoing==False:
                    return
                fn=root+os.path.sep+sfn
                if type(fn)==types.UnicodeType:
                    fn=fn.encode('ISO-8859-1')
                zipFile.write(str(fn))
                mode=os.stat(fn)
                iSize=iSize+mode.st_size
                wx.PostEvent(self.par,wxThreadArch(iSize/(1024.0*1024.0),-1))
        
    def GenerateZip(self,sDir):
        cur=0
        zipFile=zipfile.ZipFile(self.sFN,'w',zipfile.ZIP_DEFLATED)
        self.__add2CompressFile__(zipFile,sDir)
        zipFile.close()
        if self.keepGoing==False:
            os.remove(self.sFN)
    def GenerateBz2(self,sDir):
        cur=0
        i=string.rfind(self.sFN,'.')
        tarFN=self.sFN[:i]+'.tar.bz2'
        tar=tarfile.TarFile.open(tarFN,'w:bz2')
        #self.__add2CompressFile__(tar,sDir)
        iSize=0
        #self.gagProcess.SetRange(self.iSize/(1024.0*1024.0))
        for root,dirs,files in os.walk(sDir):
            for sfn in files:
                if self.keepGoing==False:
                    tar.close()
                    os.remove(tarFN)
                    return
                fn=root+os.path.sep+sfn
                if type(fn)==types.UnicodeType:
                    fn=fn.encode('ISO-8859-1')
                tar.add(str(fn))
                mode=os.stat(fn)
                iSize=iSize+mode.st_size
                wx.PostEvent(self.par,wxThreadArch(iSize/(1024.0*1024.0),-1))
        tar.close()
        if 1==0:
            print "tar",tarFN
            print "bz2",tarFN+'.bz2'
            f=os.open(tarFN,os.O_RDONLY)
            
            zip=bz2.BZ2File(tarFN+'.bz2','w',0,1)
            bStop=False
            iBufSize=1024
            while bStop==False:
                d=os.read(f,iBufSize)
                zip.write(d)
                print len(d),iBufSize
                if len(d)<=0:
                    bStop=True
            zip.close()

    def Run(self):
        try:
            sDir=os.path.split(self.sFN)
            os.makedirs(sDir[0])
        except:
            pass
        sCurDir=os.getcwd()
        sDir=os.path.split(self.archDN)
        os.chdir(sDir[0])

        i=string.rfind(self.sFN,'.')
        ext=self.sFN[i:]
        if ext=='.zip':
            self.GenerateZip(sDir[1])
        elif ext=='.bz2':
            self.GenerateBz2(sDir[1])
                
        os.chdir(sCurDir)
        
        wx.PostEvent(self.par,wxThreadArch(0,0))
        if self.keepGoing==True:
            wx.PostEvent(self.par,wxThreadArchFinished())
        self.keepGoing = self.running = False
        

[wxID_VGENERATEARCHDIALOG, wxID_VGENERATEARCHDIALOGCBBROWSEDIR, 
 wxID_VGENERATEARCHDIALOGGAGPROCESS, wxID_VGENERATEARCHDIALOGGCBBCANCEL, 
 wxID_VGENERATEARCHDIALOGGCBBOK, wxID_VGENERATEARCHDIALOGLBLDIR, 
 wxID_VGENERATEARCHDIALOGLBLFN, wxID_VGENERATEARCHDIALOGLBLSIZE, 
 wxID_VGENERATEARCHDIALOGLSTFILE, wxID_VGENERATEARCHDIALOGTXTDIR, 
 wxID_VGENERATEARCHDIALOGTXTFN, wxID_VGENERATEARCHDIALOGTXTSIZE, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vGenerateArchDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFN, 7, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFN, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsDir, 1, border=4,
              flag=wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lstFile, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gagProcess, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=8,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDir, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtDir, 3, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbBrowseDir, 2, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblSize, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtSize, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_bxsDir_Items(self.bxsDir)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGENERATEARCHDIALOG,
              name=u'vGenerateArchDialog', parent=prnt, pos=wx.Point(222, 118),
              size=wx.Size(493, 356),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vPrjDoc Archive Generate')
        self.SetClientSize(wx.Size(485, 329))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHDIALOGGCBBOK,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Ok'),
              name=u'gcbbOk', parent=self, pos=wx.Point(150, 291),
              size=wx.Size(76, 30), style=0)
        self.gcbbOk.SetMinSize(wx.Size(-1, -1))
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGENERATEARCHDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHDIALOGGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(258, 291),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGENERATEARCHDIALOGGCBBCANCEL)

        self.lstFile = wx.ListCtrl(id=wxID_VGENERATEARCHDIALOGLSTFILE,
              name=u'lstFile', parent=self, pos=wx.Point(4, 63),
              size=wx.Size(477, 203), style=wx.LC_REPORT)
        self.lstFile.SetMinSize(wx.Size(-1, -1))

        self.lblDir = wx.StaticText(id=wxID_VGENERATEARCHDIALOGLBLDIR,
              label=_(u'Directory'), name=u'lblDir', parent=self,
              pos=wx.Point(4, 29), size=wx.Size(59, 30), style=wx.ALIGN_RIGHT)
        self.lblDir.SetMinSize(wx.Size(-1, -1))

        self.txtDir = wx.TextCtrl(id=wxID_VGENERATEARCHDIALOGTXTDIR,
              name=u'txtDir', parent=self, pos=wx.Point(67, 29),
              size=wx.Size(174, 30), style=0, value=u'')
        self.txtDir.SetMinSize(wx.Size(-1, -1))

        self.lblFN = wx.StaticText(id=wxID_VGENERATEARCHDIALOGLBLFN,
              label=_(u'Filename'), name=u'lblFN', parent=self, pos=wx.Point(4,
              4), size=wx.Size(59, 21), style=wx.ALIGN_RIGHT)
        self.lblFN.SetMinSize(wx.Size(-1, -1))

        self.txtFN = wx.TextCtrl(id=wxID_VGENERATEARCHDIALOGTXTFN,
              name=u'txtFN', parent=self, pos=wx.Point(67, 4), size=wx.Size(413,
              21), style=0, value=u'')
        self.txtFN.Enable(False)
        self.txtFN.SetMinSize(wx.Size(-1, -1))

        self.cbBrowseDir = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGENERATEARCHDIALOGCBBROWSEDIR,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=_(u'Browse Dir'),
              name=u'cbBrowseDir', parent=self, pos=wx.Point(245, 29),
              size=wx.Size(115, 30), style=0)
        self.cbBrowseDir.Bind(wx.EVT_BUTTON, self.OnCbBrowseDirButton,
              id=wxID_VGENERATEARCHDIALOGCBBROWSEDIR)

        self.lblSize = wx.StaticText(id=wxID_VGENERATEARCHDIALOGLBLSIZE,
              label=_(u'Size'), name=u'lblSize', parent=self, pos=wx.Point(364,
              29), size=wx.Size(55, 30), style=wx.ALIGN_RIGHT)
        self.lblSize.SetMinSize(wx.Size(-1, -1))

        self.txtSize = wx.TextCtrl(id=wxID_VGENERATEARCHDIALOGTXTSIZE,
              name=u'txtSize', parent=self, pos=wx.Point(423, 29),
              size=wx.Size(55, 30), style=0, value=u'')
        self.txtSize.SetMinSize(wx.Size(-1, -1))

        self.gagProcess = wx.Gauge(id=wxID_VGENERATEARCHDIALOGGAGPROCESS,
              name=u'gagProcess', parent=self, pos=wx.Point(4, 270), range=100,
              size=wx.Size(477, 13), style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.gagProcess.SetMinSize(wx.Size(-1, 13))
        self.gagProcess.SetMaxSize(wx.Size(-1, 13))

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.attr={}
        self.docinfos={}
        self.prjinfos={}
        self.bGenerated=False
        self.tmplFN=None
        self.sFullName=None
        self.sDoc=''
        self.sRev=''
        self.selectedIdx=-1
        
        self.thdArch=thdArch(self)
        EVT_THREAD_ARCH_FINISHED(self,self.OnArchFin)
        EVT_THREAD_ARCH(self,self.OnArchProgress)
        
        #img=images.getBuildBitmap()
        #self.gcbbOk.SetBitmapLabel(img)
        
        #img=images.getCancelBitmap()
        #self.gcbbCancel.SetBitmapLabel(img)
        
        # setup list
        self.lstFile.InsertColumn(0,_(u'Filename'),wx.LIST_FORMAT_LEFT,280)
        self.lstFile.InsertColumn(1,_(u'Size'),wx.LIST_FORMAT_RIGHT,90)
        self.lstFile.InsertColumn(2,_(u'Date'),wx.LIST_FORMAT_LEFT,80)
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def SetFileNames(self,tmplFN,FN):
        self.tmplFN=tmplFN
        self.sFullName=FN
    def Set(self,archNode,fn,prjDN):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;prjDN:%s'%(fn,prjDN),self)
        self.bGenerated=False
        self.gagProcess.SetValue(0)
        self.archDN=''
        self.sFN=fn
        self.prjDN=prjDN
        fn=fnUtil.getFilenameRel2BaseDir(fn,prjDN)
        self.txtFN.SetValue(fn)
        #self.sFullName=fn
        self.lstFile.DeleteAllItems()
        self.archNode=archNode
        self.txtSize.SetValue('0.00 MB')
        self.gcbbOk.Enable(True)
        
    def SetDir(self,dn):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s'%(dn),self)
        self.archDN=dn
        self.txtDir.SetValue(dn)
        lstFiles=[]
        self.subDN=os.path.split(dn)[0]
        self.lstFile.DeleteAllItems()
        iOfs=len(self.subDN)
        iSize=0
        for root,dirs,files in os.walk(dn):
            for sfn in files:
                fn=root+os.path.sep+sfn
                index = self.lstFile.InsertImageStringItem(sys.maxint, '.'+fn[iOfs:], -1)
                mode=os.stat(fn)
                iSize=iSize+mode.st_size
                size="%10.2f kB"%(mode.st_size/1024.0)
                self.lstFile.SetStringItem(index, 1, size)
        size="%10.2f MB"%(iSize/(1024.0*1024.0))
        self.txtSize.SetValue(size)
        self.iSize=iSize
    def GetDir(self):
        return self.archDN
    def OnArchProgress(self,evt):
        self.gagProcess.SetValue(evt.GetValue())
    def OnArchFin(self,evt):
        self.gagProcess.SetValue(0)
        self.bGenerated=True
        self.gcbbOk.Enable(True)
        self.EndModal(1)
    def Generate(self):
        if self.thdArch.IsRunning():
            return
        self.gagProcess.SetValue(0)
        self.gagProcess.SetRange(self.iSize/(1024.0*1024.0))
        self.gcbbOk.Enable(False)
        self.thdArch.Arch(self.archDN,self.sFN)
    def OnGcbbOkButton(self, event):
        if self.bGenerated==False:
            if self.thdArch.IsRunning()==False:
                self.Generate()
        else:
            self.EndModal(1)
        #self.Show(False)
        #self.Close()
        event.Skip()
    
    def OnGcbbCancelButton(self, event):
        if self.thdArch.IsRunning():
            self.thdArch.Stop()
            return
        self.EndModal(0)
        #self.Close()
        event.Skip()

    def OnCbBrowseDirButton(self, event):
        dlg = wx.DirDialog(self, _(u"Choose the directory to archive:"),
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.archDN is not None:
                dlg.SetPath(self.archDN)
            if dlg.ShowModal() == wx.ID_OK:
                dn = dlg.GetPath()
                self.SetDir(dn)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        
        event.Skip()
