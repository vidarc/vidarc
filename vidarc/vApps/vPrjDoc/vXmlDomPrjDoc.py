#----------------------------------------------------------------------------
# Name:         vXmlPrjDoc.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlDomPrjDoc.py,v 1.4 2014/10/01 09:02:28 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDom import vtXmlDom


class vXmlPrjDoc(vtXmlDom):
    def __init__(self,attr='id',skip=[],synch=False,verbose=0):
        for s in ['prjinfo','config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDom.__init__(self,attr,skip,synch,verbose)
        self.nodes2synch.append('prjinfo')
        
        self.verbose=verbose
    def IsSkip(self,node):
        if vtXmlDom.IsSkip(self,node):
            return True
        if self.getTagName(node) not in ['docgroup']:
            return True
        return False
    def __genIds__(self,c,node,ids):
        sTag=self.getTagName(c)
        if sTag in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        elif sTag in ['docgroup','document','archivetmpl','archive']:
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'prjdocs')
    def New(self,revision='1.0',root='projectdocuments',bConnection=False):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'root:%s'%root)
        vtXmlDom.New(self,revision,root)
        elem=self.createSubNode(self.getRoot(),'prjdocs')
        #self.setAttribute(elem,self.attr,'')
        #self.checkId(elem)
        #self.processMissingId()
        #self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #print self.doc
        self.AlignDoc()
        
