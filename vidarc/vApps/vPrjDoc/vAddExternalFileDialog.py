#Boa:Dialog:vAddExternalFileDialog
#----------------------------------------------------------------------------
# Name:         vAddExternalFileDialog.py
# Purpose:      add an external file to vPrjDoc
#
# Author:       Walter Obweger
#
# Created:      20070118
# CVS-ID:       $Id: vAddExternalFileDialog.py,v 1.7 2012/09/09 14:36:41 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    import vidarc.vApps.vDoc.vXmlDocTree
    from vidarc.tool.xml.vtXmlTree import *
    
    from vGuiFrmWX import vGuiFrmWX
    import os,os.path,stat

    import vidarc.tool.InOut.fnUtil as fnUtil
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

def create(parent):
    return vAddExternalFileDialog(parent)

[wxID_VADDEXTERNALFILEDIALOG, wxID_VADDEXTERNALFILEDIALOGCANCEL, 
 wxID_VADDEXTERNALFILEDIALOGCBCOPY, wxID_VADDEXTERNALFILEDIALOGCBMOVE, 
 wxID_VADDEXTERNALFILEDIALOGCBSUBDIRCLR, wxID_VADDEXTERNALFILEDIALOGCBSUBDIRSEARCH,
 wxID_VADDEXTERNALFILEDIALOGCBSUBDIRDATE, 
 wxID_VADDEXTERNALFILEDIALOGCBSUBDIRNUM, wxID_VADDEXTERNALFILEDIALOGLBLDESTFN, 
 wxID_VADDEXTERNALFILEDIALOGLBLDOCGRP, wxID_VADDEXTERNALFILEDIALOGLBLPRJDN, 
 wxID_VADDEXTERNALFILEDIALOGLBLRELDN, wxID_VADDEXTERNALFILEDIALOGLBLSRCFN, 
 wxID_VADDEXTERNALFILEDIALOGSPNSUBDIR, wxID_VADDEXTERNALFILEDIALOGTRDOC, 
 wxID_VADDEXTERNALFILEDIALOGTXTDESTFN, wxID_VADDEXTERNALFILEDIALOGTXTPRJDN, 
 wxID_VADDEXTERNALFILEDIALOGTXTRELDN, wxID_VADDEXTERNALFILEDIALOGTXTSRCFN, 
 wxID_VADDEXTERNALFILEDIALOGTXTSUBDIR, 
] = [wx.NewId() for _init_ctrls in range(20)]

class vAddExternalFileDialog(wx.Dialog,vtXmlDomConsumer):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCopy, 0, border=0, flag=0)
        parent.AddWindow(self.cbMove, 0, border=8, flag=wx.LEFT | wx.RIGHT)
        parent.AddWindow(self.Cancel, 0, border=8, flag=wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSrcFN, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtSrcFN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblDocGrp, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.trDoc, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsSubDir, 0, border=4,
              flag=wx.ALIGN_LEFT | wx.EXPAND)
        parent.AddWindow(self.lblPrjDN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.txtPrjDN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblRelDN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtRelDN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblDestFN, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtDestFN, 0, border=4,
              flag=wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.TOP)

    def _init_coll_bxsSubDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spnSubDir, 1, border=0,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.txtSubDir, 2, border=0,
              flag=wx.GROW | wx.EXPAND | wx.TOP)
        parent.AddWindow(self.cbSubDirDate, 0, border=0, flag=0)
        parent.AddWindow(self.cbSubDirNum, 0, border=0, flag=0)
        parent.AddWindow(self.cbSubDirClr, 0, border=0, flag=0)
        parent.AddWindow(self.cbSubDirSearch, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=11, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSubDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsSubDir_Items(self.bxsSubDir)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VADDEXTERNALFILEDIALOG,
              name=u'vAddExternalFileDialog', parent=prnt, pos=wx.Point(199,
              146), size=wx.Size(299, 383), style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vPrjDoc Add External File Dialog'))
        self.SetClientSize(wx.Size(291, 356))

        self.trDoc = vidarc.vApps.vDoc.vXmlDocTree.vXmlDocTree(controller=False,
              id=wxID_VADDEXTERNALFILEDIALOGTRDOC, master=False, name=u'trDoc',
              parent=self, pos=wx.Point(4, 55), size=wx.Size(283, 123),
              style=0)
        self.trDoc.SetMinSize(wx.Size(-1, -1))
        self.trDoc.Bind(vEVT_VTXMLTREE_ITEM_SELECTED,
              self.OnTrDocVtxmltreeItemSelected,
              id=wxID_VADDEXTERNALFILEDIALOGTRDOC)

        self.lblSrcFN = wx.StaticText(id=wxID_VADDEXTERNALFILEDIALOGLBLSRCFN,
              label=_(u'source filename'), name=u'lblSrcFN', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(283, 13), style=0)
        self.lblSrcFN.SetMinSize(wx.Size(-1, -1))

        self.txtSrcFN = wx.TextCtrl(id=wxID_VADDEXTERNALFILEDIALOGTXTSRCFN,
              name=u'txtSrcFN', parent=self, pos=wx.Point(4, 17),
              size=wx.Size(283, 21), style=0, value=u'')
        self.txtSrcFN.Enable(False)
        self.txtSrcFN.SetMinSize(wx.Size(-1, -1))

        self.lblDocGrp = wx.StaticText(id=wxID_VADDEXTERNALFILEDIALOGLBLDOCGRP,
              label=_(u'document group'), name=u'lblDocGrp', parent=self,
              pos=wx.Point(4, 42), size=wx.Size(283, 13), style=0)

        self.lblRelDN = wx.StaticText(id=wxID_VADDEXTERNALFILEDIALOGLBLRELDN,
              label=u'relative directory', name=u'lblRelDN', parent=self,
              pos=wx.Point(4, 246), size=wx.Size(283, 13), style=0)
        self.lblRelDN.SetMinSize(wx.Size(-1, -1))

        self.txtRelDN = wx.TextCtrl(id=wxID_VADDEXTERNALFILEDIALOGTXTRELDN,
              name=u'txtRelDN', parent=self, pos=wx.Point(4, 259),
              size=wx.Size(283, 21), style=0, value=u'')
        self.txtRelDN.SetMinSize(wx.Size(-1, -1))
        self.txtRelDN.Enable(False)

        self.lblDestFN = wx.StaticText(id=wxID_VADDEXTERNALFILEDIALOGLBLDESTFN,
              label=_(u'destination filename'), name=u'lblDestFN', parent=self,
              pos=wx.Point(4, 280), size=wx.Size(283, 13), style=0)
        self.lblDestFN.SetMinSize(wx.Size(-1, -1))

        self.txtDestFN = wx.TextCtrl(id=wxID_VADDEXTERNALFILEDIALOGTXTDESTFN,
              name=u'txtDestFN', parent=self, pos=wx.Point(4, 293),
              size=wx.Size(283, 21), style=0, value=u'')
        self.txtDestFN.Enable(False)
        self.txtDestFN.SetMinSize(wx.Size(-1, -1))

        self.lblPrjDN = wx.StaticText(id=wxID_VADDEXTERNALFILEDIALOGLBLPRJDN,
              label=_(u'project directory'), name=u'lblPrjDN', parent=self,
              pos=wx.Point(4, 212), size=wx.Size(283, 13), style=0)
        self.lblPrjDN.SetMinSize(wx.Size(-1, -1))

        self.txtPrjDN = wx.TextCtrl(id=wxID_VADDEXTERNALFILEDIALOGTXTPRJDN,
              name=u'txtPrjDN', parent=self, pos=wx.Point(4, 225),
              size=wx.Size(283, 21), style=0, value=u'')
        self.txtPrjDN.Enable(False)
        self.txtPrjDN.SetMinSize(wx.Size(-1, -1))

        self.cbCopy = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VADDEXTERNALFILEDIALOGCBCOPY, label=_(u'Copy'),
              name=u'cbCopy', parent=self, pos=wx.Point(19, 322),
              size=wx.Size(76, 30), style=0)
        self.cbCopy.SetMinSize(wx.Size(-1, -1))
        self.cbCopy.Bind(wx.EVT_BUTTON, self.OnCbCopyButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBCOPY)

        self.cbMove = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Move),
              id=wxID_VADDEXTERNALFILEDIALOGCBMOVE, label=_(u'Move'),
              name=u'cbMove', parent=self, pos=wx.Point(103, 322),
              size=wx.Size(76, 30), style=0)
        self.cbMove.SetMinSize(wx.Size(-1, -1))
        self.cbMove.Bind(wx.EVT_BUTTON, self.OnCbMoveButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBMOVE)

        self.Cancel = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VADDEXTERNALFILEDIALOGCANCEL, label=_(u'Cancel'),
              name=u'Cancel', parent=self, pos=wx.Point(195, 322),
              size=wx.Size(76, 30), style=0)
        self.Cancel.SetMinSize(wx.Size(-1, -1))
        self.Cancel.Bind(wx.EVT_BUTTON, self.OnCancelButton,
              id=wxID_VADDEXTERNALFILEDIALOGCANCEL)

        self.spnSubDir = wx.SpinCtrl(id=wxID_VADDEXTERNALFILEDIALOGSPNSUBDIR,
              initial=-1, max=99, min=-2, name=u'spnSubDir', parent=self,
              pos=wx.Point(0, 178), size=wx.Size(66, 30),
              style=wx.SP_ARROW_KEYS)
        self.spnSubDir.Bind(wx.EVT_TEXT, self.OnSpnSubDirText,
              id=wxID_VADDEXTERNALFILEDIALOGSPNSUBDIR)
        self.spnSubDir.Bind(wx.EVT_SPIN, self.OnSpnSubDirSpin,
              id=wxID_VADDEXTERNALFILEDIALOGSPNSUBDIR)

        self.txtSubDir = wx.TextCtrl(id=wxID_VADDEXTERNALFILEDIALOGTXTSUBDIR,
              name=u'txtSubDir', parent=self, pos=wx.Point(66, 178),
              size=wx.Size(132, 30), style=0, value=u'img')
        self.txtSubDir.Bind(wx.EVT_TEXT, self.OnTxtSubDirText,
              id=wxID_VADDEXTERNALFILEDIALOGTXTSUBDIR)

        self.cbSubDirDate = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Calendar),
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRDATE, name=u'cbSubDirDate',
              parent=self, pos=wx.Point(198, 178), size=wx.Size(31, 30),
              style=0)
        self.cbSubDirDate.Bind(wx.EVT_BUTTON, self.OnCbSubDirDateButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRDATE)

        self.cbSubDirNum = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Calc),
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRNUM, name=u'cbSubDirNum',
              parent=self, pos=wx.Point(229, 178), size=wx.Size(31, 30),
              style=0)
        self.cbSubDirNum.Bind(wx.EVT_BUTTON, self.OnCbSubDirNumButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRNUM)

        self.cbSubDirClr = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Clear),
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRCLR, name=u'cbSubDirClr',
              parent=self, pos=wx.Point(260, 178), size=wx.Size(31, 30),
              style=0)
        self.cbSubDirClr.Bind(wx.EVT_BUTTON, self.OnCbSubDirClrButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRCLR)

        self.cbSubDirSearch = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Search),
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRSEARCH, name=u'cbSubDirSearch',
              parent=self, pos=wx.Point(260, 178), size=wx.Size(31, 30),
              style=0)
        self.cbSubDirSearch.Bind(wx.EVT_BUTTON, self.OnCbSubDirSearchButton,
              id=wxID_VADDEXTERNALFILEDIALOGCBSUBDIRSEARCH)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        self.spnSubDir.SetValue(-1)
        self.dtNow=vtDateTime(bLocal=True)
        
        self.dlgSearch=None
        
        self.Layout()
        self.Clear()
        self.widLogging=vtLog.GetPrintMsgWid(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomConsumer.__del__(self)
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.idDocGrp=-1
        self.sFN=''
    def IsBusy(self):
        return self.IsShown()
    def Stop(self):
        try:
            if self.IsBusy():
                self.EndModal(-1)
        except:
            vtLog.vtLngTB(__name__)
    def SetNetDocs(self,d):
        if 'vDoc' in d:
            dd=d['vDoc']
            self.trDoc.SetDoc(dd['doc'],dd['bNet'])
            self.docDoc=dd['doc']
        if 'vPrjDoc' in d:
            dd=d['vPrjDoc']
            self.SetDoc(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'doc;bNet:%d'%(bNet),self)
            vtXmlDomConsumer.SetDoc(self,doc)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetLanguage(self,lang):
        try:
            self.trDoc.SetLanguage(lang)
            self.trDoc.SetNode(None)
        except:
            vtLog.vtLngTB(self.GetName())
    def __updateCompleteFN(self):
        t=self.doc.SplitFN(self.idDocGrp,self.sFN)
        if t is not None:
            self.sTitle=t[1]
            self.sRev=t[2]
            self.sExt=t[3]
        self.__updateFN()
    def __updateFN(self):
        sDN,sFN=self.doc.GenDocGrpExternalFN(self.idDocGrp,self.sFN)
        bDbg=False
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
        if sDN is None:
            self.txtPrjDN.SetValue('')
        else:
            self.txtPrjDN.SetValue(sDN)
        if sFN is None:
            self.txtRelDN.SetValue('')
            self.txtDestFN.SetValue('')
        else:
            sRelDN,sRelFN=os.path.split(sFN)
            iSubDirNum=self.spnSubDir.GetValue()
            sSubDirName=self.txtSubDir.GetValue()
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'iSubDirNum;%r;%r'%(iSubDirNum,type(iSubDirNum)),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'sSubDirName;%r;%r'%(sSubDirName,type(sSubDirName)),self)
            if len(sSubDirName)<=0:
                sSubDirName=None
            sSubDir=None
            if iSubDirNum==-2:
                # no sub dir
                sSubDir=sSubDirName
            elif iSubDirNum==-1:
                # no sub dir
                sSubDir=None #sSubDirName
            else:
                if sSubDirName is not None:
                    sSubDir="%02d_%s"%(iSubDirNum,sSubDirName)
            #print iSubDirNum,sSubDirName
            #print sSubDir
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'sSubDir;%r;%r'%(sSubDir,type(sSubDir)),self)
            if sSubDir is not None:
                sRelDN=u"/".join([sRelDN,sSubDir])
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'sRelDN;%r;%r'%(sRelDN,type(sRelDN)),self)
            self.txtRelDN.SetValue(sRelDN)
            self.txtDestFN.SetValue(sRelFN)
    def SetFN(self,fn):
        try:
            self.sFN=os.path.split(fn)[1]
            self.txtSrcFN.SetValue(fn)
            self.__updateCompleteFN()
            return 0
        except:
            vtLog.vtLngTB(self.GetName())
            #self.EndModal(-2)
            return -1
    def GetFN(self):
        sDN=self.txtPrjDN.GetValue()
        sRelDN=self.txtRelDN.GetValue()
        sRelFN=self.txtDestFN.GetValue()
        sFN=os.path.join(sRelDN,sRelFN)
        sFullFN=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,sDN)
        return sFullFN
    def OnCbCopyButton(self, event):
        event.Skip()
        self.EndModal(1)
    def OnCbMoveButton(self, event):
        event.Skip()
        self.EndModal(2)
    def OnCancelButton(self, event):
        event.Skip()
        self.EndModal(-1)
    def OnTrDocVtxmltreeItemSelected(self, event):
        event.Skip()
        try:
            node=event.GetTreeNodeData()
            if (node is None) or (self.doc is None):
                self.idDocGrp=-1
            else:
                self.idDocGrp=self.doc.getKeyNum(node)
                self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetSelDoc(self):
        try:
            node=self.trDoc.GetSelected()
            if self.docDoc.IsNodeKeyValid(node):
                return node
        except:
            vtLog.vtLngTB(self.GetName())
        return None

    def OnCbSubDirDateButton(self, event):
        event.Skip()
        try:
            self.spnSubDir.SetValue(-2)
            self.dtNow.Now()
            self.txtSubDir.SetValue(self.dtNow.GetDateSmallStr())
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSubDirNumButton(self, event):
        event.Skip()
        try:
            self.spnSubDir.SetValue(0)
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSubDirClrButton(self, event):
        event.Skip()
        try:
            self.spnSubDir.SetValue(-1)
            self.txtSubDir.SetValue(u"")
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSearchApply(self,evt):
        try:
            sCmd=evt.GetCmd()
            if sCmd=='ok':
                pass
            else:
                return
            l=self.dlgSearch.lstData.GetSelected()
            sSel=l[0][1]
            lSel=sSel.split('_')
            try:
                iSubDirNum=int(lSel[0])
                sSubDirName=u'_'.join(lSel[1:])
            except:
                iSubDirNum=-2
                sSubDirName=sSel
            self.spnSubDir.SetValue(iSubDirNum)
            self.txtSubDir.SetValue(sSubDirName)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSubDirSearchButton(self,event):
        event.Skip()
        try:
            if self.dlgSearch is None:
                self.dlgSearch=vGuiFrmWX(name='dlgSearch',parent=self,kind='popup',
                    iArrange=0,widArrange=self.cbSubDirSearch,bAnchor=True,
                    lGrowRow=[(0,1),(1,1)],
                    lGrowCol=[(0,1),(1,1)],
                    lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstData','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'name'),-1,-1,1],
                                    [_(u'type'),-1,80,1],
                                    ],
                                'map':[('name',1),('result',2)],
                                'tip':_(u'action'),
                                },None),
                        ])
                self.dlgSearch.BindEvent('cmd',self.OnSearchApply)
            if self.dlgSearch is None:
                return
            sDN=self.txtPrjDN.GetValue()
            sRelDN=self.txtRelDN.GetValue()
            sRelFN=self.txtDestFN.GetValue()
            sFN=os.path.join(sRelDN,sRelFN)
            #sFullFN=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,sDN)
            sFullDN=fnUtil.getAbsoluteFilenameRel2BaseDir(sRelDN,sDN)
            l=os.listdir(sFullDN)
            ll=[]
            for sFN in l:
                sFullFN = os.path.join(sFullDN, sFN)
                mode = os.stat(sFullFN)[stat.ST_MODE]
                if stat.S_ISDIR(mode):
                    ll.append([sFN,[sFN,_('directory')]])
                elif stat.S_ISREG(mode):
                    ll.append([sFN,[sFN,_('file')]])
            self.dlgSearch.lstData.SetValue(ll)
            self.dlgSearch.Show()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSpnSubDirText(self, event):
        event.Skip()
        try:
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSpnSubDirSpin(self, event):
        event.Skip()
        try:
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTxtSubDirText(self, event):
        event.Skip()
        try:
            self.__updateCompleteFN()
        except:
            vtLog.vtLngTB(self.GetName())
