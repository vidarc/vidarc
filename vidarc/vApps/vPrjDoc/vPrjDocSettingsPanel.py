#Boa:FramePanel:vPrjDocSettingsPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocSettingsPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocSettingsPanel.py,v 1.4 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjDoc.vDocSelPanel import MODES

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VPRJDOCSETTINGSPANEL, wxID_VPRJDOCSETTINGSPANELCBBROWSELATEXFN, 
 wxID_VPRJDOCSETTINGSPANELCHCMODE, wxID_VPRJDOCSETTINGSPANELGCBBBROWSEPRJDN, 
 wxID_VPRJDOCSETTINGSPANELGCBBBROWSETMPLDN, 
 wxID_VPRJDOCSETTINGSPANELLBLBASETMPLDN, wxID_VPRJDOCSETTINGSPANELLBLDUMMY, 
 wxID_VPRJDOCSETTINGSPANELLBLMODE, wxID_VPRJDOCSETTINGSPANELLBLPRJDN, 
 wxID_VPRJDOCSETTINGSPANELLBLTEXFN, wxID_VPRJDOCSETTINGSPANELTXTLATEXFN, 
 wxID_VPRJDOCSETTINGSPANELTXTPRJDN, wxID_VPRJDOCSETTINGSPANELTXTTMPLDN, 
] = [wx.NewId() for _init_ctrls in range(13)]

#----------------------------------------------------------------------
def getBrowseData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02zIDAT8\x8d\x85\x93?hSQ\x14\xc6\x7f\xf7\xf5!\xb7\xf0\x8a\t\xb4\xf0\
\x82\x0e\xbe\xaa`\x86\x0e\xaf\xb84\x10\xc4H\x15\n\xae-M\xc1\xc1E\xb0\x8bC\
\xb7 \xb6\x12\x14D\xc5\xc1\x16\xfc\x87S\x04\x15\x1d\x04+RM\x86B:\x14\x92j$)8\
\xbc\xc1\xc0{h\xa1o\x08\xe4\x82\x8f^\x874i\x0b\xa2\x17\xbe\xe9p~\xf7p\xce\
\xf7!\x8c>\xbaJ\x9eJj\xc8k\xc8k\xf7\xb4\xab!\xaf\xf7\xd7\xff&\x83}o&;\x83\
\xfe\x9d#\x7f\x13\xeeN\xa6x\xff\xd8\xe1\xf2\xd4\x19\xadw\x164\x80\xdeY\xd0\
\x1d\xad\xean\x8f\xd9-\n\xe3\x86\x80m0\x15\xb9\xeb9\x00\x1a\x1b\r&\x06\xc7\
\x98u\x8b\x14\xd6\xa7\xf4\xdct\x19\x19sY\xfc\x90\xe7M\xe9\x9a\xde\xa8<\x10}\
\x90\x99\xd7\xed\x1c\xe6\xa1/\xf3\xd2J0\xec\x98XV\x08\xf43d\x0f\x91L\x9eG\
\x1c\xbf\x00\xd6\x00\xbf~\x06\x98\xd2$y8b\xf4\xc8\x10\x85O\xb7\xe7\x05\x94\
\xf5\xe7w\xdb\xa4\xcef\x90Q\x15\xaf\xea\x11\xc6}bV\x12\xe7D\x1c\xb0\x81\x04 \
\x01(\xae\x14\xf1\xbe\xbd\xc0\x19t8w\t\x0ca\xa4\xc5Z\xa9\x80\x0cC\x88<\x9c\
\x11\x1b\xf7\xa8K\x828k\xaf+\x14KU\xc2\xad2\xe0\x01\x8a\xccx\x86\xa0\xe5P\
\xadU\x80Lg\x89\xc5\r\x05RB\x04D\x014\x03d\xe436\x92 \xe5H\xbc\xcd5\xde>_\
\x04\x1a@\x80\xf7\xc3\xa3\xb8\xa9\x10FZt\x00\xa5Qh5 \x94\x10\xb4\tb\x12\xa5$\
(E\xb8\xe5\x93\x94\xe0\x9eL\x11\x04\n\xa8\xe3\x07>\x8d\xef\t\x80\xee\x193,\
\xaf\x14@)\x14\x10S\nIH\x10\xb5\xb1M\x08\xb1q,E\xbdV$\x08B\xa4%\xf16\x9d=\
\x800\xd2\xa2\xb8\xee\xf5\xfc [\x1eD\x1e1\x13\x14`\x9b\xfd(\xda@\x05\xbf\xb9\
Lc\xbd\xbc{v\xf6\x8ct\xefI\n\x800\x02L\x1be\xda\x1d\x18\xfd\x04Q\x1bd\x1c\
\xa5Rx^\x19\x19K\xf4>\xdb\xe7\xc4\x0csK\x05\xa4\nP\xbb\x8d\x00\x8a61\x13P\
\xdb$\xc2\x06^\xb33\xe9\xd5+\xaf\xf4\xb1\xe1\xa6\xee\x01\x84\x91\x16\xf7\x9f\
}\x14\xf14\xe4\xef,\x12\xb6\xbc\xde\x04D\n\x00_I\x1a\xb5\x19\xe4\x80DZe&\xc6\
\xcb\x07\xb3\xd0\xb5\xf4\xad\x97_E<\rs\xbb\xa0\x10\t\x91"\xb4\x14\xf5Z\x1d\
\xc7vP\xad\x14K\x8f&\xc5?\x93&\x8c> \xafg\xa7]\xed\xaf>\xd4\xf5\xe5\x9c\x96R\
\xea\xec\xf4\xd3^J\xff\x0b\xd8\x0f\xca^t\xb5mg\x0fD\xfc\x0f\xe7R\x0f5\x91\
\xbd\xa0@\x00\x00\x00\x00IEND\xaeB`\x82' 

def getBrowseBitmap():
    return wx.BitmapFromImage(getBrowseImage())

def getBrowseImage():
    stream = cStringIO.StringIO(getBrowseData())
    return wx.ImageFromStream(stream)


class vPrjDocSettingsPanel(wx.Panel):
    def _init_coll_bxsTmplLaTex_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTexFn, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtLatexFN, 3, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbBrowseLatexFn, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrjDn, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsMode, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsTmplDN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsTmplLaTex, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTmplDN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblBaseTmplDN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtTmplDN, 3, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.gcbbBrowseTmplDN, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsMode_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMode, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcMode, 2, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lblDummy, 2, border=0,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsPrjDn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjDN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPrjDN, 3, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.gcbbBrowsePrjDN, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)

        self.bxsPrjDn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMode = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplLaTex = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplDN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsPrjDn_Items(self.bxsPrjDn)
        self._init_coll_bxsMode_Items(self.bxsMode)
        self._init_coll_bxsTmplLaTex_Items(self.bxsTmplLaTex)
        self._init_coll_bxsTmplDN_Items(self.bxsTmplDN)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCSETTINGSPANEL,
              name=u'vPrjDocSettingsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(488, 157), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(480, 130))

        self.lblPrjDN = wx.StaticText(id=wxID_VPRJDOCSETTINGSPANELLBLPRJDN,
              label=_(u'Project Directory'), name=u'lblPrjDN', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(96, 30), style=wx.ALIGN_RIGHT)
        self.lblPrjDN.SetMinSize(wx.Size(-1, -1))

        self.txtPrjDN = wx.TextCtrl(id=wxID_VPRJDOCSETTINGSPANELTXTPRJDN,
              name=u'txtPrjDN', parent=self, pos=wx.Point(100, 0),
              size=wx.Size(280, 30), style=0, value=u'')
        self.txtPrjDN.SetMinSize(wx.Size(-1, -1))

        self.gcbbBrowsePrjDN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCSETTINGSPANELGCBBBROWSEPRJDN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Browse'),
              name=u'gcbbBrowsePrjDN', parent=self, pos=wx.Point(384, 0),
              size=wx.Size(96, 30), style=0)
        self.gcbbBrowsePrjDN.SetMinSize(wx.Size(-1, -1))
        self.gcbbBrowsePrjDN.Bind(wx.EVT_BUTTON, self.OnGcbbPrjDNBrowseButton,
              id=wxID_VPRJDOCSETTINGSPANELGCBBBROWSEPRJDN)

        self.lblMode = wx.StaticText(id=wxID_VPRJDOCSETTINGSPANELLBLMODE,
              label=_(u'Mode'), name=u'lblMode', parent=self, pos=wx.Point(0,
              34), size=wx.Size(96, 21), style=wx.ALIGN_RIGHT)
        self.lblMode.SetMinSize(wx.Size(-1, -1))

        self.chcMode = wx.Choice(choices=[],
              id=wxID_VPRJDOCSETTINGSPANELCHCMODE, name=u'chcMode', parent=self,
              pos=wx.Point(100, 34), size=wx.Size(184, 21), style=0)
        self.chcMode.SetMinSize(wx.Size(-1, -1))

        self.lblDummy = wx.StaticText(id=wxID_VPRJDOCSETTINGSPANELLBLDUMMY,
              label=u'', name=u'lblDummy', parent=self, pos=wx.Point(288, 34),
              size=wx.Size(192, 21), style=0)
        self.lblDummy.SetMinSize(wx.Size(-1, -1))

        self.lblBaseTmplDN = wx.StaticText(id=wxID_VPRJDOCSETTINGSPANELLBLBASETMPLDN,
              label=_(u'Base Template Dir'), name=u'lblBaseTmplDN', parent=self,
              pos=wx.Point(0, 59), size=wx.Size(96, 30), style=wx.ALIGN_RIGHT)
        self.lblBaseTmplDN.SetMinSize(wx.Size(-1, -1))

        self.txtTmplDN = wx.TextCtrl(id=wxID_VPRJDOCSETTINGSPANELTXTTMPLDN,
              name=u'txtTmplDN', parent=self, pos=wx.Point(100, 59),
              size=wx.Size(280, 30), style=0, value=u'')
        self.txtTmplDN.SetMinSize(wx.Size(-1, -1))

        self.gcbbBrowseTmplDN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCSETTINGSPANELGCBBBROWSETMPLDN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Browse'),
              name=u'gcbbBrowseTmplDN', parent=self, pos=wx.Point(384, 59),
              size=wx.Size(96, 30), style=0)
        self.gcbbBrowseTmplDN.SetMinSize(wx.Size(-1, -1))
        self.gcbbBrowseTmplDN.Bind(wx.EVT_BUTTON, self.OnGcbbTmplDNBrowseButton,
              id=wxID_VPRJDOCSETTINGSPANELGCBBBROWSETMPLDN)

        self.lblTexFn = wx.StaticText(id=wxID_VPRJDOCSETTINGSPANELLBLTEXFN,
              label=_(u'Latex Template'), name=u'lblTexFn', parent=self,
              pos=wx.Point(0, 93), size=wx.Size(96, 30), style=wx.ALIGN_RIGHT)
        self.lblTexFn.SetMinSize(wx.Size(-1, -1))

        self.txtLatexFN = wx.TextCtrl(id=wxID_VPRJDOCSETTINGSPANELTXTLATEXFN,
              name=u'txtLatexFN', parent=self, pos=wx.Point(100, 93),
              size=wx.Size(280, 30), style=0, value=u'')
        self.txtLatexFN.SetMinSize(wx.Size(-1, -1))

        self.cbBrowseLatexFn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCSETTINGSPANELCBBROWSELATEXFN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Browse'),
              name=u'cbBrowseLatexFn', parent=self, pos=wx.Point(384, 93),
              size=wx.Size(96, 30), style=0)
        self.cbBrowseLatexFn.SetMinSize(wx.Size(-1, -1))
        self.cbBrowseLatexFn.Bind(wx.EVT_BUTTON, self.OnGcbbLatexFnBrowseButton,
              id=wxID_VPRJDOCSETTINGSPANELCBBROWSELATEXFN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        self.tmplDN=None
        self.latexFN=None
        self.prjDN=None
        self.bModified=False
        
        img=getBrowseBitmap()
        
        self.gcbbBrowseTmplDN.SetBitmapLabel(img)
        self.cbBrowseLatexFn.SetBitmapLabel(img)
        self.gcbbBrowsePrjDN.SetBitmapLabel(img)
        
        for m in MODES:
            self.chcMode.Append(m)
        self.chcMode.SetSelection(0)
        self.Move(pos)
        self.SetSize(size)
        
    def __setModified__(self):
        self.bModified=True
    def SetFileLatex(self,fn):
        self.latexFN=fn
        self.txtLatexFN.SetValue(self.latexFN)
        self.__setModified__()
    def SetDirTmpl(self,dn):
        self.tmplDN=dn
        self.txtTmplDN.SetValue(self.tmplDN)
        self.__setModified__()
    def SetDirPrj(self,dn):
        self.prjDN=dn
        self.txtPrjDN.SetValue(self.prjDN)
        self.__setModified__()
    def SetMode(self,mode):
        try:
            idx=MODES.index(mode)
            self.chcMode.SetSelection(idx)
        except:
            self.chcMode.SetSelection(0)
        self.__setModified__()
    def SetXml(self,doc,node):
        self.SetDirTmpl(doc.getNodeText(node,'templatedn'))
        self.SetFileLatex(doc.getNodeText(node,'latexfn'))
        self.SetDirPrj(doc.getNodeText(node,'projectdn'))
        self.SetMode(doc.getNodeText(node,'mode'))
    def GetXml(self,doc,node):
        doc.setNodeText(node,'templatedn',self.txtTmplDN.GetValue())
        doc.setNodeText(node,'latexfn',self.txtLatexFN.GetValue())
        doc.setNodeText(node,'projectdn',self.txtPrjDN.GetValue())
        m=self.chcMode.GetString(self.chcMode.GetSelection())
        doc.setNodeText(node,'mode',m)
    def OnGcbbLatexFnBrowseButton(self,event):
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("TEX files (*.tex)|*.tex|all files (*.*)|*.*"), wx.OPEN)
        try:
            if self.latexFN is not None:
                dlg.SetDirectory(self.latexFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileLatex(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
        
    def OnGcbbTmplDNBrowseButton(self, event):
        #'open'
        dlg = wx.DirDialog(self, _("Choose the template directory:"),
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.tmplDN is not None:
                dlg.SetPath(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetDirTmpl(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbPrjDNBrowseButton(self, event):
        #'open'
        dlg = wx.DirDialog(self, _("Choose the project directory:"),
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.prjDN is not None:
                dlg.SetPath(self.prjDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetDirPrj(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
