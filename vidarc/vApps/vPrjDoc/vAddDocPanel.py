#Boa:FramePanel:vAddDocPanel
#----------------------------------------------------------------------------
# Name:         vAddDocPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vAddDocPanel.py,v 1.2 2006/01/08 15:12:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import  wx.lib.anchors as anchors
from vidarc.vApps.vPrjDoc.vPrjDocGrpInfoNoteBook import *

[wxID_VADDDOCPANEL, wxID_VADDDOCPANELPANEL1, wxID_VADDDOCPANELPANEL2, 
 wxID_VADDDOCPANELSPWINFO, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vAddDocPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VADDDOCPANEL, name=u'vAddDocPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(600, 300),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(592, 273))

        self.spwInfo = wx.SplitterWindow(id=wxID_VADDDOCPANELSPWINFO,
              name=u'spwInfo', parent=self, point=wx.Point(8, 8),
              size=wx.Size(600, 300), style=wx.SP_3D|wx.SP_LIVE_UPDATE)

        self.panel1 = wx.Panel(id=wxID_VADDDOCPANELPANEL1, name='panel1',
              parent=self.spwInfo, pos=wx.Point(2, 2), size=wx.Size(596, 198),
              style=wx.TAB_TRAVERSAL)

        self.panel2 = wx.Panel(id=wxID_VADDDOCPANELPANEL2, name='panel2',
              parent=self.spwInfo, pos=wx.Point(2, 207), size=wx.Size(596, 91),
              style=wx.TAB_TRAVERSAL)
        self.spwInfo.SplitHorizontally(self.panel1, self.panel2, 200)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)

        self.vglbDocGrpInfo=vglbDocGrpInfo(self.spwInfo,wx.NewId(),
                pos=(0,0),size=(190,340),style=0,name="vglbDocGrpInfo")
        #self.vglbDocGrpInfo.SetConstraints(
        #        anchors.LayoutAnchors(self.vglbDocGrpInfo, True, True, True, True)
        #        )
        #EVT_VGPDOC_GRP_INFO_CHANGED(self.vglbDocGrpInfo,self.OnDocGrpInfoChanged)
        #EVT_VGPDOC_CHANGED(self.vglbDocGrpInfo,self.OnDocChanged)
        self.spwInfo.ReplaceWindow(self.panel1,self.vglbDocGrpInfo)
        self.panel1.Destroy()
        #self.vglbDocGrpInfo.SetLanguages(self.languages,self.languageIds)
        
