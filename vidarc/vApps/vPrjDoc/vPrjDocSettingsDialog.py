#Boa:Dialog:vPrjDocSettingsDialog
#----------------------------------------------------------------------------
# Name:         vPrjDocSettingsDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjDocSettingsDialog.py,v 1.4 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO
from vidarc.vApps.vPrjDoc.vPrjDocSettingsPanel import *
from vidarc.tool.xml.vtXmlNodeDialog import *

import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vPrjDocSettingsDialog(parent)

[wxID_VPRJDOCSETTINGSDIALOG, wxID_VPRJDOCSETTINGSDIALOGGCBBCANCEL, 
 wxID_VPRJDOCSETTINGSDIALOGGCBBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)


class vPrjDocSettingsDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=0, flag=wx.ALIGN_CENTER,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJDOCSETTINGSDIALOG,
              name=u'vPrjDocSettingsDialog', parent=prnt, pos=wx.Point(279, 47),
              size=wx.Size(451, 280), style=wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vPrjDoc Settings Dialog'))
        self.SetClientSize(wx.Size(443, 253))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCSETTINGSDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Ok'), name=u'gcbbOk',
              parent=self, pos=wx.Point(0, 20), size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VPRJDOCSETTINGSDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJDOCSETTINGSDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(108, 20),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VPRJDOCSETTINGSDIALOGGCBBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vPrjDoc')
        self._init_ctrls(parent)
        
        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableRow(0)
        #self.panel=vgdPrjTimerSettings(parent=self , id=wx.NewId(),
        #      pos=wx.Point(0, 0), size=wx.Size(240, 140), 
        #      style=wx.TAB_TRAVERSAL, name=u'vgpGroup')
        self.panel=vPrjDocSettingsPanel(self , wx.NewId(),
              wx.Point(0, 0), wx.Size(240, 140), 
              wx.TAB_TRAVERSAL, u'vPrjDocSettings')
        self.gbsData.AddWindow(self.panel, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
        self.gbsData.Layout()
        self.gbsData.Fit(self)

        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)
    def SetXml(self,doc,node):
        self.panel.SetXml(doc,node)
    def GetXml(self,doc,node):
        self.panel.GetXml(doc,node)
    def SetRegNode(self,obj):
        pass
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
        self.bNet=bNet
        
    def SetNode(self,node):
        nodeTmp=self.doc.getChild(self.doc.getRoot(),'settings')
        self.panel.SetXml(self.doc,nodeTmp)
    def SetNodeExt(self,objReg,nodePar,node):
        self.objReg=objReg
        self.nodePar=nodePar
        self.panel.SetNode(node)
    def GetNode(self,node=None):
        nodeTmp=self.doc.getChild(self.doc.getRoot(),'settings')
        self.panel.GetXml(self.doc,nodeTmp)
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
