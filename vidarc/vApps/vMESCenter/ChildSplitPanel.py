#Boa:FramePanel:ChildSplitPanel

import wx
from wx.lib.anchors import LayoutAnchors
import  wx.lib.anchors as anchors

[wxID_CHILDSPLITPANEL, wxID_CHILDSPLITPANELLSTLOG, 
 wxID_CHILDSPLITPANELSPWMAIN, wxID_CHILDSPLITPANELWININFO, 
 wxID_CHILDSPLITPANELWINNAV, 
] = [wx.NewId() for _init_ctrls in range(5)]

class ChildSplitPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_CHILDSPLITPANEL,
              name=u'ChildSplitPanel', parent=prnt, pos=wx.Point(304, 117),
              size=wx.Size(461, 268), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(453, 241))
        self.Bind(wx.EVT_SIZE, self.OnChildSplitPanelSize)

        self.spwMain = wx.SplitterWindow(id=wxID_CHILDSPLITPANELSPWMAIN,
              name=u'spwMain', parent=self, point=wx.Point(0, 0),
              size=wx.Size(200, 100), style=wx.SP_3D|wx.SP_LIVE_UPDATE)

        self.winInfo = wx.Window(id=wxID_CHILDSPLITPANELWININFO,
              name=u'winInfo', parent=self.spwMain, pos=wx.Point(87, 2),
              size=wx.Size(111, 96), style=0)
        self.winInfo.SetAutoLayout(True)

        self.lstLog = wx.ListCtrl(id=wxID_CHILDSPLITPANELLSTLOG, name=u'lstLog',
              parent=self.winInfo, pos=wx.Point(8, 8), size=wx.Size(328, 208),
              style=wx.LC_REPORT)

        self.winNav = wx.Window(id=wxID_CHILDSPLITPANELWINNAV, name=u'winNav',
              parent=self.spwMain, pos=wx.Point(2, 2), size=wx.Size(78, 96),
              style=0)
        self.winNav.Bind(wx.EVT_SIZE, self.OnWinNavSize)
        self.spwMain.SplitVertically(self.winNav, self.winInfo, 80)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        #lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self, wx.Top, 2)
        #lc.left.SameAs     (self, wx.Left, 2)
        #lc.bottom.SameAs      (self, wx.Bottom, 2)
        #lc.width.PercentOf  (self, wx.Width, 2)
        #self.spwMain.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs     (self, wx.Left, 2)
        lc.bottom.SameAs      (self, wx.Bottom, 2)
        lc.width.PercentOf  (self, wx.Width, 2)
        self.lstLog.SetConstraints(lc)

    def OnChildSplitPanelSize(self, event):
        self.spwMain.SetSize(event.GetSize())
        #self.spwMain.UpdateSize()
        #event.Skip()

    def OnWinInfoSize(self, event):
        self.lstLog.SetSize(event.GetSize())
        event.Skip()
        pass

    def OnWinNavSize(self, event):
        event.Skip()
        
