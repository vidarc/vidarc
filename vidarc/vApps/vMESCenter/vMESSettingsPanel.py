#Boa:FramePanel:vMESSettingsPanel
#----------------------------------------------------------------------------
# Name:         vMESSettingsPanel.py
# Purpose:      configuration panel
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vMESSettingsPanel.py,v 1.1 2006/02/10 22:37:03 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton

import sys,os.path
import images

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

[wxID_VMESSETTINGSPANEL, wxID_VMESSETTINGSPANELCBADD, 
 wxID_VMESSETTINGSPANELCBDEL, wxID_VMESSETTINGSPANELDBBDN, 
 wxID_VMESSETTINGSPANELLSTDN, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vMESSettingsPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDN, (0, 0), border=2, flag=wx.EXPAND | wx.ALL,
              span=(1, 1))
        parent.AddWindow(self.cbAdd, (0, 2), border=2,
              flag=wx.TOP | wx.RIGHT | wx.BOTTOM, span=(1, 1))
        parent.AddWindow(self.cbDel, (1, 2), border=2,
              flag=wx.TOP | wx.RIGHT | wx.BOTTOM, span=(1, 1))
        parent.AddWindow(self.lstDN, (1, 0), border=2, flag=wx.EXPAND | wx.ALL,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VMESSETTINGSPANEL,
              name=u'vMESSettingsPanel', parent=prnt, pos=wx.Point(378, 250),
              size=wx.Size(386, 192), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(378, 165))
        self.SetAutoLayout(True)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'choose a directory'),
              id=wxID_VMESSETTINGSPANELDBBDN, labelText=_(u'directory:'),
              parent=self, pos=wx.Point(2, 2), size=wx.Size(283, 34),
              startDirectory='.', style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self, pos=wx.Point(297, 4), size=wx.Size(76, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VMESSETTINGSPANELCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(297, 42), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VMESSETTINGSPANELCBDEL)

        self.lstDN = wx.ListCtrl(id=wxID_VMESSETTINGSPANELLSTDN, name=u'lstDN',
              parent=self, pos=wx.Point(4, 42), size=wx.Size(279, 110),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstDN.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnLstDNListItemSelected,
              id=wxID_VMESSETTINGSPANELLSTDN)
        self.lstDN.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstDNListItemDeselected, id=wxID_VMESSETTINGSPANELLSTDN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        
        self.gbsData.AddGrowableRow(1)
        self.gbsData.AddGrowableCol(0)
        
        self.lstDN.InsertColumn(0,_(u'path'),wx.LIST_FORMAT_LEFT,250)
        self.cbAdd.SetBitmapLabel(images.getAddAttrBitmap())
        self.cbDel.SetBitmapLabel(images.getDelAttrBitmap())
        self.Move(pos)
        self.SetSize(size)
        self.SetupImageList()
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['dnOk']=self.imgLstTyp.Add(images.getApplyBitmap())
        self.imgDict['dnFlt']=self.imgLstTyp.Add(images.getCancelBitmap())
        self.lstDN.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.lstDN.DeleteAllItems()
        self.dbbDN.SetValue('')
    def SetDoc(self,doc,bNet):
        vtLog.CallStack('')
        vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node):
        try:
            self.Clear()
            if self.doc is None:
                return -1
            vtXmlDomConsumer.SetNode(self,node)
            tmp=self.doc.getChild(self.node,'pluginDNs')
            if tmp is not None:
                for c in self.doc.getChilds(tmp,'pluginDN'):
                    sVal=self.doc.getText(c)
                    self.__insertDN__(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def __insertDN__(self,sVal):
        try:
            if os.path.isdir(sVal):
                img=self.imgDict['dnOk']
            else:
                img=self.imgDict['dnFlt']
        except:
            img=-1
        if len(sVal)>0:
            idx=self.lstDN.InsertImageStringItem(sys.maxint,sVal,img)
    def GetNode(self):
        try:
            if self.doc is None:
                return -1
            if self.node is None:
                return -1
            tmp=self.doc.getChildForced(self.node,'pluginDNs')
            self.doc.delNode(tmp)
            lst=[]
            for i in range(self.lstDN.GetItemCount()):
                sVal=self.lstDN.GetItemText(i)
                lst.append({'tag':'pluginDN','val':sVal})
            self.doc.createChildByLst(self.node,'pluginDNs',lst)
                #self.doc.setNodeText(tmp,'pluginDN',sVal)
            self.doc.AlignNode(self.node,iRec=2)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbAddButton(self, event):
        try:
            sVal=self.dbbDN.GetValue()
            self.__insertDN__(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnCbDelButton(self, event):
        iSel=-1
        for i in range(self.lstDN.GetItemCount()):
            if self.lstDN.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                iSel=i
                break
        #iSel=self.lstDN.GetSelection()
        if iSel>=0:
            self.lstDN.DeleteItem(iSel)
        event.Skip()

    def OnLstDNListbox(self, event):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.lstDN.GetSelection(),self)
            
        except:
            pass
        event.Skip()

    def OnLstDNListItemSelected(self, event):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            idx=event.GetIndex()
            it=self.lstDN.GetItem(idx,0)
            sDN=it.m_text
            self.dbbDN.SetValue(sDN)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnLstDNListItemDeselected(self, event):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.dbbDN.SetValue('')
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
