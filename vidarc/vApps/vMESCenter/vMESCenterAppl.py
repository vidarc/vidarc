#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vMESCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterAppl.py,v 1.5 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback,time
import wx

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSplashFrame as vSplashFrame

#import Frame
import vidarc.vApps.vMESCenter.vMESCenterParentFrame as vMESCenterParentFrame
import vidarc.vApps.vMESCenter.images_splash as images_splash

modules ={#u'Frame': [1, 'Main frame of Application', u'Frame.py'],
 u'vMESCenterParentFrame': [0, '', u'vMESCenterParentFrame.py']}

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        wx.App.__init__(self,num)
    def OnInit(self):
        wx.InitAllImageHandlers()
        if 1==0:
            vtLog.vtLngInit(self.sAppl,'%s.log'%self.sAppl,self.iLogLv,iSockPort=self.iSockPort)
            self.main = vMESCenterParentFrame.create(None)
            self.main.OpenCfgFile(self.cfgFN)
            self.main.Show()
            self.SetTopWindow(self.main)
        else:
            #return True
        #self.main = Frame.create(None)
        #self.main = vMESCenterParentFrame.create(None)
        #self.main.Show()
        #self.SetTopWindow(self.main)
        #return True
            actionList=[]
            actionListProc=[]
            d={'label':u'  import vMESCenterMain ...',
                'eval':'__import__("vidarc.vApps.vMESCenter.vMESCenterParentFrame")'}
            actionList.append(d)
            d={'label':u'  Create Center ...',
                'eval':'self.res[0].vApps.vMESCenter.vMESCenterParentFrame'}
            actionList.append(d)
            d={'label':u'  Create Center ...',
                'eval':'getattr(self.res[1],"create")'}
            actionList.append(d)
            d={'label':u'  Create Center ...',
                'eval':'self.res[2](None)'}
            actionList.append(d)
            
            
            #d={'label':u'  Show   Center ...',
            #    'eval':'self.res[3].Show()'}
            #actionList.append(d)
            
            d={'label':u'  Finished.'}
            actionList.append(d)
            
            d={'label':u'  Open Config...',
                'eval':'self.res[3].OpenCfgFile("%s")'%self.cfgFN}
            actionListProc.append(d)
            
            d={'label':u'  Open File...',
                'eval':'self.res[3].OpenFile("%s")'%self.fn}
            actionListProc.append(d)
            
            self.splash = vSplashFrame.create(None,'MES','Center',
                images_splash.getSplashBitmap(),
                actionList,
                actionListProc,
                self.sAppl,
                self.iLogLv,
                self.iSockPort)
            vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
            vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
            vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
            vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
            self.splash.Show()
        
        return True
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[3]
        self.main.Show()
        self.SetTopWindow(self.main)
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        #self.main=self.splash.res[3]
        self.main.Destroy()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main():
    optshort,optlong='l:f:c:h',['lang=','file=','config=','log=','help']
    vtLgBase.initAppl(optshort,optlong,'vMESCenter')
        
    fn=None
    cfgFN='vMESCfg.xml'
    iLogLv=vtLog.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLog.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLog.INFO
                elif o[1]=='2':
                    iLogLv=vtLog.WARN
                elif o[1]=='3':
                    iLogLv=vtLog.ERROR
                elif o[1]=='4':
                    iLogLv=vtLog.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLog.FATAL

    except:
        showHelp()
        traceback.print_exc()
    application = BoaApp(0,'vMESCenter',cfgFN,fn,iLogLv,60099)
    application.MainLoop()

if __name__ == '__main__':
    main()
