#Boa:Frame:ChildTextFrame

import wx

def create(parent):
    return ChildTextFrame(parent)

[wxID_CHILDTEXTFRAME, wxID_CHILDTEXTFRAMEPNMAIN, wxID_CHILDTEXTFRAMETXTNAME, 
] = [wx.NewId() for _init_ctrls in range(3)]

class ChildTextFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_CHILDTEXTFRAME, name=u'ChildTextFrame',
              parent=prnt, pos=wx.Point(176, 176), size=wx.Size(400, 250),
              style=wx.DEFAULT_FRAME_STYLE, title=u'Child Text')
        self.SetClientSize(wx.Size(392, 223))

        self.pnMain = wx.Panel(id=wxID_CHILDTEXTFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(392, 223),
              style=wx.TAB_TRAVERSAL)

        self.txtName = wx.TextCtrl(id=wxID_CHILDTEXTFRAMETXTNAME,
              name=u'txtName', parent=self.pnMain, pos=wx.Point(24, 16),
              size=wx.Size(100, 21), style=0, value=u'name')

    def __init__(self, parent):
        self._init_ctrls(parent)
