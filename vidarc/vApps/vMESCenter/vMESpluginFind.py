#----------------------------------------------------------------------------
# Name:         vMESpluginFind.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060209
# CVS-ID:       $Id: vMESpluginFind.py,v 1.4 2006/07/17 11:06:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,os.path,sys
import string,thread,threading,time

import vidarc.tool.log.vtLog as vtLog

wxEVT_VMESPLUGIN_THREAD_PROC=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_PROC=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_PROC,1)
def EVT_VMESPLUGIN_THREAD_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_PROC,func)
class vMESpluginThreadProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_PROC(<widget_name>, self.OnProc)
    """

    def __init__(self,obj,iAct,iSize):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.iAct=iAct
        self.iSize=iSize
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_PROC)
    def GetAct(self):
        return self.iAct
    def GetSize(self):
        return self.iSize

wxEVT_VMESPLUGIN_THREAD_ADD=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD,1)
def EVT_VMESPLUGIN_THREAD_ADD(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD,func)
class vMESpluginThreadAdd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD(<widget_name>, self.OnPlugInAdd)
    """

    def __init__(self,obj,name,bFrame,dn):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.name=name
        self.bFrame=bFrame
        self.dn=dn
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD)
    def GetName(self):
        return self.name
    def IsFrame(self):
        return self.bFrame
    def GetDN(self):
        return self.dn

wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,1)
def EVT_VMESPLUGIN_THREAD_ADD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,func)
class vMESpluginThreadAddFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_FINISHED(<widget_name>, self.OnPlugInFin)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED)
    
wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,1)
def EVT_VMESPLUGIN_THREAD_ADD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,func)
class vMESpluginThreadAddAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_ABORTED(<widget_name>, self.OnPlugInAbort)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED)

class thdMESpluginFind:
    DIR_2_SKIP=['CVS','locale']
    def __init__(self,par,verbose=-1):
        self.par=par
        self.lDN=[]
        self.running = False
        self.verbose=verbose
        self.updateProcessbar=True
    def Find(self,lDN,func=None,*args):
        self.lDN=lDN
        self.zStart=time.clock()
        self.func=func
        self.args=args
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def __getImport__(self,fn):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,fn,self.par)
            if self.verbose>0:
                vtLog.CallStack('')
            for dn in sys.path:
                if self.verbose>0:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;%s'%(dn,fn),self.par)
                if sys.platform=='win32':
                    sfn=fn.lower()
                else:
                    sfn=fn
                if sfn.find(dn)==0:
                    iLen=len(dn)
                    if not dn[-1] in ['/','\\']:
                        iLen+=1
                    name=fn[iLen:]
                    name=name.replace('/','.')
                    name=name.replace('\\','.')
                    return name
        except:
            vtLog.vtLngTB(self.par.GetName())
        return None
    def __findPlugin__(self,dn):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,dn,self.par)
            base,name=os.path.split(dn)
            if name in self.DIR_2_SKIP:
                return -1
            lFiles=os.listdir(dn)
            lFiles.sort()
            lFiles.reverse()
            lPlugIns=[]
            for fn in lFiles:
                strs=fn.split('.')
                if strs[-1]=='vidarc':
                    sPlugIn='.'.join(strs[:-4])
                    if sPlugIn not in lPlugIns:
                        lPlugIns.append(sPlugIn)
            vtLog.vtLngCurWX(vtLog.DEBUG,'plugins:%s'%(vtLog.pformat(lPlugIns)),self.par)
            sys.path.insert(0,dn)
            bPlugInFound=False
            for sPlugIn in lPlugIns:
                sImp='%s.__init__'%sPlugIn
                vtLog.vtLngCurWX(vtLog.DEBUG,sImp,self.par)
                try:
                    plgin=__import__(sImp,[],[],['PLUGABLE'])
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s plugin:%s has __init__'%(dn,sPlugIn),self.par)
                    if self.verbose>0:
                            vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(plgin.__dict__),self.par)
                    try:
                        sPlgInName=plgin.PLUGABLE
                        bPlugInFound=True
                        vtLog.vtLngCurWX(vtLog.INFO,'dir:%s is PLUGABLE'%dn,self.par)
                        if len(sPlgInName):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'PLUGABLE:%s'%sPlgInName,self.par)
                            try:
                                bPlgInFrame=plgin.PLUGABLE_FRAME
                            except:
                                bPlgInFrame=False
                            wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,sPlugIn+'.'+sPlgInName,bPlgInFrame,dn))
                    except:
                        del plgin
                        #vtLog.vtLngTB(self.par.GetName())
                        vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no PLUGABLE defined'%dn,self.par)
                except:
                    vtLog.vtLngTB(self.par.GetName())
            if bPlugInFound==False:
                sys.path=sys.path[1:]
                    
            sImp=self.__getImport__(dn+'.__init__')
            vtLog.vtLngCurWX(vtLog.DEBUG,sImp,self.par)
            if sImp is not None:
                #eval('import %s as plgin'%sImg)
                try:
                    plgin=__import__(sImp,[],[],['PLUGABLE'])
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s has __init__'%dn,self.par)
                    if self.verbose>0:
                        vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(plgin.__dict__),self.par)
                    try:
                        sPlgInName=plgin.PLUGABLE
                        vtLog.vtLngCurWX(vtLog.INFO,'dir:%s is PLUGABLE'%dn,self.par)
                        if len(sPlgInName):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'PLUGABLE:%s'%sPlgInName,self.par)
                            try:
                                bPlgInFrame=plgin.PLUGABLE_FRAME
                            except:
                                bPlgInFrame=False
                            wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,self.__getImport__(dn+'.'+sPlgInName),bPlgInFrame,dn))
                    except:
                        del plgin
                        #vtLog.vtLngTB(self.par.GetName())
                        vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no PLUGABLE defined'%dn,self.par)
                    return 0
                except:
                    vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no __init__'%dn,self.par)
                return -1
        except:
            #vtLog.vtLngTB(self.par.GetName())
            vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s no init found here'%dn,self.par)
        return -1
    def __walkDir__(self,dn):
        try:
            if self.keepGoing==False:
                return
            iRet=self.__findPlugin__(dn)
            if iRet<0:
                if self.updateProcessbar:
                    self.iAct+=self.__walkDirCalc__(dn)
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                return
            files=os.listdir(dn)
            vtLog.vtLngCurWX(vtLog.DEBUG,dn,self.par)
            if self.verbose>0:
                vtLog.CallStack(dn)
                vtLog.pprint(files)
            for fn in files:
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    self.__walkDir__(sFullFN)
        except:
            vtLog.vtLngTB(self.par.GetName())
    def __walkDirCalc__(self,dn):
        try:
            iSize=0
            if self.keepGoing==False:
                return iSize
            files=os.listdir(dn)
            if self.verbose>0:
                vtLog.CallStack(dn)
                vtLog.pprint(files)
            iSize=len(files)
            for fn in files:
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    iSize+=self.__walkDirCalc__(sFullFN)
        except:
            vtLog.vtLngTB(self.par.GetName())
        return iSize
    def Run(self):
        try:
            self.running = False
            if self.updateProcessbar:
                self.iAct=0
                self.iSize=0
                for dn in self.lDN:
                    self.iSize+=self.__walkDirCalc__(dn)
                wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
            for dn in self.lDN:
                self.__walkDir__(dn)
            if self.keepGoing:
                if self.func is not None:
                    self.func(*self.args[0])
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddFinished(self.par))
            else:
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddAborted(self.par))
        except:
            vtLog.vtLngTB(self.par.GetName())

