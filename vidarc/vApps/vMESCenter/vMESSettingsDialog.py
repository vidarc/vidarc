#Boa:Dialog:vMESSettingsDialog
#----------------------------------------------------------------------------
# Name:         vMESSettingsDialog.py
# Purpose:      configuration dialog
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vMESSettingsDialog.py,v 1.1 2006/02/10 22:37:03 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

from vMESSettingsPanel import *
import images

def create(parent):
    return vMESSettingsDialog(parent)

[wxID_VMESSETTINGSDIALOG, wxID_VMESSETTINGSDIALOGCBCANCEL, 
 wxID_VMESSETTINGSDIALOGCBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vMESSettingsDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMESSETTINGSDIALOG,
              name=u'vMESSettingsDialog', parent=prnt, pos=wx.Point(434, 167),
              size=wx.Size(400, 316), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vMES Settings')
        self.SetClientSize(wx.Size(392, 289))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(88, 256), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VMESSETTINGSDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(224, 256), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VMESSETTINGSDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.pnSettings=vMESSettingsPanel(self,-1,(4,4),(384,240),0,'pnSettings')
        self.pnSettings.SetConstraints(LayoutAnchors(self.pnSettings, True, True, True,
              True))
        self.cbOk.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
    def SetDoc(self,doc,bNet=False):
        self.pnSettings.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnSettings.SetNode(node)
    def OnCbOkButton(self, event):
        self.pnSettings.GetNode()
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
