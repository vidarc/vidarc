#Boa:MDIParent:vMESCenterParentFrame

import wx
from wx.lib.anchors import LayoutAnchors

from vidarc.tool.xml.vtXmlDom import *
from vidarc.tool.log.vtLogFileViewerFrame import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.vApps.common.vMDIFrame import vMDIFrame
from vidarc.vApps.common.vAboutDialog import vAboutDialog
import vidarc.vApps.common.vSystem as vSystem

from vMESSettingsDialog import *
from vMESpluginFind import *
from vMESplugin import *

import images

from ChildTextFrame import *
from ChildTextWindow import *
from ChildListPanel import *
from ChildSplitPanel import *
from ChildSashPanel import *

import string,os,types

def create(parent):
    return vMESCenterParentFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VMESCENTERPARENTFRAME, wxID_VMESCENTERPARENTFRAMEPNTOP, 
 wxID_VMESCENTERPARENTFRAMESBMAIN, wxID_VMESCENTERPARENTFRAMESLWBOTTOM, 
 wxID_VMESCENTERPARENTFRAMESLWNAV, wxID_VMESCENTERPARENTFRAMESLWTOP, 
 wxID_VMESCENTERPARENTFRAMETBMAIN, wxID_VMESCENTERPARENTFRAMETRPLUGINS, 
] = [wx.NewId() for _init_ctrls in range(8)]

[wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CLOSE, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW2, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SASH, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SPLIT, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(7)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

[wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP, 
 wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vMESCenterParentFrame(wx.MDIParentFrame,vStatusBarMessageLine):
    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW,
              kind=wx.ITEM_NORMAL, text=u'New')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CLOSE,
              kind=wx.ITEM_NORMAL, text=u'Close')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW2,
              kind=wx.ITEM_NORMAL, text=u'New Panel')
        parent.Append(help='',
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SPLIT,
              kind=wx.ITEM_NORMAL, text=u'New Split')
        parent.Append(help='',
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SASH,
              kind=wx.ITEM_NORMAL, text=u'New Sash')
        parent.Append(help='',
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS,
              kind=wx.ITEM_NORMAL, text=u'Settings')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tALT+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_newMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_closeMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_new2Menu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW2)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_new_splitMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SPLIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_new_sashMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_NEW_SASH)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_exitMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_settingsMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnHelp, title=_(u'Help'))

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=u'About')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=u'Log')
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbMain_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title='')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIParentFrame.__init__(self, id=wxID_VMESCENTERPARENTFRAME,
              name=u'vMESCenterParentFrame', parent=prnt, pos=wx.Point(114, 25),
              size=wx.Size(867, 676),
              style=wx.DEFAULT_FRAME_STYLE | wx.VSCROLL | wx.HSCROLL,
              title=u'VIDARC MES Center')
        self._init_utils()
        self.SetClientSize(wx.Size(859, 649))
        self.SetMenuBar(self.mnBar)
        self.Bind(wx.EVT_SIZE, self.OnVMESCenterParentFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnVMESCenterParentFrameMove)
        self.Bind(wx.EVT_CLOSE, self.OnVMESCenterParentFrameClose)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VMESCENTERPARENTFRAMESLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(120, 28),
              size=wx.Size(1000, 60), style=wx.NO_BORDER | wx.SW_3D)
        self.slwTop.SetDefaultSize((1000, 60))
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VMESCENTERPARENTFRAMESLWTOP)

        self.slwBottom = wx.SashLayoutWindow(id=wxID_VMESCENTERPARENTFRAMESLWBOTTOM,
              name=u'slwBottom', parent=self, pos=wx.Point(120, 580),
              size=wx.Size(1000, 30), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwBottom.SetDefaultSize(wx.Size(1000, 30))
        self.slwBottom.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwBottom.SetAlignment(wx.LAYOUT_BOTTOM)
        self.slwBottom.SetSashVisible(wx.SASH_TOP, True)
        self.slwBottom.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwBottomSashDragged,
              id=wxID_VMESCENTERPARENTFRAMESLWBOTTOM)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VMESCENTERPARENTFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 28),
              size=wx.Size(120, 1000), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(120, 1000))
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetExtraBorderSize(8)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VMESCENTERPARENTFRAMESLWNAV)

        self.pnTop = wx.Panel(id=wxID_VMESCENTERPARENTFRAMEPNTOP, name=u'pnTop',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.Size(736, 61),
              style=wx.TAB_TRAVERSAL)

        self.tbMain = wx.ToolBar(id=wxID_VMESCENTERPARENTFRAMETBMAIN,
              name=u'tbMain', parent=self, pos=wx.Point(0, 0), size=wx.Size(859,
              28), style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.SetToolBar(self.tbMain)

        self.sbMain = wx.StatusBar(id=wxID_VMESCENTERPARENTFRAMESBMAIN,
              name=u'sbMain', parent=self, style=0)
        self._init_coll_sbMain_Fields(self.sbMain)
        self.SetStatusBar(self.sbMain)

        self.trPlugins = wx.TreeCtrl(id=wxID_VMESCENTERPARENTFRAMETRPLUGINS,
              name=u'trPlugins', parent=self.slwNav, pos=wx.Point(0, 0),
              size=wx.Size(120, 584), style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT)
        self.trPlugins.Bind(wx.EVT_LEFT_DCLICK, self.OnTrPluginsLeftDclick)

    def __init__(self, parent):
        self._init_ctrls(parent)
        vStatusBarMessageLine.__init__(self,self.sbMain,STATUS_TEXT_POS,5000)
        
        self.winCount=0
        self.bBlockCfgEventHandling=True
        self.docCfg=vtXmlDom()
        
        self.dlgSettings=None
        self.dMDI={}
        icon = getApplicationIcon()
        self.SetIcon(icon)

        # setup statusbar
        self.iLogOldSum=0
        self.zLogUnChanged=0
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbMain.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbMain, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(1000)
        self.gProcess.SetValue(0)
        #self.Bind(
        #    wx.EVT_SASH_DRAGGED_RANGE, self.OnSashDrag, id2=wxID_VMESCENTERPARENTFRAMESLWTOP, 
        #    id=wxID_VMESCENTERPARENTFRAMESLWBOTTOM
        #    )
        #print wxID_VMESCENTERPARENTFRAMESLWTOP,wxID_VMESCENTERPARENTFRAMESLWNAV,wxID_VMESCENTERPARENTFRAMESLWBOTTOM
        #print wxID_VMESCENTERPARENTFRAMESLWTOP,wxID_VMESCENTERPARENTFRAMESLWBOTTOM
        self.thdPlugin=thdMESpluginFind(self,verbose=0)
        EVT_VMESPLUGIN_THREAD_PROC(self,self.OnPlugInProc)
        EVT_VMESPLUGIN_THREAD_ADD(self,self.OnPlugInAdd)
        EVT_VMESPLUGIN_THREAD_ADD_FINISHED(self,self.OnPlugInFin)
        self.dPlugIn={}
        
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getModuleBitmap()
        self.imgDict['root']=self.imgLstTyp.Add(img)
        self.imgDict['mod']=self.imgLstTyp.Add(img)
        dLg={}
        for lg,lgName in [('en',vtArt.LangEn),('de',vtArt.LangDe),('fr',vtArt.LangFr)]:
            dLg[lg]=self.imgLstTyp.Add(vtArt.getBitmap(lgName))
        self.imgDict['lang']=dLg
        self.trPlugins.SetImageList(self.imgLstTyp)
        
        
        img=self.imgDict['root']
        self.trPlugins.AddRoot(_(u'plugins'))
        ti=self.trPlugins.GetRootItem()
        self.trPlugins.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
        self.trPlugins.SetItemImage(ti,img,wx.TreeItemIcon_Expanded)
        self.tiModuls=self.trPlugins.AppendItem(ti,_('modules'),img,img,None)
        self.tiPlgIns=self.trPlugins.AppendItem(ti,_('plugins'),img,img,None)
    def OnPlugInProc(self,evt):
        try:
            iAct=evt.GetAct()
            iSize=evt.GetSize()
            if iSize>0:
                iVal=((iAct/float(iSize))*1000)
            else:
                iVal=0
            self.gProcess.SetValue(iVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnPlugInAdd(self,evt):
        try:
            name=evt.GetName()
            self.PrintMsg(_('plugin searching ... %s found ')%(name))
            vtLog.vtLngCurWX(vtLog.DEBUG,name,self)
            i=name.find('vApps')
            if i>0:
                strs=name[i+6:].split('.')
                strs=[strs[0]]
                tip=self.tiModuls
                d=self.dPlugIn
            else:
                strs=string.split(name,'.')
                d=self.dPlugIn
                #tip=self.trPlugins.GetRootItem()
            
                tip=self.tiPlgIns
            plugin=vMESplugin(evt.GetName(),evt.IsFrame(),evt.GetDN())
            self.imgDict[name]=self.imgLstTyp.Add(plugin.GetBitmap())
            iLen=len(strs)
            i=1
            for s in strs:
                if not d.has_key(s):
                    ti=self.trPlugins.AppendItem(tip,s,-1,-1,None)
                    if i==iLen:
                        img=self.imgDict[plugin.GetName()]
                    else:
                        img=self.imgDict['mod']
                    self.trPlugins.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
                    self.trPlugins.SetItemImage(ti,img,wx.TreeItemIcon_Expanded)
                    if i==iLen:
                        try:
                            langs=plugin.getPossibleLang()
                            vtLog.vtLngCurWX(vtLog.DEBUG,langs,self)
                            for lg in langs:
                                img=self.imgDict['lang'][lg]
                                tid=wx.TreeItemData()
                                tid.SetData((plugin,lg))
                                tiLg=self.trPlugins.AppendItem(ti,lg,img,img,tid)
                        except:
                            vtLog.vtLngTB(self.GetName())
                    d[s]=(ti,{})
                    tip=ti
                else:
                    tip=d[s][0]
                d=d[s][1]
                i+=1
            tid=wx.TreeItemData()
            tid.SetData(plugin)
            self.trPlugins.SetItemData(tip,tid)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnPlugInFin(self,evt):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #tip=self.trPlugins.GetRootItem()
            #self.trPlugins.Expand(tip)
            self.trPlugins.Expand(self.tiModuls)
            self.trPlugins.Expand(self.tiPlgIns)
            self.PrintMsg(_('plugin search finished.'))
            self.gProcess.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getPluginNameLst__(self,ti):
        l=[]
        while ti is not None and ti != self.trPlugins.GetRootItem():
            l.append(self.trPlugins.GetItemText(ti))
            ti=self.trPlugins.GetItemParent(ti)
        l.reverse()
        return l
    def __getPluginCfg__(self,l):
        try:
            d={}
            cfgNode=self.docCfg.getChildByLstForced(None,l)
            if cfgNode is not None:
                for c in self.docCfg.getChilds(cfgNode):
                    d[self.docCfg.getTagName(c)]=self.docCfg.getText(c)
            return d
        except:
            vtLog.vtLngTB(self.GetName())
            return {}
    def __setPluginCfg__(self,l,d):
        try:
            cfgNode=self.docCfg.getChildByLstForced(None,l)
            for c in self.docCfg.getChilds(cfgNode):
                self.docCfg.deleteNode(c,cfgNode)
            keys=d.keys()
            keys.sort()
            for k in keys:
                kr=fnUtil.replaceSuspectChars(k)
                self.docCfg.setNodeText(cfgNode,kr,d[k])
            self.docCfg.AlignNode(cfgNode)
            self.docCfg.Save()
            return 0
        except:
            vtLog.vtLngTB(self.GetName())
            return -1
    def OnTrPluginsLeftDclick(self, event):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            ti=self.trPlugins.GetSelection()
            tid=self.trPlugins.GetPyData(ti)
            if tid is not None:
                lang=None
                if type(tid)==types.TupleType:
                    ti=self.trPlugins.GetItemParent(ti)
                    tid,lang=tid
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                self.winCount+=1
                
                #win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
                #canvas = ChildListPanel(win,id=-1, pos=(0,0), size=(200,100), 
                #                style=wx.TAB_TRAVERSAL, name='child')
                #win.Show(True)
                l=self.__getPluginNameLst__(ti)
                dCfg=self.__getPluginCfg__(l)
                self.PrintMsg(_('launch plugin %s ...')%(tid.GetName()))
                tid.setLang(lang)
                if tid.IsFrame():
                    sz=(800,300)
                    win=tid.createFrame(self, -1, wx.DefaultPosition , sz, 0 , "%s: %d" % (tid.GetWinName(),self.winCount))
                    self.dMDI[win.GetId()]=(tid,win)
                    #win.Show(True)
                    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dMDI),self)
                    try:
                        win.SetCfgData(self.__setPluginCfg__,l,dCfg)
                    except:
                        vtLog.vtLngTB(self.GetName())
                else:
                    win = wx.MDIChildFrame(self, -1, "%s: %d" % (tid.GetWinName(),self.winCount))
                    plugin=tid.create(win,-1,(0,0),wx.DefaultSize,wx.TAB_TRAVERSAL, 'child')
                    self.dMDI[win.GetId()]=(tid,plugin)
                    #win.SetClientSize(plugin.GetSize())
                    #win.Show(True)
                    try:
                        plugin.SetCfgData(self.__setPluginCfg__,l,dCfg)
                    except:
                        vtLog.vtLngCurWX(vtLog.DEBUG,traceback.format_exc(),self)
                self.PrintMsg(_('plugin %s launched.')%(tid.GetName()))
                win.Show(True)
                
                win.Bind(wx.EVT_SIZE, self.OnMDISize)
                win.Bind(wx.EVT_MOVE, self.OnMDIMove)
                win.Bind(wx.EVT_CLOSE, self.OnMDIClose)

        except:
            vtLog.vtLngTB(self.GetName())
        #event.Skip()
    def OnMDISize(self,evt):
        evt.Skip()
    def OnMDIMove(self,evt):
        evt.Skip()
    def OnMDIClose(self,evt):
        evt.Skip()
        win=evt.GetEventObject()
        id=win.GetId()
        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d;%s'%(id,vtLog.pformat(self.dMDI)),self)
        try:
            plgCls,plugin=self.dMDI[id]
            plugin.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC MES Center")
        #fn=self.netXXX.GetFN()
        #if fn is None:
        #    s=s+_(" (undef*)")
        #else:
        #    s=s+" ("+fn
        #    if self.netMaster.IsModified():
        #        s=s+"*)"
        #    else:
        #        s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    
    def OpenFile(self,sFN):
        pass
    def OpenCfgFile(self,sFN):
        self.PrintMsg(_('open config ...'))
        if self.docCfg.Open(sFN)<0:
            self.docCfg.New(root='configuration')
            self.docCfg.Save(sFN)
        self.__setCfg__()
        
        tmp=self.docCfg.getChildByLst(None,['settings','pluginDNs'])
        lDN=[]
        for c in self.docCfg.getChilds(tmp,'pluginDN'):
            lDN.append(self.docCfg.getText(c))
        self.PrintMsg(_('config opened.'))
        self.thdPlugin.Find(lDN)
        self.PrintMsg(_('plugin searching ...'))
    def __setCfg__(self):
        try:
            node=self.docCfg.getChildForced(None,'size')
            iX=self.docCfg.GetValue(node,'x',int,20)
            iY=self.docCfg.GetValue(node,'y',int,20)
            iWidth=self.docCfg.GetValue(node,'width',int,600)
            iHeight=self.docCfg.GetValue(node,'height',int,400)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.docCfg.getChildForced(None,'layout')
            vtLog.vtLngCurWX(vtLog.DEBUG,node,self)
            i=self.docCfg.GetValue(node,'bottom_sash',int,30)
            if i>80:
                i=80
            self.slwBottom.SetDefaultSize((1000, i))
            #wx.LayoutAlgorithm().LayoutMDIFrame(self)
            #self.GetClientWindow().Refresh()
            
            i=self.docCfg.GetValue(node,'nav_sash',int,120)
            self.slwNav.SetDefaultSize((i, 1000))
            #wx.LayoutAlgorithm().LayoutMDIFrame(self)
            #self.GetClientWindow().Refresh()
            
            i=self.docCfg.GetValue(node,'top_sash',int,30)
            self.slwTop.SetDefaultSize((1000, i))
            
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
            
            self.docCfg.AlignDoc()
            self.docCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSashDrag(self, event):
        try:
            vtLog.CallStack('')
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
    
            eID = event.GetId()
            #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if eID == wxID_VMESCENTERPARENTFRAMESLWTOP:
                vtLog.vtLngCurWX(vtLog.DEBUG,'top:%d'%event.GetDragRect().height,self)
                self.slwTop.SetDefaultSize((1000, event.GetDragRect().height))
    
            elif eID == wxID_VMESCENTERPARENTFRAMESLWNAV:
                vtLog.vtLngCurWX(vtLog.DEBUG,'nav:%d'%event.GetDragRect().width,self)
                self.slwNav.SetDefaultSize((event.GetDragRect().width, 1000))
    
            #elif eID == ID_WINDOW_LEFT2:
            #    self.leftWindow2.SetDefaultSize((event.GetDragRect().width, 1000))
    
            elif eID == wxID_VMESCENTERPARENTFRAMESLWBOTTOM:
                vtLog.vtLngCurWX(vtLog.DEBUG,'bottom:%d'%event.GetDragRect().height,self)
                self.slwBottom.SetDefaultSize((1000, event.GetDragRect().height))
    
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def __checkSash__(self,modified,val):
        sz=self.GetClientSize()
        szTop=self.slwTop.GetSize()
        szBottom=self.slwBottom.GetSize()
        szNav=self.slwNav.GetSize()
        #vtLog.CallStack('')
        #print sz
        #print szTop
        #print szBottom
        #print szNav
        #print val
        val+=10
        if modified==0:
            # top
            if val+szBottom[1]>=sz[1]:
                val=sz[1]-szBottom[1]
        elif modified==1:
            # bottom
            if val+szTop[1]>=sz[1]:
                val=sz[1]-szTop[1]
        elif modified==2:
            # nav
            pass
        val-=10
        if val<0:
            val=0
        return val
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            #if self.bBlockCfgEventHandling:
            #    return
            node=self.docCfg.getChildForced(None,'layout')
            iHeight=event.GetDragRect().height
            vtLog.vtLngCurWX(vtLog.DEBUG,iHeight,self)
            iHeight=self.__checkSash__(0,iHeight)
            if iHeight>80:
                iHeight=80
            self.docCfg.SetValue(node,'top_sash',iHeight)
            self.docCfg.Save()
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
        #event.Skip()

    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            #if self.bBlockCfgEventHandling:
            #    return
            node=self.docCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            vtLog.vtLngCurWX(vtLog.DEBUG,iWidth,self)
            iWidth=self.__checkSash__(2,iWidth)
            if iWidth>180:
                iWidth=180
            self.docCfg.SetValue(node,'nav_sash',iWidth)
            self.docCfg.Save()
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
        #event.Skip()

    def OnSlwBottomSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            #if self.bBlockCfgEventHandling:
            #    return
            node=self.docCfg.getChildForced(None,'layout')
            iHeight=event.GetDragRect().height
            vtLog.vtLngCurWX(vtLog.DEBUG,iHeight,self)
            iHeight=self.__checkSash__(1,iHeight)
            if iHeight>80:
                iHeight=80
            self.docCfg.SetValue(node,'bottom_sash',iHeight)
            self.docCfg.Save()
            self.slwBottom.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
        #event.Skip()

    def OnMnFileMnfile_newMenu(self, event):
        self.winCount+=1
        win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
        canvas = ChildTextFrame(win)
        win.Show(True)
        #event.Skip()

    def OnMnFileMnfile_closeMenu(self, event):
        self.winCount+=1
        win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
        canvas = ChildTextWindow(win)
        win.Show(True)
        #event.Skip()

    def OnMnFileMnfile_new2Menu(self, event):
        self.winCount+=1
        win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
        canvas = ChildListPanel(win,id=-1, pos=(0,0), size=(200,100), 
                        style=wx.TAB_TRAVERSAL, name='child')
        win.Show(True)
        #event.Skip()

    def OnMnFileMnfile_new_splitMenu(self, event):
        self.winCount+=1
        win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
        canvas = ChildSplitPanel(win,id=-1, pos=(0,0), size=(200,100), 
                        style=wx.TAB_TRAVERSAL, name='child')
        win.Show(True)
        event.Skip()

    def OnMnFileMnfile_new_sashMenu(self, event):
        self.winCount+=1
        win = wx.MDIChildFrame(self, -1, "Child Window: %d" % self.winCount)
        canvas = ChildSashPanel(win,id=-1, pos=(0,0), size=(200,100), 
                        style=wx.TAB_TRAVERSAL, name='child')
        win.Show(True)
        event.Skip()

    def OnVMESCenterParentFrameSize(self, event):
        node=self.docCfg.getChildForced(None,'size')
        self.docCfg.SetValue(node,'width',event.GetSize()[0])
        self.docCfg.SetValue(node,'height',event.GetSize()[1])
        self.docCfg.Save()
        wx.LayoutAlgorithm().LayoutMDIFrame(self)
        #self.GetClientWindow().Refresh()
        
        #event.Skip()

    def OnVMESCenterParentFrameMove(self, event):
        pos=self.GetPosition()
        node=self.docCfg.getChildForced(None,'size')
        self.docCfg.SetValue(node,'x',pos[0])
        self.docCfg.SetValue(node,'y',pos[1])
        self.docCfg.Save()
        #event.Skip()

    def OnMnFileMnfile_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnVMESCenterParentFrameClose(self, event):
        if False==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vMESCenterAppl'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.SaveFile()
                pass
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        frm=vtLogFileViewerFrame(self)
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()

    def OnVMESCenterParentFrameIdle(self, event):
        vtLog.CallStack('')
        #self.Disconnect(wx.EVT_IDLE, self.OnVMESCenterParentFrameIdle)
        #self.Disconnect(self.OnVMESCenterParentFrameIdle)
        #self.Disconnect(wxID_VMESCENTERPARENTFRAME,eventType=wx.EVT_IDLE_PROCESS_ALL)
        try:
            self.Disconnect(wx.wxEVT_IDLE, self.GetId())#,self.OnVMESCenterParentFrameIdle)
        except:
            print 'exe'
        event.Skip()

    def OnMnFileMn_file_settingsMenu(self, event):
        if self.dlgSettings is None:
            self.dlgSettings=vMESSettingsDialog(self)
            self.dlgSettings.Centre()
            self.dlgSettings.SetDoc(self.docCfg,False)
        node=self.docCfg.getChildForced(None,'settings')
        self.dlgSettings.SetNode(node)
        iRet=self.dlgSettings.ShowModal()
        if iRet>0:
            self.docCfg.AlignNode(node,iRec=3)
            self.docCfg.Save()
        #dlg.Destroy()
        event.Skip()

