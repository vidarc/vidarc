#Boa:FramePanel:ChildSashPanel

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

[wxID_CHILDSASHPANEL, wxID_CHILDSASHPANELCHECKLISTBOX1, 
 wxID_CHILDSASHPANELLISTCTRL1, wxID_CHILDSASHPANELNBINFO, 
 wxID_CHILDSASHPANELPANEL2, wxID_CHILDSASHPANELPANEL3, 
 wxID_CHILDSASHPANELPNMAIN, wxID_CHILDSASHPANELSLWINFO, 
 wxID_CHILDSASHPANELSLWMAIN, 
] = [wx.NewId() for _init_ctrls in range(9)]

class ChildSashPanel(wx.Panel):
    def _init_coll_nbInfo_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.panel2, select=False,
              text='Pages0')
        parent.AddPage(imageId=-1, page=self.panel3, select=True, text='Pages1')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_CHILDSASHPANEL, name=u'ChildSashPanel',
              parent=prnt, pos=wx.Point(176, 176), size=wx.Size(498, 332),
              style=0)
        self.SetClientSize(wx.Size(490, 305))
        self.Bind(wx.EVT_SIZE, self.OnChildSashPanelSize)

        self.slwInfo = wx.SashLayoutWindow(id=wxID_CHILDSASHPANELSLWINFO,
              name=u'slwInfo', parent=self, pos=wx.Point(0, 0), size=wx.Size(72,
              304), style=wx.SW_3D)
        self.slwInfo.SetDefaultSize((30,1000))
        self.slwInfo.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwInfo.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwInfo.SetAlignment(wx.LAYOUT_LEFT)
        self.slwInfo.SetExtraBorderSize(8)
        self.slwInfo.Bind(wx.EVT_SASH_DRAGGED_RANGE, self.OnSlwInfoSashDragged,
              id=wxID_CHILDSASHPANELSLWINFO)

        self.slwMain = wx.SashLayoutWindow(id=wxID_CHILDSASHPANELSLWMAIN,
              name=u'slwMain', parent=self, pos=wx.Point(72, 0),
              size=wx.Size(424, 32), style=wx.SW_3D)
        self.slwMain.SetDefaultSize((1000, 30))
        self.slwMain.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwMain.SetAlignment(wx.LAYOUT_TOP)
        self.slwMain.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwMain.SetBackgroundColour(wx.Colour(168, 162, 251))
        self.slwMain.Bind(wx.EVT_SASH_DRAGGED_RANGE, self.OnSlwMainSashDragged,
              id=wxID_CHILDSASHPANELSLWMAIN)

        self.checkListBox1 = wx.CheckListBox(choices=['test', 'll'],
              id=wxID_CHILDSASHPANELCHECKLISTBOX1, name='checkListBox1',
              parent=self.slwInfo, pos=wx.Point(2, 2), size=wx.Size(65, 300),
              style=0)

        self.pnMain = wx.Panel(id=wxID_CHILDSASHPANELPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(72, 32), size=wx.Size(408, 272),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.nbInfo = wx.Notebook(id=wxID_CHILDSASHPANELNBINFO, name=u'nbInfo',
              parent=self.pnMain, pos=wx.Point(8, 8), size=wx.Size(400, 256),
              style=0)
        self.nbInfo.SetConstraints(LayoutAnchors(self.nbInfo, True, True, True,
              True))

        self.panel2 = wx.Panel(id=wxID_CHILDSASHPANELPANEL2, name='panel2',
              parent=self.nbInfo, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.panel3 = wx.Panel(id=wxID_CHILDSASHPANELPANEL3, name='panel3',
              parent=self.nbInfo, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)
        self.panel3.SetAutoLayout(True)

        self.listCtrl1 = wx.ListCtrl(id=wxID_CHILDSASHPANELLISTCTRL1,
              name='listCtrl1', parent=self.panel3, pos=wx.Point(8, 8),
              size=wx.Size(384, 208), style=wx.LC_ICON)
        self.listCtrl1.SetConstraints(LayoutAnchors(self.listCtrl1, True, True,
              True, True))

        self._init_coll_nbInfo_Pages(self.nbInfo)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        #self.remainingSpace = wx.Panel(self, -1, style=wx.SUNKEN_BORDER)
        #self.remainingSpace = self.nbInfo
        self.remainingSpace = self.pnMain
    def OnChildSashPanelSize(self, event):
        wx.LayoutAlgorithm().LayoutWindow(self, self.remainingSpace)
        #event.Skip()
        pass

    def OnSlwMainSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            #self.log.write('drag is out of range')
            print 'exit'
            return

        eobj = event.GetEventObject()
        #if eobj is self.slwMain:
            #self.log.write('topwin received drag event')
        self.slwMain.SetDefaultSize((1000, event.GetDragRect().height))
            #self.slwMain.SetDefaultSize((event.GetDragRect().width, 1000))
        #if eobj is self.slwInfo:
        #    self.slwInfo.SetDefaultSize((event.GetDragRect().width, 1000))
        wx.LayoutAlgorithm().LayoutWindow(self, self.remainingSpace)
        self.remainingSpace.Refresh()
        #event.Skip()

    def OnSlwInfoSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            #self.log.write('drag is out of range')
            return
        self.slwInfo.SetDefaultSize((event.GetDragRect().width, 1000))        
        wx.LayoutAlgorithm().LayoutWindow(self, self.remainingSpace)
        self.remainingSpace.Refresh()
        #event.Skip()
