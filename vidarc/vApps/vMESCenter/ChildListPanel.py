#Boa:FramePanel:ChildListPanel

import wx

[wxID_CHILDLISTPANEL, wxID_CHILDLISTPANELLSTLOG, 
] = [wx.NewId() for _init_ctrls in range(2)]

class ChildListPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_CHILDLISTPANEL, name=u'ChildListPanel',
              parent=prnt, pos=wx.Point(304, 117), size=wx.Size(349, 249),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(341, 222))

        self.lstLog = wx.ListCtrl(id=wxID_CHILDLISTPANELLSTLOG, name=u'lstLog',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(328, 208),
              style=wx.LC_REPORT)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
