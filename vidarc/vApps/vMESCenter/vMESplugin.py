#----------------------------------------------------------------------------
# Name:         vMESplugin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060209
# CVS-ID:       $Id: vMESplugin.py,v 1.4 2006/06/07 10:12:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase

import string
try:
    import cStringIO
    from wx import BitmapFromImage as wxBitmapFromImage
    from wx import ImageFromStream as wxImageFromStream
    from wx import EmptyIcon as wxEmptyIcon
    GUI=1
except:
    GUI=0

class vMESplugin:
    def __init__(self,name,bFrame,dn):
        self.name=name
        self.bFrame=bFrame
        self.dn=dn
        self.domain=''
        self.plugin=None
        self.objClass=None
        self.img=None
    def GetWinName(self):
        strs=self.name.split('.')
        return strs[-1]
    def GetName(self):
        return self.name
    def IsFrame(self):
        return self.bFrame
    def GetDN(self):
        return self.dn
    def GetImage(self):
        self.__import__()
        return self.img
    def GetBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetImage())
        else:
            return None
    def GetIcon(self):
        icon = wxEmptyIcon()
        icon.CopyFromBitmap(self.GetBitmap())
        return icon
    def __import__(self):
        if self.objClass is None:
            strs=self.name.split('.')
            self.domain=strs[-2]
            self.plugin=__import__(self.name,[],[],[strs[-1],'getPluginImage'])
            self.objClass=getattr(self.plugin,strs[-1])
            self.img=getattr(self.plugin,'getPluginImage')()
    def create(self,parent,id,pos,size,style,name):
        self.__import__()
        try:
            parent.SetIcon(self.GetIcon())
        except:
            pass
        return self.objClass(parent,id,pos,size,style,name)
    def createFrame(self,parent,id,pos,size,style,name):
        self.__import__()
        return self.objClass(parent,id,pos,size,style,name)
    def getPossibleLang(self):
        return vtLgBase.getPossibleLang(self.domain,self.dn)
    def setLang(self,lang):
        vtLgBase.setPluginLang(self.domain,self.dn,lang)
