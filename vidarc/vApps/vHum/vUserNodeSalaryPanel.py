#Boa:FramePanel:vUserNodeSalaryPanel
#----------------------------------------------------------------------------
# Name:         vUserNodeSalaryPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vUserNodeSalaryPanel.py,v 1.6 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.ext.state.veStateInput
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeDatedValues
import wx.lib.buttons

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import sys

    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.xml.vtXmlDomConsumerLang import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VUSERNODESALARYPANEL, wxID_VUSERNODESALARYPANELCHCCLASSIFICATIONCUR, 
 wxID_VUSERNODESALARYPANELCHCCLASSIFICATIONNEXT, 
 wxID_VUSERNODESALARYPANELDTECLASSIFICATIONNEXT, 
 wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONCUR, 
 wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONNEXT, 
 wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONNEXTAT, 
 wxID_VUSERNODESALARYPANELLBLDATE, wxID_VUSERNODESALARYPANELLBLSALARY, 
 wxID_VUSERNODESALARYPANELLBLSTATE, wxID_VUSERNODESALARYPANELVIDATED, 
 wxID_VUSERNODESALARYPANELVISALARY, wxID_VUSERNODESALARYPANELVISALARYFULL, 
 wxID_VUSERNODESALARYPANELVISTATE, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vUserNodeSalaryPanel(wx.Panel,vtXmlNodePanel,vtXmlDomConsumerLang):
    VERBOSE=0
    def _init_coll_bxsClassificationCur_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblClassificationCur, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcClassificationCur, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsClassificationNext_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblClassificationNext, 1, border=0,
              flag=wx.EXPAND)
        parent.AddWindow(self.chcClassificationNext, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDated, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsSalary, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsClassificationCur, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsClassificationNext, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsClassificationNextBy, 1, border=0,
              flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsState, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsClassificationNextBy_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblClassificationNextAt, 1, border=0,
              flag=wx.EXPAND)
        parent.AddWindow(self.dteClassificationNext, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSalary_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSalary, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viSalary, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.viSalaryFull, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsDated_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDated, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsState_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblState, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viState, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=8, vgap=4)

        self.bxsDated = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSalary = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsClassificationCur = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsClassificationNext = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsClassificationNextBy = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsState = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsDated_Items(self.bxsDated)
        self._init_coll_bxsSalary_Items(self.bxsSalary)
        self._init_coll_bxsClassificationCur_Items(self.bxsClassificationCur)
        self._init_coll_bxsClassificationNext_Items(self.bxsClassificationNext)
        self._init_coll_bxsClassificationNextBy_Items(self.bxsClassificationNextBy)
        self._init_coll_bxsState_Items(self.bxsState)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERNODESALARYPANEL,
              name=u'vUserNodeSalaryPanel', parent=prnt, pos=wx.Point(405, 257),
              size=wx.Size(378, 331), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(370, 304))

        self.lblDate = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLDATE,
              label=_(u'valid by'), name=u'lblDate', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(123, 24), style=wx.ALIGN_RIGHT)

        self.viDated = vidarc.tool.time.vtTimeDatedValues.vtTimeDatedValues(id=wxID_VUSERNODESALARYPANELVIDATED,
              parent=self, pos=wx.Point(127, 0), size=wx.Size(242, 24),
              style=0)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_ADDED,
              self.OnViDatedVttimeDatedValuesAdded,
              id=wxID_VUSERNODESALARYPANELVIDATED)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_DELETED,
              self.OnViDatedVttimeDatedValuesDeleted,
              id=wxID_VUSERNODESALARYPANELVIDATED)

        self.lblSalary = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLSALARY,
              label=_(u'salary'), name=u'lblSalary', parent=self,
              pos=wx.Point(0, 40), size=wx.Size(123, 22), style=wx.ALIGN_RIGHT)
        self.lblSalary.SetMinSize(wx.Size(-1, -1))

        self.viSalary = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VUSERNODESALARYPANELVISALARY,
              integer_width=6, maximum=20000.0, minimum=0.0, name=u'viSalary',
              parent=self, pos=wx.Point(127, 40), size=wx.Size(119, 22),
              style=0)

        self.viSalaryFull = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VUSERNODESALARYPANELVISALARYFULL,
              integer_width=6, maximum=2000, minimum=0.0, name=u'viSalaryFull',
              parent=self, pos=wx.Point(250, 40), size=wx.Size(119, 22),
              style=0)
        self.viSalaryFull.Enable(False)

        self.lblClassificationCur = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONCUR,
              label=_(u'current classification'), name=u'lblClassificationCur',
              parent=self, pos=wx.Point(0, 66), size=wx.Size(123, 21),
              style=wx.ALIGN_RIGHT)
        self.lblClassificationCur.SetMinSize(wx.Size(-1, -1))

        self.chcClassificationCur = wx.Choice(choices=['---'],
              id=wxID_VUSERNODESALARYPANELCHCCLASSIFICATIONCUR,
              name=u'chcClassificationCur', parent=self, pos=wx.Point(127, 66),
              size=wx.Size(242, 21), style=0)
        self.chcClassificationCur.SetMinSize(wx.Size(-1, -1))

        self.lblClassificationNext = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONNEXT,
              label=_(u'next classification'), name=u'lblClassificationNext',
              parent=self, pos=wx.Point(0, 91), size=wx.Size(123, 21),
              style=wx.ALIGN_RIGHT)
        self.lblClassificationNext.SetMinSize(wx.Size(-1, -1))

        self.chcClassificationNext = wx.Choice(choices=['---'],
              id=wxID_VUSERNODESALARYPANELCHCCLASSIFICATIONNEXT,
              name=u'chcClassificationNext', parent=self, pos=wx.Point(127, 91),
              size=wx.Size(242, 21), style=0)
        self.chcClassificationNext.SetMinSize(wx.Size(-1, -1))

        self.lblClassificationNextAt = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLCLASSIFICATIONNEXTAT,
              label=_(u'by'), name=u'lblClassificationNextAt', parent=self,
              pos=wx.Point(0, 116), size=wx.Size(123, 24),
              style=wx.ALIGN_RIGHT)
        self.lblClassificationNextAt.SetMinSize(wx.Size(-1, -1))

        self.dteClassificationNext = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VUSERNODESALARYPANELDTECLASSIFICATIONNEXT,
              name=u'dteClassificationNext', parent=self, pos=wx.Point(127,
              116), size=wx.Size(242, 24), style=0)

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VUSERNODESALARYPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(127, 156),
              size=wx.Size(242, 24), style=0)

        self.lblState = wx.StaticText(id=wxID_VUSERNODESALARYPANELLBLSTATE,
              label=_(u'state'), name=u'lblState', parent=self, pos=wx.Point(0,
              156), size=wx.Size(123, 24), style=wx.ALIGN_RIGHT)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        vtXmlDomConsumerLang.__init__(self)
        
        self.viDated.SetCB(self.__showDated__)
        self.viSalary.SetTagName('salary_value')
        self.dteClassificationNext.SetEnableMark(False)
        self.viState.SetNodeAttr('salary','salState')
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def IsBusy(self):
        if vtXmlNodePanel.IsBusy(self):
            return True
        return False
    def Stop(self):
        vtXmlNodePanel.Stop(self)
    def __showClassification__(self):
        self.chcClassificationCur.Clear()
        self.chcClassificationNext.Clear()
        self.chcClassificationCur.Append('---')
        self.chcClassificationNext.Append('---')
        if self.doc is not None:
            l=self.doc.GetSalarySchemeClassificationLst()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(l),self)
            if l is not None:
                for tup in l:
                    self.chcClassificationCur.Append(tup[0],tup[1])
                    self.chcClassificationNext.Append(tup[0],tup[1])
    def ClearLang(self):
        self.chcClassificationCur.Clear()
        self.chcClassificationNext.Clear()
        self.chcClassificationCur.Append('---')
        self.chcClassificationNext.Append('---')
    def UpdateLang(self):
        self.__showClassification__()
    def OnBuild(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.UpdateLang()
    def __clearDated__(self):
        self.viSalary.Clear()
        self.chcClassificationCur.SetSelection(0)
        self.chcClassificationNext.SetSelection(0)
        self.dteClassificationNext.Clear()
    def __enableDated__(self,flag):
        self.viSalary.Enable(flag)
        self.chcClassificationCur.Enable(flag)
        self.chcClassificationNext.Enable(flag)
        self.dteClassificationNext.Enable(flag)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        
        # add code here
        self.viDated.Clear()
        self.__clearDated__()
        self.viState.Clear()
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.doc is not None:
            if bNet:
                EVT_NET_XML_BUILD_TREE_DISCONNECT(self.doc,self.OnBuild)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        if self.doc is not None:
            if bNet:
                EVT_NET_XML_BUILD_TREE(self.doc,self.OnBuild)
        # add code here
        self.viDated.SetDoc(doc)
        self.viState.SetDoc(doc)
    def __showDated__(self,node,*args,**kwargs):
        if self.VERBOSE:
            vtLog.CallStack('')
            print node
            print args
            print kwargs
        if node is None:
            self.__clearDated__()
            self.__enableDated__(False)
        else:
            fMin,fMax=self.objRegNode.GetSalaryLimit(node)
            if self.VERBOSE:
                print 'salary limit',fMin,fMax
            self.viSalaryFull.SetValue(fMin)
            self.viSalaryFull.SetLimit(fMin,fMax)
            
            fMin,fMax=self.objRegNode.GetSalaryLimitWorkingTimes(node)
            self.viSalary.SetNode(node,self.doc)
            self.viSalary.SetLimit(fMin,fMax)
            iClass,sName,idx=self.objRegNode.GetClassificationCurFull(node)
            if self.VERBOSE:
                print 'cur',iClass,sName,idx
            try:
                self.chcClassificationCur.SetSelection(idx+1)
            except:
                pass
            iClass,sName,idx,sDt=self.objRegNode.GetClassificationNextFull(node)
            if self.VERBOSE:
                print 'nxt',iClass,sName,idx,sDt
            try:
                self.chcClassificationNext.SetSelection(idx+1)
                self.dteClassificationNext.SetValueStr(sDt)
            except:
                pass
            
            self.__enableDated__(True)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.__showClassification__()
            self.viDated.SetNode(node)
            self.viState.SetNode(self.node)
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            nodeDated=self.viDated.GetNode(node)
            if self.VERBOSE:
                print nodeDated
            if nodeDated is not None:
                self.viSalary.GetNode(nodeDated,self.doc)
                idx=self.chcClassificationCur.GetSelection()
                try:
                    id=self.chcClassificationCur.GetClientData(idx)
                except:
                    id=-1
                if self.VERBOSE:
                    print 'cur',idx,id
                self.objRegNode.SetClassificationCur(nodeDated,id)
                idx=self.chcClassificationNext.GetSelection()
                try:
                    id=self.chcClassificationNext.GetClientData(idx)
                except:
                    id=-1
                sDt=self.dteClassificationNext.GetValueStr()
                if self.VERBOSE:
                    print 'nxr',idx,id,sDt
                self.objRegNode.SetClassificationNext(nodeDated,id,sDt)
            self.__showDated__(nodeDated)
            self.viState.GetNode(node)
            
            self.viState.SetNode(node)  # Refresh
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viDated.Enable(False)
            self.viState.Enable(False)
            self.__enableDated__(False)
        else:
            # add code here
            self.viDated.Enable(True)
            self.viState.Enable(True)

    def OnViDatedVttimeDatedValuesAdded(self, event):
        self.__enableDated__(True)
        event.Skip()

    def OnViDatedVttimeDatedValuesDeleted(self, event):
        self.__enableDated__(False)
        event.Skip()
