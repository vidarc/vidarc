#Boa:FramePanel:vUserInfoSecurityPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoSecurityPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoSecurityPanel.py,v 1.11 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import cStringIO,sha
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import __config__
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.tool.time.vTimeTimeEdit import *
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import string,sys,calendar

    import __config__
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VUSERINFOSECURITYPANEL, wxID_VUSERINFOSECURITYPANELCHKLSTGRP, 
 wxID_VUSERINFOSECURITYPANELLBLPASSWD, wxID_VUSERINFOSECURITYPANELLBLSECLV, 
 wxID_VUSERINFOSECURITYPANELSPNSECLEVEL, wxID_VUSERINFOSECURITYPANELTGBPASSWD, 
 wxID_VUSERINFOSECURITYPANELTXTPASSWD, 
] = [wx.NewId() for _init_ctrls in range(7)]

import images_hum_tree

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUserInfoSecurityPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsSecLv_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSecLv, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.spnSecLevel, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(30, 30), border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSecLv, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsPasswd, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.chklstGrp, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsPasswd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswd, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.tgbPasswd, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsSecLv = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsSecLv_Items(self.bxsSecLv)
        self._init_coll_bxsPasswd_Items(self.bxsPasswd)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERINFOSECURITYPANEL,
              name=u'vUserInfoSecurityPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(258, 317), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(250, 290))
        self.SetAutoLayout(True)

        self.lblSecLv = wx.StaticText(id=wxID_VUSERINFOSECURITYPANELLBLSECLV,
              label=_(u'Security Level'), name=u'lblSecLv', parent=self,
              pos=wx.Point(0, 4), size=wx.Size(106, 30), style=wx.ALIGN_RIGHT)
        self.lblSecLv.SetMinSize(wx.Size(-1, -1))

        self.spnSecLevel = wx.SpinCtrl(id=wxID_VUSERINFOSECURITYPANELSPNSECLEVEL,
              initial=-1, max=32, min=-1, name=u'spnSecLevel', parent=self,
              pos=wx.Point(110, 4), size=wx.Size(106, 30),
              style=wx.SP_ARROW_KEYS)
        self.spnSecLevel.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VUSERINFOSECURITYPANELLBLPASSWD,
              label=_(u'password'), name=u'lblPasswd', parent=self, pos=wx.Point(0,
              38), size=wx.Size(106, 30), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtPasswd = wx.TextCtrl(id=wxID_VUSERINFOSECURITYPANELTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(110, 38),
              size=wx.Size(106, 30), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))
        self.txtPasswd.Bind(wx.EVT_TEXT, self.OnTxtPasswdText,
              id=wxID_VUSERINFOSECURITYPANELTXTPASSWD)

        self.chklstGrp = wx.CheckListBox(choices=[],
              id=wxID_VUSERINFOSECURITYPANELCHKLSTGRP, name=u'chklstGrp',
              parent=self, pos=wx.Point(0, 72), size=wx.Size(250, 218),
              style=0)
        self.chklstGrp.SetMinSize(wx.Size(-1, -1))

        self.tgbPasswd = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VUSERINFOSECURITYPANELTGBPASSWD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgbPasswd', parent=self,
              pos=wx.Point(220, 38), size=wx.Size(30, 30), style=0)
        self.tgbPasswd.Bind(wx.EVT_BUTTON, self.OnTgbPasswdButton,
              id=wxID_VUSERINFOSECURITYPANELTGBPASSWD)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        self.doc=None
        self.node=None
        self.bModified=False
        self.passwd=None
        self.grps=[]
        
        self.tgbPasswd.SetBitmapLabel(vtArt.getBitmap(vtArt.SecUnlocked))
        self.tgbPasswd.SetBitmapSelected(vtArt.getBitmap(vtArt.SecLocked))
        #lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self.dteStart, wx.Top, 0)
        #lc.left.SameAs      (self.dteStart, wx.Right, 10)
        #lc.bottom.AsIs      ()
        #lc.right.AsIs       ()
        #self.lstDays.SetConstraints(lc)
        self.__showPasswd__()
    def __showPasswd__(self):
        flag=False
        flagAdmin=False
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print 'loggedin',self.doc.GetLoggedInSecLv(),'limit',__config__.SEC_LEVEL_LIMIT_ADMIN
            if self.node is not None:
                try:
                    if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                        flag=True
                        flagAdmin=True
                    else:
                        if self.doc.IsLoggedInSelf(self.node):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'IsLoggedInSel',self)
                            flag=True
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
        self.tgbPasswd.Enable(flag)
        self.txtPasswd.Enable(flag)
        self.spnSecLevel.Enable(flagAdmin)
        self.chklstGrp.Enable(flagAdmin)
        
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.SetModified(False)
            self.passwd=None
            self.grps=[]
            self.txtPasswd.SetValue('')
            self.tgbPasswd.SetToggle(False)
            self.chklstGrp.Clear()
            self.spnSecLevel.SetValue(-1)
            #self.lstRanges.DeleteAllItems()
            self.selectedIdx=-1
            self.__showPasswd__()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__showPasswd__()
            self.grps=[]
            grpsNode=self.doc.getChild(self.doc.getBaseNode(),'groups')
            self.chklstGrp.Clear()
            if grpsNode is not None:
                for c in self.doc.getChilds(grpsNode,'group'):
                    id=self.doc.getKey(c)
                    sName=self.doc.getNodeText(c,'name')
                    self.grps.append((sName,id))
            def compFunc(a,b):
                return cmp(a[0],b[0])
            self.grps.sort(compFunc)
            dGrps={}
            i=0
            for sName,id in self.grps:
                try:
                    dGrps[long(id)]=i
                    self.chklstGrp.Append(sName)
                    i+=1
                except:
                    pass
            try:
                i=int(self.doc.getNodeText(node,'sec_level'))
            except:
                i=-1
            self.spnSecLevel.SetValue(i)
            
            self.passwd=self.doc.getNodeText(node,'passwd')
            if len(self.passwd)==0:
                self.passwd=None
                self.tgbPasswd.SetToggle(False)
            else:
                self.tgbPasswd.SetToggle(True)
            # setup attributes
            for c in self.doc.getChilds(node,'grp'):
                try:
                    i=dGrps[long(self.doc.getAttribute(c,'fid'))]
                    self.chklstGrp.Check(i,True)
                except:
                    pass
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            bModSec=True
            bModPasswd=True
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                #self.SetModified(False)
                #return
                bModSec=False
                bModPasswd=False
            if self.doc.IsLoggedInSelf(node):
                bModPasswd=True
            if bModPasswd:
                if self.passwd is None:
                    if self.tgbPasswd.GetToggle():
                        passwd=self.txtPasswd.GetValue()
                        if len(passwd)>0:
                            m=sha.new()
                            m.update(passwd)
                            self.doc.setNodeText(node,'passwd',m.hexdigest())
                        else:
                            self.doc.setNodeText(node,'passwd','')
                    else:
                        self.doc.setNodeText(node,'passwd','')
                else:
                    self.doc.setNodeText(node,'passwd',self.passwd)
            if bModSec:
                self.doc.setNodeText(node,'sec_level','%d'%self.spnSecLevel.GetValue())
                childs=self.doc.getChilds(node,'grp')
                iChilds=len(childs)
                grps=[]
                for i in range(self.chklstGrp.GetCount()):
                    if self.chklstGrp.IsChecked(i):
                        grps.append(i)
                iLen=len(grps)
                for i in range(iLen):
                    try:
                        child=childs[i]
                    except:
                        child=self.doc.createSubNode(node,'grp')
                    self.doc.setAttribute(child,'fid',self.grps[grps[i]][1])
                    #self.doc.setNodeText(child,'name',self.grps[grps[i]][0])
                for i in range(iLen,iChilds):
                    self.doc.deleteNode(childs[i],node)
        except:
            self.__logTB__()
    def GetGroupLst(self):
        grps=[]
        for i in range(self.chklstGrp.GetCount()):
            if self.chklstGrp.IsChecked(i):
                grps.append(self.grps[i][1])
        return grps
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.spnSecLevel.Enable(False)
            self.chklstGrp.Enable(False)
            self.tgbPasswd.Enable(False)
            self.txtPasswd.Enable(False)
        else:
            self.__showPasswd__()
            #self.spnSecLevel.Enable(True)
            #self.chklstGrp.Enable(True)

    def OnTxtPasswdText(self, event):
        self.passwd=None
        if len(self.txtPasswd.GetValue())>0:
            self.tgbPasswd.SetToggle(True)
        event.Skip()

    def OnTgbPasswdButton(self, event):
        if self.tgbPasswd.GetToggle()==False:
            self.passwd=None
            self.txtPasswd.SetValue('')
        event.Skip()
