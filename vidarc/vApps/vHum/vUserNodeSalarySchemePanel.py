#Boa:FramePanel:vUserNodeSalarySchemePanel
#----------------------------------------------------------------------------
# Name:         vNodeXXXPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vUserNodeSalarySchemePanel.py,v 1.5 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputAttrValMLPanel
import vidarc.tool.xml.vtXmlNodeTagPanel
import vidarc.tool.input.vtInputGridSmall
import wx.lib.buttons

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import sys

    from vidarc.tool.xml.vtXmlNodePanel import *

    import  vidarc.vApps.vHum.images_data as img_data

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VUSERNODESALARYSCHEMEPANEL, wxID_VUSERNODESALARYSCHEMEPANELGRDSCHEME, 
 wxID_VUSERNODESALARYSCHEMEPANELNBDATA, wxID_VUSERNODESALARYSCHEMEPANELPNDATA, 
 wxID_VUSERNODESALARYSCHEMEPANELPNSETUP, 
 wxID_VUSERNODESALARYSCHEMEPANELVISETUP, wxID_VUSERNODESALARYSCHEMEPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vUserNodeSalarySchemePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsSetup_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viSetup, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.nbData, 1, border=4,
              flag=wx.BOTTOM | wx.RIGHT | wx.EXPAND | wx.TOP | wx.LEFT)

    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.grdScheme, 1, border=0, flag=wx.EXPAND)

    def _init_coll_nbData_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnData, select=False, text=_(u'salary'))
        parent.AddPage(imageId=-1, page=self.pnSetup, select=True,
              text=_(u'setup'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsSetup = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsData_Items(self.bxsData)
        self._init_coll_bxsSetup_Items(self.bxsSetup)

        self.SetSizer(self.fgsData)
        self.pnData.SetSizer(self.bxsData)
        self.pnSetup.SetSizer(self.bxsSetup)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERNODESALARYSCHEMEPANEL,
              name=u'vUserNodeSalarySchemePanel', parent=prnt, pos=wx.Point(405,
              257), size=wx.Size(529, 350), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(521, 323))

        self.viTag = vidarc.tool.xml.vtXmlNodeTagPanel.vtXmlNodeTagPanel(id=wxID_VUSERNODESALARYSCHEMEPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(0, 0), size=wx.Size(521,
              93), style=0)

        self.nbData = wx.Notebook(id=wxID_VUSERNODESALARYSCHEMEPANELNBDATA,
              name=u'nbData', parent=self, pos=wx.Point(4, 105),
              size=wx.Size(513, 214), style=0)
        self.nbData.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED,
              self.OnNbDataNotebookPageChanged,
              id=wxID_VUSERNODESALARYSCHEMEPANELNBDATA)

        self.pnData = wx.Panel(id=wxID_VUSERNODESALARYSCHEMEPANELPNDATA,
              name=u'pnData', parent=self.nbData, pos=wx.Point(0, 0),
              size=wx.Size(505, 188), style=wx.TAB_TRAVERSAL)

        self.pnSetup = wx.Panel(id=wxID_VUSERNODESALARYSCHEMEPANELPNSETUP,
              name=u'pnSetup', parent=self.nbData, pos=wx.Point(0, 0),
              size=wx.Size(505, 188), style=wx.TAB_TRAVERSAL)

        self.grdScheme = vidarc.tool.input.vtInputGridSmall.vtInputGridSmall(id=wxID_VUSERNODESALARYSCHEMEPANELGRDSCHEME,
              name=u'grdScheme', parent=self.pnData, pos=wx.Point(0, 0),
              size=wx.Size(505, 188))

        self.viSetup = vidarc.tool.input.vtInputAttrValMLPanel.vtInputAttrValMLPanel(id=wxID_VUSERNODESALARYSCHEMEPANELVISETUP,
              name=u'viSetup', parent=self.pnSetup, pos=wx.Point(0, 0),
              size=wx.Size(505, 188), style=0)

        self._init_coll_nbData_Pages(self.nbData)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self.doc=None
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        
        self.lLblRow=[]
        self.lLblCol=[]
        self.lVal=[]
        self.grdScheme.SetColumnSize([150,100])
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(img_data.getUserInfoSalaryBitmap())
        self.imgLst.Add(vtArt.getBitmap(vtArt.Settings))
        self.nbData.SetImageList(self.imgLst)
        for i in xrange(0,2):
            self.nbData.SetPageImage(i,i)
            
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            # add code here
            self.viTag.Clear()
            self.viSetup.Clear()
            self.lLblRow=[]
            self.lLblCol=[]
            self.lVal=[]
            self.grdScheme.SetUp(self.lVal,self)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def ClearInt(self):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            vtXmlNodePanel.Clear(self)
            # add code here
            self.viTag.ClearInt()
            self.viSetup.ClearInt()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def ClearOld(self):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            vtXmlNodePanel.Clear(self)
            # add code here
            self.viTag.Clear()
            self.viSetup.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viSetup.SetDoc(doc)
        self.objReg=self.doc.GetRegisteredNode('salaryScheme')
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viSetup.SetNode(self.node)
            self.lVal=self.objReg.GetSalaryLst(self.node)
            if self.VERBOSE:
                vtLog.pprint(self.lVal)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            self.__setup__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viSetup.GetNode(node)
            self.grdScheme.Get()
            self.objReg.SetSalaryLst(node,self.lVal)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viSetup.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viSetup.Enable(True)
    def GetRowColType(self,row,col):
        """required for vtInputGrid"""
        if col==-1:
            return self.doc,None,'string'
        try:
            if self.node is not None:
                nodeTmp=self.doc.getChild(self.node,'attributes')
                sType=self.doc.getNodeText(nodeTmp,'type')
                return self.doc,nodeTmp,sType
        except:
            pass
        return self.doc,None,'float'
    def GetRowColValue(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lVal[row][col]
        except:
            return None
    def SetRowColValue(self,row,col,val):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d val:%s'%(row,col,str(val)),self)
        try:
            self.lVal[row][col]=val
        except:
            pass
    def GetColLabelValue(self,col,row=-1):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lLblCol[col]
        except:
            return ''
    def GetRowLabelValue(self,row):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d'%(row),self)
        try:
            return self.lLblRow[row]
        except:
            return ''
    def GetDataNodes(self):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        return self.lVal
    def GetDataNode(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        return None
    def __setup__(self):
        self.dAttrs=self.viSetup.GetValueDict()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(self.dAttrs),self)
        iRow=0
        iCol=0
        dRow={}
        dCol={}
        for k in self.dAttrs.keys():
            strs=k.split('_')
            try:
                if strs[0]=='row':
                    i=int(strs[1])
                    dRow[i]=self.dAttrs[k]
                    if i>iRow:
                        iRow=i
                elif strs[0]=='col':
                    i=int(strs[1])
                    dCol[i]=self.dAttrs[k]
                    if i>iCol:
                        iCol=i
            except:
                pass
        
        ll=[]
        self.lLblRow=[]
        self.lLblCol=[]
        for i in xrange(0,iRow+1):
            try:
                self.lLblRow.append(dRow[i])
            except:
                self.lLblRow.append(str(i))
            l=[]
            for j in xrange(0,iCol+1):
                try:
                    l.append(self.lVal[i][j])
                except:
                    l.append('0.0')
            ll.append(l)
        lCol=[150]
        for j in xrange(0,iCol+1):
            try:
                self.lLblCol.append(dCol[j])
            except:
                self.lLblCol.append(str(j))
            lCol.append(100)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pprint(ll),self)
        self.lVal=ll
        self.grdScheme.SetColumnSize(lCol)
        self.grdScheme.SetUp(self.lVal,self)
    def OnNbDataNotebookPageChanged(self, event):
        event.Skip()
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel==0:
            self.__setup__()
        else:
            self.grdScheme.Get()
