#Boa:Dialog:vgdXmlDlgEditUsr
#----------------------------------------------------------------------------
# Name:         vXmlEditUsrDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlEditUsrDialog.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM

def create(parent):
    return vgdXmlDlgEditUsr(parent)

[wxID_VGPXMLDLGRENAME, wxID_VGPXMLDLGRENAMEGCBBCANCEL, 
 wxID_VGPXMLDLGRENAMEGCBBOK, wxID_VGPXMLDLGRENAMELBLCURNAME, 
 wxID_VGPXMLDLGRENAMELBLNEWNAME, wxID_VGPXMLDLGRENAMETXTCURNAME, 
 wxID_VGPXMLDLGRENAMETXTNEWNAME, 
] = map(lambda _init_ctrls: wx.NewId(), range(7))

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)


[wxID_VGDXMLDLGEDITUSR, wxID_VGDXMLDLGEDITUSRGCBBCANCEL, 
 wxID_VGDXMLDLGEDITUSRGCBBOK, wxID_VGDXMLDLGEDITUSRLBLNAME, 
 wxID_VGDXMLDLGEDITUSRLBLSTARTDT, wxID_VGDXMLDLGEDITUSRSTATICTEXT1, 
 wxID_VGDXMLDLGEDITUSRTXTUSRNAME, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vgdXmlDlgEditUsr(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDXMLDLGEDITUSR,
              name=u'vgdXmlDlgEditUsr', parent=prnt, pos=wx.Point(334, 226),
              size=wx.Size(226, 166), style=wxDEFAULT_DIALOG_STYLE,
              title=u'vgd xml-Node Edit User')
        self.SetClientSize(wx.Size(218, 139))

        self.lblName = wx.StaticText(id=wxID_VGDXMLDLGEDITUSRLBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(26, 13), style=0)

        self.txtUsrName = wx.TextCtrl(id=wxID_VGDXMLDLGEDITUSRTXTUSRNAME,
              name=u'txtUsrName', parent=self, pos=wx.Point(80, 8),
              size=wx.Size(128, 21), style=0, value=u'')
        self.txtUsrName.Enable(True)

        self.lblStartDT = wx.StaticText(id=wxID_VGDXMLDLGEDITUSRLBLSTARTDT,
              label=u'start date time', name=u'lblStartDT', parent=self,
              pos=wx.Point(6, 40), size=wx.Size(66, 13), style=0)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDXMLDLGEDITUSRGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk', parent=self,
              pos=wx.Point(16, 96), size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDXMLDLGEDITUSRGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDXMLDLGEDITUSRGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'gcbbCancel',
              parent=self, pos=wx.Point(136, 96), size=wx.Size(76, 30),
              style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDXMLDLGEDITUSRGCBBCANCEL)

        self.staticText1 = wx.StaticText(id=wxID_VGDXMLDLGEDITUSRSTATICTEXT1,
              label=u'end date time', name='staticText1', parent=self,
              pos=wx.Point(6, 64), size=wx.Size(64, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)

        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)

        self.zdtlStartGM=vTimeDateTimeGM.wxDateTimeGM(self,wx.NewId(),
                    pos=(80,40),size = (130,22))
        
        self.zdtlEndGM=vTimeDateTimeGM.wxDateTimeGM(self,wx.NewId(),
                    pos=(80,70),size = (130,22))

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def SetNode(self,node):
        sUsrName=vtXmlDomTree.getNodeText(node,'name')
        sStartDT=vtXmlDomTree.getNodeText(node,'startdatetime')
        sEndDT=vtXmlDomTree.getNodeText(node,'enddatetime')
        
        self.txtUsrName.SetValue(sUsrName)
        self.zdtlStartGM.SetValue(sStartDT)
        self.zdtlEndGM.SetValue(sEndDT)
        
    def GetNode(self,node):
        sUsrName=self.txtUsrName.GetValue()
        sStartDT=self.zdtlStartGM.GetValue()
        sEndDT=self.zdtlEndGM.GetValue()
        
        vtXmlDomTree.setNodeText(node,'name',sUsrName)
        vtXmlDomTree.setNodeText(node,'startdatetime',sStartDT)
        vtXmlDomTree.setNodeText(node,'enddatetime',sEndDT)
        
        return sUsrName
