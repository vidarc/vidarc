#Boa:FramePanel:vUserInfoWorkingTimesPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoWorkingTimesPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoWorkingTimesPanel.py,v 1.12 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeDatedValues
import vidarc.tool.time.vTimeSelectorHoursPanel
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.time.vtTimeTimeEdit
import wx.grid
import wx.lib.buttons
import cStringIO
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.tool.time.vTimeSelectorHoursPanel import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import __config__

    import calendar
    import string,sys
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

if VERBOSE>0:
    import pprint
    pp=pprint.PrettyPrinter(indent=2)

[wxID_VUSERINFOWORKINGTIMESPANEL, wxID_VUSERINFOWORKINGTIMESPANELCBQUICKGEN, 
 wxID_VUSERINFOWORKINGTIMESPANELCHCSTARTDAY, 
 wxID_VUSERINFOWORKINGTIMESPANELCHCTYPE, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLDATE, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLDAYS, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLEXTRA, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLHOURS, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLPERCENTAGE, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLSTART, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLSTARTDAY, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLTYPE, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLVAC, 
 wxID_VUSERINFOWORKINGTIMESPANELLBLVACEXTRA, 
 wxID_VUSERINFOWORKINGTIMESPANELSPDAYS, 
 wxID_VUSERINFOWORKINGTIMESPANELSPEXTRA, 
 wxID_VUSERINFOWORKINGTIMESPANELSPPERCENT, 
 wxID_VUSERINFOWORKINGTIMESPANELSPSTART, wxID_VUSERINFOWORKINGTIMESPANELSPVAC, 
 wxID_VUSERINFOWORKINGTIMESPANELSPVACEXTRA, 
 wxID_VUSERINFOWORKINGTIMESPANELVGTMHOURS, 
 wxID_VUSERINFOWORKINGTIMESPANELVIDATED, 
 wxID_VUSERINFOWORKINGTIMESPANELVIHOURSWEEK, 
] = [wx.NewId() for _init_ctrls in range(23)]

import images_hum_tree
import images

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUserInfoWorkingTimesPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    MAP_TYPE_IDX=['normal','over','flex']
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(6)

    def _init_coll_bxsPercent_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPercentage, 2, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.spPercent, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblExtra, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.spExtra, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsDated_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viDated, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsVac_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblVac, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.spVac, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblVacExtra, 2, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.spVacExtra, 1, border=3, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsQuick_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHours, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viHoursWeek, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblStart, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.spStart, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblStartDay, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.chcStartDay, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblDays, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.spDays, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.cbQuickGen, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsType_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblType, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcType, 2, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDated, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsPercent, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsVac, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsType, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsQuick, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vgTmHours, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsType = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPercent = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDated = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsVac = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsQuick = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=4)

        self._init_coll_bxsType_Items(self.bxsType)
        self._init_coll_bxsPercent_Items(self.bxsPercent)
        self._init_coll_bxsDated_Items(self.bxsDated)
        self._init_coll_bxsVac_Items(self.bxsVac)
        self._init_coll_bxsQuick_Items(self.bxsQuick)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERINFOWORKINGTIMESPANEL,
              name=u'vUserInfoWorkingTimesPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(569, 327), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(561, 300))
        self.SetAutoLayout(True)

        self.lblDate = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLDATE,
              label=_(u'valid by'), name=u'lblDate', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(187, 24), style=wx.ALIGN_RIGHT)
        self.lblDate.SetMinSize(wx.Size(-1, -1))

        self.viDated = vidarc.tool.time.vtTimeDatedValues.vtTimeDatedValues(id=wxID_VUSERINFOWORKINGTIMESPANELVIDATED,
              name=u'viDated', parent=self, pos=wx.Point(191, 0),
              size=wx.Size(370, 24), style=0)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_DELETED,
              self.OnViDatedVttimeDatedValuesDeleted,
              id=wxID_VUSERINFOWORKINGTIMESPANELVIDATED)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_ADDED,
              self.OnViDatedVttimeDatedValuesAdded,
              id=wxID_VUSERINFOWORKINGTIMESPANELVIDATED)

        self.lblPercentage = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLPERCENTAGE,
              label=_(u'percentage'), name=u'lblPercentage', parent=self,
              pos=wx.Point(0, 40), size=wx.Size(187, 21), style=wx.ALIGN_RIGHT)
        self.lblPercentage.SetMinSize(wx.Size(-1, -1))

        self.spPercent = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPPERCENT,
              initial=0, max=100, min=0, name=u'spPercent', parent=self,
              pos=wx.Point(191, 40), size=wx.Size(89, 21),
              style=wx.SP_ARROW_KEYS)
        self.spPercent.SetMinSize(wx.Size(-1, -1))

        self.lblExtra = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLEXTRA,
              label=_(u'extra hours [hours/month]'), name=u'lblExtra',
              parent=self, pos=wx.Point(284, 40), size=wx.Size(183, 21),
              style=wx.ALIGN_RIGHT)

        self.spExtra = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPEXTRA,
              initial=0, max=100, min=-1, name=u'spExtra', parent=self,
              pos=wx.Point(471, 40), size=wx.Size(89, 21),
              style=wx.SP_ARROW_KEYS)

        self.lblVac = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLVAC,
              label=_(u'vacation [days/year]'), name=u'lblVac', parent=self,
              pos=wx.Point(4, 65), size=wx.Size(183, 21), style=wx.ALIGN_RIGHT)
        self.lblVac.SetMinSize(wx.Size(-1, -1))

        self.spVac = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPVAC,
              initial=0, max=365, min=0, name=u'spVac', parent=self,
              pos=wx.Point(191, 65), size=wx.Size(89, 21),
              style=wx.SP_ARROW_KEYS)
        self.spVac.SetMinSize(wx.Size(-1, -1))

        self.lblVacExtra = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLVACEXTRA,
              label=_(u'extra vacation [days/year]'), name=u'lblVacExtra',
              parent=self, pos=wx.Point(284, 65), size=wx.Size(183, 21),
              style=wx.ALIGN_RIGHT)
        self.lblVacExtra.SetMinSize(wx.Size(-1, -1))

        self.spVacExtra = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPVACEXTRA,
              initial=0, max=365, min=0, name=u'spVacExtra', parent=self,
              pos=wx.Point(470, 65), size=wx.Size(90, 21),
              style=wx.SP_ARROW_KEYS)
        self.spVacExtra.SetMinSize(wx.Size(-1, -1))

        self.lblType = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLTYPE,
              label=_(u'Type'), name=u'lblType', parent=self, pos=wx.Point(0,
              90), size=wx.Size(187, 21), style=wx.ALIGN_RIGHT)
        self.lblType.SetMinSize(wx.Size(-1, -1))

        self.chcType = wx.Choice(choices=[_(u'normal time'), _(u'overtime'),
              _(u'flextime')], id=wxID_VUSERINFOWORKINGTIMESPANELCHCTYPE,
              name=u'chcType', parent=self, pos=wx.Point(191, 90),
              size=wx.Size(370, 21), style=0)
        self.chcType.SetMinSize(wx.Size(-1, -1))
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VUSERINFOWORKINGTIMESPANELCHCTYPE)

        self.vgTmHours = vidarc.tool.time.vTimeSelectorHoursPanel.vTimeSelectorHoursPanel(dayLabels=[],
              days=7, gridDay=15, gridHr=15, hours=24,
              id=wxID_VUSERINFOWORKINGTIMESPANELVGTMHOURS, name=u'vgTmHours',
              parent=self, pos=wx.Point(0, 149), size=wx.Size(561, 151),
              style=0, sum=True)

        self.lblStart = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLSTART,
              label=_(u'start hour'), name=u'lblStart', parent=self,
              pos=wx.Point(130, 115), size=wx.Size(65, 30),
              style=wx.ALIGN_RIGHT)
        self.lblStart.SetMinSize(wx.Size(-1, -1))

        self.spStart = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPSTART,
              initial=0, max=23, min=0, name=u'spStart', parent=self,
              pos=wx.Point(195, 115), size=wx.Size(65, 30),
              style=wx.SP_ARROW_KEYS)
        self.spStart.SetMinSize(wx.Size(-1, -1))

        self.lblHours = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLHOURS,
              label=_(u'hours [week]'), name=u'lblHours', parent=self,
              pos=wx.Point(0, 115), size=wx.Size(65, 30), style=wx.ALIGN_RIGHT)
        self.lblHours.SetMinSize(wx.Size(-1, -1))

        self.viHoursWeek = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VUSERINFOWORKINGTIMESPANELVIHOURSWEEK,
              integer_width=2, maximum=168, minimum=0.0, name=u'viHoursWeek',
              parent=self, pos=wx.Point(65, 115), size=wx.Size(65, 30),
              style=0)

        self.lblStartDay = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLSTARTDAY,
              label=_(u'start day'), name=u'lblStartDay', parent=self,
              pos=wx.Point(260, 115), size=wx.Size(65, 30),
              style=wx.ALIGN_RIGHT)
        self.lblStartDay.SetMinSize(wx.Size(-1, -1))

        self.chcStartDay = wx.Choice(choices=[_(u'Monday'), _(u'Thuesday'),
              _(u'Wednsday'), _(u'Thursday'), _(u'Friday'), _(u'Saturday'),
              _(u'Sunday')], id=wxID_VUSERINFOWORKINGTIMESPANELCHCSTARTDAY,
              name=u'chcStartDay', parent=self, pos=wx.Point(325, 115),
              size=wx.Size(65, 21), style=0)
        self.chcStartDay.SetMinSize(wx.Size(-1, -1))

        self.lblDays = wx.StaticText(id=wxID_VUSERINFOWORKINGTIMESPANELLBLDAYS,
              label=_(u'days'), name=u'lblDays', parent=self, pos=wx.Point(390,
              115), size=wx.Size(65, 30), style=wx.ALIGN_RIGHT)
        self.lblDays.SetMinSize(wx.Size(-1, -1))

        self.spDays = wx.SpinCtrl(id=wxID_VUSERINFOWORKINGTIMESPANELSPDAYS,
              initial=5, max=7, min=0, name=u'spDays', parent=self,
              pos=wx.Point(455, 115), size=wx.Size(65, 30),
              style=wx.SP_ARROW_KEYS)
        self.spDays.SetMinSize(wx.Size(-1, -1))

        self.cbQuickGen = wx.lib.buttons.GenBitmapButton(ID=wxID_VUSERINFOWORKINGTIMESPANELCBQUICKGEN,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbQuickGen', parent=self,
              pos=wx.Point(524, 115), size=wx.Size(31, 30), style=0)
        self.cbQuickGen.Bind(wx.EVT_BUTTON, self.OnCbQuickGenButton,
              id=wxID_VUSERINFOWORKINGTIMESPANELCBQUICKGEN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        
        self.viDated.SetCB(self.__showDated__)
        
        self.chcType.SetSelection(0)
        self.iType=0
        self.spPercent.SetValue(100)
        self.viHoursWeek.SetValue(40)
        self.spStart.SetValue(8)
        self.chcStartDay.SetSelection(0)
        self.spDays.SetValue(5)
        self.actRange=None
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __clearDated__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.vgTmHours.Clear()
        self.iType=0
        self.chcType.SetSelection(0)
        self.spPercent.SetValue(100)
        self.spExtra.SetValue(100)
        self.spVac.SetValue(25)
        self.spVacExtra.SetValue(0)
        self.dictTimes=self.objRegNode.GetTimeDict(None)
        self.chcType.SetSelection(0)
        self.vgTmHours.SetValue(self.dictTimes['normal'])
    def __enableDated__(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.chcType.Enable(flag)
        self.vgTmHours.Enable(flag)
        self.spPercent.Enable(flag)
        self.spExtra.Enable(flag)
        self.spVac.Enable(flag)
        self.spVacExtra.Enable(flag)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.SetModified(False)
            self.viDated.Clear()
            self.dictTimes=self.objRegNode.GetTimeDict(None)
            #self.__clearDated__()
            #self.chcRange.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.SetDoc(doc)
    def __showDated__(self,node,*args,**kwargs):
        if node is None:
            self.__clearDated__()
            #self.__enableDated__(False)
        else:
            try:
                self.spPercent.SetValue(int(self.objRegNode.GetPercentage(node)))
            except:
                self.spPercent.SetValue(100)
            try:
                self.spExtra.SetValue(int(self.objRegNode.GetExtraHour(node)))
            except:
                self.spExtra.SetValue(0)
            self.spVac.SetValue(self.objRegNode.GetVacationInt(node))
            self.spVacExtra.SetValue(self.objRegNode.GetVacationExtraInt(node))
            
            self.dictTimes=self.objRegNode.GetTimeDict(node)
            if self.VERBOSE:
                vtLog.pprint(self.dictTimes)
            self.chcType.SetSelection(0)
            self.vgTmHours.SetValue(self.dictTimes['normal'])
            #self.__enableDated__(True)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.viDated.SetNode(node)
            
            #self.__initDictTimes__()
            #self.dictTimes=self.doc.GetDictTimes(node)
            #if VERBOSE>0:
            #    vtLog.CallStack('')
            #    pp.pprint(self.dictTimes)
            #self.__showRanges__()
            # setup attributes
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            nodeDated=self.viDated.GetNode(node)
            if nodeDated is not None:
                self.objRegNode.SetPercentage(nodeDated,str(self.spPercent.GetValue()))
                self.objRegNode.SetExtraHour(nodeDated,str(self.spExtra.GetValue()))
                self.objRegNode.SetVacation(nodeDated,str(self.spVac.GetValue()))
                self.objRegNode.SetVacationExtra(nodeDated,str(self.spVacExtra.GetValue()))
                self.dictTimes[self.MAP_TYPE_IDX[self.iType]]=self.vgTmHours.GetValue()
                self.objRegNode.SetTimeDict(nodeDated,self.dictTimes)
            #self.__storePercent__()
            #if VERBOSE:
            #    vtLog.CallStack('dict')
            #    pp.pprint(self.dictTimes)
            #self.SetModified(False)
            #if self.doc.IsLoggedInAdmin()==False:
            #    return
            #self.doc.SetDictTimes(node,self.dictTimes)
        except:
            self.__logTB__()
    def OnGcbbApplyButton(self, event):
        event.Skip()
        try:
            self.GetNode(self.node)
            wxPostEvent(self,vgpUserInfoChanged(self,self.node))
        except:
            self.__logTB__()

    def OnGcbbCancelButton(self, event):
        event.Skip()
        try:
            self.SetNode(self.doc,self.node,self.ids)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_CONFIG:
            flag=True
        if flag:
            self.viDated.Enable(False)
            self.__enableDated__(False)
        else:
            self.viDated.Enable(True)
            self.__enableDated__(True)

    def OnChcTypeChoice(self, event):
        event.Skip()
        try:
            iType=self.chcType.GetSelection()
            self.dictTimes[self.MAP_TYPE_IDX[self.iType]]=self.vgTmHours.GetValue()
            self.iType=iType
            self.vgTmHours.SetValue(self.dictTimes[self.MAP_TYPE_IDX[self.iType]])
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'times:%s'%(vtLog.pformat(self.dictTimes)),self)
            #self.__showRanges__()
        except:
            self.__logTB__()
    def __storePercent__(self):
        try:
            self.actRange['percent']=self.spPercent.GetValue()
        except:
            vtLog.vtLngTB(self.GetName())
            pass
    def OnViDatedVttimeDatedValuesDeleted(self, event):
        event.Skip()

    def OnViDatedVttimeDatedValuesAdded(self, event):
        event.Skip()

    def OnCbQuickGenButton(self, event):
        event.Skip()
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'times:%s'%(vtLog.pformat(self.dictTimes)),self)
            iType=self.chcType.GetSelection()
            sType=self.MAP_TYPE_IDX[self.iType]
            self.dictTimes[sType]=self.vgTmHours.GetValue()
            iStartHr=self.spStart.GetValue()
            iDyS=self.chcStartDay.GetSelection()
            iDays=self.spDays.GetValue()
            iDyE=iDyS+iDays
            fHoursWk=self.viHoursWeek.GetValue()
            fHoursDay=fHoursWk/float(iDays)
            iEndHr=iStartHr+int(fHoursDay)
            if iEndHr>24:
                iEndHr=24
            iEndMn=int(round((fHoursDay%1)*60))
            sHrS='%02d00'%iStartHr
            sHrE='%02d%02d'%(iEndHr,iEndMn)
            if sHrE>'2400':
                sHrE='2400'
            l=[]
            for iDy in xrange(7):
                if (iDy>=iDyS) and (iDy<iDyE):
                    ll=[[sHrS,sHrE]]
                else:
                    ll=[]
                l.append(ll)
            self.dictTimes[sType]=l
            #self.iType=iType
            self.vgTmHours.SetValue(self.dictTimes[sType])
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'times:%s'%(vtLog.pformat(self.dictTimes)),self)
        except:
            self.__logTB__()
