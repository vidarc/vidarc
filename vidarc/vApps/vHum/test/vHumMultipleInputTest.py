#----------------------------------------------------------------------------
# Name:         vHumMultipleInputTest.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060228
# CVS-ID:       $Id: vHumMultipleInputTest.py,v 1.1 2006/03/02 10:54:45 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.art.vtArt as vtArt
from vidarc.vApps.vHum.vXmlHum import *
from vidarc.vApps.vHum.vXmlHumInputTreeUsers import *
from vidarc.vApps.vHum.vXmlHumInputTreeGroups import *
from vidarc.tool.xml.vtXmlDom import *
import sys

def create(self):
    self.doc=vtXmlDom()
    self.doc.Open(self.FNbase+'_0.xml')
    
    self.docHum=vXmlHum()
    self.docHum.Open('human_clt.xml')
    
    self.inTst1=vXmlHumInputTreeUsers(self.panel,-1,pos=(0,0),size=(200,100),name='selUsr')
    self.inTst2=vXmlHumInputTreeGroups(self.panel,-1,pos=(0,0),size=(200,100),name='selGrp')
    
    self.inTst1.SetDoc(self.doc)
    self.inTst1.SetDocTree(self.docHum)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input01'])
    self.inTst1.SetNode(node)
    
    self.inTst2.SetDoc(self.doc)
    self.inTst2.SetDocTree(self.docHum)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input02'])
    self.inTst2.SetNode(node)
    
    return [self.inTst1,self.inTst2],-1,[]
def SetNode(self):
    pass

import test.vTestApplSizer
test.vTestApplSizer.run(__file__,create,SetNode)
