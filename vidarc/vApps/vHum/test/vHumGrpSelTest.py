#----------------------------------------------------------------------------
# Name:         vHumGrpSelTest.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060228
# CVS-ID:       $Id: vHumGrpSelTest.py,v 1.1 2006/03/02 10:54:45 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.art.vtArt as vtArt
from vidarc.vApps.vHum.vXmlHum import *
from vidarc.vApps.vHum.vXmlHumInputSelGrpPanel import *
from vidarc.vApps.vHum.vXmlHumInputSelUsrPanel import *
from vidarc.tool.xml.vtXmlDom import *
import sys

def create(self):
    self.doc=vtXmlDom()
    self.doc.Open('human4.xml')
    
    self.docHum=vXmlHum()
    self.docHum.Open(self.FNbase+'_0.xml')
    
    self.inGrp=vXmlHumInputSelGrpPanel(self.panel,-1,(0,0),(200,100),0,name='selGrp')
    self.inUsr=vXmlHumInputSelUsrPanel(self.panel,-1,(0,0),(200,100),0,name='selUsr')
    
    self.inGrp.SetDoc(self.doc)
    self.inGrp.SetDocHum(self.docHum)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input01'])
    self.inGrp.SetNode(node)

    self.inUsr.SetDoc(self.doc)
    self.inUsr.SetDocHum(self.docHum)
    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input02'])
    self.inUsr.SetNode(node)

    return [self.inGrp,self.inUsr],-1,[]
def SetNode(self):
    pass

import test.vTestApplSizer
test.vTestApplSizer.run(__file__,create,SetNode)
