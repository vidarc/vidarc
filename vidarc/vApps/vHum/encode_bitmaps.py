#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: encode_bitmaps.py,v 1.8 2007/11/18 15:02:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Element img/Box01_16.png images.py",
    "-a -u -n ElementSel img/Box02_16.png images.py",
    "-a -u -n Text img/Note01_16.png images.py",
    "-a -u -n TextSel img/Note01_16.png images.py",
    "-a -u -n Group img/Grp02_16.png images.py",
    "-a -u -n GroupSel img/Grp02_16.png images.py",
    "-a -u -n User img/Usr02_16.png images.py",
    "-a -u -n UserSel img/Usr02_16.png images.py",
    "-a -u -n Human img/vidarcAppIconHum_16.png images.py",
    "-a -u -n HumanSel img/vidarcAppIconHum_16.png images.py",
    "-a -u -n Tel img/Fax01_16.png images.py",
    "-a -u -n Mobile img/Mobile01_16.png images.py",
    "-a -u -n Fax img/Fax01_16.png images.py",
    "-a -u -n Email img/Email01_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Stapler           img/OfficeStapler01_16.png      images.py",
    "-a -u -n Apply img/ok.png images.py",
    "-a -u -n Application img/VidMod_Hum_16.ico images.py",
    "-a -u -n Plugin img/VidMod_Hum_16.png images.py",
    
    "-u -i -n UserInfoAccess img_data/SecKey01_16.png images_data.py",
    "-a -u -n UserInfoSecurity img_data/Safe02_16.png images_data.py",
    "-a -u -n UserInfoAttr img_data/Attr01_16.png images_data.py",
    "-a -u -n UserInfoWorkingTimes img_data/Timer01_16.png images_data.py",
    "-a -u -n UserInfoAccuTimes img_data/TimerSum01_16.png images_data.py",
    "-a -u -n UserInfoSalary img_data/Salary01_16.png images_data.py",
    "-a -u -n GroupInfoAccess img_data/SecKey01_16.png images_data.py",
    "-a -u -n GroupInfoAttr img_data/Attr01_16.png images_data.py",
    "-a -u -n GroupInfoSecurity img_data/Safe02_16.png images_data.py",
    
    "-u -i -n Splash img/splashHuman01.png images_splash.py",
    
    "-a -u -n NoIcon  img/noicon.png  images_hum_tree.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

