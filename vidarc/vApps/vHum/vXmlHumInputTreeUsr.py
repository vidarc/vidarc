#----------------------------------------------------------------------------
# Name:         vXmlHumInputTreeUsr.py
# Purpose:      input widget for human / user xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vXmlHumInputTreeUsr.py,v 1.5 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vHum.vXmlHumTree import vXmlHumTree

class vXmlHumInputTreeUsr(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='user'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vHum')
    def __createTree__(*args,**kwargs):
        tr=vXmlHumTree(**kwargs)
        tr.SetNodeInfos(['surname','firstname',args[0].tagNameInt,'|id'])
        tr.SetGrouping([],[('surname',''),('firstname',''),(args[0].tagNameInt,'')])
        tr.SetSkipInfo(['users'])
        tr.SetValidInfo(['user'])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
    def Clear(self):
        vtInputTree.Clear(self)
        try:
            if self.doc is None:
                return
            if self.doc.GetLogin() is None:
                return
            fid=self.doc.GetLogin().GetUsrId()
            self.SetValueID('',fid)
        except:
            vtLog.vtLngTB(self.GetName())
            pass
