#----------------------------------------------------------------------------
# Name:         vXmlHumGrpTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlHumGrpTreeList.py,v 1.5 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx

from vidarc.tool.xml.vtXmlGrpTreeList import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import traceback
import string,types

import images

class vXmlHumGrpTreeList(vtXmlGrpTreeList):
    def __init__(self, parent, id, pos, size, style, name,
                    cols=None,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        bSetDfts=False
        if cols==None:
            bSetDfts=True
            cols=[_(u'hierarchy'),_(u'name'),_(u'ACL')]
        vtXmlGrpTreeList.__init__(self, parent, id, pos, size, style, name,cols,
                    master,controller,gui_update,verbose)
        if bSetDfts:
            self.SetColumnWidth(0, 200)
            self.SetColumnWidth(1, 80)
            self.SetColumnWidth(2, 80)
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[[('name','')]]
        self.bGrouping=False
        self.SetNodeInfos(['name','|id'],'|id')
    def SetupImageList(self):
        self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=images.getHumanBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['human']=[self.imgLstTyp.Add(img)]
        img=images.getHumanSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['human'].append(self.imgLstTyp.Add(img))
        
        img=images.getUserBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['user']=[self.imgLstTyp.Add(img)]
        img=images.getUserSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['user'].append(self.imgLstTyp.Add(img))
        
        img=images.getUserBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['users']=[self.imgLstTyp.Add(img)]
        img=images.getUserSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['users'].append(self.imgLstTyp.Add(img))
        
        img=images.getGroupBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['group']=[self.imgLstTyp.Add(img)]
        img=images.getGroupSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['group'].append(self.imgLstTyp.Add(img))
        
        img=images.getGroupBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['groups']=[self.imgLstTyp.Add(img)]
        img=images.getGroupSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['groups'].append(self.imgLstTyp.Add(img))
        
        
        img=images.getTextBitmap()
        #img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getTextSelBitmap()
        #img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
        #icon=images.getApplicationIcon()
        #self.SetIcon(icon)
        self.SetImageList(self.imgLstTyp)
    
    