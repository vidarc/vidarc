#----------------------------------------------------------------------------
# Name:         vXmlHumTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlHumTree.py,v 1.16 2007/11/18 15:21:03 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.tool.xml.vtXmlGrpTree import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vHum.vXmlEditGrpDialog import *
from vidarc.vApps.vHum.vXmlEditUsrDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

import images


class vXmlHumTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        #self.lastUsrId=None
        #self.lastGrpId=None
        #self.usrIds={}
        #self.grpIds={}
        self.nodeInfos=['name','|id','surname','firstname','inactive']
        self.nodeKey='|id'
        self.usrIds=None#vtXmlDom.NodeIds()
        self.grpIds=None#vtXmlDom.NodeIds()
        self.doc=None
        self.bLangMenu=True
        self.SetPossibleGrouping([
                ('surname'   , _(u'surname'), [('inactive','inactive'),] , 
                            {None:[('name','')],
                            'user':[('surname',''),('firstname',''),('name','')],
                            'group':[('name','')]}),
                ('login'   , _(u'login'), [] , 
                            {None:[('name','')],
                            'user':[('name',''),('surname',''),('firstname','')],
                            'group':[('name','')]}),
                ('firstname'   , _(u'firstname'), [] , 
                            {None:[('name','')],
                            'user':[('firstname',''),('surname',''),('name','')],
                            'group':[('name','')]}),
                ('surnameGrp'   , _(u'surname grouping'), {'user':[('surname',',1')]} , 
                            {None:[('name','')],
                            'user':[('surname',''),('firstname',''),('name','')],
                            'group':[('name','')]}),
                
                ])
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlGrpTree.__del__(self)
    def SetDftNodeInfos(self):
        self.nodeInfos=['name','|id','surname','firstname','inactive']
        self.nodeKey='|id'
    def SetDftGrouping(self):
        self.grouping=[('inactive','inactive'),]
        self.label={None:[('name','')],
                'user':[('surname',''),('firstname',''),('name','')],
                'group':[('name','')]}
        self.bGrouping=False
    def GetGroupIds(self):
        return self.grpIds
    def GetUserIds(self):
        return self.usrIds
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            images.getPluginImage(),
                            images.getPluginImage(),False)
            self.__addElemImage2ImageList__('human',
                            images.getPluginImage(),
                            images.getPluginImage(),False)
            # 071118:wro use CallAfter as workaround to keep image indexes
            wx.CallAfter(self.__addImage2ImageList__,'grp','inactive',
                            images.getStaplerImage(),
                            images.getStaplerImage(),False)
            self.SetImageList(self.imgLstTyp)
    def OnTcNodeExpanded(self,evt):
        vtXmlGrpTree.OnTcNodeExpanded(self,evt)
        self.SortChildren(evt.GetItem())
        evt.Skip()
    def __getValFormat__(self,g,val,infos=None):
        try:
            if g[1]=='inactive':
                if val in ['1','True']:
                    return _('inactive')
                else:
                    return None
        except:
            pass
        return vtXmlGrpTree.__getValFormat__(self,g,val,infos)
    def __getImgFormat__(self,g,val):
        try:
            if g[0]=='inactive':
                if val in ['1','True']:
                    img=self.imgDict['grp']['inactive'][0]
                    imgSel=self.imgDict['grp']['inactive'][1]
                    return (img,imgSel)
        except:
            pass
        return vtXmlGrpTree.__getImgFormat__(self,g,val)
    def OnTcNodeRightDownD(self, event):
        if self.bMaster==False:
            event.Skip()
            return
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdAddGrp'):
            self.popupIdAddGrp=wx.NewId()
            self.popupIdAddUsr=wx.NewId()
            self.popupIdDel=wx.NewId()
            EVT_MENU(self,self.popupIdAddGrp,self.OnTreeAddGrp)
            EVT_MENU(self,self.popupIdAddUsr,self.OnTreeAddUsr)
            EVT_MENU(self,self.popupIdDel,self.OnTreeDel)
        menu=wx.Menu()
        menu.Append(self.popupIdAddGrp,'Add Group')
        menu.Append(self.popupIdAddUsr,'Add User')
        menu.Append(self.popupIdDel,'Delete')
        
        self.PopupMenu(menu,wx.Point(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def __createMenuEditIds__(self):
        self.popupIdAddGrp=wx.NewId()
        self.popupIdAddUsr=wx.NewId()
        self.popupIdEditReq=wx.NewId()
        self.popupIdEditRel=wx.NewId()
        self.popupIdDel=wx.NewId()
        wx.EVT_MENU(self,self.popupIdAddGrp,self.OnTreeAddGrp)
        wx.EVT_MENU(self,self.popupIdAddUsr,self.OnTreeAddUsr)
        wx.EVT_MENU(self,self.popupIdEditReq,self.OnTreeEditReq)
        wx.EVT_MENU(self,self.popupIdEditRel,self.OnTreeEditRel)
        #wx.EVT_MENU(self,self.popupIdAdd,self.OnTreeAdd)
        #wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDel)

    def __addMenuEdit__(self,menu):
        if self.bMaster:
            
            mnIt=wx.MenuItem(menu,self.popupIdAddGrp,_(u'Add Group'))
            mnIt.SetBitmap(images_treemenu.getAddBitmap())
            menu.AppendItem(mnIt)
            #nodePar=self.objTreeItemSel
            #if self.doc is not None:
            #    if not self.doc.hasPossibleNode(nodePar):
            #        mnIt.Enable(False)
            
            mnIt=wx.MenuItem(menu,self.popupIdAddUsr,_(u'Add User'))
            mnIt.SetBitmap(images_treemenu.getAddBitmap())
            menu.AppendItem(mnIt)
            #mnIt=wx.MenuItem(menu,self.popupIdEdit,_(u'Edit'))
            #mnIt.SetBitmap(images_treemenu.getEditBitmap())
            #menu.AppendItem(mnIt)
            
            mnIt=wx.MenuItem(menu,self.popupIdDel,_(u'Delete'))
            mnIt.SetBitmap(images_treemenu.getDelBitmap())
            menu.AppendItem(mnIt)
            if self.objTreeItemSel is None:
                mnIt.Enable(False)
            menu.AppendSeparator()
            
            try:
                nodeTmp=self.GetSelected()
                if nodeTmp is not None:
                    if self.doc.IsLocked(nodeTmp):
                        mnIt=wx.MenuItem(menu,self.popupIdEditRel,_(u'Release'))
                        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Stop))
                        menu.AppendItem(mnIt)
                    else:
                        mnIt=wx.MenuItem(menu,self.popupIdEditReq,_(u'Request'))
                        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Pin))
                        menu.AppendItem(mnIt)
                    menu.AppendSeparator()
            except:
                vtLog.vtLngTB(self.GetName())
            return True
        return False

    def OnTreeAddGrp(self,event):
        if self.doc is None:
            return
        grps=self.doc.getChild(self.doc.getBaseNode(),'groups')
        self.AddGroupNode(grps)
        pass
    def OnTreeAddUsr(self,event):
        if self.doc is None:
            return
        usrs=self.doc.getChild(self.doc.getBaseNode(),'users')
        self.AddUserNode(usrs)
        pass
    def OnTreeDelOld(self,event):
        if self.doc is None:
            return
        if self.rootNode is None:
            return
        if self.objTreeItemSel is None:
            return
        
        self.doc.delNode(self.objTreeItemSel)
        self.Delete(self.triSelected)
        if self.tiCache is not None:
            self.tiCache.DelID(self.doc.getKey(self.objTreeItemSel))
        self.triSelected=self.GetSelection()
        try:
            self.objTreeItemSel=self.GetPyData(self.triSelected)
        except:
            self.objTreeItemSel=None
            self.triSelected=None
        
        pass
    def AddGroupNode(self,node):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'add group')
        grp=self.doc.createSubNode(node,'group',False)
        grpName=self.doc.createSubNodeText(grp,'name','---',False)
        grpStartDT=self.doc.createSubNodeText(grp,'startdatetime','---',False)
        grpEndDT=self.doc.createSubNodeText(grp,'enddatetime','---',False)
        self.doc.AlignNode(grp,iRec=2)
        self.doc.addNode(node,grp,self.SetIdManual)
        #self.idManualAdd=self.doc.getKey(grp)
        
        #tup=self.__findNodeByID__(None,self.doc.getKey(node))
        #if (tup is not None) and (grp is not None):
        #        ti=self.__addElements__(tup[0],grp)
        return 0
    def AddUserNode(self,node):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'add usr')
        usr=self.doc.createSubNode(node,'user')
        usrName=self.doc.createSubNodeText(usr,'name','---')
        usrSurName=self.doc.createSubNodeText(usr,'surname','---')
        usrFirstName=self.doc.createSubNode(usr,'firstname','---')
        usrStartDT=self.doc.createSubNode(usr,'startdatetime','---')
        usrEndDT=self.doc.createSubNode(usr,'enddatetime','---')
        usrAttrs=self.doc.createSubNode(usr,'attributes')
        for attr in ['title','birth','tel','mobile','email']:
            usrAttr=self.doc.createSubNode(usrAttrs,attr,'---')
        self.doc.AlignNode(usr,iRec=2)
        self.doc.addNode(node,usr,self.SetIdManual)
        #self.idManualAdd=self.doc.getKey(usr)
        
        #tup=self.__findNodeByID__(None,self.doc.getKey(node))
        #if (tup is not None) and (usr is not None):
        #        ti=self.__addElements__(tup[0],usr)
        return 0
