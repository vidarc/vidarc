#----------------------------------------------------------------------------
# Name:         vUserNodeSalary.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vUserNodeSalary.py,v 1.6 2007/11/18 15:02:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeDatedValues import *

try:
    if vcCust.is2Import(__name__):
        from vUserNodeSalaryPanel import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeSalary(vtXmlNodeDatedValues):
    NODE_ATTRS=[
            ('Salary',None,'salary_value',None),
            ('ClassificationCur',None,'classific_cur',None),
            ('ClassificationNext',None,'classific_nxt',None),
            ('SalState',None,'salState',None),
            #('desc',None,'desc',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='salary'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeDatedValues.__init__(self,tagName)
        self.oSMlinker=None
    def GetDescription(self):
        return _(u'salary')
    # ---------------------------------------------------------
    # specific
    def GetSalary(self,node):
        if self.IsDatedNode(node)==False:
            return '0.0'
        return self.Get(node,'salary_value')
    def GetSalaryLimit(self,node):
        o=self.doc.GetRegisteredNode('salaryScheme')
        if self.IsDatedNode(node)==False:
            iClass=-1
        else:
            iClass=self.GetChildAttr(node,'classification_cur','class')
        fMin,fMax=o.GetSalaryLimit(iClass)
        return fMin,fMax
    def GetSalaryLimitWorkingTimes(self,node):
        o=self.doc.GetRegisteredNode('salaryScheme')
        if self.IsDatedNode(node)==False:
            iClass=-1
            nodeUsr=self.doc.getParent(node)
        else:
            iClass=self.GetChildAttr(node,'classification_cur','class')
            nodeUsr=self.doc.getParent(self.doc.getParent(node))
        fMin,fMax=o.GetSalaryLimit(iClass)
        oWrk=self.doc.GetRegisteredNode('workingtimes')
        if oWrk is not None:
            sDt=self.GetDatedValidAt(node)
            nodeWrk=self.doc.getChild(nodeUsr,oWrk.GetTagName())
            nodeWrk=oWrk.GetDatedNodeValidAt(nodeWrk,sDt)
            sPer=oWrk.GetPercentage(nodeWrk)
            try:
                iPer=int(sPer)
            except:
                iPer=100
            fPer=iPer/100.0
        return fMin*fPer,fMax*fPer
    def GetClassificationCurFull(self,node):
        if self.IsDatedNode(node)==False:
            return iClass,'',-1
        iClass=self.GetChildAttr(node,'classification_cur','class')
        o=self.doc.GetRegisteredNode('salaryScheme')
        if o is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'salary scheme not found',self)
            return iClass,'',-1
        else:
            l=o.GetClassificationLst()
            i=0
            for tup in l:
                if tup[1]==iClass:
                    return iClass,tup[0],i
                i+=1
        return iClass,'',-1
    def GetClassificationCur(self,node):
        tup=self.GetClassificationCurFull(node)
        return tup[1]
    def GetClassificationNextFull(self,node):
        if self.IsDatedNode(node)==False:
            return 0,'',-1,''
        iClass=self.GetChildAttr(node,'classification_nxt','class')
        sDt=self.Get(node,'classification_nxt')
        o=self.doc.GetRegisteredNode('salaryScheme')
        if o is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'salary scheme not found',self)
            return iClass,'',-1,sDt
        else:
            l=o.GetClassificationLst()
            i=0
            for tup in l:
                if tup[1]==iClass:
                    return iClass,tup[0],i,sDt
                i+=1
        return iClass,'',-1,sDt
    def GetClassificationNext(self,node):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        tmp=self.GetDatedNode(node,bForced=False)
        tup=self.GetClassificationNextFull(tmp)
        return tup[3]
    
    def SetSalary(self,node,val):
        if self.IsDatedNode(node):
            self.Set(node,'salary_value',val)
    def SetClassificationCur(self,node,val):
        if self.IsDatedNode(node):
            self.SetChildAttrStr(node,'classification_cur','class',str(val))
    def SetClassificationNext(self,node,val,sDt=None):
        if self.IsDatedNode(node):
            self.SetChildAttrStr(node,'classification_nxt','class',str(val))
            if sDt is None:
                self.dt.Now()
                sDt=self.dt.GetDateStr()
            self.Set(node,'classification_nxt',sDt)
    
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,'salState','owner','vHum')
    def GetSalState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,'salState')
            id=self.GetChildAttr(node,'salState','fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetRolesDict(self,node):
        return {}
    def SetSalState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,'salState')
            c=self.GetChildForced(node,'salState')
            par=self.doc.getParent(node)
            if par is not None:
                idLogin=self.doc.getKey(par)
            else:
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,'salState','owner',val,'vHum')
    def GetActionCmdDict(self):
        return None
    def GetMsgTag(self,node,lang=None):
        nodePar=self.doc.getParent(node)
        objPar=self.doc.GetRegByNode(nodePar)
        return objPar.GetMsgTag(nodePar,lang)
    def GetMsgName(self,node,lang=None):
        nodePar=self.doc.getParent(node)
        objPar=self.doc.GetRegByNode(nodePar)
        return objPar.GetMsgName(nodePar,lang)
    def GetMsgPrior(self,node):
        return '0'
    def GetMsgInfo(self,node,lang=None):
        return ''
        try:
            self.dt.SetStr(self.GetStartDt(node))
        except:
            pass
        dtS=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        try:
            self.dt.SetStr(self.GetEndDt(node))
        except:
            pass
        dtE=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        return _(u'vacation')+' '+_('from:')+dtS+' '+_('to:')+dtE
    def ClassificationNext(self,node):
        tmp=self.GetDatedNodeValidAt(node,None)
        tup=self.GetClassificationNextFull(tmp)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'%s;%s'%(tmp,tup),self)
        return tup[3]
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('salary_value',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        _(u'salary_value')
        return _(name)
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xa5IDAT8\x8d}\x93\xd1Q\x1b1\x10\x86?\x9d)@\xc9{\x92m\xc0\xa0\x06 *\
\x81\x12T\x81}%\\\t\x9e\xc93\x8c\x924\xe0\x12\x84y\x06\xb6\x04\x01\r\xa8\x83\
\xcd\xc3\xd9\xe7\x0bv\xb23\x9a\x1d\xe9\xfe\xef\xd7\xeeI\xc2u\x0b\xe6#\x84`)%\
\x9b\xaf\x11\xa3\xc5\x18\xed\xa3\xd6u\x0bN\x16RJ\xa6\xaa6\x87)\x83\x91\xf3y\
\x13\xd7-\x00X\xaf\x96\xa6\xaa<\xec\x16\x8e}\xd8\xcd\xb51DR\x81\x0c B\xcc\
\x99\x87\xdd\xe3\xa4q\xae[\xb0^-m\x18"\x00\x9f>\xffps\x98R@\xd2>\xcb\x89I\
\x07\xa0\xaa\xcc\xf3\x01\x0e\xaa#,\x19b\x84Z\x01\xa8"|\xbf\xb9\xb6\xa9\x82yL\
e\xd7J&\x82d\x92\x08T!W\x81<\xce\x7f\xfe\xfa}\xac\xe0\x7fp\xf0~\xfa\x1e\xc2\
\x16R\xa2\x00\xdf\xbe~\xb1\xbf\x0c\xe6=\x1f`\xaa\x10Z\x80* u\xcaq\xb6i\xf7\
\xcf\x9e\xcb(\xcb\x14\x90\x8a\xb6\x86\xb6@\xca\x95\xd6\x1a1\xc6\xa3\x01)\x11\
\xb6\r\xf5\xb7\xc7\xb2\xe3\x08R\xe2\x08\xeb-\xa9\x14J)x\xef\xe9\xfb\xfex\x8c\
\x00v\x7fg\x078x\x0fUF\xd0+\xd44\xc1\xafo\xef\x8eY8\xd7-\xb0\xfb;\x0b^\t\xa1\
\x8d\xc7\xd9\xda$\x98\xef\xfc\x11>\xb6\x903z\xf8Y\x80n\x14\xed\x1b\xba\r\x13\
,RY\xaf\x96vu\xb9\xb4\x97\xe7\'{y~\xb2\xab\xcb\xa5]\x00\xb8\xdd\xa33\xb0\x9c\
\x12a\xa3l\xd3\x06\xef=\xc30Pj\xe5\xf5\xed\xdd\xa9\xf6\x06\xd0\xf7\r\xef=\
\xad5B\x08\\L\xbd\xecMTd\x12\xcc\xef\xbc\xaa""\xa8*\xad5Zk\xe3\xcd=y\x9e1\
\x9a\x88\x9c}\xba\xe7\xc6\x1f\xcb\xff\xff>\x06U+\x96\x00\x00\x00\x00IEND\xae\
B`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vUserNodeSalaryPanel
        else:
            return None

