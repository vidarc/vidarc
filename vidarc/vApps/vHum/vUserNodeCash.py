#----------------------------------------------------------------------------
# Name:         vUserNodeCash.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060503
# CVS-ID:       $Id: vUserNodeCash.py,v 1.6 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeCash(vtXmlNodeAttrML):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='cash',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'cash')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'desc')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'desc',val)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02?IDAT8\x8dm\x93\xdfKSa\x18\xc7?\xe7,\xb2\x8e\x0c\xcf\xce\xfc\xb1p\
\xb4\r\xb1\xa3\xa1)\xf3\xa2D\xb2\xa0\x0b\x87\x17\x16+\x88.\xba1\xea\xaa\xdb \
\x88.\x85\xa2\xbf@\x8d\xfd\x07A\xe9M\x05\xc6R\xa2\xa0\x06%\n;\x11m\x8au\xb2\
\xe9\xd9\x8c\xa4\xa9\xed\xbc]\x1c\xd6\xdc9>\xf0\xf2\xbc|\xf9\xbe\xcf\xf3}~\
\xbcH\xb2\x0f\xf7\th\x1fE\xd5\xef\xbf\x1f\xc4\x95$\xd9\xc7~\x8b\xc6R\xe2\xe6\
\xf87\xe6\xdf\xc8\x9c?\xeb`\xe9\x05\x18\x1e\xb2\x99N\xb5\x93\xcf\x8dK\xfb\
\xf9\x87pY\xa9\xd8\x87,\x9b\xdc\xbds\x86\xee\x13\x1d\xe4VW8\xd9m1\x97\x9e#\
\x9f\xbb/\xb9\xf9\xde\x00\xa5\x01i\xf2qJ\xf8\xe4\xf7 \xd9\x18\x86A\xd1\xdabf\
6\xee\xa6\x1e\x1c\x00 \x9f\x1b\x97\xd6\xcc\xdb\xc2\x9c1\xa9\xd8\x15|\xb2\x8f\
|\xee\x9e';\x80\xec\x06T5#F\x12\x13\x02\xa0bW\xa8\xfa\x91\xc4\x84P\xd5\x8cp\
\xf3\xeb\x9a8\x92\x98\x10\x17\xce\xf9\x08\xb5\x85him\xe1\xc3\xec4\x00\xf1\
\xd1\x1b\xec\xee\xed\x925\xb2\xa4\x17\xe0\xc5\xf3\x9a\x9a\xff%\x84\xc3S\xe2j\
R\xa6\xa3\xb3\x83\xe6\xd6f\xbe\x1b\x19z\xdb\x9e\x00\xa0HW\x08Et\xec\x8aM[\
\xcb\x16\xcbKSbm\xed\x96TWB2\xb9HSS\x13ZPc\xb7\xf0\x95p\xf9:z\x17\xe8]\x10\
\xfa}\x8d_\xa6A\xf8x\x18\xbf\xdfO2\xb9X\xdf\x83h,%\xe2\xa7\xe2hA\x8d\xa0\x16\
`e\xe9\x1d\xba\xee\x10\x8c,\xcc<\x85\xad\xd5eTMEQ\x14\xfa{\xfa\x89\xc6R\xc2\
\xd3D\xa5Q\x01@\x1f\x18\xe6\xd1C\x07\x1b\xbb\x08c\x97\x1c\xechC\x03J\xa3\x82\
,\xcb\xf5\n\xaaV.\x979\xd2p\x18#3_\xcb\xfe\x0ct\x1d\x8c\xcc<\x7fvv\xdcCp\x9a\
X*\xf6\xb1^xI$\x1a\xc1\xfcY \xd2s\x1a]\xab\x91\x0c\x03\x82\xb1^\xca\xe52\xd6\
\xa6\x85\xb9nR*&j\nJ\xa5\x01\xe9\xd5k\x1bk\xd3\xc2*X\x94\xfe\xfa\xf9\\\x99\
\xc4\xc8:*\xbe\xd8\xd3(\xad\x9dl\xfc\xd8`{{\x9b\xf4\x82\xf3\xc6\xb3\x07\xa3\
\xa3\x0f\xc4\xd0\xe0\x1e\xed\xc7\xda\x01\xc8\xbdu\xc6\x18\x1b\xbc\x0c\x80\
\xb9nz\xf6\xc0\xf3\x1bU5#\xd4\xc0'O\xad\xd5R\xab\x99\xab\xf6\x0f\x9d>\xddw*\
\xdc\xa42\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtInputAttrCfgMLPanel
        else:
            return None

