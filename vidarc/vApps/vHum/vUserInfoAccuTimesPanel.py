#Boa:FramePanel:vUserInfoAccuTimesPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoWorkingTimesPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoAccuTimesPanel.py,v 1.3 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeDatedValues
import vidarc.tool.time.vTimeSelectorHoursPanel
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.time.vtTimeTimeEdit
import wx.grid
import wx.lib.buttons
import cStringIO
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.tool.time.vTimeSelectorHoursPanel import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import __config__
    
    import calendar
    import string,sys
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

if VERBOSE>0:
    import pprint
    pp=pprint.PrettyPrinter(indent=2)

[wxID_VUSERINFOACCUTIMESPANEL, wxID_VUSERINFOACCUTIMESPANELLBLDATE, 
 wxID_VUSERINFOACCUTIMESPANELLBLDUMMY, 
 wxID_VUSERINFOACCUTIMESPANELLBLOVERTIME, wxID_VUSERINFOACCUTIMESPANELLBLVAC, 
 wxID_VUSERINFOACCUTIMESPANELLBLVACEXTRA, wxID_VUSERINFOACCUTIMESPANELVIDATED, 
 wxID_VUSERINFOACCUTIMESPANELVIOVERTIME, wxID_VUSERINFOACCUTIMESPANELVIVAC, 
 wxID_VUSERINFOACCUTIMESPANELVIVACEXTRA, 
] = [wx.NewId() for _init_ctrls in range(10)]

import images_hum_tree
import images

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUserInfoAccuTimesPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    MAP_TYPE_IDX=['normal','over','flex']
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(5)
        parent.AddGrowableCol(0)

    def _init_coll_bxsPercent_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblOverTime, 2, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viOverTime, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblDummy, 3, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsDated_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viDated, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsVac_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblVac, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.viVac, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblVacExtra, 2, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.viVacExtra, 1, border=3, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDated, 0, border=0,
              flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsPercent, 1, border=4,
              flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsVac, 1, border=4,
              flag=wx.TOP | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsPercent = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDated = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsVac = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsPercent_Items(self.bxsPercent)
        self._init_coll_bxsDated_Items(self.bxsDated)
        self._init_coll_bxsVac_Items(self.bxsVac)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERINFOACCUTIMESPANEL,
              name=u'vUserInfoAccuTimesPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(464, 317), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(456, 290))
        self.SetAutoLayout(True)

        self.lblDate = wx.StaticText(id=wxID_VUSERINFOACCUTIMESPANELLBLDATE,
              label=_(u'valid by'), name=u'lblDate', parent=self,
              pos=wx.Point(4, 0), size=wx.Size(149, 24), style=wx.ALIGN_RIGHT)
        self.lblDate.SetMinSize(wx.Size(-1, -1))

        self.viDated = vidarc.tool.time.vtTimeDatedValues.vtTimeDatedValues(id=wxID_VUSERINFOACCUTIMESPANELVIDATED,
              name=u'viDated', parent=self, pos=wx.Point(157, 0),
              size=wx.Size(294, 24), style=0)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_DELETED,
              self.OnViDatedVttimeDatedValuesDeleted,
              id=wxID_VUSERINFOACCUTIMESPANELVIDATED)
        self.viDated.Bind(vidarc.tool.time.vtTimeDatedValues.vEVT_VTTIME_DATED_VALUES_ADDED,
              self.OnViDatedVttimeDatedValuesAdded,
              id=wxID_VUSERINFOACCUTIMESPANELVIDATED)

        self.lblOverTime = wx.StaticText(id=wxID_VUSERINFOACCUTIMESPANELLBLOVERTIME,
              label=_(u'over time [hours]'), name=u'lblOverTime', parent=self,
              pos=wx.Point(4, 36), size=wx.Size(149, 24), style=wx.ALIGN_RIGHT)
        self.lblOverTime.SetMinSize(wx.Size(-1, -1))

        self.lblDummy = wx.StaticText(id=wxID_VUSERINFOACCUTIMESPANELLBLDUMMY,
              label=u'', name=u'lblDummy', parent=self, pos=wx.Point(231, 36),
              size=wx.Size(220, 24), style=wx.ALIGN_RIGHT)

        self.lblVac = wx.StaticText(id=wxID_VUSERINFOACCUTIMESPANELLBLVAC,
              label=_(u'vacation [days]'), name=u'lblVac', parent=self,
              pos=wx.Point(8, 64), size=wx.Size(145, 22), style=wx.ALIGN_RIGHT)
        self.lblVac.SetMinSize(wx.Size(-1, -1))

        self.lblVacExtra = wx.StaticText(id=wxID_VUSERINFOACCUTIMESPANELLBLVACEXTRA,
              label=_(u'extra vacation [days]'), name=u'lblVacExtra', parent=self,
              pos=wx.Point(231, 64), size=wx.Size(145, 22),
              style=wx.ALIGN_RIGHT)
        self.lblVacExtra.SetMinSize(wx.Size(-1, -1))

        self.viVac = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VUSERINFOACCUTIMESPANELVIVAC,
              integer_width=5, maximum=2000, minimum=-100, name=u'viVac',
              parent=self, pos=wx.Point(157, 64), size=wx.Size(70, 22),
              style=0)

        self.viVacExtra = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=1, id=wxID_VUSERINFOACCUTIMESPANELVIVACEXTRA,
              integer_width=4, maximum=100.0, minimum=0.0, name=u'viVacExtra',
              parent=self, pos=wx.Point(379, 64), size=wx.Size(71, 22),
              style=0)

        self.viOverTime = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VUSERINFOACCUTIMESPANELVIOVERTIME,
              integer_width=5, maximum=4000, minimum=-1000, name=u'viOverTime',
              parent=self, pos=wx.Point(157, 36), size=wx.Size(70, 24),
              style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        
        self.viDated.SetCB(self.__showDated__)
        
        #self.spPercent.SetValue(100)
        self.actRange=None
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __clearDated__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.viOverTime.SetValue(0)
        self.viVac.SetValue(0)
        self.viVacExtra.SetValue(0)
    def __enableDated__(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.viOverTime.Enable(flag)
        self.viVac.Enable(flag)
        self.viVacExtra.Enable(flag)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        self.viDated.Clear()
        #self.__clearDated__()
        #self.chcRange.Clear()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.SetDoc(doc)
    def __showDated__(self,node,*args,**kwargs):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            print node
            print args
            print kwargs
        if node is None:
            self.__clearDated__()
            #self.__enableDated__(False)
        else:
            self.viOverTime.SetValue(self.objRegNode.GetOverTimeFloat(node))
            self.viVac.SetValue(self.objRegNode.GetVacationFloat(node))
            self.viVacExtra.SetValue(self.objRegNode.GetVacationExtraFloat(node))
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.viDated.SetNode(node)
            
            #self.__initDictTimes__()
            #self.dictTimes=self.doc.GetDictTimes(node)
            #if VERBOSE>0:
            #    vtLog.CallStack('')
            #    pp.pprint(self.dictTimes)
            #self.__showRanges__()
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'set node finished',self)
            # setup attributes
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            nodeDated=self.viDated.GetNode(node)
            if self.VERBOSE:
                print nodeDated
            if nodeDated is not None:
                self.objRegNode.SetOverTime(nodeDated,str(self.viOverTime.GetValue()))
                self.objRegNode.SetVacation(nodeDated,str(self.viVac.GetValue()))
                self.objRegNode.SetVacationExtra(nodeDated,str(self.viVacExtra.GetValue()))
        except:
            self.__logTB__()
    def OnGcbbApplyButton(self, event):
        self.GetNode(self.node)
        wxPostEvent(self,vgpUserInfoChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode(self.doc,self.node,self.ids)
        event.Skip()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.__Close__()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.__Cancel__()
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_CONFIG:
            flag=True
        if flag:
            self.viDated.Enable(False)
            self.__enableDated__(False)
        else:
            self.viDated.Enable(True)
            self.__enableDated__(True)

    def OnViDatedVttimeDatedValuesDeleted(self, event):
        event.Skip()

    def OnViDatedVttimeDatedValuesAdded(self, event):
        event.Skip()
