#----------------------------------------------------------------------------
# Name:         vGroupsNode.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060503
# CVS-ID:       $Id: vGroupsNode.py,v 1.5 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        #from vUserInfoPanel import *
        #from vUserNodeEditDialog import *
        #from vUserNodeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vGroupsNode(vtXmlNodeBase):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='groups'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'groups')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xdeIDAT8\x8d\x85\x91?hSQ\x14\xc6\x7f\xef\xe5\x91\xa6\x92'\x08\r\x9a\
`\x90\x08\xf9g'\xc5 \x08b\x17\xc3SIC\x88\xab\x9b\x18\xcd\x96=C;9\x88\x938(\
\xee.\xba\xd8v\xb55A(>\xd3-&7(4b\xf2\x14C@\xfa\x94\x0e/\\\x87\xa4\xadm^\xd2\
\x0f\x0e\x9c\xfb\xdds\xbes\xeewQT\x0f\x8a\xeaa\xd67+K\xa5\x924n\x1aR\xd34y\
\x1c\xbf\x17\xfbI\xb9\\\x96RJYy_\x91\xd1xT*\xaa\x07\x9f\xcf's\xb9\xdc\x18\
\xff\x7f\xa8\x8c`~4\xa9V\xaa\xd4\xebu\x1c\xc7\xc1H\xa7e{\xbb\x8da\x18d\xb2\
\x99}\xfe(\xb4\xbd$u%C\xa1X\xc0q\x1cz?w\x89\xde\x8a\xe2\x9d\xf1\x129\x17A\
\x08\x81\x10\x02\xef\xd7\x1e\x07#\x87\xf0(\x8aJ\xe6\xf6=\xf9\xf0A\x91\xf0\
\xd9$\xe7#if\xbc:\xbb\xfdw\xb4\xb6\xfbl\x9a\x9b\xe8~\x9d\xa7\xa7np'~\x15\x90\
K\xb5\xfe\xb7\xe5\xb1\r\xb6j\x16\x9a\x1af1\x13dc5\xcb\xa3gox]\xcc\x13\x06\
\xee\x06\xe1m\xc2Gj\xfd$\x96\x10\x87\xb6\xd08\x82\xad\x9a\xc5\xc2e\x98\xf7\
\xe6\x99_]\x02.\xc2\xca2\x1b\xcf_\x01\x85\xc9\x1e\xb8\xe2\xc7\xd2\xc8\xe1\
\xc9%\xea\xe4+w\x08\xecq\x81\x95\xb5\x97\n\x80i\x9at\xbb]\xd7F{\x07j\x96\xa0\
\xa5\xfeU\xa6n\xd0\xe9t\xa85\x83\xc3&1\xe4\xbe\x08\xd8\xd1\x17\tu\x7f\x8f\t\
\xbbz\xf0\xa9\x19\x04,\xec&\xd0\x01\xbf\x0e\xfcq\x7f\xd2\xb1\x1e\xf8u8\x13:8\
'\x921\x99H\xc6\xe4\x98\xc0\xe3'\xf7\xb1mk\xaa\x98\xc0&\x10\x08\x90\xbc\x90$\
\x97\xcfJ\x00EQ=\xae\xc5\xeb/\x062\xfe}4\xdd\x84\xd4Z\x9c\x13\xb1\xd3\xcc\
\x05\xe6h|n\xd0l\xb4\x94\xa9\x02\x00\xd7/\r\xe4B\x08\xea\xed\x00\x83\xd85z\
\xbfzT+\x1f\x0e\xfd\xc2?\xb2#\xb7\xa3}\xc3\xb6\xb4\x00\x00\x00\x00IEND\xaeB`\
\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vUserNodeEditDialog
            return None
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vUserNodeAddDialog
            return None
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            #return vUserInfoPanel
            return None
        else:
            return None

