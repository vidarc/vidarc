#Boa:FramePanel:vUserInfoAccessPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoAccessPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoAccessPanel.py,v 1.9 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.time.vtTimeTimeEdit
import vidarc.tool.time.vtTimeDateGM
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import cStringIO
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.time.vTimeDateGM import *
    from vidarc.tool.time.vTimeTimeEdit import *

    import string,sys,calendar

    import __config__
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VUSERINFOACCESSPANEL, wxID_VUSERINFOACCESSPANELDTEEND, 
 wxID_VUSERINFOACCESSPANELDTESTART, wxID_VUSERINFOACCESSPANELGCBBDEL, 
 wxID_VUSERINFOACCESSPANELGCBBSET, wxID_VUSERINFOACCESSPANELLBLENDDATE, 
 wxID_VUSERINFOACCESSPANELLBLFROM, wxID_VUSERINFOACCESSPANELLBLSTARTDATE, 
 wxID_VUSERINFOACCESSPANELLBLTO, wxID_VUSERINFOACCESSPANELLSTDAYS, 
 wxID_VUSERINFOACCESSPANELLSTRANGES, wxID_VUSERINFOACCESSPANELTMEEND, 
 wxID_VUSERINFOACCESSPANELTMESTART, 
] = [wx.NewId() for _init_ctrls in range(13)]

import images

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUserInfoAccessPanel(wx.Panel,vtXmlNodePanel):
    TAGS=['startdate','enddate','day','starttime','endtime']
    def _init_coll_fgsRg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsRg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRanges, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 1, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsDtDisp, 0, border=4,
              flag=wx.BOTTOM | wx.RIGHT | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.fgsRg, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDtDisp_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(2)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbSet, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbDel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsDtDisp_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsDt, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstDays, 0, border=4, flag=wx.RIGHT | wx.EXPAND)

    def _init_coll_fgsDt_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsDt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblStartDate, 0, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.dteStart, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblEndDate, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.dteEnd, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFrom, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.tmeStart, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblTo, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.tmeEnd, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.fgsDt = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.fgsDtDisp = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.fgsRg = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsDt_Items(self.fgsDt)
        self._init_coll_fgsDt_Growables(self.fgsDt)
        self._init_coll_fgsDtDisp_Items(self.fgsDtDisp)
        self._init_coll_fgsDtDisp_Growables(self.fgsDtDisp)
        self._init_coll_fgsRg_Growables(self.fgsRg)
        self._init_coll_fgsRg_Items(self.fgsRg)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERINFOACCESSPANEL,
              name=u'vUserInfoAccessPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(469, 280), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(461, 253))
        self.SetAutoLayout(True)

        self.lblStartDate = wx.StaticText(id=wxID_VUSERINFOACCESSPANELLBLSTARTDATE,
              label=_(u'Start Date'), name=u'lblStartDate', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(58, 24), style=wx.ALIGN_RIGHT)
        self.lblStartDate.SetMinSize(wx.Size(-1, -1))

        self.lstRanges = wx.ListCtrl(id=wxID_VUSERINFOACCESSPANELLSTRANGES,
              name=u'lstRanges', parent=self, pos=wx.Point(4, 123),
              size=wx.Size(374, 126), style=wx.LC_REPORT)
        self.lstRanges.SetMinSize(wx.Size(-1, -1))
        self.lstRanges.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRangesListItemSelected,
              id=wxID_VUSERINFOACCESSPANELLSTRANGES)
        self.lstRanges.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstRangesListItemDeselected,
              id=wxID_VUSERINFOACCESSPANELLSTRANGES)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VUSERINFOACCESSPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Set'), name=u'gcbbSet',
              parent=self, pos=wx.Point(382, 123), size=wx.Size(72, 30),
              style=0)
        self.gcbbSet.SetConstraints(LayoutAnchors(self.gcbbSet, False, True,
              True, False))
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VUSERINFOACCESSPANELGCBBSET)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VUSERINFOACCESSPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Delete'), name=u'gcbbDel',
              parent=self, pos=wx.Point(382, 157), size=wx.Size(72, 30),
              style=0)
        self.gcbbDel.SetConstraints(LayoutAnchors(self.gcbbDel, False, True,
              True, False))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VUSERINFOACCESSPANELGCBBDEL)

        self.lblEndDate = wx.StaticText(id=wxID_VUSERINFOACCESSPANELLBLENDDATE,
              label=_(u'End Date'), name=u'lblEndDate', parent=self,
              pos=wx.Point(4, 32), size=wx.Size(58, 24), style=wx.ALIGN_RIGHT)
        self.lblEndDate.SetMinSize(wx.Size(-1, -1))

        self.lstDays = wx.CheckListBox(choices=[],
              id=wxID_VUSERINFOACCESSPANELLSTDAYS, name=u'lstDays', parent=self,
              pos=wx.Point(241, 4), size=wx.Size(211, 111), style=0)
        self.lstDays.Bind(wx.EVT_CHECKLISTBOX, self.OnLstDaysChecklistbox,
              id=wxID_VUSERINFOACCESSPANELLSTDAYS)

        self.lblFrom = wx.StaticText(id=wxID_VUSERINFOACCESSPANELLBLFROM,
              label=_(u'From'), name=u'lblFrom', parent=self, pos=wx.Point(4, 60),
              size=wx.Size(58, 24), style=wx.ALIGN_RIGHT)
        self.lblFrom.SetMinSize(wx.Size(-1, -1))

        self.lblTo = wx.StaticText(id=wxID_VUSERINFOACCESSPANELLBLTO,
              label=_(u'To'), name=u'lblTo', parent=self, pos=wx.Point(4, 88),
              size=wx.Size(58, 24), style=wx.ALIGN_RIGHT)
        self.lblTo.SetMinSize(wx.Size(-1, -1))

        self.dteStart = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VUSERINFOACCESSPANELDTESTART,
              name=u'dteStart', parent=self, pos=wx.Point(66, 4),
              size=wx.Size(166, 24), style=0)

        self.dteEnd = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VUSERINFOACCESSPANELDTEEND,
              name=u'dteEnd', parent=self, pos=wx.Point(66, 32),
              size=wx.Size(166, 24), style=0)

        self.tmeStart = vidarc.tool.time.vtTimeTimeEdit.vtTimeTimeEdit(id=wxID_VUSERINFOACCESSPANELTMESTART,
              name=u'tmeStart', parent=self, pos=wx.Point(66, 60),
              size=wx.Size(166, 24), style=0)

        self.tmeEnd = vidarc.tool.time.vtTimeTimeEdit.vtTimeTimeEdit(id=wxID_VUSERINFOACCESSPANELTMEEND,
              name=u'tmeEnd', parent=self, pos=wx.Point(66, 88),
              size=wx.Size(166, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        self.ranges=[]
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        self.dt=wx.DateTime()
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        self.__showDays__()
        
        # setup list
        self.lstRanges.InsertColumn(0,_(u'Start'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(1,_(u'End'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(2,_(u'Day'),wx.LIST_FORMAT_LEFT,40)
        self.lstRanges.InsertColumn(3,_(u'From'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(4,_(u'To'),wx.LIST_FORMAT_LEFT,70)
        self.SetupImageList()
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        #img=images_hum_tree.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstRanges.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)

    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.SetModified(False)
            self.lstRanges.DeleteAllItems()
            self.selectedIdx=-1
            self.ranges=[]
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __showDays__(self):
        self.lstDays.Clear()
        for i in range(7):
            self.lstDays.Append(calendar.day_name[i])
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.Clear()
            sStartDT=self.doc.getNodeText(node,'startdatetime')
            sEndDT=self.doc.getNodeText(node,'enddatetime')
            
            # setup attributes
            for rangeNode in self.doc.getChilds(node,'range'):
                range={}
                for k in self.TAGS:
                    val=self.doc.getNodeText(rangeNode,k)
                    range[k]=val
                self.ranges.append(range)
            self.__showRanges__()
        except:
            self.__logTB__()
    def __showRanges__(self):
        self.lstRanges.DeleteAllItems()
        self.selectedIdx=-1
        def compFunc(a,b):
            for i in [0,2,3]:
                res=cmp(a[self.TAGS[i]],b[self.TAGS[i]])
                if res!=0:
                    return res
            return 0
        self.ranges.sort(compFunc)
        for rng in self.ranges:
            self.dt.ParseFormat(rng['startdate'],'%Y%m%d')
            val=self.dt.FormatISODate()
            index = self.lstRanges.InsertImageStringItem(sys.maxint, val, -1)
            i=1
            for k in self.TAGS[1:]:
                val=rng[k]
                if k in ['startdate','enddate']:
                    self.dt.ParseFormat(val,'%Y%m%d')
                    val=self.dt.FormatISODate()
                elif k in ['starttime','endtime']:
                    self.dt.ParseFormat(val,'%H%M%S')
                    val=val[0:2]+':'+val[2:4]+':'+val[4:6]
                elif k=='day':
                    try:
                        val=calendar.day_abbr[int(val)]
                    except:
                        pass
                self.lstRanges.SetStringItem(index, i, val)
                i+=1
        pass
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                return
            #sStartDT=self.zdtlStartGM.GetValue()
            #sEndDT=self.zdtlEndGM.GetValue()
            
            #self.doc.setNodeText(node,'startdatetime',sStartDT)
            #self.doc.setNodeText(node,'enddatetime',sEndDT)
            node=self.doc.getChildForced(node,'access')
            if node is None:
                return 
            childs=self.doc.getChilds(node,'range')
            iChilds=len(childs)
            iLen=len(self.ranges)
            for i in range(iLen):
                try:
                    child=childs[i]
                except:
                    child=self.doc.createSubNode(node,'range')
                for k in self.TAGS:
                    self.doc.setNodeText(child,k,self.ranges[i][k])
            for i in range(iLen,iChilds):
                self.doc.deleteNode(childs[i],node)
        except:
            self.__logTB__()
    def OnGcbbSetButton(self,event):
        if self.selectedIdx<0:
            for i in range(self.lstDays.GetCount()):
                if self.lstDays.IsChecked(i):
                    self.ranges.append({'startdate':self.dteStart.GetValue(),
                        'enddate':self.dteEnd.GetValue(),
                        'starttime':self.tmeStart.GetTimeStr(),
                        'endtime':self.tmeEnd.GetTimeStr(),
                        'day':str(i)})
        else:
            rng=self.ranges[self.selectedIdx]
            rng['startdate']=self.dteStart.GetValue()
            rng['enddate']=self.dteEnd.GetValue()
            rng['starttime']=self.tmeStart.GetTimeStr()
            rng['endtime']=self.tmeEnd.GetTimeStr()
            #range['day']=i
        self.__showRanges__()
        self.SetModified(True)
        event.Skip()
        
    def OnGcbbDelButton(self, event):
        if self.selectedIdx>=0:
            self.ranges=self.ranges[:self.selectedIdx]+self.ranges[self.selectedIdx+1:]
            self.__showRanges__()
        self.SetModified(True)
        event.Skip()
    def OnLstRangesListItemSelected(self, event):
        idx=event.GetIndex()
        self.selectedIdx=idx
        rng=self.ranges[idx]
        self.dteStart.SetValue(rng['startdate'])
        self.dteEnd.SetValue(rng['enddate'])
        self.tmeStart.SetTimeStr(rng['starttime'])
        self.tmeEnd.SetTimeStr(rng['endtime'])
        try:
            iDay=int(rng['day'])
        except:
            iDay=-1
            pass
        
        for i in range(self.lstDays.GetCount()):
            self.lstDays.Check(i,i==iDay)
        
    def OnLstRangesListItemDeselected(self, event):
        self.selectedIdx=-1
        self.lstDays.Enable(True)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                flag=True
        except:
            flag=True
        if flag:
            self.dteStart.Enable(False)
            self.dteEnd.Enable(False)
            self.tmeStart.Enable(False)
            self.tmeEnd.Enable(False)
            self.lstDays.Enable(False)
            self.lstRanges.Enable(False)
            self.gcbbSet.Enable(False)
            self.gcbbDel.Enable(False)
        else:
            self.dteStart.Enable(True)
            self.dteEnd.Enable(True)
            self.tmeStart.Enable(True)
            self.tmeEnd.Enable(True)
            self.lstDays.Enable(True)
            self.lstRanges.Enable(True)
            self.gcbbSet.Enable(True)
            self.gcbbDel.Enable(True)

    def OnLstDaysChecklistbox(self, event):
        if self.selectedIdx>=0:
            rng=self.ranges[self.selectedIdx]
            try:
                iDay=int(rng['day'])
            except:
                iDay=event.GetSelection()
                pass
            for i in range(self.lstDays.GetCount()):
                self.lstDays.Check(i,i==iDay)
        event.Skip()
