#----------------------------------------------------------------------------
# Name:         vNetHum.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20050503
# CVS-ID:       $Id: vNetHum.py,v 1.11 2007/08/01 15:42:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vHum.vXmlHum import vXmlHum
from vidarc.tool.net.vNetXmlWxGui import *

class vNetHum(vXmlHum,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,
                audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlHum.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                verbose=verbose,audit_trail=audit_trail)
        vXmlHum.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlHum.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlHum.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlHum.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlHum.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlHum.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlHum.Open(self,fn,True)
        else:
            iRet=vXmlHum.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlHum.Save(self,fn,encoding)
    def getChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return vXmlHum.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlHum.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlHum.deleteNode(self,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlHum.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlHum.__stop__(self)
    def NotifyContent(self):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
        #vXmlHum.Build(self)
    def NotifyGetNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def IsLoggedInSelf(self,node,usrId=None):
        if usrId is None:
            return vXmlHum.IsLoggedInSelf(self,node,self.GetLoggedInUsrId())
        else:
            return vXmlHum.IsLoggedInSelf(self,node,usrId)
