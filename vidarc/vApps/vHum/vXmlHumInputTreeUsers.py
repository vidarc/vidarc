#----------------------------------------------------------------------------
# Name:         vXmlHumInputTreeUsers.py
# Purpose:      input widget for human / user xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060228
# CVS-ID:       $Id: vXmlHumInputTreeUsers.py,v 1.5 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputMultipleTree import vtInputMultipleTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vHum.vXmlHumTree import vXmlHumTree
import vidarc.tool.lang.vtLgBase as vtLgBase

class vXmlHumInputTreeUsers(vtInputMultipleTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        try:
            _kwargs['columns']
        except:
            _kwargs['columns']=[(_(u'name'),-1),(_(u'firstname'),-1),(_(u'surname'),-1),('id',-1)]
        try:
            _kwargs['show_attrs']
        except:
            _kwargs['show_attrs']=['name','firstname','surname','|id']
        apply(vtInputMultipleTree.__init__,(self,) + _args,_kwargs)
        
        self.tagGrp='users'
        self.tagName='user'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vHum')
    def __createTree__(*args,**kwargs):
        tr=vXmlHumTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.SetSkipInfo(['users'])
        tr.SetValidInfo(['user'])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
