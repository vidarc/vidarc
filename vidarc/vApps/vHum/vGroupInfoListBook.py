#----------------------------------------------------------------------------
# Name:         vGroupInfoListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vGroupInfoListBook.py,v 1.4 2006/06/07 10:12:46 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.vApps.vHum.vGroupInfoAttrPanel as vGroupInfoAttrPanel
import vidarc.vApps.vHum.vGroupInfoAccessPanel as vGroupInfoAccessPanel
import vidarc.vApps.vHum.vGroupInfoSecurityPanel as vGroupInfoSecurityPanel

from vidarc.vApps.vHum.vUserInfoListBook import *

import vidarc.tool.log.vtLog as vtLog

VERBOSE=0

class vGroupInfoListBook(vUserInfoListBook):
    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        self._init_ctrls(parent,id, pos, size, style, name)
        vtXmlDomConsumer.__init__(self)
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.verbose=VERBOSE
        
        edit=[u'GroupInfoAttr',u'GroupInfoAccess',u'GroupInfoSecurity']
        editName=[u'Attr',u'Access',u'Security']
        il = wx.ImageList(16, 16)
        for e in edit:
            s='v%sPanel'%e
            f=getattr(images_data, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='v%sPanel'%e
            f=getattr(eval('v%sPanel'%e),'v%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
