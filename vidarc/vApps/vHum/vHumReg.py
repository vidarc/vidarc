#----------------------------------------------------------------------------
# Name:         vHumReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060930
# CVS-ID:       $Id: vHumReg.py,v 1.1 2006/10/09 11:26:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust
if vcCust.is2Import(__name__):
    import vidarc.tool.log.vtLog as vtLog
    vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    import wx
    import vidarc.tool.lang.vtLgBase as vtLgBase
    def funcCreateHum(obj,sSrc,parent):
        from vidarc.vApps.vHum.vXmlHumInputTree import vXmlHumInputTree
        global _
        _=vtLgBase.assignPluginLang('veData')
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label=_(u'human'), name='lblHum', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlHumInputTree(id=-1,name=u'%s_vitrHum'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vHum')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
else:
    def funcCreateHum(obj,sSrc,parent):
        return None,None
def funcSetDocHum(obj,viTr,doc,dNetDocs):
    viTr.SetDoc(doc)
    viTr.SetDocTree(dNetDocs['vHum']['doc'],dNetDocs['vHum']['bNet'])
    viTr.SetNodeTree(None)
def funcGetSelHum(obj,viTr,doc):
    sVal=viTr.GetValue()
    sID=viTr.GetForeignID()
    #sID='@'.join([sID,'vPrj'])
    return sVal,sID
def funcCmpId(doc,node,val,lMatch):
    if lMatch is not None:
        if val in lMatch:
            return True
        else:
            return False
    return True

def Register4Collector(self,tagNode,tagAttr,translation,tagName='dataCollector'):
    oDataColl=self.GetReg(tagName)
    dSrcWid={tagAttr:{
                    'create':funcCreateHum,
                    'setDoc':funcSetDocHum,
                    'getSel':funcGetSelHum,
                    'translation':translation,
                    'match':funcCmpId},
            }
    oDataColl.UpdateSrcWidDict(dSrcWid)
    #oDataColl.UpdateDataProcDict(tagNode,dataProc)
