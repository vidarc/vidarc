#----------------------------------------------------------------------------
# Name:         impMin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060605
# CVS-ID:       $Id: impMin.py,v 1.1 2006/06/07 10:36:45 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust

vcCust.set2Import('vidarc.vApps.vHum.',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeAccess',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeCash',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeCompany',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodePersonal',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodePrivate',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeSalary',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeSecurity',False)
vcCust.set2Import('vidarc.vApps.vHum.vUserNodeWorkingTimes',False)

