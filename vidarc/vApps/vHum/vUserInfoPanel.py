#Boa:FramePanel:vUsrInfoPanel
#----------------------------------------------------------------------------
# Name:         vUsrInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoPanel.py,v 1.14 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.lang.vtLgSelector
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
import cStringIO

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.xml.vtXmlNodeRegListBook import *
    #from vidarc.tool.xml.vtXmlDomConsumer import *
    #import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
    #import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM
    #from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vHum.vUserInfoListBook import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import __config__
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import string,sys
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

VERBOSE=0

[wxID_VUSRINFOPANEL, wxID_VUSRINFOPANELCHKINACTIVE, 
 wxID_VUSRINFOPANELGCBBAPPLY, wxID_VUSRINFOPANELGCBBCANCEL, 
 wxID_VUSRINFOPANELLBLFIRSTNAME, wxID_VUSRINFOPANELLBLID, 
 wxID_VUSRINFOPANELLBLLOGIN, wxID_VUSRINFOPANELLBLPERID, 
 wxID_VUSRINFOPANELLBLSURNAME, wxID_VUSRINFOPANELTXTFIRSTNAME, 
 wxID_VUSRINFOPANELTXTID, wxID_VUSRINFOPANELTXTLOGIN, 
 wxID_VUSRINFOPANELTXTPERSID, wxID_VUSRINFOPANELTXTSURNAME, 
 wxID_VUSRINFOPANELVILANG, 
] = [wx.NewId() for _init_ctrls in range(15)]

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUsrInfoPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbApply, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfoBt, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_fgsInfoBt_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfo, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)

    def _init_coll_fgsInfoBt_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogin, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtLogin, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblSurName, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtSurName, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblPerId, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.txtPersId, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFirstName, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtFirstName, 2, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblId, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.txtId, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsMisc, 0, border=0, flag=0)
        parent.AddSizer(self.bxsMisc2, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)
        parent.AddGrowableCol(3)

    def _init_coll_bxsMisc2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viLang, 0, border=0, flag=0)
        parent.AddWindow(self.chkInActive, 0, border=8,
              flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsInfo = wx.FlexGridSizer(cols=4, hgap=0, rows=2, vgap=0)

        self.fgsInfoBt = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsMisc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMisc2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)
        self._init_coll_fgsInfoBt_Items(self.fgsInfoBt)
        self._init_coll_fgsInfoBt_Growables(self.fgsInfoBt)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsMisc2_Items(self.bxsMisc2)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSRINFOPANEL, name=u'vUsrInfoPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(469, 317),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(461, 290))

        self.lblLogin = wx.StaticText(id=wxID_VUSRINFOPANELLBLLOGIN,
              label=_(u'login'), name=u'lblLogin', parent=self, pos=wx.Point(4,
              4), size=wx.Size(89, 21), style=wx.ALIGN_RIGHT)
        self.lblLogin.SetMinSize(wx.Size(-1, -1))

        self.txtLogin = wx.TextCtrl(id=wxID_VUSRINFOPANELTXTLOGIN,
              name=u'txtLogin', parent=self, pos=wx.Point(97, 4),
              size=wx.Size(103, 21), style=0, value=u'')
        self.txtLogin.SetMinSize(wx.Size(-1, -1))

        self.lblId = wx.StaticText(id=wxID_VUSRINFOPANELLBLID, label=_(u'ID'),
              name=u'lblId', parent=self, pos=wx.Point(4, 54), size=wx.Size(89,
              30), style=wx.ALIGN_RIGHT)
        self.lblId.SetMinSize(wx.Size(-1, -1))

        self.txtPersId = wx.TextCtrl(id=wxID_VUSRINFOPANELTXTPERSID,
              name=u'txtPersId', parent=self, pos=wx.Point(97, 29),
              size=wx.Size(103, 21), style=wx.TE_RICH2, value=u'')
        self.txtPersId.SetMinSize(wx.Size(-1, -1))
        self.txtPersId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VUSRINFOPANELTXTID)

        self.lblSurName = wx.StaticText(id=wxID_VUSRINFOPANELLBLSURNAME,
              label=_(u'surname'), name=u'lblSurName', parent=self,
              pos=wx.Point(204, 4), size=wx.Size(51, 21), style=wx.ALIGN_RIGHT)
        self.lblSurName.SetMinSize(wx.Size(-1, -1))

        self.txtSurName = wx.TextCtrl(id=wxID_VUSRINFOPANELTXTSURNAME,
              name=u'txtSurName', parent=self, pos=wx.Point(259, 4),
              size=wx.Size(111, 21), style=0, value=u'')
        self.txtSurName.SetMinSize(wx.Size(-1, -1))

        self.lblFirstName = wx.StaticText(id=wxID_VUSRINFOPANELLBLFIRSTNAME,
              label=_(u'first name'), name=u'lblFirstName', parent=self,
              pos=wx.Point(204, 29), size=wx.Size(51, 21),
              style=wx.ALIGN_RIGHT)
        self.lblFirstName.SetMinSize(wx.Size(-1, -1))

        self.txtFirstName = wx.TextCtrl(id=wxID_VUSRINFOPANELTXTFIRSTNAME,
              name=u'txtFirstName', parent=self, pos=wx.Point(259, 29),
              size=wx.Size(111, 21), style=0, value=u'')
        self.txtFirstName.SetMinSize(wx.Size(-1, -1))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VUSRINFOPANELGCBBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'gcbbApply', parent=self, pos=wx.Point(377, 8),
              size=wx.Size(76, 30), style=0)
        self.gcbbApply.SetMinSize(wx.Size(-1, -1))
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VUSRINFOPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VUSRINFOPANELGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(377, 42),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VUSRINFOPANELGCBBCANCEL)

        self.viLang = vidarc.tool.lang.vtLgSelector.vtLgSelector(id=wxID_VUSRINFOPANELVILANG,
              name=u'viLang', parent=self, pos=wx.Point(259, 54),
              size=wx.Size(30, 30), style=0)

        self.chkInActive = wx.CheckBox(id=wxID_VUSRINFOPANELCHKINACTIVE,
              label=_(u'inactive'), name=u'chkInActive', parent=self,
              pos=wx.Point(297, 54), size=wx.Size(70, 30), style=0)
        self.chkInActive.SetValue(False)
        self.chkInActive.SetToolTipString(u'checkBox1')

        self.txtId = wx.TextCtrl(id=wxID_VUSRINFOPANELTXTID, name=u'txtId',
              parent=self, pos=wx.Point(97, 54), size=wx.Size(103, 30),
              style=wx.TE_READONLY, value=u'')
        self.txtId.Enable(False)
        self.txtId.SetMinSize(wx.Size(-1, -1))

        self.lblPerId = wx.StaticText(id=wxID_VUSRINFOPANELLBLPERID,
              label=_(u'personnel number'), name=u'lblPerId', parent=self,
              pos=wx.Point(4, 29), size=wx.Size(89, 21), style=wx.ALIGN_RIGHT)
        self.lblPerId.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vHuman User'),
                lWidgets=[self.chkInActive,
                        self.txtLogin,
                        self.txtPersId,
                        self.txtSurName,
                        self.txtFirstName,
                        ],
                lEvent=[(self.chkInActive,wx.EVT_CHECKBOX)])
        self.bModified=False
        self.bNet=False
        self.verbose=VERBOSE
        
        self.nbInfos=vtXmlNodeRegListBook(self,name='nbUsrInfo',
                    pos=wx.Point(0,118),size=wx.Size(350,50),verbose=1)
        self.nbInfos.SetMinSize((-1,-1))
        self.fgsData.AddWindow(self.nbInfos, 1, border=4, flag=wx.EXPAND|wx.ALL)
        #self.vlbUsrInfos=vUserInfoListBook(self,name='vlbUsrInfo',
        #                            pos=wx.Point(0,76),size=wx.Size(450,380))
        #self.fgsData.AddWindow(self.vlbUsrInfos, 1, border=4, flag=wx.EXPAND|wx.ALL)
        self.fgsData.Layout()
        
        self.Move(pos)
        self.SetSize(size)
        #self.Layout()
        self.SetName(name)

    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.nbInfos.GetModified():
            return True
        return False
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            #self.nbInfos.ClearWid()
            self.txtLogin.SetValue('')
            self.txtId.SetValue('')
            self.txtPersId.SetValue('')
            self.txtSurName.SetValue('')
            self.txtFirstName.SetValue('')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def ClearInt(self):
        vtXmlNodePanel.ClearInt(self)
        self.nbInfos.ClearInt()
        #self.txtLogin.SetValue('')
        #self.txtId.SetValue('')
        #self.txtPersId.SetValue('')
        #self.txtSurName.SetValue('')
        #self.txtFirstName.SetValue('')
    def Clear(self):
        vtXmlNodePanel.Clear(self)
        self.ClearInt()
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.nbInfos.SetRegNode(obj)
        return
        #self.nbInfos.SetDoc(self.doc,self.bNet)
        for s in ['personal','cash','company']:
            pn=self.nbInfos.GetPanelByName(s)
            o=self.doc.GetRegisteredNode(s)
            if pn is not None:
                pn.SetTagNames2Base(o.GetCfgBase())
        self.nbInfos.SetDocPanels(self.doc,self.bNet)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.nbInfos.SetDoc(doc,bNet)
        self.bNet=bNet
        self.viLang.SetDoc(doc)
        
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            #vtLog.CallStack(resp)
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            sLogin=self.doc.getNodeText(node,'name')
            sAttrId=self.doc.getAttribute(node,'id')
            iPersID=self.objRegNode.GetPersID(node)
            if iPersID<0:
                sPersID='%08d'%(self.doc.GetSortedPersonnelIds().GetFirstFreeId())#''
            else:
                sPersID=self.doc.ConvertIdNum2Str(iPersID)
            #print sPersID
            sSurName=self.doc.getNodeText(node,'surname')
            sFirstName=self.doc.getNodeText(node,'firstname')
            
            self.txtLogin.SetValue(sLogin)
            self.txtId.SetValue(sAttrId)
            self.txtPersId.SetValue(sPersID)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtPersId.SetStyle(0, len(sAttrId), wx.TextAttr("BLACK", bkgCol))
            self.txtSurName.SetValue(sSurName)
            self.txtFirstName.SetValue(sFirstName)
            
            self.viLang.SetValue(self.objRegNode.GetLang(self.node))
            self.chkInActive.SetValue(self.objRegNode.GetInActive(self.node))
            
            self.nbInfos.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            sLogin=self.txtLogin.GetValue()
            sPersID=self.txtPersId.GetValue()
            sSurName=self.txtSurName.GetValue()
            sFirstName=self.txtFirstName.GetValue()
            if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                ids=self.doc.GetSortedPersonnelIds()
                #vtLog.CallStack('')
                #vtLog.pprint(ids.ids)
                if ids is not None:
                    sOldId=self.objRegNode.GetPersID(node)
                    ids.RemoveId(sOldId)
                    o,lbl,iPersID=ids.GetInfoById(sPersID)
                    if o is None:
                        bOk=True
                    else:
                        if self.doc.isSame(o,node):
                            bOk=True
                        else:
                            bOk=False
                    #ret,n=ids.GetIdStr(sAttrId)
                    #if ret<0:
                    #    bOk=False
                    #else:
                    #    if n is None:
                    #        bOk=True
                    #    else:
                    #        if n==node:
                    #            bOk=True
                    #        else:
                    #            bOk=False
                    if bOk==False:
                        # fault id is not possible
                        #ids.AddNewNode(node)
                        #sAttr=ids.ConvertId2Str(node)
                        #self.txtPersId.SetValue(sAttr)
                        self.txtPersId.SetValue(sPersID)
                        #self.txtPersId.SetStyle(0, len(sPersID), wx.TextAttr("BLACK", "WHITE"))
                        self.txtPersId.SetStyle(0, len(sPersID), 
                                    wx.TextAttr("RED", "YELLOW"))
                    else:
                        #self.doc.setAttribute(node,'id',sPersID)
                        #sPersID=self.doc.ConvertIdNum2Str(long(sPersID))
                        self.objRegNode.SetPersID(node,long(sPersID))
                        self.txtPersId.SetValue(sPersID)
                        iPersID=self.objRegNode.GetPersID(node)
                        sPersID=self.doc.ConvertIdNum2Str(iPersID)
                        ids.AddNodeSingle(node,'persID',[('name','%s')],
                                        self.doc,True)
                        self.txtPersId.SetValue(sPersID)
                        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
                        self.txtPersId.SetStyle(0, len(sPersID), 
                                    wx.TextAttr("BLACK", bkgCol))#"WHITE"))
                        #ids.CheckId(node)
                        #ids.ClearMissing()
            
                self.doc.setNodeText(node,'name',sLogin)
            self.doc.setNodeText(node,'surname',sSurName)
            self.doc.setNodeText(node,'firstname',sFirstName)
            self.objRegNode.SetID(node,long(self.doc.getKey(node)))
            self.objRegNode.SetLang(node,self.viLang.GetValue())
            self.objRegNode.SetInActive(node,self.chkInActive.GetValue())
            
            self.nbInfos.GetNode(node)
            if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                pn=self.nbInfos.GetPanelByName('security')
                grps=pn.GetGroupLst()
                self.doc.setNodeText(node,'grps',','.join(grps))
            return sLogin
        except:
            self.__logTB__()
    def OnGcbbApplyButton(self, event):
        self.GetNode()
        wx.PostEvent(self,vgpUserInfoChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetModified(False)
        self.nbInfos.SetModified(False)
        self.SetNode(self.node)
        event.Skip()

    def OnTxtIdText(self, event):
        vtLog.CallStack('')
        if self.doc is None:
            return
        ids=self.doc.GetSortedPersonnelIds()
        vtLog.pprint(ids)
        sPersID=self.txtPersId.GetValue()
        o,lbl,iPersID=ids.GetInfoById(sPersID)
        if o is None:
            ids.AddNode(self.node)
            sPersID=self.doc.ConvertIdNum2Str(long(sPersID))
            #sId=ids.ConvertId2Str(self.node)
            self.txtPersId.SetValue(sPersID)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtPersId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", bkgCol))
            return
        else:
            if self.doc.isSame(o,self.node):
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtPersId.SetStyle(0, len(sPersID), wx.TextAttr("BLACK", bkgCol))
        else:
            # id is USED -> fault
            self.txtPersId.SetStyle(0, len(sPersID), wx.TextAttr("RED", "YELLOW"))
        event.Skip()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.gcbbApply.Enable(False)
            self.txtLogin.Enable(False)
            self.txtPersId.SetEditable(False)
            self.txtSurName.Enable(False)
            self.txtFirstName.Enable(False)
            self.chkInActive.Enable(False)
        else:
            self.gcbbApply.Enable(True)
            #self.txtLogin.Enable(True)
            #self.txtLogin.SetEditable(True)
            #self.txtId.SetEditable(True)
            self.txtSurName.Enable(True)
            self.txtFirstName.Enable(True)
            flagAdmin=self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN
            if flagAdmin:
                self.txtLogin.Enable(True)
                self.txtLogin.SetEditable(True)
                self.txtPersId.SetEditable(True)
                self.chkInActive.Enable(True)
            else:
                self.txtLogin.Enable(False)
                self.txtLogin.SetEditable(False)
                self.txtPersId.SetEditable(False)
                self.chkInActive.Enable(False)
            
        self.nbInfos.Lock(flag)
