#Boa:FramePanel:vXmlHumInputSelUsrPanel
#----------------------------------------------------------------------------
# Name:         vHumGrpInputSelGrpPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060228
# CVS-ID:       $Id: vXmlHumInputSelUsrPanel.py,v 1.4 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlDomConsumer import *
import string,sys


[wxID_VXMLHUMINPUTSELUSRPANEL, wxID_VXMLHUMINPUTSELUSRPANELCHKLSTUSR, 
] = [wx.NewId() for _init_ctrls in range(2)]

import images_hum_tree

# defined event for vgpXmlTree item selected
wxEVT_VXMLHUMINPUT_SELUSER_CHANGED=wx.NewEventType()
vEVT_VXMLHUMINPUT_SELUSER_CHANGED=wx.PyEventBinder(wxEVT_VXMLHUMINPUT_SELUSER_CHANGED,1)
def EVT_VXMLHUMINPUT_SELUSER_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VHUMINPUT_SELUSER_CHANGED,func)
class vXmlHumInputSelUsrChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VXMLHUMINPUT_SELUSER_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VXMLHUMINPUT_SELUSER_CHANGED)
        self.obj=obj
        self.node=node
    def GetObject(self):
        return self.obj
    def GetNode(self):
        return self.node

class vXmlHumInputSelUsrPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_fgsGrp_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chklstUsr, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsGrp_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsGrp = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsGrp_Items(self.fgsGrp)
        self._init_coll_fgsGrp_Growables(self.fgsGrp)

        self.SetSizer(self.fgsGrp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLHUMINPUTSELUSRPANEL,
              name=u'vXmlHumInputSelUsrPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(189, 103), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(181, 76))
        self.SetAutoLayout(True)

        self.chklstUsr = wx.CheckListBox(choices=[],
              id=wxID_VXMLHUMINPUTSELUSRPANELCHKLSTUSR, name=u'chklstUsr',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(181, 76), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlDomConsumer.__init__(self)
        
        self._init_ctrls(parent)
        self.SetName(name)
        self.bModified=False
        self.usrs=[]
        self.Move(pos)
        self.SetSize(size)

    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    
    def Clear(self):
        self.SetModified(False)
        self.usrs=[]
        self.chklstUsr.Clear()
        self.selectedIdx=-1
        
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)

    def SetNode(self,node):
        self.Clear()
        vtXmlDomConsumer.Clear(self)
        if (self.doc is None) or (node is None):
            return
        self.node=node
        
        self.usrs=[]
        grpsNode=self.doc.getChild(self.doc.getBaseNode(),'users')
        if grpsNode is not None:
            for c in self.doc.getChilds(grpsNode,'user'):
                id=self.doc.getKey(c)
                sName=self.doc.getNodeText(c,'name')
                self.usrs.append((sName,id))
        def compFunc(a,b):
            return cmp(a[0],b[0])
        self.usrs.sort(compFunc)
        dGrps={}
        i=0
        for sName,id in self.usrs:
            try:
                dGrps[long(id)]=i
                self.chklstUsr.Append(sName)
                i+=1
            except:
                pass
        
        node=self.doc.getChild(node,'users')
        if node is None:
            return
        
        # setup attributes
        for c in self.doc.getChilds(node,'user'):
            try:
                i=dGrps[long(self.doc.getAttribute(c,'fid'))]
                self.chklstUsr.Check(i,True)
            except:
                pass
                
    def ClearHum(self):
        self.nodeHum=None
        self.usrs=[]
        self.chklstUsr.Clear()
        
    def SetDocHum(self,doc,bNet=False):
        self.docHum=self.doc
        
    def SetNodeHum(self,node):
        self.ClearHum()
        if (self.docHum is None) or (node is None):
            return
        self.nodeHum=node
        
        self.usrs=[]
        grpsNode=self.doc.getChild(self.doc.getBaseNode(),'users')
        if grpsNode is not None:
            for c in self.doc.getChilds(grpsNode,'user'):
                id=self.doc.getKey(c)
                sName=self.doc.getNodeText(c,'name')
                self.usrs.append((sName,id))
        def compFunc(a,b):
            return cmp(a[0],b[0])
        self.usrs.sort(compFunc)
        dUsrs={}
        i=0
        for sName,id in self.usrs:
            try:
                dUsrs[long(id)]=i
                self.chklstUsr.Append(sName)
                i+=1
            except:
                pass
    
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if (self.doc is None) or (node is None):
            return
        node=self.doc.getChildForced(node,'users')
        if node is None:
            return 
        childs=self.doc.getChilds(node,'user')
        iChilds=len(childs)
        grps=[]
        for i in range(self.chklstUsr.GetCount()):
            if self.chklstUsr.IsChecked(i):
                usrs.append(i)
        iLen=len(grps)
        for i in range(iLen):
            try:
                child=childs[i]
            except:
                child=self.doc.createSubNode(node,'user')
            self.doc.setAttribute(child,'fid',self.usrs[usrs[i]][1])
            #self.doc.setNodeText(child,'name',self.usrs[grps[i]][0])
        for i in range(iLen,iChilds):
            self.doc.deleteNode(childs[i],node)
        self.SetModified(False)
        
    def Lock(self,flag,locker=''):
        if flag:
            self.chklstUsr.Enable(False)
        else:
            self.chklstUsr.Enable(True)
