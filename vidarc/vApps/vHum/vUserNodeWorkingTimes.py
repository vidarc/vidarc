#----------------------------------------------------------------------------
# Name:         vUserNodeWorkingTimes.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vUserNodeWorkingTimes.py,v 1.7 2007/04/18 15:59:54 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import calendar
import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeDatedValues import *
try:
    if vcCust.is2Import(__name__):
        from vUserInfoWorkingTimesPanel import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeWorkingTimes(vtXmlNodeDatedValues):
    NODE_ATTRS=[
            ('Percentage',None,'percent',None),
            ('ExtraHour',None,'extrahour',None),
            ('Vacation',None,'vacation',None),
            ('VacationExtra',None,'vacation_extra',None),
            #('desc',None,'desc',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='workingtimes'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeDatedValues.__init__(self,tagName)
    def GetDescription(self):
        return _(u'working times')
    # ---------------------------------------------------------
    # specific
    def __convTime__(self,s):
        l=[[] for i in xrange(7)]
        if s is None:
            return l
        try:
            sDys=s.split(';')
            iDy=0
            for sDy in sDys:
                if len(sDy)>0:
                    l[iDy]=[sHrs.split(':') for sHrs in sDy.split(',')]
                iDy+=1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return l
    def __convLst2Str__(self,l):
        s=';'.join([','.join([':'.join(lHr) for lHr in lHrs]) for lHrs in l])
        return s
    def GetPercentage(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'percent')
    def GetExtraHour(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'extrahour')
    def GetVacation(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'vacation')
    def GetVacationExtra(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'vacation_extra')
    def GetNormalTime(self,node):
        if self.IsDatedNode(node)==True:
            return self.__convTime__(self.Get(node,'normaltime'))
        return self.__convTime__(None)
    def GetOverTime(self,node):
        if self.IsDatedNode(node)==True:
            return self.__convTime__(self.Get(node,'overtime'))
        return self.__convTime__(None)
    def GetFlexTime(self,node):
        if self.IsDatedNode(node)==True:
            return self.__convTime__(self.Get(node,'flextime'))
        return self.__convTime__(None)
        
    def SetPercentage(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'percent',val)
    def SetExtraHour(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'extrahour',val)
    def SetVacation(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'vacation',val)
    def SetVacationExtra(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'vacation_extra',val)
    def SetNormalTime(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'normaltime',self.__convLst2Str__(val))
    def SetOverTime(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'overtime',self.__convLst2Str__(val))
    def SetFlexTime(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'flextime',self.__convLst2Str__(val))
    
    def GetTimeDict(self,node):
        d={
            'normal':self.GetNormalTime(node),
            'over':self.GetOverTime(node),
            'flex':self.GetFlexTime(node),
            }
        return d
    def SetTimeDict(self,node,val):
        if self.IsDatedNode(node)==True:
            self.SetNormalTime(node,val['normal'])
            self.SetOverTime(node,val['over'])
            self.SetFlexTime(node,val['flex'])
    def GetPercentageInt(self,node,fallback=100):
        try:
            return int(self.GetPercentage(node))
        except:
            return fallback
    def GetExtraHourInt(self,node,fallback=0):
        try:
            return int(self.GetExtraHour(node))
        except:
            return fallback
    def GetVacationInt(self,node,fallback=0):
        try:
            return int(self.GetVacation(node))
        except:
            return fallback
    def GetVacationExtraInt(self,node,fallback=0):
        try:
            return int(self.GetVacationExtra(node))
        except:
            return fallback
    
    def GetTimeNormal(self,node,iYear,iMon,iDay):
        docGlb=self.doc.GetNetMaster().GetNetDoc('vGlobals')
        iRet=docGlb.IsHoliday('',iYear,iMon,iDay)
        if node is None or 1:
            iFirst=calendar.firstweekday()
            try:
                iWkDy=calendar.weekday(iYear,iMon,iDay)
            except:
                return -1,-1,-1,-1,-1
            if iWkDy>=(iFirst+5)%7:
                return -1,-1,-1,-1,0
            if iRet==0:
                return -1,-1,-1,-1,0
            elif iRet==1:
                return 13,0,16,60,50
            elif iRet==2:
                return 9,0,12,60,50
            
            return 9,0,16,60,100
        else:
            return 9,0,16,60,100
    def GetTimeNormalProp(self,node,iYear,iMon,iDay):
        docGlb=self.doc.GetNetMaster().GetNetDoc('vGlobals')
        iRet=docGlb.IsHoliday('',iYear,iMon,iDay)
        if node is None or 1:
            iFirst=calendar.firstweekday()
            iWkDy=calendar.weekday(iYear,iMon,iDay)
            if iWkDy>=(iFirst+5)%7:
                return 0.0
            if iRet==0:
                return 0.0
            elif iRet==1:
                return 0.5
            elif iRet==2:
                return 0.5
            return 1.0
        else:
            return 1.0

    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('percent',vtXmlFilterType.FILTER_TYPE_INT),
            ('extrahour',vtXmlFilterType.FILTER_TYPE_INT),
            ('vacation',vtXmlFilterType.FILTER_TYPE_INT),
            ('vacation_extra',vtXmlFilterType.FILTER_TYPE_INT),
            ]
    def GetTranslation(self,name):
        _(u'percent'),_(u'extrahour'),_(u'vacation'),_(u'vacation_extra')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01gIDAT8\x8d\x85\x93\xc1a\xdb0\x0cE\x1f\xa4\\|s&0\x17H\xaa\x05\x1cc\
\x04\x8d\xa0\x114\x02\xbbAF`Gp;@\xa8x\x81v\x03*Y\x80\xea\x00E\x0f\xb2\x1c9\
\x92[\\x\xc1\xff\xfc\xc0\xc7\x17)J\xd6\xea\xf0\xb4\xb7\x9c39g\x9cst\xaf'Y\
\xeb+V\xd1\xe7\xaa\xeb\x1a\xe7\xdc\xbfZ\x10U5\x80\x94\x12\xfd\xdb\xbb\x00|y|\
\xb0\xaa\xaa\xae\x1aCHHq\x92\xc3\xd3\xde\xaa\xaa\xe2\xf9\xf9'R\x9c\xe4\x0e\
\xc0{\x8f\xf7\x1e\xc0\xea\xba\xa6m[v\xbb\x1dt\x1d\x1c\x0e\x00\xa8~#\x04,\xe7\
\xccv\xbbE\x15\xbaW`\xdc\x81\x9a\xaaZ\x8c\xd1\xcc\xcclz\xe7\x15\xa3\xa5\x94\
\xaci\x1as\xce\x99\x14\xe5\x88\x9d\x08B\x08f1\x9a\xf7~\x86Y\x12\xa5\x94LU\
\xaf\tT\xd5RJ\x97\xa6\x89dN6W\x12B0\x18I\x16\xe09\xc9*\xc1Y\xd9\xa4\x02U\xbd\
97pS\xc5D\xf0q\x07\xe7m_\xaa\xeb\xf0\xde\xa3\xaaK\xf3\xe7\xbd\xab#\xac\xb9pc\
\x84\x02\xa0i\x1a\xfa\xbe\xbf\xfc\xbcP\xf3IYJ\x89\x18\xa7S\x1c\xb3p\xb1\xf1\
\x7f\xb5j\xa3\x14%\xce\xb9U\xdf\xe7cM\x874a\xa4()E\xc6=\x0e\xc3\xef\xaf\xf1\
\xe5\xc5\xf7}\xcff\xb3AD\x18\x8eG\x86\xfb{\x86a\xe0W\xce\xb4m\xcb\xf1\xfb\
\x8f\xabT\xca\xe78\xdb\x9f\xbd9\x97\x16)\x8c\x11\xa4XF\xfa/\xb8I\xc8\x15%n\
\x06+\x00\x00\x00\x00IEND\xaeB`\x82"  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vUserInfoWorkingTimesPanel
        else:
            return None

