#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: __init__.py,v 1.5 2007/11/21 21:59:20 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import images as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vHumanMDIFrame'
PLUGABLE_FRAME=True
PLUGABLE_ACTIVE=0
PLUGABLE_SRV='vXmlHum@vXmlSrv'
DOMAINS=['core','vHum']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','PLUGABLE_SRV',
        'getPluginData',]

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
