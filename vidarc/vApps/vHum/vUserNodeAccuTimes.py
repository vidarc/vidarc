#----------------------------------------------------------------------------
# Name:         vUserNodeAccuTimes.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060618
# CVS-ID:       $Id: vUserNodeAccuTimes.py,v 1.2 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import calendar
import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeDatedValues import *
try:
    if vcCust.is2Import(__name__):
        from vUserInfoAccuTimesPanel import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeAccuTimes(vtXmlNodeDatedValues):
    NODE_ATTRS=[
            ('OverTime',None,'overtime',None),
            ('Vacation',None,'vacation',None),
            ('VacationExtra',None,'vacation_extra',None),
            #('desc',None,'desc',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='accutimes'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeDatedValues.__init__(self,tagName)
    def GetDescription(self):
        return _(u'acculated times')
    # ---------------------------------------------------------
    # specific
    def GetOverTime(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'overtime')
    def GetVacation(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'vacation')
    def GetVacationExtra(self,node):
        if self.IsDatedNode(node)==False:
            return '0'
        return self.Get(node,'vacation_extra')
        
    def SetOverTime(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'overtime',val)
    def SetVacation(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'vacation',val)
    def SetVacationExtra(self,node,val):
        if self.IsDatedNode(node)==True:
            self.Set(node,'vacation_extra',val)
    
    def GetOverTimeFloat(self,node,fallback=100):
        try:
            return float(self.GetPercentage(node))
        except:
            return fallback
    def GetVacationFloat(self,node,fallback=0):
        try:
            return float(self.GetVacation(node))
        except:
            return fallback
    def GetVacationExtraFloat(self,node,fallback=0):
        try:
            return float(self.GetVacationExtra(node))
        except:
            return fallback
    
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('overtime',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ('vacation',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ('vacation extra',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        _(u'percent'),_(u'vacation'),_(u'vacation extra')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01IIDAT8\x8d\x85\x93\xedQ\xc30\x10D\x9f\x1c\nH\x03`5\x00\xb8\x81\x90+A\
%\xb8\x84\x94\xa0\x12(\xc1T\x12\x994@\t2i@\x15\xb0\xfc01\x0eq\xc2\xcex<\xa3\
\xb9{\xd2}\xacs\xd5\x8a%m_6*\xa5PJ\xc1{O\xff~pKq\xd5b\xf6\x8fB\x08x\xefo\x85\
\xe0\xccL\x009g\x86\xcf\xa3\x03x~zT\xd34g\x81]\x97q\xd5\xc1m_6j\x9a\x86\xd7\
\xd7\x0f\\upw\x001Fb\x8c\x00\n!\xb0\xdb\xed\xa8\xeb\x1a\xfa\x1e\xb6[\x00\xcc\
\xde\xe8:TJa\xbd^c\x06\xfd;0\xf6\xc0dfJ)I\x92t\xfa\xcf\x95\x92r\xcej\xdbV\
\xde{\xb9j5\xe6\x9e\x00]\xd7I))\xc68\xcb\xb9\x04\xe5\x9cef\xe7\x003S\xcey\n:\
A\xe6\xb0k\xbaH\x9eC\xe6\x00`\xf93\xb3\xabu\x03\x13\xe4\x1a\xe0w\x0f~\xba=\
\xa9\xef\x891bf\x00\x8c\x8cQ\xc7\xe1\x88\xa4\xf1l\xb1\x84\xa5)\xfc)E\x92\\\
\xb5\x1a7\xb1m[\x86a\x98n\xbex\xcd\xcdU\x1c\xbd0\x8d\xf1f\xc7g\xb7Og\xa7yz\
\xef\x17\xe7\xfe\xdf\x14\xdc\xdc\x8d\xf5\xc3\xbdB\x08\xbf&J\t\xcc\xa8\xeb\
\x1a\xe7\x16\xcd\x88\xfbkg}m\xe4}>s\xe1~\xbf\xbf\xda\x82o\xd4\xa6\xae\xa8q\
\x86x\x92\x00\x00\x00\x00IEND\xaeB`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vUserInfoAccuTimesPanel
        else:
            return None

