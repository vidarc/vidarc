#----------------------------------------------------------------------------
# Name:         vXmlHum.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20050503
# CVS-ID:       $Id: vXmlHum.py,v 1.22 2008/04/07 06:48:13 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import DateTime as wxDateTime
import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

from vidarc.tool.xml.vtXmlNodeAddrSingle import vtXmlNodeAddrSingle
from vidarc.tool.xml.vtXmlNodeAddrMultiple import vtXmlNodeAddrMultiple
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML
from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc
from vidarc.vApps.vHum.vXmlNodeHumRoot import vXmlNodeHumRoot
from vidarc.vApps.vHum.vGroupsNode import vGroupsNode
from vidarc.vApps.vHum.vGroupNode import vGroupNode
from vidarc.vApps.vHum.vUsersNode import vUsersNode
from vidarc.vApps.vHum.vUserNode import vUserNode
from vidarc.vApps.vHum.vUserNodePersonal import vUserNodePersonal
from vidarc.vApps.vHum.vUserNodePrivate import vUserNodePrivate
from vidarc.vApps.vHum.vUserNodeCash import vUserNodeCash
from vidarc.vApps.vHum.vUserNodeCompany import vUserNodeCompany
from vidarc.vApps.vHum.vUserNodeAccess import vUserNodeAccess
from vidarc.vApps.vHum.vUserNodeAddresses import vUserNodeAddresses
from vidarc.vApps.vHum.vUserNodeSecurity import vUserNodeSecurity
from vidarc.vApps.vHum.vUserNodeWorkingTimes import vUserNodeWorkingTimes
from vidarc.vApps.vHum.vUserNodeSalary import vUserNodeSalary
from vidarc.vApps.vHum.vUserNodeAccuTimes import vUserNodeAccuTimes
from vidarc.vApps.vHum.vUserNodeSalaryScheme import vUserNodeSalaryScheme

from vidarc.tool.xml.vtXmlNodeFilter import vtXmlNodeFilter
from vidarc.tool.xml.vtXmlNodeMessages import vtXmlNodeMessages
from vidarc.tool.xml.vtXmlNodeMsg import vtXmlNodeMsg
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateStateMachineLinker import veStateStateMachineLinker
from vidarc.ext.state.veStateState import veStateState
from vidarc.ext.state.veStateTransition import veStateTransition

def getPluginData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xaf0\xc2\x13}\x01\x89\x89?8\xa1\
\xe2\rV^\xe0\nF\x8e\x81\tT5qU\x04\x19\x9d\xd8u#G\xc4UH\xab\r\xfd\xa9@\x00\
\xfb\xb4J\x8a\xb9\xd2A\x0b\x03\x00\xb9\x1d\xb0WY\xdd\x82\xa6\x837\x95\x15\
\x18\xfd\x81\xd1\x1fy\x01f=&2\x916\xf4\xa7t\xf3\xa0\x04\x18a\xd9\x1a\xed\xa0\
\x94\xccz\xff/\xd1\xb2\x99\xeb9@\x87\xc4\x1aL\xb1'\x07,a1\x81\xb9\xda\xa7Uj\
\xea7C\x8c\xeb\xf3\x12\xc5\xbaF@\xdf\x06\xfb\xaa\xe9\xc0J'K\xec\x05\xafK8\
\xb4%M\x8aV\x00\x00\x00\x00IEND\xaeB`\x82"

class vXmlHum(vtXmlDomReg):
    TAGNAME_REFERENCE='name'
    TAGNAME_ROOT='humanroot'
    APPL_REF=['vGlobals','vMsg']
    def __init__(self,attr='id',skip=[],synch=False,appl='vHum',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
        self.hums=None
        self.personnel=None
        self.grps=None
        try:
            self.dt=wxDateTime()
            oRoot=vXmlNodeHumRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeHumRoot()
            oGroups=vGroupsNode()
            oGroup=vGroupNode()
            oUsers=vUsersNode()
            oUser=vUserNode()
            oUserPersonal=vUserNodePersonal()
            oUserPrivate=vUserNodePrivate()
            oUserCash=vUserNodeCash()
            oUserCompany=vUserNodeCompany()
            oUserAccess=vUserNodeAccess()
            oUserSecurity=vUserNodeSecurity()
            oUserWorkTimes=vUserNodeWorkingTimes()
            oUserAccuTimes=vUserNodeAccuTimes()
            oUserSalary=vUserNodeSalary()
            oUserSalaryScheme=vUserNodeSalaryScheme()
            oAddr=vtXmlNodeAddrSingle()
            oAddrs=vUserNodeAddresses()
            #oAddrs=vtXmlNodeAddrMultiple()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            oAttrCfgML=vtXmlNodeAttrCfgML()
            oCalc=vtXmlNodeCalc()
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oGroups,True)
            self.RegisterNode(oGroup,False)
            self.RegisterNode(oUsers,True)
            self.RegisterNode(oUser,False)
            self.RegisterNode(oUserPersonal,False)
            self.RegisterNode(oUserPrivate,False)
            self.RegisterNode(oUserCash,False)
            self.RegisterNode(oUserCompany,False)
            self.RegisterNode(oUserSalary,False)
            self.RegisterNode(oUserSalaryScheme,False)
            self.RegisterNode(oUserWorkTimes,False)
            self.RegisterNode(oUserAccuTimes,False)
            self.RegisterNode(oUserAccess,False)
            self.RegisterNode(oUserSecurity,False)
            self.RegisterNode(oAddr,False)
            self.RegisterNode(oAddrs,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(oAttrCfgML,False)
            self.RegisterNode(oCalc,False)
            self.LinkRegisteredNode(oGroups,oGroup)
            self.LinkRegisteredNode(oGroup,oLog)
            self.LinkRegisteredNode(oUsers,oUser)
            self.LinkRegisteredNode(oUser,oUserPersonal)
            self.LinkRegisteredNode(oUser,oUserCash)
            self.LinkRegisteredNode(oUser,oUserCompany)
            self.LinkRegisteredNode(oUser,oUserPrivate)
            self.LinkRegisteredNode(oUser,oAddr)
            self.LinkRegisteredNode(oUser,oAddrs)
            self.LinkRegisteredNode(oUser,oUserAccuTimes)
            self.LinkRegisteredNode(oUser,oUserWorkTimes)
            self.LinkRegisteredNode(oUser,oUserSalary)
            self.LinkRegisteredNode(oUser,oLog)
            self.LinkRegisteredNode(oUser,oUserAccess)
            self.LinkRegisteredNode(oUser,oUserSecurity)
            self.LinkRegisteredNode(oCfg,oUserSalaryScheme)
            
            
            oStateMachineLinker=veStateStateMachineLinker({
                'salary':[('salState','smSalary')],
                })
            oStateMachine=veStateStateMachine()
            
            flt={None:[('GetClassificationNext',vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL),
                    ]}
            oSMsalary=veStateStateMachine(tagName='smSalary',
                filter=flt,
                trans={None:{None:_('salary'),'GetClassificationNext':_('valid by')} },
                cmd=oUserSalary.GetActionCmdDict())
            oState=veStateState()
            oTrans=veStateTransition(filter=flt)
            oFilter=vtXmlNodeFilter()
            
            oMsgs=vtXmlNodeMessages()
            oMsg=vtXmlNodeMsg()
            
            self.RegisterNode(oMsgs,True)
            self.RegisterNode(oMsg,True)
            self.RegisterNode(oStateMachineLinker,False)
            self.RegisterNode(oStateMachine,False)
            self.RegisterNode(oSMsalary,False)
            self.RegisterNode(oState,False)
            self.RegisterNode(oTrans,False)
            self.RegisterNode(oFilter,False)
            
            self.LinkRegisteredNode(oCfg,oStateMachineLinker)
            self.LinkRegisteredNode(oStateMachineLinker,oStateMachine)
            self.LinkRegisteredNode(oStateMachineLinker,oSMsalary)
            self.LinkRegisteredNode(oSMsalary,oState)
            self.LinkRegisteredNode(oMsgs,oMsg)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def __genIdsOld__(self,c,node,ids):
        if c.name in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
        elif c.name in ['groups','users','group','user','cfg']:
            ids.CheckId(c,self,None)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'humans')
        #return self.getRoot()
    #def New(self,revision='1.0',root='humanroot',bConnection=False,bAcquire=True):
    #    vtXmlDomReg.New(self,revision,root,bConnection=bConnection,bAcquire=bAcquire)
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        vtXmlDomReg.ShutDown(self)
        self.hums=None
        self.personnel=None
        self.grps=None
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #vtXmlDomReg.New(self,revision,root)
        elemHum=self.createSubNode(self.getRoot(),'humans')
        if bConnection==False:
            elem=self.createSubNode(elemHum,'groups')
            self.setAttribute(elem,self.attr,'')
            self.checkId(elem)
            elem=self.createSubNode(elemHum,'users')
            self.setAttribute(elem,self.attr,'')
            self.checkId(elem)
            self.processMissingId()
            self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
    def __initDaysTimes__(self):
        lst=[]
        for i in range(7):
            lst.append([])
        return lst
    def __initDictTimes__(self):
        dictTimes={'normal':{},'over':{},'flex':{}}
        #dictTimes['normal']['dft']=self.__initDaysTimes__()
        #dictTimes['over']['dft']=self.__initDaysTimes__()
        #dictTimes['flex']['dft']=self.__initDaysTimes__()
        return dictTimes
    def GetDictTimes(self,node):
        if node is None:
            return
        sTag=self.getTagName(node)
        if sTag=='workingtimes':
            nodeTimes=node
        else:
            nodeTimes=self.getChild(node,'workingtimes')
        if nodeTimes is None:
            return self.__initDictTimes__()
        dictTimes=self.__initDictTimes__()
        for k in [u'normaltime',u'overtime',u'flextime']:
            node=self.getChild(nodeTimes,k)
            if node is None:
                continue
            dTmp={}
            for nodeRange in self.getChilds(node,'range'):
                #sDay=self.doc.getChild(nodeRange,'day')
                sStart=self.getNodeText(nodeRange,'start')
                sEnd=self.getNodeText(nodeRange,'end')
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'start:%s end:%s'%(sStart,sEnd),callstack=False)
                try:
                    self.dt.ParseFormat(sStart,'%Y%m%d')
                    sTmp=self.dt.FormatISODate()
                    self.dt.ParseFormat(sEnd,'%Y%m%d')
                    sTmp=self.dt.FormatISODate()
                except:
                    continue
                sPercent=self.getNodeText(nodeRange,'percent')
                try:
                    iPercent=int(sPercent)
                    if iPercent<0:
                        iPercent=0
                    elif iPercent>100:
                        iPercent=100
                except:
                    iPercent=100
                #lstDays=self.__initDaysTimes__()
                lstDays=[]
                sHours=self.getNodeText(nodeRange,'hours')
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'days:%s'%(sHours),callstack=False)
                strsDay=sHours.split(';')
                for sDay in strsDay:
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'hours:%s'%(sDay),callstack=False)
                    strsHr=sDay.split(',')
                    lstDay=[]
                    for sHr in strsHr:
                        try:
                            iStartHr=int(sHr[0:2])
                            iStartMn=int(sHr[2:4])
                            iEndHr=int(sHr[5:7])
                            iEndMn=int(sHr[7:9])
                            lstDay.append(['%02d%02d'%(iStartHr,iStartMn),
                                        '%02d%02d'%(iEndHr,iEndMn)])
                        except:
                            pass
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'day:%s'%(repr(lstDay)),callstack=False)
                    def compFunc(a,b):
                        return cmp(a[0],b[0])
                    lstDay.sort(compFunc)
                            
                    lstDays.append(lstDay)
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'days:%s'%(repr(lstDays)),callstack=False)
                try:
                    #iDay=int(sDay)
                    if k==u'normaltime':
                        dictTimes['normal'][(sStart,sEnd)]={'days':lstDays,'percent':iPercent}
                    elif k==u'overtime':
                        dictTimes['over'][(sStart,sEnd)]={'days':lstDays}
                    elif k==u'flextime':
                        dictTimes['flex'][(sStart,sEnd)]={'days':lstDays}
                except:
                    pass
        return dictTimes
    def SetDictTimes(self,node,dictTimes):
        if node is None:
            return
        sTag=self.getTagName(node)
        if sTag=='workingtimes':
            nodeTimes=node
        else:
            nodeTimes=self.getChildForced(node,'workingtimes')
        if nodeTimes is None:
            return 
        for k in [u'normaltime',u'overtime',u'flextime']:
            node=self.getChildForced(nodeTimes,k)
            if node is None:
                continue
            if k==u'normaltime':
                sKey='normal'
            elif k==u'overtime':
                sKey='over'
            elif k==u'flextime':
                sKey='flex'
            else:
                continue
            if self.verbose:
                vtLog.vtLogCallDepth(self,'k:%s key:%s'%(k,sKey),callstack=False)
            childs=self.getChilds(node,'range')
            iChilds=len(childs)
            dRanges=dictTimes[sKey]
            keys=dRanges.keys()
            keys.sort()
            iLen=len(keys)
            if self.verbose:
                vtLog.vtLogCallDepth(self,'childs:%s len:%s'%(iChilds,iLen),callstack=False)
            for i in range(iLen):
                try:
                    sStart=keys[i][0]
                    sEnd=keys[i][1]
                    self.dt.ParseDate(sStart)
                    sTmp=self.dt.Format('%Y%m%d')
                    self.dt.ParseDate(sEnd)
                    sTmp=self.dt.Format('%Y%m%d')
                except Exception,list:
                    continue
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'start:%s end:%s'%(sStart,sEnd),callstack=False)
                
                try:
                    child=childs[i]
                except:
                    child=self.createSubNode(node,'range')
                
                self.setNodeText(child,'start',sStart)
                self.setNodeText(child,'end',sEnd)
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'key:%s'%(keys[i]),callstack=False)
                days=dRanges[keys[i]]['days']
                sDays=[]
                for day in days:
                    sHours=[]
                    for hr in day:
                        sHours.append(hr[0]+':'+hr[1])
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'hours:%s'%(sHours),callstack=False)
                    sDays.append(','.join(sHours))
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'days:%s'%(sDays),callstack=False)
                self.setNodeText(child,'hours',';'.join(sDays))
                try:
                    sPercent='%s'%dRanges[keys[i]]['percent']
                except:
                    sPercent=''
                if len(sPercent)>0:
                    self.setNodeText(child,'percent',sPercent)
                    
            for i in range(iLen,iChilds):
                self.deleteNode(childs[i],node)
        return 0
    def ConvertDictTimesToDayRanges(self,dictTimes):
        dRanges={}
        for k in dictTimes.keys():
            dRanges[k]=[]
            for rngKey in dictTimes[k].keys():
                rng=dictTimes[k][rngKey]['days']
                lst=[]
                for day in rng:
                    sum=0.0
                    for hr in day:
                        self.dt.ParseFormat(hr[1],'%H%M')
                        fHr=self.dt.GetHour()+self.dt.GetMinute()/60.0
                        self.dt.ParseFormat(hr[0],'%H%M')
                        fHr-=self.dt.GetHour()+self.dt.GetMinute()/60.0
                        sum+=fHr
                    lst.append(sum)
                for i in range(len(lst),7):
                    lst.append(0.0)
                try:
                    #self.dt.ParseDate(rngKey[:10])
                    #sStart=self.dt.Format('%Y%m%d')
                    #self.dt.ParseDate(rngKey[-10:])
                    #sEnd=self.dt.Format('%Y%m%d')
                    sStart=rngKey[0]
                    sEnd=rngKey[1]
                    
                except Exception,list:
                    continue
                try:
                    iPercent=int(dictTimes[k][rngKey]['percent'])
                except:
                    iPercent=100
                dRanges[k].append((sStart,sEnd,lst,iPercent))
        return dRanges
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL,
                'group':self.ACL_MSK_NORMAL,
                'user':self.ACL_MSK_NORMAL,
                },['all','group','user']
    def GetPossibleFilterOld(self):
        return {'group':[
                            ('name',self.FILTER_TYPE_STRING),
                            ('desc',self.FILTER_TYPE_STRING),
                            ('startdatetime',self.FILTER_TYPE_DATE_TIME),
                            ('enddatetime',self.FILTER_TYPE_DATE_TIME),
                            ],
                 'user':[
                            ('name',self.FILTER_TYPE_STRING),
                            ('firstname',self.FILTER_TYPE_STRING),
                            ('surname',self.FILTER_TYPE_STRING),
                            ('startdatetime',self.FILTER_TYPE_DATE_TIME),
                            ('enddatetime',self.FILTER_TYPE_DATE_TIME),
                            ],
                }
    def GetTranslationOld(self,kind,name):
        _(u'name'),_(u'desc'),_(u'startdatetime'),_(u'enddatetime')
        _(u'firstname'),_(u'surname')
        return _(name)
    def GetGroupLst(self):
        grps=[]
        grpsNode=self.getChild(self.getBaseNode(),'groups')
        if grpsNode is not None:
            for c in self.getChilds(grpsNode,'group'):
                id=self.getKey(c)
                sName=self.getNodeText(c,'name')
                grps.append((sName,id))
        return grps
    def __checkMember__(self,node,oUsr,grpId=-1,usrs=[]):
        sTag=self.getTagName(node)
        if sTag!='user':
            return 0
        s=self.getNodeText(node,'grps')
        try:
            ids=map(long,s.split(','))
            ids.index(grpId)
            if oUsr.GetInActive(node)==False:
                usrs.append(self.getKey(node))
        except:
            pass
        return 0
    def GetGroupMemberLst(self,grpId):
        usrs=[]
        grps={}
        grpsNode=self.getChild(self.getBaseNode(),'groups')
        if grpsNode is not None:
            for c in self.getChilds(grpsNode,'group'):
                id=self.getKey(c)
                sName=self.getNodeText(c,'name')
                grps[long(id)]=sName
        usrsNode=self.getChild(self.getBaseNode(),'users')
        if usrsNode is not None:
            oUsr=self.GetReg('user')
            self.procChildsKeys(usrsNode,self.__checkMember__,oUsr,grpId=long(grpId),usrs=usrs)
        #print usrs
        return usrs
    def GetSalarySchemeClassificationLst(self):
        o=self.GetRegisteredNode('salaryScheme')
        if o is not None:
            return o.GetClassificationLst()
        return None
    def Build(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        vtXmlDomReg.Build(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        self.BuildLang()
    def BuildLang(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        vtXmlDomReg.BuildLang(self)
        tmp=self.getChildByLst(self.getBaseNode(),['cfg','salaryScheme'])
        if tmp is not None:
            o=self.GetRegisteredNode('salaryScheme')
            if o is not None:
                o.CalcSalaryLimit(tmp)
                lLv=o.CalcClassificationLst(tmp)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%s'%vtLog.pformat(lLv),self.appl)
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def IsNodeAclOk(self,node,iAcl2Chk):
        bOk=vtXmlDomReg.IsNodeAclOk(self,node,iAcl2Chk)
        if bOk==False:
            if node is not None:
                if self.getTagName(node)=='user':
                    c=self.getChild(node,'security')
                    if c is not None:
                        return vtXmlDomReg.IsNodeAclOk(self,c,iAcl2Chk)
        return bOk
    def IsAccessOk(self,node,objLogin,iAcl):
        bOk=vtXmlDomReg.IsAccessOk(self,node,objLogin,iAcl)
        if bOk==False:
            if node is not None:
                if self.getTagName(node)=='user':
                    c=self.getChild(node,'security')
                    if c is not None:
                        return vtXmlDomReg.IsAccessOk(self,c,objLogin,iAcl)
        return bOk
    def IsLoggedInSelf(self,node,usrId):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(self.getKey(node)),self)
        if node is not None:
            if self.getTagName(node)=='user':
                tmp=node
            else:
                tmp=self.getParent(node)
            iUsrId,appl=self.convForeign(usrId,-1)
            if self.getKeyNum(tmp)==iUsrId:
                return True
        return False
    def CreateIds(self,attr='id'):
        self.hums=None
        self.personnel=None
        self.grps=None
        vtXmlDomReg.CreateIds(self,attr)
    def GenerateSortedIds(self):
        self.GenerateSortedUsrIds()
        self.GenerateSortedPersonnelIds()
        self.GenerateSortedGrpIds()
    def GenerateSortedUsrIds(self):
        self.hums=self.getSortedNodeIds(['users'],
                        'user','id',[('name','%s')])
    def GenerateSortedPersonnelIds(self):
        self.personnel=self.getSortedNodeIds(['users'],
                        'user','persID',[('name','%s')])
    def GenerateSortedGrpIds(self):
        self.grps=self.getSortedNodeIds(['groups'],'group','id',
                        [('name','%s')])
    def GetSortedUsrIds(self):
        try:
            return self.hums
        except:
            self.GenerateSortedUsrIds()
            return self.hums
    def GetSortedPersonnelIds(self):
        try:
            return self.personnel
        except:
            self.GenerateSortedPersonnelIds()
            return self.personnel
    def GetSortedGrpIds(self):
        try:
            return self.grps
        except:
            self.GenerateSortedGrpIds()
            return self.grps
