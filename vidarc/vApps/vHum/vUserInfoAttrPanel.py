#Boa:FramePanel:vUserInfoAttrPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoAttrPanel.py,v 1.7 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputAttrPanel
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import cStringIO
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM
import string,sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VUSERINFOATTRPANEL, wxID_VUSERINFOATTRPANELVIATTR, 
] = [wx.NewId() for _init_ctrls in range(2)]


# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vUserInfoAttrPanel(wx.Panel,vtXmlNodePanel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VUSERINFOATTRPANEL,
              name=u'vUserInfoAttrPanel', parent=prnt, pos=wx.Point(237, 84),
              size=wx.Size(511, 229), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(503, 202))
        self.SetAutoLayout(True)

        self.viAttr = vidarc.tool.input.vtInputAttrPanel.vtInputAttrPanel(id=wxID_VUSERINFOATTRPANELVIATTR,
              name=u'viAttr', parent=self, pos=wx.Point(0, 0), size=wx.Size(500,
              200), style=0)
        self.viAttr.SetConstraints(LayoutAnchors(self.viAttr, True, True, True,
              True))

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        #self.doc=None
        #self.node=None
        self.bModified=False
            
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.viAttr.__Clear__()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def ClearOld(self):
        vtXmlNodePanel.Clear(self)
        self.viAttr.Clear()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viAttr.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viAttr.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viAttr.GetNode(node)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viAttr.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viAttr.Cancel()
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.viAttr.Enable(False)
        else:
            self.viAttr.Enable(True)
