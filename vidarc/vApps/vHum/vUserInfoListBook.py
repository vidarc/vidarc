#----------------------------------------------------------------------------
# Name:         vUserInfoListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vUserInfoListBook.py,v 1.4 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import images_data

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.vApps.vHum.vUserInfoAttrPanel as vUserInfoAttrPanel
    import vidarc.vApps.vHum.vUserInfoAccessPanel as vUserInfoAccessPanel
    import vidarc.vApps.vHum.vUserInfoWorkingTimesPanel as vUserInfoWorkingTimesPanel
    import vidarc.vApps.vHum.vUserInfoSecurityPanel as vUserInfoSecurityPanel
    #import vidarc.vApps.vPrjTimer.vReportBuildPanel as vReportBuildPanel
    #import vidarc.tool.time.vTmLimit as vTmLimit
    from vidarc.tool.input.vtInputAttrCfgMLPanel import *
    import types
    import vidarc.tool.log.vtLog as vtLog
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vUserInfoListBook(wx.Notebook,vtXmlNodePanel):
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)
        vtXmlNodePanel.__init__(self)

    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        self._init_ctrls(parent,id, pos, size, style, name)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.verbose=VERBOSE
        
        edit=[None,None,u'UserInfoWorkingTimes',u'UserInfoAccess',u'UserInfoSecurity']
        editName=[u'Attr',u'Bank',u'Times',u'Access',u'Security']
        editBmp=[vtArt.Attr,vtArt.Cash]
        editBase=[['cfg','attr'],['cfg','cash']]
        il = wx.ImageList(16, 16)
        i=0
        for e in edit:
            if e is not None:
                f=getattr(images_data, 'get%sBitmap' % e)
                bmp = f()
            else:
                bmp=vtArt.getBitmap(editBmp[i])
            il.Add(bmp)
            i+=1
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            if e is not None:
                s='v%sPanel'%e
                f=getattr(eval('v%sPanel'%e),'v%sPanel'%e)
                panel=f(self,wx.NewId(),
                        pos=(0,0),size=(190,340),style=0,name=s)
            else:
                s='vPanel'+editName[i]
                panel=vtInputAttrCfgMLPanel(self,wx.NewId(),
                        pos=(0,0),size=(190,340),style=0,name=s)
                panel.SetTagNames2Base(editBase[i])
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
        
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        for pn in self.lstPanel:
            pn.__Clear__()
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        for pn in self.lstPanel:
            pn.Clear()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        for pn in self.lstPanel:
            pn.__Lock__(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDoc(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
        
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            for pn in self.lstPanel:
                pn.SetNode(node)
        except:
            self.__logTB__()
        #if self.__isModified__():
        #    # ask
        #    dlg=wx.MessageDialog(self,_(u'Do you to apply modified data?') ,
        #                _(u'vHuman User'),
        #                wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #    if dlg.ShowModal()==wx.ID_YES:
        #        self.GetNode()
        #        #wx.PostEvent(self,vgpDataChanged(self,self.node))
        #self.SetModified(False)
        
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            for pn in self.lstPanel:
                pn.GetNode()
        except:
            self.__logTB__()
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = event.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
    