#Boa:FramePanel:vGrpInfoPanel
#----------------------------------------------------------------------------
# Name:         vGroupInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vGroupInfoPanel.py,v 1.13 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import cStringIO

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import __config__
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM
    #from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vHum.vGroupInfoListBook import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import string,sys
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VGRPINFOPANEL, wxID_VGRPINFOPANELGCBBAPPLY, 
 wxID_VGRPINFOPANELGCBBCANCEL, wxID_VGRPINFOPANELLBLDESCRIPTION, 
 wxID_VGRPINFOPANELLBLID, wxID_VGRPINFOPANELLBLLOGIN, 
 wxID_VGRPINFOPANELTXTDESC, wxID_VGRPINFOPANELTXTID, 
 wxID_VGRPINFOPANELTXTLOGIN, 
] = [wx.NewId() for _init_ctrls in range(9)]

import images

# defined event for vgpXmlTree item selected
wxEVT_VGPGROUP_INFO_CHANGED=wx.NewEventType()
def EVT_VGPGROUP_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPGROUP_INFO_CHANGED,func)
class vgpGroupInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPGROUP_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPGROUP_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpGroupInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vGrpInfoPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogin, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtLogin, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfo, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)

    def _init_coll_bxsNameID_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsName, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsID, 0, border=4, flag=wx.EXPAND | wx.TOP)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDescription, 0, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 0, border=4, flag=wx.EXPAND)

    def _init_coll_bxsID_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblId, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtId, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbApply, 0, border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsNameID, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsDesc, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsID = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsInfo = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.bxsNameID = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsID_Items(self.bxsID)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)
        self._init_coll_bxsNameID_Items(self.bxsNameID)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGRPINFOPANEL, name=u'vGrpInfoPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(448, 317),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(440, 290))
        self.SetAutoLayout(True)

        self.lblLogin = wx.StaticText(id=wxID_VGRPINFOPANELLBLLOGIN,
              label=_(u'name'), name=u'lblLogin', parent=self, pos=wx.Point(0,
              4), size=wx.Size(52, 21), style=wx.ALIGN_RIGHT)
        self.lblLogin.SetMinSize(wx.Size(-1, -1))

        self.txtLogin = wx.TextCtrl(id=wxID_VGRPINFOPANELTXTLOGIN,
              name=u'txtLogin', parent=self, pos=wx.Point(56, 4),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtLogin.SetMinSize(wx.Size(-1, -1))

        self.lblId = wx.StaticText(id=wxID_VGRPINFOPANELLBLID, label=_(u'ID'),
              name=u'lblId', parent=self, pos=wx.Point(0, 29), size=wx.Size(52,
              21), style=wx.ALIGN_RIGHT)
        self.lblId.SetMinSize(wx.Size(-1, -1))

        self.txtId = wx.TextCtrl(id=wxID_VGRPINFOPANELTXTID, name=u'txtId',
              parent=self, pos=wx.Point(56, 29), size=wx.Size(100, 21),
              style=wx.TE_READONLY, value=u'')
        self.txtId.Enable(False)
        self.txtId.SetMinSize(wx.Size(-1, -1))
        self.txtId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VGRPINFOPANELTXTID)

        self.lblDescription = wx.StaticText(id=wxID_VGRPINFOPANELLBLDESCRIPTION,
              label=_(u'description'), name=u'lblDescription', parent=self,
              pos=wx.Point(160, 4), size=wx.Size(196, 13), style=0)
        self.lblDescription.SetMinSize(wx.Size(-1, -1))

        self.txtDesc = wx.TextCtrl(id=wxID_VGRPINFOPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(160, 17),
              size=wx.Size(196, 63), style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetMinSize(wx.Size(-1, -1))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGRPINFOPANELGCBBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'gcbbApply', parent=self, pos=wx.Point(364, 4),
              size=wx.Size(72, 30), style=0)
        self.gcbbApply.SetMinSize(wx.Size(-1, -1))
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VGRPINFOPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGRPINFOPANELGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(364, 38),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGRPINFOPANELGCBBCANCEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vHuman Group'),
                lWidgets=[])
        self.verbose=VERBOSE
        
        self.vlbGrpInfos=vGroupInfoListBook(self,name='vlbGrpInfo',
                                    pos=wx.Point(0,76),size=wx.Size(450,380))
        self.fgsData.AddWindow(self.vlbGrpInfos, 1, border=4, flag=wx.EXPAND|wx.ALL)
        
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.vlbGrpInfos.GetModified():
            return True
        return False
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.vlbGrpInfos.__Clear__()
            self.txtLogin.SetValue('')
            self.txtId.SetValue('')
            self.txtDesc.SetValue('')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.vlbGrpInfos.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            sLogin=self.doc.getNodeText(node,'name')
            sAttrId=self.doc.getAttribute(node,'id')
            sDesc=self.doc.getNodeText(node,'desc')
            self.sOldId=sAttrId
            
            self.txtLogin.SetValue(sLogin)
            self.txtId.SetValue(sAttrId)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtId.SetStyle(0, len(sAttrId), wx.TextAttr("BLACK", bkgCol))
            self.txtDesc.SetValue(sDesc)
            
            self.vlbGrpInfos.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            sLogin=self.txtLogin.GetValue()
            sDesc=self.txtDesc.GetValue()
            sAttrId=self.txtId.GetValue()
            
            self.doc.setNodeText(node,'name',sLogin)
            if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                ids=self.doc.getIds()
                if ids is not None:
                    sOldId=self.doc.getAttribute(node,'id')
                    ids.RemoveId(sOldId)
                    ret,n=ids.GetIdStr(sAttrId)
                    if ret<0:
                        bOk=False
                    else:
                        if n is None:
                            bOk=True
                        else:
                            if n==node:
                                bOk=True
                            else:
                                bOk=False
                    if bOk==False:
                        # fault id is not possible
                        ids.AddNewNode(node)
                        sAttr=ids.ConvertId2Str(node,self.doc)
                        self.txtId.SetValue(sAttr)
                        self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
                    else:
                        self.doc.setAttribute(node,'id',sAttrId)
                        sAttr=ids.ConvertId2Str(node,self.doc)
                        self.txtId.SetValue(sAttr)
                        self.doc.setAttribute(node,'id',sAttr)
                        ids.CheckId(node,self.doc)
                        ids.ClearMissing()
                    
            #self.doc.setNodeText(node,'surname',sSurName)
            self.doc.setNodeText(node,'desc',sDesc)
            #self.doc.setNodeText(self.node,'startdatetime',sStartDT)
            #self.doc.setNodeText(self.node,'enddatetime',sEndDT)
            
            self.vlbGrpInfos.GetNode(node)
        except:
            self.__logTB__()
    def OnGcbbApplyButton(self, event):
        self.GetNode()
        #sAttrId=self.node.getAttribute('id')
        wx.PostEvent(self,vgpGroupInfoChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetModified(False)
        self.vlbGrpInfos.SetModified(False)
        self.SetNode(self.node)
        event.Skip()

    def OnTxtIdText(self, event):
        if self.doc is None:
            return
        ids=self.doc.getIds()
        sAttr=self.txtId.GetValue()
        ret,n=ids.GetIdStr(sAttr)
        if ret<0:
            ids.AddNewNode(self.node)
            sId=ids.ConvertId2Str(self.node,self.doc)
            self.txtId.SetValue(sId)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            return
        if n is None:
            bOk=True
        else:
            if self.doc.isSame(n,self.node):
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.gcbbApply.Enable(False)
            self.txtLogin.Enable(False)
            self.txtId.SetEditable(False)
            self.txtDesc.Enable(False)
        else:
            self.gcbbApply.Enable(True)
            if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                self.txtLogin.Enable(True)
                self.txtId.SetEditable(True)
                self.txtDesc.Enable(True)
            else:
                self.txtLogin.Enable(False)
                self.txtId.SetEditable(False)
                self.txtDesc.Enable(False)
        self.vlbGrpInfos.Lock(flag)
