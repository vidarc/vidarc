#Boa:MDIChild:vHumanMDIFrame
#----------------------------------------------------------------------------
# Name:         vHumanMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060602
# CVS-ID:       $Id: vHumanMDIFrame.py,v 1.12 2008/03/26 23:10:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vHum.images as images
import vidarc.tool.log.vtLog as vtLog

try:
    import vidarc.vApps.vHum.__init__ as plgInit
    from vidarc.vApps.vHum.vHumanPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

DOMAINS=plgInit.DOMAINS
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vHumanMDIFrame(parent, id, name, pos, size)

[wxID_VHUMANMDIFRAME, wxID_VHUMANMDIFRAMEPANEL1, 
 wxID_VHUMANMDIFRAMESTATUSBAR1, wxID_VHUMANMDIFRAMETEXTCTRL1, 
 wxID_VHUMANMDIFRAMETOOLBAR1, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vHumanMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=-1,
              name=u'vHumanMDIFrame', parent=prnt, pos=wx.DefaultPosition,
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vHuman')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Human')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vHumanPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vHumanPlugIn')
            self.pn.OpenCfgFile('vHumCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            EVT_NET_XML_SET_NODE(self.netDocMain,self.OnSetNode)
            EVT_NET_XML_DEL_NODE(self.netDocMain,self.OnDelNode)
            EVT_NET_XML_ADD_NODE_RESPONSE(self.netDocMain,self.OnAddResponse)
            EVT_NET_XML_MOVE_NODE_RESPONSE(self.netDocMain,self.OnMoveResponse)
            
            if 1:
                self.BindNetDocEvents(None,None)
                self.netMaster.BindEvents(
                        funcStart=self.OnMasterStart,
                        funcFinish=self.OnMasterFinish,
                        funcShutDown=self.OnMasterShutDown)
            else:
                EVT_NET_XML_SYNCH_START(self.netDocMain,self.OnSynchStart)
                EVT_NET_XML_SYNCH_PROC(self.netDocMain,self.OnSynchProc)
                EVT_NET_XML_SYNCH_FINISHED(self.netDocMain,self.OnSynchFinished)
                EVT_NET_XML_SYNCH_ABORTED(self.netDocMain,self.OnSynchAborted)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools , self.mnAnalyse)
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        #self.Layout()
        #self.Refresh()
    #def __del__(self):
    #    self.pn.Destroy()
    def __getPluginImage__(self):
        return images.getPluginImage()
    def __getPluginBitmap__(self):
        return images.getPluginBitmap()
