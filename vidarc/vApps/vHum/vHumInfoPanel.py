#Boa:FramePanel:vgpHumInfo

import wx
import wx.lib.buttons
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM
import string

[wxID_VGPHUMINFO, wxID_VGPHUMINFOGCBBAPPLY, wxID_VGPHUMINFOGCBBCANCEL, 
 wxID_VGPHUMINFOLBLFIRSTNAME, wxID_VGPHUMINFOLBLLOGIN, 
 wxID_VGPHUMINFOLBLSTARTDT, wxID_VGPHUMINFOLBLSURNAME, 
 wxID_VGPHUMINFOGRIDATTR ,
 wxID_VGPHUMINFOTXTENDDT, wxID_VGPHUMINFOTXTFIRSTNAME, 
 wxID_VGPHUMINFOTXTLOGIN, wxID_VGPHUMINFOTXTSURNAME, 
] = [wx.NewId() for _init_ctrls in range(11)]


#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

# defined event for vgpXmlTree item selected
wxEVT_VGPHUMAN_INFO_CHANGED=wxNewEventType()
def EVT_VGPHUMAN_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPHUMAN_INFO_CHANGED,func)
class vgpHumanInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPHUMAN_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPHUMAN_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpHumanInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vgpHumInfo(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGPHUMINFO, name=u'vgpHumInfo',
              parent=prnt, pos=wx.Point(244, 200), size=wx.Size(464, 278),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(456, 251))

        self.lblLogin = wx.StaticText(id=wxID_VGPHUMINFOLBLLOGIN,
              label=u'login', name=u'lblLogin', parent=self, pos=wx.Point(16,
              8), size=wx.Size(24, 13), style=0)

        self.txtLogin = wx.TextCtrl(id=wxID_VGPHUMINFOTXTLOGIN,
              name=u'txtLogin', parent=self, pos=wx.Point(48, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblSurName = wx.StaticText(id=wxID_VGPHUMINFOLBLSURNAME,
              label=u'surname', name=u'lblSurName', parent=self,
              pos=wx.Point(16, 40), size=wx.Size(40, 13), style=0)

        self.txtSurName = wx.TextCtrl(id=wxID_VGPHUMINFOTXTSURNAME,
              name=u'txtSurName', parent=self, pos=wx.Point(80, 40),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblFirstName = wx.StaticText(id=wxID_VGPHUMINFOLBLFIRSTNAME,
              label=u'first name', name=u'lblFirstName', parent=self,
              pos=wx.Point(192, 40), size=wx.Size(45, 13), style=0)

        self.txtFirstName = wx.TextCtrl(id=wxID_VGPHUMINFOTXTFIRSTNAME,
              name=u'txtFirstName', parent=self, pos=wx.Point(256, 40),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblStartDT = wx.StaticText(id=wxID_VGPHUMINFOLBLSTARTDT,
              label=u'Start Date Time', name=u'lblStartDT', parent=self,
              pos=wx.Point(8, 96), size=wx.Size(74, 13), style=0)

        self.txtEndDT = wx.StaticText(id=wxID_VGPHUMINFOTXTENDDT,
              label=u'End Date Time', name=u'txtEndDT', parent=self,
              pos=wx.Point(8, 120), size=wx.Size(71, 13), style=0)

        self.gridAttr = wx.grid.Grid(id=wxID_VGPHUMINFOGRIDATTR,
              name=u'gridAttr', parent=self, pos=wx.Point(8, 144),
              size=wx.Size(272, 168), style=0)
        self.gridAttr.SetColLabelSize(20)
        self.gridAttr.SetRowLabelSize(120)
        
        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGPHUMINFOGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(384, 8), size=wx.Size(68, 30), style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VGPHUMINFOGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGPHUMINFOGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbCancel',
              parent=self, pos=wx.Point(384, 48), size=wx.Size(72, 30),
              style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGPHUMINFOGCBBCANCEL)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.node=None

        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbApply.SetBitmapLabel(img)
        
        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)

        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbDel.SetBitmapLabel(img)

        self.zdtlStartGM=vTimeDateTimeGM.vTimeDateTimeGM(self,wxNewId(),
                    pos=(80,96),size = (130,22))
        
        self.zdtlEndGM=vTimeDateTimeGM.vTimeDateTimeGM(self,wxNewId(),
                    pos=(80,120),size = (130,22))
        
        # setup grid
        self.iMaxAttrs=20
        self.gridAttr.CreateGrid(self.iMaxAttrs,1)
        self.gridAttr.SetColLabelValue(0, "Value")
        self.gridAttr.SetColSize(0, 120)
        for i in range(0,self.iMaxAttrs):
            s="%d"%i
            self.gridAttr.SetRowLabelValue(i,s)
            self.gridAttr.SetCellValue(i,0,'')

    def SetNode(self,doc,node):
        if node is None:
            self.txtLogin.SetValue('')
            self.txtSureName.SetValue('')
            self.txtFirstName.SetValue('')
            #self.txtLogText.SetValue('')
            self.zdtlStartGM.SetValue('')
            self.zdtlEndGM.SetValue('')
            #for i in range(0,self.iMaxAttrs):
            #    s="%d"%i
            #    self.gridAttr.SetRowLabelValue(i,s)
            #    self.gridAttr.SetCellValue(i,0,'')
            return
        self.node=node
        self.doc=doc
        sLogin=self.doc.getNodeText(node,'name')
        sSurName=self.doc.getNodeCData(node,'surname')
        sFirstName=self.doc.getNodeCData(node,'firstname')
        sStartDT=self.doc.getNodeText(node,'startdatetime')
        sEndDT=self.doc.getNodeText(node,'enddatetime')
        
        self.txtLogin.SetValue(sLogin)
        self.txtSurName.SetValue(sSurName)
        self.txtFirstName.SetValue(sFirstName)
        #self.txtLogText.SetValue('')
        self.zdtlStartGM.SetValue(sStartDT)
        self.zdtlEndGM.SetValue(sEndDT)

        # setup attributes
        attrNode=self.doc(node,'attributes')
        if attrNode is not None:
            attrs=[]
            for o in self.doc.getChilds(attrsNode):
                    s=self.doc.getText(o)
                    attrs.append((self.doc.getTagName(o),s))
        else:
            attrs=[('manager','')]
        self.gridAttr.SetColLabelValue(0, "Value")
        self.gridAttr.SetColSize(0, 120)
        
        for i in range(0,len(attrs)):
            if i < self.iMaxAttrs:
                self.gridAttr.SetRowLabelValue(i,attrs[i][0])
                self.gridAttr.SetCellValue(i,0,attrs[i][1])
        pass
    def GetNode(self,node):
        if node is None:
            return ''
        sLogin=self.txtLogin.GetValue()
        sSurName=self.txtSurName.GetValue()
        sFirstName=self.txtFirstName.GetValue()
        sStartDT=self.zdtlStartGM.GetValue()
        sEndDT=self.zdtlEndGM.GetValue()
        
        self.doc.setNodeText(node,'name',sLogin)
        self.doc.setNodeCData(node,'surname',sSurName)
        self.doc.setNodeCData(node,'firstname',sFirstName)
        self.doc.setNodeText(node,'startdatetime',sStartDT)
        self.doc.setNodeText(node,'enddatetime',sEndDT)
        
        attrNode=self.doc.getChild(node,'attributes')
        if attrNode is not None:
            #attrs=[]
            for i in range(0,self.gridAttr.GetNumberRows()):
                sAttrName=self.gridAttr.GetRowLabelValue(i)
                sAttrVal=self.gridAttr.GetCellValue(i,0)
                self.doc.setNodeText(attrsNode,sAttrName,sAttrVal)
        return sLogin
        

    def OnGcbbApplyButton(self, event):
        event.Skip()

    def OnGcbbCancelButton(self, event):
        event.Skip()
