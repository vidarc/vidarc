#----------------------------------------------------------------------------
# Name:         vUserNodeSalaryScheme.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vUserNodeSalaryScheme.py,v 1.8 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vUserNodeSalarySchemePanel import vUserNodeSalarySchemePanel
        #from vUserNodeSalarySchemeEditDialog import *
        #from vUserNodeSalarySchemeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeSalaryScheme(vtXmlNodeTag):
    VERBOSE=0
    SALARY_MIN_ABS=0
    SALARY_MAX_ABS=20000
    #NODE_ATTRS=[
    #        ('tag',None,'tag',None),
    #        ('name',None,'name',None),
    #        ('desc',None,'desc',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='salaryScheme'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeTag.__init__(self,tagName)
        self.lClassification=[]
        self.dSalaryLimit={}
    def GetDescription(self):
        return _(u'salary scheme')
    # ---------------------------------------------------------
    # specific
    def GetSchemeLst(self,node):
        return self.Get(node,'tag')
    def SetSchemeLst(self,node,val):
        self.Set(node,'tag',val)
    def GetClassificationLst(self):
        return self.lClassification
    def CalcClassificationLst(self,node,lang=None):
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
            self.lClassification=[]
            if node is None:
                return lClassification
            tmp=self.doc.getChild(node,'attributes')
            if tmp is None:
                return lClassification
            d={'row':{},'col':{}}
            if lang is None:
                lang=self.doc.GetLang()
            for c in self.doc.getChilds(tmp):
                try:
                    strs=self.doc.getTagName(c).split('_')
                    if strs[0] in ['row','col']:
                        if self.doc.getAttribute(c,'language')==lang:
                            d[strs[0]][int(strs[1])]=self.doc.getText(c)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
            if self.VERBOSE:
                vtLog.pprint(d)
            sOrder=self.doc.getNodeText(tmp,'order_level')
            if len(sOrder)==0:
                sOrder='row'
            if sOrder not in ['row','col']:
                sOrder='row'
            dRow=d['row']
            dCol=d['col']
            keysRow=dRow.keys()
            keysCol=dCol.keys()
            if sOrder=='row':
                for kRow in keysRow:
                    for kCol in keysCol:
                        kClass=kRow*1000+kCol
                        self.lClassification.append((dRow[kRow]+' / '+dCol[kCol],kClass))
            elif sOrder=='col':
                for kCol in keysCol:
                    for kRow in keysRow:
                        kClass=kRow*1000+kCol
                        self.lClassification.append((dCol[kCol]+' / '+dRow[kRow],kClass))
            if self.VERBOSE:
                vtLog.pprint(self.lClassification)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return self.lClassification
    def GetSalaryLimit(self,iClass):
        if self.VERBOSE:
            vtLog.CallStack('')
            print iClass
            vtLog.pprint(self.dSalaryLimit)
        if iClass<0:
            return self.SALARY_MIN_ABS,self.SALARY_MAX_ABS
        iRow=iClass / 1000
        iCol=iClass % 1000
        if self.VERBOSE:
            vtLog.CallStack('')
            print iClass,iRow,iCol
        try:
            fMin,fMax=self.dSalaryLimit[iClass]
        except:
            fMin,fMax=self.SALARY_MIN_ABS,self.SALARY_MAX_ABS
        return fMin,fMax
    def CalcSalaryLimit(self,node):
        try:
            l=self.GetSalaryLst(node)
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(l)
            self.dSalaryLimit={}
            try:
                tmp=self.doc.getChild(node,'attributes')
                if tmp is not None:
                    fMax=float(self.GetML(tmp,'salary_max'))
                else:
                    fMax=self.SALARY_MAX_ABS
            except:
                fMax=self.SALARY_MAX_ABS
            iRow=0
            for lCol in l:
                iCol=0
                for fMin in lCol:
                    kClass=iRow*1000+iCol
                    self.dSalaryLimit[kClass]=(float(fMin),float(fMax))
                    iCol+=1
                iRow+=1
            if self.VERBOSE:
                vtLog.pprint(self.dSalaryLimit)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetSalaryLst(self,node):
        try:
            d={}
            lVal=[]
            iMax=0
            for c in self.doc.getChilds(node,'salary_values'):
                if self.VERBOSE:
                    print c
                try:
                    i=int(self.doc.getAttribute(c,'row'))
                    if i>iMax:
                        iMax=i
                    d[i]=self.doc.getText(c).split(';')
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
            for i in xrange(0,iMax+1):
                try:
                    lVal.append(d[i])
                except:
                    lVal.append([])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return lVal
    def SetSalaryLst(self,node,lVal):
        try:
            for c in self.doc.getChilds(node,'salary_values'):
                self.doc.deleteNode(c,node)
            i=0
            for l in lVal:
                self.doc.setNodeTextAttr(node,'salary_values',';'.join(l),'row','%02d'%i)
                i+=1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xa5IDAT8\x8d}\x93\xd1Q\x1b1\x10\x86?\x9d)@\xc9{\x92m\xc0\xa0\x06 *\
\x81\x12T\x81}%\\\t\x9e\xc93\x8c\x924\xe0\x12\x84y\x06\xb6\x04\x01\r\xa8\x83\
\xcd\xc3\xd9\xe7\x0bv\xb23\x9a\x1d\xe9\xfe\xef\xd7\xeeI\xc2u\x0b\xe6#\x84`)%\
\x9b\xaf\x11\xa3\xc5\x18\xed\xa3\xd6u\x0bN\x16RJ\xa6\xaa6\x87)\x83\x91\xf3y\
\x13\xd7-\x00X\xaf\x96\xa6\xaa<\xec\x16\x8e}\xd8\xcd\xb51DR\x81\x0c B\xcc\
\x99\x87\xdd\xe3\xa4q\xae[\xb0^-m\x18"\x00\x9f>\xffps\x98R@\xd2>\xcb\x89I\
\x07\xa0\xaa\xcc\xf3\x01\x0e\xaa#,\x19b\x84Z\x01\xa8"|\xbf\xb9\xb6\xa9\x82yL\
e\xd7J&\x82d\x92\x08T!W\x81<\xce\x7f\xfe\xfa}\xac\xe0\x7fp\xf0~\xfa\x1e\xc2\
\x16R\xa2\x00\xdf\xbe~\xb1\xbf\x0c\xe6=\x1f`\xaa\x10Z\x80* u\xcaq\xb6i\xf7\
\xcf\x9e\xcb(\xcb\x14\x90\x8a\xb6\x86\xb6@\xca\x95\xd6\x1a1\xc6\xa3\x01)\x11\
\xb6\r\xf5\xb7\xc7\xb2\xe3\x08R\xe2\x08\xeb-\xa9\x14J)x\xef\xe9\xfb\xfex\x8c\
\x00v\x7fg\x078x\x0fUF\xd0+\xd44\xc1\xafo\xef\x8eY8\xd7-\xb0\xfb;\x0b^\t\xa1\
\x8d\xc7\xd9\xda$\x98\xef\xfc\x11>\xb6\x903z\xf8Y\x80n\x14\xed\x1b\xba\r\x13\
,RY\xaf\x96vu\xb9\xb4\x97\xe7\'{y~\xb2\xab\xcb\xa5]\x00\xb8\xdd\xa33\xb0\x9c\
\x12a\xa3l\xd3\x06\xef=\xc30Pj\xe5\xf5\xed\xdd\xa9\xf6\x06\xd0\xf7\r\xef=\
\xad5B\x08\\L\xbd\xecMTd\x12\xcc\xef\xbc\xaa""\xa8*\xad5Zk\xe3\xcd=y\x9e1\
\x9a\x88\x9c}\xba\xe7\xc6\x1f\xcb\xff\xff>\x06U+\x96\x00\x00\x00\x00IEND\xae\
B`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vUserNodeSalarySchemeEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(348, 174),'pnName':'pnNodeSalaryScheme',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vUserNodeSalarySchemeAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(348, 174),'pnName':'pnNodeSalaryScheme',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vUserNodeSalarySchemePanel
        else:
            return None

