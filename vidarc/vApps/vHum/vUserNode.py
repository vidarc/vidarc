#----------------------------------------------------------------------------
# Name:         vUserNode.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060503
# CVS-ID:       $Id: vUserNode.py,v 1.8 2007/11/18 15:02:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vUserInfoPanel import *
        #from vUserNodeEditDialog import *
        #from vUserNodeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNode(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('SurName',None,'surname',None),
            ('FirstName',None,'firstname',None),
        ]
    FUNCS_GET_SET_4_LST=['SurName','FirstName','Name']
    def __init__(self,tagName='user'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'user')
    # ---------------------------------------------------------
    # specific
    def GetFirstName(self,node):
        return self.Get(node,'firstname')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetSurName(self,node):
        return self.Get(node,'surname')
    def GetID(self,node):
        return self.GetAttr(node,'id')
    def GetPersID(self,node):
        return self.GetAttr(node,'persID')
    def GetLang(self,node):
        return self.GetAttrStr(node,'language')
    def GetEmail(self,node):
        nodePers=self.doc.getChild(node,'personal')
        if nodePers is None:
            return ''
        oPers=self.doc.GetRegByNode(nodePers)
        return oPers.Get(nodePers,'email')
    def GetInActive(self,node):
        sVal=self.Get(node,'inactive')
        return sVal in ['1','True']
    def GetMsgTag(self,node,lang=None):
        return self.GetName(node)
    def GetMsgName(self,node,lang=None):
        return self.GetSurName(node)+', '+self.GetFirstName(node)
    
    def SetFirstName(self,node,val):
        self.Set(node,'firstname',val)
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def SetSurName(self,node,val):
        self.Set(node,'surname',val)
    def SetID(self,node,id):
        self.SetAttr(node,'id',id)
        try:
            o=self.doc.GetRegisteredNode('salary')
            nodeTmp=self.doc.getChild(node,'salary')
            if nodeTmp is not None:
                o.SetOwner(nodeTmp,id)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetPersID(self,node,id):
        self.SetAttr(node,'persID',id)
    def SetLang(self,node,val):
        self.SetAttrStr(node,'language',val)
    def SetInActive(self,node,val):
        if val:
            sVal='1'
        else:
            sVal='0'
        self.Set(node,'inactive',sVal)
    
    def GetTimeNormal(self,node,iYear,iMon,iDay):
        oWrk=self.doc.GetRegisteredNode('workingtimes')
        if node is not None:
            c=self.doc.getChild(node,'workingtimes')
        else:
            c=None
        return oWrk.GetTimeNormal(c,iYear,iMon,iDay)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [
                ('name',vtXmlFilterType.FILTER_TYPE_STRING),
                ('surname',vtXmlFilterType.FILTER_TYPE_STRING),
                ('firstname',vtXmlFilterType.FILTER_TYPE_STRING),
                ]
    def GetTranslation(self,name):
        _('name'),_('surname'),_('firstname')
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01%IDAT8\x8d\xa5\x93=K\xc3P\x14\x86\x9f\xa4\x9a.\xb6D\xa9 ]\xa4C\xaa7\
\xa3uU3\x15\x07A\xc4\xbf\xe2\xac\xd9\xec\xd2\xdf\xa1\xa3\x83? U\xdc\xac\x9d4\
E\x11\xcc Q\x9a\xad]\xcbq\xa8\x15\x9a/\xac\x1e8p?\x0e\xcf}\xcf{\xef\xd54\xbd\
\xc0\x7fB\xcf\xda(\x1a\x8bb\x9ae\xf9\x13`\xbf\xd9\x94\xe0-\xa0u\xde\xa2RY\
\xc9\x85\xa4\x02,\xcb\xc2(\x1a\xd4\xd6k\x98\xcbf\xae\x82\x85\xb4\xc5\xf0\xf5\
\x82v\xbbL\xf8\x19\x12\r\xa2\\\x00\x9a^\x98I\xf7\xc4\x11\xe9\xba\xe26\x10\
\xb7\x81x\x07\x88\xb3\x8d\xc4\xeb\xa6\x99T0\xf2@\xf78\xbbv'\xf3\x07\x17\xe7\
\x14:\xf3\xb4\x00\xc0\xc77\xe0*\xbf\x83\xcck\xfcm$\x00\xde}\xb2h4\xcc!\xa4\
\x19#=dx\x89H\x0fy\xd9\xcd60\xdd\xc4\xe9\xa9}\xe0\x1d\xd6\xaa\xb0\xa9\xea\
\x02\xd0\xf7\x9f\xb5x]\xb6\x89\xc0R\t(M\xc6\xcaV([\x89\xff\xe4\xcf\x80\xe66Q\
\xd9\x8a\xa3\xe3\xc3\x9f\xe7\x9d\xaa\xa0\xd3\x85\x8d\x0c@\\\x81\x96\xf5\x9d\
\xf7\xb6\xc6\xe2T\xe11Xe\\\xdf!\x1aD\xdc\xde\xdc%<\xf8\x02;\x99Y\xc7\xf9\xc9\
\x06\xfb\x00\x00\x00\x00IEND\xaeB`\x82"
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vUserNodeEditDialog
            return None
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vUserNodeAddDialog
            return None
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vUserInfoPanel
        else:
            return None

