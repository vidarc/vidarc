#----------------------------------------------------------------------------
# Name:         vXmlHumInputTree.py
# Purpose:      input widget for human / user xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060203
# CVS-ID:       $Id: vXmlHumInputTree.py,v 1.6 2006/05/19 10:55:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vHum.vXmlHumTree import vXmlHumTree

class vXmlHumInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='usr_grp'
        self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vHum')
    def __createTree__(*args,**kwargs):
        tr=vXmlHumTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        #tr.SetSkipInfo(['users'])
        #tr.SetValidInfo(['user'])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
