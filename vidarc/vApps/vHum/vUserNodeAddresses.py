#----------------------------------------------------------------------------
# Name:         vUserNodeAddresses.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060711
# CVS-ID:       $Id: vUserNodeAddresses.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeAddrMultiple import vtXmlNodeAddrMultiple

class vUserNodeAddresses(vtXmlNodeAddrMultiple):
    def IsSkip(self):
        return True
