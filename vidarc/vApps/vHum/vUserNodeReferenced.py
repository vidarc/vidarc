#----------------------------------------------------------------------------
# Name:         vUserNodeReferenced.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vUserNodeReferenced.py,v 1.5 2008/04/16 08:39:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vHum.vUserNode import vUserNode

class vUserNodeReferenced(vUserNode):
    NODE_ATTRS=[
        ]
    FUNCS_GET_4_TREE=['SurName','FirstName','Name']
    FMT_GET_4_TREE=[('SurName',''),('FirstName',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='userReferenced'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vUserNode.__init__(self,tagName)
    def GetDescription(self):
        return _(u'user referenced')
    # ---------------------------------------------------------
    # specific
    def GetUsrId(self,node):
        return self.GetForeignKey(node,'fid','vHum')
    def SetUsrId(self,node,val):
        return self.SetForeignKey(node,'fid',val,'vHum')
    def __getVal__(self,node,sFuncPart,*args,**kwargs):
        if sFuncPart is None:
            return '*****'
        docHum=self.doc.GetNetDoc('vHum')
        if docHum is None:
            return ''
        nodeUsr=docHum.getNodeByIdNum(self.GetUsrId(node))
        if nodeUsr is None:
            return '*****'
        oUsr=docHum.GetRegisteredNode('user')
        f=getattr(oUsr,'Get%s'%sFuncPart)
        return f(nodeUsr,*args,**kwargs)
    def GetFirstName(self,node):
        return self.__getVal__(node,'FirstName')
    def GetName(self,node):
        return self.__getVal__(node,'Name')
    def GetSurName(self,node):
        return self.__getVal__(node,'SurName')
    def GetTimeNormal(self,node,iYear,iMon,iDay):
        return self.__getVal__(node,'TimeNormal',iYear,iMon,iDay)
    def SetFirstName(self,node,val):
        pass
    def SetName(self,node,val):
        pass
    def SetSurName(self,node,val):
        pass
    def GetInActive(self,node):
        return self.__getVal__(node,'InActive')
    # ---------------------------------------------------------
    # inheritance
    def IsNotContained(self):
        return True
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def GetEditDialogClass(self):
        if GUI:
            #return vUserNodeEditDialog
            return None
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vUserNodeAddDialog
            return None
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            #return vUserInfoPanel
            return None
        else:
            return None
