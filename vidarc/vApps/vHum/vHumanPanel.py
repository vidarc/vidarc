#Boa:FramePanel:vHumanPanel
#----------------------------------------------------------------------------
# Name:         vHumanPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060529
# CVS-ID:       $Id: vHumanPanel.py,v 1.10 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import  wx.lib.anchors as anchors
from wx.lib.anchors import LayoutAnchors
from wx import EmptyIcon
import time

try:
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.vHum.vXmlHumTree import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    from vidarc.vApps.vHum.vUserInfoPanel import *
    from vidarc.vApps.vHum.vGroupInfoPanel import *
    from vidarc.tool.net.vNetPlaceDialog import vNetPlaceDialog
    
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.vApps.vHum.images as imgHum
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vHumanMainFrame(parent)

[wxID_VHUMANMAINFRAME, wxID_VHUMANMAINFRAMEPNDATA, 
 wxID_VHUMANMAINFRAMESBSTATUS, wxID_VHUMANMAINFRAMESLWNAV, 
] = [wx.NewId() for _init_ctrls in range(4)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getPluginImage():
    return imgHum.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(imgHum.getPluginBitmap())
    return icon

[wxID_VHUMANMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VHUMANMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VHUMANMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VHUMANMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK] = [wx.NewId() for _init_coll_mnTools_Items in range(1)]

[wxID_VHUMANMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

class vHumanPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VHUMANMAINFRAME,
              name=u'vHumanPanel', parent=prnt, pos=wx.Point(115, 28),
              size=wx.Size(717, 330), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(709, 303))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VHUMANMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              264), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VHUMANMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VHUMANMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 10), size=wx.Size(448, 246),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        #vtLog.setPluginOrigin(vtLgBase.getPluginDomain())
        
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.verbose=VERBOSE
        self.fn=None
        
        self.xdCfg=vtXmlDom(appl='vHumCfg',audit_trail=False)
        
        appls=['vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netDocHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0,
                                            audit_trail=True)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vHumNetMaster')
        
        self.dlgNet=None
        
        self.bModified=False
        self.vgpTree=vXmlHumTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(200,264),style=0,name="vHumTree",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_VTXMLTREE_ITEM_ADDED(self.vgpTree,self.OnTreeItemAdd)
        self.vgpTree.SetDoc(self.netDocHum,True)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        self.sActPanel='empty'
        self.vgpUsrInfo=vUsrInfoPanel(self,wx.NewId(),
                pos=(208,0),size=(100,100),style=0,name="vgpUserInfo")
        EVT_VGPUSER_INFO_CHANGED(self.vgpUsrInfo,self.OnUserInfoChanged)
        self.vgpUsrInfo.SetDoc(self.netDocHum,True)
        self.vgpUsrInfo.SetRegNode(self.netDocHum.GetRegisteredNode('user'))
        self.vgpUsrInfo.Show(False)
        
        self.vgpGrpInfo=vGrpInfoPanel(self,wx.NewId(),
                pos=(208,0),size=(100,100),style=0,name="vgpGroupInfo")
        EVT_VGPGROUP_INFO_CHANGED(self.vgpGrpInfo,self.OnGroupInfoChanged)
        self.vgpGrpInfo.SetDoc(self.netDocHum,True)
        self.vgpGrpInfo.Show(False)
        
        #self.CreateNew()
        
        self.bAutoConnect=True
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        #self.Move(pos)
        self.SetSize(size)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        #self.vgpTree.Destroy()
        
        self.vgpTree=None
    def GetDocMain(self):
        return self.netDocHum
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnUserInfoChanged(self,event):
        self.netDocHum.doEdit(self.vgpTree.GetSelected())
        self.vgpTree.RefreshSelected()
        event.Skip()
    def OnGroupInfoChanged(self,event):
        self.netDocHum.doEdit(self.vgpTree.GetSelected())
        self.vgpTree.RefreshSelected()
        event.Skip()
    def OnTreeItemSel(self,event):
        #print event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #try:
        #self.netDocHum.endEdit(None)
        if event is not None:
            node=event.GetTreeNodeData()
        else:
            node=self.vgpTree.GetSelected()
        if node is None:
            self.pnData.Show(True)
            self.vgpGrpInfo.Show(False)
            self.vgpUsrInfo.Show(False)
            self.netDocHum.endEdit(None)
            return
        tagName=self.netDocHum.getTagName(node)
        if tagName=='user':
            self.vgpUsrInfo.Show(True)
            self.vgpGrpInfo.Show(False)
            self.pnData.Show(False)
            #self.netDocHum.startEdit(node)
            self.vgpUsrInfo.SetNode(node)
            if self.sActPanel!='user' and 0:
                self.spwMain.ReplaceWindow(self.pnInfo,self.vgpUsrInfo)
                self.sActPanel='user'
                self.pnInfo.Show(False)
                self.pnInfo=self.vgpUsrInfo
                self.pnInfo.Show(True)
                #self.spwMain.UpdateSize()
        elif tagName=='group':
            self.vgpGrpInfo.Show(True)
            self.vgpUsrInfo.Show(False)
            self.pnData.Show(False)
            self.vgpGrpInfo.SetNode(node)
            #self.netDocHum.startEdit(node)
            if self.sActPanel!='group' and 0:
                self.spwMain.ReplaceWindow(self.pnInfo,self.vgpGrpInfo)
                self.sActPanel='group'
                self.pnInfo.Show(False)
                self.pnInfo=self.vgpGrpInfo
                self.pnInfo.Show(True)
        else:
            self.vgpUsrInfo.Clear()
            self.vgpGrpInfo.Clear()
            self.pnData.Show(True)
            self.vgpGrpInfo.Show(False)
            self.vgpUsrInfo.Show(False)
            #self.spwMain.ReplaceWindow(self.pnInfo,self.pnEmpty)
            self.sActPanel='empty'
            node=None
        event.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #self.netDocHum.AlignDoc()
        self.__setModified__(True)
        event.Skip()
    def OpenFile(self,fn):
        if self.netDocHum.ClearAutoFN()>0:
            self.netDocHum.Open(fn)
            self.setDoc()
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def setDoc(self):
        self.vgpGrpInfo.SetNode(None)
        self.vgpUsrInfo.SetNode(None)
    def CreateNew(self):
        self.netDocHum.New()
        if 1==0:
            self.dom.New(root="human")
            node=self.dom.getRoot()
            self.dom.createSubNode(node,'groups',False)
            self.dom.createSubNode(node,'users',False)
            self.vgpGrpInfo.SetNode(None)
            self.vgpUsrInfo.SetNode(None)
        self.vgpTree.SetNode(self.netDocHum.getRoot())
        
    def OnVgfHumanMainActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()

    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpGrpInfo)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpUsrInfo)
            
            self.pnData.Refresh()
            self.vgpGrpInfo.Refresh()
            self.vgpUsrInfo.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            #self.pnData.Refresh()
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpGrpInfo)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpUsrInfo)
            
            self.pnData.Refresh()
            self.vgpGrpInfo.Refresh()
            self.vgpUsrInfo.Refresh()
                
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpGrpInfo)
            wx.LayoutAlgorithm().LayoutWindow(self, self.vgpUsrInfo)
            
            self.pnData.Refresh()
            self.vgpGrpInfo.Refresh()
            self.vgpUsrInfo.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnMnToolsMn_tools_req_lockMenu(self, event):
        node=self.vgpTree.GetSelected()
        self.netDocHum.reqLock(node)
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
    def GetCfgData(self):
        #try:
        #    sz=self.GetParent().GetSize()
        #    self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        #except:
        #    pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Human module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Human'),desc,version
