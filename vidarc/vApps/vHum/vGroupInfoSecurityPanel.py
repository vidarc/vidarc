#Boa:FramePanel:vGroupInfoSecurityPanel
#----------------------------------------------------------------------------
# Name:         vUserInfoSecurityPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vGroupInfoSecurityPanel.py,v 1.6 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import cStringIO,sha
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import string,sys,calendar
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VGROUPINFOSECURITYPANEL, wxID_VGROUPINFOSECURITYPANELCHKADMIN, 
 wxID_VGROUPINFOSECURITYPANELLBLSECLV, 
 wxID_VGROUPINFOSECURITYPANELSPNSECLEVEL, 
] = [wx.NewId() for _init_ctrls in range(4)]

import images_hum_tree

# defined event for vgpXmlTree item selected
wxEVT_VGPUSER_INFO_CHANGED=wx.NewEventType()
def EVT_VGPUSER_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPUSER_INFO_CHANGED,func)
class vgpUserInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPUSER_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPUSER_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpUserInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vGroupInfoSecurityPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSecLv, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.chkAdmin, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsSecLv_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSecLv, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.spnSecLevel, 1, border=4, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsSecLv = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsSecLv_Items(self.bxsSecLv)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGROUPINFOSECURITYPANEL,
              name=u'vGroupInfoSecurityPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(258, 317), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(250, 290))
        self.SetAutoLayout(True)

        self.lblSecLv = wx.StaticText(id=wxID_VGROUPINFOSECURITYPANELLBLSECLV,
              label=_(u'Security Level'), name=u'lblSecLv', parent=self,
              pos=wx.Point(0, 4), size=wx.Size(121, 21), style=wx.ALIGN_RIGHT)
        self.lblSecLv.SetMinSize(wx.Size(-1, -1))

        self.spnSecLevel = wx.SpinCtrl(id=wxID_VGROUPINFOSECURITYPANELSPNSECLEVEL,
              initial=-1, max=32, min=-1, name=u'spnSecLevel', parent=self,
              pos=wx.Point(125, 4), size=wx.Size(125, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnSecLevel.SetMinSize(wx.Size(-1, -1))

        self.chkAdmin = wx.CheckBox(id=wxID_VGROUPINFOSECURITYPANELCHKADMIN,
              label=_(u'administration'), name=u'chkAdmin', parent=self,
              pos=wx.Point(0, 29), size=wx.Size(250, 13), style=0)
        self.chkAdmin.SetValue(False)
        self.chkAdmin.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        self.doc=None
        self.node=None
        self.bModified=False
        self.grps=[]
        self.__showPasswd__()
    def __showPasswd__(self):
        flag=False
        flagAdmin=False
        if self.node is not None:
            try:
                if self.doc.GetLoggedInSecLv()>=24:
                    flag=True
                    flagAdmin=True
                else:
                    if self.doc.getNodeText(self.node,'name')==self.doc.GetLoggedInUsr():
                        flag=True
            except:
                pass
        self.spnSecLevel.Enable(flagAdmin)
        self.chkAdmin.Enable(flagAdmin)
        
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.spnSecLevel.SetValue(-1)
            self.chkAdmin.SetValue(False)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.__showPasswd__()
            node=self.doc.getChild(node,'security')
            if node is None:
                return
            
            try:
                i=int(self.doc.getNodeText(node,'sec_level'))
            except:
                i=-1
            self.spnSecLevel.SetValue(i)
            
            try:
                i=int(self.doc.getNodeText(node,'admin'))
                if i==1:
                    i=True
                else:
                    i=False
            except:
                i=False
            self.chkAdmin.SetValue(i)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.doc.GetLoggedInSecLv()<24:
                self.SetModified(False)
                return
            node=self.GetNodeStart(node)
            if node is None:
                return
            node=self.doc.getChildForced(node,'security')
            if node is None:
                return 
            self.doc.setNodeText(node,'sec_level','%d'%self.spnSecLevel.GetValue())
            if self.chkAdmin.GetValue():
                self.doc.setNodeText(node,'admin','1')
            else:
                self.doc.setNodeText(node,'admin','0')
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag,locker=''):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.spnSecLevel.Enable(False)
            self.chkAdmin.Enable(False)
        else:
            self.__showPasswd__()
