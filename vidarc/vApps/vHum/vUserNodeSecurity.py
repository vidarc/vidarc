#----------------------------------------------------------------------------
# Name:         vUserNodeSecurity.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vUserNodeSecurity.py,v 1.5 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vUserInfoSecurityPanel import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vUserNodeSecurity(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            #('Name',None,'name',None),
            #('Desc',None,'desc',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='security'):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'security')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetDesc(self,node):
        return self.GetML(node,'desc')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'desc',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xcdIDAT(\x91\x8d\x92?h\x13q\x14\xc7?\xd1\x1bN\xb83\t\x8a\xf0\xfb\x11B\
\xa6\xbb)\x9c\xf42\xc5\xe1\x1c\x1dZ$\x94 F\x08ti\xa7,\xads\xd5\xcd\xa5\xdd\
\xeb^C)\x88\xd2\x0e\x1d\x8a\x18p\x10\xedanp\xc8\r\xa1\xa0\xdc\r!$\xf0;A\xe2P\
\x873\x7f(H\xfdn\xef\xfb\xbe\xdf\xf7\xbe\xf0^\x06Xq\xf9O\x1c\xf9dv\xb6\xeeon\
=&\x8a!\x9c\xf2=\x80$\x01Pj.\x17b\xf7U\x989;z\xee\x9am,\xeb\xca\xf1\xf1\xf1\
\x87\xe8\xc2\xd4 \xc40\x88\xa2Y\xa3?\xc8\xfb!\x89J\xd6\x1e\xfc\x9e\xcbM7\xc2\
\x85P\xbb4\xe6\xf4{=\xc1\xfe\x95WZ\x9e\xcd\xfd\xaf\x0f\x0b\x07\x9e\'\x808Nb\
\x15\n\x03\xedo\xe2\xa9\xfa\xe4\xe3 \x08N\x1d\xc7\t\x82\x00\x80Gt\x0e<O\x08\
\xa3G/\xc2\x95\xf3\r\xfdA>\xc1\x86\x01\xe08NJ\x8e\xc7\xe3\xb7\x9f\xefx\xde<\
\xc25T23(\xa5\x80f\xb3Y,\x16[\xadV\xbb\xdd\xce\xe5r\xe3\x1bw}?^0L1\x1a\x8d\
\x80 \x08\x82 (\x95J\xddnW\xd7\xf5i\xb094\x94\x02\x03\xa8\xdf\x1b\x1d\xf6\
\xf5\x94\xadV\xab\xba\xae\xd7j\xb5l6\xfb\xe3\xfd\xb6\xbb*g\x86\xeb\xeb+\xa6\
\x94fZ$q\xe7\xb6\xbd6\x1c\x0e\xcb\xe5\xf2d2\xb1m\xfb\xdb\xa77/\xeb?M\xab\x84\
\\W\xe6M\xd4\xb9\x06\xe0\xbeN\xcf\xac\xf9\x1b\x16;\x11\xd6\xf9\xf0V\xa1P\xf8\
\xf2n{o\xc3\x14K\x16\x10\xfb\xbb\xe1q\xcfZ\x92\x1a\xd0~Z\xb1,\x13\x90\xd2\
\x14\xc2\xf0\xdc\xc8\xf7}\xc0]\x95\x08\x91.\x17\xc20/\x002g{\xd2u\x85\xdf\tQ\
*Yx\x9c\xc5\'J\xb1\xdc\xb0\xfd\x9e\xca\xd8\x92\xfdg\xf2r\xf3\x1fx\xf2"\x02hT\
hT\xae\x90\xce4\x7f\x00\x88b\xad$b\xcd\xd4h\x00\x00\x00\x00IEND\xaeB`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vUserInfoSecurityPanel
        else:
            return None

