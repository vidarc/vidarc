#----------------------------------------------------------------------------
# Name:         vGroupInfoAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vGroupInfoAttrPanel.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.vApps.vHum.vUserInfoAttrPanel import *


class vGroupInfoAttrPanel(vUserInfoAttrPanel):
    def dummy(self):
        pass