#Boa:Frame:vLaunchMainFrame

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import time,traceback
import images
#from xml.dom.minidom import getDOMImplementation,parse,parseString,Element
import getpass
import codecs,string,os,os.path,stat
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vLaunch.vInfoPanel import *

def create(parent):
    return vLaunchMainFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
    
[wxID_VLAUNCHMAINFRAME, wxID_VLAUNCHMAINFRAMECBCLOSE, 
 wxID_VLAUNCHMAINFRAMECBHOLLOW, wxID_VLAUNCHMAINFRAMECBLAUNCH, 
 wxID_VLAUNCHMAINFRAMECBLB, wxID_VLAUNCHMAINFRAMECBLT, 
 wxID_VLAUNCHMAINFRAMECBRB, wxID_VLAUNCHMAINFRAMECBRT, 
 wxID_VLAUNCHMAINFRAMECHCAUTOCOLLAPSE, wxID_VLAUNCHMAINFRAMELBLTIME, 
 wxID_VLAUNCHMAINFRAMEPNINFO, wxID_VLAUNCHMAINFRAMETGBEXPAND, 
] = [wx.NewId() for _init_ctrls in range(12)]

[wxID_VGFMAINTBMAINTOOL_LEFT_BOTTOM, wxID_VGFMAINTBMAINTOOL_LEFT_TOP, 
 wxID_VGFMAINTBMAINTOOL_RIGHT_BOTTOM, wxID_VGFMAINTBMAINTOOL_RIGHT_TOP, 
] = [wx.NewId() for _init_coll_tbMain_Tools in range(4)]

dictLaunchApps={
    u'vPrjTimer':[u'vPrjTimer/vPrjTimerAppl.py',u'vPrjTimer/vProjectTimer.exe'],
    u'vPrjEng':[u'vPrjEng/vPrjEngAppl.py',u'vPrjEng/vPrjEng.exe'],
    u'vPrj':[u'vPrj/vPrjAppl.py',u'vProject/vProject.exe'],
    u'vPrjDoc':[u'vPrjDoc/vPrjDocAppl.py',u'vPrjDoc/vPrjDoc.exe'],
    u'vHum':[u'vHum/vHumAppl.py',u'vHum/vHuman.exe'],
    u'vDoc':[u'vDoc/vDocAppl.py',u'vDoc/vDoc.exe'],
    u'vSec':[],
    u'Net':[],
    u'Tools':[],
    u'vVSAS':[u'vVSAS/vVSASAppl.py',u'vVSAS/vVSAS.exe']
                }
class vLaunchMainFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VLAUNCHMAINFRAME,
              name=u'vLaunchMainFrame', parent=prnt, pos=wx.Point(210, 126),
              size=wx.Size(400, 360),
              style=wx.STAY_ON_TOP|wx.MINIMIZE_BOX|wx.CLOSE_BOX|wx.SYSTEM_MENU|wx.CAPTION|wx.RESIZE_BORDER|wx.FRAME_TOOL_WINDOW,
              title=_(u'vLaunch'))
        self.SetClientSize(wx.Size(392, 333))
        self.Bind(wx.EVT_CLOSE, self.OnVgfMainClose)
        self.Bind(wx.EVT_PAINT, self.OnVgfMainPaint)
        self.Bind(wx.EVT_SIZE, self.OnVgfMainSize)

        self.pnInfo = wx.Panel(id=wxID_VLAUNCHMAINFRAMEPNINFO, name=u'pnInfo',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(392, 333),
              style=wx.TAB_TRAVERSAL)
        self.pnInfo.SetAutoLayout(True)

        self.cbRT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBRT, name=u'cbRT', parent=self.pnInfo,
              pos=wx.Point(368, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbRT.SetConstraints(LayoutAnchors(self.cbRT, False, True, True,
              False))
        self.cbRT.Bind(wx.EVT_BUTTON, self.OnCbRTButton,
              id=wxID_VLAUNCHMAINFRAMECBRT)

        self.cbRB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBRB, name=u'cbRB', parent=self.pnInfo,
              pos=wx.Point(368, 309), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbRB.SetConstraints(LayoutAnchors(self.cbRB, False, False, True,
              True))
        self.cbRB.Bind(wx.EVT_BUTTON, self.OnCbRBButton,
              id=wxID_VLAUNCHMAINFRAMECBRB)

        self.cbLB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBLB, name=u'cbLB', parent=self.pnInfo,
              pos=wx.Point(0, 309), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLB.SetConstraints(LayoutAnchors(self.cbLB, True, False, False,
              True))
        self.cbLB.Bind(wx.EVT_BUTTON, self.OnCbLBButton,
              id=wxID_VLAUNCHMAINFRAMECBLB)

        self.cbLT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBLT, name=u'cbLT', parent=self.pnInfo,
              pos=wx.Point(0, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLT.SetConstraints(LayoutAnchors(self.cbLT, True, True, False,
              False))
        self.cbLT.Bind(wx.EVT_BUTTON, self.OnLeftTop,
              id=wxID_VLAUNCHMAINFRAMECBLT)

        self.cbClose = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBCLOSE, name=u'cbClose',
              parent=self.pnInfo, pos=wx.Point(336, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbClose.SetConstraints(LayoutAnchors(self.cbClose, False, True,
              True, False))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VLAUNCHMAINFRAMECBCLOSE)

        self.cbHollow = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHMAINFRAMECBHOLLOW, name=u'cbHollow',
              parent=self.pnInfo, pos=wx.Point(304, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbHollow.SetConstraints(LayoutAnchors(self.cbHollow, False, True,
              True, False))
        self.cbHollow.Bind(wx.EVT_BUTTON, self.OnCbHollowButton,
              id=wxID_VLAUNCHMAINFRAMECBHOLLOW)

        self.lblTime = wx.StaticText(id=wxID_VLAUNCHMAINFRAMELBLTIME, label=u'',
              name=u'lblTime', parent=self.pnInfo, pos=wx.Point(212, 316),
              size=wx.Size(150, 13), style=0)
        self.lblTime.SetConstraints(LayoutAnchors(self.lblTime, False, False,
              True, True))

        self.tgbExpand = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VLAUNCHMAINFRAMETGBEXPAND,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgbExpand',
              parent=self.pnInfo, pos=wx.Point(30, 0), size=wx.Size(36, 36),
              style=0)
        self.tgbExpand.Bind(wx.EVT_BUTTON, self.OnTgbExpandButton,
              id=wxID_VLAUNCHMAINFRAMETGBEXPAND)

        self.chcAutoCollapse = wx.CheckBox(id=wxID_VLAUNCHMAINFRAMECHCAUTOCOLLAPSE,
              label=_(u'Auto'), name=u'chcAutoCollapse', parent=self.pnInfo,
              pos=wx.Point(72, 8), size=wx.Size(73, 13), style=0)
        self.chcAutoCollapse.SetValue(False)
        self.chcAutoCollapse.Bind(wx.EVT_CHECKBOX,
              self.OnChcAutoCollapseCheckbox,
              id=wxID_VLAUNCHMAINFRAMECHCAUTOCOLLAPSE)

        self.cbLaunch = wx.BitmapButton(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VLAUNCHMAINFRAMECBLAUNCH, name=u'cbLaunch',
              parent=self.pnInfo, pos=wx.Point(168, 0), size=wx.Size(36, 36),
              style=wx.BU_AUTODRAW)
        self.cbLaunch.Bind(wx.EVT_BUTTON, self.OnCbLaunchButton,
              id=wxID_VLAUNCHMAINFRAMECBLAUNCH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.dom=vtXmlDom.vtXmlDom()
        self.lbkInfo=wx.Listbook(style=wx.TAB_TRAVERSAL, name='', parent=self.pnInfo, 
                id=wx.NewId(), 
                pos=wx.Point(4,42),size=wx.Size(384, 261))
        self.lbkInfo.SetConstraints(LayoutAnchors(self.lbkInfo, True, True, True,
              True))
        
        self.launchers=[u'vPrjTimer',u'vPrjEng',u'vPrj',u'vPrjDoc',u'vHum',u'vDoc',
                u'vSec',u'Net',u'Tools',u'vVSAS']
        il = wx.ImageList(32, 32)
        for e in self.launchers:
            s='v%sPanel'%e
            f=getattr(images, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.iCfgImg=il.Add(images.getSettingsBitmap())
        self.lbkInfo.AssignImageList(il)
        
        self.sBaseDir=None
        self.lstPanel=[]
        i=0
        for e in self.launchers:
            panel=self.__genPanel__(self.lbkInfo,e)
            self.lbkInfo.AddPage(panel,e,False,i)
            self.lstPanel.append(panel)
            i=i+1
        
        vtLog.CallStack(i)
        vtLog.CallStack(len(self.launchers))
        #add config
        self.iProtect=i
        self.pnCfg=self.__genPanel__(self.lbkInfo,e)
        self.lbkInfo.AddPage(self.pnCfg,u'Settings',False,self.iCfgImg)
        self.lstPanel.append(self.pnCfg)
        EVT_INFO_ADDED(self.pnCfg,self.OnInfoAdded)
        EVT_INFO_DELETED(self.pnCfg,self.OnInfoDeleted)
        
        img=images.getPosLTBitmap()
        self.cbLT.SetBitmapLabel(img)
        
        img=images.getPosRTBitmap()
        self.cbRT.SetBitmapLabel(img)
        
        img=images.getPosLBBitmap()
        self.cbLB.SetBitmapLabel(img)
        
        img=images.getPosRBBitmap()
        self.cbRB.SetBitmapLabel(img)
        
        img=images.getCloseBitmap()
        self.cbClose.SetBitmapLabel(img)
        
        img=images.getHollowBitmap()
        self.cbHollow.SetBitmapLabel(img)
        
        self.tgbExpand.SetBitmapLabel(images.getCollapseBitmap())
        self.tgbExpand.SetBitmapSelected(images.getExpandBitmap())
        self.tgbExpand.SetValue(True)
        self.bExpand=True
        
        self.cbLaunch.SetBitmapLabel(images.getLaunchBitmap())
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        if wx.Platform == '__WXMSW__':
            self.TBMENU_RESTORE=wx.NewId()
            self.TBMENU_CLOSE=wx.NewId()
            # setup a taskbar icon, and catch some events from it
            self.tbicon = wx.TaskBarIcon()
            self.tbicon.SetIcon(icon, "VIDARC Launcher")
            wx.EVT_TASKBAR_LEFT_DCLICK(self.tbicon, self.OnTaskBarActivate)
            wx.EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
            wx.EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
            wx.EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)

        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        self.iPos=0
        self.OpenFile(os.path.join(os.getcwdu(),u'launcher.xml'))
    def __genPanel__(self,parent,name,isLauncher=False):
        panel = vInfoPanel(id=wx.NewId(), name=u'pn%s'%name,
              parent=parent, pos=wx.Point(0, 0), size=wx.Size(192, 253),
              style=wx.TAB_TRAVERSAL,isLauncher=isLauncher)
        EVT_INFO_CHANGED(panel,self.OnInfoChanged)
        EVT_INFO_LAUNCH(panel,self.OnInfoLaunch)
        panel.SetAutoLayout(True)
        return panel
    def Notify(self):
        t = time.localtime(time.time())
        st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
        self.lblTime.SetLabel(st)
        pass
    def OnTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
        if not self.IsShown():
            self.Show(True)
        self.Raise()

    def OnTaskBarMenu(self, evt):
        menu = wx.Menu()
        #menu.Append(self.TBMENU_RESTORE, "Restore VIDARC Launcher")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_RESTORE,
                kind=wx.ITEM_NORMAL, text=_(u"Restore VIDARC Launcher"))
        it.SetBitmap(images.getOpenBitmap())
        menu.AppendItem(it)
        menu.AppendSeparator()
        #menu.Append(self.TBMENU_CLOSE,   "Close")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_CLOSE,
                kind=wx.ITEM_NORMAL, text=_(u"Close"))
        it.SetBitmap(images.getCancelBitmap())
        menu.AppendItem(it)
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()
        #self.Destroy()

    def OnVgfMainClose(self, event):
        if hasattr(self, "tbicon"):
            #del self.tbicon
            self.tbicon.Destroy()
            #del self.tbicon
        self.Destroy()

    def OnLeftTop(self, event):
        self.iPos=0
        self.__update__()
        event.Skip()
    def __update__(self):
        pos=self.GetPosition()
        #vtLog.CallStack(str(pos))
        if self.bExpand:
            self.size=self.GetSize()
            self.lblTime.Show(True)
            self.cbHollow.Show(True)
            self.cbClose.Show(True)
            self.chcAutoCollapse.Show(True)
            self.cbLaunch.Show(True)
            
        else:
            self.lblTime.Show(False)
            self.cbHollow.Show(False)
            self.cbClose.Show(False)
            self.chcAutoCollapse.Show(False)
            self.cbLaunch.Show(False)
            wsize=self.GetSize()
            #size=self.GetVirtualSize()
            size=self.GetClientSize()
            vtLog.CallStack('wsize:%s size:%s'%(str(wsize),str(size)))
            dw=wsize.GetWidth()-size.GetWidth()
            dy=wsize.GetHeight()-size.GetHeight()
            vtLog.CallStack('size:%3d,%3d'%(dw,dy))
            
            self.SetSize(wx.Size(dw+24+48+24,dy+24+24))
        size=self.GetSize()
        
        #self.SetPosition(wx.Point(0,0))
        dispSize=wx.ClientDisplayRect()
        if self.iPos==0:
            self.SetPosition(wx.Point(dispSize[0],dispSize[1]))
        elif self.iPos==1:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],0))
        elif self.iPos==2:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0],
                        dispSize[1]+dispSize[3]-size[1]))
        elif self.iPos==3:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],
                    dispSize[1]+dispSize[3]-size[1]))
        pos=self.GetPosition()
        size=self.GetSize()
        self.dom.setNodeText(self.baseNode,'pos',str(self.iPos))
        self.dom.setNodeText(self.baseNode,'auto_collapse',
                str(int(self.chcAutoCollapse.GetValue())))
        self.dom.setNodeText(self.baseNode,'geometry',
                '%d,%d,%d,%d'%(pos.x,pos.y,size.GetWidth(),size.GetHeight()))
        self.SaveFile()
    def OnCbRTButton(self, event):
        self.iPos=1
        self.__update__()
        event.Skip()

    def OnCbLBButton(self, event):
        self.iPos=2
        self.__update__()
        event.Skip()

    def OnCbRBButton(self, event):
        self.iPos=3
        self.__update__()
        event.Skip()

    def OnVgfMainPaint(self, event):
        self.__update__()
        event.Skip()

    def OnVgfMainSize(self, event):
        size=event.GetSize()
        #size=event.GetClientSize()
        #dw=wsize.GetWidth()-size.GetWidth()
        #dy=wsize.GetWidth()-size.GetHeight()
        w=size.GetWidth()
        h=size[1]
        bMod=False
        if self.bExpand:
            if w<400:
                w=300
                bMod=True
            if h<200:
                h=200
                bMod=True
        dispSize=wx.ClientDisplayRect()
        if w>dispSize[2]:
            w=dispSize[2]
            bMod=True
        if h>dispSize[3]:
            h=dispSize[3]
            bMod=True
        if  bMod==True:
            self.SetSize(wx.Size(w,h))
            self.__update__()
        event.Skip()
    def SetBaseDir(self,sDN=None):
        if sDN is None:
            sDir=os.path.split(os.getcwdu())[0]
        else:
            sDir=sDN
            bFlt=False
            # test directory
            try:
                m=os.stat(sDir)
                if not stat.S_ISDIR(m[stat.ST_MODE]):
                    bFlt=True
            except:
                bFlt=True
            if bFlt:
                sDir=os.path.split(os.getcwdu())[0]
        self.sBaseDir=sDir
    def CreateNew(self):
        self.dom.New(root='launcherentries')
        self.dom=doc
        self.baseNode=self.dom.getRoot()
        root=self.dom.getRoot()
        for e in self.launchers:
            n=self.dom.createSubNode(root,'launcher',False)
            self.dom.setNodeText(n,'name',e)
        self.SaveFile()
    def OpenFile(self,fn):
        global dictLaunchApps
        self.dom.Open(fn)
        #if self.dom is not None:
        #    self.dom.unlink()
        #self.fn=fn
        #try:
        #    self.dom=parse(self.fn)
        #except Exception,list:
            #self.SetStatusText('open fault. (%s)'%list, 1)
        #    if self.dom is None:
        #        self.CreateNew()
        self.baseNode=self.dom.getRoot()
        #self.baseNode=self.dom.documentElement
        LauncherNodes={}
        for c in self.dom.getChilds(self.baseNode,'launcher'):
            sName=self.dom.getNodeText(c,'name')
            if len(sName)<=0:
                continue
            try:
                idx=self.launchers.index(sName)
                self.lstPanel[idx].SetNode(c,self.dom)
            except:
                panel=self.__genPanel__(self.lbkInfo,sName)
                #self.lbkInfo.AddPage(panel,sName,False,-1)
                sImage=self.dom.getNodeText(c,'image')
                idxImg=-1
                try:
                    if len(sImage)>0:
                        bmp=wx.Image(sImage).ConvertToBitmap()
                        imgLst=self.lbkInfo.GetImageList()
                        idxImg=imgLst.Add(bmp)
                except:
                    pass
                #iPgCount=self.lbkInfo.GetPageCount()-1
                #self.lbkInfo.InsertPage(iPgCount,panel,sName,False,idxImg)
                #self.lstPanel.append(panel)
                iPgCount=self.lbkInfo.GetPageCount()-1
                self.lbkInfo.RemovePage(iPgCount)
                self.lbkInfo.AddPage(panel,sName,False,idxImg)
                self.lbkInfo.AddPage(self.pnCfg,u'Settings',False,self.iCfgImg)
                self.iProtect=iPgCount
                
                self.lstPanel.insert(iPgCount,panel)
                panel.SetNode(c,self.dom)
                #self.launchers.append(sName)
                self.launchers.insert(iPgCount,sName)
                
                dictLaunchApps[sName]=[]
            LauncherNodes[sName]=c
            pass
        for s in self.launchers:
            if LauncherNodes.has_key(s)==False:
                n=self.dom.createSubNode(self.baseNode,'launcher',False)
                self.dom.setNodeText(n,'name',s)
                LauncherNodes[s]=n
                idx=self.launchers.index(s)
                self.lstPanel[idx].SetNode(n,self.dom)
        self.pnCfg.SetNode(self.baseNode,self.dom,True)
        try:
            self.iPos=int(self.dom.getNodeText(self.baseNode,'pos'))
        except:
            self.iPos=0
        try:
            iColl=int(self.dom.getNodeText(self.baseNode,'auto_collapse'))
        except:
            iColl=0
        self.chcAutoCollapse.SetValue(iColl)
        self.__update__()
    def SaveFile(self,fn=None):
        #if fn is not None:
        #    self.fn=fn
        self.dom.AlignDoc()
        self.dom.Save(fn)
        #vtXmlDomTree.vtXmlDomAlign(self.dom)
        #fnUtil.shiftFile(self.fn)
        #if self.dom.encoding is None:
        #    e=u'ISO-8859-1'
        #else:
        #    e=self.dom.encoding
        #f=codecs.open(self.fn,"w",e)
        #self.dom.writexml(f,encoding=e)
        #f.close()
    def OnInfoAdded(self,evt):
        vtLog.CallStack(self.iProtect)
        global dictLaunchApps
        sName=evt.GetName()
        sImage=evt.GetImage()
        n=self.dom.createSubNode(self.baseNode,'launcher',False)
        self.dom.setNodeText(n,'name',sName)
        self.dom.setNodeText(n,'image',sImage)
        
        panel=self.__genPanel__(self.lbkInfo,sName)
        #self.lbkInfo.AddPage(panel,sName,False,-1)
        #sImage=vtXmlDomTree.getNodeText(c,'image')
        idxImg=-1
        try:
            if len(sImage)>0:
                bmp=wx.Image(sImage).ConvertToBitmap()
                imgLst=self.lbkInfo.GetImageList()
                idxImg=imgLst.Add(bmp)
        except:
            pass
        iPgCount=self.lbkInfo.GetPageCount()-1
        self.lbkInfo.RemovePage(iPgCount)
        self.lbkInfo.AddPage(panel,sName,False,idxImg)
        self.lbkInfo.AddPage(self.pnCfg,u'Settings',False,self.iCfgImg)
        self.iProtect+=1
        #self.lstPanel.append(panel)
        self.lstPanel.insert(iPgCount,panel)
        panel.SetNode(n,self.dom)
        #self.launchers.append(sName)
        self.launchers.insert(iPgCount,sName)
        
        dictLaunchApps[sName]=[]
        self.SaveFile()
        self.pnCfg.SetNode(self.baseNode,self.dom,True)
        pass
    def OnInfoDeleted(self,evt):
        #if evt.GetObj()==self.pnCfg:
        #    return
        sName=evt.GetName()
        idx=self.launchers.index(sName)
        vtLog.CallStack('idx:%3d len:%3d protect:%3d'%(idx,len(self.launchers),self.iProtect))
        if idx>=len(self.launchers):
            return
        if idx<=self.iProtect:
            for c in self.dom.getChilds(self.baseNode,'launcher'):
                s=self.dom.getNodeText(c,'name')
                if len(s)<=0:
                    continue
                if sName==s:
                    self.dom.deleteNode(c,self.baseNode)
                    #nc=c.nextSibling
                    #self.baseNode.removeChild(c)
                    #if nc is not None:
                    #    if nc.nodeType==Element.TEXT_NODE:
                    #        if len(string.strip(nc.nodeValue))<=0:
                    #            self.baseNode.removeChild(nc)
                    #            nc.unlink()
                    #c.unlink
                    self.lstPanel=self.lstPanel[:idx]+self.lstPanel[idx+1:]
                    self.launchers.remove(sName)
                    self.lbkInfo.RemovePage(idx)
                    print self.lstPanel
                    self.iProtect-=1
                    self.SaveFile()
                    self.pnCfg.SetNode(self.baseNode,self.dom,True)
                    return
        pass
    def OnInfoChanged(self,evt):
        vtLog.CallStack('')
        if evt.GetObj()==self.pnCfg:
            pass
        else:
            evt.GetObj().GetNode()
        self.SaveFile()
    def OnInfoLaunch(self,evt):
        vtLog.CallStack('')
        try:
            if evt.IsCfgFile():
                sOpenFileType=u'--config='
            else:
                sOpenFileType=u'--file='
            pnInfo=evt.GetObj()
            idx=self.lstPanel.index(pnInfo)
            sName=self.launchers[idx]
            if self.sBaseDir is None:
                sDir=os.path.split(os.getcwdu())[0]
            else:
                sDir=self.sBaseDir
            if len(dictLaunchApps[sName]):
                lstTry=[]
                bFound=False
                for sApplFN in dictLaunchApps[sName]:
                    try:
                        # test py
                        sApplFN=string.replace(sApplFN,'/',os.sep)
                        sFullFN=os.path.join(sDir,sApplFN)
                        os.stat(sFullFN)
                        # found it
                        if sApplFN[-4:]==u'.exe':
                            # Launch exe
                            cmd='"%s" "%s%s"'%(sFullFN,sOpenFileType,evt.GetFN())
                            wx.Execute(cmd)
                            bFound=True
                            break
                        elif sApplFN[-3:]==u'.py':
                            # Launch python
                            sExts=string.split(sFullFN,'.')
                            sExt=sExts[-1]
                            fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
                            #filename=fnUtil.getAbsoluteFilenameRel2BaseDir(self.sFN,self.prjDN)
                            filename=sFullFN
                            mime = fileType.GetMimeType()
                            cmd = fileType.GetOpenCommand(filename, mime)
                            cmd=cmd[:-1]+sOpenFileType+evt.GetFN()+'"'
                            wx.Execute(cmd)
                            bFound=True
                            break
                        if self.chcAutoCollapse.GetValue()>0:
                            self.tgbExpand.SetValue(0)
                            self.bExpand=0
                            self.__update__()
                    except:
                        lstTry.append((cmd,traceback.format_exc))
                if bFound==False:
                    sMsg=''
                    for tup in lstTry:
                        sMsg=tup[0]+'\n'+tup[1]+'\n\n'
                    dlg=wx.MessageDialog(self,sMsg ,
                        u'vLaunch',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
            
            else:
                sFullFN=evt.GetFN()
                sExts=string.split(sFullFN,'.')
                sExt=sExts[-1]
                fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
                #filename=fnUtil.getAbsoluteFilenameRel2BaseDir(self.sFN,self.prjDN)
                filename=sFullFN
                mime = fileType.GetMimeType()
                if mime is not None:
                    cmd = fileType.GetOpenCommand(sFullFN, mime)
                    wx.Execute(cmd)
                else:
                    wx.Execute(sFullFN)
                if self.chcAutoCollapse.GetValue()>0:
                    self.tgbExpand.SetValue(0)
                    self.bExpand=0
                    self.__update__()
        except:
            pass
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()

    def OnCbHollowButton(self, event):
        event.Skip()

    def OnTgbExpandButton(self, event):
        self.bExpand=self.tgbExpand.GetValue()
        if self.bExpand:
            self.SetSize(self.size)
        self.__update__()    
            
        event.Skip()

    def OnChcAutoCollapseCheckbox(self, event):
        event.Skip()

    def OnCbLaunchButton(self, event):
        idx=self.lbkInfo.GetSelection()
        sFN=self.lstPanel[idx].GenLaunch()
        event.Skip()
