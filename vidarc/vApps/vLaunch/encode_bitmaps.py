#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Prj img/Prj03_16.png images.py",
    "-a -u -n PrjSel img/Prj02_16.png images.py",
    "-a -u -n Prjs img/Prjs01_16.png images.py",
    "-a -u -n PrjsSel img/Prjs01_16.png images.py",
    "-a -u -n Year img/Year03_16.png images.py",
    "-a -u -n Month img/Month03_16.png images.py",
    "-a -u -n Day img/Day03_16.png images.py",
    "-a -u -n User img/Usr02_16.png images.py",
    "-a -u -n Client img/Client01_16.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Build img/Build02_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/ok.png images.py",
    "-a -u -n Hollow img/Windows01_16.png images.py",
    "-a -u -n Open img/Open01_16.png images.py",
    "-a -u -n Close img/Close01_16.png images.py",
    "-a -u -n Collapse img/Collapse01_32.png images.py",
    "-a -u -n Expand img/Expand01_32.png images.py",
    "-a -u -n Launch img/vLaunch01_32.png images.py",
    "-a -u -n DropTarget img/DropTarget01_32.png images.py",
    "-a -u -n Application img/vLaunch01_16.ico images.py",
    "-a -u -n Settings img/Settings02_32.png images.py",
    "-a -u -n vDoc img/vLaunchDoc01_32.png images.py",
    "-a -u -n vHum img/vLaunchHum01_32.png images.py",
    "-a -u -n vPrj img/vLaunchPrj01_32.png images.py",
    "-a -u -n vPrjDoc img/vLaunchPrjDoc01_32.png images.py",
    "-a -u -n vPrjEng img/vLaunchPrjEng01_32.png images.py",
    "-a -u -n vPrjTimer img/vLaunchPrjTimer01_32.png images.py",
    "-a -u -n vSec img/vLaunchSec01_32.png images.py",
    "-a -u -n Net img/vLaunchNet01_32.png images.py",
    "-a -u -n Tools img/vLaunchTools01_32.png images.py",
    "-a -u -n vVSAS img/vLaunchVSAS_32.png images.py",
    #"-a -u -n vVSAS img/vLaunchVSAS_32.png images.py",
    "-a -u -n PosLT img/vLaunchPosLT_16.png images.py",
    "-a -u -n PosRT img/vLaunchPosRT_16.png images.py",
    "-a -u -n PosLB img/vLaunchPosLB_16.png images.py",
    "-a -u -n PosRB img/vLaunchPosRB_16.png images.py",
    #"-a -u -n User Usr01_16.png hum_tree_images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    #"-a -u -n Add add.png images.py",
    #"-a -u -n Del waste.png images.py",
    #"-a -u -n Apply checkmrk.png images.py",
    # "-u -i -n Splash img/splashPrj01.png splash.py",
    
    #"-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

