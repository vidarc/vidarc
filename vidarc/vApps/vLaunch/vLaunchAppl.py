#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vLaunchAppl.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vLaunchAppl.py,v 1.2 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import sys,getopt

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vLaunchMainFrame

modules ={u'vLaunchMainFrame': [1, 'Main frame of Application', u'vLaunchMainFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = vLaunchMainFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")

def main():
    optshort,optlong='l:b:h',['lang=','base=','help']
    vtLgBase.initAppl(optshort,optlong,'vLaunch')
    application = BoaApp(0)
    iLogLv=vtLog.ERROR
    sBaseDir=None
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'b:h',['base=','log=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--base']:
                sBaseDir=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLog.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLog.INFO
                elif o[1]=='2':
                    iLogLv=vtLog.WARN
                elif o[1]=='3':
                    iLogLv=vtLog.ERROR
                elif o[1]=='4':
                    iLogLv=vtLog.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLog.FATAL
    except:
        showHelp()
    vtLog.vtLngInit('vLaunch','vLaunch.log',iLogLv)
    application.main.SetBaseDir(sBaseDir)
    application.MainLoop()
    
if __name__ == '__main__':
    main()
