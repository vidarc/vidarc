# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-12-13 15:20+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: vLaunchMainFrame.py:239
msgid "%b-%d-%Y   %H:%M:%S wk:%U"
msgstr ""

#: vLaunchAppl.py:37
msgid "--help"
msgstr ""

#: vLaunchAppl.py:40
msgid "--lang <language id according ISO639>"
msgstr ""

#: vLaunchAppl.py:41
msgid "--log <level>"
msgstr ""

#: vLaunchAppl.py:42
msgid "0 = debug"
msgstr ""

#: vLaunchAppl.py:43
msgid "1 = information"
msgstr ""

#: vLaunchAppl.py:44
msgid "2 = waring"
msgstr ""

#: vLaunchAppl.py:45
msgid "3 = error"
msgstr ""

#: vLaunchAppl.py:46
msgid "4 = critical"
msgstr ""

#: vLaunchAppl.py:47
msgid "5 = fatal"
msgstr ""

#: vLaunchMainFrame.py:132
msgid "Auto"
msgstr ""

#: vLaunchMainFrame.py:259
msgid "Close"
msgstr ""

#: vLaunchMainFrame.py:253
msgid "Restore VIDARC Launcher"
msgstr ""

#: vLaunchAppl.py:33
msgid "help"
msgstr ""

#: vLaunchAppl.py:38
msgid "show this screen"
msgstr ""

#: vLaunchMainFrame.py:56
msgid "vLaunch"
msgstr ""

#: vLaunchAppl.py:35
msgid "valid flags:"
msgstr ""
