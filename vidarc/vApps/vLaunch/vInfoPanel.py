#Boa:FramePanel:vInfoPanel
#----------------------------------------------------------------------------
# Name:         vInfoPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vInfoPanel.py,v 1.2 2005/12/13 16:46:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import sys,string,os.path
import images

[wxID_VINFOPANEL, wxID_VINFOPANELCBAPPLY, wxID_VINFOPANELCHCCFG, 
 wxID_VINFOPANELGCBBBROWSE, wxID_VINFOPANELGCBBDEL, wxID_VINFOPANELLBBBITMAP, 
 wxID_VINFOPANELLBLALAIS, wxID_VINFOPANELLSTINFO, wxID_VINFOPANELTXTALIAS, 
] = [wx.NewId() for _init_ctrls in range(9)]

# defined event for vgpData item selected
wxEVT_INFO_CHANGED=wx.NewEventType()
def EVT_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_CHANGED,func)
class vgpInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_CHANGED)
        self.obj=obj
    def GetObj(self):
        return self.obj

# defined event for vgpData item selected
wxEVT_INFO_ADDED=wx.NewEventType()
def EVT_INFO_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_ADDED,func)
class vgpInfoAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_ADDED(<widget_name>, self.OnInfoAdded)
    """

    def __init__(self,obj,name,image):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_ADDED)
        self.obj=obj
        self.name=name
        self.image=image
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name
    def GetImage(self):
        return self.image

# defined event for vgpData item selected
wxEVT_INFO_DELETED=wx.NewEventType()
def EVT_INFO_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_DELETED,func)
class vgpInfoDeleted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_DELETED(<widget_name>, self.OnInfoDeleted)
    """

    def __init__(self,obj,name):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_DELETED)
        self.obj=obj
        self.name=name
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name



# defined event for vgpData item selected
wxEVT_INFO_LAUNCH=wx.NewEventType()
def EVT_INFO_LAUNCH(win,func):
    win.Connect(-1,-1,wxEVT_INFO_LAUNCH,func)
class vgpInfoLaunch(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_LAUNCH(<widget_name>, self.OnInfoLaunch)
    """

    def __init__(self,obj,fn,cfgFile):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_LAUNCH)
        self.obj=obj
        self.fn=fn
        if cfgFile=='1':
            self.bCfgFile=True
        else:
            self.bCfgFile=False
            
    def GetObj(self):
        return self.obj
    def GetFN(self):
        return self.fn
    def IsCfgFile(self):
        return self.bCfgFile

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(u'',file,isCfgFile=None)
            pass

class vImageDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.SetImageFile(file)
            pass

class vInfoPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VINFOPANEL, name=u'vInfoPanel',
              parent=prnt, pos=wx.Point(347, 220), size=wx.Size(315, 268),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(307, 241))
        self.SetAutoLayout(True)

        self.lstInfo = wx.ListView(id=wxID_VINFOPANELLSTINFO, name=u'lstInfo',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(304, 176),
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING|wx.LC_SINGLE_SEL)
        self.lstInfo.SetConstraints(LayoutAnchors(self.lstInfo, True, True,
              True, True))
        self.lstInfo.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInfoListItemSelected, id=wxID_VINFOPANELLSTINFO)
        self.lstInfo.Bind(wx.EVT_LEFT_DCLICK, self.OnLstInfoLeftDclick)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'gcbbDel',
              parent=self, pos=wx.Point(108, 208), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.SetConstraints(LayoutAnchors(self.gcbbDel, True, False,
              False, True))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VINFOPANELGCBBDEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(8, 208),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.SetConstraints(LayoutAnchors(self.gcbbBrowse, True,
              False, False, True))
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VINFOPANELGCBBBROWSE)

        self.txtAlias = wx.TextCtrl(id=wxID_VINFOPANELTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(32, 180),
              size=wx.Size(136, 21), style=0, value=u'')
        self.txtAlias.SetConstraints(LayoutAnchors(self.txtAlias, True, False,
              False, True))

        self.lblAlais = wx.StaticText(id=wxID_VINFOPANELLBLALAIS,
              label=u'Alias', name=u'lblAlais', parent=self, pos=wx.Point(4,
              184), size=wx.Size(22, 13), style=0)
        self.lblAlais.SetConstraints(LayoutAnchors(self.lblAlais, True, False,
              False, True))

        self.cbApply = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VINFOPANELCBAPPLY, name=u'cbApply', parent=self,
              pos=wx.Point(176, 178), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbApply.SetConstraints(LayoutAnchors(self.cbApply, True, False,
              False, True))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VINFOPANELCBAPPLY)

        self.lbbBitmap = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VINFOPANELLBBBITMAP, name=u'lbbBitmap', parent=self,
              pos=wx.Point(202, 178), size=wx.Size(36, 36),
              style=wx.ST_NO_AUTORESIZE)
        self.lbbBitmap.SetConstraints(LayoutAnchors(self.lbbBitmap, True, False,
              False, True))

        self.chcCfg = wx.CheckBox(id=wxID_VINFOPANELCHCCFG, label=u'config',
              name=u'chcCfg', parent=self, pos=wx.Point(248, 184),
              size=wx.Size(56, 13), style=0)
        self.chcCfg.SetValue(False)
        self.chcCfg.SetConstraints(LayoutAnchors(self.chcCfg, True, False,
              False, True))

    def __init__(self, parent, id, pos, size, style, name,isLauncher):
        self._init_ctrls(parent)
        
        self.isLauncher=isLauncher
        self.bLockPost=False
        self.idxSel=-1
        self.files=[]
        
        dt = vFileDropTarget(self)
        self.lstInfo.SetDropTarget(dt)
        
        dt=vImageDropTarget(self)
        self.lbbBitmap.SetDropTarget(dt)
        
        self.gcbbBrowse.SetBitmapLabel(images.getBrowseBitmap())
        self.gcbbDel.SetBitmapLabel(images.getDelBitmap())
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.lbbBitmap.SetBitmap(images.getDropTargetBitmap())
        
        self.lstInfo.InsertColumn(0,u'Alais',wx.LIST_FORMAT_LEFT,80)
        self.lstInfo.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,110)
        self.lstInfo.InsertColumn(2,u'Path',wx.LIST_FORMAT_LEFT,200)
        self.lstInfo.InsertColumn(3,u'Cfg',wx.LIST_FORMAT_LEFT,20)
        
        
    def SetNode(self,node,doc,isCfg=False):
        self.idxImgFile=-1
        self.imgNew=None
        self.sImgFN=''
        
        self.imgLst=wx.ImageList(16,16)
        self.dictImg={}
        self.lstInfo.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstInfo.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.node=node
        self.doc=doc
        self.bLockPost=True
        self.lstInfo.DeleteAllItems()
        self.idxSel=-1
        self.bIsCfg=False
        if isCfg==False:
            for c in self.doc.getChilds(node,'file'):
                s=self.doc.getNodeText(c,'config')
                self.AddFile(self.doc.getNodeText(c,'alias'),
                        self.doc.getNodeText(c,'name'),
                        self.doc.getNodeText(c,'image'),
                        self.doc.getNodeText(c,'config'))
        else:
            # this is the config
            self.gcbbBrowse.Show(False)
            self.bIsCfg=True
            for c in self.doc.getChilds(node,'launcher'):
                self.AddFile(self.doc.getNodeText(c,'name'),
                        self.doc.getNodeText(c,'name'),
                        self.doc.getNodeText(c,'image'))
        self.bLockPost=False
        pass
        
    def GetNode(self):
        if self.node is None:
            return
        if self.bIsCfg==True:
            return
        for c in self.doc.getChilds(self.node,'file'):
            # remove nodes
            #nc=c.nextSibling
            #self.node.removeChild(c)
            #if nc is not None:
            #    if nc.nodeType==Element.TEXT_NODE:
            #        if len(string.strip(nc.nodeValue))<=0:
                        #self.node.removeChild(nc)
            #            self.doc.deleteNode(nc)
                        #nc.unlink()
            #c.unlink
            self.doc.delNode(c)
        for idx in range(0,self.lstInfo.GetItemCount()):
            it=self.lstInfo.GetItem(idx,0)
            sAlias=it.m_text
            iImgIdx=it.m_image
            it=self.lstInfo.GetItem(idx,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(idx,2)
            sDN=it.m_text
            it=self.lstInfo.GetItem(idx,3)
            sCfgFile=it.m_text
            sFull=os.path.join(sDN,sFN)
            c=self.doc.createSubNode(self.node,'file',False)
            self.doc.setNodeText(c,'alias',sAlias)
            self.doc.setNodeText(c,'name',sFull)
            if sCfgFile=='1':
                self.doc.setNodeText(c,'config',sCfgFile)
            sImage=u''
            for k in self.dictImg.keys():
                if self.dictImg[k]==iImgIdx:
                    sImage=k
                    break
            self.doc.setNodeText(c,'image',sImage)
        #vtXmlDomTree.vtXmlDomAlignNode(self.doc,self.node)
        self.doc.AlignNode(self.node,iRec=3)
        pass
    def AddFile(self,alias,file,image='',isCfgFile=''):
        if isCfgFile is None:
            if self.chcCfg.GetValue():
                isCfgFile='1'
            else:
                isCfgFile='0'
                
        try:
            i=self.files.index(file)
        except:
            sDN,sFN=os.path.split(file)
            i=-1
            if len(image)>0:
                if self.dictImg.has_key(image):
                    i=self.dictImg[image]
                else:
                    try:
                        bmp=wx.Image(image).ConvertToBitmap()
                        i=self.imgLst.Add(bmp)
                        self.dictImg[image]=i
                    except:
                        i=-1
            idx=self.lstInfo.InsertImageStringItem(sys.maxint, alias, i)
            self.lstInfo.SetStringItem(idx,1,sFN,-1)
            self.lstInfo.SetStringItem(idx,2,sDN,-1)
            if isCfgFile=='1':
                sVal='1'
            else:
                sVal='0'
            self.lstInfo.SetStringItem(idx,3,sVal,-1)
            self.files.append(file)
            if self.bLockPost==False:
                if self.idxSel>=0:
                    self.lstInfo.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
                    self.idxSel=-1
                wx.PostEvent(self,vgpInfoChanged(self))
    def SetImageFile(self,file):
        if len(file)>0:
            if self.dictImg.has_key(file):
                i=self.dictImg[file]
                bmp=self.imgLst.GetBitmap(i)
                self.idxImgFile=i
                self.imgNew=None
            else:
                try:
                    bmp=wx.Image(file).ConvertToBitmap()
                    self.sImgFN=file
                    self.idxImgFile=-1
                    self.imgNew=bmp
                except:
                    self.idxImgFile=-1
                    self.sImgFN=''
                    self.imgNew=None
                    return
        self.lbbBitmap.SetBitmap(wx.EmptyBitmap(32, 32))
        self.lbbBitmap.Refresh()
        self.lbbBitmap.SetBitmap(bmp)
        self.lbbBitmap.Refresh()
        pass
    def OnGcbbDelButton(self, event):
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        if self.bIsCfg==True:
            wx.PostEvent(self,vgpInfoDeleted(self,sFN))
        else:
            it=self.lstInfo.GetItem(self.idxSel,2)
            sDN=it.m_text
            sFull=os.path.join(sDN,sFN)
            try:
                idx=self.files.index(sFull)
                self.files.remove(sFull)
            except:
                pass
            self.lstInfo.DeleteItem(self.idxSel)
            self.idxSel=-1
            wx.PostEvent(self,vgpInfoChanged(self))
        #self.GetNode()
        event.Skip()

    def OnGcbbBrowseButton(self, event):
        dlg = wx.FileDialog(self, "Open", ".", "", 
                    "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            dlg.Centre()
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.AddFile('',filename,isCfgFile=None)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbUpButton(self, event):
        event.Skip()

    def OnGcbbDownButton(self, event):
        event.Skip()

    def OnLstInfoListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        if self.bIsCfg==True:
            return
        it=self.lstInfo.GetItem(self.idxSel,0)
        self.txtAlias.SetValue(it.m_text)
        event.Skip()

    def OnLstInfoLeftDclick(self, event):
        if self.bIsCfg==True:
            return
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,3)
        sCfgFile=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoLaunch(self,sFull,sCfgFile))
        event.Skip()
    def GenLaunch(self):
        if self.bIsCfg==True:
            return
        if self.idxSel>=0:
            it=self.lstInfo.GetItem(self.idxSel,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(self.idxSel,2)
            sDN=it.m_text
            it=self.lstInfo.GetItem(self.idxSel,3)
            sCfgFile=it.m_text
            sFull=os.path.join(sDN,sFN)
            wx.PostEvent(self,vgpInfoLaunch(self,sFull,sCfgFile))

    def OnCbApplyButton(self, event):
        if self.bIsCfg==True:
            alias=self.txtAlias.GetValue()
            if len(alias)>0:
                if self.imgNew is not None:
                    self.idxImgFile=self.imgLst.Add(self.imgNew)
                    self.dictImg[self.sImgFN]=self.idxImgFile
                    sImage=self.sImgFN
                else:
                    sImage=u''
                self.AddFile(alias,alias,sImage)
        else:
            if self.idxSel<0:
                return
            if self.idxImgFile<0:
                if self.imgNew is not None:
                    self.idxImgFile=self.imgLst.Add(self.imgNew)
                    self.dictImg[self.sImgFN]=self.idxImgFile
            self.lstInfo.SetStringItem(self.idxSel,0,self.txtAlias.GetValue(),
                                self.idxImgFile)
            
        self.txtAlias.SetValue('')
        self.idxImgFile=-1
        self.imgNew=None
        self.lbbBitmap.SetBitmap(wx.EmptyBitmap(32, 32))
        #self.lbbBitmap.Refresh()
        self.lbbBitmap.SetBitmap(images.getDropTargetBitmap())
        #self.lbbBitmap.Refresh()
        if self.bIsCfg==True:
            wx.PostEvent(self,vgpInfoAdded(self,alias,sImage))
        else:
            wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()
        
