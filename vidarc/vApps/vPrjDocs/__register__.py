#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: __register__.py,v 1.1 2006/11/21 21:25:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vPurchase.vXmlNodePurchase import vXmlNodePurchase
from vidarc.vApps.vPurchase.vXmlNodePurchaseDepartment import vXmlNodePurchaseDepartment
from vidarc.vApps.vPurchase.vXmlNodePurchaseParts import vXmlNodePurchaseParts
from vidarc.vApps.vPurchase.vXmlNodePurchaseDoc import vXmlNodePurchaseDoc

from vidarc.vApps.vSupplier.vXmlNodeSupplierContact import vXmlNodeSupplierContact
from vidarc.vApps.vSupplier.vXmlNodeSupplierAddr import vXmlNodeSupplierAddr

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

import vidarc.ext.report.__register__
import vidarc.ext.data.__register__

def RegisterNodes(self,bRegAsRoot=True):
    try:
        oPurchase=vXmlNodePurchase()
        oPurchaseDepartment=vXmlNodePurchaseDepartment()
        oPurchaseParts=vXmlNodePurchaseParts()
        oPurchaseDoc=vXmlNodePurchaseDoc()
        oContacts=vXmlNodeSupplierContact()
        oAddrs=vXmlNodeSupplierAddr()
        self.RegisterNode(oPurchase,False)
        self.RegisterNode(oPurchaseDepartment,False)
        self.RegisterNode(oPurchaseParts,False)
        self.RegisterNode(oPurchaseDoc,False)
        self.RegisterNode(oContacts,False)
        self.RegisterNode(oAddrs,False)
        
        oLog=self.GetReg('log')
        if oLog is None:
            oLog=vtXmlNodeLog()
            self.RegisterNode(oLog,False)
        oAttrML=self.GetReg('attrML')
        if oAttrML is None:
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
        vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
        vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterNodes(self,'vPurchase',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
        vidarc.ext.data.__register__.RegisterDateSmall(self,'purchase','orderDt',_('ordering date'))
        vidarc.ext.data.__register__.RegisterDateSmall(self,'purchase','deliveryDt',_('delivery date'))
        self.LinkRegisteredNode(oPurchaseDepartment,oPurchase)
        self.LinkRegisteredNode(oPurchase,oPurchaseParts)
        self.LinkRegisteredNode(oPurchase,oLog)
        self.LinkRegisteredNode(oPurchase,oPurchaseDoc)
        
        oDocs=self.GetReg('Documents')
        self.LinkRegisteredNode(oPurchaseDepartment,oDocs)
        
        flt={None:[('GetSum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                    ('orderDt',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ('deliveryDt',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ]}
        oTransPur=self.GetReg('transPurchase')
        if oTransPur is None:
            oTransPur=veStateTransition(tagName='transPurchase',filter=flt)
            self.RegisterNode(oTransPur,False)
        oSMpurchase=self.GetReg('smPurchase')
        if oSMpurchase is None:
            oSMpurchase=veStateStateMachine(tagName='smPurchase',
                    tagNameState='state',tagNameTrans='transPurchase',
                    filter=flt,
                    trans={None:{None:_(u'purchase'),
                            'GetSum':_(u'sum'),
                            'orderDt':_(u'ordering date'),
                            'deliveryDt':_(u'delivery date'),
                            } },
                    cmd=oPurchase.GetActionCmdDict())
        oStateMachineLinker=self.GetReg('stateMachineLinker')
        if oStateMachineLinker is not None:
            oStateMachineLinker.AddPossible({
                    'purchase':[('purState','smPurchase')],
                    })
            self.RegisterNode(oSMpurchase,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMpurchase)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMpurchase,oState)
        else:
            vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)
    except:
        vtLog.vtLngTB(__name__)
