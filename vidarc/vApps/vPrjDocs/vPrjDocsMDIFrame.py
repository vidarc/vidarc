#Boa:MDIChild:vPrjDocsMDIFrame
#----------------------------------------------------------------------------
# Name:         vPrjDocsMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vPrjDocsMDIFrame.py,v 1.2 2007/02/19 12:32:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vPrjDocs.images as imgPrjDocs
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPrjDocs.vPrjDocsMainPanel import *

def getPluginImage():
    return imgPrjDocs.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPrjDocs.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vPrjDocsMDIFrame(parent)

DOMAINS=['document','vPrjDocs']
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

[wxID_VPRJDOCSMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vPrjDocsMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=wxID_VPRJDOCSMDIFRAME,
              name=u'vPrjDocsMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vPrjDocs')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'PrjDocs')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vPrjDocsMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vPrjDocsPlugIn')
            self.pn.OpenCfgFile('vPrjDocsCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            EVT_NET_XML_SYNCH_START(self.netDocMain,self.OnSynchStart)
            EVT_NET_XML_SYNCH_PROC(self.netDocMain,self.OnSynchProc)
            EVT_NET_XML_SYNCH_FINISHED(self.netDocMain,self.OnSynchFinished)
            EVT_NET_XML_SYNCH_ABORTED(self.netDocMain,self.OnSynchAborted)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        self.SetName(name)
        #self.Layout()
        #self.Refresh()
