#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: __init__.py,v 1.1 2006/11/21 21:25:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE='vPrjDocsMDIFrame'
PLUGABLE_FRAME=True
PLUGABLE_ACTIVE=0
PLUGABLE_SRV=['vXmlPrjDocs@vXmlSrv@vPrjDoc']

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
