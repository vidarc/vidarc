#----------------------------------------------------------------------------
# Name:         vXmlPrjDocsTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlPrjDocsTree.py,v 1.1 2006/11/21 21:25:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vPrjDocs.vNetPrjDocs import *

import vidarc.vApps.vPrjDocs.images as imgPrjDocs

class vXmlPrjDocsTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','|id'])
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgPrjDocs.getPluginImage(),
                            imgPrjDocs.getPluginImage(),True)
            self.__addElemImage2ImageList__('PrjDocs',
                            imgPrjDocs.getPluginImage(),
                            imgPrjDocs.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)
