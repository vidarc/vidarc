#----------------------------------------------------------------------------
# Name:         vXmlPrjDocs.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlPrjDocs.py,v 1.2 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vPrjDocs.vXmlNodePrjDocsRoot import vXmlNodePrjDocsRoot
from vidarc.vApps.vPrjDocs.vXmlNodePrjDoc import vXmlNodePrjDoc

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xcdIDAT8\x8d\x8d\x93\xcdj\x13a\x14\x86\x9f\x99\xb11b\xc0\x98E\x93\
\x88i\x10\x8c\xb3q\xa8\x85\x88P\x04\xaf\xa0R\xb0\xa0\x0b\x7f\xa0^\x81;\x17^@\
.\xc0\xb5;w\xee\xac\xe2N\xe8\xa6\xd4*\x08I\xed\x14\xa4\x93\x9a\x9f&u\xd2X\
\x93I\xd3Nf>\x17:\xa13\x99\x01\xcf\xea\xe3\xe5=\xefw~\xde#I\xb2B0\xb4\xd2\
\x92\x98\x00\x81\xf2\xf37R\x10\x93\xff\x97\x18\x86\x01H^\x05Q\xbfF\x85\'x&\
\x8a\xf0X\xb9\x89"+\xa8\xaa\xca\xc8\x1e\xf1b\xfbu(/\xb4\x85Gr\x11\xd7q)\x14\
\nX\x96E"\x91`96\xcfC\xf7\xfa\xa4@\xb0\xf4\xa7\xf1y\x14YA\xd34\x8e\x06G\xa4R\
)*\x95\n\x86Qe\xa7\xbc\xc6\xfd\xda\xfe\x98\xab\x95\x96\x84\xec=\x00J\xda\x13\
b\xb1\x18s7\xe6\xb0m\x9bL&\xf3/\xd9\xc0l\xef\xa1fm\x0e[\x15N\xe7\x8c[x9{\x8f\
\xc6\xd6*\x85k*\xd3\xe94\xb9\xcb9t]g\xb7\xfa\x83\xfdV\x93b\xde\xe2\xe2\xe8+\
\xbd\xde\xef\xf0\x19\x98\xdf\xca\xdcI\x1c#\x8c\r\xcc=\x03!I\xf4\xfa}Z\xcd\
\x1a\xc5\xbcEr\xb8\xceN\xfd\x17\xc9\xab\x0b>\x81\xf1\x16\x94\xa1\xc0\xed\xd4\
\x89\xbb:i\xa7C\xa7\x9b\xe3\xa0\xbdK1? y\xfc\x89j\xa3\xcb(\xbb\xc8\xbb\x9c\
\x7f\xee2\xfc\xdd\xa9$\x04\xdd\x93C6jklm\xbeg\xe6\xe7gf\xcf\xeb\\\x18\xaeS\
\xadw8I/\xb0r*\xd9\xf3\x81\xcfH\xaf\x9c\x14SS\rl\xfa\\:;\xcd\xc7\xcd\x0fl\
\x9b\x16Nv\x91\xb7\x81\x9f=\x01\x1f\xba\xac\x1c`\x9e\xbb\x8d=\xb8\x82\xd3\
\xb7i\xf7d\xec\xcc\xdd\x89\xe4\xd0\x19x\xf1\xcc\xfa\x02qx\xf0]\xd0\x14\xb7X\
\x9d\x894+\xbe\x16\x82\x114X\xd41\x85\xd6\x16vXQ\xc7\xf6\x07\x82\xbf\xc2\\\
\xc8\xe6\x9d\x19\x00\x00\x00\x00IEND\xaeB`\x82'

class vXmlPrjDocs(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='PrjDocsRoot'
    APPL_REF=['vHum','vGlobals','vPrj']
    def __init__(self,appl='vPrjDocs',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                                audit_trail=audit_trail)
            oRoot=vXmlNodePrjDocsRoot()
            self.RegisterNode(oRoot,False)
            oPrjDoc=vXmlNodePrjDoc()
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oPrjDoc,True)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            self.LinkRegisteredNode(oRoot,oPrjDoc)
            self.LinkRegisteredNode(oPrjDoc,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'PrjDocs')
    #def New(self,revision='1.0',root='PrjDocsRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'PrjDocs')
        if bConnection==False:
            vtXmlDomReg.CreateReq(self)
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def _buildSumCache(self,node,oReg):
        if self.getTagName(node)==oReg.GetTagName():
            sTag=oReg.GetTag(node)
            id=self.getKeyNum(node)
            self.dCache[sTag]=(id,node)
        return 0
    def BuildCache(self):
        self.dCache={}
        oPrjDoc=self.GetReg('PrjDoc')
        self.procChildsKeys(self.getBaseNode(),self._buildSumCache,oPrjDoc)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dCache:%s'%(vtLog.pformat(self.dCache)),self.GetOrigin())
    def FreeCache(self):
        del self.dCache
        self.dCache={}
    def _setFullSummarizedInfo(self,oReg,node,applAlias,docSum):
        oReg.SetTag(node,applAlias)
        self._setSummarizedInfo(oReg,node,applAlias,docSum)
    def _setSummarizedInfo(self,oReg,node,applAlias,docSum):
        fid,appl=docSum.getForeignKeyNumAppl(docSum.getBaseNode())#,attr='prjfid')
        oReg.SetProjectForeignKey(node,fid)
        return
        nodePrjInfo=self.getChild(node,'prjinfo')
        nodeOrig=docSum.getChild(docSum.getRoot(),'prjinfo')
        if nodeOrig is None:
            vtLog.vtLngCur(vtLog.WARN,'alias:%s prjinfo not found'%(applAlias),self.GetOrigin())
        else:
            print nodeOrig
            fid,appl=docSum.getForeignKeyNumAppl(nodeOrig)
            nodeTmp=docSum.cloneNode(nodeOrig,True)
            
            if nodePrjInfo is not None:
                self.delNode(nodePrjInfo)
            self.appendChild(node,nodeTmp)
            self.setForeignKey(nodeTmp,attr='fid',attrVal=fid,appl='vPrj')
    def AddSummarizedByDoc(self,applAlias,docSum):
        oPrjDoc=self.GetReg('PrjDoc')
        if applAlias in self.dCache:
            t=self.dCache[applAlias]
            node=t[1]
            self._setSummarizedInfo(oPrjDoc,node,applAlias,docSum)
            #vtLog.vtLngCur(vtLog.ERROR,'alias:%s already in cache'%(),self)
            pass
        else:
            oPrjDoc.Create(self.getBaseNode(),self._setFullSummarizedInfo,applAlias,docSum)
        pass
    def DelSummarized(self,applAlias):
        pass
