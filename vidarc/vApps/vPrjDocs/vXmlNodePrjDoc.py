#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDocs.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlNodePrjDoc.py,v 1.1 2006/11/21 21:25:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.sec.vtSecXmlAclDom import vtSecXmlAclDom

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vXmlNodePrjDocPanel import vXmlNodePrjDocPanel
        from vXmlNodePrjDocEditDialog import vXmlNodePrjDocEditDialog
        from vXmlNodePrjDocAddDialog import vXmlNodePrjDocAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrjDoc(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='PrjDoc'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDocs')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project document')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetProjectForeignKey(self,node):
        fid=self.GetChildForeignKey(node,'project','fid','vPrj')
        return fid
    def GetProjectID(self,node):
        fid=self.GetChildAttrStr(node,'project','fid')
        return fid
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetProjectForeignKey(self,node,val):
        self.SetChildForeignKey(node,'project','fid',val,'vPrj')
    def SetProjectID(self,node,val):
        self.GetChildAttrStr(node,'project','fid',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodePrjDocEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodePrjDocAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePrjDocPanel
        else:
            return None
