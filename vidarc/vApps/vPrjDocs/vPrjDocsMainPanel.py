#Boa:FramePanel:vPrjDocsMainPanel
#----------------------------------------------------------------------------
# Name:         vPrjDocsPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vPrjDocsMainPanel.py,v 1.2 2007/02/19 12:32:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

try:
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vPrjDocs.vNetPrjDocs import *
    from vidarc.vApps.vPrjDocs.vXmlPrjDocsTree import *
    
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    
    import vidarc.vApps.vPrjDocs.images as imgPrjDocs
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vPrjDocsMainPanel(parent)

[wxID_VPRJDOCSMAINPANEL, wxID_VPRJDOCSMAINPANELPNDATA, 
 wxID_VPRJDOCSMAINPANELSLWNAV, 
] = [wx.NewId() for _init_ctrls in range(3)]

def getPluginImage():
    return imgPrjDocs.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(imgPrjDocs.getPluginBitmap())
    return icon

class vPrjDocsMainPanel(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJDOCSMAINPANEL,
              name=u'vPrjDocsMainPanel', parent=prnt, pos=wx.Point(115, 28),
              size=wx.Size(665, 200), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(657, 173))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPRJDOCSMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPRJDOCSMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VPRJDOCSMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(440, 160),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDocs')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        
        self.xdCfg=vtXmlDom(appl='vPrjDocsCfg',audit_trail=False)
        
        appls=['vPrjDocs','vHum','vGlobals','vPrj']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netPrjDocs=self.netMaster.AddNetControl(vNetPrjDocs,'vPrjDocs',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjDocsMaster')
        
        self.vgpTree=vXmlPrjDocsTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trPrjDocs",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPrjDocs,True)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netPrjDocs
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
    def OpenFile(self,fn):
        if self.netPrjDocs.ClearAutoFN()>0:
            self.netPrjDocs.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            #i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            #self.slwNav.SetDefaultSize((i, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnMove(self, event):
        event.Skip()
        try:
            pos=self.GetParent().GetPosition()
            self.dCfg['pos']='%d,%d'%(pos[0],pos[1])
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Project documents module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC PrjDocs'),desc,version
