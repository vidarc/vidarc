
import sys, os, glob, fnmatch, tempfile
from distutils.core      import setup, Extension
from distutils.file_util import copy_file
from distutils.dir_util  import mkpath
from distutils.dep_util  import newer
from distutils.spawn     import spawn

import distutils.command.install
import distutils.command.install_data
import distutils.command.install_headers
import distutils.command.clean

import __config__

def build_options(**kwargs):
    
    #----------------------------------------------------------------------
    # build options file
    #----------------------------------------------------------------------
    #try:
    #    i=VIDARC_IMPORT
    #except:
    #    VIDARC_IMPORT=1
    print kwargs
    keys=kwargs.keys()
    keys.sort()
    build_options_template = """"""
    for k in keys:
        build_options_template += """%s=%d
        """%(k,kwargs[k])
    #build_options_template = """
    #VIDARC_IMPORT=%d
    #""" % (VIDARC_IMPORT)
    
    try: 
        from build_options import *
    except:
        build_options_file = os.path.join(os.path.dirname(__file__), "build_options.py")
        if not os.path.exists(build_options_file):
            try:
                myfile = open(build_options_file, "w")
                myfile.write(build_options_template)
                myfile.close()
            except:
                print "WARNING: Unable to create build_options.py."
    

