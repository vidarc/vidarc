#Boa:Frame:vPlugInMainFrame
#----------------------------------------------------------------------------
# Name:         vPlugInMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vPlugInMainFrame.py,v 1.1 2006/07/19 12:51:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import getpass,time

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.tool.log.vtLogFileViewerFrame import *

from vidarc.vApps.vPlugIn.vXmlPlugInTree  import *
from vidarc.vApps.vPlugIn.vNetPlugIn import *
from vidarc.vApps.vPlugIn.vNetPlugInFile import *
#from vidarc.vApps.vPlugIn.vPlugInListBook import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

import vidarc.vApps.vPlugIn.images as images

from vidarc.vApps.vHum.vNetHum import *

def create(parent):
    return vPlugInMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VPLUGINMAINFRAME, wxID_VPLUGINMAINFRAMEPNDATA, 
 wxID_VPLUGINMAINFRAMESBSTATUS, wxID_VPLUGINMAINFRAMESLWNAV, 
 wxID_VPLUGINMAINFRAMEVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(5)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VPLUGINMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

class vPlugInMainFrame(wx.Frame,vMainFrame,vStatusBarMessageLine):
    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.pnData.SetSizer(self.fgsData)


    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 0, border=4, flag=wx.EXPAND | wx.ALL)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VPLUGINMAINFRAMEMNFILEITEM_EXIT)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VPLUGINMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VPLUGINMAINFRAME,
              name=u'vPlugInMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 354), style=wx.DEFAULT_FRAME_STYLE,
              title=u'VIDARC PlugIn')
        self._init_utils()
        self.SetClientSize(wx.Size(811, 327))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VPLUGINMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPLUGINMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              288), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPLUGINMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VPLUGINMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(592, 272),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VPLUGINMAINFRAMEVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(580, 260), style=0)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        vMainFrame.__init__(self)
        vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,5000)
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='cfg')
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        appls=['vPlugIn','vHum','vPlugInFile']
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        
        self.netPlugIn=self.netMaster.AddNetControl(vNetPlugIn,'vPlugIn',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netPlugInFile=self.netMaster.AddNetControl(vNetPlugInFile,'vPlugInFile',synch=True,verbose=2)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPlugInAppl')
        EVT_NET_XML_OPEN_OK(self.netPlugIn,self.OnOpenOk)
        
        EVT_NET_XML_SYNCH_PROC(self.netPlugIn,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netPlugIn,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netPlugIn,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netPlugIn,self.OnGotContent)
        
        self.bAutoConnect=True
        
        self.vgpTree=vXmlPlugInTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trEng",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPlugIn,True)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        
        oPlugIn=self.netPlugIn.GetRegisteredNode('PlugInSrvDir')
        pnCls=oPlugIn.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPlugIn")
        pn.SetDoc(self.netPlugIn,True)
        self.viRegSel.AddWidgetDependent(pn,oPlugIn)
        pn.Show(False)
        
        oPlugInSub=self.netPlugIn.GetRegisteredNode('PlugInSrvDirSub')
        pnCls=oPlugInSub.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPlugInSub")
        pn.SetDoc(self.netPlugIn,True)
        self.viRegSel.AddWidgetDependent(pn,oPlugInSub)
        pn.Show(False)
        
        self.viRegSel.SetDoc(self.netPlugIn,True)
        self.viRegSel.SetRegNode(oPlugIn)
        self.viRegSel.SetRegNode(oPlugInSub)
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        self.Layout()
        self.Fit()
        self.OpenCfgFile()
        self.__makeTitle__()
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC PlugIn")
        fn=self.netPlugIn.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('login ... '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netPlugIn.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OpenFile(self,fn):
        if self.netPlugIn.ClearAutoFN()>0:
            self.netPlugIn.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        self.netPlugIn.New()
        
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        return
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netPlugIn.getBaseNode()
        self.baseSettings=self.netPlugIn.getChildForced(self.netPlugIn.getRoot(),'settings')
        self.__getSettings__()
    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netPlugIn.getChild(self.netPlugIn.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vPlugInSettingsDialog(self)
            dlg.SetXml(self.netPlugIn,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netPlugIn,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnFrameClose(self, event):
        if self.netMaster.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vPlugIn'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.netMaster.Save(bModified=True)
        self.netMaster.ShutDown()
        event.Skip()

    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()
    def __setCfg__(self):
        try:
            node=self.xdCfg.getChildForced(None,'size')
            iX=self.xdCfg.GetValue(node,'x',int,20)
            iY=self.xdCfg.GetValue(node,'y',int,20)
            iWidth=self.xdCfg.GetValue(node,'width',int,800)
            iHeight=self.xdCfg.GetValue(node,'height',int,600)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.xdCfg.getChildForced(None,'layout')
            
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            
            self.xdCfg.AlignDoc()
            self.xdCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.xdCfg.SetValue(node,'nav_sash',iWidth)
            self.xdCfg.Save()
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'width',sz[0])
            self.xdCfg.SetValue(node,'height',sz[1])
            self.xdCfg.Save()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        try:
            self.hlp=wx.html.HtmlHelpController()
            self.hlp.AddBook('vPlugInHelpBook.hhp')
            self.hlp.DisplayContents()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        pass
    def GetCfgData(self):
        d={}
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)
