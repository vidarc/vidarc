#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: __init__.py,v 1.1 2006/07/19 12:51:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE='vPlugInMDIFrame'
PLUGABLE_FRAME=True
PLUGABLE_ACTIVE=0

