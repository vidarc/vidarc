#----------------------------------------------------------------------------
# Name:         vNetPlugInFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vNetPlugInFile.py,v 1.1 2006/07/19 12:51:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.net.vtNetFileWxGui import *

class vNetPlugInFile(vtNetFileWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0):
        vtNetFileWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
    def ListContent(self,sTag):
        self.cltSock.ListContent(sTag)
    def GetFile(self,sTag,sDN,sFN):
        self.cltSock.GetFile(sTag,sDN,sFN)
