#Boa:FramePanel:vXmlNodePlugInSubPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePlugInSubPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlNodePlugInSubPanel.py,v 1.2 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import wx.lib.filebrowsebutton
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEPLUGINSUBPANEL, wxID_VXMLNODEPLUGINSUBPANELCBUPDATE, 
 wxID_VXMLNODEPLUGINSUBPANELLBLPLUGIN, wxID_VXMLNODEPLUGINSUBPANELLSTPLUGIN, 
 wxID_VXMLNODEPLUGINSUBPANELVINAME, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodePlugInSubPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUpdate, 0, border=0, flag=0)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viName, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblPlugIn, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstPlugIn, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_lstPlugIn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'plugin'), width=100)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'remote'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'local'), width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=3, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPLUGINSUBPANEL,
              name=u'vXmlNodePlugInSubPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(342, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(334, 180))
        self.SetAutoLayout(True)

        self.lstPlugIn = wx.ListCtrl(id=wxID_VXMLNODEPLUGINSUBPANELLSTPLUGIN,
              name=u'lstPlugIn', parent=self, pos=wx.Point(4, 55),
              size=wx.Size(295, 121), style=wx.LC_REPORT)
        self._init_coll_lstPlugIn_Columns(self.lstPlugIn)

        self.lblPlugIn = wx.StaticText(id=wxID_VXMLNODEPLUGINSUBPANELLBLPLUGIN,
              label=_(u'plugin'), name=u'lblPlugIn', parent=self,
              pos=wx.Point(4, 38), size=wx.Size(295, 13), style=0)

        self.cbUpdate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEPLUGINSUBPANELCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbUpdate',
              parent=self, pos=wx.Point(303, 51), size=wx.Size(31, 30),
              style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VXMLNODEPLUGINSUBPANELCBUPDATE)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPLUGINSUBPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(4, 4), size=wx.Size(295,
              30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPlugIn')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viName.Clear()
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viName.SetDoc(doc)
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viName.SetNode(self.node)
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viName.Close()
        
    def Cancel(self):
        # add code here
        self.viName.Cancel()
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            #self.lstPlugIn.Enable(False)
            #self.cbUpdate.Enable(False)
            pass
        else:
            # add code here
            #self.lstPlugIn.Enable(True)
            #self.cbUpdate.Enable(True)
            pass
    def OnCbUpdateButton(self, event):
        event.Skip()
