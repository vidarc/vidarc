#Boa:FramePanel:vXmlNodePlugInPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePlugInPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlNodePlugInPanel.py,v 1.2 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import wx.lib.filebrowsebutton
import wx.lib.buttons

import sys,os,stat,time,Queue

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
from vidarc.tool.net.vtNetFileWxGui import *
from vidarc.tool.vtThread import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEPLUGINPANEL, wxID_VXMLNODEPLUGINPANELCBFIND, 
 wxID_VXMLNODEPLUGINPANELCBUPDATE, wxID_VXMLNODEPLUGINPANELDBBDN, 
 wxID_VXMLNODEPLUGINPANELLBLPLUGIN, wxID_VXMLNODEPLUGINPANELLBLPLUGINS, 
 wxID_VXMLNODEPLUGINPANELLSTPLUGIN, wxID_VXMLNODEPLUGINPANELPBPLUGIN, 
 wxID_VXMLNODEPLUGINPANELPBPLUGINS, wxID_VXMLNODEPLUGINPANELTXTACTPLUGINFN, 
 wxID_VXMLNODEPLUGINPANELVINAME, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vXmlNodePlugInPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbFind, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbUpdate, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viName, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.dbbDN, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblPlugIn, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstPlugIn, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsPlugInBar, 0, border=4,
              flag=wx.LEFT | wx.RIGHT | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.txtActPlugInFN, 0, border=4,
              flag=wx.EXPAND | wx.ALL)

    def _init_coll_bxsPlugInBar_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPlugins, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.pbPlugIns, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.pbPlugIn, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_lstPlugIn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'plugin'), width=100)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'remote'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'local'), width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=6, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsPlugInBar = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsPlugInBar_Items(self.bxsPlugInBar)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPLUGINPANEL,
              name=u'vXmlNodePlugInPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(352, 263),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(344, 236))
        self.SetAutoLayout(True)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPLUGINPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(4, 4), size=wx.Size(297,
              30), style=0)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle='Choose a directory',
              id=wxID_VXMLNODEPLUGINPANELDBBDN, labelText='Select a directory:',
              parent=self, pos=wx.Point(4, 38), size=wx.Size(297, 29),
              startDirectory='.', style=0)
        self.dbbDN.SetMinSize(wx.Size(-1, -1))

        self.lstPlugIn = wx.ListCtrl(id=wxID_VXMLNODEPLUGINPANELLSTPLUGIN,
              name=u'lstPlugIn', parent=self, pos=wx.Point(4, 88),
              size=wx.Size(297, 98), style=wx.LC_REPORT)
        self.lstPlugIn.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstPlugIn_Columns(self.lstPlugIn)

        self.lblPlugIn = wx.StaticText(id=wxID_VXMLNODEPLUGINPANELLBLPLUGIN,
              label=_(u'plugin'), name=u'lblPlugIn', parent=self,
              pos=wx.Point(4, 71), size=wx.Size(297, 13), style=0)
        self.lblPlugIn.SetMinSize(wx.Size(-1, -1))

        self.cbFind = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEPLUGINPANELCBFIND,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'cbFind', parent=self,
              pos=wx.Point(309, 88), size=wx.Size(31, 30), style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VXMLNODEPLUGINPANELCBFIND)

        self.cbUpdate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEPLUGINPANELCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbUpdate',
              parent=self, pos=wx.Point(309, 130), size=wx.Size(31, 30),
              style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VXMLNODEPLUGINPANELCBUPDATE)

        self.lblPlugins = wx.StaticText(id=wxID_VXMLNODEPLUGINPANELLBLPLUGINS,
              label=u'plugins', name=u'lblPlugins', parent=self, pos=wx.Point(4,
              194), size=wx.Size(74, 13), style=wx.ALIGN_RIGHT)

        self.pbPlugIns = wx.Gauge(id=wxID_VXMLNODEPLUGINPANELPBPLUGINS,
              name=u'pbPlugIns', parent=self, pos=wx.Point(82, 194), range=100,
              size=wx.Size(70, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIns.SetMinSize(wx.Size(-1, 13))

        self.pbPlugIn = wx.Gauge(id=wxID_VXMLNODEPLUGINPANELPBPLUGIN,
              name=u'pbPlugIn', parent=self, pos=wx.Point(156, 194), range=100,
              size=wx.Size(144, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIn.SetMinSize(wx.Size(-1, 13))

        self.txtActPlugInFN = wx.TextCtrl(id=wxID_VXMLNODEPLUGINPANELTXTACTPLUGINFN,
              name=u'txtActPlugInFN', parent=self, pos=wx.Point(4, 211),
              size=wx.Size(297, 21), style=0, value=u'')
        self.txtActPlugInFN.Enable(False)
        self.txtActPlugInFN.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPlugIn')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viName.SetTagName('name')
        self.netFile=None
        self.lLocalFN=[]
        self.lRemoteFN=[]
        self.qFilesProc=Queue.Queue()
        self.qFiles=Queue.Queue()
        self.thdPlugIn=vtThread(self,bPost=True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viName.Clear()
        self.lstPlugIn.DeleteAllItems()
        self.lLocalFN=[]
        self.lRemoteFN=[]
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        if d.has_key('vPlugInFile'):
            dd=d['vPlugInFile']
            if self.netFile is not None:
                EVT_NET_FILE_LIST_CONTENT_DISCONNECT(self,self.OnListContent)
                EVT_NET_FILE_GET_START_DISCONNECT(self.netFile,self.OnFileGetStart)
                EVT_NET_FILE_GET_PROC_DISCONNECT(self.netFile,self.OnFileGetProc)
                EVT_NET_FILE_GET_FIN_DISCONNECT(self.netFile,self.OnFileGetFin)
                EVT_NET_FILE_GET_ABORT_DISCONNECT(self.netFile,self.OnFileGetAbort)
            self.netFile=dd['doc']
            EVT_NET_FILE_LIST_CONTENT(self.netFile,self.OnListContent)
            EVT_NET_FILE_GET_START(self.netFile,self.OnFileGetStart)
            EVT_NET_FILE_GET_PROC(self.netFile,self.OnFileGetProc)
            EVT_NET_FILE_GET_FIN(self.netFile,self.OnFileGetFin)
            EVT_NET_FILE_GET_ABORT(self.netFile,self.OnFileGetAbort)
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viName.SetDoc(doc)
    def __getLocal__(self,sDN):
        lDir=os.listdir(sDN)
        self.lLocalFN=[]
        for f in lDir:
            sFN=os.path.join(sDN,f)
            s=os.stat(sFN)
            if stat.S_ISREG(s[stat.ST_MODE]):
                self.lLocalFN.append(f)
    def __addPlugIn__(self,d,sPlugIn,sVer,sFN,what):
        if sPlugIn in d:
            try:
                iVerNums=[int(s) for s in sVer.split('.')]
            except:
                return
            if what not in d[sPlugIn]:
                d[sPlugIn][what]=''
                d[sPlugIn][what+'FN']=''
                bNewer=True
            else:
                sVerOld=d[sPlugIn][what]
                iVerOldNums=[int(s) for s in sVerOld.split('.')]
                bNewer=False
                for iVerOldNum,iVerNum in zip(iVerOldNums,iVerNums):
                    if iVerOldNum<iVerNum:
                        bNewer=True
                        break
            if bNewer:
                d[sPlugIn][what]=sVer
                d[sPlugIn][what+'FN']=sFN
        else:
            d[sPlugIn]={what:sVer,what+'FN':sFN}
        
    def __showPlugIn__(self):
        try:
            d={}
            self.dPlugIn=d
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'local:%s'%(vtLog.pformat(self.lLocalFN)),self)
            for sFN in self.lLocalFN:
                strs=sFN.split('.')
                try:
                    if len(strs)<5:
                        continue
                    try:
                        int(strs[-1])
                        continue
                    except:
                        pass
                    sPlugIn='.'.join(strs[:-4])
                    sVer='.'.join(strs[-4:-1])
                    self.__addPlugIn__(d,sPlugIn,sVer,sFN,'local')
                except:
                    vtLog.vtLngTB(self.GetName())
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'remote:%s'%(vtLog.pformat(self.lRemoteFN)),self)
            for sFN in self.lRemoteFN:
                strs=sFN.split('.')
                try:
                    if len(strs)<5:
                        continue
                    sPlugIn='.'.join(strs[:-4])
                    sVer='.'.join(strs[-4:-1])
                    self.__addPlugIn__(d,sPlugIn,sVer,sFN,'remote')
                except:
                    vtLog.vtLngTB(self.GetName())
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'plugins:%s'%(vtLog.pformat(d)),self)
            self.lstPlugIn.DeleteAllItems()
            keys=d.keys()
            keys.sort()
            for k in keys:
                idx=self.lstPlugIn.InsertStringItem(sys.maxint,k)
                dd=d[k]
                if 'remote' in dd:
                    self.lstPlugIn.SetStringItem(idx,1,dd['remote'])
                if 'local' in dd:
                    self.lstPlugIn.SetStringItem(idx,2,dd['local'])
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viName.SetNode(self.node)
            sDir=self.objRegNode.GetDirLocal(node)
            self.dbbDN.SetValue(sDir)
            self.__getLocal__(sDir)
            self.__showPlugIn__()
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            sDir=self.dbbDN.GetValue()
            self.objRegNode.SetDirLocal(node,sDir)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viName.Close()
    def Cancel(self):
        # add code here
        self.viName.Cancel()
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.dbbDN.Enable(False)
            #self.lstPlugIn.Enable(False)
            #self.cbUpdate.Enable(False)
        else:
            # add code here
            self.dbbDN.Enable(True)
            #self.lstPlugIn.Enable(True)
            #self.cbUpdate.Enable(True)

    def OnCbUpdateButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                return
            iCount=self.lstPlugIn.GetItemCount()
            l=[]
            for idx in xrange(iCount):
                if self.lstPlugIn.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                    sPlugIn=self.lstPlugIn.GetItem(idx,0).m_text
                    d=self.dPlugIn[sPlugIn]
                    if 'remoteFN' in d:
                        l.append(d['remoteFN'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'plugIn2Get:%s'%(vtLog.pformat(l)),self)
            sDir=self.objRegNode.GetDirLocal(self.node)
            netTmp=self.doc.GetNetDoc('vPlugIn')
            sTag=netTmp.GetHierTag(self.node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s'%(sTag),self)
            self.thdPlugIn.Do(self.__updatePlugIn__,l,sDir,self.netFile,sTag)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbFindButton(self, event):
        event.Skip()
        if self.node is None:
            return
        try:
            netTmp=self.doc.GetNetDoc('vPlugIn')
            sTag=netTmp.GetHierTag(self.node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s'%(sTag),self)
            self.netFile.ListContent(u'vPlugInFile:'+sTag)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnListContent(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(vtLog.pformat(evt.GetList())),self)
        self.lRemoteFN=evt.GetList()
        self.__showPlugIn__()
    def __updatePlugIn__(self,lPlugIn,sDir,netFile,sTag):
        iCount=len(lPlugIn)
        iAct=0
        
        for sFN in lPlugIn:
            self.qFiles.put(sFN)
            try:
                iAct+=1
                self.pbPlugIns.SetValue(iAct)
                netFile.GetFile(u'vPlugInFile:'+sTag,sDir,sFN)
            except:
                vtLog.vtLngTB(self.GetName())
        self.pbPlugIns.SetValue(0)
    def __updateFilesBar__(self):
        self.pbPlugIns.SetValue(self.qFilesProc.qsize())
        self.pbPlugIns.SetRange(self.qFiles.qsize())
        
    def OnFileGetStart(self,event):
        try:
            self.qFilesProc.put(event.GetFN())
            self.__updateFilesBar__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFileGetProc(self,event):
        try:
            lSize=event.GetSize()
            lAct=event.GetAct()
            fPer=lAct/float(lSize)*100.0
            iPer=int(fPer)
            self.pbPlugIn.SetValue(iPer)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFileGetFin(self,event):
        try:
            if self.qFilesProc.qsize()==self.qFiles.qsize():
                self.qFilesProc.empty()
                self.qFiles.empty()
                self.__updateFilesBar__()
                self.pbPlugIns.SetValue(0)
                self.pbPlugIn.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFileGetAbort(self,event):
        try:
            if self.qFilesProc.qsize()==self.qFiles.qsize():
                self.qFilesProc.empty()
                self.qFiles.empty()
                self.__updateFilesBar__()
                self.pbPlugIns.SetValue(0)
                self.pbPlugIn.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
