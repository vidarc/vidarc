#----------------------------------------------------------------------------
# Name:         vXmlPlugIn.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlPlugIn.py,v 1.2 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vPlugIn.vXmlNodePlugInRoot import vXmlNodePlugInRoot
from vidarc.vApps.vPlugIn.vXmlNodePlugIn import vXmlNodePlugIn
from vidarc.vApps.vPlugIn.vXmlNodePlugInSub import vXmlNodePlugInSub
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML
from vidarc.tool.net.vtNetXmlNodeHostsClt import vtNetXmlNodeHostsClt

class vXmlPlugIn(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='PlugInSrvRoot'
    APPL_REF=['vHum','vPlugInFile']
    def __init__(self,appl='vPlugIn',attr='id',skip=[],synch=False,verbose=0):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose)
            oRoot=vXmlNodePlugInRoot()
            self.RegisterNode(oRoot,False)
            oPlugIn=vXmlNodePlugIn()
            oPlugInSub=vXmlNodePlugInSub()
            oHosts=vtNetXmlNodeHostsClt()
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oPlugIn,True)
            self.RegisterNode(oPlugInSub,False)
            self.RegisterNode(oHosts,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            #self.LinkRegisteredNode(oRoot,oPlugIn)
            self.LinkRegisteredNode(oPlugIn,oPlugInSub)
            self.LinkRegisteredNode(oPlugInSub,oPlugInSub)
            #self.LinkRegisteredNode(oPlugIn,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'PlugIns')
    #def New(self,revision='1.0',root='PlugInSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'PlugIns')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def __getHier__(self,node,lst):
        o=self.GetRegByNode(node)
        sTag=o.GetTag(node)
        lst.append(sTag)
        if o.GetTagName()!='PlugInSrvDir':
            self.__getHier__(self.getParent(node),lst)
    def GetHierTag(self,node):
        lTag=[]
        self.__getHier__(node,lTag)
        lTag.reverse()
        return '/'.join(lTag)
