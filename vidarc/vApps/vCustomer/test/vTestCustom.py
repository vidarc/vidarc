
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlDom import *
from vidarc.vApps.vCustomer.vXmlCustomer import vXmlCustomer
from vidarc.vApps.vCustomer.vXmlCustomerInputTree import *

def create(self):
    self.inTst1=vXmlCustomerInputTree(self.panel,id=-1,pos=(4,4),
                        name='inTst1')
    EVT_VTINPUT_TREE_CHANGED(self.inTst1,OnTst1Changed)
    #self.inTst1.SetTreeFunc(createXmlTree,show)

    self.doc=vtXmlDom()
    self.doc.SetLanguages([
        ('english','en',vtArt.getBitmap(vtArt.LangEn)),
        ('german','de',vtArt.getBitmap(vtArt.LangDe)),
        ])
    self.doc.SetLang('en')
    self.doc.Open(self.FNbase+'_0.xml')
    
    docInfo=vXmlCustomer()
    #docInfo=vtXmlDom()
    docInfo.Open(self.FNbase+'.xml')
    docInfo.SetLanguages([
        ('english','en',vtArt.getBitmap(vtArt.LangEn)),
        ('german','de',vtArt.getBitmap(vtArt.LangDe)),
        ])
    docInfo.SetLang('en')
    self.inTst1.SetDocTree(docInfo)
    nodeTmp=docInfo.getChildByLst(docInfo.getRoot(),['customersroot','customers'])
    #nodeTmp=docInfo.getChildByLst(docInfo.getRoot(),['customers','customer'])
    nodeTmp=docInfo.getChildByLst(docInfo.getRoot(),['customers'])
    self.inTst1.SetNodeTree(nodeTmp)

    self.inTst1.SetDoc(self.doc)
    #self.inTst2.SetDoc(self.doc)
    SetNode(self)
    
    return 150,-5        # x-offset , timeout
def SetNode(self):
    node=self.doc.getChildByLst(self.doc.getRoot(),['prj','projects'])
    self.inTst1.SetNode(node)
    
def OnTst1Changed(evt):
    vtLog.CallStack('')
    print evt.GetNode()
    print evt.GetId()
    print evt.GetValue()
def OnTst2Changed(evt):
    vtLog.CallStack('')
    print evt.GetNode()
    print evt.GetId()
    print evt.GetValue()

import test.vTestAppl
test.vTestAppl.run(__file__,create,SetNode)
