#Boa:Dialog:vXmlNodeCustomerAddDialog

import wx
import wx.lib.buttons

from vXmlNodeCustomerPanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeAddDialog import *

def create(parent):
    return vXmlNodeCustomerAddDialog(parent)

[wxID_VXMLNODECUSTOMERADDDIALOG, wxID_VXMLNODECUSTOMERADDDIALOGCBAPPLY, 
 wxID_VXMLNODECUSTOMERADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeCustomerAddDialog(wx.Dialog,vtXmlNodeAddDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODECUSTOMERADDDIALOG,
              name=u'vXmlNodeCustomerAddDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(510, 360), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLoc Customer Add Dialog')
        self.SetClientSize(wx.Size(502, 333))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODECUSTOMERADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(96, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODECUSTOMERADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODECUSTOMERADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(128, 24),
              size=wx.Size(96, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODECUSTOMERADDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        self._init_ctrls(parent)
        vtXmlNodeAddDialog.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vXmlNodeCustomerPanel(self,id=wx.NewId(),pos=(0,0),size=(500,280),
                        style=0,name=u'pnLoc')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
        except:
            vtLog.vtLngTB('')
    
