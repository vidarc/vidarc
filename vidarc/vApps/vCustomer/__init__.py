#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: __init__.py,v 1.3 2006/07/17 11:06:19 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE='vCustomerMDIFrame'
PLUGABLE_FRAME=True
PYTHON_LOGGIN_SOCKET=0
PLUGABLE_SRV='vXmlCustomer@vXmlSrv'

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
