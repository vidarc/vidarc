#Boa:FramePanel:vXmlNodeAreaPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeAreaPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060216
# CVS-ID:       $Id: vXmlNodeAreaPanel.py,v 1.6 2007/08/21 18:16:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML

import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

VERBOSE=0

[wxID_VXMLNODEAREAPANEL, wxID_VXMLNODEAREAPANELLBLNAME, 
 wxID_VXMLNODEAREAPANELLBLTAG, wxID_VXMLNODEAREAPANELTXTMLNAME, 
 wxID_VXMLNODEAREAPANELTXTTAG, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodeAreaPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, (1, 0), border=4,
              flag=wx.RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.txtTag, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              2))
        parent.AddWindow(self.lblTag, (0, 0), border=4,
              flag=wx.RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.txtmlName, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEAREAPANEL,
              name=u'vXmlNodeAreaPanel', parent=prnt, pos=wx.Point(286, 253),
              size=wx.Size(222, 83), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(214, 56))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEAREAPANELLBLTAG,
              label=u'tag', name=u'lblTag', parent=self, pos=wx.Point(0, 21),
              size=wx.Size(72, 30), style=wx.ALIGN_RIGHT)

        self.txtTag = wx.TextCtrl(id=wxID_VXMLNODEAREAPANELTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(76, 0),
              size=wx.Size(130, 21), style=0, value=u'')

        self.txtmlName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEAREAPANELTXTMLNAME,
              name=u'txtmlName', parent=self, pos=wx.Point(76, 21),
              size=wx.Size(130, 30), size_button=wx.Size(31, 30),
              size_text=wx.Size(100, 22), style=0)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEAREAPANELLBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(72, 21), style=wx.ALIGN_RIGHT)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        self._init_ctrls(parent)
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self.bModified=False
        self.verbose=VERBOSE
        self.gbsData.AddGrowableCol(1)
        
        self.objRegNode=None
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def SetModified(self,state):
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        if self.verbose:
            vtLog.CallStack('')
        self.SetModified(False)
        self.doc.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.txtTag.SetValue('')
        self.txtmlName.Clear()
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        if doc is not None:
            if bNet==True:
                EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
                EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
                EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
                EVT_NET_XML_LOCK(doc,self.OnLock)
                EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
                EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
        self.txtmlName.SetDoc(doc)
        pass
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnSetNode(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)

    def SetNode(self,node):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,node,self)
        self.Clear()
        self.node=node
        self.doc.startEdit(node)
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.verbose:
            vtLog.CallStack('')
            print self.node
        self.txtTag.SetValue(self.doc.getNodeText(self.node,'tag'))
        self.txtmlName.SetNode(self.node,'name')
    def GetNode(self,node=None):
        if self.doc is None:
            return
        if node is None:
            if self.node is None:
                return
            node=self.node
        self.doc.setNodeText(node,'tag',self.txtTag.GetValue())
        self.txtmlName.GetNode(node)
        self.doc.AlignNode(node,iRec=2)
        self.doc.doEdit(node)

    def Lock(self,flag):
        if flag:
            self.txtTag.Enable(False)
            self.txtmlName.Enable(False)
        else:
            self.txtTag.Enable(True)
            self.txtmlName.Enable(True)
            

