#----------------------------------------------------------------------------
# Name:         vXmlCustomerInputTree.py
# Purpose:      input widget for human / user xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060218
# CVS-ID:       $Id: vXmlCustomerInputTree.py,v 1.4 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
#from vidarc.tool.input.vtInputTree import EVT_VTINPUT_TREE_CHANGED
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vCustomer.vXmlCustomerTree import vXmlCustomerTree

VERBOSE=0

# defined event for vtInputTextML item changed
wxEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED=wx.NewEventType()
vEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED=wx.PyEventBinder(wxEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED,1)
def EVT_VXMLCUSTOMER_INPUT_TREE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED,func)
class vXmlCustomerInputTreeChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VXMLCUSTOMER_INPUT_TREE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,node,fid,val,abbr):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED)
        self.node=node
        self.fid=fid
        self.val=val
        self.abbr=abbr
    def GetNode(self):
        return self.node
    def GetId(self):
        return self.fid
    def GetValue(self):
        return self.val
    def GetAbbr(self):
        return self.abbr

class vXmlCustomerInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='client'
        self.attrName='abbr'
        self.tagNameInt='name'
        self.tagNameIntAttr='abbreviation'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.appl='vCustomer'
    def SetTagNames(self,tagName,tagNameInt,attrName,tagNameIntAttr):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
        self.attrName=attrName
        self.tagNameInt=tagNameInt
        self.tagNameIntAttr=tagNameIntAttr
    def __markModifiedO__(self,flag=True):
        vtInputTree.__markModified__(self,flag)
        wx.PostEvent(self,vXmlCustomerInputTreeChanged(self,self.nodeSel,self.fid,
                self.txtVal.GetValue(),self.GetValAttr()))
    def __getSelNode__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__getSelNode__',
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.node is None or self.doc is None:
                self.fid=''
                return
            self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',self.appl)
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s'%(self.fid),self)
            if self.appl is not None and self.fid=='-3':
                self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',None)
                vtLog.vtLngCurWX(vtLog.WARN,'fallback id:%s'%self.fid,self)
            if VERBOSE:
                print self.fid
            self.nodeSel=None
            if self.docTreeTup is not None:
                docTree=self.docTreeTup[0]
                self.nodeSel=docTree.getNodeById(self.fid)
                if VERBOSE:
                    print self.nodeSel
            if self.nodeSel is not None:
                sVal=self.txtVal.GetValue()
                self.__apply__(self.nodeSel)
                if sVal!=self.txtVal.GetValue():
                    self.__markModified__(True)
                    wx.PostEvent(self,vXmlCustomerInputTreeChanged(self,self.nodeSel,self.fid,
                            self.txtVal.GetValue(),self.GetValAttr()))
            else:
                if self.node is not None:
                    vtLog.vtLngCurWX(vtLog.WARN,'%s;%s'%(self.fid,self.node),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def __apply__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;node:%s'%(node),
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack(node)
        try:
            self.fid=''
            self.nodeSel=node
            if self.nodeSel is not None:
                self.fid=self.docTreeTup[0].getKey(node)
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            if self.trShow is not None:
                sVal=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            else:
                sVal=''
            if self.bBlock==False:
                self.__markModified__(True)
                wx.PostEvent(self,vXmlCustomerInputTreeChanged(self,self.nodeSel,self.fid,
                            sVal,self.GetValAttr()))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(sVal.split('\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
    def OnTextText(self,evt):
        if self.bBlock:
            return
        self.nodeSel=None
        self.fid=''
        sVal=self.txtVal.GetValue()
        self.__markModified__()
        wx.PostEvent(self,vXmlCustomerInputTreeChanged(self,self.nodeSel,
                self.fid,sVal,''))

    def GetValAttr(self):
        docTree=self.docTreeTup[0]
        if docTree is None:
            return ''
        if self.nodeSel is not None:
            if self.tagNameIntAttr is not None:
                return docTree.getNodeText(self.nodeSel,self.tagNameIntAttr)
        return ''
    def __createTree__(*args,**kwargs):
        tr=vXmlCustomerTree(**kwargs)
        tr.SetValidInfo(['customer'])
        tr.SetBlockInfo(['cfg','address'])
        tr.EnableLanguageMenu(False)
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
    def __setNode__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__setNode__;node:%s'%(self.node),
        #                origin=self.GetName())
        try:
            docTree=self.docTreeTup[0]
            sValAttr=None
            if self.nodeSel is None:
                sVal=self.txtVal.GetValue()
            else:
                sVal=docTree.getNodeText(self.nodeSel,self.tagNameInt).split('\n')[0]
                if self.tagNameIntAttr is not None:
                    if self.attrName is not None:
                        sValAttr=docTree.getNodeText(self.nodeSel,self.tagNameIntAttr)
            self.doc.setNodeText(node,self.tagName,sVal)
            tmp=self.doc.getChild(node,self.tagName)
            if tmp is not None:
                if len(self.fid)==0:
                    self.doc.removeAttribute(tmp,'fid')
                else:
                    self.doc.setForeignKey(tmp,attr='fid',attrVal=self.fid,appl=self.appl)
                #if sValAttr is not None:
                #    self.doc.setAttribute(tmp,self.attrName,sValAttr)
        except:
            vtLog.vtLngTB(self.GetName())
