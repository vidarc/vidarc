#Boa:FramePanel:vCustomerPanel
#----------------------------------------------------------------------------
# Name:         vCustomerPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vCustomerPanel.py,v 1.5 2007/08/21 18:16:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors

import vidarc.vApps.vHum.impMin

from vidarc.tool.xml.vtXmlTree import *
from vidarc.vApps.vLoc.vLocCountries import COUNTRIES
from vidarc.vApps.vGlobals.vNetGlobals import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

VERBOSE=0

[wxID_VCUSTOMERPANEL, wxID_VCUSTOMERPANELCHCCOUNTRY, 
 wxID_VCUSTOMERPANELLBLCOUNTRY, wxID_VCUSTOMERPANELLBLDIST, 
 wxID_VCUSTOMERPANELLBLDISTUNIT, wxID_VCUSTOMERPANELLBLLOC, 
 wxID_VCUSTOMERPANELLBLREGION, wxID_VCUSTOMERPANELSPNDIST, 
 wxID_VCUSTOMERPANELTXTLOC, wxID_VCUSTOMERPANELTXTREGION, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vCustomerPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCUSTOMERPANEL, name=u'vCustomerPanel',
              parent=prnt, pos=wx.Point(323, 286), size=wx.Size(240, 146),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(232, 119))
        self.SetAutoLayout(True)

        self.lblLoc = wx.StaticText(id=wxID_VCUSTOMERPANELLBLLOC,
              label=_(u'Location'), name=u'lblLoc', parent=self, pos=wx.Point(8,
              8), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

        self.txtLoc = wx.TextCtrl(id=wxID_VCUSTOMERPANELTXTLOC, name=u'txtLoc',
              parent=self, pos=wx.Point(96, 4), size=wx.Size(128, 21), style=0,
              value=u'')
        self.txtLoc.SetConstraints(LayoutAnchors(self.txtLoc, True, True, True,
              False))

        self.chcCountry = wx.Choice(choices=[],
              id=wxID_VCUSTOMERPANELCHCCOUNTRY, name=u'chcCountry', parent=self,
              pos=wx.Point(96, 32), size=wx.Size(130, 21), style=0)
        self.chcCountry.SetConstraints(LayoutAnchors(self.chcCountry, True,
              True, True, False))
        self.chcCountry.Bind(wx.EVT_CHOICE, self.OnChcCountryChoice,
              id=wxID_VCUSTOMERPANELCHCCOUNTRY)

        self.txtRegion = wx.TextCtrl(id=wxID_VCUSTOMERPANELTXTREGION,
              name=u'txtRegion', parent=self, pos=wx.Point(96, 60),
              size=wx.Size(128, 21), style=0, value=u'')
        self.txtRegion.SetConstraints(LayoutAnchors(self.txtRegion, True, True,
              True, False))

        self.spnDist = wx.SpinCtrl(id=wxID_VCUSTOMERPANELSPNDIST, initial=0,
              max=40000, min=0, name=u'spnDist', parent=self, pos=wx.Point(96,
              88), size=wx.Size(108, 21), style=wx.SP_ARROW_KEYS)
        self.spnDist.SetConstraints(LayoutAnchors(self.spnDist, True, True,
              True, False))

        self.lblDistUnit = wx.StaticText(id=wxID_VCUSTOMERPANELLBLDISTUNIT,
              label=u'km', name=u'lblDistUnit', parent=self, pos=wx.Point(208,
              92), size=wx.Size(14, 13), style=0)
        self.lblDistUnit.SetConstraints(LayoutAnchors(self.lblDistUnit, False,
              True, True, False))

        self.lblCountry = wx.StaticText(id=wxID_VCUSTOMERPANELLBLCOUNTRY,
              label=_(u'Country'), name=u'lblCountry', parent=self,
              pos=wx.Point(8, 36), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

        self.lblRegion = wx.StaticText(id=wxID_VCUSTOMERPANELLBLREGION,
              label=_(u'Region'), name=u'lblRegion', parent=self,
              pos=wx.Point(8, 64), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

        self.lblDist = wx.StaticText(id=wxID_VCUSTOMERPANELLBLDIST,
              label=_(u'Distance'), name=u'lblDist', parent=self,
              pos=wx.Point(8, 92), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        self._init_ctrls(parent)
        self.node=None
        self.doc=None
        self.bModified=False
        self.verbose=VERBOSE
        
        self.lstCountries=[]
        for k in COUNTRIES.keys():
            self.lstCountries.append((COUNTRIES[k],k))
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        self.lstCountries.sort(cmpFunc)
        
        for it in self.lstCountries:
            self.chcCountry.Append(it[0])
            
        self.Move(pos)
        self.SetSize(size)

    def OnChcCountryChoice(self, event):
        event.Skip()
        
    def SetModified(self,state):
        self.bModified=state    
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        if self.verbose:
            vtLog.CallStack('')
        self.SetModified(False)
        self.node=None
        self.txtLoc.SetValue('')
        self.chcCountry.SetSelection(-1)
        self.txtRegion.SetValue('')
        self.spnDist.SetValue(0)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc,bNet=False):
        if doc is None:
            return
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)

        pass
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)

    def SetNode(self,node):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'SetNode',origin='vLocPanel')
        self.Clear()
        self.node=node
        if self.verbose:
            vtLog.CallStack('')
            print self.node
        self.txtLoc.SetValue(self.doc.getNodeText(self.node,'name'))
        try:
            sCountry=self.doc.getNodeText(self.node,'country')
            self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
        except:
            pass
        self.txtRegion.SetValue(self.doc.getNodeText(self.node,'region'))
        try:
            iDist=long(self.doc.getNodeText(self.node,'distance'))
        except:
            iDist=0
        self.spnDist.SetValue(iDist)
        pass
    def GetNode(self,node=None):
        if self.doc is None:
            return
        if node is None:
            if self.node is None:
                return
            node=self.node
        self.doc.setNodeText(node,'name',self.txtLoc.GetValue())
        try:
            i=self.chcCountry.GetSelection()
            sCountry=self.lstCountries[i][1]
        except:
            sCountry='ch'
        self.doc.setNodeText(node,'country',sCountry)
        self.doc.setNodeText(node,'region',self.txtRegion.GetValue())
        self.doc.setNodeText(node,'distance',str(self.spnDist.GetValue()))
        self.doc.AlignNode(node,iRec=2)
        pass

    def Lock(self,flag):
        if flag:
            self.txtLoc.Enable(False)
            self.chcCountry.Enable(False)
            self.txtRegion.Enable(False)
            self.spnDist.Enable(False)
        else:
            self.txtLoc.Enable(True)
            self.chcCountry.Enable(True)
            self.txtRegion.Enable(True)
            self.spnDist.Enable(True)
            
