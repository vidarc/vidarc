#Boa:Frame:vCustomerMainFrame
#----------------------------------------------------------------------------
# Name:         vCustomerMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vCustomerMainFrame.py,v 1.10 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
from wxPython.wx import *
import  wx.lib.anchors as anchors
import getpass,time

import vidarc.vApps.vHum.impMin

import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.vApps.vCustomer.vXmlCustomerTree  import *
from vidarc.vApps.vCustomer.vNetCustomer import *
from vidarc.vApps.vCustomer.vCustomerListBook import *
from vidarc.vApps.vGlobals.vNetGlobals import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
from vidarc.tool.log.vtLogFileViewerFrame import *
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vHum.vNetHum import *


import images
import images_lang

VERBOSE=0

def create(parent):
    return vCustomerMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wxNewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wxNewId(), range(4))

[wxID_VCUSTOMERMAINFRAME, wxID_VCUSTOMERMAINFRAMECBAPPLY, 
 wxID_VCUSTOMERMAINFRAMECBCANCEL, wxID_VCUSTOMERMAINFRAMEPNDATA, 
 wxID_VCUSTOMERMAINFRAMESBSTATUS, wxID_VCUSTOMERMAINFRAMESLWNAV, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VCUSTOMERMAINFRAMEMNFILEITEM_EXIT, 
 wxID_VCUSTOMERMAINFRAMEMNFILEITEM_SETTING, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(2)]

[wxID_VCUSTOMERMAINFRAMEMNIMPORTITEM_IMPORT_CSV, 
 wxID_VCUSTOMERMAINFRAMEMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VCUSTOMERMAINFRAMEMNEXPORTITEM_EXPORT_CSV, 
 wxID_VCUSTOMERMAINFRAMEMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VCUSTOMERMAINFRAMEMNTOOLSMN_TOOL_REG_LOCK] = [wx.NewId() for _init_coll_mnTools_Items in range(1)]

[wxID_VCUSTOMERMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VCUSTOMERMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(2)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VCUSTOMERMAINFRAMEMNVIEWITEM_LANG, 
 wxID_VCUSTOMERMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VCUSTOMERMAINFRAMEMNLANGITEM_LANG_DE, 
 wxID_VCUSTOMERMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vCustomerMainFrame(wxFrame,vtXmlDomConsumer.vtXmlDomConsumer,vMainFrame,vStatusBarMessageLine):
    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help=u'', id=wxID_VCUSTOMERMAINFRAMEMNFILEITEM_SETTING,
              kind=wx.ITEM_NORMAL, text=_(u'Settings'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VCUSTOMERMAINFRAMEMNFILEITEM_EXIT,
              kind=wxITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNFILEITEM_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_settingMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNFILEITEM_SETTING)

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_LOG)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNHELPMN_HELP_HELP)

    def _init_coll_mnTools_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VCUSTOMERMAINFRAMEMNTOOLSMN_TOOL_REG_LOCK,
              kind=wx.ITEM_NORMAL, text=_(u'Request Lock\tF4'))
        self.Bind(wx.EVT_MENU, self.OnMnToolsMn_tool_reg_lockMenu,
              id=wxID_VCUSTOMERMAINFRAMEMNTOOLSMN_TOOL_REG_LOCK)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'&View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Anal&yse'))
        parent.Append(menu=self.mnTools, title=_(u'&Tools'))
        parent.Append(menu=self.mnHelp, title=_(u'&?'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wxMenuBar()

        self.mnFile = wxMenu(title=u'')

        self.mnAnalyse = wxMenu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title='')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnTools_Items(self.mnTools)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_VCUSTOMERMAINFRAME,
              name=u'vCustomerMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(711, 546), style=wxDEFAULT_FRAME_STYLE,
              title=u'VIDARC Customer')
        self._init_utils()
        self.SetClientSize(wx.Size(703, 519))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VCUSTOMERMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VCUSTOMERMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              480), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VCUSTOMERMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VCUSTOMERMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(200, 0), size=wx.Size(504, 480),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCUSTOMERMAINFRAMECBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self.pnData, pos=wx.Point(398, 8), size=wx.Size(100, 30),
              style=0)
        self.cbApply.SetConstraints(LayoutAnchors(self.cbApply, False, True,
              True, False))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VCUSTOMERMAINFRAMECBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCUSTOMERMAINFRAMECBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self.pnData, pos=wx.Point(398, 48),
              size=wx.Size(100, 30), style=0)
        self.cbCancel.SetConstraints(LayoutAnchors(self.cbCancel, False, True,
              True, False))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VCUSTOMERMAINFRAMECBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        vMainFrame.__init__(self)
        vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,-1)#5000)
        self.sbStatus.Bind(wx.EVT_RIGHT_UP, self.OnSbMainRightUp)
        self.zMsgLast=time.clock()
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        appls=['vCustomer','vHum','vGlobals']
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netCustomer=self.netMaster.AddNetControl(vNetCustomer,'vCustomer',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        #self.netCustomer.SetLanguages([
        #    ('english','en',images_lang.getLangEnBitmap()),
        #    ('german','de',images_lang.getLangDeBitmap()),
        #    ])
        #self.netCustomer.SetLang('en')
        #self.netCustomer.SetDftLanguages()
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,self.netCustomer)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vCustomerAppl')
        #EVT_NET_XML_SETUP_CHANGED(self.netCustomer,self.OnSetupChanged)
        #self.netCustomer.SetSetupNode(None,['settings','server'])
        #EVT_NET_XML_OPEN_START(self.netCustomer,self.OnOpenStart)
        EVT_NET_XML_OPEN_OK(self.netCustomer,self.OnOpenOk)
        #EVT_NET_XML_OPEN_FAULT(self.netCustomer,self.OnOpenFault)
        
        #EVT_NET_XML_SYNCH_START(self.netCustomer,self.OnSynchStart)
        EVT_NET_XML_SYNCH_PROC(self.netCustomer,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netCustomer,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netCustomer,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netCustomer,self.OnGotContent)
        
        #self.bAutoConnect=True
        #self.OpenCfgFile()
        
        self.vgpTree=vXmlCustomerTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(390,482),style=0,name="vgpTree",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netCustomer,True)
        #self.vgpTree.SetLanguages(self.languages,self.languageIds)
        #self.vgpTree.SetSkipInfo(['cfg','area'])
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        
        self.nbData=vCustomerListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(382, 462),style=0,name="nbCustomer")
        self.nbData.SetDoc(self.netCustomer,True)
        self.nbData.SetNetDocHuman(self.netHum,True)
        self.nbData.SetConstraints(LayoutAnchors(self.nbData, True, True, True,
              True))
        #EVT_VGTRXMLDOC_GROUPING_CHANGED(self.vgpTree,self.OnDocGrpChanged)
        
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        #mnItems=self.mnViewTree.GetMenuItems()
        #mnItems[0].Check(True)
        
        self.actLanguage='en'
        #self.vgpTree.SetLanguage(self.actLanguage)
        #self.CreateNew()
        
        self.__makeTitle__()
    def __addLangMenu__(self):
        self.mnView.Remove(wxID_VCUSTOMERMAINFRAMEMNVIEWITEM_LANG)
        
        self.mnLang=wx.Menu()
        self.langIds=[]
        self.languages=[u'English',u'Deutsch']
        self.languageIds=[u'en',u'de']
        for i in range(0,len(self.languages)):
            id=wx.NewId()
            self.langIds.append(id)
            item = wx.MenuItem(self.mnView,help='', id=id,
                kind=wx.ITEM_NORMAL, text=self.languages[i])
            f=getattr(images_lang, 'getLang%02dBitmap' % i)
            bmp=f()
            item.SetBitmap(bmp)
            self.mnLang.AppendItem(item)
            self.Bind(wx.EVT_MENU, self.OnLanguage, id=id)
        
        self.mnView.AppendMenu(help='', id=wxID_VCUSTOMERMAINFRAMEMNVIEWITEM_LANG,
              submenu=self.mnLang, text=u'Language ...')
        self.actLanguage='en'
        
        
    def OnLanguage(self,event):
        if event.GetId()==self.langIds[0]:
            self.actLanguage='en'
        elif event.GetId()==self.langIds[1]:
            self.actLanguage='de'
        else:
            self.actLanguage='en'
        self.vgpTree.SetLanguage(self.actLanguage)
        
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            tup=self.GetPrintMsgTup()
            if tup is not None:
                bIsShown=self.IsShownPopupStatusBar()
                self.zMsgLast=time.clock()
                while tup is not None:
                    if bIsShown:
                        self.AddMsg2PopupStatusBar(tup,0)
                    self.sb.SetStatusText(tup[0], self.iNum)
                    self.rbMsg.put(tup)
                    tup=self.GetPrintMsgTup()
            else:
                if (time.clock()-self.zMsgLast)>5:
                    self.sb.SetStatusText('', self.iNum)
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC Customer")
        fn=self.netCustomer.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())

        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def Clear(self):
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        self.SetNode(node)
        if event.GetTreeNodeData() is not None:
            self.netCustomer.startEdit(node)
            #self.vglbDocGrpInfo.SetNode(node)
        else:
            # grouping tree item selected
            #self.vglbDocGrpInfo.SetNode(None)
            pass
        self.nbData.SetNode(node)
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('connection login. '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netCustomer.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        #cfgNode=self.netCustomer.getChild(self.netCustomer.getRoot(),'config')
        #self.netHum.SetCfgNode(cfgNode)
        #self.netHum.AutoConnect2Srv()
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnLock(self,evt):
        if evt.GetResponse()=='ok':
            try:
                self.nodeLock=self.netCustomer.getNodeById(evt.GetID())
            except:
                self.nodeLock=None
        else:
            self.nodeLock=None
        evt.Skip()
    def OnUnLock(self,evt):
        if evt.GetResponse()=='ok':
            self.nodeLock=None
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netCustomer.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netCustomer.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netCustomer.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netCustomer.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netCustomer.getNodeText(self.baseSettings,'projectdn')
        return
        # get tree view settings
        sTreeViewGrp=self.netCustomer.getNodeText(self.baseSettings,'treeviewgrp')
        
        mnItems=self.mnViewTree.GetMenuItems()
        
        #self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netCustomer.getBaseNode()
        self.baseSettings=self.netCustomer.getChildForced(self.netCustomer.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netCustomer.createSubNode(self.netCustomer.getRoot(),'settings')
        #    self.netCustomer.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netCustomer.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netCustomer.ClearAutoFN()>0:
            self.netCustomer.Open(fn)
            
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
        #if self.bAutoConnect:
        #    self.netCustomer.Connect2SrvByNode(None,None)
        #    self.netHum.Connect2SrvByNode(None,None)
        
            #self.vgpTree.SetNode(None)
        #self.docs=vtXmlDomTree.SortedNodeIds()
        #self.docs.SetNode(self.baseDoc,'docgroup','id',
        #                [('key','%s'),('title','%s')])
        
            self.PrintMsg(_('open file finished. '))
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def SaveFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        sCurFN=self.netCustomer.GetFN()
        if (sCurFN is None) and (fn is None):
            self.OnMnFileItem_saveasMenu(None)
            self.__setModified__(False)
            return
        self.PrintMsg(_('save file ... '))
        self.netCustomer.Save(fn)
        self.PrintMsg(_('save file finished.'))
        
        self.__setModified__(False)
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netCustomer.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
        self.__setModified__(True)
    def OnMnFileItems_openMenu(self, event):
        if self.netCustomer.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaDoc',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxOPEN)
        try:
            fn=self.netCustomer.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()


    def OnMnFileItem_saveMenu(self, event):
        self.SaveFile()
        event.Skip()

    def OnMnFileItem_saveasMenu(self, event):
        if self.netCustomer.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vCustomerAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlg = wxFileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxSAVE)
        try:
            fn=self.netCustomer.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                #self.txtEditor.SaveFile(filename)
                self.SaveFile(filename)
        finally:
            dlg.Destroy()

    def OnMnFileItem_newMenu(self, event):
        if self.netCustomer.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vCustomerAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        self.PrintMsg(_('new ... '))
        self.CreateNew()
        self.PrintMsg(_('new finished.'))

        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netCustomer.getChild(self.netCustomer.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vCustomerSettingsDialog(self)
            dlg.SetXml(self.netCustomer,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netCustomer,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnMnImportItem_import_xmlMenu(self, event):
        #'open'
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if self.humanFN is not None:
                dlg.SetDirectory(self.humanFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.SetFileHuman(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def __validEmptyTextNode__(self,txt):
        if len(string.strip(txt))==0:
            return '---'
        else:
            return txt
    def OnMnImportItem_import_cvsMenu(self, event):
        #'open'
        dlg = wxFileDialog(self, _("Open"), ".", "", _("CSV files (*.csv)|*.csv|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.PrintMsg(_('import csv file ... '))
                # do import
                
                # update
                self.PrintMsg(_('generate ... '))

                self.__setModified__(True)
                self.vgpTree.SetNode(None)
                self.gProcess.SetValue(0)
                self.PrintMsg(_('import csv file finished.'))
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnExportItem_export_xmlMenu(self, event):
        event.Skip()

    def OnMnExportItem_export_csvMenu(self, event):
        event.Skip()
            
    def OnMnToolsItem_alignMenu(self, event):
        self.PrintMsg(_('align ... '))
        self.netCustomer.AlignDoc(gProcess=self.gProcess)
        self.PrintMsg(_('align finished.'))
        self.gProcess.SetValue(0)
        self.__setModified__(True)
        event.Skip()

    def __EnsureOneCheckMenuItem__(self,mnItems,iAct,iStart,iCount):
        if mnItems[iAct].IsChecked():
            # uncheck all other
            for i in range(iStart,iStart+iCount):
                if i!=iAct:
                    mnItems[i].Check(False)
            return False
        else:
            bFound=False
            for i in range(iStart,iCount):
                if mnItems[i].IsChecked()==True:
                    bFound=True
            if bFound==False:
                mnItems[iAct].Check(True)
                return True
            return False
    def OnMnViewTreeItem_normal_grpMenu(self, event):
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,0,0,2):
            return
        self.vgpTree.SetNormalGrouping()
        
        self.netCustomer.setNodeText(self.baseSettings,'treeviewgrp','normal')
        self.netCustomer.AlignNode(self.baseSettings,iRec=1)
        self.__setModified__(True)
        self.PrintMsg(_('generate ... '))
        self.vgpTree.SetNode(None)
        self.PrintMsg(_('generate finished.'))

        event.Skip()
    def OnMnViewTreeItem_iddocgrp_grpMenu(self, event):
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,1,0,2):
            return
        self.vgpTree.SetIdGrouping()
        
        self.netCustomer.setNodeText(self.baseSettings,'treeviewgrp','iddocgrp')
        self.netCustomer.AlignNode(self.baseSettings,iRec=1)
        self.__setModified__(True)
        self.PrintMsg(_('generate ... '))
        self.vgpTree.SetNode(None)
        self.PrintMsg(_('generate finished.'))
        event.Skip()
    def OnDocGrpChanged(self,event):
        iGrp=event.GetGrouping()
        mnItems=self.mnViewTree.GetMenuItems()
        if iGrp==0:
            mnItems[0].Check(True)
            mnItems[1].Check(False)
            self.netCustomer.setNodeText(self.baseSettings,'treeviewgrp','normal')
            self.netCustomer.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        elif iGrp==1:
            mnItems[0].Check(False)
            mnItems[1].Check(True)
            self.netCustomer.setNodeText(self.baseSettings,'treeviewgrp','iddocgrp')
            self.netCustomer.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        
    def OnFrameClose(self, event):
        if self.netMaster.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vCustomerAppl'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            dlg.Centre()
            if dlg.ShowModal()==wx.ID_OK:
                self.SaveFile()
                pass
        self.netMaster.ShutDown()
        
        event.Skip()

    def OnMnViewTreeItem_date_short_usr_grpMenu(self, event):
        event.Skip()

    def OnMnFileItem_open_cfgMenu(self, event):
        if self.netCustomer.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vCustomerAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxOPEN)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.OpenCfgFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_save_as_cfgMenu(self, event):
        if self.netCustomer.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vCustomerAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlg = wxFileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxSAVE)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.xdCfg.Save(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()

    def __setCfg__(self):
        node=self.xdCfg.getChildForced(None,'size')
        iX=self.xdCfg.GetValue(node,'x',int,20)
        iY=self.xdCfg.GetValue(node,'y',int,20)
        iWidth=self.xdCfg.GetValue(node,'width',int,800)
        iHeight=self.xdCfg.GetValue(node,'height',int,600)
        iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
        self.Move((iX,iY))
        self.SetSize((iWidth,iHeight))
        
        node=self.xdCfg.getChildForced(None,'layout')
        
        i=self.xdCfg.GetValue(node,'nav_sash',int,200)
        self.slwNav.SetDefaultSize((i, 1000))
        
        #i=self.xdCfg.GetValue(node,'top_sash',int,100)
        #self.slwTop.SetDefaultSize((1000, i))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.xdCfg.AlignDoc()
        self.xdCfg.Save()
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iWidth=event.GetDragRect().width
        #if iWidth>80:
        #    iWidth=80
        self.xdCfg.SetValue(node,'nav_sash',iWidth)
        self.xdCfg.Save()
        
        self.slwNav.SetDefaultSize((iWidth, 1000))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        #event.Skip()

    def OnSlwTopSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iHeight=event.GetDragRect().height
        #if iHeight>80:
        #    iHeight=80
        self.xdCfg.SetValue(node,'top_sash',iHeight)
        self.xdCfg.Save()
        
        self.slwTop.SetDefaultSize((1000, iHeight))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        #event.Skip()

    def OnFrameSize(self, event):
        bMod=False
        sz=event.GetSize()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'width',sz[0])
        self.xdCfg.SetValue(node,'height',sz[1])
        self.xdCfg.Save()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        return
        iWidth=event.GetSize()[0]
        if self.slwNav.GetSize()[0]>iWidth:
            iWidth=self.slwNav.GetSize()[0]
            bMod=True
        iHeightAdd=self.sbStatus.GetSize()[1]#*4
        iHeightAdd+=wx.SystemSettings.GetMetric(wx.SYS_CAPTION_Y)*2
        iHeightAdd+=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)*2
        iHeight=event.GetSize()[1]
        if self.slwTop.GetSize()[1]+iHeightAdd>iHeight:
            iHeight=self.slwTop.GetSize()[1]+iHeightAdd
            bMod=True
        if bMod:
            self.SetSize((iWidth,iHeight))
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'width',iWidth)
        self.xdCfg.SetValue(node,'height',iHeight)
        self.xdCfg.Save()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        #event.Skip()

    def OnFrameMove(self, event):
        pos=self.GetPosition()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'x',pos[0])
        self.xdCfg.SetValue(node,'y',pos[1])
        self.xdCfg.Save()
        event.Skip()

    def OnCbApplyButton(self, event):
        node=self.vgpTree.GetSelected()
        self.nbData.GetNode()
        self.netCustomer.doEdit(node)
        event.Skip()

    def OnCbCancelButton(self, event):
        node=self.vgpTree.GetSelected()
        self.nbData.SetNode(node)
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        event.Skip()

    def OnMnToolsMn_tool_reg_lockMenu(self, event):
        node=self.vgpTree.GetSelected()
        self.netCustomer.reqLock(node)
        event.Skip()
    def OnSbMainRightUp(self, event):
        event.Skip()
        try:
            if self.ShowPopupStatusBar():
                self.AddMsg2PopupStatusBar((None,None))
                self.rbMsg.proc(self.AddMsg2PopupStatusBar)
        except:
            vtLog.vtLngTB(self.GetName())
