#----------------------------------------------------------------------------
# Name:         vXmlNodeCustomer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060101
# CVS-ID:       $Id: vXmlNodeCustomer.py,v 1.7 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *
import vidarc.tool.lang.vtLgBase as vtLgBase
#from vidarc.vApps.vLoc.vXmlLoc import COUNTRIES
try:
    from vidarc.vApps.vCustomer.vXmlNodeCustomerPanel import *
    from vidarc.vApps.vCustomer.vXmlNodeCustomerEditDialog import *
    from vidarc.vApps.vCustomer.vXmlNodeCustomerAddDialog import *
    GUI=1
except:
    GUI=0


class vXmlNodeCustomer(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('Abbreviation',None,'abbreviation',None),
            ('Number',None,'number',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
        ]
    def __init__(self,tagName='customer'):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'customer')
    # ---------------------------------------------------------
    # specific
    def GetName(self,node):
        return self.Get(node,'name')
    def GetAbbreviation(self,node):
        return self.Get(node,'abbreviation')
    def GetNumber(self,node):
        return self.Get(node,'number')
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def SetNumber(self,node,val):
        self.Set(node,'number',val)
    def SetAbbreviation(self,node,val):
        self.Set(node,'abbreviation',val)
    # ---------------------------------------------------------
    # inheritance
    def GetAttrFilterTypes(self):
        return [('name',vtXmlFilterType.FILTER_TYPE_STRING)]
    def IsSkip(self):
        return False
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01&IDAT(\x91}P=S\x83@\x10]\x1cJ\xc0\xd6\xa3u\x1ck\x89\xd6\xd2;\x93\x1f\xa0\
\xa5q\xe8\x83}\x8e\x9fq9)-l3cM\x92&\x05\xe1\xb0T\xb0\x15\xd2\xc2Q:\x83\x05\
\x8c\x1c\x1fa\xab\xbdw\xfbv\xdf{\xd2\xcb\xb2\x84\xa1\xfa\x8a6\xfe\xfe\xd9\
\xf3\x96"X\xa4\x89\x0c\x00;?\xec\x13\\j\x96|\x0b\x1fL\x04c\x7fw2\xb8>d\x04c\
\x0b\x0ei\xff\xabEp\xa9q\xaa\xa9\x00\xc0\x02\xb2\xb0\x1f\xa0\xe0\x0eY9\xaf\
\xdeQ\x02\x00h\xaa\xd2]\xaf(\xe2\x80,>\xe6v\x9c\xf3"M\x03\xd3\xb4\xa0\xe0\
\x00\xb0\xb0\xa6p\x86Dm\xad\x0bY\xce\xb3\x9c\xa7\xc9\xfe\xf6Ro\xd0\xb6\x93a\
\xd3#%\x0f\xa2\x12\xba\xab\x9a2~\xab\xb4\x8d\x11\x1e\x9f\xea\xec]j\x80\xa2t\
\x08\x8d$\x97\x1a!#}\xbeCV\xd2\xd5\x0c\x14\xb5{an\xc7, .5:\xda0\xb6J\xbe\xfd\
\xb7\xde\x10\xb2\x9c\x9f_\xdc\x1b\x13KS\xeb\xe01F\xf5\xe8wt\xd4C\x95l\xd5#\
\xfdz\xf3\xbenE<\x1e+B\x93\xf5\xe7O\x07\x94\x93\xe8\xc0(\x1b$\x00\xdc\xe0`6\
\xd5\x7fE\xe8\x0f\xd2B\x7f\n\x9b\xaf\xe8\x9f\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeCustomerEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeCustomerAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeCustomerPanel
        else:
            return None

