#----------------------------------------------------------------------------
# Name:         vXmlCustomerTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vXmlCustomerTree.py,v 1.7 2006/06/07 13:36:21 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vCustomer.vNetCustomer import *
from vidarc.tool.loc.vLocCountries import COUNTRIES
import vidarc.vApps.vCustomer.images as images#Customer

class vXmlCustomerTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.iGrp==0
        self.grpMenu=[
                ('normal',_(u'normal'),
                    [],
                    [('name','')]
                ),
                ('address:country',_(u'country'),
                    {'customer':[('address:country','')]},
                    [('name','')]
                ),
                ('address:region',_(u'region'),
                    {'customer':[('address:country',''),('address:region','')]},
                    [('name','')]
                )]
    def SetDftGrouping(self):
        self.grouping=[]
        self.bGrouping=False
        self.label=[('name','')]
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['name','|id','address:country','address:region'])
    def SetGroupingByNameOld(self,sGrp):
        if sGrp=='normal':
            return self.SetNormalGrouping()
        elif sGrp=='address:country':
            return self.SetCountryGrouping()
        elif sGrp=='address:region':
            return self.SetRegionGrouping()
        return False
    def SetNormalGrouping(self):
        if self.iGrp==0:
            return False
        self.iGrp=0
        self.SetGrouping([],[('name','')])
        return True
    def SetCountryGrouping(self):
        if self.iGrp==1:
            return False
        self.iGrp=1
        self.SetGrouping({'customer':[('address:country','')]},[('name','')])
        return True
    def SetRegionGrouping(self):
        if self.iGrp==2:
            return False
        self.iGrp=2
        self.SetGrouping({'customer':[('address:country',''),('address:region','')]},[('name','')])
        return True
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            #self.SetImageList(self.imgLstTyp)
            #return
            img=images.getPluginBitmap()
            self.imgDict['elem']['customers']=[self.imgLstTyp.Add(img)]
            img=images.getPluginBitmap()
            self.imgDict['elem']['customers'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getCustomersImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
            self.imgDict['elem']['customers'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getCustomersSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['customers'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getCustomersImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[4])
            self.imgDict['elem']['customers'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getCustomersSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[5])
            self.imgDict['elem']['customers'].append(self.imgLstTyp.Add(img))
            
            self.__addElemImage2ImageList__('customer',
                            images.getCustomerImage(),
                            images.getCustomerImage(),True)
                            
            imgTmp=images.getCountryBitmap()
            imgGrp,imgGrpSel=self.getGroupingOverlayImages()
            img=self.__buildGrpBmp__(imgTmp,imgGrp)
            self.imgDict['grp']['address:country']=[self.imgLstTyp.Add(img)]
            imgTmp=images.getCountrySelBitmap()
            img=self.__buildGrpBmp__(imgTmp,imgGrpSel)
            self.imgDict['grp']['address:country'].append(self.imgLstTyp.Add(img))
            
            imgTmp=images.getRegionBitmap()
            imgGrp,imgGrpSel=self.getGroupingOverlayImages()
            img=self.__buildGrpBmp__(imgTmp,imgGrp)
            self.imgDict['grp']['address:region']=[self.imgLstTyp.Add(img)]
            imgTmp=images.getRegionSelBitmap()
            img=self.__buildGrpBmp__(imgTmp,imgGrpSel)
            self.imgDict['grp']['address:region'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getCountryImage()
            
            self.SetImageList(self.imgLstTyp)
    def __getValFormat__(self,g,val,infos=None):
        try:
            if val is None:
                return '---'
            val=string.strip(val)
            if g[0]=='country':
                return COUNTRIES[val]
        except:
            pass
        return vtXmlGrpTree.__getValFormat__(self,g,val,infos)
        
    def __getImgFormat__(self,g,val):
        #vtLog.CallStack(val)
        #vtLog.pprint(g)
        try:
            imgInst=-1
            imgInstSel=-1
            imgInvalid=-1
            imgInvalidSel=-1
            
            val=string.strip(val)
            if len(g[1])>0:
                sFormat=g[1]
                if g[0]=='date':
                    if sFormat=='YYYY':
                        img=self.imgDict['grp']['YYYY'][0]
                        imgSel=self.imgDict['grp']['YYYY'][0]
                        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                    elif sFormat=='MM':
                        img=self.imgDict['grp']['MM'][0]
                        imgSel=self.imgDict['grp']['MM'][0]
                        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                    elif sFormat=='DD':
                        img=self.imgDict['grp']['DD'][0]
                        imgSel=self.imgDict['grp']['DD'][0]
                        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
        except:
            pass
        try:
            #img=self.imgDict['grp'][g[0]][0]
            #imgSel=self.imgDict['grp'][g[0]][1]
            return self.imgDict['grp'][g[0]]
        except:
            pass
        if g[0]=='attributes:clientshort':
            #if sFormat=='':
            #img=self.imgDict['grp']['attributes:clientshort'][0]
            #imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return self.imgDict['grp']['attributes:clientshort'][0]
        return vtXmlGrpTree.__getImgFormat__(self,g,val)
