#Boa:FramePanel:vXmlNodeCustomerPanel

import wx
import vidarc.tool.input.vtInputAttrPanel
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTree
import wx.lib.buttons
import wx.lib.masked.textctrl
from wx.lib.anchors import LayoutAnchors
from gettext import *
from vidarc.tool.xml.vtXmlTree import *
from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import images

VERBOSE=0

[wxID_VXMLNODECUSTOMERPANEL, wxID_VXMLNODECUSTOMERPANELCBGEN, 
 wxID_VXMLNODECUSTOMERPANELLBLAREA, wxID_VXMLNODECUSTOMERPANELLBLCUSTNR, 
 wxID_VXMLNODECUSTOMERPANELLBLNAME, wxID_VXMLNODECUSTOMERPANELLBLSHORT, 
 wxID_VXMLNODECUSTOMERPANELTXTABBR, wxID_VXMLNODECUSTOMERPANELTXTCUSTOMNR, 
 wxID_VXMLNODECUSTOMERPANELTXTNAME, wxID_VXMLNODECUSTOMERPANELVIPATTR, 
 wxID_VXMLNODECUSTOMERPANELVITRAREA, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vXmlNodeCustomerPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtName, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblShort, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtAbbr, (0, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblArea, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.vitrArea, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 3))
        parent.AddWindow(self.lblCustNr, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtCustomNr, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.cbGen, (2, 3), border=0, flag=0, span=(1, 1))
        parent.AddSpacer(wx.Size(8, 8), (3, 0), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.vipAttr, (4, 0), border=0, flag=wx.EXPAND,
              span=(1, 4))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECUSTOMERPANEL,
              name=u'vXmlNodeCustomerPanel', parent=prnt, pos=wx.Point(250,
              223), size=wx.Size(433, 311), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(425, 284))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODECUSTOMERPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(0,
              0), size=wx.Size(96, 21), style=wx.ALIGN_RIGHT)

        self.txtName = wx.TextCtrl(id=wxID_VXMLNODECUSTOMERPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(100, 0),
              size=wx.Size(103, 21), style=0, value=u'')

        self.txtAbbr = wx.TextCtrl(id=wxID_VXMLNODECUSTOMERPANELTXTABBR,
              name=u'txtAbbr', parent=self, pos=wx.Point(314, 0),
              size=wx.Size(103, 21), style=0, value=u'')

        self.lblShort = wx.StaticText(id=wxID_VXMLNODECUSTOMERPANELLBLSHORT,
              label=_(u'abbreviation'), name=u'lblShort', parent=self,
              pos=wx.Point(207, 0), size=wx.Size(103, 21),
              style=wx.ALIGN_RIGHT)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODECUSTOMERPANELLBLAREA,
              label=_(u'area'), name=u'lblArea', parent=self, pos=wx.Point(0, 25),
              size=wx.Size(96, 30), style=wx.ALIGN_RIGHT)

        self.vitrArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODECUSTOMERPANELVITRAREA,
              name=u'vitrArea', parent=self, pos=wx.Point(100, 25),
              size=wx.Size(317, 30), style=0)

        self.lblCustNr = wx.StaticText(id=wxID_VXMLNODECUSTOMERPANELLBLCUSTNR,
              label=_(u'number'), name=u'lblCustNr', parent=self,
              pos=wx.Point(0, 59), size=wx.Size(96, 30), style=wx.ALIGN_RIGHT)

        self.txtCustomNr = wx.lib.masked.textctrl.TextCtrl(id=wxID_VXMLNODECUSTOMERPANELTXTCUSTOMNR,
              name=u'txtCustomNr', parent=self, pos=wx.Point(100, 59),
              size=wx.Size(210, 30), style=0, value='')

        self.cbGen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODECUSTOMERPANELCBGEN,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Build'), name=u'cbGen',
              parent=self, pos=wx.Point(314, 59), size=wx.Size(76, 30),
              style=0)

        self.vipAttr = vidarc.tool.input.vtInputAttrPanel.vtInputAttrPanel(id=wxID_VXMLNODECUSTOMERPANELVIPATTR,
              name=u'vipAttr', parent=self, pos=wx.Point(0, 117),
              size=wx.Size(417, 162), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.txtName,self.txtAbbr,
                self.txtCustomNr],
                lEvent=[
                        (self.txtName       , wx.EVT_TEXT),
                        (self.txtAbbr       , wx.EVT_TEXT),
                        (self.txtCustomNr   , wx.EVT_TEXT),
                ])
        self.bModified=False
        self.verbose=VERBOSE
        
        self.vitrArea.SetTagNames('mainArea','name')
        self.vitrArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.vitrArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.vitrArea.SetTagNames2Base(['customers','cfg'])
        
        self.objRegNode=None
        
        self.gbsData.AddGrowableRow(4)
        self.gbsData.AddGrowableCol(1)
        
        self.Move(pos)
        self.SetSize(size)
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj

    def OnChcCountryChoice(self, event):
        event.Skip()
        
    def SetModifiedOld(self,state):
        self.bModified=state
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        if self.verbose:
            vtLog.CallStack('')
        self.SetModified(False)
        vtXmlNodePanel.Clear(self)
        
        self.txtName.SetValue('')
        self.txtAbbr.SetValue('')
        self.txtCustomNr.SetValue('')
        #self.vitrArea.Clear()
        #vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        self.vitrArea.SetDoc(doc,bNet)
        self.vipAttr.SetDoc(doc,bNet)
        pass
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)

    def SetNode(self,node):
        try:
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'SetNode',origin='vLocPanel')
            vtXmlNodePanel.SetNode(self,node)
            if self.verbose:
                vtLog.CallStack('')
                print self.node
            self.txtName.SetValue(self.doc.getNodeText(self.node,'name'))
            self.txtAbbr.SetValue(self.doc.getNodeText(self.node,'abbreviation'))
            self.txtCustomNr.SetValue(self.doc.getNodeText(self.node,'number'))
            self.vitrArea.SetNode(self.node)
            self.vipAttr.SetNode(self.node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            if self.doc is None:
                return
            if node is None:
                if self.node is None:
                    return
                node=self.node
            self.doc.setNodeText(node,'name',self.txtName.GetValue())
            self.doc.setNodeText(node,'abbreviation',self.txtAbbr.GetValue())
            self.doc.setNodeText(node,'number',self.txtCustomNr.GetValue())
            self.vitrArea.GetNode(node)
            self.vipAttr.GetNode(node)
            self.doc.AlignNode(node)
            vtXmlNodePanel.GetNode(self,node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Lock(self,flag):
        if flag:
            self.txtName.Enable(False)
            self.txtAbbr.Enable(False)
            self.vitrArea.Enable(False)
            self.vipAttr.Enable(False)
        else:
            self.txtName.Enable(True)
            self.txtAbbr.Enable(True)
            self.vitrArea.Enable(True)
            self.vipAttr.Enable(True)
            
