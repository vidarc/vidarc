#Boa:FramePanel:vCustomerMainPanel
#----------------------------------------------------------------------------
# Name:         vCustomerMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vCustomerMainPanel.py,v 1.9 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
#from wxPython.wx import *
import  wx.lib.anchors as anchors
import getpass,time

try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vidarc.vApps.vCustomer.vXmlCustomerTree  import *
    from vidarc.vApps.vCustomer.vNetCustomer import *
    from vidarc.vApps.vCustomer.vCustomerListBook import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
    import vidarc.tool.InOut.fnUtil as fnUtil
    
    from vidarc.vApps.vHum.vNetHum import *
except:
    vtLog.vtLngTB('import')

import images
import images_lang

VERBOSE=0

def create(parent):
    return vCustomerMainPanel(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wx.NewId(), range(4))

[wxID_VCUSTOMERMAINPANEL, wxID_VCUSTOMERMAINPANELCBAPPLY, 
 wxID_VCUSTOMERMAINPANELCBCANCEL, wxID_VCUSTOMERMAINPANELPNDATA, 
 wxID_VCUSTOMERMAINPANELSBSTATUS, wxID_VCUSTOMERMAINPANELSLWNAV, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VCUSTOMERMAINPANELMNFILEITEMS_OPEN, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_EXIT, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_NEW, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_OPEN_CFG, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_SAVE, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_SAVEAS, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_SAVE_AS_CFG, 
 wxID_VCUSTOMERMAINPANELMNFILEITEM_SETTING, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(8)]

[wxID_VCUSTOMERMAINPANELMNIMPORTITEM_IMPORT_CSV, 
 wxID_VCUSTOMERMAINPANELMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VCUSTOMERMAINPANELMNEXPORTITEM_EXPORT_CSV, 
 wxID_VCUSTOMERMAINPANELMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VCUSTOMERMAINPANELMNTOOLSITEM_ALIGN, 
 wxID_VCUSTOMERMAINPANELMNTOOLSITEM_MNEXPORT, 
 wxID_VCUSTOMERMAINPANELMNTOOLSITEM_MN_IMPORT, 
 wxID_VCUSTOMERMAINPANELMNTOOLSMN_TOOL_REG_LOCK, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(4)]

[wxID_VCUSTOMERMAINPANELMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VCUSTOMERMAINPANELMNVIEWTREEITEM_DATE_USR_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(2)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VCUSTOMERMAINPANELMNVIEWITEM_LANG, 
 wxID_VCUSTOMERMAINPANELMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VCUSTOMERMAINPANELMNLANGITEM_LANG_DE, 
 wxID_VCUSTOMERMAINPANELMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VCUSTOMERMAINPANELMNHELPMN_HELP_ABOUT, 
 wxID_VCUSTOMERMAINPANELMNHELPMN_HELP_HELP, 
 wxID_VCUSTOMERMAINPANELMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vCustomerMainPanel(wx.Panel,vtXmlDomConsumer):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCUSTOMERMAINPANEL,
              name=u'vCustomerMainPanel', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(711, 546), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(703, 519))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VCUSTOMERMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              480), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VCUSTOMERMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VCUSTOMERMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(200, 0), size=wx.Size(504, 480),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCUSTOMERMAINPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self.pnData, pos=wx.Point(398, 8), size=wx.Size(100, 30),
              style=0)
        self.cbApply.SetConstraints(LayoutAnchors(self.cbApply, False, True,
              True, False))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VCUSTOMERMAINPANELCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCUSTOMERMAINPANELCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self.pnData, pos=wx.Point(398, 48),
              size=wx.Size(100, 30), style=0)
        self.cbCancel.SetConstraints(LayoutAnchors(self.cbCancel, False, True,
              True, False))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VCUSTOMERMAINPANELCBCANCEL)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        vtXmlDomConsumer.__init__(self)
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='vCustomerCfg',audit_trail=False)
        
        appls=['vCustomer','vHum']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netCustomer=self.netMaster.AddNetControl(vNetCustomer,'vCustomer',synch=True,
                                            verbose=0,audit_trail=True)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vCustomerAppl')
        
        self.vgpTree=vXmlCustomerTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(390,482),style=0,name="vgpTree",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netCustomer,True)
        
        self.nbData=vCustomerListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(382, 462),style=0,name="nbCustomer")
        self.nbData.SetDoc(self.netCustomer,True)
        self.nbData.SetNetDocHuman(self.netHum,True)
        self.nbData.SetConstraints(LayoutAnchors(self.nbData, True, True, True,
              True))
        
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netCustomer
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def Clear(self):
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        self.SetNode(node)
        if event.GetTreeNodeData() is not None:
            self.netCustomer.startEdit(node)
            #self.vglbDocGrpInfo.SetNode(node)
        else:
            # grouping tree item selected
            #self.vglbDocGrpInfo.SetNode(None)
            pass
        self.nbData.SetNode(node)
        event.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        event.Skip()
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netCustomer.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netCustomer.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netCustomer.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netCustomer.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netCustomer.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netCustomer.getNodeText(self.baseSettings,'treeviewgrp')
        
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netCustomer.getBaseNode()
        self.baseSettings=self.netCustomer.getChildForced(self.netCustomer.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netCustomer.createSubNode(self.netCustomer.getRoot(),'settings')
        #    self.netCustomer.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netCustomer.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netCustomer.ClearAutoFN()>0:
            self.netCustomer.Open(fn)
            
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netCustomer.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
        self.__setModified__(True)
    def __setCfg__(self):
        node=self.xdCfg.getChildForced(None,'layout')
        
        i=self.xdCfg.GetValue(node,'nav_sash',int,200)
        self.slwNav.SetDefaultSize((i, 1000))
        
        #i=self.xdCfg.GetValue(node,'top_sash',int,100)
        #self.slwTop.SetDefaultSize((1000, i))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.xdCfg.AlignDoc()
        self.xdCfg.Save()
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iWidth=event.GetDragRect().width
        #if iWidth>80:
        #    iWidth=80
        self.xdCfg.SetValue(node,'nav_sash',iWidth)
        self.xdCfg.Save()
        
        self.slwNav.SetDefaultSize((iWidth, 1000))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        #event.Skip()

    def OnSlwTopSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iHeight=event.GetDragRect().height
        #if iHeight>80:
        #    iHeight=80
        self.xdCfg.SetValue(node,'top_sash',iHeight)
        self.xdCfg.Save()
        
        self.slwTop.SetDefaultSize((1000, iHeight))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        #event.Skip()

    def OnSize(self, event):
        bMod=False
        sz=event.GetSize()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'width',sz[0])
        self.xdCfg.SetValue(node,'height',sz[1])
        self.xdCfg.Save()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
    def OnMove(self, event):
        pos=self.GetPosition()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'x',pos[0])
        self.xdCfg.SetValue(node,'y',pos[1])
        self.xdCfg.Save()
        event.Skip()

    def OnCbApplyButton(self, event):
        node=self.vgpTree.GetSelected()
        self.nbData.GetNode()
        self.netCustomer.doEdit(node)
        event.Skip()

    def OnCbCancelButton(self, event):
        node=self.vgpTree.GetSelected()
        self.nbData.SetNode(node)
        event.Skip()
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Customer module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Customer'),desc,version

