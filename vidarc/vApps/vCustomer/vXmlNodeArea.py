#----------------------------------------------------------------------------
# Name:         vXmlNodeArea.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060101
# CVS-ID:       $Id: vXmlNodeArea.py,v 1.7 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    from vidarc.vApps.vCustomer.vXmlNodeAreaPanel import *
    from vidarc.vApps.vCustomer.vXmlNodeAreaEditDialog import *
    from vidarc.vApps.vCustomer.vXmlNodeAreaAddDialog import *
    GUI=1
except:
    GUI=0
    pass

class vXmlNodeArea(vtXmlNodeBase):
    NODE_ATTRS=[
        ('Tag',None,'tag',None),
        ('Name','langId','name','language'),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='area'):
        global _
        _=vtLgBase.assignPluginLang('vCustomer')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'area')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node,lang=None):
        return self.GetML(node,'name',lang)
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val,lang=None):
        self.SetML(node,'name',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        _(u'tag'),_(u'name')
        return _(name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00kIDAT(\x91c\\~n\x07\x03)\x80\x89$\xd5\x0c\x0c\x0c,\x0c\x0c\x0c\xed{\xe7\
\xe3WT\xe9\x9cH\xbe\r\xd4\xd3p\xbex\xe5\xf9\xe2\x95\x106\xb2\x9b\xb1h@V\x8a\
\xa9\x07]\x03\\\x05&\x80\xe8\xa1\xcc\x0fx\x8c\x87\x03\x16d\x8eao8i6\x10\x03\
\x18\xd1\xd2\x12\x9eX\x87\xc47\xba\r\xc8\xa9\x00\xab8\xba\r\x98V\xa1\x19\x81\
S\x03.\x00\x00E\x11(:\x9b\xdde3\x00\x00\x00\x00IEND\xaeB`\x82' 

    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeAreaEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeAreaAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeAreaPanel
        else:
            return None

