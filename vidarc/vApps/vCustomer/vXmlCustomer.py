#----------------------------------------------------------------------------
# Name:         vXmlCustomer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051222
# CVS-ID:       $Id: vXmlCustomer.py,v 1.8 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vCustomer.vXmlNodeCustomerRoot import vXmlNodeCustomerRoot
from vidarc.vApps.vCustomer.vXmlNodeCustomer import vXmlNodeCustomer
from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x94IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xef\x00W\xf4\x03$^\xe9\t\x88[\
\xf8\x80+\x10\x07\xc1TT5q\x04\x82\x8cN\xec\xba\x91#\x12\x1a\xd4\xd5\xcd\xe3\
\xa9@\x00GZ\xa5\xc6\xc2\xd3A\x0b\x03\x00\xc9\x0e\xd8\xab\xac\xb2\xa0\xe9\xe0\
M\xb9\x02{\x8c\xd8c\xf4\x05\x98\xf5\x92\xc8D\xbay<\xa5_&%\xc0\x08\xc3\xb6i\
\x07O\xc9\xac\xf7\xff\x12-\x9b^/\x00:$\xd6`\x8d\xdd9`\t+\t\xcc\xd5\x91Vi\xa9\
_\x87X\xd6\xe7%\x8au\x8d\x80\xbe\r\xf6U\xd3\x81\x95N\x96\xd8\x0b\xe4r8\xd4\
\xd7{S\xed\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlCustomer(vtXmlDomReg):
    NODE_ATTRS=[
            ('name',None,'name',None),
            ('country',None,'contry',None),
            ('region',None,'region',None),
            ('distance',None,'distance',None),
            ('coor',None,'coor',None),
            ]
    TAGNAME_ROOT='customersroot'
    APPL_REF=['vHum','vGlobals']
    def __init__(self,attr='id',skip=[],synch=False,appl='vCustomer',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,
                        verbose=verbose,audit_trail=audit_trail)
            self.verbose=verbose
            oRoot=vXmlNodeCustomerRoot()
            oCust=vXmlNodeCustomer()
            oAddr=vtXmlNodeAddr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            oArea=vXmlNodeArea()
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oCust,True)
            self.RegisterNode(oAddr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(oArea,False)
            self.LinkRegisteredNode(oCust,oLog)
            self.LinkRegisteredNode(oCust,oAddr)
            self.LinkRegisteredNode(oCfg,oArea)
            self.LinkRegisteredNode(oArea,oArea)
            
        except:
            traceback.print_exc()
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'customers')
    #def New(self,revision='1.0',root='customersroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'customers')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
