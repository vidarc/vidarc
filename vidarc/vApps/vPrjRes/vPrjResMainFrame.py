#Boa:Frame:vDocMainFrame
#----------------------------------------------------------------------------
# Name:         vPrjResMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vPrjResMainFrame.py,v 1.4 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wxPython.wx import *
import  wx.lib.anchors as anchors
import getpass,time

import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
import vidarc.vApps.common.vStatusBarMessageLine as vStatusBarMessageLine
from vidarc.vApps.vPrjRes.vXmlPrjResTree  import *
from vidarc.vApps.vPrjRes.vNetPrjRes import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

import images
#import images_lang

VERBOSE=0

def create(parent):
    return vDocMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wxNewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wxNewId(), range(4))

[wxID_VDOCMAINFRAME, wxID_VDOCMAINFRAMEPNDATA, wxID_VDOCMAINFRAMESBSTATUS, 
 wxID_VDOCMAINFRAMESLWNAV, wxID_VDOCMAINFRAMESLWTOP, 
 wxID_VDOCMAINFRAMESTATICTEXT1, wxID_VDOCMAINFRAMETEXTCTRL1, 
] = [wx.NewId() for _init_ctrls in range(7)]

[wxID_VDOCMAINFRAMEMNFILEITEMS_OPEN, wxID_VDOCMAINFRAMEMNFILEITEM_EXIT, 
 wxID_VDOCMAINFRAMEMNFILEITEM_NEW, wxID_VDOCMAINFRAMEMNFILEITEM_OPEN_CFG, 
 wxID_VDOCMAINFRAMEMNFILEITEM_SAVE, wxID_VDOCMAINFRAMEMNFILEITEM_SAVEAS, 
 wxID_VDOCMAINFRAMEMNFILEITEM_SAVE_AS_CFG, 
 wxID_VDOCMAINFRAMEMNFILEITEM_SETTING, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(8)]

[wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_CSV, 
 wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_CSV, 
 wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VDOCMAINFRAMEMNTOOLSITEM_ALIGN, wxID_VDOCMAINFRAMEMNTOOLSITEM_MNEXPORT, 
 wxID_VDOCMAINFRAMEMNTOOLSITEM_MN_IMPORT, 
 wxID_VDOCMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(4)]

[wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(2)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VDOCMAINFRAMEMNVIEWITEM_LANG, wxID_VDOCMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VDOCMAINFRAMEMNLANGITEM_LANG_DE, wxID_VDOCMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

class vDocMainFrame(wxFrame,vtXmlDomConsumer.vtXmlDomConsumer,vStatusBarMessageLine.vStatusBarMessageLine):
    def _init_coll_fgsTop_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.staticText1, 0, border=0, flag=0)
        parent.AddWindow(self.textCtrl1, 0, border=0, flag=0)

    def _init_coll_mnLang_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNLANGITEM_LANG_EN,
              kind=wx.ITEM_RADIO, text=u'English')
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNLANGITEM_LANG_DE,
              kind=wx.ITEM_RADIO, text=u'Deutsch')

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNFILEITEM_NEW,
              kind=wx.ITEM_NORMAL, text=_(u'&New\tCtrl+N'))
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNFILEITEMS_OPEN,
              kind=wxITEM_NORMAL, text=_(u'&Open\tCtrl+O'))
        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNFILEITEM_OPEN_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'  ... Config'))
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVE,
              kind=wxITEM_NORMAL, text=_(u'&Save\tCtrl+S'))
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVEAS,
              kind=wxITEM_NORMAL, text=_(u'Save &As\tCtrl+SHIFT+S'))
        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVE_AS_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'  ... Config'))
        parent.AppendSeparator()
        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNFILEITEM_SETTING,
              kind=wx.ITEM_NORMAL, text=_(u'Settings'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNFILEITEM_EXIT,
              kind=wxITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_newMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_NEW)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_saveasMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVEAS)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_saveMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVE)
        self.Bind(wx.EVT_MENU, self.OnMnFileItems_openMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEMS_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_settingMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_SETTING)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_open_cfgMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_OPEN_CFG)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_save_as_cfgMenu,
              id=wxID_VDOCMAINFRAMEMNFILEITEM_SAVE_AS_CFG)

    def _init_coll_mnExport_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_CSV,
              kind=wx.ITEM_NORMAL, text=u'csv')
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_XML,
              kind=wx.ITEM_NORMAL, text=u'xml')
        self.Bind(wx.EVT_MENU, self.OnMnExportItem_export_xmlMenu,
              id=wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_XML)
        self.Bind(wx.EVT_MENU, self.OnMnExportItem_export_csvMenu,
              id=wxID_VDOCMAINFRAMEMNEXPORTITEM_EXPORT_CSV)

    def _init_coll_mnView_Items(self, parent):
        # generated method, don't edit

        parent.AppendMenu(help='', id=wxID_VDOCMAINFRAMEMNVIEWITEM_VIEW_TREE,
              submenu=self.mnViewTree, text=_(u'Tree...'))
        parent.AppendMenu(help='', id=wxID_VDOCMAINFRAMEMNVIEWITEM_LANG,
              submenu=self.mnLang, text=_(u'Language ...'))

    def _init_coll_mnImport_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_CSV,
              kind=wx.ITEM_NORMAL, text=u'csv')
        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_XML,
              kind=wx.ITEM_NORMAL, text=u'xml')
        self.Bind(wx.EVT_MENU, self.OnMnImportItem_import_xmlMenu,
              id=wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_XML)
        self.Bind(wx.EVT_MENU, self.OnMnImportItem_import_cvsMenu,
              id=wxID_VDOCMAINFRAMEMNIMPORTITEM_IMPORT_CSV)

    def _init_coll_mnViewTree_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP,
              kind=wx.ITEM_CHECK, text=_(u'Normal Grouping '))
        parent.Append(help=u'',
              id=wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP,
              kind=wx.ITEM_CHECK, text=_(u'ID Grouping '))
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_normal_grpMenu,
              id=wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_iddocgrp_grpMenu,
              id=wxID_VDOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP)

    def _init_coll_mnTools_Items(self, parent):
        # generated method, don't edit

        parent.AppendMenu(help='', id=wxID_VDOCMAINFRAMEMNTOOLSITEM_MNEXPORT,
              submenu=self.mnExport, text=_(u'Export ...'))
        parent.AppendMenu(help='', id=wxID_VDOCMAINFRAMEMNTOOLSITEM_MN_IMPORT,
              submenu=self.mnImport, text=_(u'Import ...'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VDOCMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK,
              kind=wx.ITEM_NORMAL, text=_(u'Request Lock\tF4'))
        parent.AppendSeparator()
        parent.Append(help=u'', id=wxID_VDOCMAINFRAMEMNTOOLSITEM_ALIGN,
              kind=wx.ITEM_NORMAL, text=_(u'Align XML'))
        self.Bind(wx.EVT_MENU, self.OnMnToolsItem_alignMenu,
              id=wxID_VDOCMAINFRAMEMNTOOLSITEM_ALIGN)
        self.Bind(wx.EVT_MENU, self.OnMnToolsMn_tools_req_lockMenu,
              id=wxID_VDOCMAINFRAMEMNTOOLSMN_TOOLS_REQ_LOCK)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'&View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Anal&yse'))
        parent.Append(menu=self.mnTools, title=_(u'&Tools'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsTop = wx.FlexGridSizer(cols=0, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsTop_Items(self.fgsTop)

        self.slwTop.SetSizer(self.fgsTop)

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wxMenuBar()

        self.mnFile = wxMenu(title=u'')

        self.mnAnalyse = wxMenu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnExport = wx.Menu(title='')

        self.mnImport = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnViewTree = wx.Menu(title=u'')

        self.mnLang = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnTools_Items(self.mnTools)
        self._init_coll_mnExport_Items(self.mnExport)
        self._init_coll_mnImport_Items(self.mnImport)
        self._init_coll_mnView_Items(self.mnView)
        self._init_coll_mnViewTree_Items(self.mnViewTree)
        self._init_coll_mnLang_Items(self.mnLang)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_VDOCMAINFRAME, name=u'vDocMainFrame',
              parent=prnt, pos=wx.Point(115, 10), size=wx.Size(814, 555),
              style=wxDEFAULT_FRAME_STYLE, title=_(u'VIDARC Document'))
        self._init_utils()
        self.SetClientSize(wx.Size(806, 528))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VDOCMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VDOCMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              489), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(u'navigation')
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VDOCMAINFRAMESLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VDOCMAINFRAMESLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetLabel(u'information')
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetToolTipString(u'information')
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VDOCMAINFRAMESLWTOP)

        self.pnData = wx.Panel(id=wxID_VDOCMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(216, 112), size=wx.Size(480, 352),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.staticText1 = wx.StaticText(id=wxID_VDOCMAINFRAMESTATICTEXT1,
              label='staticText1', name='staticText1', parent=self.slwTop,
              pos=wx.Point(0, 0), size=wx.Size(54, 13), style=0)

        self.textCtrl1 = wx.TextCtrl(id=wxID_VDOCMAINFRAMETEXTCTRL1,
              name='textCtrl1', parent=self.slwTop, pos=wx.Point(54, 0),
              size=wx.Size(100, 21), style=0, value='textCtrl1')

        self._init_sizers()

    def __init__(self, parent):
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        vStatusBarMessageLine.vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,5000)
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.__addLangMenu__()
        
        appls=['vPrjRes']
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        
        self.netPrjRes=self.netMaster.AddNetControl(vNetPrjRes,'vPrjRes',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjResAppl')
        #EVT_NET_XML_SETUP_CHANGED(self.netPrjRes,self.OnSetupChanged)
        #self.netPrjRes.SetSetupNode(None,['settings','server'])
        #EVT_NET_XML_OPEN_START(self.netPrjRes,self.OnOpenStart)
        EVT_NET_XML_OPEN_OK(self.netPrjRes,self.OnOpenOk)
        #EVT_NET_XML_OPEN_FAULT(self.netPrjRes,self.OnOpenFault)
        
        #EVT_NET_XML_SYNCH_START(self.netPrjRes,self.OnSynchStart)
        EVT_NET_XML_SYNCH_PROC(self.netPrjRes,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netPrjRes,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netPrjRes,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netPrjRes,self.OnGotContent)
        
        if 1==0:
            EVT_NET_XML_CONNECT(self.netPrjRes,self.OnConnect)
            EVT_NET_XML_DISCONNECT(self.netPrjRes,self.OnDisConnect)
            EVT_NET_XML_LOCK(self.netPrjRes,self.OnLock)
            
            EVT_NET_XML_LOGIN(self.netPrjRes,self.OnLogin)
            EVT_NET_XML_LOGGEDIN(self.netPrjRes,self.OnLoggedin)
            EVT_NET_XML_CLOSED(self.netPrjRes,self.OnClosed)
            EVT_NET_XML_CONNECT(self.netPrjRes,self.OnConnect)
            EVT_NET_XML_CONTENT_CHANGED(self.netPrjRes,self.OnChanged)
        
        self.bAutoConnect=True
        self.OpenCfgFile()
        
        self.vgpTree=vXmlPrjResTree(self.slwNav,wxNewId(),
                pos=(0,0),size=(198,489),style=0,name="vgpTree",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPrjRes,True)
        self.vgpTree.SetLanguages(self.languages,self.languageIds)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        
        #EVT_VGTRXMLDOC_GROUPING_CHANGED(self.vgpTree,self.OnDocGrpChanged)
        
        #self.vglbDocGrpInfo=vDocGrpInfoNoteBook(self.spwInfo,wxNewId(),
        #            pos=(0,0),size=(190,340),style=0,name="vDocGrpInfoNoteBook")
        #self.vglbDocGrpInfo.SetConstraints(
        #        anchors.LayoutAnchors(self.vglbDocGrpInfo, True, True, True, True)
        #        )
        #EVT_VGPDOC_GRP_INFO_CHANGED(self.vglbDocGrpInfo,self.OnDocGrpInfoChanged)
        #EVT_VGPDOC_CHANGED(self.vglbDocGrpInfo,self.OnDocChanged)
        #self.spwInfo.ReplaceWindow(self.pnInfo,self.vglbDocGrpInfo)
        #self.pnInfo.Destroy()
        
        #pn=self.vglbDocGrpInfo.AddDocumentPanel(self.spwInfo)
        #self.spwInfo.ReplaceWindow(self.pnItem,pn)
        #self.pnItem.Destroy()
        
        #self.vglbDocGrpInfo.SetLanguages(self.languages,self.languageIds)
        #self.vglbDocGrpInfo.SetNetDocHuman(self.netHum)
        #self.vglbDocGrpInfo.SetDoc(self.netPrjRes,True)
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        mnItems=self.mnViewTree.GetMenuItems()
        mnItems[0].Check(True)
        
        self.actLanguage='en'
        self.vgpTree.SetLanguage(self.actLanguage)
        #self.CreateNew()
        
        self.__makeTitle__()
    def __addLangMenu__(self):
        self.mnView.Remove(wxID_VDOCMAINFRAMEMNVIEWITEM_LANG)
        
        self.mnLang=wx.Menu()
        self.langIds=[]
        self.languages=[u'English',u'Deutsch']
        self.languageIds=[u'en',u'de']
        for i in range(0,len(self.languages)):
            id=wx.NewId()
            self.langIds.append(id)
            item = wx.MenuItem(self.mnView,help='', id=id,
                kind=wx.ITEM_NORMAL, text=self.languages[i])
            bmpName=getattr(vtArt, 'Lang%s' % self.languageIds[i].capitalize())
            item.SetBitmap(vtArt.getBitmap(bmpName))
            self.mnLang.AppendItem(item)
            self.Bind(wx.EVT_MENU, self.OnLanguage, id=id)
        
        self.mnView.AppendMenu(help='', id=wxID_VDOCMAINFRAMEMNVIEWITEM_LANG,
              submenu=self.mnLang, text=u'Language ...')
        self.actLanguage='en'
        
        
    def OnLanguage(self,event):
        if event.GetId()==self.langIds[0]:
            self.actLanguage='en'
        elif event.GetId()==self.langIds[1]:
            self.actLanguage='de'
        else:
            self.actLanguage='en'
        self.vgpTree.SetLanguage(self.actLanguage)
        
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC PrjRes")
        fn=self.netPrjRes.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())

        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def Clear(self):
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        if self.node is not None:
            self.netCustomer.endEdit(self.node)
        if event.GetTreeNodeData() is not None:
            self.netPrjRes.startEdit(node)
            #self.vglbDocGrpInfo.SetNode(node)
        else:
            # grouping tree item selected
            #self.vglbDocGrpInfo.SetNode(None)
            pass
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('connection login. '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netPrjRes.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        #cfgNode=self.netPrjRes.getChild(self.netPrjRes.getRoot(),'config')
        #self.netHum.SetCfgNode(cfgNode)
        #self.netHum.AutoConnect2Srv()
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnLock(self,evt):
        if evt.GetResponse()=='ok':
            try:
                self.nodeLock=self.netPrjRes.getNodeById(evt.GetID())
            except:
                self.nodeLock=None
        else:
            self.nodeLock=None
        evt.Skip()
    def OnUnLock(self,evt):
        if evt.GetResponse()=='ok':
            self.nodeLock=None
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netPrjRes.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netPrjRes.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netPrjRes.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netPrjRes.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netPrjRes.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netPrjRes.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
        
        self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netPrjRes.getBaseNode()
        self.baseSettings=self.netPrjRes.getChildForced(self.netPrjRes.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netPrjRes.createSubNode(self.netPrjRes.getRoot(),'settings')
        #    self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netPrjRes.ClearAutoFN()>0:
            self.netPrjRes.Open(fn)
            
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
        
            self.PrintMsg(_('open file finished. '))
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
    def SaveFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        sCurFN=self.netPrjRes.GetFN()
        if (sCurFN is None) and (fn is None):
            self.OnMnFileItem_saveasMenu(None)
            self.__setModified__(False)
            return
        self.PrintMsg(_('save file ... '))
        self.netPrjRes.Save(fn)
        self.PrintMsg(_('save file finished.'))
        
        self.__setModified__(False)
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netPrjRes.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
        self.__setModified__(True)
    def OnMnFileItems_openMenu(self, event):
        if self.netPrjRes.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vPrjResAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.netPrjRes.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()


    def OnMnFileItem_saveMenu(self, event):
        self.SaveFile()
        event.Skip()

    def OnMnFileItem_saveasMenu(self, event):
        if self.netPrjRes.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vPrjResAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlg = wx.FileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            fn=self.netPrjRes.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.txtEditor.SaveFile(filename)
                self.SaveFile(filename)
        finally:
            dlg.Destroy()

    def OnMnFileItem_newMenu(self, event):
        if self.netPrjRes.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vPrjResAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        self.PrintMsg(_('new ... '))
        self.CreateNew()
        self.PrintMsg(_('new finished.'))

        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netPrjRes.getChild(self.netPrjRes.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vPrjResSettingsDialog(self)
            dlg.SetXml(self.netPrjRes,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netPrjRes,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnMnImportItem_import_xmlMenu(self, event):
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if self.humanFN is not None:
                dlg.SetDirectory(self.humanFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.SetFileHuman(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def __validEmptyTextNode__(self,txt):
        if len(string.strip(txt))==0:
            return '---'
        else:
            return txt
    def OnMnImportItem_import_cvsMenu(self, event):
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("CSV files (*.csv)|*.csv|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.PrintMsg(_('import csv file ... '))
                # do import
                
                # update
                self.PrintMsg(_('generate ... '))

                self.__setModified__(True)
                self.vgpTree.SetNode(None)
                self.gProcess.SetValue(0)
                self.PrintMsg(_('import csv file finished.'))
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnExportItem_export_xmlMenu(self, event):
        event.Skip()

    def OnMnExportItem_export_csvMenu(self, event):
        event.Skip()
            
    def OnMnToolsItem_alignMenu(self, event):
        self.PrintMsg(_('align ... '))
        self.netPrjRes.AlignDoc(gProcess=self.gProcess)
        self.PrintMsg(_('align finished.'))
        self.gProcess.SetValue(0)
        self.__setModified__(True)
        event.Skip()

    def __EnsureOneCheckMenuItem__(self,mnItems,iAct,iStart,iCount):
        if mnItems[iAct].IsChecked():
            # uncheck all other
            for i in range(iStart,iStart+iCount):
                if i!=iAct:
                    mnItems[i].Check(False)
            return False
        else:
            bFound=False
            for i in range(iStart,iCount):
                if mnItems[i].IsChecked()==True:
                    bFound=True
            if bFound==False:
                mnItems[iAct].Check(True)
                return True
            return False
    def OnMnViewTreeItem_normal_grpMenu(self, event):
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,0,0,2):
            return
        self.vgpTree.SetNormalGrouping()
        
        self.netPrjRes.setNodeText(self.baseSettings,'treeviewgrp','normal')
        self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
        self.__setModified__(True)
        self.PrintMsg(_('generate ... '))
        self.vgpTree.SetNode(None)
        self.PrintMsg(_('generate finished.'))

        event.Skip()
    def OnMnViewTreeItem_iddocgrp_grpMenu(self, event):
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,1,0,2):
            return
        self.vgpTree.SetIdGrouping()
        
        self.netPrjRes.setNodeText(self.baseSettings,'treeviewgrp','iddocgrp')
        self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
        self.__setModified__(True)
        self.PrintMsg(_('generate ... '))
        self.vgpTree.SetNode(None)
        self.PrintMsg(_('generate finished.'))
        event.Skip()
    def OnDocGrpChanged(self,event):
        iGrp=event.GetGrouping()
        mnItems=self.mnViewTree.GetMenuItems()
        if iGrp==0:
            mnItems[0].Check(True)
            mnItems[1].Check(False)
            self.netPrjRes.setNodeText(self.baseSettings,'treeviewgrp','normal')
            self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        elif iGrp==1:
            mnItems[0].Check(False)
            mnItems[1].Check(True)
            self.netPrjRes.setNodeText(self.baseSettings,'treeviewgrp','iddocgrp')
            self.netPrjRes.AlignNode(self.baseSettings,iRec=1)
            self.__setModified__(True)
        
    def OnFrameClose(self, event):
        if self.netMaster.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vPrjResAppl'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.netMaster.Save()
                pass
        self.netMaster.ShutDown()
        
        event.Skip()

    def OnMnViewTreeItem_date_short_usr_grpMenu(self, event):
        event.Skip()

    def OnMnFileItem_open_cfgMenu(self, event):
        if self.netPrjRes.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vPrjResAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenCfgFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_save_as_cfgMenu(self, event):
        if self.netPrjRes.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vPrjResAppl',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlg = wx.FileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.xdCfg.Save(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()
    def __setCfg__(self):
        node=self.xdCfg.getChildForced(None,'size')
        iX=self.xdCfg.GetValue(node,'x',int,20)
        iY=self.xdCfg.GetValue(node,'y',int,20)
        iWidth=self.xdCfg.GetValue(node,'width',int,800)
        iHeight=self.xdCfg.GetValue(node,'height',int,600)
        iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
        self.Move((iX,iY))
        self.SetSize((iWidth,iHeight))
        
        node=self.xdCfg.getChildForced(None,'layout')
        
        #i=self.xdCfg.GetValue(node,'nav_sash',int,200)
        #self.slwNav.SetDefaultSize((i, 1000))
        
        #i=self.xdCfg.GetValue(node,'top_sash',int,100)
        #self.slwTop.SetDefaultSize((1000, i))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.xdCfg.AlignDoc()
        self.xdCfg.Save()
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iWidth=event.GetDragRect().width
        #if iWidth>80:
        #    iWidth=80
        self.xdCfg.SetValue(node,'nav_sash',iWidth)
        self.xdCfg.Save()
        
        self.slwNav.SetDefaultSize((iWidth, 1000))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()

    def OnSlwTopSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        node=self.xdCfg.getChildForced(None,'layout')
        iHeight=event.GetDragRect().height
        #if iHeight>80:
        #    iHeight=80
        self.xdCfg.SetValue(node,'top_sash',iHeight)
        self.xdCfg.Save()
        
        self.slwTop.SetDefaultSize((1000, iHeight))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()

    def OnFrameSize(self, event):
        bMod=False
        sz=event.GetSize()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'width',sz[0])
        self.xdCfg.SetValue(node,'height',sz[1])
        self.xdCfg.Save()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        return
        bMod=False
        iWidth=event.GetSize()[0]
        if self.slwNav.GetSize()[0]>iWidth:
            iWidth=self.slwNav.GetSize()[0]
            bMod=True
        iHeightAdd=self.sbStatus.GetSize()[1]#*4
        iHeightAdd+=wx.SystemSettings.GetMetric(wx.SYS_CAPTION_Y)*2
        iHeightAdd+=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)*2
        iHeight=event.GetSize()[1]
        if self.slwTop.GetSize()[1]+iHeightAdd>iHeight:
            iHeight=self.slwTop.GetSize()[1]+iHeightAdd
            bMod=True
        if bMod:
            self.SetSize((iWidth,iHeight))
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'width',iWidth)
        self.xdCfg.SetValue(node,'height',iHeight)
        self.xdCfg.Save()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        #event.Skip()

    def OnFrameMove(self, event):
        pos=self.GetPosition()
        node=self.xdCfg.getChildForced(None,'size')
        self.xdCfg.SetValue(node,'x',pos[0])
        self.xdCfg.SetValue(node,'y',pos[1])
        self.xdCfg.Save()
        event.Skip()

    def OnMnToolsMn_tools_req_lockMenu(self, event):
        node=self.vgpTree.GetSelected()
        self.netPrjRes.reqLock(node)
        event.Skip()
