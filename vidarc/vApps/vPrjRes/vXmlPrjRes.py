#----------------------------------------------------------------------------
# Name:         vXmlPrjRes.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlPrjRes.py,v 1.3 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vPrjRes.vXmlNodePrjRes import vXmlNodePrjRes
#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

import traceback
VERBOSE=1

class vXmlPrjRes(vtXmlDomReg):
    NODE_ATTRS=[
            ('name',None,'name',None),
            ('country',None,'contry',None),
            ('region',None,'region',None),
            ('distance',None,'distance',None),
            ('coor',None,'coor',None),
            ]
    TAGNAME_ROOT='prjresroot'
    def __init__(self,attr='id',skip=[],synch=False,appl='vPrjRes',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            self.verbose=verbose
            oPrjRes=vXmlNodePrjRes()
            #oAddr=vtXmlNodeAddr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #oArea=vXmlNodeArea()
            self.RegisterNode(oPrjRes,True)
            #self.RegisterNode(oAddr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(oArea,False)
            self.LinkRegisteredNode(oPrjRes,oLog)
            #self.LinkRegisteredNode(oCust,oAddr)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
        except:
            traceback.print_exc()
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'prjres')
    #def New(self,revision='1.0',root='prjresroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'prjres')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
