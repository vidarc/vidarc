#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Application img/Build02_16.png images.py",
    "-a -u -n Launch img/Launch01_32.png images.py",
    
    #"-u -i -n Lang00 img/Lang_en_16.png images_lang.py",
    #"-a -u -n Lang01 img/Lang_de_16.png images_lang.py",
    #"-a -u -n Lang00Big img/Lang_en_32.png images_lang.py",
    #"-a -u -n Lang01Big img/Lang_de_32.png images_lang.py",
    #"-a -u -n ApplyMultiple img/ApplyMultiple01_32.png images_lang.py",
    #"-a -u -n User Usr01_16.png hum_tree_images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    #"-a -u -n Add add.png images.py",
    #"-a -u -n Del waste.png images.py",
    #"-a -u -n Apply checkmrk.png images.py",

    "-u -i -n Splash img/splashTmpl02.png images_splash.py",
    "-a -u -n Logo img/Vidarc_Logo_Brief.png images_splash.py",
    
    "-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

