#----------------------------------------------------------------------------
# Name:         vXmlPrjResTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlPrjResTree.py,v 1.1 2006/02/22 11:32:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vPrjRes.vNetPrjRes import *

class vXmlPrjResTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        #self.bGrpMenu=True
        #self.grpMenu=[('normal',_(u'normal')),('address:country',_(u'country')),('address:region',_(u'region'))]
    def SetDftGrouping(self):
        self.grouping=[]
        self.bGrouping=False
        self.label=[('name','')]
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['name','|id','address:country','address:region'])
    def SetGroupingByName(self,sGrp):
        if sGrp=='normal':
            return self.SetNormalGrouping()
        elif sGrp=='address:country':
            return self.SetCountryGrouping()
        elif sGrp=='address:region':
            return self.SetRegionGrouping()
        return False
    def SetNormalGrouping(self):
        if self.iGrp==0:
            return False
        self.iGrp=0
        self.SetGrouping([],[('name','')])
        return True
    def SetCountryGrouping(self):
        if self.iGrp==1:
            return False
        self.iGrp=1
        self.SetGrouping({'customer':[('address:country','')]},[('name','')])
        return True
    def SetRegionGrouping(self):
        if self.iGrp==2:
            return False
        self.iGrp=2
        self.SetGrouping({'customer':[('address:country',''),('address:region','')]},[('name','')])
        return True
