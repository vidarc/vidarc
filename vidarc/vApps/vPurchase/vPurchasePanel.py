#Boa:FramePanel:vPurchasePanel
#----------------------------------------------------------------------------
# Name:         vPurchasePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vPurchasePanel.py,v 1.8 2007/10/25 11:02:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

try:
    import vidarc.tool.xml.vtXmlNodeRegListBook
    import vidarc.tool.xml.vtXmlNodeTagCmdPanel
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vPurchase.vXmlPurchaseTree  import *
    from vidarc.vApps.vPurchase.vNetPurchase import *
    
    from vidarc.vApps.vContact.vNetContact import *
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
    from vidarc.vApps.vPrj.vNetPrj import *
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    
    import vidarc.vApps.vPurchase.images as imgPurchase
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vPurchasePanel(parent)

[wxID_VPURCHASEPANEL, wxID_VPURCHASEPANELNBDATA, wxID_VPURCHASEPANELPNDATA, 
 wxID_VPURCHASEPANELSLWNAV, wxID_VPURCHASEPANELSLWTOP, 
 wxID_VPURCHASEPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

def getPluginImage():
    return imgPurchase.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPurchase.getPluginBitmap())
    return icon

class vPurchasePanel(wx.Panel):
    VERBOSE=0
    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbData, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPURCHASEPANEL, name=u'vPurchasePanel',
              parent=prnt, pos=wx.Point(325, 29), size=wx.Size(818, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(810, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPURCHASEPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPURCHASEPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VPURCHASEPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VPURCHASEPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VPURCHASEPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 108), size=wx.Size(592, 252),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(applName=u'vPurchase Tag',
              id=wxID_VPURCHASEPANELVITAG, name=u'viTag', parent=self.slwTop,
              pos=wx.Point(0, 0), size=wx.Size(608, 97), style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_SELECTED,
              self.OnViTagToolXmlTagSelected, id=wxID_VPURCHASEPANELVITAG)

        self.nbData = vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(applName=u'vPurchase',
              id=wxID_VPURCHASEPANELNBDATA, name=u'nbData', parent=self.pnData,
              pos=wx.Point(4, 4), size=wx.Size(580, 240), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPurchase')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.xdCfg=vtXmlDom(appl='vPurchaseCfg',audit_trail=False)
        
        appls=['vPurchase','vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netPurchase=self.netMaster.AddNetControl(vNetPurchase,'vPurchase',synch=True,verbose=0,
                                            audit_trail=True)
        self.netContact=self.netMaster.AddNetControl(vNetContact,'vContact',synch=True,verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPurchaseMaster')
        
        self.vgpTree=vXmlPurchaseTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trPurchase",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPurchase,True)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        
        self.nbData.Show(False)
        self.nbData.SetDoc(self.netPurchase,True)
        oPurchase=self.netPurchase.GetReg('purchase')
        
        oDoc=self.netPurchase.GetReg('Doc')
        oDoc.SetTreeClass(vXmlPurchaseTree)
        pnCls=oDoc.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnDoc")
        pn.SetRegNode(oDoc)
        pn.SetDoc(self.netPurchase,True)
        self.viTag.AddWidgetDependent(pn,oDoc)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        pn=self.viTag.CreateRegisteredPanel('purchaseDepartment',
                        self.netPurchase,True,
                        self.pnData)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        oDataCollector=self.netPurchase.GetReg('dataCollector')
        self.viTag.AddWidgetDependent(None,oDataCollector)
        self.viTag.AddWidgetDependent(self.nbData,oPurchase)
        self.viTag.SetDoc(self.netPurchase,True)
        self.viTag.SetRegNode(oPurchase,bIncludeBase=True)
        self.viTag.SetNetDocs(self.netPurchase.GetNetDocs())
        pnPur=self.nbData.GetPanelByName('purchase')
        pnParts=self.nbData.GetPanelByName('purchaseParts')
        pnPur.SetDependentPartsPanel(pnParts)
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        #self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netPurchase
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OpenFile(self,fn):
        if self.netPurchase.ClearAutoFN()>0:
            self.netPurchase.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbPurchase', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
        event.Skip()
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.GetParent().Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Purchase module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Purchase'),desc,version
    def OnViTagToolXmlTagSelected(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            lWid=event.GetListWidget()
            self.bxsData.Layout()
            #if lWid is not None:
            #    for wid in lWid:
            #        wid.Fit()
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
