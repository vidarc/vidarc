#----------------------------------------------------------------------------
# Name:         vXmlNodePurchase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlNodePurchase.py,v 1.6 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

import os,copy
import vidarc.tool.report.vRepLatex as latex

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.state.veStateAttr import veStateAttr
from vidarc.ext.report.veRepDoc import veRepDoc
from vidarc.tool.lang.vtLgDictML import vtLgDictML
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        from vXmlNodePurchasePanel import *
        #from vXmlNodePurchaseEditDialog import *
        #from vXmlNodePurchaseAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePurchase(vtXmlNodeTag,veStateAttr,veDataCollectorCfg):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            #('Sum',None,'sum',None),
            ('Currency',None,'currency',None),
            ('PurState',None,'purState',None),
            ('Result',None,'result',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='purchase',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vPurchase')
        vtXmlNodeTag.__init__(self,tagName)
        veStateAttr.__init__(self,'smPurchase','purState')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
        self.oSMlinker=None
        self.oRep=veRepDoc()
        self.dt=vtDateTime(True)
        self.lgDict=None
        if GUI:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            
            #domain=vtLgBase.getApplDomain()
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def GetDescription(self):
        return _(u'purchase')
    # ---------------------------------------------------------
    # specific
    def GetSum(self,node):
        c=self.doc.getChild(node,'purchaseParts')
        if c is not None:
            sVal=self.doc.GetNodeAttribute(c,'GetSum')
            if len(sVal)==0:
                return '0.00'
            return sVal
        return '0.00'
        #return self.Get(node,'sum')
    def GetSumFloat(self,node):
        try:
            return float(self.GetSum(node))
        except:
            return 0.0
    def GetProject(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def GetPrjId(self,node):
        return self.GetChildForeignKeyStr(node,'project','fid','vPrj')
    def GetPrjIdNum(self,node):
        return self.GetChildForeignKey(node,'project','fid','vPrj')
    def GetSupplier(self,node):
        return self.GetChildAttrStr(node,'supplier','fid')
    def GetSupplierDepartment(self,node):
        return self.GetChildAttrStr(node,'supplierDepart','fid')
    def GetSupplierAddress(self,node):
        return self.GetChildAttrStr(node,'supplier','address')
    def GetSupplierContact(self,node):
        return self.GetChildAttrStr(node,'supplier','contact')
    def GetOrderDt(self,node):
        return self.Get(node,'orderDt')
    def GetDeliveryDt(self,node):
        return self.Get(node,'deliveryDt')
    def GetOrderBy(self,node):
        return self.GetChildAttrStr(node,'orderBy','fid')
    def GetOrderById(self,node):
        return self.GetChildForeignKeyStr(node,'orderBy','fid','vHum')
    def GetOrderByIdNum(self,node):
        return self.GetChildForeignKey(node,'orderBy','fid','vHum')
    def GetCurrency(self,node):
        return self.Get(node,'currency')
    def GetPurchaseID(self,node):
        return self.GetAttrStr(node,'purchaseID')
    def GetResult(self,node):
        return self.Get(node,'result')
    def GetResultInt(self,node):
        try:
            s=self.GetResult(node)
            if len(s)==0:
                return 0
            return int(s)
        except:
            return 0
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    def SetSumFloat(self,node,val):
        self.SetSum(node,str(val))
    def SetSupplierStr(self,node,val):
        self.Set(node,'supplier',val)
    def SetSupplierContactStr(self,node,val):
        self.Set(node,'contact',val)
    def SetSupplierAddress(self,node,val):
        self.SetChildAttrStr(node,'supplier','address',val)
    def SetSupplierContact(self,node,val):
        self.SetChildAttrStr(node,'supplier','contact',val)
    def SetOrderDt(self,node,val):
        self.Set(node,'orderDt',val)
    def SetDeliveryDt(self,node,val):
        self.Set(node,'deliveryDt',val)
    def SetOrderBy(self,node,id):
        return self.SetChildAttrStr(node,'orderBy','fid',id)
    def SetOrderById(self,node,id):
        return self.SetChildForeignKeyStr(node,'orderBy','fid',id,'vHum')
    def SetOrderByIdNum(self,node,id):
        return self.SetChildForeignKey(node,'orderBy','fid',id,'vHum')
    def SetCurrency(self,node,val):
        self.Set(node,'currency',val)
    def SetResult(self,node,val):
        self.Set(node,'result',val)
    def SetPrjId(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKeyStr(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    def SetPrjIdNum(self,node,id):
        n=self.GetChildForced(node,'projects')
        self.SetAttrStr(n,'multiple','1')
        self.SetChildForeignKey(n,'project','fid',id,'vPrj')
        nn=self.GetChild(n,'project')
        self.SetAttrStr(nn,'fixed','0')
    #def SetName(self,node,val):
    #    self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # state machine related
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,'purState','owner','vHum')
    def GetPurState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,'purState')
            id=self.GetChildAttr(node,'purState','fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetPurState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,'purState')
            c=self.GetChildForced(node,'purState')
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,'purState','owner',val,'vHum')
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('purchase on go'),None),
            'NoGo':(_('purchase on no go'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        try:
            self.dt.SetDateSmallStr(self.GetOrderDt(node))
        except:
            pass
        dtO=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        try:
            self.dt.SetDateSmallStr(self.GetDeliveryDt(node))
        except:
            pass
        dtD=self.dt.GetDateStr()#+' '+self.dt.GetTimeStr()[:8]
        return u'\n'.join([_(u'purchase'),
                ' '+_('order date:')+dtO,
                ' '+_('delivery date:')+dtD])
    # ---------------------------------------------------------
    # document machine related
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            lang=kwargs['lang']
            self.oRep.__setLang__(lang)
            iLevel=kwargs['level']
            sName=self.GetName(node,lang)
            sDesc=self.GetDesc(node,lang)
            sCurrency=self.GetCurrency(node)
            
            f.write(os.linesep)
            f.write(self.oRep.__getHeadLine__(iLevel,sName))
            f.write(os.linesep)
            
            kkwargs=copy.copy(kwargs)
            kkwargs.update({'bSkipHeadLine':True,'bSkipSummary':True})
            veDataCollectorCfg.Process(self,f,*args,**kkwargs)
            
            
            self.__getTranslation__()
            if 0:
                oSupAdr=self.doc.GetReg('addresses')
                #oSupCont=self.doc.GetReg('contacts')
                fid=self.GetSupplier(node)
                netDoc,nodeSup=self.doc.GetNode(fid)
                oSup=netDoc.GetRegByNode(nodeSup)
                if oSup is not None:
                    sSupplier=oSup.GetML(nodeSup,'name',lang)
                else:
                    sSupplier=''
                sAdr=self.GetSupplierAddress(node)
                #sCont=self.GetSupplierContact(node)
                fidCont=self.GetSupplierContact(node)
                fidOrderBy=self.GetOrderBy(node)
                oSupAdr.SetDoc(netDoc)
                #oSupCont.SetDoc(netDoc)
                if len(fidCont)>0:
                    netDocCont,nodeCont=self.doc.GetNode(fidCont)
                    oSupCont=netDocCont.GetRegByNode(nodeCont)
                    lCont=[]
                    for a in ['title','firstname','surname']:
                        lCont.append(oSupCont.GetML(nodeCont,a,lang=lang))
                else:
                    #netDocCont,nodeCont=None,None
                    lCont=None
                if len(fidOrderBy)>0:
                    netDocOrderBy,nodeOrderBy=self.doc.GetNode(fidOrderBy)
                    oOrderBy=netDocOrderBy.GetRegByNode(nodeOrderBy)
                    nodePersonal=netDocOrderBy.getChild(nodeOrderBy,'personal')
                    oPersonal=netDocOrderBy.GetRegByNode(nodePersonal)
                    lOrderBy=[]
                    for a in ['frm','title']:
                        lOrderBy.append(oPersonal.GetML(nodePersonal,a,lang=lang))
                    lOrderBy=[' '.join(lOrderBy)]
                    for a in ['firstname','surname']:
                        lOrderBy.append(oOrderBy.Get(nodeOrderBy,a))
                else:
                    lOrderBy=None
                lAdr=oSupAdr.GetValuesFromParent(nodeSup,sAdr)
                #lCont=oSupCont.GetValuesFromParent(nodeSup,sCont)
                _(u'address'),_(u'city'),_(u'zip'),_(u'street')
                _(u'firstname'),_(u'surname'),_(u'title'),_(u'contact')
                f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
                f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                f.write(''.join([self.lgDict.Get(u'supplier',lang),' & \\\\ & ',os.linesep,]))
                self.oRep.__tableSimplePseudo__(f,'ll',[(sSupplier,'')])
                f.write(''.join(['\\\\',os.linesep,]))
                if lAdr is not None:
                    f.write(''.join([self.lgDict.Get(u'address',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'street',lang),
                            self.lgDict.Get(u'city',lang),self.lgDict.Get(u'zip',lang)],lAdr))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lCont is not None:
                    f.write(''.join([self.lgDict.Get(u'contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    #f.write(''.join([self.lgDict.Get(u'contact',lang),os.linesep,os.linesep]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lCont))
                    f.write(''.join(['\\\\',os.linesep,]))
                if lOrderBy is not None:
                    #f.write(''.join([self.lgDict.Get(u'our contact',lang),os.linesep,os.linesep]))
                    f.write(''.join([' & \\\\ ',os.linesep,self.lgDict.Get(u'our contact',lang),' & \\\\ ',os.linesep,' & ',os.linesep,]))
                    self.oRep.__tableSimplePseudo__(f,'ll',zip([self.lgDict.Get(u'title',lang),
                            self.lgDict.Get(u'firstname',lang),self.lgDict.Get(u'surname',lang)],lOrderBy))
                f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
                f.write(os.linesep.join([latex.texReplace(s) for s in sDesc.split('\n')]))
                
            oParts=self.doc.GetReg('purchaseParts')
            lSetup=oParts.GetSetup()
            lTrans=[self.lgDict.Get('pos',lang)]
            lFmt=[]
            def convBool(s):
                if s=='1':
                    return 'X'
                else:
                    return ''
            def convOrig(s):
                return s
            def convFloat(s):
                try:
                    return vtLgBase.format('%6.2f',float(s))
                    #return '%6.2f'%float(s)
                except:
                    return ''
            def convEmpty(s):
                return ''
            lConv=[]
            lCurrency=['']
            for d in lSetup:
                if 'is2Report' in d:
                    if d['is2Report']==0:
                        lConv.append(None)
                        #lFmt.append('c')
                        continue
                if 'trans' in d:
                    lTrans.append(self.lgDict.Get(d['trans'],lang))
                else:
                    lTrans.append('')
                if 'isCurrency' in d:
                    if d['isCurrency']==1:
                        lCurrency.append('[ '+sCurrency+' ]')
                    else:
                        lCurrency.append('')
                else:
                    lCurrency.append('')
                if 'typedef' in d:
                    dType=d['typedef']
                    sType=dType['type']
                    if sType=='bool':
                        func=convBool
                        sFmt='c'
                    elif sType=='string':
                        func=convOrig
                        sFmt='l'
                    elif sType=='int':
                        func=convOrig
                        sFmt='r'
                    elif sType=='float':
                        func=convFloat
                        sFmt='r'
                    else:
                        func=convEmpty
                        sFmt='c'
                    lConv.append(func)
                    lFmt.append(sFmt)
                else:
                    lConv.append(convEmpty)
                    lFmt.append('c')
            nodeParts=self.doc.getChild(node,'purchaseParts')
            lVal,lSum=oParts.GetValues(nodeParts)
            sFmt=oParts.GetLaTexTableFmt()
            sCurrencyTable=' & '.join(lCurrency)
            strs=[]
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{%s}'%sFmt
            strs.append(s)
            s='\\hline '+' & '.join(lTrans)+'\\\\ %s \\\\ \\hline'%sCurrencyTable
            strs.append(s)
            strs.append('  \\endfirsthead')
            strs.append(s)
            strs.append('  \\endhead')
            iNum=1
            for lRow in lVal:
                lVal=[str(iNum)]
                for func,val in zip(lConv,lRow):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lRow)])+'\\\\'
                strs.append(s)
                iNum+=1
            if lSum is not None:
                strs.append('\\hline \\hline')
                lVal=['']
                for func,val in zip(lConv,lSum):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lSum)])+'\\\\'
                strs.append(s)
            strs.append('\\hline')
            sPrefix=' '.join([self.lgDict.Get('part',lang),sName])
            strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
            strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            f.write(os.linesep.join(strs))
            f.write(os.linesep)
            
            #f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                #f.write(''.join([os.linesep,'\\vspace{1cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
            f.write(os.linesep)
            
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        
    # ---------------------------------------------------------
    # inheritance
    def GetAttrValueToDict(self,d,node,*args,**kwargs):
        if node is None:
            return d
        if d is None:
            d={}
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
        if netPrj is not None:
            if nodePrj is not None:
                dPrj=netPrj.GetAttrValueToDict(nodePrj,*args,**kwargs)
                for k,v in dPrj.items():
                    d[':'.join(['vPrj',k])]=v
        return d
    def GetAttributeValFullLang(self,node,tagName,lang=None):
        if tagName[:5]=='vPrj:':
            netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
            if netPrj is not None:
                if nodePrj is not None:
                    return netPrj.getNodeAttributeValFullLang(nodePrj,tagName[5:],lang=lang)
        return ''
    def SetDoc(self,doc,bNet=False):
        vtXmlNodeBase.SetDoc(self,doc)
        if self.doc is None:
            self.lgDict=None
            return
        if vcCust.is2Import(__name__):
            l=[]
            for tup in self.doc.GetLanguages():
                l.append(tup[1])
            if len(l)==0:
                l=['en','de','fr']
            try:
                self.lgDict=vtLgDictML('vPurchase',langs=l)
            except:
                try:
                    self.lgDict=vtLgDictML('vPurchase',dn=os.path.split(__file__)[0],langs=l)
                except:
                    self.lgDict=None
                    vtLog.vtLngCurCls(vtLog.ERROR,'translation veReportDoc not found',self)
                    vtLog.vtLngTB(self.__class__.__name__)
    def __createPost__(self,node,func=None,*args,**kwargs):
        try:
            if self.doc is None:
                return
            oLogin=self.doc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
                return
            if oLogin.GetUsrId()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
                return
            if self.oSMlinker is None:
                self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
            if self.oSMlinker is not None:
                oRun=self.oSMlinker.GetRunner(self.tagName,'purState')
            else:
                oRun=None
            if oRun is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.tagName,'attr01'),self)
                return
            if oRun.GetInitID()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
                return
            if node is not None:
                self.dt.Now()
                self.SetTag(node,self.dt.GetDateSmallStr())
                fid=self.GetProject(node)
                netPrj,nodePrj=self.doc.GetNode(fid)
                if nodePrj is not None:
                    sPrj=netPrj.GetPrjName(nodePrj)
                else:
                    sPrj=''
                fid=self.GetSupplier(node)
                netSup,nodeSup=self.doc.GetNode(fid)
                if nodeSup is not None:
                    langs=self.doc.GetLanguageIds()
                    if langs is not None:
                        sDt=self.dt.GetDateStr()
                        for lang in langs:
                            sSup=netSup.GetNodeAttribute(nodeSup,'GetName',lang)
                            self.SetName(node,' '.join([sPrj,sDt,sSup]),lang)
                oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return vtXmlNodeTag.GetAttrFilterTypes(self)+[
                ('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('supplier',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('contact',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('|purchaseID',vtXmlFilterType.FILTER_TYPE_LONG),
                #('projects',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00iIDAT8\x8d\xc5\x93]\x0e\xc0 \x08\x83\x07z\xc6\x1d\xd2;\xfa\xf3\xe4b\
\xb05,,Y\x1f\x8b\xc5\x0f\xa2"\x9a\xae\x884\x94\xfe\xa2Af\x85\xd2j\xb7\xde\
\xadI\\\x04(\xcc|\xb1K\\\x0f\xd9\x1bgm\xf5\xe9\x08\x08\x17y\x0f\x01\xc3v78a\
\x9e\xf4?\x01}H\x88\xa8\xb4\xda\xad\xbf\x11\xb0\xf0\x94%\x83\x04\x0c\xdf\xb5\
\x83\xb7\n\x7f\xa6\x01RM3\xf1\xf6\x82\xd9@\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=veDataCollectorCfg.GetEditDialogClass(self).copy()
            d['pnCls']=veDataCollectorCfg.GetPanelClass(self)
            return d
        else:
            return None
        return veDataCollectorCfg.GetEditDialogClass(self)
        if GUI:
            return vXmlNodePurchaseEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodePurchaseAddDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnPurchase',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePurchasePanel
        else:
            return None
    def GetCollectorPanelClass(self):
        return veDataCollectorCfg.GetPanelClass(self)

_('sum'),_('projects'),_('supplier'),_('contact'),_(u'|purchaseID')
