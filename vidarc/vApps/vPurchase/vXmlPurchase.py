#----------------------------------------------------------------------------
# Name:         vXmlPurchase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlPurchase.py,v 1.7 2007/11/05 04:14:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vPurchase.vXmlNodePurchaseRoot import vXmlNodePurchaseRoot

from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

import vidarc.vApps.vPurchase.__register__

def getPluginData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xaaIDAT8\x8d\xc5\x931\x0e\xc4 \x0c\x04\xd7\x1c\xdf\xe1^\x83\x94\x9f\
\xa04TiP~\x82\x94\xdf\xe4A\xa4\x88\x88\x90Y\xdf]w[\xae\xccz,\x1b\x11\xf7\x82\
V(\xb1M&\x80s=D{\xee\xd7B\xe6\x01\x80t\x02\xab\xab\xa5\x1e\xe8\xad\x82\x9c\
\xea\xe4m\xfb2yt\x04\xf6\xd8\xf2}(\xb1\x8d\xf3\x8dE\xbacN\x159\xd5\xc7\x0f%6\
y\xefK\xd3E\x16.\xd3\x13`a\x8fb\xa1\xff'\xf0\xc0\xbd\xd3M\x05}#8\xd7CB\x89\
\xcdY\x17\xc6\x88\xfa\x16\xc6\x10z\x89\x9f\xc6\xe9d\xbd1=$\x0b\x9fn\x81\xfdF\
M4v\xd4\xa2\x04\xeccY\x9f\xed\x02\xcc\x89Z2$\xae\xce\xe2\x00\x00\x00\x00IEND\
\xaeB`\x82" 

class vXmlPurchase(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='purchaseroot'
    APPL_REF=['vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
    def __init__(self,appl='vPurchase',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                                audit_trail=audit_trail)
            oRoot=vXmlNodePurchaseRoot('root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodePurchaseRoot()
            self.RegisterNode(oRoot,False)
            
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            
            #vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
            vidarc.vApps.vPurchase.__register__.RegisterNodes(self,bRegAsRoot=True)
            
            oDataColl=self.GetReg('dataCollector')
            self.LinkRegisteredNode(oRoot,oDataColl)
            
            oPurchase=self.GetReg('purchase')
            self.LinkRegisteredNode(oRoot,oPurchase)
            oPurchaseDepartment=self.GetReg('purchaseDepartment')
            self.LinkRegisteredNode(oRoot,oPurchaseDepartment)
            self.LinkRegisteredNode(oPurchaseDepartment,oDataColl)
            
            oDocs=self.GetReg('Documents')
            self.LinkRegisteredNode(oRoot,oDocs)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'purchases')
    #def New(self,revision='1.0',root='purchaseroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'purchases')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtXmlDomReg.Build(self)
        self.BuildStateMachine()
        self.GenerateSortedPurchaseNrIds()
    def BuildStateMachine(self):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
    def BuildSrv(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.BuildStateMachine()
        self.GenerateSortedPurchaseNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def GenerateSortedPurchaseNrIds(self):
        self.purchaseNr=self.getSortedNodeIdsRec([],
                        'purchase','purchaseID',None)
    def GetSortedPurchaseNrIds(self):
        try:
            return self.purchaseNr
        except:
            self.GenerateSortedPurchaseNrIds()
            return self.purchaseNr
