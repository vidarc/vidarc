#Boa:MDIChild:vPurchaseMDIFrame
#----------------------------------------------------------------------------
# Name:         vPurchaseMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060622
# CVS-ID:       $Id: vPurchaseMDIFrame.py,v 1.6 2007/10/02 03:36:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vPurchase.images as imgPurchase
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPurchase.vPurchasePanel import *

def getPluginImage():
    return imgPurchase.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPurchase.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vPurchaseMDIFrame(parent)

DOMAINS=['finances','vPurchase']
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

[wxID_VPURCHASEMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vPurchaseMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=-1,
              name=u'vPurchaseMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vPurchase')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Purchase')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vPurchasePanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vPurchasePlugIn')
            self.pn.OpenCfgFile('vPurchaseCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
            
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        #self.Layout()
        #self.Refresh()
    def __getPluginImage__(self):
        return imgPurchase.getPluginImage()
    def __getPluginBitmap__(self):
        return imgPurchase.getPluginBitmap()
