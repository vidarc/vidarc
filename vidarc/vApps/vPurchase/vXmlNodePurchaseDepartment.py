#----------------------------------------------------------------------------
# Name:         vXmlNodePurchaseDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060727
# CVS-ID:       $Id: vXmlNodePurchaseDepartment.py,v 1.2 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPurchase.vXmlNodePurchaseDepartmentPanel import *
        #from vidarc.vApps.vPurchase.vXmlNodePurchaseDepartmentEditDialog import *
        #from vidarc.vApps.vPurchase.vXmlNodePurchaseDepartmentAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodePurchaseDepartment(vtXmlNodeTag):
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='purchaseDepartment'):
        global _
        _=vtLgBase.assignPluginLang('vPurchase')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'purchase department')
    # ---------------------------------------------------------
    # specific
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smPurchase')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa0IDAT8\x8d\xbd\x92\xcb\r\x840\x0cD\xc7\x0e\xed\xb0}pG \xd1\xc8\
\xd6\x80\xb6\x0f$\x10w\n\xa1\x1e>\x87UP\x08\xb6\x93\x13s\x8b\x93\x99yJB\x9f_\
{ \xa1\xf5;\x93\xb6\xc7\xd2p\xe9F,\xddx\xad\xcb\xbeVKn\x01\xb11'\xa4\x90\x0e\
{UCk\xe2\xdf\x024\xf3\xb4o\x8f\xe6\x86\xdd\x15\xcaR\xbbe\x06\x80pN\xd2+\xc4\
\xe6\xb01\x0ch\xd8Q\x11\x1b-\\iF\xc4\x0e\xc0\xff\x96-\xecd@\xac\x10\xd3\n{\
\x04\xbcN ~\xe5\x14\xd1\xb4o\x87\x9f\xab\x04\x9a\xd9\xcb\x93\x99\x04\x1a~\
\xd6\x1d\xe4\xea\x04\xe9\x1f[\xc9\xa0pb\xc1\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetEditDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePurchaseDepartmentPanel
        else:
            return None

