#Boa:FramePanel:vXmlNodePurchasePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePurchasePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodePurchasePanel.py,v 1.5 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputUnique
import vidarc.tool.input.vtInputMultipleTree
import vidarc.tool.input.vtInputDistributeTree
import vidarc.vApps.vPrj.vXmlPrjInputTreePrjsPercent
import vidarc.vApps.vContact.vXmlContactInputTree
import vidarc.vApps.vContact.vXmlContactInputTreeSupplier
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputMultipledValues
import vidarc.vApps.vPrj.vXmlPrjInputTree
import vidarc.ext.state.veStateInput
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeDateGM
import vidarc.vApps.vSupplier.vXmlSupplierInputTree
import vidarc.vApps.vHum.vXmlHumInputTreeUsr

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEPURCHASEPANEL, wxID_VXMLNODEPURCHASEPANELLBLCURRENCY, 
 wxID_VXMLNODEPURCHASEPANELLBLDELIVERY, wxID_VXMLNODEPURCHASEPANELLBLID, 
 wxID_VXMLNODEPURCHASEPANELLBLORDERDT, wxID_VXMLNODEPURCHASEPANELLBLPRJ, 
 wxID_VXMLNODEPURCHASEPANELLBLSTATE, wxID_VXMLNODEPURCHASEPANELLBLSUM, 
 wxID_VXMLNODEPURCHASEPANELLBLSUP, wxID_VXMLNODEPURCHASEPANELLBLSUPADR, 
 wxID_VXMLNODEPURCHASEPANELLBLSUPCONTACT, wxID_VXMLNODEPURCHASEPANELLBLUSR, 
 wxID_VXMLNODEPURCHASEPANELTXTSUM, wxID_VXMLNODEPURCHASEPANELVICONTACT, 
 wxID_VXMLNODEPURCHASEPANELVICURRENCY, wxID_VXMLNODEPURCHASEPANELVIDTDELIVERY, 
 wxID_VXMLNODEPURCHASEPANELVIDTORDER, wxID_VXMLNODEPURCHASEPANELVIORDERING, 
 wxID_VXMLNODEPURCHASEPANELVIPRJ, wxID_VXMLNODEPURCHASEPANELVIPURID, 
 wxID_VXMLNODEPURCHASEPANELVISTATE, wxID_VXMLNODEPURCHASEPANELVISUPPDEP, 
 wxID_VXMLNODEPURCHASEPANELVISUPPLIER, 
] = [wx.NewId() for _init_ctrls in range(23)]

class vXmlNodePurchasePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsNum_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblId, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viPurId, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsState_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblState, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viState, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viOrdering, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblOrderDt, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viDtOrder, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDelivery, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viDtDelivery, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsPrj_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrj, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viPrj, 7, border=0, flag=wx.EXPAND)

    def _init_coll_bxsSup_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSup, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSupplier, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblSupAdr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSuppDep, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblSupContact, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viContact, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrj, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsUsr, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsSup, 0, border=8, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsCurrency, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsNum, 0, border=4, flag=wx.EXPAND | wx.BOTTOM)
        parent.AddSizer(self.bxsState, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsCurrency_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSum, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtSum, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCurrency, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viCurrency, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=8, vgap=0)

        self.bxsUsr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSup = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsState = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPrj = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCurrency = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsNum = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsUsr_Items(self.bxsUsr)
        self._init_coll_bxsSup_Items(self.bxsSup)
        self._init_coll_bxsState_Items(self.bxsState)
        self._init_coll_bxsPrj_Items(self.bxsPrj)
        self._init_coll_bxsCurrency_Items(self.bxsCurrency)
        self._init_coll_bxsNum_Items(self.bxsNum)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vXmlNodePurchasePanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(456, 287),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(448, 260))
        self.SetAutoLayout(True)

        self.lblPrj = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLPRJ,
              label=_(u'project'), name=u'lblPrj', parent=self, pos=wx.Point(0,
              0), size=wx.Size(52, 110), style=wx.ALIGN_RIGHT)

        self.viPrj = vidarc.vApps.vPrj.vXmlPrjInputTreePrjsPercent.vXmlPrjInputTreePrjsPercent(id=wxID_VXMLNODEPURCHASEPANELVIPRJ,
              name=u'viPrj', parent=self, pos=wx.Point(56, 0), size=wx.Size(392,
              110), style=0)

        self.lblUsr = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLUSR,
              label=_(u'ordering person'), name=u'lblUsr', parent=self,
              pos=wx.Point(0, 114), size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize((-1,-1))

        self.viOrdering = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODEPURCHASEPANELVIORDERING,
              name=u'viOrdering', parent=self, pos=wx.Point(56, 114),
              size=wx.Size(56, 24), style=0)

        self.lblSup = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLSUP,
              label=_(u'supplier'), name=u'lblSup', parent=self, pos=wx.Point(0,
              146), size=wx.Size(52, 26), style=wx.ALIGN_RIGHT)
        self.lblSup.SetMinSize((-1,-1))

        self.lblSupAdr = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLSUPADR,
              label=_(u'address'), name=u'lblSupAdr', parent=self,
              pos=wx.Point(112, 146), size=wx.Size(52, 26),
              style=wx.ALIGN_RIGHT)

        self.viSupplier = vidarc.vApps.vContact.vXmlContactInputTreeSupplier.vXmlContactInputTreeSupplier(id=wxID_VXMLNODEPURCHASEPANELVISUPPLIER,
              name=u'viSupplier', parent=self, pos=wx.Point(56, 146),
              size=wx.Size(56, 26), style=0)
        self.viSupplier.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViSupplierVtinputTreeChanged,
              id=wxID_VXMLNODEPURCHASEPANELVISUPPLIER)

        self.viSuppDep = vidarc.vApps.vContact.vXmlContactInputTreeSupplier.vXmlContactInputTreeSupplier(id=wxID_VXMLNODEPURCHASEPANELVISUPPDEP,
              parent=self, pos=wx.Point(168, 146), size=wx.Size(112, 26),
              style=0)
        self.viSuppDep.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViSuppDepVtinputTreeChanged,
              id=wxID_VXMLNODEPURCHASEPANELVISUPPDEP)

        self.viContact = vidarc.vApps.vContact.vXmlContactInputTree.vXmlContactInputTree(id=wxID_VXMLNODEPURCHASEPANELVICONTACT,
              name=u'viContact', parent=self, pos=wx.Point(336, 146),
              size=wx.Size(112, 26), style=0)

        self.lblSupContact = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLSUPCONTACT,
              label=_(u'contact'), name=u'lblSupContact', parent=self,
              pos=wx.Point(280, 146), size=wx.Size(52, 26),
              style=wx.ALIGN_RIGHT)

        self.lblOrderDt = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLORDERDT,
              label=_(u'ordering date'), name=u'lblOrderDt', parent=self,
              pos=wx.Point(112, 114), size=wx.Size(52, 24),
              style=wx.ALIGN_RIGHT)
        self.lblOrderDt.SetMinSize((-1,-1))

        self.viDtOrder = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEPURCHASEPANELVIDTORDER,
              name=u'viDtOrder', parent=self, pos=wx.Point(168, 114),
              size=wx.Size(112, 24), style=0)

        self.lblDelivery = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLDELIVERY,
              label=_(u'delivery'), name=u'lblDelivery', parent=self,
              pos=wx.Point(280, 114), size=wx.Size(52, 24),
              style=wx.ALIGN_RIGHT)
        self.lblDelivery.SetMinSize((-1,-1))

        self.viDtDelivery = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEPURCHASEPANELVIDTDELIVERY,
              name=u'viDtDelivery', parent=self, pos=wx.Point(336, 114),
              size=wx.Size(112, 24), style=0)

        self.lblCurrency = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLCURRENCY,
              label=_(u'currency'), name=u'lblCurrency', parent=self,
              pos=wx.Point(280, 176), size=wx.Size(52, 21),
              style=wx.ALIGN_RIGHT)
        self.lblCurrency.SetMinSize(wx.Size(-1, -1))

        self.viCurrency = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEPURCHASEPANELVICURRENCY,
              name=u'viCurrency', parent=self, pos=wx.Point(336, 176),
              size=wx.Size(112, 21), style=0)

        self.lblId = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLID,
              label=_(u'purchase Nr'), name=u'lblId', parent=self, pos=wx.Point(0,
              209), size=wx.Size(52, 21), style=wx.ALIGN_RIGHT)

        self.viPurId = vidarc.tool.input.vtInputUnique.vtInputUnique(id=wxID_VXMLNODEPURCHASEPANELVIPURID,
              name=u'viPurId', parent=self, pos=wx.Point(56, 209),
              size=wx.Size(392, 21), style=0)

        self.lblState = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLSTATE,
              label=_(u'state'), name=u'lblState', parent=self, pos=wx.Point(0,
              238), size=wx.Size(52, 22), style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize((-1,-1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VXMLNODEPURCHASEPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(56, 238),
              size=wx.Size(392, 22), style=0)

        self.lblSum = wx.StaticText(id=wxID_VXMLNODEPURCHASEPANELLBLSUM,
              label=_(u'sum'), name=u'lblSum', parent=self, pos=wx.Point(0,
              176), size=wx.Size(52, 21), style=wx.ALIGN_RIGHT)

        self.txtSum = wx.TextCtrl(id=wxID_VXMLNODEPURCHASEPANELTXTSUM,
              name=u'txtSum', parent=self, pos=wx.Point(56, 176),
              size=wx.Size(224, 21), style=wx.TE_READONLY | wx.TE_RIGHT,
              value=u'')
        self.txtSum.Enable(False)
        self.txtSum.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPurchase')
        self._init_ctrls(parent, id)
        self.bSupFirst=False
        
        vtXmlNodePanel.__init__(self,applName=_(u'vPurchase')+' '+_(u'purchase'),
                lWidgets=[],
                lEvent=[])
        self.viOrdering.SetTagNames('orderBy','name')
        self.viSupplier.SetTagNames('supplier','tag')
        self.viSuppDep.SetTagNames('supplierDepart','tag')
        self.viContact.SetTagNames('contact','tag')
        self.viDtOrder.SetTagName('orderDt')
        self.viDtDelivery.SetTagName('deliveryDt')
        self.viCurrency.SetTagName('currency')
        self.viState.SetNodeAttr('purchase','purState')
        self.viPurId.SetTagName('purchaseID')
        #self.viSupAdr.EnableEdit(False)
        #self.viSupContact.EnableEdit(False)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.bSupFirst=False
            self.viPrj.Clear()
            self.viOrdering.Clear()
            self.viSupplier.Clear()
            self.viSuppDep.Clear()
            self.viContact.Clear()
            #self.viSupAdr.Clear()
            #self.viSupContact.Clear()
            self.viDtOrder.Clear()
            self.viDtDelivery.Clear()
            self.viPurId.Clear()
            try:
                docGlb=self.doc.GetNetDoc('vGlobals')
                sCur=docGlb.GetGlobal(['vPurchase','purchase'],'globalsPurchase','currency')
                self.viCurrency.SetValue(sCur)
            except:
                self.viCurrency.SetValue('')
            self.viCurrency.SetModified(False)
            self.viState.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viOrdering.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vContact'):
            dd=d['vContact']
            self.viSupplier.SetDocTree(dd['doc'],dd['bNet'])
            self.viSuppDep.SetDocTree(dd['doc'],dd['bNet'])
            #self.viSupAdr.SetDoc(dd['doc'])
            self.viContact.SetDocTree(dd['doc'],dd['bNet'])
            #self.viSupContact.SetDoc(dd['doc'])
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.viPrj.SetDocTree(dd['doc'],dd['bNet'])
        
    def SetDependentPartsPanel(self,pn):
        pn.BindPartsChanged(self.OnPartsChanged)
    def OnPartsChanged(self,evt):
        evt.Skip()
        self.viState.SetNode(self.node)
        self.viState.Enable(False)
        pn=self.GetParent().GetPanelByName('purchaseParts')
        #print pn.GetSum()
        self.txtSum.SetValue(pn.GetSum())
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            t=(self.viPrj.IsModified(),self.viOrdering.IsModified(),
                    self.viSupplier.IsModified(),
                    self.viSuppDep.IsModified(),
                    self.viDtOrder.IsModified(),
                    self.viDtDelivery.IsModified(),
                    self.viCurrency.IsModified(),
                    self.viPurId.IsModified())
            vtLog.vtLngCurWX(vtLog.DEBUG,'mod viPrj:  %d;viOrdering:%d;viSupplier:%d;viSuppDep:%d;viDtOrder:%d;'\
                        'viDtDelivery:%d;viCurrency:%d;viPurId:%d'%t,self)
        if self.viPrj.IsModified():
            return True
        if self.viOrdering.IsModified():
            return True
        if self.viSupplier.IsModified():
            return True
        if self.viSuppDep.IsModified():
            return True
        if self.viDtOrder.IsModified():
            return True
        if self.viDtDelivery.IsModified():
            return True
        if self.viCurrency.IsModified():
            return True
        if self.viPurId.IsModified():
            return True
        #if self.viState.IsModified():
        #    return True
        return False

    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viPrj.SetDoc(self.doc)
        self.viOrdering.SetDoc(self.doc)
        self.viSupplier.SetDoc(self.doc)
        self.viSuppDep.SetDoc(self.doc)
        self.viContact.SetDoc(self.doc)
        #self.viSupAdr.SetDoc(self.doc)
        #self.viSupContact.SetDoc(dd['doc'])
        self.viDtOrder.SetDoc(self.doc)
        self.viDtDelivery.SetDoc(self.doc)
        self.viCurrency.SetDoc(self.doc)
        self.viPurId.SetDoc(self.doc)
        self.viPurId.SetUniqueFunc(self.doc.GetSortedPurchaseNrIds)
        self.viState.SetDoc(self.doc)
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.bSupFirst=True
            self.viPrj.SetNode(self.node)
            self.viOrdering.SetNode(self.node)
            self.viSupplier.SetNode(self.node)
            suppFid=self.objRegNode.GetSupplier(self.node)
            id,appl=self.doc.convForeign(suppFid)
            netSup,nodeSup=self.doc.GetNode(suppFid)
            self.viSuppDep.SetNodeTree(nodeSup)
            self.viSuppDep.SetNode(self.node)
            self.viContact.SetNodeTree(nodeSup)
            self.viContact.SetNode(self.node)
            self.viDtOrder.SetNode(self.node)
            self.viDtDelivery.SetNode(self.node)
            self.viCurrency.SetNode(self.node)
            self.viPurId.SetNode(self.node)
            self.viState.SetNode(self.node)
            self.txtSum.SetValue(self.objRegNode.GetSum(self.node))
            
            sCur=self.viCurrency.GetValue()
            if len(sCur)==0:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCur=docGlb.GetGlobal(['vPurchase','purchase'],'globalsPurchase','currency')
                    self.viCurrency.SetValue(sCur)
                    #self.viCurrency.__markModified__()
                    self.viCurrency.SetModified(True)
                except:
                    pass
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viPrj.GetNode(node)
            self.viOrdering.GetNode(node)
            self.viSupplier.GetNode(node)
            self.viSuppDep.GetNode(node)
            self.viContact.GetNode(node)
            #self.objRegNode.SetSupplierAddress(node,self.viSupAdr.GetValue())
            #self.objRegNode.SetSupplierContact(node,self.viContact.GetForeignID())
            #self.objRegNode.SetSupplierContact(node,self.viSupContact.GetValue())
            #self.viContact.__markModified__(False)
            
            self.viDtOrder.GetNode(node)
            self.viDtDelivery.GetNode(node)
            self.viCurrency.GetNode(node)
            self.viState.GetNode(node)
            self.viPurId.GetNode(node)
            self.viState.Enable(True)
            self.viState.SetNode(node)      # Refresh
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viPrj.Close()
        self.viOrdering.Close()
        self.viSupplier.Close()
        self.viSuppDep.Close()
        self.viContact.Close()
        self.viDtOrder.Close()
        self.viDtDelivery.Close()
        self.viState.Close()
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viPrj.Enable(False)
            self.viOrdering.Enable(False)
            self.viSupplier.Enable(False)
            self.viSuppDep.Enable(False)
            self.viContact.Enable(False)
            self.viDtOrder.Enable(False)
            self.viDtDelivery.Enable(False)
            self.viCurrency.Enable(False)
            self.viPurId.Enable(False)
            self.viState.Enable(False)
        else:
            # add code here
            self.viPrj.Enable(True)
            self.viOrdering.Enable(True)
            self.viSupplier.Enable(True)
            self.viContact.Enable(True)
            self.viDtOrder.Enable(True)
            self.viDtDelivery.Enable(True)
            self.viCurrency.Enable(True)
            self.viPurId.Enable(True)
            self.viState.Enable(True)
    
    def __refreshSupplier__(self,fid):
        self.viSuppDep.Clear()
        self.viContact.Clear()
        netSup,nodeSup=self.doc.GetNode(fid)
        if nodeSup is not None:
            self.viSuppDep.SetNodeTree(nodeSup)
            self.viContact.SetNodeTree(nodeSup)
            #self.viCustomer.Enable(True)
    def OnViSupplierVtinputTreeChanged(self, event):
        event.Skip()
        #fid='%s@vContact'%event.GetId()
        fid='@'.join([event.GetId(),'vContact'])
        suppFid=self.objRegNode.GetSupplier(self.node)
        if fid!=suppFid:
            self.__refreshSupplier__(fid)
        return
        netDoc,nodeSup=self.doc.GetNode('%s@vContact'%fid)
        self.viContact.SetNodeTree(nodeSup)
        if self.bSupFirst:
            sSupAdr=self.objRegNode.GetSupplierAddress(self.node)
            sSupCont=self.objRegNode.GetSupplierContact(self.node)
        else:
            sSupAdr=None
            sSupCont=None
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s;blocked:%d;first:%d;adr:%s;contact:%s'%(fid,
                    self.IsBlocked(),self.bSupFirst,sSupAdr,sSupCont),self)
        self.viSupAdr.SetNode(netDoc.getChild(nodeSup,'addresses'),sSupAdr)
        if sSupCont is not None:
            id,appl=self.doc.convForeign(sSupCont)
            self.viContact.SetValueID('',id)
        #self.viSupContact.SetNode(netDoc.getChild(nodeSup,'contacts'),sSupCont)
        
        self.bSupFirst=False

    def OnViSuppDepVtinputTreeChanged(self, event):
        event.Skip()
        fid='@'.join([event.GetId(),'vContact'])
        if fid!=self.objRegNode.GetSupplierDepartment(self.node):
            self.viContact.Clear()
            netSupDep,nodeSupDep=self.doc.GetNode(fid)
            if nodeSupDep is not None:
                self.viContact.SetNodeTree(nodeSupDep)
