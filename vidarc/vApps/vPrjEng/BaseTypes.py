#----------------------------------------------------------------------------
# Name:         BaseTypes.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: BaseTypes.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import string,copy

def getVal(d,k,dftVal):
        sType=d['type']
        try:
            if sType=='int':
                return int(d[k])
            elif sType=='long':
                return long(d[k])
            elif sType=='float':
                return float(d[k])
            elif sType=='pseudofloat':
                return float(d[k])
            elif sType=='text':
                return d[k]
            elif sType=='string':
                return d[k]
            elif sType=='choice':
                return d[k]
            elif sType=='choiceint':
                return int(d[k])
            elif sType=='datetime':
                return d[k]
            else:
                return d[k]
        except:
            return dftVal
def getFormat(d):
        sType=d['type']
        try:
            if sType=='int':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='long':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='float':
                try:
                    #print 'float',d
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    #print sd,sc
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='pseudofloat':
                try:
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='text':
                return "%s"
            elif sType=='string':
                return "%s"
            elif sType=='choice':
                return "%s"
            elif sType=='choiceint':
                return "%d"
            elif sType=='datetime':
                return "%s"
            else:
                return "%s"
        except:
            return "%s"
    
def setVal(d,k,val,dftVal):
        sType=d['type']
        try:
            if sType=='int':
                d[k]=getFormat(d)%(val)
            elif sType=='long':
                d[k]=getFormat(d)%(val)
            elif sType=='float':
                d[k]=getFormat(d)%(val)
            elif sType=='pseudofloat':
                d[k]=getFormat(d)%(val)
            elif sType=='text':
                d[k]=__str__(val)
            elif sType=='string':
                d[k]=__str__(val)
            elif sType=='choice':
                d[k]=__str__(val)
            elif sType=='choiceint':
                d[k]=getFormat(d)%val
            elif sType=='datetime':
                d[k]=val
            else:
                d[k]=val
        except:
            d[k]=dftVal
def getValues(d):
        lst=[]
        sType=d['type']
        if sType in ['int','long','float','pseudofloat']:
            lst.append(('min','0'))
            lst.append(('max','100'))
            lst.append(('val','0'))
        elif sType in ['text','string','choice']:
            lst.append(('val',''))
        elif sType=='choiceint':
            lst.append(('val','0'),('min','0'),('max','100'))
        elif sType=='datetime':
            lst.append(('val',''))
        else:
            pass
        return lst
def cpValues(oldDict,newDict):
        lst=getValues(oldDict)
        for k,v in lst:
            try:
                setVal(newDict,k,getVal(oldDict,k,v),v)
            except:
                pass
def setupType(dictAttributes):
    if dictAttributes.has_key('inheritance_type'):
        inherDict=dictAttributes['inheritance_type']
        oType=inherDict['type']
        sType=dictAttributes['type']['val']
        d=copy.deepcopy(inherDict)
        if sType=='int':
            d['type']='int'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='long':
            d['type']='long'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='float':
            d['type']='float'
            d['val']='0'
            d['min']='0'
            d['max']='100'
            d['digits']='6'
            d['comma']='2'
        elif sType=='pseudofloat':
            d['type']='pseudofloat'
            d['val']='0'
            d['min']='0'
            d['max']='100'
            d['digits']='6'
            d['comma']='2'
        elif sType=='text':
            d['type']='text'
            d['val']='0'
            d['len']='8'
        elif sType=='string':
            d['type']='string'
            d['val']='0'
            d['len']='8'
        elif sType=='choice':
            d['type']='choice'
            d['val']='0'
            #,'it00':'item  0','it01':'item  1',
            #        'it02':'item  2','it03':'item  3','it04':'item  4',
            #        'it05':'item  5','it06':'item  6','it07':'item  7',
            #        'it08':'item  8','it09':'item  9','it10':'item 10',}
        elif sType=='choiceint':
            d['type']='choice'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='datetime':
            d['type']='datetime'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        else:
            pass
        cpValues(inherDict,d)
        dictAttributes['inheritance_type']=d
        keys=dictAttributes.keys()
        for k in keys:
            try:
                s=dictAttributes[k]['inheritance']
                strs=string.split(s,',')
                for s in strs:
                    if s=='type':
                        val=getVal(self.dictAttributes[k],'val','0')
                        dictAttributes[k]['type']=sType
                        setVal(dictAttributes[k],'val',val,'0')
                        #val=self.getVal(self.dictAttributes[k],'val','0')        
            except:
                pass
        #self.__addElementAttr__()
    return dictAttributes
    