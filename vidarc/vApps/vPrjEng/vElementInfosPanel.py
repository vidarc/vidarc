#Boa:FramePanel:vElementInfosPanel
#----------------------------------------------------------------------------
# Name:         vElementInfosPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElementInfosPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import sys,string
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import vidarc.vApps.vPrjEng.gridAttr as gridAttr

import images

[wxID_VELEMENTINFOSPANEL, wxID_VELEMENTINFOSPANELGCBBAPPLY, 
 wxID_VELEMENTINFOSPANELGCBBCANCEL, wxID_VELEMENTINFOSPANELLBLMODIFIED, 
] = [wx.NewId() for _init_ctrls in range(4)]

# defined event for vgpXmlTree item selected
wxEVT_ELEMENT_INFOS_CHANGED=wx.NewEventType()
def EVT_ELEMENT_INFOS_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_ELEMENT_INFOS_CHANGED,func)
class vgpElementInfosChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_ELEMENT_INFOS_CHANGED(<widget_name>, self.OnElementInfosChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_ELEMENT_INFOS_CHANGED)
        self.obj=obj
        self.node=node
    def GetElementInfos(self):
        return self.obj
    def GetNode(self):
        return self.node

class vElementInfosPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VELEMENTINFOSPANEL,
              name=u'vElementInfosPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(600, 300), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(592, 273))
        self.SetAutoLayout(True)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTINFOSPANELGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(200, 235), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.SetConstraints(LayoutAnchors(self.gcbbApply, True, False,
              False, True))
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VELEMENTINFOSPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTINFOSPANELGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(290, 235),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetConstraints(LayoutAnchors(self.gcbbCancel, True,
              False, False, True))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VELEMENTINFOSPANELGCBBCANCEL)

        self.lblModified = wx.StaticText(id=wxID_VELEMENTINFOSPANELLBLMODIFIED,
              label=u'', name=u'lblModified', parent=self, pos=wx.Point(56,
              240), size=wx.Size(0, 13), style=0)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.selElem=None
        self.lang=None
        self.selAttrIdx=-1
        self.bModified=False
        self.lstFilter=[]
        #self.lstDocs.InsertColumn(0,u'Doc')
        self.gdAttr=gridAttr.gridAttrGrid(self,wx.NewId(),pos=wx.Point(4,4),
            size=wx.Size(580,220))
        self.gdAttr.SetConstraints(LayoutAnchors(self.gdAttr, True,
              True, True, True))
        gridAttr.EVT_GRIDATTR_INFO_CHANGED(self.gdAttr, self.OnInfoChanged)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
                
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def SetFilter(self,lst):
        self.lstFilter=lst
    def __setModified__(self,state):
        self.bModified=state
        if self.bModified:
            self.lblModified.SetLabel('*')
        else:
            self.lblModified.SetLabel('')
    def SetNode(self,node,lst,doc=None):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you realy want to loose modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_NO:
                self.GetNode()
        self.doc=doc
        self.node=node
        self.bModified=False
        self.selElem=None
        self.selAttrIdx=-1
        self.lstNodes=lst
        oldGrid=self.gdAttr
        
        self.gdAttr=gridAttr.gridAttrGrid(self,wx.NewId(),pos=wx.Point(4,4),
            size=oldGrid.GetSize())
        self.gdAttr.SetConstraints(LayoutAnchors(self.gdAttr, True,
              True, True, True))
        gridAttr.EVT_GRIDATTR_INFO_CHANGED(self.gdAttr, self.OnInfoChanged)
        
        self.gdAttr.Setup(node,lst,self.lstFilter)
        
        oldGrid.Destroy()
        
        self.__setModified__(False)
        
                
    def SetXmlOld(self,node,doc=None):
        self.doc=doc
        self.node=node
        self.bModified=False
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtAttrName.SetValue('')
        self.selElem=None
        self.selAttrIdx=-1
        if node is not None:
            self.__updatePosElem__(node.tagName)
            self.txtType.SetValue(node.tagName)
            s=vtXmlDomTree.getNodeText(node,'tag')
            self.txtTagname.SetValue(s)
            s=vtXmlDomTree.getNodeText(node,'name')
            self.txtName.SetValue(s)
            s=vtXmlDomTree.getNodeText(node,'description')
            self.txtDesc.SetValue(s)
            
            nodes=vtXmlDomTree.getChilds(node)
            self.lstAttr.DeleteAllItems()
            if self.selElem is None:
                return
            d=self.selElem[1]
            keys=d.keys()
            #keys.sort()
            for n in nodes:
                if n.nodeType==Element.ELEMENT_NODE:
                    id=self.doc.getKey(n)
                    if self.doc.IsKeyValid(id):
                        continue
                    tag=n.tagName
                    if tag in ['tag','name','description']:
                        continue
                    if tag[:2]=='__':
                        continue
                    sVal=vtXmlDomTree.getText(n)
                else:
                    continue
                i=self.lstAttr.FindItem(-1,tag,False)
                if i<0:
                    #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
                    index = self.lstAttr.InsertImageStringItem(sys.maxint, tag, -1)
                    self.lstAttr.SetStringItem(index,1,sVal,-1)
                else:
                    self.lstAttr.SetStringItem(i,1,sVal,-1)
                    #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
            self.lstElement.Enable(False)
        else:
            self.txtType.SetValue('')
            self.lstElement.Enable(True)
        #self.elements=[]
    def GetNode(self):
        self.gdAttr.Get()
        self.__setModified__(False)
        
    def GetXmlOld(self,node,doc=None):
        if self.selElem is None:
            return None
        n=doc.createElement(self.selElem[0])
        #n.setAttribute('id',0)
        nv=doc.createElement('tag')
        nv.appendChild(doc.createTextNode(self.txtTagname.GetValue()))
        n.appendChild(nv)
        
        nv=doc.createElement('name')
        nv.appendChild(doc.createTextNode(self.txtName.GetValue()))
        n.appendChild(nv)
        
        nv=doc.createElement('description')
        nv.appendChild(doc.createTextNode(self.txtDesc.GetValue()))
        n.appendChild(nv)
        
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i,0)
            sAttr=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sVal=it.m_text
            nv=doc.createElement(sAttr)
            nv.appendChild(doc.createTextNode(sVal))
            n.appendChild(nv)
        return n
    def OnInfoChanged(self,evt):
        self.__setModified__(True)
        pass
    
    def OnGcbbAddButton(self, event):
        sAttr=self.txtAttrName.GetValue()
        sVal=self.txtAttrVal.GetValue()
        i=self.lstAttr.FindItem(-1,sAttr,False)
        if i<0:
            self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttr, -1)
            self.lstAttr.SetStringItem(index,1,sVal,-1)
        else:
            self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        
        event.Skip()

    def OnGcbbApplyButton(self, event):
        self.GetNode()
        wx.PostEvent(self,vgpElementInfosChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.gdAttr.Set()
        self.__setModified__(False)
        event.Skip()
