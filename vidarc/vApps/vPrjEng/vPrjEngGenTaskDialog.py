#Boa:Dialog:vPrjEngGenTaskDialog
#----------------------------------------------------------------------------
# Name:         vPrjEngGenTaskDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngGenTaskDialog.py,v 1.3 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.InOut.fnUtil as fnUtil
import codecs,os.path

import images

def create(parent):
    return vPrjEngGenTaskDialog(parent)

[wxID_VPRJENGGENTASKDIALOG, wxID_VPRJENGGENTASKDIALOGGCBBBROWSE, 
 wxID_VPRJENGGENTASKDIALOGGCBBCANCEL, wxID_VPRJENGGENTASKDIALOGGCBBGENERATE, 
 wxID_VPRJENGGENTASKDIALOGLBLCLIENT, wxID_VPRJENGGENTASKDIALOGLBLFILENAME, 
 wxID_VPRJENGGENTASKDIALOGLBLPRJLONG, wxID_VPRJENGGENTASKDIALOGLBLPRJSHORT, 
 wxID_VPRJENGGENTASKDIALOGTXTCLIENT, wxID_VPRJENGGENTASKDIALOGTXTPRJLONG, 
 wxID_VPRJENGGENTASKDIALOGTXTPRJSHORT, wxID_VPRJENGGENTASKDIALOGTXTPRJTASKFN, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vPrjEngGenTaskDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJENGGENTASKDIALOG,
              name=u'vPrjEngGenTaskDialog', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(451, 210), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Generate Tasks')
        self.SetClientSize(wx.Size(443, 183))

        self.gcbbCancel = wxGenBitmapTextButton(ID=wxID_VPRJENGGENTASKDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 144),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJENGGENTASKDIALOGGCBBCANCEL)

        self.lblFileName = wx.StaticText(id=wxID_VPRJENGGENTASKDIALOGLBLFILENAME,
              label=u'Projects Task Filename', name=u'lblFileName', parent=self,
              pos=wx.Point(8, 94), size=wx.Size(110, 13), style=0)

        self.txtPrjTaskFN = wx.TextCtrl(id=wxID_VPRJENGGENTASKDIALOGTXTPRJTASKFN,
              name=u'txtPrjTaskFN', parent=self, pos=wx.Point(8, 110),
              size=wx.Size(336, 21), style=wx.TE_RICH2, value=u'')
        self.txtPrjTaskFN.Enable(True)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGGENTASKDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 146),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VPRJENGGENTASKDIALOGGCBBGENERATE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGGENTASKDIALOGGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(360, 104),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VPRJENGGENTASKDIALOGGCBBBROWSE)

        self.lblPrjShort = wx.StaticText(id=wxID_VPRJENGGENTASKDIALOGLBLPRJSHORT,
              label=u'Project Shortcut', name=u'lblPrjShort', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(76, 13), style=0)

        self.txtPrjShort = wx.TextCtrl(id=wxID_VPRJENGGENTASKDIALOGTXTPRJSHORT,
              name=u'txtPrjShort', parent=self, pos=wx.Point(8, 24),
              size=wx.Size(72, 21), style=0, value=u'')
        self.txtPrjShort.Enable(False)

        self.lblPrjLong = wx.StaticText(id=wxID_VPRJENGGENTASKDIALOGLBLPRJLONG,
              label=u'Longname', name=u'lblPrjLong', parent=self,
              pos=wx.Point(112, 8), size=wx.Size(50, 13), style=0)

        self.txtPrjLong = wx.TextCtrl(id=wxID_VPRJENGGENTASKDIALOGTXTPRJLONG,
              name=u'txtPrjLong', parent=self, pos=wx.Point(112, 24),
              size=wx.Size(320, 21), style=0, value=u'')
        self.txtPrjLong.Enable(False)

        self.txtClient = wx.TextCtrl(id=wxID_VPRJENGGENTASKDIALOGTXTCLIENT,
              name=u'txtClient', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(72, 21), style=0, value=u'')

        self.lblClient = wx.StaticText(id=wxID_VPRJENGGENTASKDIALOGLBLCLIENT,
              label=u'Client', name=u'lblClient', parent=self, pos=wx.Point(8,
              48), size=wx.Size(26, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.netPrjEng=None
        self.netTask=None
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
    def SetNode(self,netPrjEng,netTask):
        self.netPrjEng=netPrjEng
        self.netTask=netTask
        if self.netPrjEng is not None:
            node=self.netPrjEng.getChild(netPrjEng.getRoot(),'prjinfo')
        else:
            node=None
        if node is None:
            self.txtPrjShort.SetValue('')
            self.txtPrjLong.SetValue('')
            self.txtPrjTaskFN.SetValue('')
            self.txtClient.SetValue('')
        else:
            self.prjId=self.netPrjEng.getAttribute(node,'fid')
            sShort=self.netPrjEng.getNodeText(node,'name')
            sLong=self.netPrjEng.getNodeText(node,'longname')
            sClient=self.netPrjEng.getNodeText(node,'clientshort')
            self.txtPrjShort.SetValue(sShort)
            self.txtPrjLong.SetValue(sLong)
            self.txtClient.SetValue(sClient)
        self.nodeSettings=self.netPrjEng.getChild(self.netPrjEng.getRoot(),'settings')
        if self.nodeSettings is not None:
            sTaskFN=self.netPrjEng.getNodeText(self.nodeSettings,'tasksfn')
        else:
            sTaskFN=''
        
        self.txtPrjTaskFN.SetValue(sTaskFN)
        self.txtPrjTaskFN.SetStyle(0, len(sTaskFN), wx.TextAttr("BLACK", "WHITE"))
        
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
    def __getHierarchy__(self,baseNode,n):
        if baseNode is None:
            return ''
        if not self.netPrjEng.isSame(baseNode,n):
            return self.__getHierarchy__(baseNode,self.netPrjEng.getParent(n))+'<'+self.netPrjEng.getNodeText(n,'tag')+'>'#n.tagName
        else:
            return ''
    def __addAccount__(self,prjNode,tagName,doc):
        #taskIds=self.netTask.GetSortedSubTaskId()
        
        prjEngNode=self.netPrjEng.getChild(self.netPrjEng.getBaseNode(),tagName)
        accountNodes=self.netPrjEng.getChildsRec(prjEngNode,'account')
        for account in accountNodes:
            lst=[]
            accTasks=self.netPrjEng.getChilds(account,'account_task')
            for aTask in accTasks:
                accSubTasks=self.netPrjEng.getChilds(aTask,'account_subtask')
                lstSub=[]
                for aSubTask in accSubTasks:
                    sn=self.netPrjEng.getChild(aSubTask,'subtask')
                    if sn is not None:
                        if len(self.netPrjEng.getAttribute(sn,'ih'))>0:
                            val=self.netPrjEng.getText(sn)
                        else:
                            val=self.netPrjEng.getNodeText(sn,'val')
                        lstSub.append({'tag':'subtask',
                                'val':val,
                                'attr':('fid',self.netPrjEng.getAttribute(aSubTask,'id'))})
                #tn=vtXmlDomTree.getChild(aTask,'task')
                d={'tag':'name',
                    'val':Hierarchy.getTagNames(self.netPrjEng,prjEngNode,
                                    None,aTask)}#+vtXmlDomTree.getNodeText(tn,'val')
                    
                if len(lstSub)>0:
                    lstTmp=[d]+lstSub
                else:
                    lstTmp=[d]
                doc.createSubNodeDict(prjNode,{'tag':'task',
                            'attr':('fid',doc.getAttribute(aTask,'id')),
                            'lst':lstTmp})
    def OnGcbbGenerateButton(self, event):
        if self.nodeSettings is None:
            return
        sShort=self.txtPrjShort.GetValue()
        sLong=self.txtPrjLong.GetValue()
        sClient=self.txtClient.GetValue()
        
        sPrjTaskFN=self.txtPrjTaskFN.GetValue()
        if len(sShort)<=0 and len(sLong)<=0:
            return
        
        if len(sPrjTaskFN)<=0:
            sPrjTaskFN='???'
            self.txtPrjTaskFN.SetValue(sPrjTaskFN)
            self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("RED", "YELLOW"))
            return
        sFN=os.path.split(sPrjTaskFN)[1]
        sTmp=fnUtil.replaceSuspectChars(sFN)
        if sTmp!=sFN:
            self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("RED", "YELLOW"))
            return
        self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("BLACK", "WHITE"))
        fn=self.txtPrjTaskFN.GetValue()
        
        doc=vtXmlDom.vtXmlDom()
        try:
            doc.Open(fn)
        except:
            doc.New(root="account_tasks")
        root=doc.getRoot()
        prjNodes=self.netTask.getChilds(None,'prjtasks')
        bFound=False
        id=long(self.prjId)
        for prjNode in prjNodes:
            try:
                if long(self.netTask.getAttribute(prjNode,'fid'))==id:
                    bFound=True
                    break
            except:
                pass
        
        ids=doc.getIds()
        if bFound==False:
            prjNode=doc.createSubNode(root,'prjtasks')
            doc.setAttribute(prjNode,'fid',self.prjId)
            doc.AlignNode(prjNode)
            ids.CheckId(prjNode)
        ids.ProcessMissing()
        ids.ClearMissing()
        
        childs=doc.getChilds(prjNode,'tasks')
        for c in childs:
            doc.deleteNode(c,prjNode)
        # add accounts
        self.__addAccount__(prjNode,'instance',doc)
        
        doc.AlignDoc()
        doc.Save(fn)
        doc.Close()
        del doc
        self.netPrjEng.setNodeText(self.nodeSettings,'tasksfn',fn)
        self.netPrjEng.AlignNode(self.nodeSettings)
        self.EndModal(1)
        #self.EndModal(0)
        event.Skip()
    def old(self):
        prjNode=vtXmlDomTree.getChild(self.baseDoc,'project')
        accountNodes=prjNode.getElementsByTagName('account')
        if len(accountNodes)<1:
            return
        dlg = wxFileDialog(self, "Save File As", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wxSAVE)
        try:
            dlg.Centre()
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                doc=getDOMImplementation().createDocument(None, "account_tasks", None)
                #tasks=doc.createElement('account_tasks')
                #doc.documentElement.appendChild(tasks)
                lst=[]
                for account in accountNodes:
                    accTasks=vtXmlDomTree.getChilds(account,'account_task')
                    for aTask in accTasks:
                        accSubTasks=vtXmlDomTree.getChilds(aTask,'account_subtask')
                        lstSub=[]
                        for aSubTask in accSubTasks:
                            sn=vtXmlDomTree.getChild(aSubTask,'subtask')
                            if sn is not None:
                                lstSub.append({'tag':'subtask',
                                    'val':vtXmlDomTree.getNodeText(sn,'val'),
                                    'attr':('fid',aSubTask.getAttribute('id'))})
                        tn=vtXmlDomTree.getChild(aTask,'task')
                        if tn is not None:
                            if len(lstSub)>0:
                                d={'tag':'name',
                                  'val':self.__getHierarchy__(self.baseDoc,tn)+vtXmlDomTree.getNodeText(tn,'val')}
                                lst.append({'tag':'task',
                                    'attr':('fid',aTask.getAttribute('id')),
                                    'lst':[d]+lstSub})
                n=vtXmlDomTree.createChildByLst(doc.documentElement,'tasks',lst,doc)
                #doc.appendChild(n)
                
                fnUtil.shiftFile(filename)
                if doc.encoding is None:
                    e=u'ISO-8859-1'
                else:
                    e=doc.encoding
                f=codecs.open(filename,"w",e)
                doc.writexml(f,encoding=e)
                f.close()
                doc.unlink()
        finally:
            dlg.Destroy()
        
    def OnGcbbBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.txtPrjTaskFN.GetValue()
            #fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if len(fn)>0:
                dlg.SetFilename(fn)
            else:
                sShort=self.txtPrjShort.GetValue()
                dlg.SetFilename('tasks')
            #else:
            #    dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtPrjTaskFN.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
