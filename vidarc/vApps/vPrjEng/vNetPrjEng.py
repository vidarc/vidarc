#----------------------------------------------------------------------------
# Name:         vNetPrjEng.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vNetPrjEng.py,v 1.5 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPrjEng.vXmlPrjEng import vXmlPrjEng
from vidarc.tool.net.vNetXmlWxGui import *
import  vidarc.vApps.vPrjEng.Elements as Elements

class vNetPrjEng(vXmlPrjEng,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlPrjEng.__init__(self,attr,skip,synch=synch,verbose=verbose)
        vXmlPrjEng.SetFN(self,fn)
        #vXmlPrjEng.SetEventWidget(self,self)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlPrjEng.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlPrjEng.__stop__(self)
    #def __stop__(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlPrjEng.Stop(self)
    #def IsRunning(self):
    #    if vtLog.vtLngIsLogged(vtLog.INFO):
    #        vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlPrjEng.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlPrjEng.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlPrjEng.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Open',origin=self.appl)
        #vtLog.vtLogCallDepth(None,fn,verbose=1)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlPrjEng.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlPrjEng.Open(self,fn,True)
            #if iRet>=0:
            #    if silent==False:
            #        self.NotifyContent()
            #else:
            #    self.New()
        else:
            iRet=vXmlPrjEng.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
                if silent==False:
                    self.NotifyContent()
        return iRet
        
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlPrjEng.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlPrjEng.getChildByLst(self,node,lst)
    #def doEdit(self,node):
    #    type=self.getTagName(node)
    #    if Elements.isProperty(type,'isFull'):
    #        self.doEditFull(node)
    #    else:
    #        vNetXmlWxGui.doEdit(self,node)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vXmlPrjEng.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        # is this an tmpl node
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'delNode;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        childs=self.getChilds(node,'__node')
        if len(childs)>0:
            for c in childs:
                instNode=self.getNodeById(self.getAttribute(c,'iid'))
                if instNode is None:
                    continue
                vNetXmlWxGui.delNode(self,instNode)
                vXmlPrjEng.deleteNode(self,instNode)
        vNetXmlWxGui.delNode(self,node)
        vXmlPrjEng.deleteNode(self,node)
    def moveNode(self,par,node,notify=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'moveNode;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vXmlPrjEng.moveNode(self,par,node)
        if notify:
            vNetXmlWxGui.moveNode(self,par,node)
    def addNodeCorrectId(self,par,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNodeCorrectId;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.addNode(self,par,node)
    def updateNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'updateNode;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        #vXmlPrjEng.addNode(self,par,node)
        vNetXmlWxGui.doUpdate(self,node)
    def doEdit(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'doEdit;id:%s'%self.getKey(node),origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        if Elements.isProperty(node.name,'isFull'):
            self.doEditFull(node)
            return
        id=self.getKey(node)
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.cltSock is None:
            self.__setOffline__(node,bContent=True)
            # loop back
            self.NotifySetNode(id,id,'')
            return
        self.clearFingerPrint(node)
        if self.SINGLE_LOCK!=0:
            if len(self.idLock)<=0:
                return
            idLock=self.idLock
        else:
            if self.lock.HasLock(id)==False:
                return
            idLock=self.lock.GetLockId(id)
        self.cltSock.set_node(idLock,id)
        self.clearFingerPrint(node)
        self.GetFingerPrintAndStore(node)
    def NotifyOpenStartOld(self,fn):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifyOpenStart(self,fn)
        #if self.silent==False:
        #    self.NotifyContent()
        pass
    def NotifyOpenOkOld(self,fn):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifyOpenOk(self,fn)
        if self.silent==False:
            self.NotifyContent()
        pass
    def NotifyOpenFaultOld(self,fn,sExp):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifyOpenFault(self,fn,sExp)
        if self.silent==False:
            self.NotifyContent()
        pass
    def NotifySynchStartOld(self,count):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifySynchStart(self,count)
    def NotifySynchProcOld(self,act,count):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifySynchProc(self,act,count)
    def NotifySynchFinishedOld(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vNetXmlWxGui.NotifySynchFinished(self)    
    def NotifySynchAbortedOld(self):
        vNetXmlWxGui.NotifySynchAborted(self)    
    def NotifyContentOld(self):
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNodeOld(self,id):
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNodeOld(self,idPar,id,content):
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyAddNodeResponseOld(self,id,idNew):
        vNetXmlWxGui.NotifyAddNodeResponse(self,id,idNew)
    def NotifyRemoveNodeOld(self,id):
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    
