#----------------------------------------------------------------------------
# Name:         DocBuildLatex.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: DocBuildLatex.py,v 1.2 2006/01/08 15:12:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import string
import os,traceback
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.Elements as prjEngElements
import vidarc.vApps.vDraw.latex as latex
import vidarc.tool.log.vtLog as vtLog

def getIdx(str,PROCESSING_CMDS):
    d={}
    for cmd in PROCESSING_CMDS:
        i=string.find(str,cmd)
        if i>=0:
            j=i+len(cmd)
            try:
                if str[j] in ['{','[']:
                    k=string.find(str,'}',j)
                    if k>0:
                        d[i]=(i,j,k,cmd)
            except:
                d[i]=(i,i,j,cmd)
    return d
def getVal(s,v):
    if s[v[1]]=='[':
        j=string.find(s,']',v[1]+1)
        if j>0:
            #addLevel=int(val[:j])
            #val=val[j+2:]
            return (s[v[1]+1:j],s[j+2:v[2]])
        return ('',s)
    return ('',s)
def getVal2(s,v):
    if s[v[1]]=='[':
        j=string.find(s,']',v[1]+1)
        if j>0:
            #addLevel=int(val[:j])
            #val=val[j+2:]
            l=string.find(s,'(',j+1)
            if l>0:
                k=string.find(s,')',l+1)
                if k>0:
                    return (s[v[1]+1:j],s[j+2:v[2]],s[l+1:k])
                else:
                    return (s[v[1]+1:j],s[j+2:v[2]],'')
        return ('',s,'')
    return ('',s,'')
def getNode(doc,par,name,ids,val):
    if len(val[0])>0:
        tup=ids.GetIdStr(val[0])
        if tup[1] is not None:
            return tup[1]
    if par is not None:
        return getSubNode(doc,par,val[1],name)
    else:
        return None
def texReplace4Lbl(s):
    #return string.replace(s,'_','\\_')
    return string.replace(s,'_','')
    
def texReplace(s):
    s=string.replace(s,'#','\\#')
    s=string.replace(s,'_','\\_')
    s=string.replace(s,'.','.\\-')
    return s
def texReplace4Desc(s):
    s=string.replace(s,'#','\\#')
    s=string.replace(s,'_','\\_')
    #s=string.replace(s,'.','.\\-')
    return s
def texReplaceFull(s):
    s=string.replace(s,'\\','/')
    s=string.replace(s,'#','\\#')
    s=string.replace(s,'_','\\_')
    return s
def addContained(f,node,PROCESSING_CMDS,level,genObj,depth):
    def __checkDocSkip__(node):
        doc=genObj['doc']
        childs=doc.getChilds(node,'doc_skip')
        for c in childs:
            if len(doc.getAttribute(c,'ih'))>0:
                sVal=doc.getText(c)
            elif len(doc.getAttribute(c,'fid'))>0:
                sVal=doc.getText(c)
            elif doc.getChild(c,'__browse') is not None:
                sVal=Hierarchy.applyCalcNode(doc,c)
            else:
                sVal=doc.getNodeText(c,'val')
            if sVal==genObj['DocumentGroupNumber']:
                return 1
        return 0
    doc=genObj['doc']
    childs=doc.getChilds(node)
    d={}
    attrs=[]
    skipHier=[]
    for c in childs:
        if len(doc.getAttribute(c,'id'))>0:
            tagName=doc.getTagName(c)
            if prjEngElements.isProperty(tagName,'isSkip')==True:
                continue
            #if prjEngElements.isProperty(c.tagName,'isSkipHier')==True:
            #    skipHier.append(c)
            #    continue
            if prjEngElements.isProperty(tagName,'isNotContained')==True:
                continue
            if __checkDocSkip__(c):
                continue
            if d.has_key(tagName):
                d[tagName].append(c)
            else:
                d[tagName]=[c]
        else:
            attrs.append(c)
    for skipped in skipHier:
        for c in doc.getChilds(skipped):
            if len(doc.getAttribute(c,'id'))>0:
                tagName=doc.getTagName(c)
                if prjEngElements.isProperty(tagName,'isSkip')==True:
                    continue
                if prjEngElements.isProperty(tagName,'isSkipHier')==True:
                    skipHier.append(c)
                    continue
                if prjEngElements.isProperty(tagName,'isNotContained')==True:
                    continue
                if d.has_key(tagName):
                    d[tagName].append(c)
                else:
                    d[tagName]=[c]
            #else:
            #    attrs.append(c)
    # get order from ELEMENTS
    orderedLst=[]
    for k in d.keys():
        orderedLst.append((prjEngElements.getElementIdx(k),d[k]))
    def cmpElem(x,y):
        return cmp(x[0],y[0])
    orderedLst.sort(cmpElem)
    
                    
    for k,elemLst in orderedLst:
        strs=[]
        element=prjEngElements.ELEMENTS[k]
        sHierTag=texReplace(Hierarchy.getTagNamesWithRel(genObj['doc'],
                    genObj['__base_node'],genObj['__rel_node'],node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag+ ':' + texReplace(element[1]['__desc'])
        # prepare latex table
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}p{0.15\linewidth}p{0.65\linewidth}}')
        strs.append('  \\multicolumn{3}{c}{%s: \\textbf{%s}} \\\\ \\hline'%(sHierTag,element[1]['__desc']))
        tup=('    \\textbf{Nr}','\\textbf{Tag}','\\textbf{Name}')
        strs.append('    %s & %s & %s \\\\ '%tup)
        strs.append('       & \\multicolumn{2}{l}{\\textbf{Description}} \\\\ \\hline')
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Tag}','\\textbf{Name}')
        strs.append('    %s & %s & %s \\\\ '%tup)
        strs.append('       & \\multicolumn{2}{c}{\\textbf{Description}} \\\\ \\hline')
        strs.append('  \\endhead')
        iCount=0
        orderedChilds=[]
        for c in elemLst:
            orderedChilds.append((doc.getNodeText(c,'tag'),c))
        orderedChilds.sort(cmpElem)
        
        for tag,c in orderedChilds:
            # add child info to table
            tup=['  %03d '%iCount,tag,doc.getNodeText(c,'name')]
            tup=map(texReplace,tup)
            strs.append('    %s & %s & %s \\\\ '%(tup[0],tup[1],tup[2]))
            sDesc=latex.tableMultiLine(doc.getNodeText(c,'description'))
            strs.append('       & \\multicolumn{2}{l}{%s} \\\\ \\hline'%sDesc)
            iCount+=1
            pass
        strs.append('  \\caption{%s}'%sHier)
        sHier=texReplace4Lbl(Hierarchy.getTagNames(genObj['doc'],
                genObj['__base_node'],None,node))
        strs.append('  \\label{tab:%s %s}'%(sHier,texReplace4Lbl(element[1]['__desc'])))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
        f.write(string.join(strs,os.linesep))
    orderedAttrLst=[]
    iCount=0
    def __getTmplNode__(a):
        try:
            doc=genObj['doc']
            parNode=doc.getParent()
            tmplNode=genObj['ids'].GetIdStr(doc.getAttribute(parNode,'iid'))[1]
            if tmpNode is not None:
                attrNode=doc.getChild(tmplNode,doc.getTagName(a))
                if attrNode is not None:
                    return attrNode
        except:
            pass
        return a
    for a in attrs:
        tagName=doc.getTagName(a)
        if tagName not in ['tag','name','description']:
            if tagName=='inheritance_type':
                if len(doc.getAttribute(a,'ih'))>0:
                    a=__getTmplNode__(a)
                sFbUnit=doc.getNodeText(a,'unit')
            else:
                if tagName[:2]!='__':
                    orderedAttrLst.append((tagName,iCount))
        iCount += 1
    orderedAttrLst.sort(cmpElem)
    strs=[]
    if iCount>0:
        sHierTag=texReplace(Hierarchy.getTagNamesWithRel(genObj['doc'],
                    genObj['__base_node'],genObj['__rel_node'],node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag+ ': ' + 'Attributes'
        # prepare latex table
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}')
        strs.append('       p{0.35\linewidth}p{0.35\linewidth}p{0.1\linewidth}}')
        strs.append('  \\multicolumn{4}{c}{%s: \\textbf{%s}} \\\\ \\hline'%(sHierTag,'Attributes'))
        tup=('    \\textbf{Nr}','\\textbf{Name}','\\textbf{Value}','\\textbf{Unit}')
        strs.append('    %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Name}','\\textbf{Value}','\\textbf{Unit}')
        strs.append('    %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        
        iCount=0
        for tag,i in orderedAttrLst:
            a=attrs[i]
            if len(doc.getAttribute(a,'ih'))>0:
                sVal=doc.getText(a)
                a=__getTmplNode__(a)
            elif len(doc.getAttribute(a,'fid'))>0:
                sVal=doc.getText(a)
            elif doc.getChild(a,'__browse') is not None:
                sVal=Hierarchy.applyCalcNode(doc,a)
            else:
                sVal=doc.getNodeText(a,'val')
            if string.find(doc.getNodeText(a,'inheritance'),'type')>=0:
                sUnit=sFbUnit
            else:
                sUnit=doc.getNodeText(a,'unit')
                
            tup=['  %03d '%iCount,tag,sVal,sUnit]
            tup=map(texReplace,tup)
            strs.append('    %s & %s & %s & %s \\\\ \\hline'%(tup[0],tup[1],tup[2],tup[3]))
            iCount += 1
            
        strs.append('  \\caption{%s}'%sHier)
        sHier=texReplace4Lbl(Hierarchy.getTagNames(genObj['doc'],
                    genObj['__base_node'],None,node))
        strs.append('  \\label{tab:%s %s}'%(sHier,'Attributes'))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
        f.write(string.join(strs,os.linesep))
    if depth>0:
        for k,elemLst in orderedLst:
            orderedChilds=[]
            for c in elemLst:
                orderedChilds.append((doc.getNodeText(c,'tag'),c))
            orderedChilds.sort(cmpElem)
            for tag,c in orderedChilds:
                #sGrp=texReplace(Hierarchy.getTagNamesWithRel(genObj['__base_node'],
                #                        genObj['__rel_node'],c))
                element=prjEngElements.ELEMENTS[k]
                try:
                    sDesc=element[1]['__desc']
                except:
                    sDesc=''
                val=texReplace(sDesc+': '+tag)
                if level==0:
                    f.write(os.linesep+'\\section{%s}'%val)
                elif level==1:
                    f.write(os.linesep+'\\subsection{%s}'%val)
                elif level==2:
                    f.write(os.linesep+'\\subsubsection{%s}'%val)
                elif level>2:
                    f.write(os.linesep+'\\paragraph{%s}'%val)
                else:
                    f.write(os.linesep+'\\paragraph*{%s}'%val)
                docs=doc.getChild(c,'Documents')
                if docs is not None:
                    for docChild in doc.getChilds(docs,'Doc'):
                        if doc.getAttribute(docChild,'fid')==genObj['DocumentGroupNumber']:
                            processNode(f,docChild,PROCESSING_CMDS,level+1,genObj)
    
                addContained(f,c,PROCESSING_CMDS,level+1,genObj,depth-1)
def addLog(f,node,PROCESSING_CMDS,level,genObj,recursive=False):
    doc=genObj['doc']
    if recursive:
        childs=doc.getChildsRec(node,'Log')
    else:
        childs=doc.getChilds(node,'Log')
    
    orderedLst=[]
    for c in childs:
        sDate=doc.getNodeText(c,'date')
        sTime=doc.getNodeText(c,'time')
        sGrp=texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],
                                        genObj['__rel_node'],doc.getParent(c)))
        orderedLst.append((sGrp,sDate+' '+sTime,c))
    def cmpElem(x,y):
        i=cmp(y[0],x[0])  # inverse sort order
        if i==0:
            return cmp(x[1],y[1])
        else:
            return i
    orderedLst.sort(cmpElem)
    orderedLst.reverse()
    
    
    if len(orderedLst)>0:
        strs=[]
        sHierTag=latex.texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],genObj['__rel_node'],node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag+ ':' + 'Log'
        # prepare latex table
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}')
        strs.append('       p{0.14\linewidth}p{0.1\linewidth}p{0.55\linewidth}}')
        strs.append('  \\multicolumn{4}{c}{%s: \\textbf{%s}} \\\\ \\hline'%(sHierTag,'Log'))
        tup=('    \\textbf{Nr}','\\textbf{Date}','\\textbf{Time}','\\textbf{Name}')
        strs.append('    %s & %s & %s & %s \\\\ '%tup)
        tup=('    ','\\textbf{User}','\\textbf{Description}')
        strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Date}','\\textbf{Time}','\\textbf{Name}')
        strs.append('    %s & %s & %s & %s \\\\ '%tup)
        tup=('    ','\\textbf{User}','\\textbf{Description}')
        strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        
        iCount=0
        sActHier=''
        for sHierCur,s,c in orderedLst:
            if sActHier!=sHierCur:
                sActHier=sHierCur
                strs.append('     \\multicolumn{4}{l}{\\textbf{%s}} \\\\ \\hline'%(sActHier))
                
            tup=['  %03d '%iCount,
                doc.getNodeText(c,'date'),
                doc.getNodeText(c,'time'),
                doc.getNodeText(c,'name')]
            tup=map(latex.texReplace,tup)
            strs.append('    %s & %s & %s & %s \\\\ '%(tup[0],tup[1],tup[2],tup[3]))
            tup=['   ',
                latex.texReplace(doc.getNodeText(c,'usr')),
                string.replace(latex.tableMultiLine(doc.getNodeText(c,'desc'),'p{0.65\linewidth}'),'\n',os.linesep)]
            #tup=map(texReplace,tup)
            strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\ \\hline'%(tup[0],tup[1],tup[2]))
            iCount += 1
            
        strs.append('  \\caption{%s}'%sHier)
        sHier=Hierarchy.getTagNames(doc,genObj['__base_node'],None,node)
        strs.append('  \\label{tab:%s %s}'%(sHier,'Log'))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
        f.write(string.join(strs,os.linesep))
def addRev(f,node,PROCESSING_CMDS,level,genObj,recursive=False):
    doc=genObj['doc']
    if recursive:
        childs=doc.getChildsRec(node,'Safe')
    else:
        childs=doc.getChilds(node,'Safe')
    
    orderedLst=[]
    for c in childs:
        sDate=doc.getNodeText(c,'date')
        sTime=doc.getNodeText(c,'time')
        sGrp=texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],
                                        genObj['__rel_node'],doc.getParent(c)))
        orderedLst.append((sGrp,sDate+' '+sTime,c))
    def cmpElem(x,y):
        i=cmp(y[0],x[0])  # inverse sort order
        if i==0:
            return cmp(x[1],y[1])
        return i
    orderedLst.sort(cmpElem)
    orderedLst.reverse()
    
    if len(orderedLst)>0:
        strs=[]
        sHierTag=texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],genObj['__rel_node'],node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag+ ':' + 'Revision'
        # prepare latex table
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}')
        strs.append('       p{0.14\linewidth}p{0.1\linewidth}p{0.55\linewidth}}')
        strs.append('  \\multicolumn{4}{c}{%s: \\textbf{%s}} \\\\ \\hline'%(sHierTag,'Revision'))
        tup=('    \\textbf{Nr}','\\textbf{Date}','\\textbf{Time}','\\textbf{Name}')
        strs.append('    %s & %s & %s & %s \\\\ '%tup)
        tup=('    ','\\textbf{Rev}','\\textbf{User}','\\textbf{Description}')
        strs.append('    %s  & %s & %s & %s  \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Date}','\\textbf{Time}','\\textbf{Name}')
        strs.append('    %s & %s & %s & %s \\\\ '%tup)
        tup=('    ','\\textbf{Rev}','\\textbf{User}','\\textbf{Description}')
        strs.append('    %s  & %s & %s & %s  \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        
        iCount=0
        sActHier=''
        for sHierCur,s,c in orderedLst:
            if sActHier!=sHierCur:
                sActHier=sHierCur
                strs.append('     \\multicolumn{4}{l}{\\textbf{%s}} \\\\ \\hline'%(sActHier))
            tup=['  %03d '%iCount,
                doc.getNodeText(c,'date'),
                doc.getNodeText(c,'time'),
                doc.getNodeText(c,'name')]
            tup=map(texReplace,tup)
            strs.append('    %s & %s & %s & %s \\\\ '%(tup[0],tup[1],tup[2],tup[3]))
            tup=['   ',doc.getNodeText(c,'rev'),
                latex.texReplace(doc.getNodeText(c,'usr')),
                latex.tableMultiLine(doc.getNodeText(c,'desc'),'p{0.55\linewidth}')]
            strs.append('    %s & %s & %s & %s \\\\ \\hline'%(tup[0],tup[1],tup[2],tup[3]))
            iCount += 1
            
        strs.append('  \\caption{%s}'%sHier)
        sHier=Hierarchy.getTagNames(doc,genObj['__base_node'],None,node)
        strs.append('  \\label{tab:%s %s}'%(sHier,'Revision'))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
        f.write(string.join(strs,os.linesep))

def addHierarchy(f,node,PROCESSING_CMDS,level,genObj,bDown):
    doc=genObj['doc']
    d={}
    lstNodes=[]
    iCount=0
    if 1==1:
        strs=[]
        sHierTag=texReplace(Hierarchy.getTagNamesWithBase(doc,genObj['__base_node'],None,node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag#+ ': ' + prjEngElements.ELEMENTS[k][1]['__desc']
        # prepare latex table
        iLvMax=20    
        strs.append('\\begin{center}')
        s='  \\begin{longtable}{|p{0.05\linewidth}|p{0.6\linewidth}|p{0.25\linewidth}|}'
        strs.append(s)
        strs.append('  \\hline \\multicolumn{3}{|c|}{\\textit{%s}} \\\\ \\hline '%(sHier))
        strs.append('  %s & %s & %s \\\\ \\hline'%('lv','tag','elem'))
        s='    \\textbf{Tag}'
        strs.append('  \\endfirsthead')
        strs.append('  \\hline \\multicolumn{3}{|c|}{\\textit{%s}} \\\\ \\hline'%(sHier))
        strs.append('  %s & %s & %s \\\\ \\hline'%('lv','tag','elem'))
        strs.append('  \\endhead')
        
        if bDown==False:
            lst=[]
            parNode=node
            while parNode is not None:
                lst.append(parNode)
                if prjEngElements.isProperty(doc.getTagName(parNode),'isRoot'):
                    parNode=None
                else:
                    parNode=doc.getParent(parNode)
            lst.reverse()
            i=0
            s='    '
            for n in lst:
               strs.append('    %02d & \\hspace{%4.2fcm}%s & %s \\\\'%(i,0.3*i,texReplace(doc.getNodeText(n,'tag')),texReplace(prjEngElements.getProperty(doc.getTagName(n),'__desc'))))
               i+=1
            name='hierarchy upwards'
        else:
            i=0
            parNode=node
            while parNode is not None:
                i+=1
                if prjEngElements.isProperty(doc.getTagName(parNode),'isRoot'):
                    parNode=None
                else:
                    parNode=doc.getParent(parNode)
            def sortFunc(a,b):
                return cmp(doc.getNodeText(a,'tag'),doc.getNodeText(b,'tag'))
            def addChilds(n,lv):
                lst=doc.getChildsAttr(n,'id')
                lst.sort(sortFunc)
                for c in lst:
                    if prjEngElements.isProperty(doc.getTagName(c),'isSkip')==False:
                        strs.append('    %02d & \\hspace{%4.2fcm}%s & %s \\\\'%(i+lv,0.3*lv,texReplace(doc.getNodeText(c,'tag')),texReplace(prjEngElements.getProperty(doc.getTagName(c),'__desc'))))
                        addChilds(c,lv+1)
            if doc.getTagName(node)=='filter':
                genObj['dlgFilter'].SetNode(node,False)
                lstTup=genObj['dlgFilter'].GetNodeList()
                lst=[]
                oldId=None
                for it in lstTup:
                    c=doc.getParent(it[1])
                    newId=doc.getAttribute(c,'id')
                    if oldId==newId:
                        continue
                    oldId=newId
                    lst.append(c)
                lst.sort(sortFunc)
                bFirst=True
                id=doc.getKey(doc.getParent(node))
                dictId={}
                lstId=[]
                for n in lst:
                    if bFirst:
                        bFirst=False
                    else:
                        #strs.append('  \\hline')
                        pass
                    lstPar=[]
                    parNode=n
                    while parNode is not None:
                        lstPar.append(parNode)
                        if doc.getKey(parNode)==id:
                            parNode=None
                        elif prjEngElements.isProperty(doc.getTagName(parNode),'isRoot'):
                            parNode=None
                        else:
                            parNode=doc.getParent(parNode)
                    lstPar.reverse()
                    lv=0
                    s='    '
                    dId=dictId
                    lId=lstId
                    for nn in lstPar:
                        bAdd=False
                        nnId=doc.getKey(nn)
                        try:
                            tup=dId[nnId]
                            #lstId.append(nn)
                        except:
                            dId[nnId]=({},[])
                            lId.append(nn)
                        dId,lId=dId[nnId]
                        #if len(lstId)<=lv:
                        #    lstId.append(nnId)
                        #    bAdd=True
                        #else:
                        #    if lstId[lv]!=nnId:
                        #        bAdd=True
                        #        lstId[lv]=nnId
                        #if bAdd:
                        #    strs.append('    %02d & \\hspace{%4.2fcm}%s & %s \\\\'%(i+lv,0.3*lv,texReplace(doc.getNodeText(nn,'tag')),texReplace(prjEngElements.getProperty(doc.getTagName(nn),'__desc'))))
                        lv+=1
                def procTup(d,lst,lv):
                    lst.sort(sortFunc)
                    for n in lst:
                        strs.append('    %02d & \\hspace{%4.2fcm}%s & %s \\\\'%(i+lv,0.3*lv,texReplace(doc.getNodeText(n,'tag')),texReplace(prjEngElements.getProperty(doc.getTagName(n),'__desc'))))
                        nId=doc.getKey(n)
                        try:
                            tup=d[nId]
                            procTup(tup[0],tup[1],lv+1)
                        except:
                            pass
                procTup(dictId,lstId,0)
                #for k in dictId.keys():
                    
                    #strs.append('    %02d & \\hspace{%4.2fcm}%s & %s \\\\'%(i,0.0,texReplace(doc.getNodeText(n,'tag')),texReplace(prjEngElements.getProperty(doc.getTagName(n),'__desc'))))
                    #addChilds(n,1)
                    
            else:
                addChilds(node,0)
            name='hierarchy downwards'
            pass
        sHier=texReplace(Hierarchy.getTagNames(doc,genObj['__base_node'],None,node))
        strs.append('  \\hline')
        strs.append('  \\caption{%s %s}'%(sHier,name))
        sHier=texReplace4Lbl(Hierarchy.getTagNames(doc,genObj['__base_node'],None,node))
        strs.append('  \\label{tab_childs_attr:%s %s}'%(sHier,name))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
    f.write(string.join(strs,os.linesep))

def childAttr(f,node,PROCESSING_CMDS,level,genObj):
    doc=genObj['doc']
    d={}
    iCount=0
    for c in doc.getChilds(node):
        if len(doc.getAttribute(c,'id'))>0:
            tagName=doc.getTagName(c)
            if prjEngElements.isProperty(tagName,'isSkip')==True:
                continue
            #if prjEngElements.isProperty(c.tagName,'isSkipHier')==True:
            #    skipHier.append(c)
            #    continue
            if prjEngElements.isProperty(tagName,'isNotContained')==True:
                continue
            if d.has_key(tagName):
                d[tagName].append(c)
            else:
                d[tagName]=[c]
        #else:
        #    attrs.append(c)
    if 1==0:
        print 'checkNodes'
        for k in d.keys():
            print '  ',k
            for c in d[k]:
                print '    ',doc.getNodeText(c,'tag')
        
    orderedLst=[]
    for k in d.keys():
        orderedLst.append((prjEngElements.getElementIdx(k),d[k]))
    def cmpElem(x,y):
        return cmp(x[0],y[0])
    orderedLst.sort(cmpElem)
    if 1==0:
        print 'orderedLst'
        for it in orderedLst:
            print '  ',it[0],len(it[1])
            for c in it[1]:
                print '    ',doc.getNodeText(c,'tag')
        #print 'orderedattr'
        #print '  ',len(orderedAttrLst),orderedAttrLst
    
    strs=[]
    if len(orderedLst)>0:
        for k,childs in orderedLst:
            orderedAttrLst=[]
            orderedChildLst=[]
            iIdx=0
            for c in childs:
                for a in doc.getChilds(c):
                    tagName=doc.getTagName(a)
                    if tagName not in ['tag','name','description']:
                        if tagName=='inheritance_type':
                            sFbUnit=doc.getNodeText(a,'unit')
                        else:
                            if tagName[:2]!='__':
                                try:
                                    idx=orderedAttrLst.index(tagName)
                                except:
                                    orderedAttrLst.append(tagName)
                                iCount += 1
                orderedChildLst.append((doc.getNodeText(c,'tag'),iIdx))
                iIdx+=1
            orderedAttrLst.sort()
            orderedChildLst.sort(cmpElem)
            if 1==0:
                print 'orderedChildLst'
                print '  ',len(childs)
                print '  ',len(orderedChildLst)
                for itTmpl in orderedChildLst:
                    print '    ',itTmpl[0]
            sHierTag=texReplace(Hierarchy.getTagNamesWithRel(doc,
                    genObj['__base_node'],genObj['__rel_node'],node))
            if sHierTag=='\\_':
                sHierTag='.'
            sHier=sHierTag+ ': ' + prjEngElements.ELEMENTS[k][1]['__desc']
            # prepare latex table
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{|p{0.15\linewidth}|'
            iLenAttr=len(orderedAttrLst)
            for i in range(0,len(orderedAttrLst)):
                s+='r|'
            strs.append('%s}'%s)
            strs.append('   \\hline \\multicolumn{%d}{|c|}{\\textbf{%s}} \\\\ \\hline'%(iLenAttr+1,sHier))
            s='    '
            for aName in orderedAttrLst:
                s+=' & '
            strs.append(s + ' \\\\ ')
            s='    \\textbf{Tag}'
            for aName in orderedAttrLst:
                s+=' & \\rotatebox{90}{%s}'%latex.texReplace(aName)
            strs.append(s + ' \\\\ \\hline')
            strs.append('  \\endfirsthead')
            strs.append(s + ' \\\\ \\hline')
            strs.append('  \\endhead')
        
            for tag,idx in orderedChildLst:
                c=childs[idx]
                s='    ' + texReplace(tag)
                for aName in orderedAttrLst:
                    try:
                        a=doc.getChild(c,aName)
                        sVal=doc.getNodeText(a,'val')
                    except:
                        sVal=''
                    s+=' & ' + texReplace(sVal)
                strs.append(s + ' \\\\ \\hline')
            
            
            strs.append('  \\caption{%s %s}'%(sHier,prjEngElements.ELEMENTS[k][1]['__desc']))
            sHier=Hierarchy.getTagNames(genObj['doc'],genObj['__base_node'],None,node)
            strs.append('  \\label{tab:%s %s}'%(sHier,prjEngElements.ELEMENTS[k][1]['__desc']))
            
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            strs.append('')
    f.write(string.join(strs,os.linesep))
def childAttrs(f,node,PROCESSING_CMDS,level,genObj,lstElem,lstAttr,lstAttrCount,name):
    lstCountVal=[]
    lstSumVal=[]
    lstCountEntryVal=[]
    for s in lstAttrCount:
        strs=string.split(s,':')
        if len(strs)>1:
            if 'sum' in strs[1:]:
                lstSumVal.append(strs[0])
            if 'count' in strs[1:]:
                lstCountVal.append(strs[0])
            if 'count_entry' in strs[1:]:
                lstCountEntryVal.append(strs[0])
        else:
            lstCountVal.append(s)
            #lstSumVal.append(s)
            #lstCountEntryVal.append(s)
    doc=genObj['doc']
    d={}
    lstNodes=[]
    iCount=0
    def __getAttrLst__(node,attrDict,attrLst):
        dAttrTmp={}
        for c in doc.getChilds(node):
            if len(doc.getAttribute(c,'id'))==0:
                try:
                    tagName=doc.getTagName(c)
                    idx=lstAttr.index(tagName)
                    #attrLst.append(c)
                    try:
                        lst=attrDict[tagName]
                        lst.append(c)
                    except:
                        attrDict[tagName]=[c]
                    try:
                        orderedAttrLst.index(tagName)
                    except:
                        orderedAttrLst.append(tagName)
                    dAttrTmp[tagName]=c
                except:
                    pass
        if len(dAttrTmp.keys())>0:
            attrLst.append((node,dAttrTmp))
        for c in doc.getChilds(node):
            __getAttrLst__(c,attrDict,attrLst)
    def __checkNodes__(node,recursive=True):
        if len(doc.getAttribute(node,'id'))>0:
            try:
                tagName=doc.getTagName(node)
                idx=lstElem.index(tagName)
                if prjEngElements.isProperty(tagName,'isSkip')==True:
                    #continue
                    return
                #if prjEngElements.isProperty(c.tagName,'isSkipHier')==True:
                #    skipHier.append(c)
                #    continue
                if prjEngElements.isProperty(tagName,'isNotContained')==True:
                    #continue
                    return
                attrLst=[]
                attrDict={}
                __getAttrLst__(node,attrDict,attrLst)
                if d.has_key(tagName):
                    d[tagName].append((node,attrLst))
                else:
                    d[tagName]=[(node,attrLst)]
                if len(attrLst)>0:
                    lstNodes.append((node,attrLst))
            except:
                pass
        if recursive:
            for c in doc.getChildsAttr(node,'id'):
                __checkNodes__(c)
    def __checkAttr__(node):
        for c in doc.getChilds(node):
            if len(doc.getAttribute(c,'id'))==0:
                try:
                    tagName=doc.getTagName(c)
                    idx=lstAttr.index(tagName)
                    if d.has_key(tagName):
                        dAttr[tagName].append(c)
                    else:
                        dAttr[tagName]=[c]
                except:
                    pass
            __checkAttr__(c)
    orderedAttrLst=[]
    if doc.getTagName(node)=='filter':
        #__checkNodes__(doc.getParent(node))
        genObj['dlgFilter'].SetNode(node,False)
        lst=genObj['dlgFilter'].GetNodeList()
        oldId=None
        
        for it in lst:
            c=doc.getParent(it[1])
            newId=doc.getAttribute(c,'id')
            #print doc.getParent(it[1])
            #__checkNodes__(doc.getParent(it[1]))
            #dAttr={}
            #__checkAttr__(doc.getParent(it[1]))
            if oldId==newId:
                continue
            oldId=newId
            try:
                tagName=doc.getTagName(c)
                idx=lstElem.index(tagName)
                
                attrLst=[]
                attrDict={}
                __getAttrLst__(c,attrDict,attrLst)
                if d.has_key(tagName):
                    d[tagName].append((c,attrLst))
                else:
                    d[tagName]=[(c,attrLst)]
                if len(attrLst)>0:
                    lstNodes.append((c,attrLst))
            except:
                pass
    else:
        __checkNodes__(node)
    if 1==0:
        print 'checkNodes'
        for k in d.keys():
            print '  ',k
            for c in d[k]:
                print '    ',doc.getNodeText(c[0],'tag'),len(c[1])
        
    orderedLst=[]
    for k in d.keys():
        orderedLst.append((prjEngElements.getElementIdx(k),d[k]))
    def cmpElem(x,y):
        return cmp(x[0],y[0])
    orderedLst.sort(cmpElem)
    
    if 1==0:
        print 'orderedLst'
        for it in orderedLst:
            print '  ',it[0],len(it[1])
            for c in it[1]:
                print '    ',doc.getNodeText(c[0],'tag'),len(c[1])
        print 'orderedattr'
        print '  ',len(orderedAttrLst),orderedAttrLst
    strs=[]
    if len(orderedLst)>0:
        orderedAttrLst.sort()
        iIdx=0
        for it in ['tag','name','description']:
            try:
                orderedAttrLst.index(it)
                orderedAttrLst.remove(it)
                orderedAttrLst.insert(iIdx,it)
                iIdx+=1
            except:
                pass
        sHierTag=texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],genObj['__rel_node'],node))
        if sHierTag=='\\_':
            sHierTag='.'
        sHier=sHierTag#+ ': ' + prjEngElements.ELEMENTS[k][1]['__desc']
        # prepare latex table
        dSum={}
        dCount={}
        dCountVal={}
            
        strs.append('\\begin{center}')
        iLenAttr=1#len(orderedAttrLst)
        iNum=0
        try:
            orderedAttrLst.index('description')
            bDesc=True
            s='  \\begin{longtable}{rl'
        except:
            s='  \\begin{longtable}{|r|l|'
            bDesc=False
        for i in range(0,len(orderedAttrLst)):
            dSum[orderedAttrLst[i]]=0.0
            dCount[orderedAttrLst[i]]=0
            dCountVal[orderedAttrLst[i]]={}
            if orderedAttrLst[i]=='description':
                continue
            if bDesc:
                s+='r'
            else:
                s+='r|'
            iLenAttr+=1
        strs.append('%s}'%s)
        if bDesc:
            strs.append('   \\hline \\multicolumn{%d}{l}{\\textit{%s}} \\\\ \\hline'%(iLenAttr+1,sHier))
            strs.append('           \\multicolumn{%d}{l}{\\textbf{%s}} \\\\ \\hline'%(iLenAttr+1,name))
        else:
            strs.append('   \\hline \\multicolumn{%d}{|l|}{\\textit{%s}} \\\\ \\hline'%(iLenAttr+1,sHier))
            strs.append('           \\multicolumn{%d}{|l|}{\\textbf{%s}} \\\\ \\hline'%(iLenAttr+1,name))
        sDescTitle=''
        #s='    '
        #for aName in orderedAttrLst:
        #    if aName=='description':
        #        continue
        #    s+=' & '
        #strs.append(s + ' \\\\ ')
        s='    \\textbf{Nr} & \\textbf{Tag}'
        for aName in orderedAttrLst:
            if aName=='description':
                sDescTitle='      & \\multicolumn{%d}{p{0.5\linewidth}}{\\hspace{1cm}%s} \\\\ \\hline'%(iLenAttr,aName)
                continue
            s+=' & \\rotatebox{90}{%s }'%latex.texReplace(aName)
            #s+=' & %s'%aName
        if len(sDescTitle)>0:
            #strs.append(s + ' \\\\ \\cline{%d-%d}'%(2,iLenAttr))
            strs.append(s + ' \\\\ ')
            strs.append(sDescTitle)
        else:
            strs.append(s + ' \\\\ \\hline')
        strs.append('  \\endfirsthead')
        if len(sDescTitle)>0:
            #strs.append(s + ' \\\\ \\cline{%d-%d}'%(2,iLenAttr))
            strs.append(s + ' \\\\ ')
            strs.append(sDescTitle)
        else:
            strs.append(s + ' \\\\ \\hline')
        strs.append('  \\endhead')
        
        for k,childs in orderedLst:
            orderedChildLst=[]
            iIdx=0
            for c in childs:
                #for ccTup in c[1]:
                try:
                    ccTup=c[1][0]
                    cc,attrDict=ccTup
                    sHierRel=Hierarchy.getTagNames(doc,genObj['__base_node'],genObj['__rel_node'],cc)
                    #sHierRel=Hierarchy.getTagNames(doc,genObj['__base_node'],node,cc)
                    orderedChildLst.append((sHierRel,iIdx,c,ccTup))
                    iIdx+=1
                except:
                    pass
        
            orderedChildLst.sort(cmpElem)
            if 1==0:
                print 'orderedChildLst'
                print '  ',len(orderedChildLst),
                for itTmp in orderedChildLst:
                    print '    ',itTmp[0]
            
            oldName=None
            for tag,idx,elemNodeTup,ccTup in orderedChildLst:
                elemNode=elemNodeTup[0]
                if oldName!=doc.getTagName(elemNode):
                    idx=prjEngElements.getElementIdx(doc.getTagName(elemNode))
                    sDesc=prjEngElements.ELEMENTS[idx][1]['__desc']
                    if bDesc:
                        sDescTbl=string.replace(latex.tableMultiLine(sDesc,'p{0.51\linewidth}'),'\n',os.linesep)
                        strs.append('   & \\multicolumn{%d}{l|}{\\textbf{%s}} \\\\ \\hline'%(iLenAttr,sDescTbl))
                    else:
                        strs.append('   & \\multicolumn{%d}{l|}{\\textbf{%s}} \\\\ \\hline'%(iLenAttr,sDesc))
                    oldName=doc.getTagName(elemNode)
                #c=childs[idx]
                c=ccTup[0]
                dAttr=ccTup[1]
                
                iNum+=1
                sTag=texReplace(tag)
                if sTag=='\\_':
                    sTag='.'
                s='    %d & %s'%(iNum,sTag)
                tupMulti=(0,'')
                for aName in orderedAttrLst:
                    bSkipValProc=False
                    try:
                        if aName in ['tag','name','description']:
                            sVal=doc.getText(dAttr[aName])
                            if aName=='description':
                                tupMulti=latex.tableMultiLineTup(sVal,'p{0.51\linewidth}')
                                #tupMulti=(1,string.replace(latex.tableMultiLine(sVal,'p{0.8\linewidth}'),'\n',os.linesep))
                                continue
                                sVal='%s'%tupMulti[1]
                                if tupMulti[0]>0:
                                    bSkipValProc=True
                            else:
                                sVal=latex.texReplace(sVal)
                                bSkipValProc=True
                        else:
                            if len(doc.getAttribute(dAttr[aName],'ih'))>0:
                                sVal=doc.getText(dAttr[aName])
                            elif len(doc.getAttribute(dAttr[aName],'fid'))>0:
                                sVal=doc.getText(dAttr[aName])
                            elif doc.getChild(dAttr[aName],'__browse') is not None:
                                sVal=Hierarchy.applyCalcNode(doc,dAttr[aName])
                            else:
                                sVal=doc.getNodeText(dAttr[aName],'val')
                            sValCalc=sVal
                            sVal=latex.texReplace(sVal)
                        
                        try:
                            lstCountVal.index(aName)
                            bCount=True
                        except:
                            bCount=False
                        try:
                            lstSumVal.index(aName)
                            bSum=True
                        except:
                            bSum=False
                        try:
                            lstCountEntryVal.index(aName)
                            bCountEntry=True
                        except:
                            bCountEntry=False
                        if bCount or bSum or bCountEntry:
                            bSkipValProc=False
                        else:
                            bSkipValProc=True
                        if bSkipValProc==False:
                            dCount[aName]+=1
                            try:
                                dCountVal[aName][sVal]+=1
                            except:
                                dCountVal[aName][sVal]=1
                            try:
                                dSum[aName]+=float(sValCalc)
                            except:
                                dSum[aName]=None
                                #dSum[aName]+=0.0
                    except:
                        sVal=''
                    s+=' & ' + sVal #texReplace(sVal)
                if tupMulti[0]>0:
                    #strs.append(s + ' \\\\ \\cline{%d-%d}'%(2,iLenAttr))
                    strs.append(s + ' \\\\ ')
                    sVal='%s'%tupMulti[1]
                    strs.append('   \\multicolumn{%d}{l}{\\hspace{1cm}%s} \\\\ \\hline'%(iLenAttr+1,sVal))
                else:
                    strs.append(s + ' \\\\ \\hline')
        if len(lstSumVal)>0:
            strs.append(' \\hline')
            s='    & sum'
            for aName in orderedAttrLst:
                if aName=='description':
                    continue
                try:
                    lstSumVal.index(aName)
                    sVal='%4.2f'%dSum[aName]
                except:
                    sVal='-'
                s+=' & ' + sVal
            strs.append(s + ' \\\\ \\hline')
        
        if len(lstCountVal)>0:
            lstCount=[]
            s='    & count (all)'
            for aName in orderedAttrLst:
                if aName=='description':
                    continue
                try:
                    lstCountVal.index(aName)
                    sVal='%d'%dCount[aName]
                except:
                    sVal='-'
                s+=' & ' + sVal
                lstCount.append(len(dCountVal[aName].keys()))
            strs.append(s + ' \\\\ \\hline')
        
        if len(lstCountEntryVal)>0:
            iMax=0
            for count in lstCount:
                if count>iMax:
                    iMax=count
            lstCount=[]
            for i in range(0,iMax):
                lstCountVal=[]
                for aName in orderedAttrLst:
                    keys=dCountVal[aName].keys()
                    keys.sort()
                    try:
                        lstCountVal.append((keys[i],dCountVal[aName][keys[i]]))
                    except:
                        lstCountVal.append(('',''))
                lstCount.append(lstCountVal)
            for it in lstCount:
                s='     &'
                #try:
                #    lstCountEntryVal.index(aName)
                #except:
                #    continue
                bFound=False
                i=0
                for attr in it:
                    try:
                        aName=orderedAttrLst[i]
                        lstCountEntryVal.index(aName)
                        sVal=latex.texReplace(attr[0])
                        bFound=True
                    except:
                        sVal=''
                    s+=' & ' + sVal 
                    i+=1
                if bFound:
                    strs.append(s + ' \\\\ ')
                
                i=0
                bFound=False
                s='     &'
                for attr in it:
                    try:
                        aName=orderedAttrLst[i]
                        lstCountEntryVal.index(aName)
                        sVal=latex.texReplace('%d'%attr[1])
                        bFound=True
                    except:
                        sVal=''
                    s+=' & ' + sVal
                    i+=1
                if bFound:
                    strs.append(s + ' \\\\ ')
                
            strs.append('  \\hline')
            
        sHier=texReplace(Hierarchy.getTagNames(doc,genObj['__base_node'],genObj['__rel_node'],node))
        strs.append('  \\caption{%s %s}'%(sHier,name))
        sHier=texReplace4Lbl(Hierarchy.getTagNames(doc,genObj['__base_node'],None,node))
        strs.append('  \\label{tab_childs_attr:%s %s}'%(sHier,name))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        strs.append('')
    f.write(string.join(strs,os.linesep))
def childs_attr_reference(f,node,genObj,lstElem,lstAttr,name):
    doc=genObj['doc']
    sHierTag=texReplace(Hierarchy.getTagNamesWithRel(doc,genObj['__base_node'],genObj['__rel_node'],node))
    if sHierTag=='\\_':
        sHierTag='.'
    sHier=sHierTag#+ ': ' + prjEngElements.ELEMENTS[k][1]['__desc']
    
    strs=['\\ref{tab_childs_attr:%s %s} \#\\pageref{tab_childs_attr:%s %s}'%(sHier,name,sHier,name)]
    f.write(string.join(strs,os.linesep))
def processChilds(id,f,node,PROCESSING_CMDS,level,skipnode=None,genObj={}):
    doc=genObj['doc']
    childs=doc.getChilds(node)
    for c in childs:
        if c==skipnode:
            continue
        if len(doc.getAttribute(c,'id'))<=0:
            continue
        if doc.getTagName(c)=='Documents':
            for docChild in doc.getChilds(c):
                if doc.getTagName(docChild)=='Doc':
                    if doc.getAttribute(docChild,'fid')==id:
                        processNode(f,docChild,PROCESSING_CMDS,level,genObj)
        else:
            processChilds(id,f,c,PROCESSING_CMDS,level+1,skipnode,genObj)
def getSubNode(doc,node,val,subTag):
    vals=string.split(val,'.')
    for c in doc.getChilds(node):
        tag=doc.getNodeText(c,'tag')
        if vals[0]==tag:
            if len(vals)>1:
                return getSubNode(doc,c,string.join(vals[1:]),subTag)
            else:
                return doc.getChild(c,subTag)
def getAdditionalInfo(l,v):
    iLevelStart=v[2]+1
    if iLevelStart<len(l):
        if l[iLevelStart]=='(':
            iLevelEnd=string.find(l,')',iLevelStart)
            if iLevelEnd>0:
                return u' '+l[iLevelStart+1:iLevelEnd]
    return ''
def insertAttr(f,strsVal,aNode,sVal,PROCESSING_CMDS,level,node,genObj):
    i=string.rfind(sVal,'.')
    
    bImg=False
    if i>0:
        sExt=sVal[i+1:]
        if sExt in ['jpg','png']:
            bImg=True
    if sVal[0]=='.':
        sFN=string.replace(genObj['prjDN'],'\\','/')+sVal[1:]
    else:
        sFN=sVal
    if bImg:
        if len(strsVal)>1:
            f.write(os.linesep+'\\includegraphics[%s]{%s}'%(strsVal[1],sFN))
        else:
            f.write(os.linesep+'\\includegraphics{%s}'%sFN)
    return
def processNode(f,node,PROCESSING_CMDS,level,genObj):
    doc=genObj['doc']
    if level==0:
        if genObj.has_key('__root_node'):
            rootNode=genObj['__root_node']
            rootTag=doc.getNodeText(rootNode,'tag')
            nodeTag=doc.getNodeText(node,'tag')
            if rootTag!=nodeTag:
                return
            rootName=doc.getNodeText(rootNode,'name')
            nodeName=doc.getNodeText(node,'name')
            if rootName!=nodeName:
                return
        else:
            genObj['__root_node']=node
            genObj['__base_node']=Hierarchy.getRootNode(doc,node)#vtXmlDomTree.getChild(genObj['doc'].documentElement,'prjengs')
            genObj['__rel_node']=Hierarchy.getRelBaseNode(doc,node)
    else:
        pass
        #genObj['__root_node']=node
        #genObj['__base_node']=Hierarchy.getRootNode(node)#vtXmlDomTree.getChild(genObj['doc'].documentElement,'prjengs')
        #genObj['__rel_node']=Hierarchy.getRelBaseNode(node)
    sText=doc.getNodeText(node,'__data')
    parNode=doc.getParent(node)
    parNode=doc.getParent(parNode)
    #lines=string.split(sText,os.linesep)
    lines=string.split(sText,'\n')
    scope=''
    #bStrip=True
    bStrip=False
    for l in lines:
        d=getIdx(l,PROCESSING_CMDS)
        keys=d.keys()
        keys.sort()
        iStart=0
        for k in keys:
            v=d[k]
            iAddSkip=0
            #str=string.strip(l[iStart:v[0]])
            str=l[iStart:v[0]]
            f.write(str)
            if v[3]=='\\headline':
                val=l[v[1]+1:v[2]]
                if l[v[1]]=='[':
                    j=string.find(val,']')
                    if j>0:
                        addLevel=int(val[:j])
                        val=val[j+2:]
                tmpLevel=level+addLevel
                if tmpLevel==0:
                    f.write(os.linesep+'\\section{%s}'%val)
                elif tmpLevel==1:
                    f.write(os.linesep+'\\subsection{%s}'%val)
                elif tmpLevel==2:
                    f.write(os.linesep+'\\subsubsection{%s}'%val)
                elif tmpLevel>2:
                    f.write(os.linesep+'\\paragraph{%s}'%val)
                pass
            elif v[3]=='\\process_children':
                if level==0:
                    processChilds(doc.getAttribute(node,'fid'),f,
                            doc.getParent(node),PROCESSING_CMDS,
                            level,node,genObj)
                pass
            elif v[3]=='\\process_doc':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    try:
                        levelTmp=int(addVal)
                    except:
                        levelTmp=0
                    nodeDoc=getNode(genObj['doc'],None,'Doc',
                                    genObj['ids'],getVal(l,v))
                    if nodeDoc is not None:
                        processNode(f,nodeDoc,PROCESSING_CMDS,
                                    levelTmp,genObj)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\link':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(genObj['doc'],None,'',
                                    genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                            genObj['__root_node'])
                        sTag=Hierarchy.getTagNames(doc,
                                genObj['__base_node'],relNode,nodeLink)
                        sName=doc.getNodeText(nodeLink,'name')
                        s='\\textit{%s %s} %s '%(sTag,sName,addVal)
                        s=texReplace(s)
                        f.write(s)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\link_tag':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(doc,None,'',genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                genObj['__root_node'])
                        sTag=Hierarchy.getTagNames(doc,
                                genObj['__base_node'],relNode,nodeLink)
                        s='\\textit{%s} %s '%(sTag,addVal)
                        s=texReplace(s)
                        f.write(s)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\link_name':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(doc,None,'',genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                    genObj['__root_node'])
                        sName=doc.getNodeText(nodeLink,'name')
                        #s='\\textit{%s} %s '%(sName,addVal)
                        #s=texReplace(s)
                        #s=texReplaceFull(s)
                        s=' %s %s '%(texReplaceFull(sName),texReplaceFull(addVal))
                        f.write(s)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\link_desc':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(doc,None,'',genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                    genObj['__root_node'])
                        sName=doc.getNodeText(nodeLink,'description')
                        #s='\\textit{%s} %s '%(sName,addVal)
                        #s=texReplace4Desc(s)
                        #s=texReplaceFull(s)
                        s='%s %s '%(texReplaceFull(sName),texReplaceFull(addVal))
                        f.write(s)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\link_attr':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(doc,None,'',genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                    genObj['__root_node'])
                        aNode=doc.getChild(nodeLink,string.strip(addVal))
                        if aNode is not None:
                            if len(doc.getAttribute(aNode,'fid'))>0:
                                sVal=doc.getText(aNode)
                            elif len(doc.getAttribute(aNode,'ih'))>0:
                                sVal=doc.getText(aNode)
                            elif doc.getChild(aNode,'__browse') is not None:
                                sVal=Hierarchy.applyCalcNode(doc,aNode)
                            else:
                                sVal=doc.getNodeText(aNode,'val')
                            s='\\textit{%s} '%(texReplaceFull(sVal))
                            f.write(s)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\insert_attr':
                addVal=getAdditionalInfo(l,v)
                if len(addVal)>0:
                    tupVal=getVal(l,v)
                    nodeLink=getNode(doc,None,'',genObj['ids'],tupVal)
                    if nodeLink is not None:
                        
                        relNode=Hierarchy.getRelBaseNode(doc,
                                    genObj['__root_node'])
                        strsVal=string.split(addVal,'|')
                        aNode=doc.getChild(nodeLink,string.strip(strsVal[0]))
                        if aNode is not None:
                            if len(doc.getAttribute(aNode,'fid'))>0:
                                sVal=doc.getText(aNode)
                            elif len(doc.getAttribute(aNode,'ih'))>0:
                                sVal=doc.getText(aNode)
                            elif doc.getChild(aNode,'__browse') is not None:
                                sVal=Hierarchy.applyCalcNode(doc,aNode)
                            else:
                                sVal=doc.getNodeText(aNode,'val')
                            insertAttr(f,strsVal,aNode,sVal,PROCESSING_CMDS,level,node,genObj)
                    iAddSkip=len(addVal)+1
            elif v[3]=='\\process_SFCspec':
                if level==0:
                    nodeSFC=getNode(doc,parNode,'drawSFC',
                                    genObj['ids'],getVal(l,v))
                    #for c in vtXmlDomTree.getChilds(node.parentNode):
                    #    tag=vtXmlDomTree.getNodeText(c,'tag')
                    #    if tag==val:
                    #        nodeSFC=vtXmlDomTree.getChild(c,'drawSFC')
                    if nodeSFC is not None:
                        if genObj.has_key('drawSFC'):
                            gen=genObj['drawSFC']
                            nodeSFC=doc.getChild(nodeSFC,'drawSFC')
                            if nodeSFC is not None:
                                #gen.SetNode(nodeSFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeSFC)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
                                tab=gen.GenTexTab()
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\process_SFCspecFig':
                if level==0:
                    nodeSFC=getNode(doc,parNode,'drawSFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeSFC is not None:
                        if genObj.has_key('drawSFC'):
                            gen=genObj['drawSFC']
                            nodeSFC=doc.getChild(nodeSFC,'drawSFC')
                            if nodeSFC is not None:
                                #gen.SetNode(nodeSFC,genObj['ids'],
                                #                genObj['doc'])
                                gen.SetNode(nodeSFC)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\process_SFCspecTab':
                if level==0:
                    nodeSFC=getNode(doc,parNode,'drawSFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeSFC is not None:
                        if genObj.has_key('drawSFC'):
                            gen=genObj['drawSFC']
                            nodeSFC=doc.getChild(nodeSFC,'drawSFC')
                            if nodeSFC is not None:
                                #gen.SetNode(nodeSFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeSFC)
                                tab=gen.GenTexTab()
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\reference_SFCspecFig':
                if level==0:
                    nodeSFC=getNode(doc,parNode,'drawSFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeSFC is not None:
                        if genObj.has_key('drawSFC'):
                            gen=genObj['drawSFC']
                            nodeSFC=doc.getChild(nodeSFC,'drawSFC')
                            if nodeSFC is not None:
                                #fig=gen.GenTexFigRef(nodeSFC,genObj['doc'])
                                fig=gen.GenTexFigRef(nodeSFC)
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_SFCspecTab':
                if level==0:
                    nodeSFC=getNode(doc,parNode,'drawSFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeSFC is not None:
                        if genObj.has_key('drawSFC'):
                            nodeSFC=doc.getChild(nodeSFC,'drawSFC')
                            if nodeSFC is not None:
                                gen=genObj['drawSFC']
                                #tab=gen.GenTexTabRef(nodeSFC,genObj['doc'])
                                tab=gen.GenTexTabRef(nodeSFC)
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\process_FCFig':
                if level==0:
                    nodeFC=getNode(doc,parNode,'drawFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeFC is not None:
                        if genObj.has_key('drawFC'):
                            nodeFC=doc.getChild(nodeFC,'drawFC')
                            if nodeFC is not None:
                                gen=genObj['drawFC']
                                #gen.SetNode(nodeFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeFC)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\process_FCTab':
                if level==0:
                    nodeFC=getNode(doc,parNode,'drawFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeFC is not None:
                        if genObj.has_key('drawFC'):
                            nodeFC=doc.getChild(nodeFC,'drawFC')
                            if nodeFC is not None:
                                gen=genObj['drawFC']
                                #gen.SetNode(nodeFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeFC)
                                fig=gen.GenTexTab()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_FCFig':
                if level==0:
                    nodeFC=getNode(doc,parNode,'drawFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeFC is not None:
                        if genObj.has_key('drawFC'):
                            gen=genObj['drawFC']
                            nodeFC=doc.getChild(nodeFC,'drawFC')
                            if nodeFC is not None:
                                #fig=gen.GenTexFigRef(nodeFC,genObj['doc'])
                                fig=gen.GenTexFigRef(nodeFC)
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_FCTab':
                if level==0:
                    nodeFC=getNode(doc,parNode,'drawFC',
                                    genObj['ids'],getVal(l,v))
                    if nodeFC is not None:
                        if genObj.has_key('drawFC'):
                            nodeFC=doc.getChild(nodeFC,'drawFC')
                            if nodeFC is not None:
                                gen=genObj['drawFC']
                                #tab=gen.GenTexTabRef(nodeFC,genObj['doc'])
                                tab=gen.GenTexTabRef(nodeFC)
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\process_S88':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88'):
                            gen=genObj['drawS88']
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                #gen.SetNode(nodeS88,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeS88)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
                                tab=gen.GenTexTab()
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\process_S88Fig':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #gen.SetNode(nodeS88,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeS88)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\process_S88FigTab':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #gen.SetNode(nodeS88,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeS88)
                                fig=gen.GenTexFigTab()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\process_S88Tab':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #gen.SetNode(nodeS88,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetNode(nodeS88)
                                tab=gen.GenTexTab()
                                #tab=string.replace(tab,'_','\_')
                                f.write(tab+os.linesep)
            elif v[3]=='\\process_TimingFig':
                if level==0:
                    nodeTiming=getNode(doc,parNode,'drawTiming',
                                    genObj['ids'],getVal(l,v))
                    if nodeTiming is not None:
                        if genObj.has_key('drawTiming'):
                            nodeTiming=doc.getChild(nodeTiming,'drawTiming')
                            if nodeTiming is not None:
                                gen=genObj['drawTiming']
                                #gen.SetNode(nodeFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetBaseNode(genObj['__base_node'])
                                gen.SetRelNode(genObj['__rel_node'])
                                gen.SetNode(nodeTiming)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\process_PrjEngFig':
                if level==0:
                    nodePrjEng=getNode(doc,parNode,'drawPrjEng',
                                    genObj['ids'],getVal(l,v))
                    if nodePrjEng is not None:
                        if genObj.has_key('drawPrjEng'):
                            nodePrjEng=doc.getChild(nodePrjEng,'drawPrjEng')
                            if nodePrjEng is not None:
                                gen=genObj['drawPrjEng']
                                #gen.SetNode(nodeFC,genObj['ids'],
                                #                    genObj['doc'])
                                gen.SetBaseNode(genObj['__base_node'])
                                gen.SetRelNode(genObj['__rel_node'])
                                gen.SetNode(nodePrjEng)
                                fig=gen.GenTexFig()
                                #fig=string.replace(fig,'_','\_')
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_S88Fig':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #fig=gen.GenTexFigRef(nodeS88,genObj['doc'])
                                fig=gen.GenTexFigRef(nodeS88)
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_S88FigTab':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #fig=gen.GenTexFigTabRef(nodeS88,genObj['doc'])
                                fig=gen.GenTexFigTabRef(nodeS88)
                                f.write(fig+os.linesep)
            elif v[3]=='\\reference_S88Tab':
                if level==0:
                    nodeS88=getNode(doc,parNode,'drawS88',
                                    genObj['ids'],getVal(l,v))
                    if nodeS88 is not None:
                        if genObj.has_key('drawS88state'):
                            nodeS88=doc.getChild(nodeS88,'drawS88')
                            if nodeS88 is not None:
                                gen=genObj['drawS88state']
                                #tab=gen.GenTexTabRef(nodeS88,genObj['doc'])
                                tab=gen.GenTexTabRef(nodeS88)
                                f.write(tab+os.linesep)
            elif v[3]=='\\reference_TimingFig':
                if level==0:
                    nodeTiming=getNode(doc,parNode,'drawTiming',
                                    genObj['ids'],getVal(l,v))
                    if nodeTiming is not None:
                        if genObj.has_key('drawTiming'):
                            nodeTiming=doc.getChild(nodeTiming,'drawTiming')
                            if nodeTiming is not None:
                                gen=genObj['drawTiming']
                                #tab=gen.GenTexTabRef(nodeS88,genObj['doc'])
                                gen.SetBaseNode(genObj['__base_node'])
                                gen.SetRelNode(genObj['__rel_node'])
                                tab=gen.GenTexTabRef(nodeTiming)
                                f.write(tab+os.linesep)
            elif v[3]=='\\reference_PrjEngFig':
                if level==0:
                    nodePrjEng=getNode(doc,parNode,'drawPrjEng',
                                    genObj['ids'],getVal(l,v))
                    if nodePrjEng is not None:
                        if genObj.has_key('drawPrjEng'):
                            nodePrjEng=doc.getChild(nodePrjEng,'drawPrjEng')
                            if nodePrjEng is not None:
                                gen=genObj['drawPrjEng']
                                gen.SetBaseNode(genObj['__base_node'])
                                gen.SetRelNode(genObj['__rel_node'])
                                gen.SetNode(nodePrjEng)
                                fig=gen.GenTexFigRef(nodePrjEng,genObj['__base_node'])
                                f.write(fig+os.linesep)
            elif v[3]=='\\file_reference':
                f.write(u'\\textbf{%s}'%(l[v[1]+1:v[2]]))
            elif v[3]=='\\url':
                f.write(u'\\textbf{%s}'%(l[v[1]+1:v[2]]))
            elif v[3]=='\\reference_doc':
                nodeDoc=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                if nodeDoc is not None:
                    nodeFN=doc.getChild(nodeDoc,'__fn')
                    if nodeFN is not None:
                        f.write(u'\\textit{%s}'%(string.replace(doc.getText(nodeFN),'_','\\_')))
            elif v[3]=='\\reference_docfull':
                nodeDoc=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                if nodeDoc is not None:
                    nodeDN=doc.getNodeText(nodeDoc,'__docpath')
                    nodeFN=doc.getNodeText(nodeDoc,'__fn')
                    if len(nodeDN)>0:
                        fn=os.path.join(nodeDN,nodeFN)
                    else:
                        fn=nodeFN
                    fn=string.replace(fn,'\\','\\\\')
                    fn=string.replace(fn,'_','\\_')
                    f.write(u'\\textit{%s}'%(fn))                        
            elif v[3]=='scope':
                scope=val
            elif v[3]=='\\add_contained':
                val=getVal(l,v)
                depth=0
                if len(val[0])>0:
                    try:
                        depth=int(val[0])
                    except:
                        depth=0
                try:
                    addLevel=int(val[1])
                except:
                    addLevel=0
                addContained(f,parNode,PROCESSING_CMDS,level+addLevel,genObj,depth)
            elif v[3]=='\\log':
                if level==0:
                    val=getVal(l,v)
                    if len(val[0])>0:
                        nodeLog=getNode(doc,parNode,'Log',
                                        genObj['ids'],getVal(l,v))
                        if nodeLog is not None:
                            addLog(f,nodeLog,PROCESSING_CMDS,level,genObj)
            elif v[3]=='\\rev':
                if level==0:
                    val=getVal(l,v)
                    if len(val[0])>0:
                        nodeRev=getNode(doc,parNode,'Safe',
                                        genObj['ids'],getVal(l,v))
                        if nodeRev is not None:
                            addRev(f,nodeRev,PROCESSING_CMDS,level,genObj)
            elif v[3]=='\\child_attr':
                val=getVal(l,v)
                if len(val[0])>0:
                    nodeAttr=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                    if nodeAttr is not None:
                        childAttr(f,nodeAttr,PROCESSING_CMDS,level,genObj)
            elif v[3]=='\\childs_attr':
                val=getVal2(l,v)
                iAddSkip=len(val[2])+2
                if len(val[0])>0:
                    nodeAttr=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                    if nodeAttr is not None:
                        try:
                            name=string.split(val[1],'|')[1]
                        except:
                            name=''
                        tmpStrs=string.split(val[2],'|')
                        lstElem=[]
                        lstAttr=[]
                        lstCountAttrVal=[]
                        try:
                            lstElem=string.split(tmpStrs[0],',')
                            lstAttr=string.split(tmpStrs[1],',')
                            lstCountAttrVal=string.split(tmpStrs[2],',')
                        except:
                            pass
                        childAttrs(f,nodeAttr,PROCESSING_CMDS,level,genObj,
                                    lstElem,lstAttr,lstCountAttrVal,name)
            elif v[3]=='\\hierarchy_down':
                val=getVal(l,v)
                if len(val[0])>0:
                    nodeAttr=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                    if nodeAttr is not None:
                        addHierarchy(f,nodeAttr,PROCESSING_CMDS,level,genObj,True)
            elif v[3]=='\\hierarchy_up':
                val=getVal(l,v)
                if len(val[0])>0:
                    nodeAttr=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                    if nodeAttr is not None:
                        addHierarchy(f,nodeAttr,PROCESSING_CMDS,level,genObj,False)
            elif v[3]=='\\reference_childs_attr':
                val=getVal2(l,v)
                iAddSkip=len(val[2])+2
                if len(val[0])>0:
                    nodeAttr=getNode(doc,None,'',
                                    genObj['ids'],getVal(l,v))
                    if nodeAttr is not None:
                        childs_attr_reference(f,nodeAttr,genObj,lstElem,lstAttr,name)
            elif v[3]=='\\all_logs':
                addLog(f,parNode,PROCESSING_CMDS,level,genObj,True)
            elif v[3]=='\\all_revs':
                addRev(f,parNode,PROCESSING_CMDS,level,genObj,True)
            else:
                print 'node'
            iStart=v[2]+1+iAddSkip
        
        if bStrip:
            str=string.strip(l[iStart:])
        else:
            str=l[iStart:]
        
        if str in ['\\begin{verbatim}','\\begin{verbatim*}','\\begin{quotation}','\\begin{quotation*}']:
            bStrip=False
        elif str in ['\\end{verbatim}','\\end{verbatim*}','\\end{quotation}','\\end{quotation*}']:
            bStrip=True
        
        f.write(str+os.linesep)
def generate(tmpl,f,dictRepl,node,PROCESSING_CMDS,genObj={}):
    lines=tmpl.readlines()
    for l in lines:
        i=string.find(l,'$')
        if i>0:
            j=string.find(l,'$',i+1)
            if j>0:
                k=l[i+1:j]
                if dictRepl.has_key(k):
                    l=l[:i]+dictRepl[k]+l[j+1:]
        f.write(l)
    processNode(f,node,PROCESSING_CMDS,0,genObj)
    f.write(os.linesep+'\\end{document}'+os.linesep)
    pass
def generateNew(f,dictRepl,node,PROCESSING_CMDS,genObj={}):
    processNode(f,node,PROCESSING_CMDS,0,genObj)
    pass
