#----------------------------------------------------------------------------
# Name:         InputChoice.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: InputChoice.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked as masked

class InputChoice:

    def __init__(self, parent, pos, size,shown=True):
        w=30
        
        self.chcValue = wx.Choice(choices=[], id=wx.NewId(),
              name=u'chcValue', parent=parent, pos=pos,
              size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0)
        self.chcValue.Show(shown)
    def Set(self,dict,dictType):
        if dictType is None:
            dictType=dict
        sType=dict['type']
        if sType=='choice':
            self.chcValue.Show(True)
            keys=dictType.keys()
            keys.sort()
            choices=[]
            for k in keys:
                if k[:2]=='it':
                    self.chcValue.Append(dictType[k])
            self.chcValue.SetStringSelection(dict['val'])
    def Enable(self,state):
        self.chcValue.Show(state)
        pass
    def Clear(self):
        #self.mskValue.SetValue('')
        pass
    def Get(self):
        return self.chcValue.GetString(self.chcValue.GetSelection())
        