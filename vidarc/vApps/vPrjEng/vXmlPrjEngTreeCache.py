

class vgtrXmlPrjEngTreeCache:
    def __init__(self,doc,idName='id'):
        self.doc=doc
        self.idName=idName
        self.cache=None
        self.Clear()
    def Clear(self):
        self.bValid=False
        if self.cache is not None:
            del self.cache
        self.cache={}
        self.tree=None
        self.verbose=0
    def SetDoc(self,doc):
        self.doc=doc
    def __build__(self,ti):
        node=self.tree.GetPyData(ti)
        if node is not None:
            try:
                if self.verbose>0:
                    print '__build',self.idName,self.doc.getAttribute(node,self.idName)
                id=self.doc.getAttribute(node,self.idName)
                self.cache[id]=ti
                if self.verbose>1:
                    print '__build',len(self.cache.keys())
            except:
                pass
        triChild=self.tree.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__build__(triChild[0])
            triChild=self.tree.GetNextChild(ti,triChild[1])
    def Build(self,tree=None):
        if tree is not None:
            self.tree=tree
            self.bValid=False
            self.cache={}
        if self.tree is None:
            return
        ti=self.tree.GetRootItem()
        try:
            self.__build__(ti)
        except:
            pass
        self.bValid=True
        
    def Add(self,ti):
        if self.tree is not None:
            node=self.tree.GetPyData(ti)
            self.__build__(ti)
    def AddID(self,ti,id):
        try:
            self.cache[id]=ti
        except:
            pass
    def GetTreeItem(self,node):
        try:
            id=self.doc.getAttribute(node,self.idName)
            return self.GetTreeItemById(id)
        except:
            return None
    def GetTreeItemById(self,id):
        if self.bValid==False:
            self.Build()
        if self.bValid:
            try:
                ti=self.cache[id]
                return ti
            except:
                return None
        return None
    def SetInvalid(self):
        self.bValid=False
