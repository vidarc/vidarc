#Boa:Dialog:vAnalyseGeneralDialog
#----------------------------------------------------------------------------
# Name:         vAnalyseGeneralDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vAnalyseGeneralDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.vElementsPanel as ELEMENTS
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import sys,string
import images

def create(parent):
    return vAnalyseGeneralDialog(parent)

[wxID_VANALYSEGENERALDIALOG, wxID_VANALYSEGENERALDIALOGCBGOTO, 
 wxID_VANALYSEGENERALDIALOGCBOK, wxID_VANALYSEGENERALDIALOGLBLATTRCOUNT, 
 wxID_VANALYSEGENERALDIALOGLBLNODECOUNT, wxID_VANALYSEGENERALDIALOGLBLSUM, 
 wxID_VANALYSEGENERALDIALOGLSTATTR, wxID_VANALYSEGENERALDIALOGLSTELEMENTS, 
 wxID_VANALYSEGENERALDIALOGLSTITEM, wxID_VANALYSEGENERALDIALOGTXTATTRCOUNT, 
 wxID_VANALYSEGENERALDIALOGTXTCOUNT, wxID_VANALYSEGENERALDIALOGTXTNODECOUNT, 
 wxID_VANALYSEGENERALDIALOGTXTSUM, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vAnalyseGeneralDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VANALYSEGENERALDIALOG,
              name=u'vAnalyseGeneralDialog', parent=prnt, pos=wx.Point(356,
              120), size=wx.Size(400, 380), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vAnalyse General Dialog')
        self.SetClientSize(wx.Size(392, 353))

        self.lblNodeCount = wx.StaticText(id=wxID_VANALYSEGENERALDIALOGLBLNODECOUNT,
              label=u'Nodes', name=u'lblNodeCount', parent=self, pos=wx.Point(8,
              8), size=wx.Size(31, 13), style=0)

        self.txtNodeCount = wx.TextCtrl(id=wxID_VANALYSEGENERALDIALOGTXTNODECOUNT,
              name=u'txtNodeCount', parent=self, pos=wx.Point(48, 8),
              size=wx.Size(88, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSEGENERALDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(152, 320), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VANALYSEGENERALDIALOGCBOK)

        self.lstElements = wx.ListCtrl(id=wxID_VANALYSEGENERALDIALOGLSTELEMENTS,
              name=u'lstElements', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(176, 152), style=wx.LC_REPORT)
        self.lstElements.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstElementsListItemSelected,
              id=wxID_VANALYSEGENERALDIALOGLSTELEMENTS)
        self.lstElements.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstElementsListItemDeselected,
              id=wxID_VANALYSEGENERALDIALOGLSTELEMENTS)

        self.cbGoTo = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSEGENERALDIALOGCBGOTO,
              bitmap=wx.EmptyBitmap(16, 16), label=u'go to', name=u'cbGoTo',
              parent=self, pos=wx.Point(8, 320), size=wx.Size(76, 30), style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,
              id=wxID_VANALYSEGENERALDIALOGCBGOTO)

        self.lstItem = wx.ListCtrl(id=wxID_VANALYSEGENERALDIALOGLSTITEM,
              name=u'lstItem', parent=self, pos=wx.Point(8, 200),
              size=wx.Size(376, 104), style=wx.LC_REPORT)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstItemListItemDeselected,
              id=wxID_VANALYSEGENERALDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstItemListItemSelected,
              id=wxID_VANALYSEGENERALDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LEFT_DCLICK, self.OnLstItemLeftDclick)

        self.txtCount = wx.TextCtrl(id=wxID_VANALYSEGENERALDIALOGTXTCOUNT,
              name=u'txtCount', parent=self, pos=wx.Point(144, 8),
              size=wx.Size(100, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.lstAttr = wx.ListCtrl(id=wxID_VANALYSEGENERALDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(192, 40),
              size=wx.Size(192, 120), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VANALYSEGENERALDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VANALYSEGENERALDIALOGLSTATTR)

        self.lblAttrCount = wx.StaticText(id=wxID_VANALYSEGENERALDIALOGLBLATTRCOUNT,
              label=u'Attr', name=u'lblAttrCount', parent=self,
              pos=wx.Point(256, 8), size=wx.Size(16, 13), style=0)

        self.txtAttrCount = wx.TextCtrl(id=wxID_VANALYSEGENERALDIALOGTXTATTRCOUNT,
              name=u'txtAttrCount', parent=self, pos=wx.Point(280, 8),
              size=wx.Size(100, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.txtSum = wx.TextCtrl(id=wxID_VANALYSEGENERALDIALOGTXTSUM,
              name=u'txtSum', parent=self, pos=wx.Point(224, 168),
              size=wx.Size(160, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.lblSum = wx.StaticText(id=wxID_VANALYSEGENERALDIALOGLBLSUM,
              label=u'Sum', name=u'lblSum', parent=self, pos=wx.Point(192, 168),
              size=wx.Size(21, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.node=None
        self.doc=None
        self.vgpTree=None
        self.selElem=-1
        self.selItem=-1
        
        img=images.getApplyBitmap()
        self.cbOk.SetBitmapLabel(img)
        
        img=images.getNavBitmap()
        self.cbGoTo.SetBitmapLabel(img)
        
        self.lstElements.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,110)
        self.lstElements.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,40)
        
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,200)
        self.lstItem.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,60)
        self.lstItem.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,100)
        
        self.lstAttr.InsertColumn(0,u'Attr',wx.LIST_FORMAT_LEFT,130)
        self.lstAttr.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,40)
        
    def SetTree(self,vgpTree):
        self.vgpTree=vgpTree
    def __procNode__(self,node):
        try:
            lst=self.dElem[self.doc.getTagName(node)]
            sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
            lst.append((sHier,node))
        except:
            sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
            self.dElem[self.doc.getTagName(node)]=[(sHier,node)]
        self.iCount+=1
        childs=self.doc.getChilds(node)
        for c in childs:
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__procNode__(c)
            pass
    def __procAttr__(self,node):
        nodeType=self.doc.getChild(node, 'type')
        bSkip=False
        bSkipChild=False
        if len(self.doc.getAttribute(node,'id'))>0:
            bSkip=True
        tagName=self.doc.getTagName(node)
        if tagName[:2]=='__':
            bSkip=True
            bSkipChild=True
        if ELEMENTS.isProperty(tagName,'isSkip'):
            bSkipChild=True
        if nodeType is not None:
            if len(self.doc.getChilds(nodeType))==0:
                #bSkip=True
                bSkipChild=True
        if bSkip==False:
            try:
                lst=self.dAttr[tagName]
                sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                lst.append((sHier,node))
            except:
                sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                self.dAttr[tagName]=[(sHier,node)]
            self.iAttrCount+=1
        if bSkipChild==False:
            childs=self.doc.getChilds(node)
            for c in childs:
                self.__procAttr__(c)
    def __getCount__(self,node):
        self.iSumCount+=1
        childs=self.doc.getChilds(node)
        for c in childs:
            self.__getCount__(c)
    def SetDoc(self,doc):
        self.doc=doc
    def SetNode(self,node):
        self.node=node
        self.dElem={}
        self.dAttr={}
        
        self.iCount=0
        self.iAttrCount=0
        self.iSumCount=0
        self.__getCount__(self.node)
        self.__procNode__(self.node)
        self.__procAttr__(self.node)
        self.txtCount.SetValue('%d'%self.iSumCount)
        
        self.selItem=-1
        self.lstItem.DeleteAllItems()
        self.__updateElem__()
        self.__updateAttr__()
    def __updateElem__(self):
        self.txtNodeCount.SetValue('%d'%self.iCount)
        keys=self.dElem.keys()
        keys.sort()
        self.selElem=-1
        self.lstElements.DeleteAllItems()
        for k in keys:
            index = self.lstElements.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dElem[k])
            self.lstElements.SetStringItem(index,1,'%d'%iCount,-1)
    def __updateAttr__(self):
        self.txtAttrCount.SetValue('%d'%self.iAttrCount)
        keys=self.dAttr.keys()
        keys.sort()
        self.selAttr=-1
        self.lstAttr.DeleteAllItems()
        for k in keys:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dAttr[k])
            self.lstAttr.SetStringItem(index,1,'%d'%iCount,-1)
    def OnCbGoToButton(self, event):
        if self.selItem<0:
            return
        if self.selAttr>=0:
            it=self.lstAttr.GetItem(self.selAttr,0)
            lst=self.dAttr[it.m_text]
            node=self.doc.getParent(lst[self.selItem][1])
            id=self.doc.getAttribute(node,'id')
        else:
            if self.selElem>=0:
                it=self.lstElements.GetItem(self.selElem,0)
                lst=self.dElem[it.m_text]
                node=lst[self.selItem][1]
                id=self.doc.getAttribute(node,'id')
            else:
                return
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        if event is not None:
            event.Skip()

    def OnCbOkButton(self, event):
        self.Show(False)
        event.Skip()

    def OnLstElementsListItemSelected(self, event):
        self.selElem=event.GetIndex()
        it=self.lstElements.GetItem(self.selElem,0)
        lst=self.dElem[it.m_text]
        
        def cmpTag(a,b):
            return cmp(a[0],b[0])
        lst.sort(cmpTag)
        
        self.dAttr={}
        self.iAttrCount=0
        
        self.selItem=-1
        self.lstItem.ClearAll()
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,200)
        self.lstItem.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,60)
        self.lstItem.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,100)
        for it in lst:
            sTag=self.doc.getNodeText(it[1],'tag')
            sName=self.doc.getNodeText(it[1],'name')
            index = self.lstItem.InsertImageStringItem(sys.maxint, it[0], -1)
            self.lstItem.SetStringItem(index,1,sTag,-1)
            self.lstItem.SetStringItem(index,2,sName,-1)
            self.__procAttr__(it[1])
        self.__updateAttr__()
        event.Skip()

    def OnLstElementsListItemDeselected(self, event):
        self.selElem=-1
        self.lstItem.DeleteAllItems()
        self.dAttr={}
        self.iAttrCount=0
        self.__procAttr__(self.node)
        self.__updateAttr__()
        event.Skip()

    def OnLstItemListItemActivated(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnLstItemListItemDeselected(self, event):
        self.selItem=-1
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        self.selAttr=event.GetIndex()
        it=self.lstAttr.GetItem(self.selAttr,0)
        lst=self.dAttr[it.m_text]
        sum=0
        count=0
        bFault=False
        def cmpTag(a,b):
            return cmp(a[0],b[0])
        lst.sort(cmpTag)
        
        self.selItem=-1
        self.lstItem.ClearAll()
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,200)
        self.lstItem.InsertColumn(1,u'Attr',wx.LIST_FORMAT_LEFT,60)
        self.lstItem.InsertColumn(2,u'Val',wx.LIST_FORMAT_LEFT,100)
        
        for it in lst:
            sTag=self.doc.getTagName(it[1])
            if len(self.doc.getAttribute(it[1],'ih'))==0:
                sVal=self.doc.getNodeText(it[1],'val')
            else:
                sval=self.doc.getText(it[1])
            index = self.lstItem.InsertImageStringItem(sys.maxint, it[0], -1)
            self.lstItem.SetStringItem(index,1,sTag,-1)
            self.lstItem.SetStringItem(index,2,sVal,-1)
            count+=1
            try:
                val=float(sVal)
            except:
                val=0
                bFault=True
            sum+=val
        self.txtSum.SetValue('%12.2f (#%6d)'%(sum,count))
        
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.selElem=-1
        self.lstItem.DeleteAllItems()
        event.Skip()

    def OnLstItemListItemSelected(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnLstItemLeftDclick(self, event):
        self.OnCbGoToButton(None)
        event.Skip()
    
