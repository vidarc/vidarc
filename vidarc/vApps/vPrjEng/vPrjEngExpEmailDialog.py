#Boa:Dialog:vPrjEngExpEmailDialog
#----------------------------------------------------------------------------
# Name:         vPrjEngExpEmailDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngExpEmailDialog.py,v 1.6 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
#import cStringIO
#from vidarc.vApps.vPrjTimer.vgpPrjTimerSettings import *
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.InOut.mailBox as mailBox
import codecs,os.path,string
import threading,thread,traceback

from vidarc.vApps.vPrjEng.vDocSelDialog  import *

import images

def create(parent):
    return vPrjEngExpEmailDialog(parent)

[wxID_VPRJENGEXPEMAILDIALOG, wxID_VPRJENGEXPEMAILDIALOGGCBBBROWSEDOC, 
 wxID_VPRJENGEXPEMAILDIALOGGCBBCANCEL, wxID_VPRJENGEXPEMAILDIALOGGCBBGENERATE, 
 wxID_VPRJENGEXPEMAILDIALOGGPPROCESS, wxID_VPRJENGEXPEMAILDIALOGLBLCLIENT, 
 wxID_VPRJENGEXPEMAILDIALOGLBLPRJLONG, wxID_VPRJENGEXPEMAILDIALOGLBLPRJSHORT, 
 wxID_VPRJENGEXPEMAILDIALOGRBEXPORT, wxID_VPRJENGEXPEMAILDIALOGTXTCLIENT, 
 wxID_VPRJENGEXPEMAILDIALOGTXTDOCGRPNAME, 
 wxID_VPRJENGEXPEMAILDIALOGTXTDOCGRPNR, wxID_VPRJENGEXPEMAILDIALOGTXTPRJLONG, 
 wxID_VPRJENGEXPEMAILDIALOGTXTPRJSHORT, 
] = [wx.NewId() for _init_ctrls in range(14)]

class thdExport:
    def __init__(self,par):
        self.par=par
        self.running=self.keepGoing=False
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def Start(self,lst,gProc,iExport,func):
        self.lst=lst
        self.gProc=gProc
        self.iExport=iExport
        self.func=func
        thread.start_new_thread(self.Run, ())
    def Run(self):
        self.running=self.keepGoing=True
        try:
            self.selDocNode=None
            for sFN,sDN,sPrefixFN,self.selDocNode in self.lst:
                if self.iExport==0:
                    mailBox.export(sFN,sDN,sPrefixFN,
                            gProc=self.gProc,
                            func=self.addFile)
                elif self.iExport==1:
                    mailBox.exportEml(sFN,sDN,sPrefixFN,
                            gProc=self.gProc,
                            func=self.addFile)
                if self.keepGoing==False:
                    break
        except:
            vtLog.vtLngCallStack(None,vtLog.ERROR,
                            traceback.format_exc(),
                            origin='vgdPrjExpMain')
        self.running=self.keepGoing=False
        self.func()
    def addFile(self,title,fn):
        self.par.addFile(title,fn,self.selDocNode)

class vPrjEngExpEmailDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJENGEXPEMAILDIALOG,
              name=u'vPrjEngExpEmailDialog', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(451, 264), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Generate Tasks')
        self.SetClientSize(wx.Size(443, 237))

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGEXPEMAILDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 200),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJENGEXPEMAILDIALOGGCBBCANCEL)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGEXPEMAILDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 202),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VPRJENGEXPEMAILDIALOGGCBBGENERATE)

        self.lblPrjShort = wx.StaticText(id=wxID_VPRJENGEXPEMAILDIALOGLBLPRJSHORT,
              label=u'Project Shortcut', name=u'lblPrjShort', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(76, 13), style=0)

        self.txtPrjShort = wx.TextCtrl(id=wxID_VPRJENGEXPEMAILDIALOGTXTPRJSHORT,
              name=u'txtPrjShort', parent=self, pos=wx.Point(8, 24),
              size=wx.Size(72, 21), style=wx.TE_READONLY, value=u'')
        self.txtPrjShort.Enable(False)

        self.lblPrjLong = wx.StaticText(id=wxID_VPRJENGEXPEMAILDIALOGLBLPRJLONG,
              label=u'Longname', name=u'lblPrjLong', parent=self,
              pos=wx.Point(112, 8), size=wx.Size(50, 13), style=0)

        self.txtPrjLong = wx.TextCtrl(id=wxID_VPRJENGEXPEMAILDIALOGTXTPRJLONG,
              name=u'txtPrjLong', parent=self, pos=wx.Point(112, 24),
              size=wx.Size(320, 21), style=wx.TE_READONLY, value=u'')
        self.txtPrjLong.Enable(False)

        self.txtClient = wx.TextCtrl(id=wxID_VPRJENGEXPEMAILDIALOGTXTCLIENT,
              name=u'txtClient', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY, value=u'')

        self.lblClient = wx.StaticText(id=wxID_VPRJENGEXPEMAILDIALOGLBLCLIENT,
              label=u'Client', name=u'lblClient', parent=self, pos=wx.Point(8,
              48), size=wx.Size(26, 13), style=0)

        self.gcbbBrowseDoc = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGEXPEMAILDIALOGGCBBBROWSEDOC,
              bitmap=wx.EmptyBitmap(16, 16), label=u'DocGrp',
              name=u'gcbbBrowseDoc', parent=self, pos=wx.Point(360, 56),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseDoc.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseDocButton,
              id=wxID_VPRJENGEXPEMAILDIALOGGCBBBROWSEDOC)

        self.txtDocGrpNr = wx.TextCtrl(id=wxID_VPRJENGEXPEMAILDIALOGTXTDOCGRPNR,
              name=u'txtDocGrpNr', parent=self, pos=wx.Point(128, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY | wx.TE_CENTRE,
              value=u'')

        self.txtDocGrpName = wx.TextCtrl(id=wxID_VPRJENGEXPEMAILDIALOGTXTDOCGRPNAME,
              name=u'txtDocGrpName', parent=self, pos=wx.Point(208, 64),
              size=wx.Size(132, 21), style=wx.TE_READONLY, value=u'')

        self.rbExport = wx.RadioBox(choices=['plain', 'eml'],
              id=wxID_VPRJENGEXPEMAILDIALOGRBEXPORT, label=u'Export Style',
              majorDimension=1, name=u'rbExport', parent=self, point=wx.Point(8,
              96), size=wx.DefaultSize, style=wx.RA_SPECIFY_COLS)

        self.gpProcess = wx.Gauge(id=wxID_VPRJENGEXPEMAILDIALOGGPPROCESS,
              name=u'gpProcess', parent=self, pos=wx.Point(8, 160), range=100,
              size=wx.Size(424, 16), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.lang='en'
        self.dlgDocSel=None
        self.doc=None
        self.netPrjDoc=None
        self.selNode=None
        self.selDocNode=None
        self.thdExport=thdExport(self)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowseDoc.SetBitmapLabel(img)
    def __del__(self):
        if self.dlgDocSel is not None:
            self.dlgDocSel.Destroy()
            #self.dlgDocSel=None
        if self.netPrjDoc is not None:
            self.netPrjDoc.DelConsumer(self)
            self.netPrjDoc=None
    def Destroy(self):
        if self.dlgDocSel is not None:
            self.dlgDocSel.Destroy()
            self.dlgDocSel=None
        wx.Dialog.Destroy(self)
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def Stop(self):
        self.thdExport.Stop()
    def IsBusy(self):
        return self.thdExport.IsRunning()
    def Clear(self):
        pass
    def SetNode(self,doc,netPrjDoc,netDoc,nodeEng,nodeSettings,selNode):
        self.doc=doc
        if self.netPrjDoc is not None:
            self.netPrjDoc.DelConsumer(self)
        self.netPrjDoc=netPrjDoc
        if self.netPrjDoc is not None:
            self.netPrjDoc.AddConsumer(self,self.Clear())
        self.netDoc=netDoc
        self.nodeEng=nodeEng
        self.selNode=selNode
        node=self.netPrjDoc.GetPrjInfo()
        if node is None:
            self.txtPrjShort.SetValue('')
            self.txtPrjLong.SetValue('')
            self.txtPrjTaskFN.SetValue('')
            self.txtClient.SetValue('')
        else:
            self.prjId=self.doc.getAttribute(node,'fid')
            sShort=self.doc.getNodeText(node,'name')
            sLong=self.doc.getNodeText(node,'longname')
            sClient=self.doc.getNodeText(node,'clientshort')
            self.txtPrjShort.SetValue(sShort)
            self.txtPrjLong.SetValue(sLong)
            self.txtClient.SetValue(sClient)
        self.nodeSettings=nodeSettings
        if self.selNode is None:
            if self.nodeSettings is not None:
                sDocGrpNr=self.doc.getNodeText(nodeSettings,'docgrpnr')
                sDocGrpName=self.doc.getNodeText(nodeSettings,'docgrpname')
                self.docFN=self.doc.getNodeText(nodeSettings,'docfn')
                try:
                    if self.netDoc is not None:
                        self.selDocNode=self.netDoc.getNodeByIdNum(long(sDocGrpNr))
                except:
                    self.selDocNode=None
            else:
                sDocGrpNr=''
                sDocGrpName=''
                self.selDocNode=None
        else:
            sDocGrpNr=''
            sDocGrpName=''
            self.selDocNode=None
            sDocGrpNr=self.doc.getNodeText(self.selNode,'__docgrpnr')
            sDocGrpName=self.doc.getNodeText(self.selNode,'__docgrpname')
            self.docFN=self.doc.getNodeText(self.selNode,'__docfn')
            try:
                if self.netDoc is not None:
                    self.selDocNode=self.netDoc.getNodeByIdNum(long(sDocGrpNr))
            except:
                self.selDocNode=None
            
        self.txtDocGrpNr.SetValue(sDocGrpNr)
        self.txtDocGrpNr.SetStyle(0, len(sDocGrpNr), wx.TextAttr("BLACK", "WHITE"))
        self.txtDocGrpName.SetValue(sDocGrpName)
        self.txtDocGrpName.SetStyle(0, len(sDocGrpName), wx.TextAttr("BLACK", "WHITE"))
        
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        if self.thdExport.IsRunning():
            self.thdExport.Stop()
            event.Skip()
            return
        self.EndModal(0)
        event.Skip()
        
    def __getHierarchy__(self,baseNode,n):
        if baseNode is None:
            return ''
        if baseNode!=n:
            return self.__getHierarchy__(self.doc,baseNode,self.doc.getParent(n))+'<'+self.doc.getNodeText(n,'tag')+'>'#n.tagName
        else:
            return ''
    def OnGcbbGenerateButton(self, event):
        if self.nodeSettings is None:
            return
        if self.nodeEng is None:
            return
        if self.selDocNode is None:
            return
        sShort=self.txtPrjShort.GetValue()
        sLong=self.txtPrjLong.GetValue()
        sClient=self.txtClient.GetValue()
        if len(sShort)<=0 and len(sLong)<=0:
            return
        #sPrjDN=self.doc.getNodeText(self.nodeSettings,'projectdn')
        iRes,sPrjDN=self.netPrjDoc.GetProjectPath()
        vtLog.vtLngCurWX(vtLog.INFO,'prjDN:%s'%(sPrjDN),self)
        sDocGrpNrGlobal=self.doc.getNodeText(self.nodeSettings,'docgrpnr')
        sDN=self.doc.getNodeText(self.nodeSettings,'docpath')
        sPrefixFN=self.doc.getNodeText(self.nodeSettings,'prefixfn')
        lst=[]
        if self.selNode is None:
            eMailNodes=self.doc.getChildsRec(self.doc.getBaseNode(),'eMailBox')
            if len(eMailNodes)>0:
                for eMailNode in eMailNodes:
                    try:
                        sDocGrpNr=self.doc.getNodeText(eMailNode,'__docgrpnr')
                        if len(sDocGrpNr)>0:
                            id=long(sDocGrpNr)
                        else:
                            id=long(sDocGrpNrGlobal)
                        node=self.netDoc.getNodeByIdNum(id)
                        iRes,sDocPath,sPrefixFN=self.netPrjDoc.GetDocumentPathPrefixFN(self.netDoc,node)
                    except:
                        vtLog.vtLngCallStack(None,vtLog.ERROR,
                            traceback.format_exc(),
                            origin='vgdPrjExpMain')
                        continue
                    childs=self.doc.getChildsNoAttr(eMailNode,self.doc.attr)
                    for c in childs:
                        sTag=self.doc.getTagName(c)
                        if sTag[:-2]==u'file':
                            sFN=self.doc.getNodeText(c,'val')
                            self.gcbbGenerate.Enable(False)
                            #self.gccbCancel.Enable(False)
                            self.iPrefixFN=len(sPrefixFN)
                            self.iPrjDN=len(sPrjDN)
                            lst.append([sFN,os.path.join(sPrjDN,sDocPath),
                                        sPrefixFN,node])
        else:
            try:
                sDocGrpNr=self.doc.getNodeText(self.selNode,'__docgrpnr')
                if len(sDocGrpNr)>0:
                    id=long(sDocGrpNr)
                else:
                    id=long(sDocGrpNrGlobal)
                node=self.netDoc.getNodeByIdNum(id)
                iRes,sDocPath,sPrefixFN=self.netPrjDoc.GetDocumentPathPrefixFN(self.netDoc,node)
            except:
                vtLog.vtLngCallStack(None,vtLog.ERROR,
                            traceback.format_exc(),
                            origin='vgdPrjExpMain')
                event.Skip()
                return
            childs=self.doc.getChildsNoAttr(self.selNode,self.doc.attr)
            for c in childs:
                sTag=self.doc.getTagName(c)
                if sTag[:-2]==u'file':
                    sFN=self.doc.getNodeText(c,'val')
                    self.gcbbGenerate.Enable(False)
                    #self.gccbCancel.Enable(False)
                    self.iPrefixFN=len(sPrefixFN)
                    self.iPrjDN=len(sPrjDN)
                    lst.append([sFN,os.path.join(sPrjDN,sDocPath),
                                sPrefixFN,node])
        self.thdExport.Start(lst,self.gpProcess,
                                self.rbExport.GetSelection(),
                                self.exportFinished)
            
        #self.EndModal(1)
        event.Skip()
    def addFile(self,title,fn,node):
        try:
            iExt=string.rfind(fn,'.')
            if iExt<0:
                iExt=len(fn)
                sRev='0'
            else:
                sRev=fn[iExt+1:]
            sTitle=title[self.iPrefixFN:]
        except:
            sTitle=title
            sRev='0'
        self.netPrjDoc.SetDocument(self.netDoc,node,
                sTitle,sRev,u'.'+fn[self.iPrjDN:])
    def exportFinished(self):
        self.gcbbGenerate.Enable(True)
        self.EndModal(1)
        
    def SetLang(self,lang):
        self.lang=lang
    def OnGcbbBrowseDocButton(self, event):
        if self.dlgDocSel is None:
            self.dlgDocSel=vDocSelDialog(self)
            self.dlgDocSel.SetLanguage(self.lang)
            #self.dlgDocSel.SetDocFN(self.docFN)
            self.dlgDocSel.SetDoc(self.netDoc)
        self.dlgDocSel.Centre()
        ret=self.dlgDocSel.ShowModal()
        if ret==1:
            node=self.dlgDocSel.GetSelNode()
            self.selDocNode=node
            if node is not None:
                self.txtDocGrpNr.SetValue(self.doc.getAttribute(node,'id'))
                self.txtDocGrpName.SetValue(self.doc.getNodeTextLang(node,
                                    'title',self.lang))
                iRes,sPrjPath=self.netPrjDoc.GetProjectPath()
                iRes,sDocPath,sPrefixFN=self.netPrjDoc.GetDocumentPathPrefixFN(self.netDoc,node)
                if self.selNode is None:
                    self.doc.setNodeText(self.nodeSettings,'docgrpnr',self.doc.getAttribute(node,'id'))
                    self.doc.setNodeText(self.nodeSettings,'docgrpname',
                            self.doc.getNodeTextLang(node,'title',self.lang))
                    self.doc.setNodeText(self.nodeSettings,'docpath',sDocPath)
                    self.doc.setNodeText(self.nodeSettings,'prefixfn',sPrefixFN)
                else:
                    self.doc.setNodeText(self.selNode,'__docgrpnr',self.doc.getAttribute(node,'id'))
                    self.doc.setNodeText(self.selNode,'__docgrpname',
                            self.doc.getNodeTextLang(node,'title',self.lang))
                    
                    #self.doc.setNodeText(self.selNode,'projectpath',sPrjPath)
                    #self.doc.setNodeText(self.selNode,'docpath',sDocPath)
                    #self.doc.setNodeText(self.selNode,'prefixfn',sPrefixFN)
                    self.doc.AlignNode(self.selNode)
                self.doc.setNodeText(self.nodeSettings,'projectpath',sPrjPath)
                self.doc.AlignNode(self.nodeSettings)
                    
            pass
        event.Skip()
    
