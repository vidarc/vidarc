#Boa:FramePanel:vAttrShortPanel
#----------------------------------------------------------------------------
# Name:         vAttrShortPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vAttrShortPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

from vidarc.vApps.vPrjEng.vgdElemAttr import *
from vidarc.vApps.vPrjEng.InputValues import *
from vidarc.vApps.vPrjEng.vgdTypeInheritance import *
import string,sys,copy

import images

[wxID_VATTRSHORTPANEL, wxID_VATTRSHORTPANELGCBBADD, 
 wxID_VATTRSHORTPANELGCBBDEL, wxID_VATTRSHORTPANELGCBBSET, 
 wxID_VATTRSHORTPANELGCBBTYPE, wxID_VATTRSHORTPANELLBLATTR, 
 wxID_VATTRSHORTPANELLSTATTR, wxID_VATTRSHORTPANELSCWDATA, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vAttrShortPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VATTRSHORTPANEL,
              name=u'vAttrShortPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(494, 267), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(486, 240))
        self.SetAutoLayout(True)

        self.lstAttr = wx.ListView(id=wxID_VATTRSHORTPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(232, 200), style=wx.LC_REPORT)
        self.lstAttr.SetConstraints(LayoutAnchors(self.lstAttr, True, True,
              True, True))
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VATTRSHORTPANELLSTATTR)

        self.gcbbType = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRSHORTPANELGCBBTYPE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Type', name=u'gcbbType',
              parent=self, pos=wx.Point(400, 144), size=wx.Size(76, 30),
              style=0)
        self.gcbbType.SetConstraints(LayoutAnchors(self.gcbbType, False, True,
              True, False))
        self.gcbbType.Bind(wx.EVT_BUTTON, self.OnGcbbTypeButton,
              id=wxID_VATTRSHORTPANELGCBBTYPE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRSHORTPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(400, 8), size=wx.Size(76, 30), style=0)
        self.gcbbSet.SetConstraints(LayoutAnchors(self.gcbbSet, False, True,
              True, False))
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VATTRSHORTPANELGCBBSET)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRSHORTPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(400, 48), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.SetConstraints(LayoutAnchors(self.gcbbAdd, False, True,
              True, False))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VATTRSHORTPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRSHORTPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(400, 88), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.SetConstraints(LayoutAnchors(self.gcbbDel, False, True,
              True, False))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VATTRSHORTPANELGCBBDEL)

        self.lblAttr = wx.StaticText(id=wxID_VATTRSHORTPANELLBLATTR,
              label=u'Attribute', name=u'lblAttr', parent=self, pos=wx.Point(16,
              8), size=wx.Size(39, 13), style=0)

        self.scwData = wx.ScrolledWindow(id=wxID_VATTRSHORTPANELSCWDATA,
              name=u'scwData', parent=self, pos=wx.Point(240, 32),
              size=wx.Size(152, 200), style=wx.HSCROLL | wx.VSCROLL)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.dlgAttr=None
        self.dlgType=None
        self.inputs=[]
        self.attrName=''
        self.dictAttributes={}
        self.selAttrIdx=-1
        
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_RIGHT,150)
        
        self.inpValue=InputValues(parent=self,pos=wx.Point(100, 8),
                size=wx.Size(135,21))
        self.__genInputs__()
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getEditBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getTypeBitmap()
        self.gcbbType.SetBitmapLabel(img)
        
        self.bModified=False
    def __setModified__(self,state):
        self.bModified=state
    def SetNode(self,name,node,ids,doc):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you realy want to loose modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_NO:
                self.GetNode(self.node)
        self.__setModified__(False)
        self.doc=doc
        self.node=node
        self.name=name
        self.ids=ids
        self.inpValue.Clear()
        
        self.dictAttr=None
        self.dictAttributes={}
        if node is not None:
            nodes=vtXmlDomTree.getChilds(node)
            self.lstAttr.DeleteAllItems()
            for n in nodes:
                id=n.getAttribute('id')
                if len(id)>0:
                    continue
                tag=n.tagName
                if tag in ['tag','name','description']:
                    continue
                if tag[:2]=='__':
                    continue
                sVal=vtXmlDomTree.getNodeText(n,'val')
                self.dictAttributes[tag]={'val':sVal}
                
                attrs=vtXmlDomTree.getChilds(n)
                for a in attrs:
                    self.dictAttributes[tag][a.tagName]=vtXmlDomTree.getText(a)
            self.__addElementAttr__()
        self.__genInputs__()
    def GetNode(self,node,doc=None):
        if doc is None:
            doc=self.doc
        if doc is None:
            doc=vtXmlDomTree.__getDocument__(node)
        if doc is None:
            return
        keys=self.dictAttributes.keys()
        keys.sort()
        for k in keys:   
            child=vtXmlDomTree.getChild(node,k)
            if child is not None:
                nv=child
            else:
                nv=doc.createElement(k)
                node.appendChild(nv)
                child=nv
        
            d=self.dictAttributes[k]
            if d is None:
                node.removeChild(child)
                continue
            keys=d.keys()
            keys.sort()
            for k in keys:
                if d[k] is None:
                    continue
                child=vtXmlDomTree.getChild(nv,k)
                if child is not None:
                    vtXmlDomTree.setNodeText(nv,k,d[k])
                else:                
                    a=doc.createElement(k)
                    a.appendChild(doc.createTextNode(d[k]))
                    nv.appendChild(a)
        vtXmlDomTree.vtXmlDomAlignNode(doc,node)
        self.__setModified__(False)
    def Apply(self):
        self.GetNode(self.node,self.doc)
    def Cancel(self):
        self.__setModified__(False)
        self.SetNode(self.name,self.node,self.ids,self.doc)
    def __doUpdateTypeHeridatage__(self,attrName):
        if attrName=='type':
            attrKeys=self.dictAttributes.keys()
            newType=self.dictAttributes['type']['type']
            for attrKey in attrKeys:
                if attrKey=='type':
                    continue
                try:
                    attrDict=self.dictAttributes[attrKey]
                    val=attrDict['inheritance_type']
                    if attrDict['type']!=newType:
                        #do change type
                        oldType=attrDict['type']
                        attrDict['type']=newType
                        
                except:
                    pass
    def getAttrValStr(self,key):
        d=self.dictAttributes[key]
        df=d
        try:
            s=self.dictAttributes[key]['inheritance']
            strs=string.split(s,',')
            for s in strs:
                if s=='type':
                    df=self.dictAttributes['inheritance_type']
                    break
        except:
            pass
        t=type(self.__getVal__(d,'val',''))
        val=self.__getVal__(d,'val','')
        fmt=self.__getFormat__(df)
        #s=fmt%val
        #print key,s,fmt%val,type(fmt),fmt,type(val),val
        return fmt%val
    def __getVal__(self,d,k,dftVal):
        sType=d['type']
        try:
            if sType=='int':
                return int(d[k])
            elif sType=='long':
                return long(d[k])
            elif sType=='float':
                return float(d[k])
            elif sType=='pseudofloat':
                return float(d[k])
            elif sType=='text':
                return d[k]
            elif sType=='string':
                return d[k]
            elif sType=='choice':
                return d[k]
            elif sType=='choiceint':
                return int(d[k])
            elif sType=='datetime':
                return d[k]
            else:
                return d[k]
        except:
            return dftVal
    def __getFormat__(self,d):
        sType=d['type']
        try:
            if sType=='int':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='long':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='float':
                try:
                    #print 'float',d
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    #print sd,sc
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='pseudofloat':
                try:
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='text':
                return "%s"
            elif sType=='string':
                return "%s"
            elif sType=='choice':
                return "%s"
            elif sType=='choiceint':
                return "%d"
            elif sType=='datetime':
                return "%s"
            else:
                return "%s"
        except:
            return "%s"
    
    def __setVal__(self,d,k,val,dftVal):
        sType=d['type']
        try:
            if sType=='int':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='long':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='float':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='pseudofloat':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='text':
                d[k]=val
            elif sType=='string':
                d[k]=val
            elif sType=='choice':
                d[k]=val
            elif sType=='choiceint':
                d[k]=self.__getFormat__(d)%val
            elif sType=='datetime':
                d[k]=val
            else:
                d[k]=val
        except:
            d[k]=dftVal
    def __getValues__(self,d):
        lst=[]
        sType=d['type']
        if sType in ['int','long','float','pseudofloat']:
            lst.append(('min','0'))
            lst.append(('max','100'))
            lst.append(('val','0'))
        elif sType in ['text','string','choice']:
            lst.append(('val',''))
        elif sType=='choiceint':
            lst.append(('val','0'),('min','0'),('max','100'))
        elif sType=='datetime':
            lst.append(('val',''))
        else:
            pass
        return lst
    def __cpValues__(self,oldDict,newDict):
        lst=self.__getValues__(oldDict)
        for k,v in lst:
            try:
                self.__setVal__(newDict,k,self.__getVal__(oldDict,k,v),v)
            except:
                pass
    def __setupType__(self):
        inherDict=self.dictAttributes['inheritance_type']
        oType=inherDict['type']
        sType=self.dictAttributes['type']['val']
        d=copy.deepcopy(inherDict)
        if sType=='int':
            d['type']='int'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='long':
            d['type']='long'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='float':
            d['type']='float'
            d['val']='0'
            d['min']='0'
            d['max']='100'
            d['digits']='6'
            d['comma']='2'
        elif sType=='pseudofloat':
            d['type']='pseudofloat'
            d['val']='0'
            d['min']='0'
            d['max']='100'
            d['digits']='6'
            d['comma']='2'
        elif sType=='text':
            d['type']='text'
            d['val']='0'
            d['len']='8'
        elif sType=='string':
            d['type']='string'
        elif sType=='choice':
            d['type']='choice'
            d['val']='0'
            #,'it00':'item  0','it01':'item  1',
            #        'it02':'item  2','it03':'item  3','it04':'item  4',
            #        'it05':'item  5','it06':'item  6','it07':'item  7',
            #        'it08':'item  8','it09':'item  9','it10':'item 10',}
        elif sType=='choiceint':
            d['type']='choice'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        elif sType=='datetime':
            d['type']='datetime'
            d['val']='0'
            d['min']='0'
            d['max']='100'
        else:
            pass
        self.__cpValues__(inherDict,d)
        self.dictAttributes['inheritance_type']=d
        keys=self.dictAttributes.keys()
        for k in keys:
            try:
                s=self.dictAttributes[k]['inheritance']
                strs=string.split(s,',')
                for s in strs:
                    if s=='type':
                        val=self.__getVal__(self.dictAttributes[k],'val','0')
                        self.dictAttributes[k]['type']=sType
                        self.__setVal__(self.dictAttributes[k],'val',val,'0')
                        val=self.__getVal__(self.dictAttributes[k],'val','0')        
            except:
                pass
        self.__addElementAttr__()
    def __genInputs__(self):
        keys=self.dictAttributes.keys()
        keys.sort()
        i=0
        for k in keys:
            if k=='type':
                continue
            elif string.find(k,'inheritance')==0:
                continue
            if i<len(self.inputs):
                inpTup=self.inputs[i]
                inpTup[0].SetLabel(k)
            else:
                lbl=wx.StaticText(id=wx.NewId(), label=k,
                            name=u'Attr%02d'%i, parent=self.scwData, 
                            pos=wx.Point(4, 4+i*30),
                            size=wx.Size(39, 13), style=0)
                inp=InputValues(parent=self.scwData,pos=wx.Point(100, 4+i*30),
                            size=wx.Size(135,21))
                self.inputs.append((lbl,inp))
            i=i+1
        self.scwData.SetScrollbars(5,5,200,8+i*30,0,0)
    def __addElementAttr__(self):
        self.lstAttr.DeleteAllItems()
        keys=self.dictAttributes.keys()
        keys.sort()
        for k in keys:
            if k=='type':
                continue
            elif string.find(k,'inheritance')==0:
                continue
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            sVal=self.dictAttributes[k]['val']
            sVal=self.getAttrValStr(k)
            self.lstAttr.SetStringItem(index,1,sVal,-1)
            
    def __setAttrValue__(self):
        try:
            s=self.dictAttributes[self.attrName]['inheritance']
            strs=string.split(s,',')
            strs.index('type')
            inheritanceType=self.dictAttributes['inheritance_type']
        except:
            inheritanceType=None
        self.inpValue.Set(self.dictAttributes[self.attrName],inheritanceType)

    def OnGcbbTypeButton(self, event):
        if self.dlgType is None:
            self.dlgType=vgdTypeInheritance(self)
        self.dlgType.Centre()
        self.dlgType.Set(self.attrName,
                    copy.deepcopy(self.dictAttributes['inheritance_type']),
                    self.dictAttributes['type'])
        ret=self.dlgType.ShowModal()
        if ret==1:
            sType,self.dictType=self.dlgType.Get()
            self.dictAttributes['inheritance_type']=self.dictType
            if sType!=self.dictAttributes['type']['val']:
                self.dictAttributes['type']['val']=sType
                self.__setupType__()
                self.__setModified__(True)
        event.Skip()

    def OnGcbbAddButton(self, event):
        if self.dlgAttr is None:
            self.dlgAttr=vgdElemAttr(self)
        if self.selAttrIdx<0:
            try:
                self.attrName='undefined'
                d={'inheritance':'type','type':self.dictAttributes['type']['val'],'val':''}
                self.dlgAttr.Set('undefined',d,
                            self.dictAttributes['inheritance_type'])
            except:
                return
        else:
            if self.dictAttributes.has_key('inheritance_type'):
                self.dlgAttr.Set(self.attrName,
                        copy.deepcopy(self.dictAttributes[self.attrName]),
                        self.dictAttributes['inheritance_type'])
            else:
                self.dlgAttr.Set(self.attrName,
                        copy.deepcopy(self.dictAttributes[self.attrName]),
                        None)
        ret=self.dlgAttr.ShowModal()
        if ret==1:
            newAttrName,self.dictAttr=self.dlgAttr.Get()
            if newAttrName!=self.attrName:
                self.__setModified__(True)
                self.dictAttributes[newAttrName]=self.dictAttr
                self.__addElementAttr__()
            else:
                if self.dictAttributes[self.attrName]!=self.dictAttr:
                    self.__setModified__(True)
                    self.dictAttributes[self.attrName]=self.dictAttr
                    sVal=self.dictAttributes[self.attrName]['val']
                    self.lstAttr.SetStringItem(self.selAttrIdx,1,sVal,-1)
                    self.__setAttrValue__()
            if newAttrName=='type':
                self.__setupType__()
        event.Skip()

    def OnGcbbSetButton(self, event):
        i=self.selAttrIdx
        if i>=0:
            sVal=self.inpValue.Get()#self.txtAttrVal.GetValue()
            self.lstAttr.SetStringItem(i,1,sVal,-1)
            self.dictAttributes[self.attrName]['val']=sVal
            #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            self.__setModified__(True)
        #else:
        #    self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnGcbbDelButton(self, event):
        self.dictAttributes[self.attrName]=None
        self.__setModified__(True)
        self.lstAttr.DeleteItem(self.selAttrIdx)
        self.selAttrIdx=-1
        self.attrName=''
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        #self.txtAttrName.SetValue(it.m_text)
        k=it.m_text
        self.selAttrIdx=it.m_itemId
        try:
            self.attrName=k
        except:
            self.attrName=''
        self.__setAttrValue__()
        event.Skip()
        
