#Boa:FramePanel:vDrawSFCPanel
#----------------------------------------------------------------------------
# Name:         vDrawSFCPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vDrawSFCPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.stc
from wx.lib.anchors import LayoutAnchors
import  wx.stc  as  stc

import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.evtDataChanged import *
#from xml.dom.minidom import Element
import vidarc.vApps.vPrjEng.DocBuildLatex as DocBuildLatex

from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS
from vidarc.vApps.vPrjEng.vBrowseTransNameDialog import vBrowseTransNameDialog
from vidarc.vApps.vPrjEng.vBrowseTransValDialog import vBrowseTransValDialog
from vidarc.vApps.vPrjEng.vBrowseActionNameDialog import vBrowseActionNameDialog
from vidarc.vApps.vPrjEng.vBrowseActionValDialog import vBrowseActionValDialog
from vidarc.vApps.vPrjEng.vBrowseLinkDialog import vBrowseLinkDialog
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import vidarc.vApps.vDraw.vDrawSFCPanel as vDrawSFCspecPanel
from vidarc.vApps.vDraw.vDrawS88statePanel import vDrawS88statePanel
from vidarc.vApps.vDraw.vDrawFlowChartPanel import vDrawFlowChartPanel

import images

[wxID_VDRAWSFCPANEL] = [wx.NewId() for _init_ctrls in range(1)]


PROCESSING_CMDS=[
    "\drawing",
    "\headline",
    "\process_children"]

PROCESSING_CMDS_VIS=[
    "\drawing",
    "\headline[y]{x}",
    "\process_children"]

if wx.Platform == '__WXMSW__':
    faces = { 'times': 'Times New Roman',
              'mono' : 'Courier New',
              'helv' : 'Courier New',
              'other': 'Comic Sans MS',
              'size' : 10,
              'size2': 8,
             }
else:
    faces = { 'times': 'Times',
              'mono' : 'Courier',
              'helv' : 'Helvetica',
              'other': 'new century schoolbook',
              'size' : 12,
              'size2': 10,
             }


class vDrawSFCPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDRAWSFCPANEL, name=u'vDrawSFCPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(600, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(592, 373))
        self.SetAutoLayout(True)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.dlgDocSel=None
        self.lang='en'
        self.docFN=''
        self.prjDN=''
        
        self.vgpDrawSFCspec=vDrawSFCspecPanel.vDrawSFCPanel(id=wx.NewId(),pos=wx.Point(4,4),
                    size=wx.Size(584,365),parent=self)
        self.vgpDrawSFCspec.SetConstraints(LayoutAnchors(self.vgpDrawSFCspec, True,
              True, True, True))
        
        self.vgpDrawS88state=vDrawS88statePanel(id=wx.NewId(),pos=wx.Point(4,4),
                    size=wx.Size(584,365),parent=self)
        self.vgpDrawS88state.SetConstraints(LayoutAnchors(self.vgpDrawS88state, True,
              True, True, True))
        
        self.vgpDrawFC=vDrawFlowChartPanel(id=wx.NewId(),pos=wx.Point(4,4),
                    size=wx.Size(584,365),parent=self)
        self.vgpDrawFC.SetConstraints(LayoutAnchors(self.vgpDrawFC, True,
              True, True, True))
        
        self.dlgBrowseTrans=vBrowseTransNameDialog(self)
        self.bBrowseTrans2set=True
        self.vgpDrawSFCspec.SetStepBrowseDlg(self.dlgBrowseTrans)
        self.vgpDrawS88state.SetBrowseDlg(self.dlgBrowseTrans)
        
        self.dlgBrowseTransVal=vBrowseTransValDialog(self)
        self.bBrowseTransVal2set=True
        self.vgpDrawSFCspec.SetStepBrowseValDlg(self.dlgBrowseTransVal)
        self.vgpDrawS88state.SetBrowseValDlg(self.dlgBrowseTransVal)
        
        self.dlgBrowseAction=vBrowseActionNameDialog(self)
        self.bBrowseAction2set=True
        self.vgpDrawSFCspec.SetActionBrowseDlg(self.dlgBrowseAction)
        
        self.dlgBrowseActionVal=vBrowseActionValDialog(self)
        self.bBrowseActionVal2set=True
        self.vgpDrawSFCspec.SetActionBrowseValDlg(self.dlgBrowseActionVal)
        
        self.dlgBrowseLink=vBrowseLinkDialog(self)
        self.bBrowseLink2set=True
        self.vgpDrawFC.SetBrowseDlg(self.dlgBrowseLink)
        
        self.vgpDrawSFCspec.Show(False)
        self.vgpDrawS88state.Show(False)
        self.vgpDrawFC.Show(False)

        self.bModified=False
    def __setModified__(self,state):
        self.bModified=state
    def GetS88draw(self):
        return self.vgpDrawS88state
    def Clear(self):
        self.__setModified__(False)
        self.vgpDrawSFCspec.Show(False)
        self.vgpDrawS88state.Show(False)
        self.vgpDrawFC.Show(False)
        self.vgpDrawSFCspec.Clear()
        self.vgpDrawS88state.Clear()
        self.vgpDrawFC.Clear()
    def SetHumans(self,humans):
        #self.chcUsr.Clear()
        self.humans=humans
        #for i in self.humans.GetKeys():
        #    self.chcUsr.Append(i)
    def SetLanguage(self,lang):
        self.lang=lang
        if self.dlgDocSel is not None:
            #self.dlgDocSel.Destroy()
            #self.dlgDocSel=None
            self.dlgDocSel.SetLanguage(self.lang)
    def SetDocFN(self,fn):
        self.docFN=fn
        if self.dlgDocSel is not None:
            self.dlgDocSel.SetDocFN(self.docFN)
    def SetPrjDN(self,dn):
        self.prjDN=dn
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
        self.dlgBrowseTrans.SetDoc(doc,bNet)
        self.dlgBrowseTransVal.SetDoc(doc,bNet)
        self.dlgBrowseAction.SetDoc(doc,bNet)
        self.dlgBrowseActionVal.SetDoc(doc,bNet)
        #self.vgpDrawSFCspec
        #self.vgpDrawS88state
        #self.vgpDrawFC
    def SetNode(self,name,node):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode()
        if self.bBrowseTrans2set==True:
            if self.dlgBrowseTrans is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgBrowseTrans.SetNode(browseNode)
                self.bBrowseTrans2set=False
        if self.bBrowseTransVal2set==True:
            if self.dlgBrowseTransVal is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgBrowseTransVal.SetNode(browseNode)
                self.bBrowseTransVal2set=False
        if self.bBrowseAction2set==True:
            if self.dlgBrowseAction is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgBrowseAction.SetNode(browseNode)
                self.bBrowseAction2set=False
        if self.bBrowseActionVal2set==True:
            if self.dlgBrowseActionVal is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgBrowseActionVal.SetNode(browseNode)
                self.bBrowseActionVal2set=False
        if self.bBrowseLink2set==True:
            if self.dlgBrowseLink is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgBrowseLink.SetNode(browseNode)
                self.bBrowseLink2set=False
        relNode=Hierarchy.getRelBaseNode(self.doc,node)
        if self.dlgBrowseTrans is not None:
            self.dlgBrowseTrans.SetRelNode(relNode)
        if self.dlgBrowseTransVal is not None:
            self.dlgBrowseTransVal.SetRelNode(relNode)
        if self.dlgBrowseAction is not None:
            self.dlgBrowseAction.SetRelNode(relNode)
        if self.dlgBrowseActionVal is not None:
            self.dlgBrowseActionVal.SetRelNode(relNode)
        
        self.__setModified__(False)
        self.node=node
        self.name=name
        self.vgpDrawSFCspec.Clear()
        self.bFoundSFC=False
        self.bFoundS88=False
        self.bFoundFC=False
        self.drawSFCnode=None
        self.drawS88node=None
        self.drawFCnode=None
        if self.node is not None:
            for e in ELEMENTS:
                if e[0]==self.doc.getTagName(self.node):
                    if e[1].has_key('__properties'):
                        for prop in e[1]['__properties']:
                            if prop=='isDrawSFC':
                                self.bFoundSFC=True
                                break
                            if prop=='isDrawS88':
                                self.bFoundS88=True
                                break
                            if prop=='isDrawFC':
                                self.bFoundFC=True
                                break
                    else:
                        break
            if self.bFoundSFC==True:
                self.drawSFCnode=self.doc.getChild(self.node,'drawSFC')
                #if self.drawSFCnode is None:
                #    self.drawSFCnode=vtXmlDomTree.createSubNode(self.node,'drawSFC',doc,False)
            else:
                self.drawSFCnode=None
            if self.drawSFCnode is not None:
                self.vgpDrawSFCspec.SetNode(self.drawSFCnode)
            else:
                self.vgpDrawSFCspec.Clear()
            if self.bFoundS88==True:
                self.drawS88node=self.doc.getChild(self.node,'drawS88')
            else:
                self.drawS88node=None
            if self.drawS88node is not None:
                self.vgpDrawS88state.SetNode(self.drawS88node)
            else:
                self.vgpDrawS88state.Clear()
            if self.bFoundFC==True:
                self.drawFCnode=self.doc.getChild(self.node,'drawFC')
            else:
                self.drawFCnode=None
            if self.drawFCnode is not None:
                self.vgpDrawFC.SetNode(self.drawFCnode)
            else:
                self.vgpDrawFC.Clear()
            
        if self.bFoundSFC==True:
            self.vgpDrawSFCspec.Show(True)
        else:
            self.vgpDrawSFCspec.Show(False)
        if self.bFoundS88==True:
            self.vgpDrawS88state.Show(True)
        else:
            self.vgpDrawS88state.Show(False)
        if self.bFoundFC==True:
            self.vgpDrawFC.Show(True)
        else:
            self.vgpDrawFC.Show(False)
        #if bFound:
        #    self.sctData.Show(True)
        #else:
        #    self.sctData.Show(False)
    def GetNode(self):
        if self.doc is None:
            return
        if self.bFoundSFC:
            if self.drawSFCnode is None:
                self.drawSFCnode=self.doc.createSubNode(self.node,'drawSFC',False)
                if self.ids is not None:
                    self.ids.AddMissing(self.drawSFCnode)
                    self.ids.ProcessMissing()
                    self.ids.ClearMissing()
            self.vgpDrawSFCspec.GetNode(self.drawSFCnode)
        if self.bFoundS88:
            if self.drawS88node is None:
                self.drawS88node=self.doc.createSubNode(self.node,'drawS88',False)
                if self.ids is not None:
                    self.ids.AddMissing(self.drawS88node)
                    self.ids.ProcessMissing()
                    self.ids.ClearMissing()
            self.vgpDrawS88state.GetNode(self.drawS88node)
        if self.bFoundFC:
            if self.drawFCnode is None:
                self.drawFCnode=self.doc.createSubNode(self.node,'drawFC',False)
                if self.ids is not None:
                    self.ids.AddMissing(self.drawFCnode)
                    self.ids.ProcessMissing()
                    self.ids.ClearMissing()
            self.vgpDrawFC.GetNode(self.drawFCnode)
        pass
    def Apply(self):
        if self.node is not None:
            self.GetNode()
            #sText=self.sctData.GetText()
            #vtXmlDomTree.setNodeText(self.node,'__data',sText,self.doc)
            #vtXmlDomTree.vtXmlDomAlignNode(self.doc,self.node)
            pass
        self.__setModified__(False)
    def Cancel(self):
        self.__setModified__(False)
    
