# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-12-13 15:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: vPrjEngMainFrame.py:163 vPrjEngMainFrame.py:169
msgid "  ... Config"
msgstr ""

#: vPrjEngMainFrame.py:630
msgid " (undef*)"
msgstr ""

#: vPrjEngMainFrame.py:617
msgid "%b-%d-%Y   %H:%M:%S wk:%W"
msgstr ""

#: vPrjEngMainFrame.py:306
msgid "&Analyse"
msgstr ""

#: vPrjEngMainFrame.py:304
msgid "&File"
msgstr ""

#: vPrjEngMainFrame.py:159
msgid "&New"
msgstr ""

#: vPrjEngMainFrame.py:161
msgid "&Open"
msgstr ""

#: vPrjEngMainFrame.py:165
msgid "&Save"
msgstr ""

#: vPrjEngMainFrame.py:307
msgid "&Tools"
msgstr ""

#: vPrjEngMainFrame.py:305
msgid "&View"
msgstr ""

#: vPrjEngAppl.py:88
msgid "--config <filename>"
msgstr ""

#: vPrjEngAppl.py:91
msgid "--file <filename>"
msgstr ""

#: vPrjEngAppl.py:94
msgid "--help"
msgstr ""

#: vPrjEngAppl.py:97
msgid "--lang <language id according ISO639>"
msgstr ""

#: vPrjEngAppl.py:98
msgid "--log <level>"
msgstr ""

#: vPrjEngAppl.py:87
msgid "-c <filename>"
msgstr ""

#: vPrjEngAppl.py:90
msgid "-f <filename>"
msgstr ""

#: vPrjEngAppl.py:99
msgid "0 = debug"
msgstr ""

#: vPrjEngAppl.py:100
msgid "1 = information"
msgstr ""

#: vPrjEngAppl.py:101
msgid "2 = waring"
msgstr ""

#: vPrjEngAppl.py:102
msgid "3 = error"
msgstr ""

#: vPrjEngAppl.py:103
msgid "4 = critical"
msgstr ""

#: vPrjEngAppl.py:104
msgid "5 = fatal"
msgstr ""

#: vPrjEngMainFrame.py:218
msgid "Activities"
msgstr ""

#: vPrjEngMainFrame.py:297
msgid "Align XML"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:159
msgid "Close"
msgstr ""

#: vPrjEngMainFrame.py:1195 vPrjEngMainFrame.py:1228 vPrjEngMainFrame.py:1248
#: vPrjEngMainFrame.py:1661 vPrjEngMainFrame.py:1683
msgid "Close connection to use this feautre."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:206
msgid "Config"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:213
msgid "Connect"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:142
msgid "Connection"
msgstr ""

#: vPrjEngMainFrame.py:1195 vPrjEngMainFrame.py:1228 vPrjEngMainFrame.py:1248
#: vPrjEngMainFrame.py:1661 vPrjEngMainFrame.py:1683
msgid "Connection still active!"
msgstr ""

#: vPrjEngMainFrame.py:257
msgid "Date User Grouping "
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:220
msgid "Discon"
msgstr ""

#: vPrjEngMainFrame.py:273
msgid "Document ID Group"
msgstr ""

#: vPrjEngMainFrame.py:270
msgid "Document Normal"
msgstr ""

#: vPrjEngMainFrame.py:175
msgid "E&xit"
msgstr ""

#: vPrjEngMainFrame.py:225
msgid "Element Filter"
msgstr ""

#: vPrjEngMainFrame.py:291
msgid "Export ..."
msgstr ""

#: vPrjEngMainFrame.py:294
msgid "Import ..."
msgstr ""

#: vPrjEngMainFrame.py:222
msgid "Language ..."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:146
msgid "Logs"
msgstr ""

#: vPrjEngMainFrame.py:198 vPrjEngMainFrame.py:243
msgid "Node"
msgstr ""

#: vPrjEngMainFrame.py:246
msgid "Office"
msgstr ""

#: vPrjEngMainFrame.py:1206 vPrjEngMainFrame.py:1292 vPrjEngMainFrame.py:1669
#: ../../tool/net/vNetXmlGuiMasterDialog.py:234
msgid "Open"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:241
msgid "Save"
msgstr ""

#: vPrjEngMainFrame.py:167
msgid "Save &As"
msgstr ""

#: vPrjEngMainFrame.py:1237 vPrjEngMainFrame.py:1690
msgid "Save File As"
msgstr ""

#: vPrjEngMainFrame.py:172
msgid "Se&ttings"
msgstr ""

#: vPrjEngMainFrame.py:215
msgid "Search"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:144
msgid "Settings"
msgstr ""

#: vPrjEngMainFrame.py:260
msgid "Short Date User Grouping "
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:227
msgid "Stop"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:148
msgid "Synch"
msgstr ""

#: vPrjEngMainFrame.py:200
msgid "Task"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:1004
#: ../../tool/net/vNetXmlGuiMasterDialog.py:1022
msgid "Timeout on pausing"
msgstr ""

#: vPrjEngMainFrame.py:220
msgid "Tree..."
msgstr ""

#: vPrjEngMainFrame.py:1496
msgid "Unsaved data present! Do you want to save data?"
msgstr ""

#: vPrjEngMainFrame.py:263
msgid "User Date Grouping"
msgstr ""

#: vPrjEngMainFrame.py:266
msgid "User short Date Grouping"
msgstr ""

#: vPrjEngMainFrame.py:359 vPrjEngMainFrame.py:627
msgid "VIDARC Project Engineering"
msgstr ""

#: vPrjEngMainFrame.py:228
msgid "View Elements Table"
msgstr ""

#: vPrjEngMainFrame.py:1206 vPrjEngMainFrame.py:1237 vPrjEngMainFrame.py:1292
#: vPrjEngMainFrame.py:1669 vPrjEngMainFrame.py:1690
msgid "XML files (*.xml)|*.xml|all files (*.*)|*.*"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:129
#: ../../tool/net/vNetXmlGuiMasterDialog.py:371
msgid "access"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:345
msgid "alias"
msgstr ""

#: vPrjEngMainFrame.py:1349
msgid "align ... "
msgstr ""

#: vPrjEngMainFrame.py:1354
msgid "align finished."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:344
#: ../../tool/net/vNetXmlGuiMasterDialog.py:352
msgid "appl"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:130
msgid "blocked"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:131
msgid "closed"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:374
msgid "configuation"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:376
msgid "connection"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:128
msgid "connection closed"
msgstr ""

#: vPrjEngMainFrame.py:712
msgid "connection established ... please wait"
msgstr ""

#: vPrjEngMainFrame.py:717
msgid "connection lost."
msgstr ""

#: vPrjEngMainFrame.py:741
msgid "content got. generate ..."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:133
msgid "copy"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:134
msgid "disconnected"
msgstr ""

#: vPrjEngMainFrame.py:203
msgid "eMail"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:134
msgid "established"
msgstr ""

#: vPrjEngMainFrame.py:673
msgid "file open fault."
msgstr ""

#: vPrjEngMainFrame.py:656
msgid "file opened ..."
msgstr ""

#: vPrjEngMainFrame.py:665
msgid "file opened and parsed."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:348
msgid "fn"
msgstr ""

#: vPrjEngMainFrame.py:1388 vPrjEngMainFrame.py:1405 vPrjEngMainFrame.py:1422
#: vPrjEngMainFrame.py:1438 vPrjEngMainFrame.py:1449 vPrjEngMainFrame.py:1465
#: vPrjEngMainFrame.py:1487
msgid "generate ..."
msgstr ""

#: vPrjEngMainFrame.py:602
msgid "generate ... "
msgstr ""

#: vPrjEngMainFrame.py:1390 vPrjEngMainFrame.py:1407 vPrjEngMainFrame.py:1424
#: vPrjEngMainFrame.py:1440 vPrjEngMainFrame.py:1456 vPrjEngMainFrame.py:1472
#: vPrjEngMainFrame.py:1489
msgid "generate finished."
msgstr ""

#: vPrjEngMainFrame.py:613
msgid "generate finished. "
msgstr ""

#: vPrjEngMainFrame.py:752
msgid "generated."
msgstr ""

#: vPrjEngMainFrame.py:722
msgid "get content ... "
msgstr ""

#: vPrjEngAppl.py:84
msgid "help"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:346
msgid "host"
msgstr ""

#: vPrjEngMainFrame.py:1615
msgid "import ...."
msgstr ""

#: vPrjEngMainFrame.py:1619
msgid "import finished."
msgstr ""

#: vPrjEngMainFrame.py:1325
msgid "import node file finished."
msgstr ""

#: vPrjEngMainFrame.py:646
msgid "master started ..."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:353
msgid "msg"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:368
msgid "name"
msgstr ""

#: vPrjEngMainFrame.py:1257
msgid "new ... "
msgstr ""

#: vPrjEngMainFrame.py:1259
msgid "new finished."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:129
msgid "no access"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:130
#: ../../tool/net/vNetXmlGuiMasterDialog.py:372
msgid "notify"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:373
msgid "open"
msgstr ""

#: vPrjEngMainFrame.py:1078
msgid "open file finished."
msgstr ""

#: vPrjEngAppl.py:92
msgid "open the <filename> at start"
msgstr ""

#: vPrjEngAppl.py:89
msgid "open the <filename> to configure application"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:131
msgid "opened"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:135
#: ../../tool/net/vNetXmlGuiMasterDialog.py:377
msgid "paused"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:349
msgid "port"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:135
msgid "running"
msgstr ""

#: vPrjEngMainFrame.py:1137 vPrjEngMainFrame.py:1143
msgid "save file ..."
msgstr ""

#: vPrjEngMainFrame.py:1149
msgid "save file finished."
msgstr ""

#: vPrjEngAppl.py:95
msgid "show this screen"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:343
#: ../../tool/net/vNetXmlGuiMasterDialog.py:370
msgid "state"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:355
msgid "status"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:102
msgid "stopping thread and closing connections please wait ..."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:375
msgid "synch"
msgstr ""

#: vPrjEngMainFrame.py:702
msgid "synch aborted."
msgstr ""

#: vPrjEngMainFrame.py:694
msgid "synch finished."
msgstr ""

#: vPrjEngMainFrame.py:680 vPrjEngMainFrame.py:690
msgid "synch started ..."
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:133
msgid "synchable"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:124
msgid "thread stopped"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:354
msgid "time"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:132
msgid "unknown"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:347
msgid "usr"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:98
msgid "vNet XML Master Close Dialog"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:132
msgid "valid"
msgstr ""

#: vPrjEngAppl.py:86
msgid "valid flags:"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterDialog.py:369
msgid "value"
msgstr ""

#: vPrjEngMainFrame.py:1497
msgid "vgaPrjEng Close"
msgstr ""
