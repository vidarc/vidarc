#Boa:FramePanel:vPrjEngSettingsPanel
#----------------------------------------------------------------------------
# Name:         vPrjEngSettingsPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngSettingsPanel.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetPlaceDialog import vNetPlaceDialog

import images

[wxID_VPRJENGSETTINGSPANEL, wxID_VPRJENGSETTINGSPANELCBDOCNET, 
 wxID_VPRJENGSETTINGSPANELCBHUMANNET, wxID_VPRJENGSETTINGSPANELCBLOCNET, 
 wxID_VPRJENGSETTINGSPANELCBNET, wxID_VPRJENGSETTINGSPANELCBVSASNET, 
 wxID_VPRJENGSETTINGSPANELCHCAUTOAPPLY, wxID_VPRJENGSETTINGSPANELCHCMODE, 
 wxID_VPRJENGSETTINGSPANELGCBBBROWSEPRJDN, 
 wxID_VPRJENGSETTINGSPANELGCBBBROWSETMPLDN, 
 wxID_VPRJENGSETTINGSPANELGCBBDOCBROWSE, 
 wxID_VPRJENGSETTINGSPANELGCBBHUMANBROWSE, 
 wxID_VPRJENGSETTINGSPANELGCBBLATEXFN, wxID_VPRJENGSETTINGSPANELGCBBLOCBROWSE, 
 wxID_VPRJENGSETTINGSPANELGCBBVSASBROWSE, 
 wxID_VPRJENGSETTINGSPANELLBLBASETMPLDN, wxID_VPRJENGSETTINGSPANELLBLDOCFN, 
 wxID_VPRJENGSETTINGSPANELLBLHUMANFN, wxID_VPRJENGSETTINGSPANELLBLLATEX, 
 wxID_VPRJENGSETTINGSPANELLBLLOC, wxID_VPRJENGSETTINGSPANELLBLMODE, 
 wxID_VPRJENGSETTINGSPANELLBLPRJDN, wxID_VPRJENGSETTINGSPANELLBLVSASFN, 
 wxID_VPRJENGSETTINGSPANELTXTDOCFN, wxID_VPRJENGSETTINGSPANELTXTHUMANFN, 
 wxID_VPRJENGSETTINGSPANELTXTLATEXFN, wxID_VPRJENGSETTINGSPANELTXTLOCFN, 
 wxID_VPRJENGSETTINGSPANELTXTPRJDN, wxID_VPRJENGSETTINGSPANELTXTTMPLDN, 
 wxID_VPRJENGSETTINGSPANELTXTVSASFN, 
] = [wx.NewId() for _init_ctrls in range(30)]

#----------------------------------------------------------------------
def getBrowseData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02zIDAT8\x8d\x85\x93?hSQ\x14\xc6\x7f\xf7\xf5!\xb7\xf0\x8a\t\xb4\xf0\
\x82\x0e\xbe\xaa`\x86\x0e\xaf\xb84\x10\xc4H\x15\n\xae-M\xc1\xc1E\xb0\x8bC\
\xb7 \xb6\x12\x14D\xc5\xc1\x16\xfc\x87S\x04\x15\x1d\x04+RM\x86B:\x14\x92j$)8\
\xbc\xc1\xc0{h\xa1o\x08\xe4\x82\x8f^\x874i\x0b\xa2\x17\xbe\xe9p~\xf7p\xce\
\xf7!\x8c>\xbaJ\x9eJj\xc8k\xc8k\xf7\xb4\xab!\xaf\xf7\xd7\xff&\x83}o&;\x83\
\xfe\x9d#\x7f\x13\xeeN\xa6x\xff\xd8\xe1\xf2\xd4\x19\xadw\x164\x80\xdeY\xd0\
\x1d\xad\xean\x8f\xd9-\n\xe3\x86\x80m0\x15\xb9\xeb9\x00\x1a\x1b\r&\x06\xc7\
\x98u\x8b\x14\xd6\xa7\xf4\xdct\x19\x19sY\xfc\x90\xe7M\xe9\x9a\xde\xa8<\x10}\
\x90\x99\xd7\xed\x1c\xe6\xa1/\xf3\xd2J0\xec\x98XV\x08\xf43d\x0f\x91L\x9eG\
\x1c\xbf\x00\xd6\x00\xbf~\x06\x98\xd2$y8b\xf4\xc8\x10\x85O\xb7\xe7\x05\x94\
\xf5\xe7w\xdb\xa4\xcef\x90Q\x15\xaf\xea\x11\xc6}bV\x12\xe7D\x1c\xb0\x81\x04 \
\x01(\xae\x14\xf1\xbe\xbd\xc0\x19t8w\t\x0ca\xa4\xc5Z\xa9\x80\x0cC\x88<\x9c\
\x11\x1b\xf7\xa8K\x828k\xaf+\x14KU\xc2\xad2\xe0\x01\x8a\xccx\x86\xa0\xe5P\
\xadU\x80Lg\x89\xc5\r\x05RB\x04D\x014\x03d\xe436\x92 \xe5H\xbc\xcd5\xde>_\
\x04\x1a@\x80\xf7\xc3\xa3\xb8\xa9\x10FZt\x00\xa5Qh5 \x94\x10\xb4\tb\x12\xa5$\
(E\xb8\xe5\x93\x94\xe0\x9eL\x11\x04\n\xa8\xe3\x07>\x8d\xef\t\x80\xee\x193,\
\xaf\x14@)\x14\x10S\nIH\x10\xb5\xb1M\x08\xb1q,E\xbdV$\x08B\xa4%\xf16\x9d=\
\x800\xd2\xa2\xb8\xee\xf5\xfc [\x1eD\x1e1\x13\x14`\x9b\xfd(\xda@\x05\xbf\xb9\
Lc\xbd\xbc{v\xf6\x8ct\xefI\n\x800\x02L\x1be\xda\x1d\x18\xfd\x04Q\x1bd\x1c\
\xa5Rx^\x19\x19K\xf4>\xdb\xe7\xc4\x0csK\x05\xa4\nP\xbb\x8d\x00\x8a61\x13P\
\xdb$\xc2\x06^\xb33\xe9\xd5+\xaf\xf4\xb1\xe1\xa6\xee\x01\x84\x91\x16\xf7\x9f\
}\x14\xf14\xe4\xef,\x12\xb6\xbc\xde\x04D\n\x00_I\x1a\xb5\x19\xe4\x80DZe&\xc6\
\xcb\x07\xb3\xd0\xb5\xf4\xad\x97_E<\rs\xbb\xa0\x10\t\x91"\xb4\x14\xf5Z\x1d\
\xc7vP\xad\x14K\x8f&\xc5?\x93&\x8c> \xafg\xa7]\xed\xaf>\xd4\xf5\xe5\x9c\x96R\
\xea\xec\xf4\xd3^J\xff\x0b\xd8\x0f\xca^t\xb5mg\x0fD\xfc\x0f\xe7R\x0f5\x91\
\xbd\xa0@\x00\x00\x00\x00IEND\xaeB`\x82' 

def getBrowseBitmap():
    return wx.BitmapFromImage(getBrowseImage())

def getBrowseImage():
    stream = cStringIO.StringIO(getBrowseData())
    return wx.ImageFromStream(stream)


class vPrjEngSettingsPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJENGSETTINGSPANEL,
              name=u'vPrjEngSettingsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(488, 294), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(480, 267))

        self.lblDocFN = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLDOCFN,
              label=u'Document Filename', name=u'lblDocFN', parent=self,
              pos=wx.Point(2, 8), size=wx.Size(94, 13), style=0)

        self.lblVsasFN = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLVSASFN,
              label=u'VSAS', name=u'lblVsasFN', parent=self, pos=wx.Point(8,
              32), size=wx.Size(28, 13), style=0)

        self.lblHumanFN = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLHUMANFN,
              label=u'Human Filename', name=u'lblHumanFN', parent=self,
              pos=wx.Point(8, 56), size=wx.Size(79, 13), style=0)

        self.txtDocFN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTDOCFN,
              name=u'txtDocFN', parent=self, pos=wx.Point(96, 8),
              size=wx.Size(216, 21), style=0, value=u'')

        self.txtVsasFN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTVSASFN,
              name=u'txtVsasFN', parent=self, pos=wx.Point(96, 32),
              size=wx.Size(216, 21), style=0, value=u'')

        self.txtHumanFN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTHUMANFN,
              name=u'txtHumanFN', parent=self, pos=wx.Point(96, 56),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbDocBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBDOCBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbDocBrowse', parent=self, pos=wx.Point(320, 8),
              size=wx.Size(76, 22), style=0)
        self.gcbbDocBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbDocBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBDOCBROWSE)

        self.gcbbVsasBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBVSASBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbVsasBrowse', parent=self, pos=wx.Point(320, 32),
              size=wx.Size(76, 22), style=0)
        self.gcbbVsasBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbVsasBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBVSASBROWSE)

        self.gcbbHumanBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBHUMANBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbHumanBrowse', parent=self, pos=wx.Point(320, 56),
              size=wx.Size(76, 22), style=0)
        self.gcbbHumanBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbHumanBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBHUMANBROWSE)

        self.lblLoc = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLLOC,
              label=u'Location Filename', name=u'lblLoc', parent=self,
              pos=wx.Point(8, 81), size=wx.Size(86, 13), style=0)

        self.txtLocFN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTLOCFN,
              name=u'txtLocFN', parent=self, pos=wx.Point(96, 81),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbLocBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBLOCBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbLocBrowse', parent=self, pos=wx.Point(320, 81),
              size=wx.Size(76, 22), style=0)
        self.gcbbLocBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbLocBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBLOCBROWSE)

        self.lblBaseTmplDN = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLBASETMPLDN,
              label=u'Base Template Dir', name=u'lblBaseTmplDN', parent=self,
              pos=wx.Point(2, 106), size=wx.Size(87, 13), style=0)

        self.txtTmplDN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTTMPLDN,
              name=u'txtTmplDN', parent=self, pos=wx.Point(96, 106),
              size=wx.Size(216, 21), style=0, value=u'')

        self.lblPrjDN = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLPRJDN,
              label=u'Project Directory', name=u'lblPrjDN', parent=self,
              pos=wx.Point(2, 132), size=wx.Size(78, 13), style=0)

        self.txtPrjDN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTPRJDN,
              name=u'txtPrjDN', parent=self, pos=wx.Point(96, 132),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbBrowseTmplDN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBBROWSETMPLDN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseTmplDN', parent=self, pos=wx.Point(320, 106),
              size=wx.Size(76, 22), style=0)
        self.gcbbBrowseTmplDN.Bind(wx.EVT_BUTTON, self.OnGcbbTmplDNBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBBROWSETMPLDN)

        self.gcbbBrowsePrjDN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBBROWSEPRJDN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowsePrjDN', parent=self, pos=wx.Point(320, 132),
              size=wx.Size(76, 22), style=0)
        self.gcbbBrowsePrjDN.Bind(wx.EVT_BUTTON, self.OnGcbbPrjDNBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBBROWSEPRJDN)

        self.lblMode = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLMODE,
              label=u'Mode', name=u'lblMode', parent=self, pos=wx.Point(32,
              192), size=wx.Size(27, 13), style=0)

        self.chcMode = wx.Choice(choices=[],
              id=wxID_VPRJENGSETTINGSPANELCHCMODE, name=u'chcMode', parent=self,
              pos=wx.Point(96, 192), size=wx.Size(130, 21), style=0)

        self.txtLatexFN = wx.TextCtrl(id=wxID_VPRJENGSETTINGSPANELTXTLATEXFN,
              name=u'txtLatexFN', parent=self, pos=wx.Point(96, 160),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbLatexFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELGCBBLATEXFN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbLatexFN', parent=self, pos=wx.Point(320, 160),
              size=wx.Size(76, 22), style=0)
        self.gcbbLatexFN.Bind(wx.EVT_BUTTON, self.OnGcbbLatexBrowseButton,
              id=wxID_VPRJENGSETTINGSPANELGCBBLATEXFN)

        self.lblLatex = wx.StaticText(id=wxID_VPRJENGSETTINGSPANELLBLLATEX,
              label=u'Latex Template', name=u'lblLatex', parent=self,
              pos=wx.Point(2, 160), size=wx.Size(73, 13), style=0)

        self.cbDocNet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELCBDOCNET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Net', name=u'cbDocNet',
              parent=self, pos=wx.Point(400, 8), size=wx.Size(76, 22), style=0)
        self.cbDocNet.Bind(wx.EVT_BUTTON, self.OnCbDocNetButton,
              id=wxID_VPRJENGSETTINGSPANELCBDOCNET)

        self.cbVSASNet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELCBVSASNET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Net', name=u'cbVSASNet',
              parent=self, pos=wx.Point(400, 32), size=wx.Size(76, 22),
              style=0)
        self.cbVSASNet.Bind(wx.EVT_BUTTON, self.OnCbVSASNetButton,
              id=wxID_VPRJENGSETTINGSPANELCBVSASNET)

        self.cbHumanNet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELCBHUMANNET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Net', name=u'cbHumanNet',
              parent=self, pos=wx.Point(400, 56), size=wx.Size(76, 22),
              style=0)
        self.cbHumanNet.Bind(wx.EVT_BUTTON, self.OnCbHumanNetButton,
              id=wxID_VPRJENGSETTINGSPANELCBHUMANNET)

        self.cbLocNet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELCBLOCNET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Net', name=u'cbLocNet',
              parent=self, pos=wx.Point(400, 80), size=wx.Size(76, 22),
              style=0)
        self.cbLocNet.Bind(wx.EVT_BUTTON, self.OnCbLocNetButton,
              id=wxID_VPRJENGSETTINGSPANELCBLOCNET)

        self.cbNet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGSETTINGSPANELCBNET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Net', name=u'cbNet',
              parent=self, pos=wx.Point(400, 192), size=wx.Size(76, 22),
              style=0)
        self.cbNet.Bind(wx.EVT_BUTTON, self.OnCbNetButton,
              id=wxID_VPRJENGSETTINGSPANELCBNET)

        self.chcAutoApply = wx.CheckBox(id=wxID_VPRJENGSETTINGSPANELCHCAUTOAPPLY,
              label=u'Auto Apply', name=u'chcAutoApply', parent=self,
              pos=wx.Point(96, 224), size=wx.Size(73, 13), style=0)
        self.chcAutoApply.SetValue(False)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.doc=None
        self.docFN=None
        self.vsasFN=None
        self.humanFN=None
        self.locFN=None
        self.tmplDN=None
        self.prjDN=None
        self.latexFN=None
        self.bModified=False
        
        img=getBrowseBitmap()
        
        self.gcbbDocBrowse.SetBitmapLabel(img)
        self.gcbbVsasBrowse.SetBitmapLabel(img)
        self.gcbbHumanBrowse.SetBitmapLabel(img)
        self.gcbbLocBrowse.SetBitmapLabel(img)
        self.gcbbBrowseTmplDN.SetBitmapLabel(img)
        self.gcbbBrowsePrjDN.SetBitmapLabel(img)
        self.gcbbLatexFN.SetBitmapLabel(img)
        
        img=images.getUndefBitmap()
        self.cbDocNet.SetBitmapLabel(img)
        self.cbVSASNet.SetBitmapLabel(img)
        self.cbHumanNet.SetBitmapLabel(img)
        self.cbLocNet.SetBitmapLabel(img)
        self.cbNet.SetBitmapLabel(img)
        
        self.dlgNet=None
    def __setModified__(self):
        self.bModified=True
    def SetFileDoc(self,fn):
        self.docFN=fn
        self.txtDocFN.SetValue(self.docFN)
        self.__setModified__()
    def SetFileVsas(self,fn):
        self.vsasFN=fn
        self.txtVsasFN.SetValue(self.vsasFN)
        self.__setModified__()
    def SetFileHuman(self,fn):
        self.humanFN=fn
        self.txtHumanFN.SetValue(self.humanFN)
        self.__setModified__()
    def SetFileLoc(self,fn):
        self.locFN=fn
        self.txtLocFN.SetValue(self.locFN)
        self.__setModified__()
    def SetDirTmpl(self,dn):
        self.tmplDN=dn
        self.txtTmplDN.SetValue(self.tmplDN)
        self.__setModified__()
    def SetDirPrj(self,dn):
        self.prjDN=dn
        self.txtPrjDN.SetValue(self.prjDN)
        self.__setModified__()
    def SetFileLatex(self,fn):
        self.latexFN=fn
        self.txtLatexFN.SetValue(self.latexFN)
        self.__setModified__()
    def SetMode(self,mode):
        try:
            idx=MODES.index(mode)
            self.chcMode.SetSelection(idx)
        except:
            self.chcMode.SetSelection(0)
        self.__setModified__()
    def SetAutoApply(self,mode):
        if mode=='1':
            self.chcAutoApply.SetValue(True)
        else:
            self.chcAutoApply.SetValue(False)
        self.__setModified__()
    def SetXml(self,doc,node):
        self.doc=doc
        self.node=node
        self.SetFileDoc(self.doc.getNodeText(node,'docfn'))
        self.SetFileVsas(self.doc.getNodeText(node,'vsasfn'))
        self.SetFileHuman(self.doc.getNodeText(node,'humanfn'))
        self.SetFileLoc(self.doc.getNodeText(node,'locationfn'))
        self.SetDirTmpl(self.doc.getNodeText(node,'templatedn'))
        self.SetDirPrj(self.doc.getNodeText(node,'projectdn'))
        self.SetFileLatex(self.doc.getNodeText(node,'latexfn'))
        self.SetMode(self.doc.getNodeText(node,'mode'))
        self.SetAutoApply(self.doc.getNodeText(node,'auto_apply'))
        
        def setupNet(node,tag,appl):
            n=self.doc.createSubNode(node,tag,False)
            self.doc.createSubNodeText(n,'appl',appl,False)
            
            for tag in ['alias','host','port','usr','passwd']:
                self.doc.createSubNodeText(n,tag,'',False)
            self.doc.AlignNode(n,iRec=2)
        n=self.doc.getChild(node,'doc')
        if n is None:
            setupNet(node,'doc','vDoc')
        n=self.doc.getChild(node,'vsas')
        if n is None:
            setupNet(node,'vsas','vVSAS')
        n=self.doc.getChild(node,'human')
        if n is None:
            setupNet(node,'human','vHum')
        n=self.doc.getChild(node,'location')
        if n is None:
            setupNet(node,'location','vLoc')
        n=self.doc.getChild(node,'server')
        if n is None:
            setupNet(node,'server','vPrjEng')
        
    def GetXml(self,node):
        self.doc.setNodeText(node,'docfn',self.txtDocFN.GetValue())
        self.doc.setNodeText(node,'vsasfn',self.txtVsasFN.GetValue())
        self.doc.setNodeText(node,'humanfn',self.txtHumanFN.GetValue())
        self.doc.setNodeText(node,'locationfn',self.txtLocFN.GetValue())
        self.doc.setNodeText(node,'templatedn',self.txtTmplDN.GetValue())
        self.doc.setNodeText(node,'projectdn',self.txtPrjDN.GetValue())
        self.doc.setNodeText(node,'latexfn',self.txtLatexFN.GetValue())
        m=self.chcMode.GetString(self.chcMode.GetSelection())
        self.doc.setNodeText(node,'mode',m)
        if self.chcAutoApply.GetValue():
            val='1'
        else:
            val='0'
        self.doc.setNodeText(node,'auto_apply',val)
    def OnGcbbDocBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.docFN is not None:
                dlg.SetDirectory(self.docFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileDoc(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbVsasBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.vsasFN is not None:
                dlg.SetDirectory(self.vsasFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileVsas(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbHumanBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.humanFN is not None:
                dlg.SetDirectory(self.humanFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileHuman(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
        

    def OnGcbbLocBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.locFN is not None:
                dlg.SetDirectory(self.locFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileLoc(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbTmplDNBrowseButton(self, event):
        #'open'
        dlg = wx.DirDialog(self, "Choose the template directory:",
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.tmplDN is not None:
                dlg.SetPath(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetDirTmpl(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbPrjDNBrowseButton(self, event):
        #'open'
        dlg = wx.DirDialog(self, "Choose the project directory:",
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.prjDN is not None:
                dlg.SetPath(self.prjDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetDirPrj(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def OnGcbbLatexBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "Latex files (*.tex)|*.tex", wx.OPEN)
        try:
            if self.latexFN is not None:
                dlg.SetDirectory(self.latexFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileLatex(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnCbDocNetButton(self, event):
        if self.dlgNet is None:
            self.dlgNet=vNetPlaceDialog(self)
        self.dlgNet.Centre()
        n=self.doc.getChild(self.node,'doc')
        self.dlgNet.SetNode(self.doc,n)
        iRet=self.dlgNet.ShowModal()
        if iRet==2:
            self.cbDocNet.SetBitmapLabel(images.getRunningBitmap())
        else:
            self.cbDocNet.SetBitmapLabel(images.getStoppedBitmap())
        if iRet>0:
            self.dlgNet.GetNode(n,'doc')
        event.Skip()

    def OnCbVSASNetButton(self, event):
        if self.dlgNet is None:
            self.dlgNet=vNetPlaceDialog(self)
        self.dlgNet.Centre()
        n=self.doc.getChild(self.node,'vsas')
        self.dlgNet.SetNode(self.doc,n)
        iRet=self.dlgNet.ShowModal()
        if iRet==2:
            self.cbVSASNet.SetBitmapLabel(images.getRunningBitmap())
        else:
            self.cbVSASNet.SetBitmapLabel(images.getStoppedBitmap())
        if iRet>0:
            self.dlgNet.GetNode(n,'vsas')
        event.Skip()

    def OnCbHumanNetButton(self, event):
        if self.dlgNet is None:
            self.dlgNet=vNetPlaceDialog(self)
        self.dlgNet.Centre()
        n=self.doc.getChild(self.node,'human')
        self.dlgNet.SetNode(self.doc,n)
        iRet=self.dlgNet.ShowModal()
        if iRet==2:
            self.cbHumanNet.SetBitmapLabel(images.getRunningBitmap())
        else:
            self.cbHumanNet.SetBitmapLabel(images.getStoppedBitmap())
        if iRet>0:
            self.dlgNet.GetNode()
        event.Skip()

    def OnCbLocNetButton(self, event):
        if self.dlgNet is None:
            self.dlgNet=vNetPlaceDialog(self)
        self.dlgNet.Centre()
        n=self.doc.getChild(self.node,'location')
        self.dlgNet.SetNode(self.doc,n)
        iRet=self.dlgNet.ShowModal()
        if iRet==2:
            self.cbLocNet.SetBitmapLabel(images.getRunningBitmap())
        else:
            self.cbLocNet.SetBitmapLabel(images.getStoppedBitmap())
        if iRet>0:
            self.dlgNet.GetNode()
        event.Skip()

    def OnCbNetButton(self, event):
        if self.dlgNet is None:
            self.dlgNet=vNetPlaceDialog(self)
        self.dlgNet.Centre()
        n=self.doc.getChild(self.node,'server')
        self.dlgNet.SetNode(self.doc,n)
        iRet=self.dlgNet.ShowModal()
        if iRet==2:
            self.cbNet.SetBitmapLabel(images.getRunningBitmap())
        else:
            self.cbNet.SetBitmapLabel(images.getStoppedBitmap())
        if iRet>0:
            self.dlgNet.GetNode()
        event.Skip()
    
