#Boa:Dialog:vPrjEngCanvasDialog
#----------------------------------------------------------------------------
# Name:         vPrjEngCanvasDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngCanvasDialog.py,v 1.2 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.masked.numctrl

import sys,traceback

import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vPrjEng.vPrjEngTreeListAttr import *
from vidarc.tool.draw.vtDrawCanvasBlockIO import *

import images

def create(parent):
    return vPrjEngCanvasDialog(parent)

[wxID_VPRJENGCANVASDIALOG, wxID_VPRJENGCANVASDIALOGCBADDLAYER, 
 wxID_VPRJENGCANVASDIALOGCBAPPLY, wxID_VPRJENGCANVASDIALOGCBCANCEL, 
 wxID_VPRJENGCANVASDIALOGCBDELLAYER, wxID_VPRJENGCANVASDIALOGCBDOWN, 
 wxID_VPRJENGCANVASDIALOGCBOK, wxID_VPRJENGCANVASDIALOGCBUP, 
 wxID_VPRJENGCANVASDIALOGCHCFONT, wxID_VPRJENGCANVASDIALOGCHCLAYER, 
 wxID_VPRJENGCANVASDIALOGCHCNAME, wxID_VPRJENGCANVASDIALOGCHCVAL, 
 wxID_VPRJENGCANVASDIALOGCHKADDED, wxID_VPRJENGCANVASDIALOGCHKAUTO, 
 wxID_VPRJENGCANVASDIALOGCHKENABLE, wxID_VPRJENGCANVASDIALOGCHKROTATE, 
 wxID_VPRJENGCANVASDIALOGLBLFONT, wxID_VPRJENGCANVASDIALOGLBLGRID, 
 wxID_VPRJENGCANVASDIALOGLBLLBLNAME, wxID_VPRJENGCANVASDIALOGLBLMAX, 
 wxID_VPRJENGCANVASDIALOGLBLSCALE, wxID_VPRJENGCANVASDIALOGLBLXMLLEVEL, 
 wxID_VPRJENGCANVASDIALOGLSTATTRS, wxID_VPRJENGCANVASDIALOGLSTVAL, 
 wxID_VPRJENGCANVASDIALOGNBEDIT, wxID_VPRJENGCANVASDIALOGNUMSCALE, 
 wxID_VPRJENGCANVASDIALOGNUMSCALEX, wxID_VPRJENGCANVASDIALOGNUMSCALEY, 
 wxID_VPRJENGCANVASDIALOGPGPROCESS, wxID_VPRJENGCANVASDIALOGPNGEN, 
 wxID_VPRJENGCANVASDIALOGPNLABELS, wxID_VPRJENGCANVASDIALOGPNREP, 
 wxID_VPRJENGCANVASDIALOGPNVAL, wxID_VPRJENGCANVASDIALOGPNXML, 
 wxID_VPRJENGCANVASDIALOGSCALEVIS, wxID_VPRJENGCANVASDIALOGSNBSCALEX, 
 wxID_VPRJENGCANVASDIALOGSNVAL, wxID_VPRJENGCANVASDIALOGSPBSCALEY, 
 wxID_VPRJENGCANVASDIALOGSPGRIDX, wxID_VPRJENGCANVASDIALOGSPGRIDY, 
 wxID_VPRJENGCANVASDIALOGSPMAXX, wxID_VPRJENGCANVASDIALOGSPMAXY, 
 wxID_VPRJENGCANVASDIALOGSPXMLLEVELS, wxID_VPRJENGCANVASDIALOGTXTLBLNAME, 
 wxID_VPRJENGCANVASDIALOGTXTNAME, wxID_VPRJENGCANVASDIALOGTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(46)]

class vPrjEngCanvasDialog(wx.Dialog):
    ADDED_TREE_IDX     = 3
    ADDED_MARKER       = u'X'
    NOT_ADDED_MARKER   = u''
    
    def _init_coll_nbEdit_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=False,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnXml, select=True, text=u'XML')
        parent.AddPage(imageId=-1, page=self.pnVal, select=False,
              text=u'Objects')
        parent.AddPage(imageId=-1, page=self.pnLabels, select=False,
              text=u'Layer')
        parent.AddPage(imageId=-1, page=self.pnRep, select=False,
              text=u'Report')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJENGCANVASDIALOG,
              name=u'vPrjEngCanvasDialog', parent=prnt, pos=wx.Point(333, 170),
              size=wx.Size(423, 332), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vtDrawCanvasXml Dialog')
        self.SetClientSize(wx.Size(415, 305))
        self.Bind(wx.EVT_ACTIVATE, self.OnDialogActivate)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(104, 272), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VPRJENGCANVASDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(216, 272), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VPRJENGCANVASDIALOGCBCANCEL)

        self.nbEdit = wx.Notebook(id=wxID_VPRJENGCANVASDIALOGNBEDIT,
              name=u'nbEdit', parent=self, pos=wx.Point(8, 8), size=wx.Size(400,
              256), style=0)

        self.pnGen = wx.Panel(id=wxID_VPRJENGCANVASDIALOGPNGEN, name=u'pnGen',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblGrid = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLGRID,
              label=u'Grid', name=u'lblGrid', parent=self.pnGen, pos=wx.Point(8,
              14), size=wx.Size(19, 13), style=0)

        self.spGridX = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSPGRIDX,
              initial=0, max=100, min=0, name=u'spGridX', parent=self.pnGen,
              pos=wx.Point(50, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridX.Bind(wx.EVT_TEXT, self.OnSpGridXText,
              id=wxID_VPRJENGCANVASDIALOGSPGRIDX)

        self.spGridY = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSPGRIDY,
              initial=0, max=100, min=0, name=u'spGridY', parent=self.pnGen,
              pos=wx.Point(110, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridY.Bind(wx.EVT_TEXT, self.OnSpGridYText,
              id=wxID_VPRJENGCANVASDIALOGSPGRIDY)

        self.pnVal = wx.Panel(id=wxID_VPRJENGCANVASDIALOGPNVAL, name=u'pnVal',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.chcName = wx.Choice(choices=[], id=wxID_VPRJENGCANVASDIALOGCHCNAME,
              name=u'chcName', parent=self.pnVal, pos=wx.Point(4, 36),
              size=wx.Size(200, 21), style=wx.CB_SORT)
        self.chcName.Bind(wx.EVT_CHOICE, self.OnChcNameChoice,
              id=wxID_VPRJENGCANVASDIALOGCHCNAME)

        self.txtName = wx.TextCtrl(id=wxID_VPRJENGCANVASDIALOGTXTNAME,
              name=u'txtName', parent=self.pnVal, pos=wx.Point(4, 4),
              size=wx.Size(200, 21), style=0, value=u'')

        self.cbUp = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBUP,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Up', name=u'cbUp',
              parent=self.pnVal, pos=wx.Point(216, 4), size=wx.Size(64, 30),
              style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VPRJENGCANVASDIALOGCBUP)

        self.cbDown = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBDOWN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Down', name=u'cbDown',
              parent=self.pnVal, pos=wx.Point(216, 36), size=wx.Size(65, 30),
              style=0)
        self.cbDown.Bind(wx.EVT_BUTTON, self.OnCbDownButton,
              id=wxID_VPRJENGCANVASDIALOGCBDOWN)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self.pnVal, pos=wx.Point(280, 70), size=wx.Size(65, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VPRJENGCANVASDIALOGCBAPPLY)

        self.lstVal = wx.ListCtrl(id=wxID_VPRJENGCANVASDIALOGLSTVAL,
              name=u'lstVal', parent=self.pnVal, pos=wx.Point(176, 104),
              size=wx.Size(216, 120), style=wx.LC_REPORT)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstValListItemSelected, id=wxID_VPRJENGCANVASDIALOGLSTVAL)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstValListItemDeselected,
              id=wxID_VPRJENGCANVASDIALOGLSTVAL)

        self.lblMax = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLMAX,
              label=u'Max', name=u'lblMax', parent=self.pnGen, pos=wx.Point(8,
              54), size=wx.Size(20, 13), style=0)

        self.spMaxX = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSPMAXX, initial=0,
              max=1000, min=0, name=u'spMaxX', parent=self.pnGen,
              pos=wx.Point(50, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxX.Bind(wx.EVT_TEXT, self.OnSpMaxXText,
              id=wxID_VPRJENGCANVASDIALOGSPMAXX)

        self.spMaxY = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSPMAXY, initial=0,
              max=1000, min=0, name=u'spMaxY', parent=self.pnGen,
              pos=wx.Point(110, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxY.Bind(wx.EVT_TEXT, self.OnSpMaxYText,
              id=wxID_VPRJENGCANVASDIALOGSPMAXY)

        self.pnLabels = wx.Panel(id=wxID_VPRJENGCANVASDIALOGPNLABELS,
              name=u'pnLabels', parent=self.nbEdit, pos=wx.Point(0, 0),
              size=wx.Size(392, 230), style=wx.TAB_TRAVERSAL)

        self.chcLayer = wx.Choice(choices=[],
              id=wxID_VPRJENGCANVASDIALOGCHCLAYER, name=u'chcLayer',
              parent=self.pnLabels, pos=wx.Point(40, 8), size=wx.Size(130, 21),
              style=0)
        self.chcLayer.Bind(wx.EVT_CHOICE, self.OnChcLabelChoice,
              id=wxID_VPRJENGCANVASDIALOGCHCLAYER)

        self.txtLblName = wx.TextCtrl(id=wxID_VPRJENGCANVASDIALOGTXTLBLNAME,
              name=u'txtLblName', parent=self.pnLabels, pos=wx.Point(56, 40),
              size=wx.Size(72, 21), style=0, value=u'')

        self.lblLblName = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLLBLNAME,
              label=u'Name', name=u'lblLblName', parent=self.pnLabels,
              pos=wx.Point(8, 44), size=wx.Size(28, 13), style=0)

        self.cbAddLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBADDLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddLayer',
              parent=self.pnLabels, pos=wx.Point(200, 8), size=wx.Size(60, 30),
              style=0)
        self.cbAddLayer.Bind(wx.EVT_BUTTON, self.OnCbAddLayerButton,
              id=wxID_VPRJENGCANVASDIALOGCBADDLAYER)

        self.cbDelLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJENGCANVASDIALOGCBDELLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelLayer',
              parent=self.pnLabels, pos=wx.Point(272, 8), size=wx.Size(60, 30),
              style=0)
        self.cbDelLayer.Bind(wx.EVT_BUTTON, self.OnCbDelLayerButton,
              id=wxID_VPRJENGCANVASDIALOGCBDELLAYER)

        self.pnRep = wx.Panel(id=wxID_VPRJENGCANVASDIALOGPNREP, name=u'pnRep',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblScale = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLSCALE,
              label=u'Scale', name=u'lblScale', parent=self.pnRep,
              pos=wx.Point(8, 12), size=wx.Size(27, 13), style=0)

        self.numScale = wx.lib.masked.numctrl.NumCtrl(id=wxID_VPRJENGCANVASDIALOGNUMSCALE,
              name=u'numScale', parent=self.pnRep, pos=wx.Point(48, 8),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScale.SetFractionWidth(2)
        self.numScale.SetIntegerWidth(2)

        self.lblFont = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLFONT,
              label=u'Font', name=u'lblFont', parent=self.pnRep, pos=wx.Point(8,
              44), size=wx.Size(21, 13), style=0)

        self.chcFont = wx.Choice(choices=[u'micro', u'smal', u'normal', u'big',
              u'huge'], id=wxID_VPRJENGCANVASDIALOGCHCFONT, name=u'chcFont',
              parent=self.pnRep, pos=wx.Point(48, 40), size=wx.Size(100, 21),
              style=0)
        self.chcFont.SetSelection(2)

        self.chkRotate = wx.CheckBox(id=wxID_VPRJENGCANVASDIALOGCHKROTATE,
              label=u'rotate', name=u'chkRotate', parent=self.pnRep,
              pos=wx.Point(208, 8), size=wx.Size(73, 13), style=0)
        self.chkRotate.SetValue(False)

        self.numScaleX = wx.lib.masked.numctrl.NumCtrl(id=wxID_VPRJENGCANVASDIALOGNUMSCALEX,
              name=u'numScaleX', parent=self.pnGen, pos=wx.Point(50, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleX.SetIntegerWidth(2)
        self.numScaleX.SetInsertionPoint(14)
        self.numScaleX.SetFractionWidth(2)

        self.numScaleY = wx.lib.masked.numctrl.NumCtrl(id=wxID_VPRJENGCANVASDIALOGNUMSCALEY,
              name=u'numScaleY', parent=self.pnGen, pos=wx.Point(134, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleY.SetIntegerWidth(2)
        self.numScaleY.SetInsertionPoint(14)
        self.numScaleY.SetFractionWidth(2)

        self.spbScaleY = wx.SpinButton(id=wxID_VPRJENGCANVASDIALOGSPBSCALEY,
              name=u'spbScaleY', parent=self.pnGen, pos=wx.Point(186, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.spbScaleY.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleYSpinDown,
              id=wxID_VPRJENGCANVASDIALOGSPBSCALEY)
        self.spbScaleY.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleYSpinUp,
              id=wxID_VPRJENGCANVASDIALOGSPBSCALEY)

        self.snbScaleX = wx.SpinButton(id=wxID_VPRJENGCANVASDIALOGSNBSCALEX,
              name=u'snbScaleX', parent=self.pnGen, pos=wx.Point(98, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.snbScaleX.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleXSpinDown,
              id=wxID_VPRJENGCANVASDIALOGSNBSCALEX)
        self.snbScaleX.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleXSpinUp,
              id=wxID_VPRJENGCANVASDIALOGSNBSCALEX)

        self.ScaleVis = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGSCALEVIS,
              label=u'Scale', name=u'ScaleVis', parent=self.pnGen,
              pos=wx.Point(8, 94), size=wx.Size(27, 13), style=0)

        self.txtVal = wx.TextCtrl(id=wxID_VPRJENGCANVASDIALOGTXTVAL,
              name=u'txtVal', parent=self.pnVal, pos=wx.Point(176, 76),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTxtValText,
              id=wxID_VPRJENGCANVASDIALOGTXTVAL)

        self.snVal = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSNVAL, initial=0,
              max=100, min=0, name=u'snVal', parent=self.pnVal,
              pos=wx.Point(176, 74), size=wx.Size(100, 21),
              style=wx.SP_ARROW_KEYS)
        self.snVal.Show(False)
        self.snVal.Bind(wx.EVT_TEXT, self.OnSnValText,
              id=wxID_VPRJENGCANVASDIALOGSNVAL)

        self.chkEnable = wx.CheckBox(id=wxID_VPRJENGCANVASDIALOGCHKENABLE,
              label=_(u'visible'), name=u'chkEnable', parent=self.pnVal,
              pos=wx.Point(310, 40), size=wx.Size(73, 13), style=0)
        self.chkEnable.SetValue(False)
        self.chkEnable.Bind(wx.EVT_CHECKBOX, self.OnChkEnableCheckbox,
              id=wxID_VPRJENGCANVASDIALOGCHKENABLE)

        self.chcVal = wx.Choice(choices=[], id=wxID_VPRJENGCANVASDIALOGCHCVAL,
              name=u'chcVal', parent=self.pnVal, pos=wx.Point(176, 74),
              size=wx.Size(100, 21), style=0)
        self.chcVal.Show(False)
        self.chcVal.Bind(wx.EVT_CHOICE, self.OnChcValChoice,
              id=wxID_VPRJENGCANVASDIALOGCHCVAL)

        self.pnXml = wx.Panel(id=wxID_VPRJENGCANVASDIALOGPNXML, name=u'pnXml',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lstAttrs = wx.ListCtrl(id=wxID_VPRJENGCANVASDIALOGLSTATTRS,
              name=u'lstAttrs', parent=self.pnVal, pos=wx.Point(8, 64),
              size=wx.Size(160, 160), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstAttrs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrsListItemSelected,
              id=wxID_VPRJENGCANVASDIALOGLSTATTRS)
        self.lstAttrs.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrsListItemDeselected,
              id=wxID_VPRJENGCANVASDIALOGLSTATTRS)

        self.chkAdded = wx.CheckBox(id=wxID_VPRJENGCANVASDIALOGCHKADDED,
              label=u'added', name=u'chkAdded', parent=self.pnXml,
              pos=wx.Point(16, 208), size=wx.Size(73, 13), style=0)
        self.chkAdded.SetValue(False)
        self.chkAdded.Bind(wx.EVT_CHECKBOX, self.OnChkAddedCheckbox,
              id=wxID_VPRJENGCANVASDIALOGCHKADDED)

        self.spXmlLevels = wx.SpinCtrl(id=wxID_VPRJENGCANVASDIALOGSPXMLLEVELS,
              initial=1, max=1000, min=1, name=u'spXmlLevels',
              parent=self.pnGen, pos=wx.Point(106, 150), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spXmlLevels.Bind(wx.EVT_TEXT, self.OnSpXmlMaxLevelText,
              id=wxID_VPRJENGCANVASDIALOGSPXMLLEVELS)

        self.lblXmlLevel = wx.StaticText(id=wxID_VPRJENGCANVASDIALOGLBLXMLLEVEL,
              label=u'XML Level', name=u'lblXmlLevel', parent=self.pnGen,
              pos=wx.Point(8, 154), size=wx.Size(51, 13), style=0)

        self.chkAuto = wx.CheckBox(id=wxID_VPRJENGCANVASDIALOGCHKAUTO,
              label=u'auto', name=u'chkAuto', parent=self.pnVal,
              pos=wx.Point(310, 6), size=wx.Size(82, 13), style=0)
        self.chkAuto.SetValue(True)

        self.pgProcess = wx.Gauge(id=wxID_VPRJENGCANVASDIALOGPGPROCESS,
              name=u'pgProcess', parent=self.pnXml, pos=wx.Point(180, 210),
              range=100, size=wx.Size(200, 13),
              style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)

        self._init_coll_nbEdit_Pages(self.nbEdit)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.doc=None
        self.node=None
        self.objChk=None
        self.bNode2Update=False
        
        self.name=u'vPrjEngCanvas Dialog'
        img=images.getApplyBitmap()
        self.cbOk.SetBitmapLabel(img)
        self.cbApply.SetBitmapLabel(img)
        self.cbAddLayer.SetBitmapLabel(img)
        
        self.cbUp.SetBitmapLabel(images.getGrpUpBitmap())
        self.cbDown.SetBitmapLabel(images.getGrpDownBitmap())
        
        self.txtName.Enable(False)
        
        self.trlstExternal=vPrjEngTreeListAttr(parent=self.pnXml,
                id=wx.NewId(), name=u'trlstExternal', 
                pos=wx.Point(4, 4), size=wx.Size(380, 200), style=wx.TR_HAS_BUTTONS,
                cols=[_(u'tag'),_(u'name'),_(u'type'),_(u'added')],
                master=False,verbose=1)
        EVT_VGTRXMLTREE_ITEM_SELECTED(self.trlstExternal,self.OnTreeItemSel)
        self.trlstExternal.SetColumnWidth(0, 200)
        self.trlstExternal.SetColumnWidth(1, 80)
        self.trlstExternal.SetColumnWidth(2, 40)
        self.trlstExternal.SetColumnWidth(3, 20)
        EVT_THREAD_ADD_ELEMENTS(self.trlstExternal,self.OnAddElement)
        EVT_THREAD_ADD_ELEMENTS_FINISHED(self.trlstExternal,self.OnAddFinished)
        self.selNode=None
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        self.iSelLblIdx=-1
        self.iSelIdx=-1
        self.iSelAttrIdx=-1
        self.iSelObjIdx=-1
        self.canvas=None
        self.lst=[]
        self.lstGrp=[]
        self.numScaleX.SetValue(1.0)
        self.numScaleY.SetValue(1.0)
        
        img=images.getDelBitmap()
        #self.cbDel.SetBitmapLabel(img)
        #self.cbDelName.SetBitmapLabel(img)
        #self.cbDelLbl.SetBitmapLabel(img)
        
        self.chcLayer.SetSelection(0)
        
        self.lstVal.InsertColumn(0,_(u'Nr'),wx.LIST_FORMAT_RIGHT,30)
        self.lstVal.InsertColumn(1,_(u'Name'),wx.LIST_FORMAT_LEFT,80)
        self.lstVal.InsertColumn(2,_(u'Value'),wx.LIST_FORMAT_RIGHT,60)
        
        self.lstAttrs.InsertColumn(0,_(u'Name'),wx.LIST_FORMAT_LEFT,80)
        #self.lstAttr.InsertColumn(1,_(u'Name'),wx.LIST_FORMAT_LEFT,80)
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
    def IsBusy(self):
        #if self.thdAddElements.IsRunning():
        #    return True
        return False
    def IsThreadRunning(self):
        bRunning=False
        #if self.thdAddElements.IsRunning():
        #    bRunning=True
        return bRunning
    def Stop(self):
        #if self.thdAddElements.IsRunning():
        #    self.thdAddElements.Stop()
        #while self.IsThreadRunning():
        #    time.sleep(1.0)
        pass
    def Clear(self):
        self.selNode=None
        self.objChk=None
        self.iSelLblIdx=-1
        self.iSelIdx=-1
        self.iSelAttrIdx=-1
        self.iSelObjIdx=-1
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.txtName.SetValue('')
        
        self.chcName.Clear()
        self.chcLayer.Clear()
        self.lstVal.DeleteAllItems()
        self.lstAttrs.DeleteAllItems()
        #self.trlstExternal.DeleteAllItems()
    def OnTreeItemSel(self,evt):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        self.selNode=evt.GetTreeNodeData()
        sVal=self.trlstExternal.GetValue(self.ADDED_TREE_IDX)
        if sVal==self.ADDED_MARKER:
            self.chkAdded.SetValue(True)
        else:
            self.chkAdded.SetValue(False)
        evt.Skip()
    def OnAddElement(self,evt):
        if evt.GetCount()>0:
            self.pgProcess.SetRange(evt.GetCount())
        self.pgProcess.SetValue(evt.GetValue())
    def OnAddFinished(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
            'lst:%s canvas:%s'%(repr(self.lst),repr(self.canvas.objs)),
            origin=self.GetName())
        if self.trlstExternal.IsScheduled()==False:
            self.checkObjects()
            self.trlstExternal.Enable(True)
        evt.Skip()
    def SetName(self,name):
        self.name=name
    def SetReportValue(self,scale,fontsize,rotate):
        self.numScale.SetValue(scale)
        self.chcFont.SetSelection(fontsize)
        self.chkRotate.SetValue(rotate)
    def SetDoc(self,doc,bNet=False):
        if doc is None:
            return
        if self.doc is not None:
            self.doc.DelConsumer(self)
        
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        self.trlstExternal.SetDoc(doc,bNet)
    def SetNode(self,node):
        vtLog.CallStack('')
        if self.doc is None:
            return
        vtLog.CallStack('')
        print '    ',node is None,self.node is None
        if self.doc.isSame(self.node,node):
            return
        self.node=node
        if self.IsShown():
            self.trlstExternal.Enable(False)
            self.trlstExternal.SetNode(node)
        else:
            self.bNode2Update=True
    def SetCanvas(self,canvas):
        self.canvas=canvas
    def SetValue(self,**kwargs):
        if kwargs.has_key('grid'):
            self.grid=kwargs['grid']
            self.spGridX.SetValue(self.grid[0])
            self.spGridY.SetValue(self.grid[1])
        if kwargs.has_key('max'):
            self.max=kwargs['max']
            self.spMaxX.SetValue(self.max[0])
            self.spMaxY.SetValue(self.max[1])
        if kwargs.has_key('objects'):
            self.lst=kwargs['objects']
        if kwargs.has_key('canvas'):
            self.canvas=kwargs['canvas']
        if kwargs.has_key('xml_level'):
            self.iXmlLevel=kwargs['xml_level']
            self.spXmlLevels.SetValue(self.iXmlLevel)
            self.trlstExternal.SetLevelLimit(self.iXmlLevel,-1)
        if kwargs.has_key('report_scale'):
            self.numScale.SetValue(kwargs['report_scale'])
        if kwargs.has_key('report_font'):
            self.chcFont.SetSelection(kwargs['report_font'])
        if kwargs.has_key('report_rotate'):
            self.chkRotate.SetValue(kwargs['report_rotate'])
        
        self.iSelIdx=-1
        self.iSelLblIdx=-1
        self.iSelObjIdx=-1
        #self.__updateLabels__()
        self.lstGrp=[]
        self.txtName.SetValue('')
        
        self.__updateLst__()
    def GetValue(self,name):
        if name=='objects':
            return self.lst
        elif name=='max':
            return self.max
        elif name=='grid':
            return self.grid
        elif name=='xml_level':
            return self.iXmlLevel
        elif name=='report_scale':
            return self.numScale.GetValue()
        elif name=='report_font':
            return self.chcFont.GetSelection()
        elif name=='report_rotate':
            return self.chkRotate.GetValue()
        elif name=='xml_level':
            return self.iXmlLevel

    def __updateLst__(self):
        self.iSelObjIdx=-1
        self.objChk=None
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.txtName.SetValue('')
        
        self.chcName.Clear()
        self.chcLayer.Clear()
        self.lstVal.DeleteAllItems()
        self.lstAttrs.DeleteAllItems()
        try:
            i=0
            for it in self.lst:
                sName=it.GetName()
                idx=self.chcName.Append(sName)
                self.chcName.SetClientData(idx,i)
                i+=1
            self.chcName.SetSelection(0)
            wx.CallAfter(self.__selName__)
        except:
            traceback.print_exc()
    def __updateLayer__(self):
        i=self.chcLayer.GetSelection()
        #self.lstLabels.DeleteAllItems()
        
        if self.labels==None:
            self.labels=[[],[]]
        try:
            lst=self.labels[i]
            def cmpFunc(a,b):
                return int(a[0]-b[0])
            lst.sort(cmpFunc)
            for it in lst:
                idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%it[0],-1)
                self.lstLabels.SetStringItem(idx,1,it[1],-1)
        except:
            pass
    def __selName__(self):
        self.objChk=None
        idx=self.chcName.GetSelection()
        if idx < 0:
            return
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        self.iSelObjIdx=iSel
        self.objChk=obj
        self.lstVal.DeleteAllItems()
        self.iSelIdx=-1
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.chkEnable.SetValue(obj.GetVisible())
        self.lstAttrs.DeleteAllItems()
        i=0
        try:
            for o in obj.GetObjectsXml():
                idx=self.lstAttrs.InsertImageStringItem(sys.maxint,o.GetName(),-1)
                #self.lstAttrs.SetStringItem(idx,1,o.GetName(),-1)
                self.lstAttrs.SetItemData(idx,i)
                i+=1
        except:
            pass
        i=0
        bFound=True
        while 1==1:
            sName,sVal,sFmt,sTyp=obj.GetAttr(i)
            if sTyp==-1:
                break
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%i, -1)
            self.lstVal.SetStringItem(idx,1,sName,-1)
            self.lstVal.SetStringItem(idx,2,sFmt%sVal,-1)
            i+=1
        
    def OnCbOkButton(self, event):
        self.canvas.ShowAll()
        self.Show(False)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.Show(False)
        event.Skip()

    def OnLstValListItemSelected(self, event):
        if self.objChk is None:
            return
        self.iSelIdx=event.GetIndex()
        if 1==0:
            idx=self.chcName.GetSelection()
            iSel=self.chcName.GetClientData(idx)
            obj=self.lst[iSel]
        else:
            obj=self.objChk
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
        
        sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if sTyp==-1:
            pass
        elif sTyp==0:
            self.txtVal.SetValue(sVal)
            self.txtVal.Show(True)
        elif sTyp==1:
            self.snVal.SetValue(sVal)
            self.snVal.Show(True)
        elif sTyp==2:
            self.chcVal.Clear()
            for s in obj.GetAttrEntries(self.iSelIdx):
                self.chcVal.Append(s)
            self.chcVal.SetSelection(obj.GetAttrEntrySel(self.iSelIdx))
            self.chcVal.Show(True)
        event.Skip()

    def OnLstValListItemDeselected(self, event):
        self.iSelIdx=-1
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        event.Skip()

    def OnChcNameChoice(self, event):
        self.__selName__()
        event.Skip()

    def OnSpbScaleXSpinDown(self, event):
        fVal=self.numScaleX.GetValue()
        fVal-=0.1
        self.numScaleX.SetValue(fVal)
        event.Skip()

    def OnSpbScaleXSpinUp(self, event):
        fVal=self.numScaleX.GetValue()
        fVal+=0.1
        self.numScaleX.SetValue(fVal)
        event.Skip()

    def OnSpbScaleYSpinDown(self, event):
        fVal=self.numScaleY.GetValue()
        fVal-=0.1
        self.numScaleY.SetValue(fVal)
        event.Skip()

    def OnSpbScaleYSpinUp(self, event):
        fVal=self.numScaleY.GetValue()
        fVal+=0.1
        self.numScaleY.SetValue(fVal)
        event.Skip()
    
    def __getBaseObj__(self,obj):
        if len(self.lstGrp)>0:
            return self.lstGrp[0][1]
        return obj
    def OnCbApplyButton(self, event):
        if self.objChk is None:
            return
        if 1==0:
            if self.iSelIdx<0:
                return
            idx=self.chcName.GetSelection()
            iSel=self.chcName.GetClientData(idx)
            obj=self.lst[iSel]
        else:
            obj=self.objChk
        
        if self.iSelObjIdx!=-1:
            objCont=self.lst[self.iSelObjIdx]
        else:
            objCont=None
        objBase=self.__getBaseObj__(objCont)
        objCont.SetVisible(False)
        self.canvas.ShowObj(objBase,False)
        
        sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if sTyp==-1:
            return
        elif sTyp==0:
            val=self.txtVal.GetValue()
        elif sTyp==1:
            val=self.snVal.GetValue()
        elif sTyp==2:
            val=self.chcVal.GetSelection()
            
        obj.SetAttr(self.iSelIdx,val)
        self.canvas.Recalc(self.lstGrp,objCont,obj)
        sName,val,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if self.iSelIdx>=0:
            self.lstVal.SetStringItem(self.iSelIdx,2,sFmt%val,-1)
        #event.Skip()
        objCont.SetVisible(self.chkEnable.GetValue())
        #objBase=self.__getBaseObj__(objCont)
        self.canvas.ShowObj(objBase,True)
        
    def OnCbDelButton(self, event):
        return
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        if self.iSelIdx>=0:
            dict['values']=vals[:self.iSelIdx]+vals[self.iSelIdx+1:]
            self.lstVal.DeleteItem(self.iSelIdx)
            self.iSelIdx=-1
        event.Skip()

    def OnCbUpButton(self, event):
        if len(self.lstGrp)>0:
            self.lst=self.lstGrp[-1][0]
            self.lstGrp=self.lstGrp[:-1]
            if len(self.lstGrp)>0:
                self.txtName.SetValue(self.lstGrp[-1][1].GetName())
            else:
                self.txtName.SetValue('')
            self.__updateLst__()
        event.Skip()

    def OnCbDownButton(self, event):
        idx=self.chcName.GetSelection()
        if idx<0:
            return
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        try:
            objs=obj.GetObjects()
            self.txtName.SetValue(obj.GetName())
            self.lstGrp.append((self.lst,obj))
            self.lst=objs
            self.__updateLst__()
        except:
            pass
        event.Skip()

    def OnSpMaxXText(self, event):
        self.max=(self.spMaxX.GetValue(),self.max[1])
        event.Skip()

    def OnSpMaxYText(self, event):
        self.max=(self.max[0],self.spMaxY.GetValue())
        event.Skip()
        
    def OnSpXmlMaxLevelText(self, event):
        self.iXmlLevel=self.spXmlLevels.GetValue()
        self.trlstExternal.SetLevelLimit(self.iXmlLevel,-1)
        event.Skip()
        
    def OnLstLabelsListItemDeselected(self, event):
        self.iSelLblIdx=-1
        event.Skip()

    def OnLstLabelsListItemSelected(self, event):
        self.iSelLblIdx=event.GetIndex()
        idx=self.iSelLblIdx
        it=self.lstLabels.GetItem(idx,0)
        sPos=it.m_text
        it=self.lstLabels.GetItem(idx,1)
        sName=it.m_text
        self.txtLblName.SetValue(sName)
        self.numPos.SetValue(sPos)
        event.Skip()

    def OnCbAddLayerButton(self, event):
        i=self.chcLabel.GetSelection()
        tup=(self.numPos.GetValue(),self.txtLblName.GetValue())
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+[tup]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.SetStringItem(self.iSelLblIdx,0,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(self.iSelLblIdx,1,tup[1],-1)
        else:
            self.labels[i].append(tup)
            idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(idx,1,tup[1],-1)
        event.Skip()

    def OnCbDelLayerButton(self, event):
        i=self.chcLabel.GetSelection()
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.DeleteItem(self.iSelLblIdx)
            self.iSelLblIdx=-1
        event.Skip()

    def OnSpRngMaxText(self, event):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnSpRngMinText(self, event):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnChcLabelChoice(self, event):
        self.__updateLabels__()
        event.Skip()
    
    def OnSpGridXText(self, event):
        self.grid=(self.spGridX.GetValue(),self.grid[1])
        event.Skip()

    def OnSpGridYText(self, event):
        self.grid=(self.grid[0],self.spGridY.GetValue())
        event.Skip()

    def __checkAddPossible__(self):
        if self.iSelIdx==-1:
            dlg=wx.MessageDialog(self,_(u'Select list item first!') ,
                        self.name,
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
            return False
        return True
    def OnCbAddSetMinButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,0.0])
        else:
            self.__add__([0.0])
        event.Skip()

    def __add__(self,nValLst):
        i,vals=self.__getSelVal__()
        i=self.__getNextValidTimeEntry__(i,vals)
        if i<0:
            return
        tup=vals[i]
        iOld=i
        for nVal in nValLst:
            n=[round(tup[0]-0.5)+1.0,nVal]
            vals.insert(i+1,n)
            idx=self.lstVal.InsertImageStringItem(i+1, '%d'%(i+1), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%n[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%n[1],-1)
            i+=1
        
        for idx in range(i+1,len(vals)):
            self.lstVal.SetStringItem(idx,0,'%d'%idx,-1)
            
        self.__manipulateVals__(1.0,i+1)
        self.lstVal.SetItemState(iOld, 0 ,wx.LIST_STATE_SELECTED )
        self.lstVal.SetItemState(i, wx.LIST_STATE_SELECTED ,wx.LIST_STATE_SELECTED )
    def OnCbAddSetMaxButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,1.0])
        else:
            self.__add__([1.0])
        event.Skip()

    def OnCbAddSetUndefButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,-1.0])
        else:
            self.__add__([-1.0])
        event.Skip()

    def OnCbAddToggleButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v==0.0:
            self.__add__([0.0,1.0])
        elif v==1.0:
            self.__add__([1.0,0.0])
        elif v==-1.0:
            self.__add__([-1.0])
        else:
            self.__add__([v])
        event.Skip()

    def OnCbTruncXButton(self, event):
        fVal=self.numX.GetValue()
        self.numX.SetValue(round(fVal-0.5))
        event.Skip()

    def OnCbMoveDownButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        self.__manipulateVals__(1.0)
        event.Skip()

    def OnCbMoveUpButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        self.__manipulateVals__(-1.0)
        event.Skip()
    def __getNextValidTimeEntry__(self,idx,vals):
        bFound=False
        i=idx
        iLen=len(vals)
        while bFound==False:
            if i>0:
                if i>=iLen:
                    return -1
                v=vals[i][0]
                if v>=0:
                    return i
            else:
                return -1
            i+=1
            
    def __manipulateVals__(self,dif,idx=-1):
        i,vals=self.__getSelVal__()
        if idx==-1:
            idx=i
        bFound=False
        i=idx
        while bFound==False:
            i-=1
            if i>0:
                v=vals[i][0]
                if v>=0:
                    bFound=True
            else:
                v=0.0
                bFound=True
        bPossible=True
        for i in range(idx,len(vals)):
            if vals[i][0]<0:
                continue
            nv=vals[i][0]+dif
            if nv<v:
                bPossible=False
                break
            v=nv
        if bPossible:
            for i in range(idx,len(vals)):
                if vals[i][0]<0:
                    continue
                vals[i][0]+=dif
                self.lstVal.SetStringItem(i,1,'%6.2f'%vals[i][0],-1)
        
    def __getSelVal__(self):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        return (self.iSelIdx , vals)
        if 1==0:
            dict['values']=vals[:self.iSelIdx]+[tup]+vals[self.iSelIdx+1:]
            self.lstVal.SetStringItem(self.iSelIdx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(self.iSelIdx,2,'%6.2f'%tup[1],-1)
        else:
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%len(vals), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%tup[1],-1)
            vals.append(tup)

    def OnChkAddedCheckbox(self, event):
        if self.selNode is not None:
            bAdded=self.chkAdded.GetValue()
            if bAdded:
                sVal=self.ADDED_MARKER
            else:
                sVal=self.NOT_ADDED_MARKER
            self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal)
            id=self.doc.getKey(self.selNode)
            if self.doc.IsKeyValid(id):
                bUpdate=False
                if bAdded:
                    node=self.selNode
                    idNode=self.doc.getKey(self.node)
                    id=self.doc.getKey(node)
                    lst=[]
                    while id!=idNode:
                        if self.trlstExternal.GetValue(self.ADDED_TREE_IDX,node)!=self.ADDED_MARKER:
                            self.trlstExternal.SetValue(self.ADDED_TREE_IDX,self.ADDED_MARKER,node)
                            lst.append(node)
                            #os=self.canvas.checkNode(node,bAdded)
                            #if os is not None:
                            #    bUpdate=True
                        node=self.doc.getParent(node)
                        id=self.doc.getKey(node)
                    lst.reverse()
                    lst.append(self.selNode)
                    for node in lst:
                        os=self.canvas.checkNode(node,bAdded)
                        if os is not None:
                            bUpdate=True
                    #nObjs=self.canvas.checkNode(self.selNode,bAdded)
                    #if nObjs is not None:
                    #    bUpdate=True
                else:
                    def __clrSel__(n):
                        lst=self.trlstExternal.GetNodeAttrsTup(n)
                        for it in lst:
                            self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                        for c in self.doc.getChildsAttr(n,self.doc.attr):
                            if self.trlstExternal.GetValue(self.ADDED_TREE_IDX,c)==self.ADDED_MARKER:
                                os=self.canvas.checkNode(c,bAdded)
                                if os is not None:
                                    bUpdate=True
                                self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal,c)
                            lst=self.trlstExternal.GetNodeAttrsTup(c)
                            for it in lst:
                                self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            __clrSel__(c)
                        os=self.canvas.checkNode(n,bAdded)
                        if os is not None:
                            bUpdate=True
                    __clrSel__(self.selNode)
                        
                #ti=self.trlstExternal.Get
                if bAdded:
                    node=self.selNode
                    id=self.doc.getKey(node)
                    
                    lst=self.trlstExternal.GetNodeAttrsTup()
                    for it in lst:
                        if self.doc.getTagName(it[1]) in ['tag','name']:
                            self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,
                                sVal)
                            self.canvas.addNodeAttr(self.selNode,it[1],
                                vtDrawCanvasBlockIO.TYPE_PARAM,vtDrawCanvasBlockIO.DISPLAY_VAL)
                        #else:
                        #    self.canvas.addNodeAttr(self.selNode,it[1])
            else:
                nodePar=self.doc.getParent(self.selNode)
                if bAdded:
                    self.canvas.addNodeAttr(nodePar,self.selNode)
                else:
                    self.canvas.delNodeAttr(nodePar,self.selNode)
                bUpdate=True
            #if nObjs is not None:
            if bUpdate:
                self.lst=self.canvas.objs#nObjs
                self.__updateLst__()
                self.canvas.ShowAll()
        event.Skip()
    def __checkObjects__(self,objs):
        for o in objs:
            id=o.id
            it=self.trlstExternal.__findNodeByID__(None,id)
            if it is not None:
                self.trlstExternal.SetTreeItemValue(it[0],
                                self.ADDED_TREE_IDX,self.ADDED_MARKER)
                lst=self.trlstExternal.GetNodeAttrsTup(it[1])
                for it in lst:
                    def isAttrAdded(name):
                        sName=name
                        for ao in o.objsXml:
                            if ao.name==sName:
                                return True
                        return False
                    if isAttrAdded(self.doc.getTagName(it[1])):
                        self.trlstExternal.SetTreeItemValue(it[0],
                                self.ADDED_TREE_IDX,self.ADDED_MARKER)
            try:
                if len(o.objs)>0:
                    self.__checkObjects__(o.objs)
            except:
                pass
    def checkObjects(self):
        self.trlstExternal.SetTreeItemValueAll(None,
                    self.ADDED_TREE_IDX,self.NOT_ADDED_MARKER)
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
            'lst:%s canvas:%s'%(repr(self.lst),repr(self.canvas.objs)),
            origin=self.GetName())
        self.lst=self.canvas.objs
        self.__checkObjects__(self.lst)
        self.__updateLst__()
    def OnLstAttrsChecklistbox(self, event):
        event.Skip()

    def OnLstAttrsListbox(self, event):
        idxChk=event.GetSelection()
        event.Skip()

    def OnLstAttrsListItemSelected(self, event):
        self.iSelAttrIdx=event.GetIndex()
        idxChk=self.lstAttrs.GetItemData(self.iSelAttrIdx)
        #it=self.lstLabels.GetItem(idx,0)
        #sPos=it.m_text
        idx=self.chcName.GetSelection()
        if idx < 0:
            return
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        self.lstVal.DeleteAllItems()
        self.iSelIdx=-1
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.objChk=obj.GetObjectsXml()[idxChk]
        self.chkEnable.SetValue(self.objChk.GetVisible())
        i=0
        bFound=True
        while 1==1:
            sName,sVal,sFmt,sTyp=self.objChk.GetAttr(i)
            if sTyp==-1:
                break
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%i, -1)
            self.lstVal.SetStringItem(idx,1,sName,-1)
            self.lstVal.SetStringItem(idx,2,sFmt%sVal,-1)
            i+=1
        
        event.Skip()

    def OnLstAttrsListItemDeselected(self, event):
        self.objChk=None
        self.iSelAttrIdx=-1
        self.__selName__()
        event.Skip()

    def OnChkEnableCheckbox(self, event):
        if self.objChk is not None:
            objBase=self.__getBaseObj__(self.objChk)
            self.objChk.SetVisible(False)
            self.canvas.ShowObj(objBase,False)
            self.objChk.SetVisible(self.chkEnable.GetValue())
            self.canvas.ShowObj(objBase,True)
        event.Skip()

    def OnTxtValText(self, event):
        if self.chkAuto.GetValue():
            self.OnCbApplyButton(None)
        event.Skip()

    def OnSnValText(self, event):
        if self.chkAuto.GetValue():
            self.OnCbApplyButton(None)
        event.Skip()

    def OnChcValChoice(self, event):
        if self.chkAuto.GetValue():
            self.OnCbApplyButton(None)
        event.Skip()
    
    def OnDialogActivate(self, event):
        try:
            if self.bNode2Update:
                self.trlstExternal.Enable(False)
                self.trlstExternal.SetNode(self.node)
                self.bNode2Update=False
        except:
            pass
        event.Skip()
