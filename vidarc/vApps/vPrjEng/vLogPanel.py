#Boa:FramePanel:vLogPanel
#----------------------------------------------------------------------------
# Name:         vLogPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vLogPanel.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.evtDataChanged import *
import sys
import images

[wxID_VLOGPANEL, wxID_VLOGPANELCHCUSR, wxID_VLOGPANELGCBBADD, 
 wxID_VLOGPANELGCBBSET, wxID_VLOGPANELLSTLOG, wxID_VLOGPANELTXTLOGDESC, 
 wxID_VLOGPANELTXTLOGNAME, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vLogPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VLOGPANEL, name=u'vLogPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(480, 300),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(472, 273))
        self.SetAutoLayout(True)

        self.lstLog = wx.ListView(id=wxID_VLOGPANELLSTLOG, name=u'lstLog',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(176, 256),
              style=wx.LC_REPORT)
        self.lstLog.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogListItemSelected, id=wxID_VLOGPANELLSTLOG)
        self.lstLog.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstLogListColClick,
              id=wxID_VLOGPANELLSTLOG)

        self.txtLogName = wx.TextCtrl(id=wxID_VLOGPANELTXTLOGNAME,
              name=u'txtLogName', parent=self, pos=wx.Point(192, 58),
              size=wx.Size(176, 21), style=0, value=u'')

        self.chcUsr = wx.Choice(choices=[], id=wxID_VLOGPANELCHCUSR,
              name=u'chcUsr', parent=self, pos=wx.Point(376, 56),
              size=wx.Size(88, 21), style=0)

        self.txtLogDesc = wx.TextCtrl(id=wxID_VLOGPANELTXTLOGDESC,
              name=u'txtLogDesc', parent=self, pos=wx.Point(192, 88),
              size=wx.Size(272, 182), style=wx.TE_MULTILINE, value=u'')

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOGPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(200, 16), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VLOGPANELGCBBADD)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOGPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(292, 16), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VLOGPANELGCBBSET)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        lc = wx.LayoutConstraints()
        #lc.centreX.SameAs   (self.panelA, wx.CentreX)
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs     (self, wx.Left, 2)
        lc.bottom.SameAs      (self, wx.Bottom, 2)
        lc.width.PercentOf  (self, wx.Width, 40)
        self.lstLog.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lstLog, wx.Top, 68)
        lc.left.SameAs      (self.lstLog, wx.Right, 2)
        lc.bottom.SameAs (self.lstLog, wx.Bottom, 0)
        lc.right.SameAs  (self, wx.Right, 2)
        self.txtLogDesc.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 5)
        lc.left.SameAs      (self.lstLog, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbAdd.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 5)
        lc.left.SameAs      (self.gcbbAdd, wx.Right, 5)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbSet.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbSet, wx.Bottom, 8)
        lc.left.SameAs      (self.txtLogDesc, wx.Left, 2)
        #lc.right.SameAs     (self, wx.Right, 2)
        lc.width.PercentOf  (self, wx.Width, 35)
        lc.height.AsIs      ()
        self.txtLogName.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self.txtLogName, wx.Bottom, 2)
        lc.top.SameAs       (self.txtLogName, wx.Top, 0)
        #lc.left.SameAs      (self.txtLogDesc, wx.Left, 2)
        lc.left.SameAs      (self.txtLogName, wx.Right, 2)
        lc.right.SameAs     (self, wx.Right,2)
        lc.height.AsIs      ()
        #lc.width.AsIs       ()
        self.chcUsr.SetConstraints(lc);
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        self.bSortAsc=False
        self.iSortCol=0
        
        self.lstLog.InsertColumn(0,u'Date',wx.LIST_FORMAT_LEFT,120)
        self.lstLog.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,130)
        self.lstLog.InsertColumn(2,u'User',wx.LIST_FORMAT_LEFT,70)
        
        self.bModified=False
        self.bAutoApply=False
    def Lock(self,flag):
        if flag:
            self.lstLog.Enable(False)
            self.chcUsr.Enable(False)
            self.txtLogName.Enable(False)
            self.txtLogDesc.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbSet.Enable(False)
        else:
            self.lstLog.Enable(True)
            self.chcUsr.Enable(True)
            self.txtLogName.Enable(True)
            self.txtLogDesc.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbSet.Enable(True)
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def Clear(self):
        self.__setModified__(False)
        self.txtLogName.SetValue('')
        self.txtLogDesc.SetValue('')
        self.lstLog.DeleteAllItems()
    def SetHumans(self,humans,humDoc):
        if 1==0:
            self.chcUsr.Clear()
            self.humans=humans
            self.chcUsr.Append('author')
            for i in self.humans.GetKeys():
                self.chcUsr.Append(i)
            self.chcUsr.SetSelection(0)
    def SetNetDocHuman(self,netDoc):
        self.netDocHuman=netDoc
        EVT_NET_XML_CONTENT_CHANGED(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_GET_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_ADD_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_DEL_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_REMOVE_NODE(self.netDocHuman,self.OnHumanChanged)
    def OnHumanChanged(self,evt):
        self.chcUsr.Clear()
        hums=self.netDocHuman.GetSortedUsrIds()
        self.chcUsr.Append('author')
        for i in hums.GetKeys():
            self.chcUsr.Append(i)
        self.chcUsr.SetSelection(0)
        evt.Skip()
    def SetDoc(self,doc,bNet):
        self.doc=doc
    def SetNode(self,name,node):
        if self.bModified==True:
            if self.bAutoApply:
                self.GetNode(self.node)
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
        self.__setModified__(False)
        self.node=node
        self.name=name
        self.txtLogName.SetValue('')
        self.txtLogDesc.SetValue('')
        childs=self.doc.getChilds(self.node,self.name)
        self.lstLog.DeleteAllItems()
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
            pass
        
        pass
    def GetNode(self):
        self.__setModified__(False)
        pass
    def Apply(self,bDoApply=False):
        #self.__setModified__(False)
        return False
    def Cancel(self):
        self.__setModified__(False)
    def OnGcbbAddButton(self, event):
        if self.chcUsr.GetSelection()<1:
            return
        dt=wx.DateTime.Now()
        sUsr=self.chcUsr.GetStringSelection()
        hums=self.netDocHuman.GetSortedUsrIds()
        usrID=hums.GetIdByName(sUsr)[0]
        lst=[{'tag':'date','val':dt.FormatISODate()},
             {'tag':'time','val':dt.FormatISOTime()},
             {'tag':'name','val':self.txtLogName.GetValue()},
             {'tag':'desc','val':self.txtLogDesc.GetValue()},
             {'tag':'usr','val':sUsr,'attr':('fid',usrID)}]
        
        n=self.doc.createChildByLst(self.node,self.name,lst)
        self.doc.checkId(n)
        self.doc.processMissingId()
        self.doc.clearMissingId()
        self.doc.addNode(self.node,n)
        self.__addInfo__(n)
        self.doc.AlignNode(n,iRec=1)
        #wx.PostEvent(self,vgpDataChanged(self,self.node))
        event.Skip()
    def __addInfo__(self,node):
        sDate=self.doc.getNodeText(node,'date')
        sTime=self.doc.getNodeText(node,'time')
        sName=self.doc.getNodeText(node,'name')
        usrNode=self.doc.getChild(node,'usr')
        sUsrId=self.doc.getAttribute(usrNode,'fid')
        hums=self.netDocHuman.GetSortedUsrIds()
        if len(sUsrId)>0:
            sUsr=hums.GetInfoById(sUsrId)[1]
        else:
            sUsr=self.doc.getNodeText(node,'usr')
            
        if self.bSortAsc:
            idx=sys.maxint
        else:
            idx=0
        self.logs[sDate+' '+sTime+' '+sName]=node
        index = self.lstLog.InsertImageStringItem(idx, sDate+' '+sTime, -1)
        self.lstLog.SetStringItem(index,1,sName,-1)
        self.lstLog.SetStringItem(index,2,sUsr,-1)
    def OnGcbbSetButton(self, event):
        if self.chcUsr.GetSelection()<1:
            return
        sName=self.txtLogName.GetValue()
        sDesc=self.txtLogDesc.GetValue()
        sUsr=self.chcUsr.GetStringSelection()
        it=self.lstLog.GetItem(self.selIdx,0)
        sDate=it.m_text
        it=self.lstLog.GetItem(self.selIdx,1)
        k=sDate+' '+it.m_text
        n=self.logs[k]
        self.logs[k]=None
        
        k=sDate+' '+sName
        self.doc.setNodeText(n,'name',sName)
        self.doc.setNodeText(n,'desc',sDesc)
        usrNode=self.doc.getChild(n,'usr')
        self.doc.setText(usrNode,sUsr)
        hums=self.netDocHuman.GetSortedUsrIds()
        usrID=hums.GetIdByName(sUsr)[0]
        self.doc.setAttribute(usrNode,'fid',usrID)
        self.logs[k]=n
        
        self.lstLog.SetStringItem(self.selIdx,1,sName,-1)
        self.lstLog.SetStringItem(self.selIdx,2,sUsr,-1)
        #FIXME: do network update
        self.doc.setNode(n)
        wx.PostEvent(self,vgpRevisionChanged(self,self.node))
        event.Skip()

    def OnLstLogListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstLog.GetItem(idx,0)
        sDate=it.m_text
        it=self.lstLog.GetItem(idx,1)
        self.txtLogName.SetValue(it.m_text)
        k=sDate+' '+it.m_text
        self.txtLogName.SetValue(it.m_text)
        try:
            self.txtLogDesc.SetValue(self.doc.getNodeText(self.logs[k],'desc'))
        except:
            self.txtLogDesc.SetValue('')
        try:
            self.chcUsr.SetStringSelection(self.doc.getNodeText(self.logs[k],'usr'))
        except:
            self.chcUsr.SetSelection(0)
        self.selIdx=idx
        event.Skip()

    def OnLstLogListColClick(self, event):
        if event.GetColumn()==0:
            self.bSortAsc=not self.bSortAsc
        childs=self.doc.getChilds(self.node,self.name)
        self.lstLog.DeleteAllItems()
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
        
        event.Skip()
        
