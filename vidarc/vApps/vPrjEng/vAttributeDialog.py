#Boa:Dialog:vAttributeDialog
#----------------------------------------------------------------------------
# Name:         vAttributeDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vAttributeDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys,string
import images

def create(parent):
    return vAttributeDialog(parent)

[wxID_VATTRIBUTEDIALOG, wxID_VATTRIBUTEDIALOGCHCTYPE, 
 wxID_VATTRIBUTEDIALOGGCBBAPPLY, wxID_VATTRIBUTEDIALOGGCBBCANCEL, 
 wxID_VATTRIBUTEDIALOGGCBBDEL, wxID_VATTRIBUTEDIALOGGCBBSET, 
 wxID_VATTRIBUTEDIALOGLBLATTR, wxID_VATTRIBUTEDIALOGLBLNAME, 
 wxID_VATTRIBUTEDIALOGLBLTYPE, wxID_VATTRIBUTEDIALOGLSTATTR, 
 wxID_VATTRIBUTEDIALOGTXTATTRNAME, wxID_VATTRIBUTEDIALOGTXTATTRVAL, 
 wxID_VATTRIBUTEDIALOGTXTNAME, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vAttributeDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VATTRIBUTEDIALOG,
              name=u'vAttributeDialog', parent=prnt, pos=wx.Point(237, 99),
              size=wx.Size(400, 333), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Attribute')
        self.SetClientSize(wx.Size(392, 306))

        self.lblName = wx.StaticText(id=wxID_VATTRIBUTEDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VATTRIBUTEDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(72, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblType = wx.StaticText(id=wxID_VATTRIBUTEDIALOGLBLTYPE,
              label=u'Type', name=u'lblType', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(24, 13), style=0)

        self.chcType = wx.Choice(choices=[u'int', u'long', u'float', u'text',
              u'string', u'choice', u'choiceint', u'datetime'],
              id=wxID_VATTRIBUTEDIALOGCHCTYPE, name=u'chcType', parent=self,
              pos=wx.Point(72, 40), size=wx.Size(130, 21), style=0)
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VATTRIBUTEDIALOGCHCTYPE)

        self.lblAttr = wx.StaticText(id=wxID_VATTRIBUTEDIALOGLBLATTR,
              label=u'Attributes', name=u'lblAttr', parent=self, pos=wx.Point(8,
              72), size=wx.Size(44, 13), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VATTRIBUTEDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 104),
              size=wx.Size(288, 160), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VATTRIBUTEDIALOGLSTATTR)

        self.txtAttrName = wx.TextCtrl(id=wxID_VATTRIBUTEDIALOGTXTATTRNAME,
              name=u'txtAttrName', parent=self, pos=wx.Point(64, 72),
              size=wx.Size(100, 21), style=0, value=u'')

        self.txtAttrVal = wx.TextCtrl(id=wxID_VATTRIBUTEDIALOGTXTATTRVAL,
              name=u'txtAttrVal', parent=self, pos=wx.Point(176, 72),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRIBUTEDIALOGGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(304, 104), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VATTRIBUTEDIALOGGCBBSET)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRIBUTEDIALOGGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(304, 144), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VATTRIBUTEDIALOGGCBBDEL)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRIBUTEDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(88, 272), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VATTRIBUTEDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRIBUTEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(176, 272),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VATTRIBUTEDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)

        self.selAttrIdx=-1
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        #img=images.getAddBitmap()
        #self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
                
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_RIGHT,150)

    def Set(self,name,d):
        if name is not None:
            self.txtName.SetValue(name)
        self.selAttrIdx=-1
        self.lstAttr.DeleteAllItems()
        if d is None:
            return
        self.dictAttr=d
        keys=d.keys()
        keys.sort()
        for k in keys:
            if k == 'type':
                for i in range(0,self.chcType.GetCount()):
                    s=self.chcType.GetString(i)
                    if s==d[k]:
                        self.chcType.SetSelection(i)
            else:
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
                self.lstAttr.SetStringItem(index,1,d[k],-1)
    def Get(self):
        return self.dictAttr
    def OnGcbbSetButton(self, event):
        #self.selAttrIdx=-1
        #self.lstAttr.DeleteAllItems()
        sAttr=self.txtAttrName.GetValue()
        sVal=self.txtAttrVal.GetValue()
        index=self.lstAttr.FindItem(-1,sAttr,False)
        if index<0:
            #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttr, -1)
            self.lstAttr.SetStringItem(index,1,sVal,-1)
        else:
            self.lstAttr.SetStringItem(index,1,sVal,-1)
            #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        self.dictAttr[sAttr]=sVal
        event.Skip()

    def OnGcbbDelButton(self, event):
        it=self.lstAttr.GetItem(self.selAttrIdx,0)
        self.dictAttr[it.m_text]=None
        self.lstAttr.DeleteItem(self.selAttrIdx)
        
        event.Skip()

    def OnGcbbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnChcTypeChoice(self, event):
        i=event.GetSelection()
        sType=self.chcType.GetString(i)
        if sType=='int':
            d={'type':'int','val':'0','min':'0','max':'100'}
        elif sType=='long':
            d={'type':'long','val':'0','min':'0','max':'100'}
        elif sType=='float':
            d={'type':'float','val':'0.0','digits':'6','comma':'2','min':'0','max':'100'}
        elif sType=='text':
            d={'type':'text','val':'0','len':'8'}
        elif sType=='string':
            d={'type':'string','val':'0','len':'8'}
        elif sType=='choice':
            d={'type':'choice','val':'0','it00':'item  0','it01':'item  1',
                    'it02':'item  2','it03':'item  3','it04':'item  4',
                    'it05':'item  5','it06':'item  6','it07':'item  7',
                    'it08':'item  8','it09':'item  9','it10':'item 10',}
        elif sType=='choiceint':
            d={'type':'choiceint','val':'0','min':'0','max':'0'}
        elif sType=='datetime':
            d={'type':'datetime','val':'0','min':'0','max':'0'}
        else:
            return
        self.Set(None,d)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        self.txtAttrName.SetValue(it.m_text)
        self.selAttrIdx=it.m_itemId
        it=self.lstAttr.GetItem(idx,1)
        self.txtAttrVal.SetValue(it.m_text)
        event.Skip()
