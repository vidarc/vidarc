#Boa:Dialog:vInstanceDialog
#----------------------------------------------------------------------------
# Name:         vInstanceDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vInstanceDialog.py,v 1.2 2007/08/21 18:16:42 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string
from vidarc.vApps.vPrjEng.vXmlPrjEngTree  import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS

import images

def create(parent):
    return vInstanceDialog(parent)

[wxID_VINSTANCEDIALOG, wxID_VINSTANCEDIALOGGCBBAPPLY, 
 wxID_VINSTANCEDIALOGGCBBCANCEL, wxID_VINSTANCEDIALOGGPROCESS, 
 wxID_VINSTANCEDIALOGPNINFO, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vInstanceDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VINSTANCEDIALOG,
              name=u'vInstanceDialog', parent=prnt, pos=wx.Point(237, 99),
              size=wx.Size(234, 362), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Instance')
        self.SetClientSize(wx.Size(226, 335))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCEDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(24, 296), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VINSTANCEDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(112, 296),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VINSTANCEDIALOGGCBBCANCEL)

        self.pnInfo = wx.Panel(id=wxID_VINSTANCEDIALOGPNINFO, name=u'pnInfo',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(208, 258),
              style=wx.TAB_TRAVERSAL)

        self.gProcess = wx.Gauge(id=wxID_VINSTANCEDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(8, 272), range=112,
              size=wx.Size(212, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.selNode=None
        self.node=None
        self.bSet=False
        self.bUpdate=False
        
        self.vgpTree=vXmlPrjEngTree(self.pnInfo,wx.NewId(),
                pos=(4,4),size=(200,252),style=0,name="vgpTree")
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #self.vgpTree.SetProcessBar(self.gProcess,None)
        self.vgpTree.EnableCache()
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetMainTree(self,vgTree):
        #EVT_VGTRXMLTREE_ITEM_EDITED(vgTree,self.OnTreeItemEdit)
        #EVT_VGTRXMLTREE_ITEM_ADDED(vgTree,self.OnTreeItemAdded)
        #EVT_VGTRXMLTREE_ITEM_DELETED(vgTree,self.OnTreeItemDel)
        #EVT_VGTRXMLTREE_ITEM_INSTANCE(vgTree,self.OnTreeItemInstance)
        pass
    def OnTreeItemEdit(self,event):
        if self.IsShown():
            if self.bSet==True:
                node=event.GetTreeNodeData()
                if node is not None:
                    id=self.doc.getAttribute(node,'id')
                else:
                    id=None
                if id is not None:
                    self.vgpTree.RefreshByID(id)
            else:
                self.vgpTree.SetNode(self.vgpTree.rootNode,['templates'])
        else:
            self.bUpdate=True
            if self.bSet==True:
                node=event.GetTreeNodeData()
                if node is not None:
                    id=self.doc.getAttribute(node,'id')
                else:
                    id=None
                if id is not None:
                    self.vgpTree.RefreshByID(id)
                    self.bUpdate=False
        event.Skip()
    def OnTreeItemAdded(self,event):
        if self.IsShown():
            if self.bSet==True:
                id=event.GetParentID()
                node=event.GetTreeNodeData()
                if node is not None:
                    idChild=self.doc.getAttribute(node,'id')
                else:
                    idChild=None
                if idChild is not None:
                    self.vgpTree.AddByID(id,idChild)
            else:
                self.vgpTree.SetNode(self.vgpTree.rootNode,['templates'])
        else:
            self.bUpdate=True
            if self.bSet==True:
                id=event.GetParentID()
                node=event.GetTreeNodeData()
                if node is not None:
                    idChild=self.doc.getAttribute(node,'id')
                else:
                    idChild=None
                if idChild is not None:
                    self.vgpTree.AddByID(id,idChild)
                    self.bUpdate=False
        event.Skip()
    def OnTreeItemDel(self,event):
        if self.IsShown():
            if self.vgpTree.tiCache is not None:
                if self.bSet==True:
                    id=event.GetParentID()
                    self.vgpTree.DeleteByID(id)
            else:
                self.vgpTree.SetNode(self.vgpTree.rootNode,['templates'])
        else:
            self.bUpdate=True
            if self.vgpTree.tiCache is not None:
                if self.bSet==True:
                    id=event.GetParentID()
                    self.vgpTree.DeleteByID(id)
                    self.bUpdate=False
        event.Skip()
    def OnTreeItemInstance(self,event):
        if self.IsShown():
            if self.bSet==True:
                id=event.GetParentID()
                node=event.GetTreeNodeData()
                if node is not None:
                    idChild=self.doc.getAttribute(node,'id')
                else:
                    idChild=None
                if idChild is not None:
                    self.vgpTree.AddByID(id,idChild)
            else:
                self.vgpTree.SetNode(self.vgpTree.rootNode,['templates'])
        else:
            self.bUpdate=True
            if self.bSet==True:
                id=event.GetParentID()
                node=event.GetTreeNodeData()
                if node is not None:
                    idChild=self.doc.getAttribute(node,'id')
                else:
                    idChild=None
                if idChild is not None:
                    self.vgpTree.AddByID(id,idChild)
                    self.bUpdate=False
        event.Skip()
    def SetLanguage(self,lang):
        self.vgpTree.SetLanguage(lang)
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
        self.vgpTree.SetDoc(doc,bNet)
        return
        if doc is None:
            return
        # FIXME if called more than once
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            #EVT_NET_XML_LOCK(doc,self.OnLock)
            #EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        
    def OnGotContent(self,evt):
        evt.Skip()
        self.SetNode(self.doc.getChild(None,'docnumbering'))
    def OnGetNode(self,evt):
        id=evt.GetID()
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            self.__refreshLabel__(tup[0])
        evt.Skip()
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
        idPar=evt.GetID()
        idNew=evt.GetNewID()
        child=self.doc.getNodeById(idNew)
        tup=self.__findNodeByID__(None,idPar)
        if (tup is not None) and (child is not None):
            ti=self.__addElement__(tup[0],child,True)
    def OnDelNode(self,evt):
        evt.Skip()
        id=evt.GetID()
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            self.Delete(tup[0])
            #self.doc.deleteNode(tup[1])
    def OnRemoveNode(self,evt):
        evt.Skip()
        id=evt.GetID()
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            self.Delete(tup[0])
            #self.doc.deleteNode(tup[1])
    def OnLock(self,evt):
        evt.Skip()
    def OnUnLock(self,evt):
        evt.Skip()
    def SetNode(self,node,node2instance):
        if self.doc is None:
            return
        self.node2inst=node2instance
        bSet=False
        if self.node is not None:
            if not(self.doc.isSame(self.node,node)):
                bSet=True
        else:
            bSet=True
        if bSet:
            self.vgpTree.SetNode(node,['templates'])
            self.bSet=True
        self.node=node
    def GetSelNode(self):
        return self.selNode
    def __getElementByTagName__(self,tagname):
        for e in ELEMENTS:
            if e[0]==tagname:
                return e
        return None
    def __checkPossible__(self,node):
        if self.node2inst is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.doc.getTagName(self.node2inst)
        sType=self.doc.getTagName(node)
        elemNode=self.__getElementByTagName__(sType)
        elemNode2Inst=self.__getElementByTagName__(sTypeNode2Inst)
        if elemNode is None:
            return -4
        if elemNode2Inst is None:
            return -3
        try:
            for t in elemNode[1]['__possible']:
                if t[:7]=='__type:':
                    strs=string.split(t,':')
                    if strs[1]==elemNode2Inst[1]['__type']:
                        return 1
                elif t==sTypeNode2Inst:
                    return 1
        except:
            return -5
        return 0
    def OnGcbbApplyButton(self, event):
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        ret=self.__checkPossible__(self.selNode)
        if ret==1:
            self.EndModal(1)
            event.Skip()
            return
        elif ret==0:
            # adding not possible
            sMsg=u'Instancing at this position not allowed.'
        elif ret==-1:
            # no node to instance
            sMsg=u'No node to instance!'
        elif ret==-2:
            # no node selected
            sMsg=u'No target node selected.'
        elif ret==-3:
            # element to instance not found
            sMsg=u'No element of node to instance found!'
        elif ret==-4:
            # element not found
            sMsg=u'No element of target node found!'
        elif ret==-5:
            # error oocurred
            sMsg=u'A serious error has occurred.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()
    def OnGcbbCancelButton(self, event):
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        self.EndModal(0)
        event.Skip()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        self.gProcess.SetValue(0)
        evt.Skip()
    
