#Boa:Frame:vgfSplash

import wx
import images_splash

def create(parent,actionList):
    return vgfSplash(parent,actionList)

[wxID_VGFSPLASH, wxID_VGFSPLASHGAGPROCESS, wxID_VGFSPLASHLBITMAP, 
 wxID_VGFSPLASHLBLAPPL, wxID_VGFSPLASHLBLPRG, wxID_VGFSPLASHLBLPROCESS, 
 wxID_VGFSPLASHPNMAIN, 
] = [wx.NewId() for _init_ctrls in range(7)]


#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
# defined event for vgpData item selected
wxEVT_SPLASH_PROCESSED=wx.NewEventType()
def EVT_SPLASH_PROCESSED(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_PROCESSED,func)
class vgaSplashProcessed(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_PROCESSED(<widget_name>, self.OnSplashProcessed)
    """

    def __init__(self,idx):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_PROCESSED)
        self.idx=idx
    def GetIdx(self):
        return self.idx
    
wxEVT_SPLASH_FINISHED=wx.NewEventType()
def EVT_SPLASH_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_FINISHED,func)
class vgaSplashFinished(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_FINISHED(<widget_name>, self.OnSplashFinished)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_FINISHED)

wxEVT_SPLASH_ACTION=wx.NewEventType()
def EVT_SPLASH_ACTION(win,func):
    win.Connect(-1,-1,wxEVT_SPLASH_ACTION,func)
class vgaSplashAction(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_SPLASH_ACTION(<widget_name>, self.OnSplashAction)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_SPLASH_ACTION)
        
class vgfSplash(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VGFSPLASH, name=u'vgfSplash',
              parent=prnt, pos=wx.Point(309, 121), size=wx.Size(400, 380),
              style=wx.STAY_ON_TOP, title=u'Project Engineering Splash')
        self.SetClientSize(wx.Size(392, 353))
        self.Bind(wx.EVT_ACTIVATE, self.OnVgfSplashActivate)
        self.Bind(wx.EVT_IDLE, self.OnVgfSplashIdle)

        self.pnMain = wx.Panel(id=wxID_VGFSPLASHPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(392, 353),
              style=wx.TAB_TRAVERSAL)

        self.gagProcess = wx.Gauge(id=wxID_VGFSPLASHGAGPROCESS,
              name=u'gagProcess', parent=self.pnMain, pos=wx.Point(20, 320),
              range=100, size=wx.Size(360, 20), style=wx.GA_HORIZONTAL)

        self.lblProcess = wx.StaticText(id=wxID_VGFSPLASHLBLPROCESS, label=u'',
              name=u'lblProcess', parent=self.pnMain, pos=wx.Point(20, 288),
              size=wx.Size(360, 13), style=wx.ST_NO_AUTORESIZE)

        self.lblPrg = wx.StaticText(id=wxID_VGFSPLASHLBLPRG, label=u'VIDARC',
              name=u'lblPrg', parent=self.pnMain, pos=wx.Point(80, 8),
              size=wx.Size(119, 39), style=0)
        self.lblPrg.SetFont(wx.Font(24, wx.SWISS, wx.ITALIC, wx.BOLD, False,
              u'Swis721 BT'))

        self.lblAppl = wx.StaticText(id=wxID_VGFSPLASHLBLAPPL, label=u'Project Engineering',
              name=u'lblAppl', parent=self.pnMain, pos=wx.Point(80, 48),
              size=wx.Size(72, 25), style=0)
        self.lblAppl.SetFont(wx.Font(16, wx.SWISS, wx.ITALIC, wx.BOLD, False,
              u'Swis721 BT'))

        self.lBitmap = wx.StaticBitmap(bitmap=wx.NullBitmap,
              id=wxID_VGFSPLASHLBITMAP, name=u'lBitmap', parent=self.pnMain,
              pos=wx.Point(0, 80), size=wx.Size(400, 200), style=0)

    def __init__(self, parent,actionList):
        self._init_ctrls(parent)
        
        self.lBitmap.SetBitmap(images_splash.getSplashBitmap())
        
        self.gagProcess.SetRange(len(actionList))
        self.res=[]
        self.actionList=actionList
        self.iAction=-2
        self.bActivated=False
    def DoAction(self):
        self.iAction=self.iAction+1
        if self.iAction<len(self.actionList):
            self.gagProcess.SetValue(self.iAction+1)
            dAction=self.actionList[self.iAction]
            self.lblProcess.SetLabel(dAction['label'])
            wx.PostEvent(self,vgaSplashAction())
        else:
            wx.PostEvent(self,vgaSplashFinished())
    def DoProcess(self):
        if self.iAction<len(self.actionList):
            dAction=self.actionList[self.iAction]
            if dAction.has_key('eval'):
                self.res.append(eval(dAction['eval']))
            wx.PostEvent(self,vgaSplashProcessed(self.iAction))

    def OnVgfSplashActivate(self, event):
        event.Skip()

    def OnVgfSplashIdle(self, event):
        if self.iAction==-2:
            self.iAction=-1
            wx.PostEvent(self,vgaSplashProcessed(-1))
        event.Skip()
        
        
