#Boa:Dialog:vExportNodeDialog
#----------------------------------------------------------------------------
# Name:         vExportNodeDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vExportNodeDialog.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import os.path
import vidarc.tool.InOut.fnUtil as fnUtil

import images

def create(parent):
    return vExportNodeDialog(parent)

[wxID_VEXPORTNODEDIALOG, wxID_VEXPORTNODEDIALOGGCBBBROWSE, 
 wxID_VEXPORTNODEDIALOGGCBBCANCEL, wxID_VEXPORTNODEDIALOGGCBBGENERATE, 
 wxID_VEXPORTNODEDIALOGLBLFILENAME, wxID_VEXPORTNODEDIALOGTXTPRJTASKFN, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vExportNodeDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VEXPORTNODEDIALOG,
              name=u'vExportNodeDialog', parent=prnt, pos=wx.Point(181, 101),
              size=wx.Size(451, 394), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Export Node')
        self.SetClientSize(wx.Size(443, 367))

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTNODEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 328),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VEXPORTNODEDIALOGGCBBCANCEL)

        self.lblFileName = wx.StaticText(id=wxID_VEXPORTNODEDIALOGLBLFILENAME,
              label=u'Filename', name=u'lblFileName', parent=self,
              pos=wx.Point(8, 6), size=wx.Size(42, 13), style=0)

        self.txtPrjTaskFN = wx.TextCtrl(id=wxID_VEXPORTNODEDIALOGTXTPRJTASKFN,
              name=u'txtPrjTaskFN', parent=self, pos=wx.Point(8, 22),
              size=wx.Size(336, 21), style=wx.TE_RICH2, value=u'')
        self.txtPrjTaskFN.Enable(True)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTNODEDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Export',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 330),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VEXPORTNODEDIALOGGCBBGENERATE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTNODEDIALOGGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(360, 16),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VEXPORTNODEDIALOGGCBBBROWSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.selNode=None
        self.doc=None
        self.docPrj=None
        
        self.vgpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(4,50),size=(435,270),style=0,name="vgpTree")
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getExportBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        self.vgpTree.SetLanguage(lang)
    def SetDoc(self,doc,docPrj):
        self.doc=doc
        self.docPrj=docPrj
        self.vgpTree.SetDoc(self.doc,False)
    def SetNode(self,node,nodeSettings):
        #self.node2inst=node2instance
        self.vgpTree.SetNode(node)
        self.node=node
        self.prjId=self.doc.getAttribute(node,'prjfid')
        if self.docPrj is not None:
            node=self.docPrj.GetPrj(self.prjId)
        else:
            node=None
        if node is None:
            self.prjId=u''
            self.sShort=u''
            self.sClient=u''
        else:
            #self.prjId=self.docPrj.getAttribute(node,'fid')
            self.sShort=self.docPrj.GetPrjName(node)
            self.sClient=self.docPrj.GetCltShort(node)
        
        self.nodeSettings=nodeSettings
        if self.nodeSettings is not None:
            self.sExpDN=self.doc.getNodeText(nodeSettings,'exportdn')
        else:
            self.sExpDN=''
        self.txtPrjTaskFN.SetValue(u'')
        pass
    def GetSelNode(self):
        return self.selNode
    
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
        
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
    def OnGcbbGenerateButton(self, event):
        if self.selNode is None:
            return
        sPrjTaskFN=self.txtPrjTaskFN.GetValue()
        
        if len(sPrjTaskFN)<=0:
            sPrjTaskFN='???'
            self.txtPrjTaskFN.SetValue(sPrjTaskFN)
            self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("RED", "YELLOW"))
            return
        sFN=os.path.split(sPrjTaskFN)[1]
        sTmp=fnUtil.replaceSuspectChars(sFN)
        if sTmp!=sFN:
            self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("RED", "YELLOW"))
            return
        self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("BLACK", "WHITE"))
        fn=self.txtPrjTaskFN.GetValue()
        
        # build export file
        docExp=vtXmlDom.vtXmlDom()
        docExp.New(root="export_node")
        rootNode=docExp.getRoot()
        expNode=docExp.createSubNode(rootNode,'export',False)
        dt=wx.DateTime.Now()
        lst=[]
        lst.append({'tag':'date','val':dt.FormatISODate()})
        lst.append({'tag':'time','val':dt.FormatISOTime()})
        lst.append({'tag':'project','val':self.sShort,'attr':['fid',self.prjId]})
        lst.append({'tag':'client','val':self.sClient})
        docExp.createChildByLst(expNode,'create',lst,False)
        c=docExp.createSubNode(expNode,'data',False)
        node=self.doc.cloneNode(self.selNode,True)
        docExp.removeInternalNodes(node)
        docExp.appendChild(c,node)
        
        docExp.AlignDoc()
        docExp.Save(fn)
        docExp.Close()
        del docExp
        dn=os.path.split(fn)[0]
        self.doc.setNodeText(self.nodeSettings,'exportdn',dn)
        self.doc.AlignNode(self.nodeSettings)
        self.EndModal(1)
        event.Skip()
    def OnGcbbBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Save as", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.txtPrjTaskFN.GetValue()
            #fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if len(fn)>0:
                dlg.SetFilename(fn)
            else:
                dt=wx.DateTime.Now()
                sDate=string.replace(dt.FormatISODate(),'-','')
                sTime=string.replace(dt.FormatISOTime(),':','')
                if self.selNode is not None:
                    sName=self.doc.getNodeText(self.selNode,'tag')
                else:
                    sName=u''
                fn=os.path.join(self.sExpDN,string.join([self.sClient+self.sShort,
                                u'PrjEngExp',sName,sDate,sTime],'_'))+'.xml'
                dlg.SetFilename(fn)
            #else:
            #    dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtPrjTaskFN.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
