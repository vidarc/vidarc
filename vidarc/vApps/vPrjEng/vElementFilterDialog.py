#Boa:Dialog:vElementFilterDialog
#----------------------------------------------------------------------------
# Name:         vElementFilterDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElementFilterDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS

import images

def create(parent):
    return vElementFilterDialog(parent)

[wxID_VELEMENTFILTERDIALOG, wxID_VELEMENTFILTERDIALOGGCBBCANCEL, 
 wxID_VELEMENTFILTERDIALOGGCBBOK, wxID_VELEMENTFILTERDIALOGGCBBSELECTALL, 
 wxID_VELEMENTFILTERDIALOGGCBBUNSELECTALL, 
 wxID_VELEMENTFILTERDIALOGLSTELEMENT, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vElementFilterDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VELEMENTFILTERDIALOG,
              name=u'vElementFilterDialog', parent=prnt, pos=wx.Point(152, 47),
              size=wx.Size(379, 390), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Engineering Element Filter')
        self.SetClientSize(wx.Size(371, 363))

        self.gcbbSelectAll = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTFILTERDIALOGGCBBSELECTALL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Select All',
              name=u'gcbbSelectAll', parent=self, pos=wx.Point(80, 4),
              size=wx.Size(96, 30), style=0)
        self.gcbbSelectAll.Bind(wx.EVT_BUTTON, self.OnGcbbSelectAllButton,
              id=wxID_VELEMENTFILTERDIALOGGCBBSELECTALL)

        self.gcbbUnselectAll = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTFILTERDIALOGGCBBUNSELECTALL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Unselect All',
              name=u'gcbbUnselectAll', parent=self, pos=wx.Point(220, 4),
              size=wx.Size(96, 30), style=0)
        self.gcbbUnselectAll.Bind(wx.EVT_BUTTON, self.OnGcbbUnselectAllButton,
              id=wxID_VELEMENTFILTERDIALOGGCBBUNSELECTALL)

        self.lstElement = wx.ListView(id=wxID_VELEMENTFILTERDIALOGLSTELEMENT,
              name=u'lstElement', parent=self, pos=wx.Point(16, 44),
              size=wx.Size(336, 266), style=wx.LC_REPORT)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTFILTERDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(84, 328), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VELEMENTFILTERDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTFILTERDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(184, 328),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VELEMENTFILTERDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.lstElement.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,170)
        self.lstElement.InsertColumn(1,u'Type',wx.LIST_FORMAT_LEFT,130)
        
        img=images.getApplyBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        self.__updateElem__()
    def __updateElem__(self):
        self.lstElement.DeleteAllItems()
        for e in ELEMENTS:
            try:
                index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                self.lstElement.SetStringItem(index,1,e[1]['__type'],-1)
            except:
                pass
    def GetFilter(self):
        lst=[]
        for i in range(0,self.lstElement.GetItemCount()):
            if self.lstElement.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                it=self.lstElement.GetItem(i,0)
                lst.append(it.m_text)
        return lst
    def SetFilter(self,lstFilter):
        for i in range(0,self.lstElement.GetItemCount()):
            it=self.lstElement.GetItem(i,0)
            try:
                lstFilter.index(it.m_text)
                self.lstElement.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            except:
                pass
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnGcbbSelectAllButton(self, event):
        for i in range(0,self.lstElement.GetItemCount()):
            #self.lstElement.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_MASK_STATE)
            self.lstElement.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        event.Skip()

    def OnGcbbUnselectAllButton(self, event):
        for i in range(0,self.lstElement.GetItemCount()):
            #self.lstElement.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_MASK_STATE)
            self.lstElement.SetItemState(i,0,wx.LIST_STATE_SELECTED)
        event.Skip()
        
