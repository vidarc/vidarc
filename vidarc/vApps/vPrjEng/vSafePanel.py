#Boa:FramePanel:vSafePanel
#----------------------------------------------------------------------------
# Name:         vSavePanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vSafePanel.py,v 1.2 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.evtDataChanged import *
import sys,string
import images

import vidarc.tool.log.vtLog as vtLog
VERBOSE=0

[wxID_VSAFEPANEL, wxID_VSAFEPANELCHCUSR, wxID_VSAFEPANELGCBBNEW, 
 wxID_VSAFEPANELGCBBREVERT, wxID_VSAFEPANELLBLCURREV, wxID_VSAFEPANELLBLDESC, 
 wxID_VSAFEPANELLBLNAME, wxID_VSAFEPANELLBLPREVREV, wxID_VSAFEPANELLBLREV, 
 wxID_VSAFEPANELLBLUSR, wxID_VSAFEPANELLSTREV, wxID_VSAFEPANELTXTCURREV, 
 wxID_VSAFEPANELTXTDESC, wxID_VSAFEPANELTXTNAME, wxID_VSAFEPANELTXTPREVREV, 
 wxID_VSAFEPANELTXTREV, 
] = [wx.NewId() for _init_ctrls in range(16)]

class vSafePanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSAFEPANEL, name=u'vSafePanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(472, 352),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(464, 325))
        self.SetAutoLayout(True)

        self.lstRev = wx.ListView(id=wxID_VSAFEPANELLSTREV, name=u'lstRev',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(448, 120),
              style=wx.LC_REPORT)
        self.lstRev.SetConstraints(LayoutAnchors(self.lstRev, True, True, True,
              False))
        self.lstRev.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRevListItemSelected, id=wxID_VSAFEPANELLSTREV)
        self.lstRev.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstRevListColClick,
              id=wxID_VSAFEPANELLSTREV)

        self.gcbbNew = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSAFEPANELGCBBNEW,
              bitmap=wx.EmptyBitmap(16, 16), label=u'New Rev', name=u'gcbbNew',
              parent=self, pos=wx.Point(187, 168), size=wx.Size(76, 30),
              style=0)
        self.gcbbNew.Bind(wx.EVT_BUTTON, self.OnGcbbNewButton,
              id=wxID_VSAFEPANELGCBBNEW)

        self.lblRev = wx.StaticText(id=wxID_VSAFEPANELLBLREV, label=u'Revision',
              name=u'lblRev', parent=self, pos=wx.Point(24, 176),
              size=wx.Size(41, 13), style=0)

        self.txtRev = wx.TextCtrl(id=wxID_VSAFEPANELTXTREV, name=u'txtRev',
              parent=self, pos=wx.Point(72, 176), size=wx.Size(100, 21),
              style=wx.TE_RICH2, value=u'')

        self.lblName = wx.StaticText(id=wxID_VSAFEPANELLBLNAME, label=u'Name',
              name=u'lblName', parent=self, pos=wx.Point(32, 208),
              size=wx.Size(28, 16), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VSAFEPANELTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(72, 204), size=wx.Size(232, 21),
              style=0, value=u'')

        self.lblUsr = wx.StaticText(id=wxID_VSAFEPANELLBLUSR, label=u'User',
              name=u'lblUsr', parent=self, pos=wx.Point(312, 208),
              size=wx.Size(22, 13), style=0)

        self.chcUsr = wx.Choice(choices=[], id=wxID_VSAFEPANELCHCUSR,
              name=u'chcUsr', parent=self, pos=wx.Point(340, 204),
              size=wx.Size(114, 21), style=0)

        self.lblDesc = wx.StaticText(id=wxID_VSAFEPANELLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(8, 232), size=wx.Size(53, 13), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VSAFEPANELTXTDESC, name=u'txtDesc',
              parent=self, pos=wx.Point(72, 232), size=wx.Size(384, 88),
              style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.SetConstraints(LayoutAnchors(self.txtDesc, True, True,
              True, True))

        self.gcbbRevert = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSAFEPANELGCBBREVERT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Revert Revision',
              name=u'gcbbRevert', parent=self, pos=wx.Point(72, 136),
              size=wx.Size(104, 30), style=0)
        self.gcbbRevert.Bind(wx.EVT_BUTTON, self.OnGcbbRevertButton,
              id=wxID_VSAFEPANELGCBBREVERT)

        self.txtCurRev = wx.TextCtrl(id=wxID_VSAFEPANELTXTCURREV,
              name=u'txtCurRev', parent=self, pos=wx.Point(404, 140),
              size=wx.Size(50, 21), style=wx.TE_RICH2, value=u'')
        self.txtCurRev.Bind(wx.EVT_TEXT, self.OnTxtCurRevText,
              id=wxID_VSAFEPANELTXTCURREV)

        self.lblCurRev = wx.StaticText(id=wxID_VSAFEPANELLBLCURREV,
              label=u'Current Rev', name=u'lblCurRev', parent=self,
              pos=wx.Point(344, 144), size=wx.Size(57, 13), style=0)

        self.lblPrevRev = wx.StaticText(id=wxID_VSAFEPANELLBLPREVREV,
              label=u'Previous Rev', name=u'lblPrevRev', parent=self,
              pos=wx.Point(336, 172), size=wx.Size(64, 13), style=0)

        self.txtPrevRev = wx.TextCtrl(id=wxID_VSAFEPANELTXTPREVREV,
              name=u'txtPrevRev', parent=self, pos=wx.Point(404, 168),
              size=wx.Size(50, 21), style=wx.TE_RICH2, value=u'')

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.verbose=VERBOSE
        self.node=None
        lc = wx.LayoutConstraints()
        #lc.centreX.SameAs   (self.panelA, wx.CentreX)
        lc.top.SameAs      (self.txtRev, wx.Bottom, 4)
        lc.left.SameAs     (self.txtDesc, wx.Left, 0)
        lc.height.AsIs     ()
        lc.width.PercentOf (self, wx.Width, 40)
        self.txtName.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.txtName, wx.Top, 2)
        lc.left.SameAs      (self.txtName, wx.Right, 2)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.lblUsr.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lblUsr, wx.Top, 0)
        lc.left.SameAs      (self.lblUsr, wx.Right, 2)
        lc.height.AsIs      ()
        lc.right.SameAs     (self.lstRev,wx.Right,0)
        self.chcUsr.SetConstraints(lc);
        
        img=images.getAddBitmap()
        self.gcbbNew.SetBitmapLabel(img)
        
        img=images.getRevertBitmap()
        self.gcbbRevert.SetBitmapLabel(img)
        
        self.revNode=None
        self.humans=None
        self.revs={}
        self.bSortAsc=False
        self.iSortCol=0
        
        self.lstRev.InsertColumn(0,u'Date',wx.LIST_FORMAT_LEFT,120)
        self.lstRev.InsertColumn(1,u'Revision',wx.LIST_FORMAT_LEFT,120)
        self.lstRev.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstRev.InsertColumn(3,u'User',wx.LIST_FORMAT_LEFT,70)
        
        self.bModified=False
        self.bAutoApply=False
    def Lock(self,flag):
        if flag:
            self.lstRev.Enable(False)
            self.gcbbNew.Enable(False)
            self.txtRev.Enable(False)
            self.txtName.Enable(False)
            self.chcUsr.Enable(False)
            self.txtDesc.Enable(False)
            self.gcbbRevert.Enable(False)
            self.txtCurRev.SetEditable(False)
            self.txtPrevRev.SetEditable(False)
        else:
            self.lstRev.Enable(True)
            self.gcbbNew.Enable(True)
            self.txtRev.Enable(True)
            self.txtName.Enable(True)
            self.chcUsr.Enable(True)
            self.txtDesc.Enable(True)
            self.gcbbRevert.Enable(True)
            self.txtCurRev.SetEditable(True)
            self.txtPrevRev.SetEditable(True)
            
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def Clear(self):
        self.__setModified__(False)
        self.lstRev.DeleteAllItems()
        self.txtCurRev.SetValue('')
        self.txtPrevRev.SetValue('')
        
        self.txtRev.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')    
    def SetHumans(self,humans,humDoc):
        self.chcUsr.Clear()
        self.humans=humans
        for i in self.humans.GetKeys():
            self.chcUsr.Append(i)
        self.chcUsr.SetSelection(0)
    def SetNetDocHuman(self,netDoc):
        self.netDocHuman=netDoc
        EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnHumanChanged)
    def OnHumanChanged(self,evt):
        self.chcUsr.Clear()
        hums=self.netDocHuman.GetSortedUsrIds()
        self.chcUsr.Append('author')
        for i in hums.GetKeys():
            self.chcUsr.Append(i)
        self.chcUsr.SetSelection(0)
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,name,node):
        if self.bModified==True:
            if self.bAutoApply:
                self.GetNode(self.node)
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
        self.__setModified__(False)
        self.node=node
        self.name=name
        self.revNode=None
        
        self.txtCurRev.SetValue('')
        sRev=self.doc.getNodeText(self.node,'__rev')
        self.txtCurRev.SetValue(sRev)
        self.txtCurRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        sRev=self.doc.getNodeText(self.node,'__prev_rev')
        self.txtPrevRev.SetValue(sRev)
        self.txtPrevRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        
        self.txtRev.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        childs=self.doc.getChilds(self.node,self.name)
        self.lstRev.DeleteAllItems()
        self.revs={}
        for c in childs:
            self.__addInfo__(c)
            pass
        
        pass
    def GetNode(self,node,doc=None):
        sRev=self.txtCurRev.GetValue()
        self.doc.setNodeText(self.node,'__rev',sRev)
        sRev=self.txtPrevRev.GetValue()
        self.doc.setNodeText(self.node,'__prev_rev',sRev)
        self.doc.AlignNode(self.node,iRec=10)
        self.__setModified__(False)
        pass
    def __addInfo__(self,node):
        sDate=self.doc.getNodeText(node,'date')
        sTime=self.doc.getNodeText(node,'time')
        sRev=self.doc.getNodeText(node,'rev')
        sName=self.doc.getNodeText(node,'name')
        sUsr=self.doc.getNodeText(node,'usr')
        if self.bSortAsc:
            idx=sys.maxint
        else:
            idx=0
        self.revs[sDate+' '+sTime+' '+sRev]=node
        index = self.lstRev.InsertImageStringItem(idx, sDate+' '+sTime, -1)
        self.lstRev.SetStringItem(index,1,sRev,-1)
        self.lstRev.SetStringItem(index,2,sName,-1)
        self.lstRev.SetStringItem(index,3,sUsr,-1)
        self.revNode=None
        
    def Apply(self,bDoApply=False):
        sRev=self.txtCurRev.GetValue()
        if sRev!=self.doc.getNodeText(self.node,'__rev'):
            self.__setModified__(True)
        sRev=self.txtPrevRev.GetValue()
        if sRev!=self.doc.getNodeText(self.node,'__prev_rev'):
            self.__setModified__(True)
        
        if self.bModified==True:
            if self.bAutoApply or bDoApply:
                self.GetNode(self.node)
                return True
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
                    return true
        return False
    def Cancel(self):
        sRev=self.doc.getNodeText(self.node,'__rev')
        self.txtCurRev.SetValue(sRev)
        self.txtCurRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        sRev=self.doc.getNodeText(self.node,'__prev_rev')
        self.txtPrevRev.SetValue(sRev)
        self.txtPrevRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        
        self.__setModified__(False)
    def __clone__(self,nodeSafe,node,skip):
        n=self.doc.createSubNode(nodeSafe,self.doc.getTagName(node),False)
        id=self.doc.getKey(node)
        if self.doc.IsKeyValid(id):
            self.doc.setAttribute(n,'rid',id)
        childs=self.doc.getChilds(node)
        for c in childs:
            if self.doc.getTagName(c) not in skip:
                id=self.doc.getKey(c)
                if self.doc.IsKeyValid(id):
                    nc=self.__clone__(n,c,skip)
                    self.doc.appendChild(n,nc)
                else:
                    self.doc.appendChild(n,c.copyNode(True))
        return n
    def OnGcbbNewButton(self, event):
        sRev=self.txtRev.GetValue()
        bOk=True
        if len(sRev)<=0:
            bOk=False
        elif len(string.replace(sRev,' ',''))!=len(sRev):
            bOk=False
        if bOk==True:
            for r in self.revs.keys():
                strs=string.split(r)
                if strs[2]==sRev:
                    bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            if len(sRev)<=0:
                sRev='???'
                self.txtRev.SetValue(sRev)
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
            return
        sUsr=self.chcUsr.GetStringSelection()
        humans=self.netDocHuman.GetSortedUsrIds()
        if humans is not None:
            idUsr=humans.GetIdByName(sUsr)
            usrDict={'tag':'usr','val':sUsr,'attr':('fid',idUsr[0])}
        else:
            idUsr=''
            usrDict={'tag':'usr','val':sUsr}
        dt=wx.DateTime.Now()
        lst=[{'tag':'date','val':dt.FormatISODate()},
             {'tag':'time','val':dt.FormatISOTime()},
             {'tag':'rev','val':self.txtRev.GetValue()},
             {'tag':'name','val':self.txtName.GetValue()},
             {'tag':'desc','val':self.txtDesc.GetValue()},
             usrDict]
        n=self.doc.createChildByLst(self.node,self.name,lst,False)
        c=self.__clone__(n,self.node,['Log','Safe','__node'])
        #FIXME
        self.doc.checkId(n)
        self.doc.processMissingId()
        self.doc.clearMissing()
        self.doc.AlignNode(n)
        self.__addInfo__(n)
        self.doc.addNode(self.node,n)
        wx.PostEvent(self,vgpDataChanged(self,self.node))
        event.Skip()

    def OnLstRevListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstRev.GetItem(idx,0)
        sDate=it.m_text
        it=self.lstRev.GetItem(idx,1)
        self.txtRev.SetValue(it.m_text)
        #self.txtRev.SetValue(it.m_text)
        k=sDate+' '+it.m_text
        self.revNode=self.revs[k]
        it=self.lstRev.GetItem(idx,2)
        self.txtName.SetValue(it.m_text)
        try:
            self.txtDesc.SetValue(self.doc.getNodeText(self.revs[k],'desc'))
        except:
            self.txtDesc.SetValue('')
        try:
            self.chcUsr.SetStringSelection(self.doc.getNodeText(self.revs[k],'usr'))
        except:
            self.chcUsr.SetSelection(0)
        self.selIdx=idx
        event.Skip()

    def OnLstRevListColClick(self, event):
        if event.GetColumn()==0:
            self.bSortAsc=not self.bSortAsc
        childs=self.doc.getChilds(self.node,self.name)
        self.lstRev.DeleteAllItems()
        self.revNode=None
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
        event.Skip()

    def __revert__(self,nodeBase,revNode,skip,keepNew):
        id=self.doc.getAttribute(revNode,'rid')
        if self.doc.IsKeyValid(id):
            self.doc.setAttribute(nodeBase,'id',id)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'id:%s'%id)
        
        d={}    # nodes in revision without children
        ds={}   # nodes in revision with children
        did={}  # nodes in revision
        childs=self.doc.getChilds(revNode)
        for c in childs:
            tagName=self.doc.getTagName(c)
            if self.verbose:
                vtLog.vtLog(None,'tagname:%s'%tagName,verbose=self.verbose)
            if tagName not in skip:
                id=self.doc.getAttribute(c,'rid')
                if self.doc.IsKeyValid(id):
                    did[tagName+'|'+id]=c
                else:
                    subchilds=self.doc.getChilds(c)
                    if len(subchilds):
                        try:
                            ds[tagName].append(c)
                        except:
                            ds[tagName]=[c]
                    else:
                        d[tagName]=c
        if self.verbose:
            vtLog.vtLog(None,'did:%s ds:%s d:%s'%(did.keys(),ds.keys(),d.keys()),verbose=self.verbose)
        dn={}   #nodes without children 
        dns={}  #nodes with children
        dnid={} #nodes
        childs=self.doc.getChilds(nodeBase)
        for c in childs:
            tagName=self.doc.getTagName(c)
            if tagName not in skip:
                id=self.doc.getKey(c)
                if self.doc.IsKeyValid(id):
                    dnid[tagName+'|'+id]=c
                else:
                    subchilds=self.doc.getChilds(c)
                    if len(subchilds)>0:
                        try:
                            dns[tagName].append(c)
                        except:
                            dns[tagName]=[c]
                    else:
                        dn[tagName]=c
        if self.verbose:
            vtLog.vtLog(None,'dnid:%s dns:%s dn:%s'%(dnid.keys(),dns.keys(),dn.keys()),verbose=self.verbose)
        if 1==0:
            print '---------------------------------'
            print 'rev node without childs',d.keys()
            print 'rev node with    childs',ds.keys()
            print 'rev node               ',did.keys()
            print '++++++++++++++++++++++++++++++++++'
            print '---------------------------------'
            print '    node without childs',dn.keys()
            print '    node with    childs',dns.keys()
            print '    node               ',dnid.keys()
            print '++++++++++++++++++++++++++++++++++'
        keys=d.keys()
        keys.sort()
        for k in keys:
            if dn.has_key(k):
                #print dn[k]
                childs=self.doc.getChilds(d[k])
                if len(childs)>0:
                    self.__revert__(dn[k],d[k],skip,keepNew)
                else:
                    self.doc.setText(dn[k],self.doc.getText(d[k]))
            else:
                cc=self.doc.cloneNode(d[k],True)
                if self.verbose:
                    vtLog.vtLog(None,'key:%s cc:%s'%(k,cc),verbose=self.verbose)
                self.doc.appendChild(nodeBase,cc)
                #self.doc.addNode(nodeBase,cc)
        for k in dns.keys():
            for c in dns[k]:
                # remove nodes
                self.doc.deleteNode(c,nodeBase)
                #self.doc.delNode(c)
        keys=ds.keys()
        keys.sort()
        for k in keys:
            for c in ds[k]:
                cc=self.doc.cloneNode(c,True)
                if self.verbose:
                    vtLog.vtLog(None,'key:%s cc:%s'%(k,cc),verbose=self.verbose)
                self.doc.appendChild(nodeBase,cc)
                #self.doc.addNode(nodeBase,cc)
        keys=did.keys()
        keys.sort()
        for k in keys:
            if dnid.has_key(k):
                self.__revert__(dnid[k],did[k],skip,keepNew)
                dnid[k]=None
            else:
                strs=string.split(k,'|')
                nc=self.doc.createSubNode(nodeBase,strs[0],False)
                self.doc.setAttribute(nc,'id',strs[1])
                #FIXME
                self.doc.checkId(nc)
                self.doc.processMissingId()
                self.doc.clearMissing()
                if self.verbose:
                    vtLog.vtLog(None,'key:%s cc:%s'%(k,nc),verbose=self.verbose)
                #self.doc.addNode(nodeBase,nc)
                self.__revert__(nc,did[k],skip,keepNew)
        if keepNew==False:
            for n in dnid.values():
                if n is not None:
                    #FIXME
                    #self.ids.RemoveId(self.doc.getAttribute(n,'id'))
                    #nodeBase.removeChild(n)
                    if self.verbose:
                        vtLog.vtLog(None,'delete key:%s node:%s'%(k,n),verbose=self.verbose)
                    self.doc.deleteNode(n,nodeBase)
                    #self.doc.delNode(n)
        if self.verbose:
            vtLog.vtLog(None,'exit',level=5,verbose=self.verbose)
        
    def OnGcbbRevertButton(self, event):
        # ask
        if self.revNode is None:
            return
        dlg=wx.MessageDialog(self,u'Do you really want to revert current data to selected revision?' ,
                    u'vgaPrjEng Element Revision',
                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        if dlg.ShowModal()==wx.ID_YES:
            dlg.Destroy()
            revNode=None
            childs=self.doc.getChilds(self.revNode)
            tagName=self.doc.getTagName(self.node)
            for c in childs:
                if tagName==self.doc.getTagName(c):
                    id=self.doc.getAttribute(c,'rid')
                    if self.doc.IsKeyValid(id):
                        if id==self.doc.getAttribute(self.node,'id'):
                            revNode=c
                            break
            if revNode is None:
                return
            if len(self.doc.getChilds(self.node,'__node'))==0:
                dlg=wx.MessageDialog(self,u'Do you want to keep new data?' ,
                        u'vgaPrjEng Element Revision Style',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_NO:
                    self.__revert__(self.node,revNode,[u'Log',u'Safe'],False)
                else:
                    self.__revert__(self.node,revNode,[u'Log',u'Safe'],True)
            else:
                self.__revert__(self.node,revNode,[u'Log',u'Safe'],False)
            self.doc.AlignNode(self.node)
            self.doc.setNodeFull(self.node)
            wx.PostEvent(self,vgpRevisionChanged(self,self.node))
            dlg.Destroy()
        else:
            dlg.Destroy()
        event.Skip()

    def OnTxtCurRevText(self, event):
        sRev=self.txtCurRev.GetValue()
        bOk=True
        if len(sRev)<=0:
            bOk=False
        elif len(string.replace(sRev,' ',''))!=len(sRev):
            #self.txtCurRev.SetValue(sRev)
            bOk=False
        if bOk==True:
            for r in self.revs.keys():
                strs=string.split(r)
                if strs[2]==sRev:
                    bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtCurRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            self.txtCurRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
            return
        event.Skip()
    
