#Boa:Dialog:vImportNodeDialog
#----------------------------------------------------------------------------
# Name:         vDataPanel.py
# Purpose:      export a selected node to a xml-export file
#               derived from vImportNodeDialog
# Author:       Walter Obweger
#
# Created:      20060605
# CVS-ID:       $Id: vImportNodeDialog.py,v 1.3 2006/06/07 10:12:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
#import cStringIO
from vidarc.vApps.vPrjTimer.vPrjTimerSettingsPanel import *
from vidarc.vApps.vPrjEng.vXmlPrjEngTree  import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import os.path

import images

def create(parent):
    return vImportNodeDialog(parent)

[wxID_VIMPORTNODEDIALOG, wxID_VIMPORTNODEDIALOGGCBBBROWSE, 
 wxID_VIMPORTNODEDIALOGGCBBCANCEL, wxID_VIMPORTNODEDIALOGGCBBGENERATE, 
 wxID_VIMPORTNODEDIALOGLBLCLT, wxID_VIMPORTNODEDIALOGLBLFILENAME, 
 wxID_VIMPORTNODEDIALOGLBLPRJ, wxID_VIMPORTNODEDIALOGLBLTIME, 
 wxID_VIMPORTNODEDIALOGTXTCLIENT, wxID_VIMPORTNODEDIALOGTXTDATE, 
 wxID_VIMPORTNODEDIALOGTXTPRJ, wxID_VIMPORTNODEDIALOGTXTPRJTASKFN, 
 wxID_VIMPORTNODEDIALOGTXTTIME, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vImportNodeDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VIMPORTNODEDIALOG,
              name=u'vImportNodeDialog', parent=prnt, pos=wx.Point(181, 101),
              size=wx.Size(451, 394), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Import Node')
        self.SetClientSize(wx.Size(443, 367))

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTNODEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 328),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VIMPORTNODEDIALOGGCBBCANCEL)

        self.lblFileName = wx.StaticText(id=wxID_VIMPORTNODEDIALOGLBLFILENAME,
              label=u'Filename', name=u'lblFileName', parent=self,
              pos=wx.Point(8, 6), size=wx.Size(42, 13), style=0)

        self.txtPrjTaskFN = wx.TextCtrl(id=wxID_VIMPORTNODEDIALOGTXTPRJTASKFN,
              name=u'txtPrjTaskFN', parent=self, pos=wx.Point(8, 22),
              size=wx.Size(336, 21), style=wx.TE_RICH2, value=u'')
        self.txtPrjTaskFN.Enable(True)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTNODEDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Import',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 330),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VIMPORTNODEDIALOGGCBBGENERATE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTNODEDIALOGGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(360, 16),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VIMPORTNODEDIALOGGCBBBROWSE)

        self.txtDate = wx.TextCtrl(id=wxID_VIMPORTNODEDIALOGTXTDATE,
              name=u'txtDate', parent=self, pos=wx.Point(40, 48),
              size=wx.Size(80, 21), style=0, value=u'')

        self.txtTime = wx.TextCtrl(id=wxID_VIMPORTNODEDIALOGTXTTIME,
              name=u'txtTime', parent=self, pos=wx.Point(128, 48),
              size=wx.Size(84, 21), style=0, value=u'')

        self.txtClient = wx.TextCtrl(id=wxID_VIMPORTNODEDIALOGTXTCLIENT,
              name=u'txtClient', parent=self, pos=wx.Point(40, 72),
              size=wx.Size(40, 21), style=0, value=u'')

        self.txtPrj = wx.TextCtrl(id=wxID_VIMPORTNODEDIALOGTXTPRJ,
              name=u'txtPrj', parent=self, pos=wx.Point(112, 72),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblTime = wx.StaticText(id=wxID_VIMPORTNODEDIALOGLBLTIME,
              label=u'Time', name=u'lblTime', parent=self, pos=wx.Point(8, 48),
              size=wx.Size(23, 13), style=0)

        self.lblClt = wx.StaticText(id=wxID_VIMPORTNODEDIALOGLBLCLT,
              label=u'Client', name=u'lblClt', parent=self, pos=wx.Point(8, 72),
              size=wx.Size(26, 13), style=0)

        self.lblPrj = wx.StaticText(id=wxID_VIMPORTNODEDIALOGLBLPRJ,
              label=u'Prj', name=u'lblPrj', parent=self, pos=wx.Point(88, 72),
              size=wx.Size(12, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.selNode=None
        self.doc=None
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.gProcess=None
        
        self.vgpImpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(4,100),size=(200,220),style=0,name="vgpImpTree")
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpImpTree,self.OnImpTreeItemSel)
        
        self.vgpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(224,50),size=(210,270),style=0,name="vgpTree")
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        self.vgpTree.EnableCache()
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getImportBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.txtPrjTaskFN.SetEditable(False)
        
    def OnImpTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.node2inst=event.GetTreeNodeData()
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        self.vgpImpTree.SetLanguage(lang)
        self.vgpTree.SetLanguage(lang)
    def SetMainTree(self,vgpTree):
        self.vgpMainTree=vgpTree
    def SetDoc(self,doc):
        self.doc=doc
        #self.netPrjEng=netPrjEng
        self.vgpTree.SetDoc(doc)
    def SetNode(self,node,nodeSettings):
        #self.node2inst=node2instance
        self.vgpTree.SetNode(node)
        self.node=node
        if self.node is not None:
            node=self.doc.getChild(self.doc.getParent(self.node),'prjinfo')
        else:
            node=None
        if node is None:
            self.prjId=u''
            self.sShort=u''
            self.sClient=u''
        else:
            self.prjId=self.doc.getAttribute(node,'fid')
            self.sShort=self.doc.getNodeText(node,'name')
            self.sClient=self.doc.getNodeText(node,'clientshort')
        
        self.nodeSettings=nodeSettings
        if self.nodeSettings is not None:
            self.sImpFN=self.doc.getNodeText(nodeSettings,'importfn')
        else:
            self.sImpFN=''
        
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.txtDate.SetValue(u'')
        self.txtTime.SetValue(u'')
        self.txtClient.SetValue(u'')
        self.txtPrj.SetValue(u'')
        self.txtPrjTaskFN.SetValue(self.sImpFN)
        self.vgpImpTree.SetNode(None)
        pass
    def GetSelNode(self):
        return self.selNode
    
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
        
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def __getElementByTagName__(self,tagname):
        for e in ELEMENTS:
            if e[0]==tagname:
                return e
        return None
        
    def __checkPossible__(self,node):
        if self.node2inst is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.doc.getTagName(self.node2inst)
        sType=self.doc.getTagName(node)
        elemNode=self.__getElementByTagName__(sType)
        elemNode2Inst=self.__getElementByTagName__(sTypeNode2Inst)
        if elemNode is None:
            return -4
        if elemNode2Inst is None:
            return -3
        try:
            for t in elemNode[1]['__possible']:
                if t[:7]=='__type:':
                    strs=string.split(t,':')
                    if strs[1]==elemNode2Inst[1]['__type']:
                        return 1
                elif t==sTypeNode2Inst:
                    return 1
        except:
            return -5
        return 0
    def __clearAll__(self):
        if self.domImp is not None:
            self.domImp.Close()
            del self.domImp
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.gProcess=None
        
        self.txtDate.SetValue(u'')
        self.txtTime.SetValue(u'')
        self.txtClient.SetValue(u'')
        self.txtPrj.SetValue(u'')
        
    def OnGcbbGenerateButton(self, event):
        #if self.selNode is None:
        #    return
        if self.impData is None:
            return
        
        ret=self.__checkPossible__(self.selNode)
        if ret==1:
            if self.vgpMainTree is not None:
                child=self.doc.cloneNode(self.node2inst,True)
                self.doc.clearIds(child)
                self.doc.recalcIds(child)
                self.doc.appendChild(self.selNode,child)
                self.doc.addNode(self.selNode,child)
                #tup=self.vgpMainTree.__findNodeByID__(None,self.doc.getKey(self.selNode))
                #if (tup is not None) and (child is not None):
                #    self.bAddElement=True
                #    self.vgpMainTree.__addElements__(tup[0],child)
                self.sImpFN=self.txtPrjTaskFN.GetValue()
                self.doc.setNodeText(self.nodeSettings,'importfn',self.sImpFN)
                self.doc.AlignNode(self.nodeSettings,iRec=2)
                #self.__clearAll__()
                #self.EndModal(1)
            else:
                #self.__clearAll__()
                #self.EndModal(0)
                pass
            event.Skip()
            return
        elif ret==0:
            # adding not possible
            sMsg=u'Instancing at this position not allowed.'
        elif ret==-1:
            # no node to instance
            sMsg=u'No node to instance!'
        elif ret==-2:
            # no node selected
            sMsg=u'No target node selected.'
        elif ret==-3:
            # element to instance not found
            sMsg=u'No element of node to instance found!'
        elif ret==-4:
            # element not found
            sMsg=u'No element of target node found!'
        elif ret==-5:
            # error oocurred
            sMsg=u'A serious error has occurred.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        
        #self.EndModal(1)
        event.Skip()
    def OnGcbbBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.txtPrjTaskFN.GetValue()
            #fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if len(fn)>0:
                dlg.SetFilename(fn)
            else:
                dt=wx.DateTime.Now()
                sDate=string.replace(dt.FormatISODate(),'-','')
                sTime=string.replace(dt.FormatISOTime(),':','')
                if self.selNode is not None:
                    sName=self.doc.getNodeText(self.selNode,'tag')
                else:
                    sName=u''
                fn=os.path.join(self.sImpFN,string.join([self.sClient+self.sShort,
                                u'PrjEngExp',sName,sDate,sTime],'_'))+'.xml'
                dlg.SetFilename(self.sImpFN)
            #else:
            #    dlg.SetDirectory(self.tmplDN)
            
            if dlg.ShowModal() == wx.ID_OK:
                self.domImp=None
                self.impNode=None
                self.impData=None
                self.node2inst=None
                
                filename = dlg.GetPath()
                #filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtPrjTaskFN.SetValue(filename)
                self.fn=fn
                self.domImp=vtXmlDom.vtXmlDom()
                self.domImp.Open(filename)
                # fill out import data infos
                self.impNode=self.domImp.getChild(self.domImp.getRoot(),'export')
                createNode=self.domImp.getChild(self.impNode,'create')
                self.txtDate.SetValue(self.domImp.getNodeText(createNode,'date'))
                self.txtTime.SetValue(self.domImp.getNodeText(createNode,'time'))
                self.txtClient.SetValue(self.domImp.getNodeText(createNode,'client'))
                # FIXME: use id attribute to figure out project name
                self.txtPrj.SetValue(self.domImp.getNodeText(createNode,'project'))
                
                self.impData=self.domImp.getChild(self.impNode,'data')
                self.domImp.removeInternalNodes(self.impData)
                childs=self.domImp.getChilds(self.impData)
                self.vgpImpTree.SetDoc(self.domImp,False)
                self.vgpImpTree.SetNode(self.impData)
                if len(childs)>0:
                    self.node2inst=childs[0]
                    self.vgpImpTree.SelectByID(self.domImp.getAttribute(self.node2inst,'id'))
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
