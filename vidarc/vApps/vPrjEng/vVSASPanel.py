#Boa:FramePanel:vVSASPanel
#----------------------------------------------------------------------------
# Name:         vPrjEngMainFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vVSASPanel.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys,string,copy,os.path
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.vXmlVSASTree import *
import images

[wxID_VVSASPANEL, wxID_VVSASPANELGPROCESS, wxID_VVSASPANELLBLDATA, 
 wxID_VVSASPANELLBLGRP, wxID_VVSASPANELLSTDATA, wxID_VVSASPANELLSTGRP, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vVSASPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VVSASPANEL, name=u'vVSASPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(608, 477),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(600, 450))

        self.lblGrp = wx.StaticText(id=wxID_VVSASPANELLBLGRP, label=u'Group',
              name=u'lblGrp', parent=self, pos=wx.Point(20, 2), size=wx.Size(29,
              13), style=0)

        self.lstGrp = wx.ListCtrl(id=wxID_VVSASPANELLSTGRP, name=u'lstGrp',
              parent=self, pos=wx.Point(8, 16), size=wx.Size(220, 200),
              style=wx.LC_REPORT)
        self.lstGrp.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstGroupListItemSelected, id=wxID_VVSASPANELLSTGRP)

        self.lstData = wx.ListCtrl(id=wxID_VVSASPANELLSTDATA, name=u'lstData',
              parent=self, pos=wx.Point(8, 248), size=wx.Size(582, 200),
              style=wx.LC_REPORT)
        self.lstData.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstVendorListItemSelected, id=wxID_VVSASPANELLSTDATA)

        self.lblData = wx.StaticText(id=wxID_VVSASPANELLBLDATA, label=u'Data',
              name=u'lblData', parent=self, pos=wx.Point(8, 234),
              size=wx.Size(23, 13), style=0)

        self.gProcess = wx.Gauge(id=wxID_VVSASPANELGPROCESS, name=u'gProcess',
              parent=self, pos=wx.Point(240, 224), range=100, size=wx.Size(350,
              18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.nodeGrps=None
        self.nodeVendors=None
        self.nodeData=None
        self.selNode=None
        self.lang=None
        self.dlgAttr=None
        self.grpDom=vtXmlDom.vtXmlDom()
        self.grpFN=None
        self.attrName=''
        self.selAttrIdx=-1
        #self.lstDocs.InsertColumn(0,u'Doc')
        self.vgtrVsas=vXmlVSASTree(self,wx.NewId(),
                pos=(240,16),size=(350,200),style=0,name="vgpTree")
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgtrVsas,self.OnTreeItemSel)
        
        self.lstGrp.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstGrp.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,230)
        
        self.lstData.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,70)
        self.lstData.InsertColumn(1,u'Value',wx.LIST_FORMAT_LEFT,330)
            
    def SetDoc(self,doc,baseFN):
        self.selNode=None
        self.doc=doc
        self.baseFN=baseFN
        if self.doc is None:
            self.nodeGrps=None
            self.nodeVendors=None
            self.nodeData=None
            self.grpDom.Close()
            self.grpFN=None
            self.attrName=''
            self.lstData.DeleteAllItems()
            self.lstGrp.DeleteAllItems()
            return
        self.nodeVsas=self.doc.getChild(None,'VSAS')
        self.nodeGrps=self.doc.getChild(self.nodeVsas,'groups')
        self.nodeVendors=self.doc.getChild(self.nodeVsas,'vendors')
        self.nodeData=self.doc.getChild(self.nodeVsas,'data')
        
        self.vsasGrpIds=self.doc.getSortedNodeIds(['groups'],
                        'group','id',[('nr','%s')])
        self.vsasVendorIds=self.doc.getSortedNodeIds(['vendors'],
                        'vendor','id',[('nr','%s')])
            
        self.__updateGroups__(self.nodeGrps)
        self.vgtrVsas.SetVendor(self.vsasVendorIds)
        self.vgtrVsas.SetProcess(self.gProcess)
        
    def __updateGroups__(self,node):
        self.lstGrp.DeleteAllItems()
        if node is None:
            return
        grps=self.doc.getChilds(node,'group')
        for g in grps:
            sNr=self.doc.getNodeText(g,'nr')
            sName=self.doc.getNodeText(g,'name')
            index = self.lstGrp.InsertImageStringItem(sys.maxint, sNr, -1)
            self.lstGrp.SetStringItem(index,1,sName,-1)
    def GetNode(self):
        if self.selNode is None:
            return None
        n=self.doc.createNode('VSAS')
        self.doc.setNodeText(n,'tag','')
        #n.setAttribute('id',0)
        #vtXmlDomTree.setNodeText(node,'part',self.txtTagname.GetValue())
        #vtXmlDomTree.setNodeText(node,'name',self.txtName.GetValue())
        #vtXmlDomTree.setNodeText(node,'description',self.txtDesc.GetValue())
        childs=self.doc.getChilds(self.selNode)
        for child in childs:
            sName=self.doc.getTagName(child)
            if sName in ['group','vendor','part','partnr','price','sellprice','time','text']:
                sVal=self.doc.getText(child)
                if sName=='part':
                    c=self.doc.createSubNode(n,sName)
                    self.doc.setNodeText(c,'type','string')
                    self.doc.setNodeText(c,'val',sVal)
                elif sName=='partnr':
                    sIdVal=self.doc.getAttribute(self.selNode,'id')
                    c=self.doc.createSubNode(n,sName)
                    self.doc.setAttribute(c,'vfid',sIdVal)
                    #vtXmlDomTree.setNodeText(c,'comma','2',doc)
                    #vtXmlDomTree.setNodeText(c,'digits','6',doc)
                    self.doc.setNodeText(c,'type','string')
                    self.doc.setNodeText(c,'val',sVal)
                    #vtXmlDomTree.setNodeText(c,'unit','h',doc)
                elif sName in ['price','sellprice']:
                    sIdVal=self.doc.getAttribute(child,'currency')
                    sNewName=sName+'_'+sIdVal
                    c=self.doc.createSubNode(n,sNewName)
                    self.doc.setNodeText(c,'comma','2')
                    self.doc.setNodeText(c,'digits','8')
                    self.doc.setNodeText(c,'type','float')
                    self.doc.setNodeText(c,'val',sVal)
                    self.doc.setNodeText(c,'unit',sIdVal)
                elif sName in ['group','vendor']:
                    sIdVal=self.doc.getAttribute(child,'id')
                    sNewName=sName
                    c=self.doc.createSubNode(n,sNewName)
                    self.doc.setAttribute(c,'vid',sIdVal)
                    self.doc.setNodeText(c,'type','string')
                    self.doc.setNodeText(c,'val',sVal)
                elif sName=='time':
                    #sIdVal=child.getAttribute('currency')
                    #sNewName=sName+'_'+sIdVal
                    c=self.doc.createSubNode(n,sName)
                    self.doc.setNodeText(c,'comma','2')
                    self.doc.setNodeText(c,'digits','8')
                    self.doc.setNodeText(c,'type','float')
                    self.doc.setNodeText(c,'val',sVal)
                    self.doc.setNodeText(c,'unit','h')
                elif sName in ['text']:
                    self.doc.setNodeText(n,'name',sVal)
                else:
                    if string.find(sName,'price')>=0:
                        sId='currency'
                        sFId=sId
                    else:
                        sId='id'
                        sFId='fid'
                    
                    sIdVal=self.doc.getAttribute(child,sId)
                    if len(sIdVal)>0:
                        self.doc.setNodeTextAttr(n,sName,sVal,sFId,sIdVal)
                    else:
                        self.doc.setNodeText(n,sName,sVal)
        return n
        #vtXmlDomTree.vtXmlDomAlignNode(doc,n)
        
    def OnLstGroupListItemSelected(self, event):
        idx=event.GetIndex()
        grps=self.doc.getChilds(self.nodeData,'group')
        it=self.lstGrp.GetItem(idx,0)
        sSel=it.m_text
        
        for g in grps:
            sNr=self.doc.getNodeText(g,'nr')
            if sNr==sSel:
                self.grpFN=self.doc.getNodeText(g,'filename')
                self.grpDom.Close()
                d=os.path.split(self.baseFN)
                sFN=os.path.join(d[0],self.grpFN)
                
                self.grpDom.Open(sFN)
                self.grpNode=self.grpDom.getChild(None,'VSAS')
                self.grpNode=self.grpDom.getChild(self.grpNode,'data')
                self.vgtrVsas.SetNode(self.grpDom,self.grpNode)
                pass
        event.Skip()
    def OnLstVendorListItemSelected(self, event):
        idx=event.GetIndex()
        idxElem=self.lstVendor.GetItemData(idx)
        event.Skip()
    def OnTreeItemSel(self,event):
        self.lstData.DeleteAllItems()
        node=event.GetTreeNodeData()
        self.selNode=node
        if node is not None:
            childs=self.doc.getChilds(node,None)
            for c in childs[3:]:
                sName=self.doc.getTagName(c)
                if string.find(sName,'price')>=0:
                    sCur=self.doc.getAttribute(c,'currency')
                    sName=sName+' '+sCur
                sVal=self.doc.getText(c)
                index = self.lstData.InsertImageStringItem(sys.maxint, sName, -1)
                self.lstData.SetStringItem(index,1,sVal,-1)
             
