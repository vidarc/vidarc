import wx
#----------------------------------------------------------------------------
# Name:         evtMainFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: evtMainFrame.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

# defined event for main frame status information
wxEVT_MAIN_START=wx.NewEventType()
def EVT_MAIN_START(win,func):
    win.Connect(-1,-1,wxEVT_MAIN_START,func)
class vMainStart(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_MAIN_START(<widget_name>, self.OnStart)
    """

    def __init__(self,text,count):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_MAIN_START)
        self.text=text
        self.count=count
    def GetText(self):
        return self.text
    def GetCount(self):
        return self.count

# defined event for main frame status information
wxEVT_MAIN_FINISHED=wx.NewEventType()
def EVT_MAIN_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_MAIN_FINISHED,func)
class vMainFinished(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_MAIN_FINISHED(<widget_name>, self.OnFinished)
    """

    def __init__(self,text,count):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_MAIN_FINISHED)
        self.text=text
    def GetText(self):
        return self.text

# defined event for main frame status information
wxEVT_MAIN_VALUE=wx.NewEventType()
def EVT_MAIN_VALUE(win,func):
    win.Connect(-1,-1,wxEVT_MAIN_VALUE,func)
class vMainValue(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_MAIN_VALUE(<widget_name>, self.OnValue)
    """

    def __init__(self,act):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_MAIN_START)
        self.act=act
    def GetValue(self):
        return self.act
