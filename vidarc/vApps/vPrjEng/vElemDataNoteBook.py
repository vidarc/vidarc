#----------------------------------------------------------------------------
# Name:         vElemDataNoteBook.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElemDataNoteBook.py,v 1.3 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#import ColorPanel
import images_data

import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import *

from vidarc.vApps.vPrjEng.evtDataChanged import *
import vidarc.vApps.vPrjEng.vLogPanel as vLogPanel
import vidarc.vApps.vPrjEng.vAttrPanel as vAttrPanel
import vidarc.vApps.vPrjEng.vDocPanel as vDocPanel
import vidarc.vApps.vPrjEng.vSafePanel as vSafePanel
import vidarc.vApps.vPrjEng.vDrawPanel as vDrawPanel
import vidarc.vApps.vPrjEng.vActivitiesPanel as vActivitiesPanel
#from boa.vApps.vPrjDoc.vgpPrjDocInfo import *

#class vglbElemData(wx.Listbook):
#    def _init_ctrls(self, prnt,id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), style=wx.LB_DEFAULT, name=''):
#        wx.Listbook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)
        
class vElemDataNoteBook(wx.Notebook):
    def _init_ctrls(self, prnt,id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), style=wx.LB_DEFAULT, name=''):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)
    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent,id, pos, size, style, name)
        self.node=None
        self.doc=None
        self.nodeSet=[]
        self.SetAutoLayout(True)
        #self.SetSizer(wx.NotebookSizer(self))
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        #self.Bind(wx.EVT_SIZE, self.OnSize)

        edit=[u'Attr',u'Activities',u'Doc',u'Safe',u'Draw',u'Log']
        il = wx.ImageList(32, 32)
        for e in edit:
            s='v%sPanel'%e
            f=getattr(images_data, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='v%sPanel'%e
            f=getattr(eval('v%sPanel'%e),'v%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            def OnCPSize(evt, win=panel):
                win.SetSize(evt.GetSize())
            #panel.Bind(wx.EVT_SIZE, OnCPSize)
            panel.SetAutoLayout(True)
            EVT_DATA_CHANGED(panel,self.OnDataChanged)
            if e=='Attr':
                vAttrPanel.EVT_VTXMLTREE_ITEM_GOTO_TMPL(panel,self.OnGotoTmpl)
                vAttrPanel.EVT_VTXMLTREE_ITEM_GOTO(panel,self.OnGoto)
            self.AddPage(panel,e,False,i)
            self.lstPanel.append(panel)
            i=i+1
        #self.Layout()
        EVT_REVISION_CHANGED(self.lstPanel[self.lstName.index(u'Safe')],
                        self.OnRevChanged)
    def SetAutoApply(self,flag):
        for pn in self.lstPanel:
            pn.SetAutoApply(flag)
    def SetHumans(self,humans,humDoc=None):
        for e in [u'Log',u'Activities',u'Doc',u'Safe']:
            self.lstPanel[self.lstName.index(e)].SetHumans(humans,humDoc)
    def SetNetDocHuman(self,netDoc):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Log',u'Activities',u'Doc',u'Safe']:
                self.lstPanel[i].SetNetDocHuman(netDoc)
    def SetNetDocPrj(self,netPrj):
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.netPrj=netPrj
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Attr',u'Doc']:
                self.lstPanel[i].SetNetDocPrj(netPrj)
    def SetNetDocPrjDoc(self,netPrjDoc):
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.netPrjDoc=netPrjDoc
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Doc']:
                self.lstPanel[i].SetNetDocPrjDoc(netPrjDoc)
    def SetPrjDir(self,dn):
        #self.prjDN=dn
        #if self.pnDoc is not None:
        #    self.pnDoc.SetTemplateBaseDir(self.prjDN)
        
        #for panel in self.vpgDocGrpInfos:
        #    panel.SetPrjDir(self.prjDN)
        pass
    def IsBusy(self):
        return False
    def IsThreadRunning(self):
        bRunning=False
        return bRunning
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'Stop',
                origin=self.GetName())
    def SetDoc(self,doc,bNet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'SetDoc net:%d'%(bNet),
                origin=self.GetName())
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.node=None
        self.doc=doc
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDoc(doc,bNet)
        self.doc.AddConsumer(self,self.Clear)
        
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def Lock(self,flag):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Attr',u'Doc',u'Safe',u'Log']:
                self.lstPanel[i].Lock(flag)
    def SetNetDocument(self,doc):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Attr',u'Doc']:
                self.lstPanel[i].SetNetDocument(doc)
    def SetPrjDN(self,fn):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Attr',u'Doc']:
                self.lstPanel[i].SetPrjDN(fn)
    def SetLanguage(self,lang):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Attr',u'Doc']:
                self.lstPanel[i].SetLanguage(lang)
    def GetS88draw(self):
        for i in range(0,len(self.lstName)):
            if self.lstName[i]==u'Draw':
                return self.lstPanel[i].GetS88draw()
    def Clear(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'Clear',
                origin=self.GetName())
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].Clear()
        self.node=None
        self.nodeSet=[]
    def SetNode(self,node):
        self.node=node
        sel=self.GetSelection()
        if 1==0:
            if self.node is not None:
                sName=self.doc.getTagName(node)
                sfcNode=self.doc.getChild(self.node,'drawSFC')
                s88Node=self.doc.getChild(self.node,'drawS88')
            else:
                sName=''
                sfcNode=None
                s88Node=None
        self.nodeSet=[]
        for i in range(0,len(self.lstName)):
            if 1==0:
                if sName==u'Doc':
                    if self.lstName[i]==u'Doc':
                        self.SetSelection(i)
                elif sfcNode is not None:
                    if self.lstName[i]==u'Draw':
                        self.SetSelection(i)
                elif s88Node is not None:
                    if self.lstName[i]==u'Draw':
                        self.SetSelection(i)
                else:
                    self.SetSelection(0)
            if sel==i:
                self.lstPanel[i].SetNode(self.lstName[i],node)
                self.nodeSet.append(i)
    def Apply(self,bDoApply=False):
        bChanged=False
        for i in range(0,len(self.lstName)):
            try:
                self.nodeSet.index(i)
                #vtLog.vtLogCallDepth(None,'i:%d name:%s'%(i,self.lstName[i]),verbose=1)
                #if self.lstPanel[i].bModified:
                #    bChanged=True
                if self.lstPanel[i].Apply(bDoApply):
                    bChanged=True
            except:
                pass
        #if self.node is not None:
        #    vtLog.vtLogCallDepth(None,'key:%s'%(self.doc.getKey(self.node)),verbose=1)
        if bChanged:
            return self.node
        return None
    def Cancel(self):
        bChanged=False
        for i in range(0,len(self.lstName)):
            try:
                self.nodeSet.index(i)
                self.lstPanel[i].Cancel()
            except:
                pass
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        #vtLog.CallStack('')
        #print old
        #print sel
        if sel>=len(self.lstPanel):
            return
        try:
            self.nodeSet.index(sel)
        except:
            self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
            self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def OnDataChanged(self,event):
        #self.pnCtrl.GetNode(self.node)
        #if self.pnDoc is not None:
        #    self.pnDoc.GetNode(selPanel.GetSelectedDoc(),
        #            selPanel.GetLanguage())
        #for panel in self.vpgDocGrpInfos:
        #    panel.GetNode(self.node)
        wx.PostEvent(self,vgpDataChanged(self,event.GetNode()))
        pass
    def OnRevChanged(self,event):
        for i in range(0,len(self.lstName)):
            if self.lstName[i] in [u'Log',u'Safe']:
                continue
            self.lstPanel[i].__setModified__(False)
            self.lstPanel[i].SetNode(self.lstName[i],self.node)
        wx.PostEvent(self,vgpRevisionChanged(self,self.node))
        
    def OnDocGrpCanceled(self,event):
        #self.pnCtrl.SetNode(self.node,self.ids)
        #for panel in self.vpgDocGrpInfos:
        #    panel.SetNode(self.node,self.ids)
        #wxPostEvent(self,vgpPrjDocGrpInfoCanceled(self,self.node,None))
        pass
    def OnDocumentSelected(self,event):
        #wxPostEvent(self,
        #    vgpPrjDocumentSelected(self,event.GetNode(),event.GetLanguage()))
        
        #if self.pnDoc is not None:
        #    self.pnDoc.SetNode(event.GetNode(),event.GetLanguage())
        #    pass
        pass
    def OnDocumentChanged(self,event):
        #sel = self.GetSelection() -1
        #selPanel=self.vpgDocGrpInfos[sel]
        #selPanel.RefreshDocuments()
        #wxPostEvent(self,vgpPrjDocChanged(self,None,self.lang))
        pass
    def OnDocChanged(self,event):
        #wxPostEvent(self,vgpPrjDocChanged(self,None,None))
        pass
    def OnGoto(self,evt):
        wx.PostEvent(self,vgpAttr.vgtrXmlTreeItemGoto(evt.GetID(),
                evt.GetTreeNodeData()))
    def OnGotoTmpl(self,evt):
        wx.PostEvent(self,vgpAttr.vgtrXmlTreeItemGotoTmpl(self,
                evt.GetTreeNode(),evt.GetTreeNodeData()))
    def OnSize(self,evt):
        #self.Validate()
        vtLog.CallStack('')
        print 'on size',evt.GetSize()
        #self.SetSize(evt.GetSize())
        self.Layout()
        self.Refresh()
        #wx.CallAfter(self.Refresh)
        for pn in self.lstPanel:
            #pn.Layout()
            pn.Refresh()
            #wx.CallAfter(pn.Layout)
            #wx.CallAfter(pn.Refresh)
            #wx.CallAfter(pn.Refresh)
        #wx.CallAfter(pn.Update)
        evt.Skip()
    #def OnPanelSize(self,evt):
    #    self.SetSize(evt.GetSize())
    #    evt.Skip()
        
