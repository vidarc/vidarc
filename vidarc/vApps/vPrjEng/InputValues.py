#----------------------------------------------------------------------------
# Name:         InputValues.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: InputValues.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.numctrl
import wx.lib.masked.maskededit

class InputValues:

    def __init__(self, parent, pos, size,shown=True):
        w=30
        self.chcValue = wx.Choice(choices=[], id=wx.NewId(),
              name=u'chcValue', parent=parent, pos=pos,
              size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0)
        self.chcValue.Show(False)
        
        self.numValue = wx.lib.masked.numctrl.NumCtrl(id=wx.NewId(),
              name=u'numValue', autoSize=0,parent=parent, pos=pos,
              size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
        self.numValue.Show(False)
        
        self.strValue = wx.lib.masked.textctrl.BaseMaskedTextCtrl(id=wx.NewId(),
              name=u'strValue',parent=parent, pos=pos,
              size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
        self.strValue.Show(False)
        
        self.lblUnit = wx.StaticText(id=wx.NewId(), label=u'',
              name=u'lblUnit', parent=parent, pos=wx.Point(pos.x+size.GetWidth()-w+3, pos.y+3),
              size=wx.Size(w-5,size.GetHeight()-5), style=0)

        #self.chcValue.Show(False)
        #self.numValue.Show(False)
        self.strValue.Show(shown)
        self.iSel=0
    def Set(self,dict,dictType):
        if dictType is None:
            dictType=dict
        try:
            sType=dict['type']
        except:
            sType='string'
        if sType in ['string','text']:
            self.iSel=0
            self.numValue.Show(False)
            self.chcValue.Show(False)
            self.strValue.Show(True)
            try:
                self.strValue.SetValue(dict['val'])
            except:
                self.strValue.SetValue(u'')
        elif sType in ['int','long']:
            self.iSel=1
            self.strValue.Show(False)
            self.chcValue.Show(False)
            self.numValue.Show(True)
            try:
                sMin=long(dictType['min'])
            except:
                sMin=None
            self.numValue.SetMin(sMin)
            try:
                sMax=long(dictType['max'])
            except:
                sMax=None
            self.numValue.SetMax(sMax)
            iIntW=12
            self.numValue.SetIntegerWidth(iIntW)
            iFriW=0
            if sType=='int':
                self.fmt='%'+'%d'%(iIntW)+'d'
            elif sType=='long':
                self.fmt='%'+'%d'%(iIntW)+'d'
            else:
                self.fmt='%d'                
            self.numValue.SetFractionWidth(iFriW)
            try:
                self.numValue.SetValue(int(dict['val']))
            except:
                self.numValue.SetValue(0)
        elif sType in ['float','double','pseudofloat']:
            self.iSel=1
            self.strValue.Show(False)
            self.chcValue.Show(False)
            self.numValue.Show(True)
            try:
                sMin=float(dictType['min'])
            except:
                sMin=None
            self.numValue.SetMin(sMin)
            try:
                sMax=float(dictType['max'])
            except:
                sMax=None
            self.numValue.SetMax(sMax)
            try:
                iIntW=int(dictType['digits'])
            except:
                iIntW=8
            self.numValue.SetIntegerWidth(iIntW)
            try:
                iFriW=int(dictType['comma'])
            except:
                iFriW=2
            self.fmt='%'+'%d'%iIntW+'.'+'%d'%iFriW+'f'
            self.numValue.SetFractionWidth(iFriW)
            try:
                self.numValue.SetValue(dict['val'])
            except:
                self.numValue.SetValue(0.0)
        elif sType=='choice':
            self.iSel=2
            self.strValue.Show(False)
            self.numValue.Show(False)
            self.chcValue.Show(True)
            self.chcValue.Clear()
            keys=dictType.keys()
            keys.sort()
            for k in keys:
                if k[:2]=='it':
                    self.chcValue.Append(dictType[k])
            self.chcValue.SetStringSelection(dict['val'])
        try:
            sUnit=dictType['unit']
        except:
            sUnit=''
        self.lblUnit.SetLabel(sUnit)
        self.lblUnit.Show(True)
    def Enable(self,state):
        self.strValue.Show(state)
        self.numValue.Show(False)
        self.chcValue.Show(False)
        self.lblUnit.Show(state)
    def SetConstraints(self,lc,lc2):
        self.strValue.SetConstraints(lc)
        self.numValue.SetConstraints(lc)
        #self.chcValue.SetContraints(lc)
        self.lblUnit.SetConstraints(lc2)
    def Clear(self):
        self.iSel=0
        self.strValue.Show(True)
        self.numValue.Show(False)
        self.chcValue.Show(False)
        self.strValue.SetValue('')
        self.lblUnit.SetLabel('')
    def Get(self):
        if self.iSel==0:
            return self.strValue.GetValue()
        elif self.iSel==1:
            return self.fmt%self.numValue.GetValue()
        elif self.iSel==2:
            return self.chcValue.GetString(self.chcValue.GetSelection())
    