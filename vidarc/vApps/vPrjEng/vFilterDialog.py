#Boa:Dialog:vFilterDialog
#----------------------------------------------------------------------------
# Name:         vFilterDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vFilterDialog.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string,copy
import vidarc.vApps.vPrjEng.vElementsPanel as ELEMENTS
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import images

def create(parent):
    return vFilterDialog(parent)

[wxID_VFILTERDIALOG, wxID_VFILTERDIALOGCBADDATTR, wxID_VFILTERDIALOGCBADDELEM, 
 wxID_VFILTERDIALOGCBCANCEL, wxID_VFILTERDIALOGCBDELATTR, 
 wxID_VFILTERDIALOGCBDELELEM, wxID_VFILTERDIALOGCBOK, 
 wxID_VFILTERDIALOGLBLATTR, wxID_VFILTERDIALOGLBLELEM, 
 wxID_VFILTERDIALOGLSTATTR, wxID_VFILTERDIALOGLSTATTRFILTER, 
 wxID_VFILTERDIALOGLSTELEM, wxID_VFILTERDIALOGLSTELEMFILTER, 
 wxID_VFILTERDIALOGLSTITEM, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vFilterDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VFILTERDIALOG, name=u'vFilterDialog',
              parent=prnt, pos=wx.Point(337, 41), size=wx.Size(432, 475),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'v Prj Eng Filter')
        self.SetClientSize(wx.Size(424, 441))

        self.lstElem = wx.ListCtrl(id=wxID_VFILTERDIALOGLSTELEM,
              name=u'lstElem', parent=self, pos=wx.Point(8, 24),
              size=wx.Size(160, 100), style=wx.LC_REPORT)

        self.lblElem = wx.StaticText(id=wxID_VFILTERDIALOGLBLELEM,
              label=u'Elements', name=u'lblElem', parent=self, pos=wx.Point(8,
              8), size=wx.Size(43, 13), style=0)

        self.cbAddElem = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBADDELEM,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddElem',
              parent=self, pos=wx.Point(176, 40), size=wx.Size(72, 30),
              style=0)
        self.cbAddElem.Bind(wx.EVT_BUTTON, self.OnCbAddElemButton,
              id=wxID_VFILTERDIALOGCBADDELEM)

        self.cbDelElem = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBDELELEM,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelElem',
              parent=self, pos=wx.Point(176, 80), size=wx.Size(72, 30),
              style=0)
        self.cbDelElem.Bind(wx.EVT_BUTTON, self.OnCbDelElemButton,
              id=wxID_VFILTERDIALOGCBDELELEM)

        self.lstElemFilter = wx.ListCtrl(id=wxID_VFILTERDIALOGLSTELEMFILTER,
              name=u'lstElemFilter', parent=self, pos=wx.Point(256, 24),
              size=wx.Size(160, 100), style=wx.LC_REPORT)

        self.lstAttr = wx.ListCtrl(id=wxID_VFILTERDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 152),
              size=wx.Size(160, 100), style=wx.LC_REPORT)

        self.lblAttr = wx.StaticText(id=wxID_VFILTERDIALOGLBLATTR,
              label=u'Attributes', name=u'lblAttr', parent=self, pos=wx.Point(8,
              136), size=wx.Size(44, 13), style=0)

        self.cbAddAttr = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBADDATTR,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddAttr',
              parent=self, pos=wx.Point(176, 168), size=wx.Size(72, 30),
              style=0)
        self.cbAddAttr.Bind(wx.EVT_BUTTON, self.OnCbAddAttrButton,
              id=wxID_VFILTERDIALOGCBADDATTR)

        self.cbDelAttr = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBDELATTR,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelAttr',
              parent=self, pos=wx.Point(176, 208), size=wx.Size(72, 30),
              style=0)
        self.cbDelAttr.Bind(wx.EVT_BUTTON, self.OnCbDelAttrButton,
              id=wxID_VFILTERDIALOGCBDELATTR)

        self.lstAttrFilter = wx.ListCtrl(id=wxID_VFILTERDIALOGLSTATTRFILTER,
              name=u'lstAttrFilter', parent=self, pos=wx.Point(256, 152),
              size=wx.Size(160, 100), style=wx.LC_REPORT)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(120, 408), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VFILTERDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFILTERDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(240, 408), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VFILTERDIALOGCBCANCEL)

        self.lstItem = wx.ListCtrl(id=wxID_VFILTERDIALOGLSTITEM,
              name=u'lstItem', parent=self, pos=wx.Point(8, 272),
              size=wx.Size(408, 128), style=wx.LC_REPORT)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.filter_elem=[]
        self.filter_attr=[]
        self.doc=None
        self.selNode=None
        self.node=None
        self.relNode=None
        self.bUpdate=False
        self.bSelect=False
        self.ID2Select=''
        
        img=images.getApplyBitmap()
        self.cbOk.SetBitmapLabel(img)

        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)

        img=images.getAddBitmap()
        self.cbAddElem.SetBitmapLabel(img)
        self.cbAddAttr.SetBitmapLabel(img)

        img=images.getDelBitmap()
        self.cbDelElem.SetBitmapLabel(img)
        self.cbDelAttr.SetBitmapLabel(img)

        self.lstElem.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,110)
        self.lstElem.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,45)

        self.lstElemFilter.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,130)
        
        self.lstAttr.InsertColumn(0,u'Attr',wx.LIST_FORMAT_LEFT,130)
        self.lstAttr.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,45)

        self.lstAttrFilter.InsertColumn(0,u'Attr',wx.LIST_FORMAT_LEFT,130)
        
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,200)
        self.lstItem.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,100)
        self.lstItem.InsertColumn(2,u'Val',wx.LIST_FORMAT_LEFT,60)
    def __procNode__(self,node):
        tagName=self.doc.getTagName(node)
        try:
            lst=self.dElem[tagName]
            sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
            lst.append((sHier,node))
        except:
            sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
            self.dElem[tagName]=[(sHier,node)]
        self.iCount+=1
        childs=self.doc.getChilds(node)
        for c in childs:
            if self.doc.getTagName(c)==u'filter':
                continue
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__procNode__(c)
            pass
    def __procAttr__(self,node):
        nodeType=self.doc.getChild(node, 'type')
        bSkip=False
        bSkipChild=False
        tagName=self.doc.getTagName(node)
        if len(self.doc.getAttribute(node,'id'))>0:
            bSkip=True
        if tagName[:2]=='__':
            bSkip=True
            bSkipChild=True
        if ELEMENTS.isProperty(tagName,'isSkip'):
            bSkipChild=True
        if nodeType is not None:
            if len(self.doc.getChilds(nodeType))==0:
                #bSkip=True
                bSkipChild=True
        if bSkip==False:
            parTagName=self.doc.getTagName(self.doc.getParent(node))
            if parTagName in self.filter_elem:
                try:
                    lst=self.dAttr[tagName]
                    sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                    lst.append((sHier,node))
                except:
                    sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                    self.dAttr[tagName]=[(sHier,node)]
                self.iAttrCount+=1
        if bSkipChild==False:
            childs=self.doc.getChilds(node)
            for c in childs:
                self.__procAttr__(c)
    def isFilterElemOk(self,node):
        parTagName=self.doc.getTagName(self.doc.getParent(node))
        if parTagName in self.filter_elem:
            return True
        return False
    def isFilterAttrOk(self,node):
        parTagName=self.doc.getTagName(self.doc.getParent(node))
        if parTagName in self.filter_elem:
            if self.doc.getTagName(node) in self.filter_attr:
                return True
        return False
    def __updateElem__(self):
        #self.txtNodeCount.SetValue('%d'%self.iCount)
        keys=self.dElem.keys()
        keys.sort()
        self.selElem=-1
        self.lstElem.DeleteAllItems()
        for k in keys:
            index = self.lstElem.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dElem[k])
            self.lstElem.SetStringItem(index,1,'%d'%iCount,-1)
    def __updateFilterElem__(self):
        self.lstElemFilter.DeleteAllItems()
        for k in self.filter_elem:
            index = self.lstElemFilter.InsertImageStringItem(sys.maxint, k, -1)
            #iCount=len(self.dElem[k])
            #self.lstElemFilter.SetStringItem(index,1,'%d'%iCount,-1)
    def __updateAttr__(self):
        #self.txtAttrCount.SetValue('%d'%self.iAttrCount)
        keys=self.dAttr.keys()
        keys.sort()
        self.selAttr=-1
        self.lstAttr.DeleteAllItems()
        for k in keys:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dAttr[k])
            self.lstAttr.SetStringItem(index,1,'%d'%iCount,-1)
    def __updateFilterAttr__(self):
        self.lstAttrFilter.DeleteAllItems()
        for k in self.filter_attr:
            index = self.lstAttrFilter.InsertImageStringItem(sys.maxint, k, -1)
    def __procItem__(self,node,lst):
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                if ELEMENTS.isProperty(self.doc.getTagName(c),'isSkip'):
                    pass
                else:
                    if self.doc.getTagName(c)!='filter':
                        self.__procItem__(c,lst)
            else:
                if self.doc.getTagName(c)[0:2]=='__':
                    continue
                if self.isFilterAttrOk(c):
                    sHier=Hierarchy.getTagNames(self.doc,self.node,self.doc.getParent(self.node),c)
                    lst.append((sHier,c))
    def __updateItem__(self):
        self.lstItem.DeleteAllItems()
        lst=[]
        self.__procItem__(self.doc.getParent(self.node),lst)
        for it in lst:
            idx=self.lstItem.InsertImageStringItem(sys.maxint, it[0], -1)
            self.lstItem.SetStringItem(idx,1,self.doc.getTagName(it[1]),-1)
            if self.doc.getTagName(it[1]) in ['tag','name','description']:
                val=self.doc.getText(it[1])
            else:
                if len(self.doc.getAttribute(it[1],'ih'))<=0:
                    val=self.doc.getNodeText(it[1],'val')
                else:
                    val=self.doc.getText(it[1])
            self.lstItem.SetStringItem(idx,2,val,-1)
    def GetNodeList(self):
        lst=[]
        self.__procItem__(self.doc.getParent(self.node),lst)
        return lst
    def GetFilterElem(self):
        return self.filter_elem
    def GetFilterAttr(self):
        return self.filter_attr
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node,doVisible=True):
        self.node=node
        self.dElem={}
        self.dAttr={}
        
        if doVisible:
            self.lstElem.DeleteAllItems()
            self.lstElemFilter.DeleteAllItems()
            self.lstAttr.DeleteAllItems()
            self.lstAttrFilter.DeleteAllItems()
        # get XML info
        s=self.doc.getNodeText(self.node,'filter_elem')
        self.filter_elem=string.split(s,',')
        s=self.doc.getNodeText(self.node,'filter_attr')
        self.filter_attr=string.split(s,',')
        
        # do visual update
        self.iCount=0
        self.iAttrCount=0
        self.iSumCount=0
        parNode=self.doc.getParent(self.node)
        self.__procNode__(parNode)
        self.selItem=-1
        if doVisible:
            self.lstItem.DeleteAllItems()
            self.__updateElem__()
            self.__updateFilterElem__()
            
            self.dAttr={}
            self.__procAttr__(self.doc.getParent(self.node))
            
            self.__updateAttr__()
            self.__updateFilterAttr__()
            self.__updateItem__()
    def GetNode(self):
        if self.node is None:
            return
        s=string.join(self.filter_elem,',')
        self.doc.setNodeText(self.node,'filter_elem',s)
        
        s=string.join(self.filter_attr,',')
        self.doc.setNodeText(self.node,'filter_attr',s)
        self.doc.AlignNode(self.node,iRec=2)
    def OnCbAddElemButton(self, event):
        iCount=self.lstElem.GetItemCount()
        bChanged=False
        for i in range(0,iCount):
            it=self.lstElem.GetItem(i)
            if self.lstElem.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                try:
                    idx=self.filter_elem.index(it.m_text)
                except:
                    self.filter_elem.append(it.m_text)
                    bChanged=True
                #idx=self.lstElemFilter.FindItem(0,it.m_text)
                #if idx<0:
                #    self.lstElemFilter.InsertImageStringItem(sys.maxint, it.m_text, -1)
        if bChanged:
            self.filter_elem.sort()
            self.__updateFilterElem__()
            self.dAttr={}
            self.__procAttr__(self.doc.getParent(self.node))
            self.__updateAttr__()
        event.Skip()

    def OnCbDelElemButton(self, event):
        iCount=self.lstElemFilter.GetItemCount()
        bChanged=False
        for i in range(0,iCount):
            it=self.lstElemFilter.GetItem(i)
            if self.lstElemFilter.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                try:
                    self.filter_elem.remove(it.m_text)
                    bChanged=True
                except:
                    pass
        if bChanged:
            self.filter_elem.sort()
            self.__updateFilterElem__()
            self.dAttr={}
            self.__procAttr__(self.doc.getParent(self.node))
            self.__updateAttr__()
            bChanged=False
            for k in copy.copy(self.filter_attr):
                if not self.dAttr.has_key(k):
                    self.filter_attr.remove(k)
                    bChanged=True
            if bChanged:
                self.filter_attr.sort()
                self.__updateFilterAttr__()
                self.__updateItem__()
        event.Skip()

    def OnCbAddAttrButton(self, event):
        iCount=self.lstAttr.GetItemCount()
        bChanged=False
        for i in range(0,iCount):
            it=self.lstAttr.GetItem(i)
            if self.lstAttr.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                try:
                    idx=self.filter_attr.index(it.m_text)
                except:
                    self.filter_attr.append(it.m_text)
                    bChanged=True
        if bChanged:
            self.filter_attr.sort()
            self.__updateFilterAttr__()
            self.__updateItem__()
        event.Skip()

    def OnCbDelAttrButton(self, event):
        iCount=self.lstAttrFilter.GetItemCount()
        bChanged=False
        for i in range(0,iCount):
            it=self.lstAttrFilter.GetItem(i)
            if self.lstAttrFilter.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                try:
                    self.filter_attr.remove(it.m_text)
                    bChanged=True
                except:
                    pass
        if bChanged:
            self.filter_attr.sort()
            self.__updateFilterAttr__()
            self.__updateItem__()
        event.Skip()

    def OnCbOkButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
