#Boa:Dialog:vBrowseElementAttr4DocDialog
#----------------------------------------------------------------------------
# Name:         vBrowseElementAttr4DocDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vBrowseElementAttr4DocDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string
from vidarc.vApps.vPrjEng.vXmlPrjEngTree  import *
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.vElementsPanel as ELEMENTS

import images

def create(parent):
    return vBrowseElementAttr4DocDialog(parent)

[wxID_VBROWSEELEMENTATTR4DOCDIALOG, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBAPPLY, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBCANCEL, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGGPROCESS, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGLBLNAME, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTR, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTRVAL, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTELEMENTS, 
 wxID_VBROWSEELEMENTATTR4DOCDIALOGTXTNAME, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vBrowseElementAttr4DocDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VBROWSEELEMENTATTR4DOCDIALOG,
              name=u'vBrowseElementAttr4DocDialog', parent=prnt,
              pos=wx.Point(237, 99), size=wx.Size(544, 384),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Browse Element Attribute for Document')
        self.SetClientSize(wx.Size(536, 357))
        self.Bind(wx.EVT_ACTIVATE, self.OnVgdBrowseActionNameActivate)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(176, 320), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(280, 320),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGGCBBCANCEL)

        self.lstElements = wx.ListCtrl(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTELEMENTS,
              name=u'lstElements', parent=self, pos=wx.Point(320, 8),
              size=wx.Size(208, 100), style=wx.LC_REPORT)
        self.lstElements.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstElementsListItemSelected,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTELEMENTS)
        self.lstElements.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstElementsListItemDeselected,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTELEMENTS)

        self.lstAttr = wx.ListCtrl(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(320, 112),
              size=wx.Size(208, 100), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTR)

        self.lblName = wx.StaticText(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(48, 4),
              size=wx.Size(160, 21), style=0, value=u'')

        self.gProcess = wx.Gauge(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(8, 294), range=100,
              size=wx.Size(292, 18), style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)

        self.lstAttrVal = wx.ListCtrl(id=wxID_VBROWSEELEMENTATTR4DOCDIALOGLSTATTRVAL,
              name=u'lstAttrVal', parent=self, pos=wx.Point(320, 216),
              size=wx.Size(208, 96), style=wx.LC_REPORT)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.selNode=None
        self.node=None
        self.relNode=None
        self.bSet=False
        self.bUpdate=False
        self.bSelect=False
        self.ID2Select=''
        self.attrs=''
        self.dElem={}
        self.dAttr={}
        
        self.vgpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(4,40),size=(304,244),style=0,name="vgpTree",gui_update=True)
        self.vgpTree.EnableCache()
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #self.vgpTree.SetProcessBar(self.gProcess,None)
        self.vgpTree.EnableCache()
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        self.vgpTree.SetNetResponse(False)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        self.lstElements.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,110)
        self.lstElements.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,45)
        
        self.lstAttr.InsertColumn(0,u'Attr',wx.LIST_FORMAT_LEFT,130)
        self.lstAttr.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,45)

        self.lstAttrVal.InsertColumn(0,u'Attr',wx.LIST_FORMAT_LEFT,130)
        self.lstAttrVal.InsertColumn(1,u'Count',wx.LIST_FORMAT_RIGHT,45)
        
    def __procNode__(self,node):
        tagName=self.doc.getTagName(node)
        if tagName=='filter':
            s=self.doc.getNodeText(node,'filter_elem')
            lst=string.split(s,',')
            for it in lst:
                self.dElem[it]=[(it,None)]
        else:
            try:
                lst=self.dElem[tagName]
                sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                lst.append((sHier,node))
            except:
                sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                self.dElem[tagName]=[(sHier,node)]
            childs=self.doc.getChildsAttr(node,'id')
            for c in childs:
                self.__procNode__(c)
                pass
    def __procAttr__(self,node):
        tagName=self.doc.getTagName(node)
        if tagName=='filter':
            s=self.doc.getNodeText(node,'filter_attr')
            lst=string.split(s,',')
            for it in lst:
                self.dAttr[it]=[(it,None)]
        else:
            nodeType=self.doc.getChild(node, 'type')
            bSkip=False
            bSkipChild=False
            if len(self.doc.getAttribute(node,'id'))>0:
                bSkip=True
            if tagName[:2]=='__':
                bSkip=True
                bSkipChild=True
            if ELEMENTS.isProperty(tagName,'isSkip'):
                bSkipChild=True
            if nodeType is not None:
                if len(self.doc.getChilds(nodeType))==0:
                    #bSkip=True
                    bSkipChild=True
            if bSkip==False:
                try:
                    lst=self.dAttr[tagName]
                    sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                    lst.append((sHier,node))
                except:
                    sHier=Hierarchy.getTagNames(self.doc,self.node,None,node)
                    self.dAttr[tagName]=[(sHier,node)]
            if bSkipChild==False:
                childs=self.doc.getChilds(node)
                for c in childs:
                    self.__procAttr__(c)
    def __updateElem__(self):
        keys=self.dElem.keys()
        keys.sort()
        self.selElem=-1
        self.lstElements.DeleteAllItems()
        for k in keys:
            index = self.lstElements.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dElem[k])
            self.lstElements.SetStringItem(index,1,'%d'%iCount,-1)
    def __updateAttr__(self):
        keys=self.dAttr.keys()
        keys.sort()
        self.selAttr=-1
        self.lstAttr.DeleteAllItems()
        self.selAttrVal=-1
        self.lstAttrVal.DeleteAllItems()
        for k in keys:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dAttr[k])
            self.lstAttr.SetStringItem(index,1,'%d'%iCount,-1)
            index = self.lstAttrVal.InsertImageStringItem(sys.maxint, k, -1)
            iCount=len(self.dAttr[k])
            self.lstAttrVal.SetStringItem(index,1,'%d'%iCount,-1)
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
            self.dElem={}
            self.dAttr={}
            
            self.__procNode__(self.selNode)
            self.__procAttr__(self.selNode)
        
            self.__updateElem__()
            self.__updateAttr__()
            
            if self.attrs!='':
                strs=string.split(self.attrs,'|')
                i=0
                for ss in strs:
                    if i==0:
                        keys=self.dElem.keys()
                    elif i==1:
                        keys=self.dAttr.keys()
                    elif i==2:
                        keys=self.dAttr.keys()
                    else:
                        keys=[]
                    keys.sort()
                    substrs=string.split(ss,',')
                    for s in substrs:
                        try:
                            idx=keys.index(s)
                        except:
                            continue
                        if i==0:
                            wx.CallAfter(self.lstElements.SetItemState, idx,
                                wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
                        elif i==1:
                            wx.CallAfter(self.lstAttr.SetItemState, idx,
                                wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
                        elif i==2:
                            wx.CallAfter(self.lstAttrVal.SetItemState, idx,
                                wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
                    i+=1
                self.attrs=''
    def SetLanguage(self,lang):
        self.vgpTree.SetLanguage(lang)
    def SetRelNode(self,node):
        self.relNode=node
    def SelectByID(self,sID):
        self.bSelect=True
        self.ID2Select=sID
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
        self.vgpTree.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.dElem={}
        self.dAttr={}
        self.__updateElem__()
        self.__updateAttr__()
        
        if self.IsShown():
            self.vgpTree.SetNode(node,['templates'])
        else:
            self.node=node
        pass
    def GetSelNode(self):
        return self.selNode
    def SetDocElementInfo(self,name,sID,attrs):
        self.bSelect=True
        self.ID2Select=sID
        self.attrs=attrs
        self.txtName.SetValue(name)
    def __getSelList__(self,obj):
        lst=[]
        iCount=obj.GetItemCount()
        for i in range(0,iCount):
            if obj.GetItemState(i, wx.LIST_STATE_SELECTED):
                lst.append(obj.GetItemText(i))
        return string.join(lst,',')
    def GetDocElementInfo(self):
        if self.selNode is not None:
            sName=Hierarchy.getTagNames(self.doc,self.vgpTree.rootNode,self.relNode,self.selNode)
            sID=self.doc.getAttribute(self.selNode,'id')
            s1=self.__getSelList__(self.lstElements)
            s2=self.__getSelList__(self.lstAttr)
            s3=self.__getSelList__(self.lstAttrVal)
            s=s1+u'|'+s2+u'|'+s3
            if len(self.txtName.GetValue())>0:
                sName += '|' + self.txtName.GetValue()
            return sID, sName , s
        return ('','')
    def GetId(self):
        if self.selNode is not None:
            return self.selNode.getAttribute('id')
        return ''
    def __getElementByTagName__(self,tagname):
        for e in ELEMENTS.ELEMENTS:
            if e[0]==tagname:
                return e
        return None
    def __checkPossible__(self,node):
        if self.node2inst is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.node2inst.tagName
        sType=node.tagName
        elemNode=self.__getElementByTagName__(node.tagName)
        elemNode2Inst=self.__getElementByTagName__(sTypeNode2Inst)
        if elemNode is None:
            return -4
        if elemNode2Inst is None:
            return -3
        try:
            for t in elemNode[1]['__possible']:
                if t[:7]=='__type:':
                    strs=string.split(t,':')
                    if strs[1]==elemNode2Inst[1]['__type']:
                        return 1
                elif t==sTypeNode2Inst:
                    return 1
        except:
            return -5
        return 0
    def OnGcbbApplyButton(self, event):
        #ret=self.__checkPossible__(self.selNode)
        ret=1
        if ret==1:
            self.EndModal(1)
            event.Skip()
            return
        elif ret==0:
            # adding not possible
            sMsg=u'Instancing at this position not allowed.'
        elif ret==-1:
            # no node to instance
            sMsg=u'No node to instance!'
        elif ret==-2:
            # no node selected
            sMsg=u'No target node selected.'
        elif ret==-3:
            # element to instance not found
            sMsg=u'No element of node to instance found!'
        elif ret==-4:
            # element not found
            sMsg=u'No element of target node found!'
        elif ret==-5:
            # error oocurred
            sMsg=u'A serious error has occurred.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnVgdBrowseActionNameActivate(self, event):
        if not(self.vgpTree.IsNetResponse()):
            self.vgpTree.SetNetResponse(True)
            self.vgpTree.SetNode(None)#),['templates'])
            self.bSet=True
        if self.node is not None:
            #self.vgpTree.SetNode(self.node,['templates'])
            self.bSet=True
            self.node=None
        elif self.bUpdate==True:
            #self.vgpTree.SetNode(self.vgpTree.rootNode,['templates'])
            self.bSet=True
        self.bUpdate=False
        if self.vgpTree.IsThreadRunning()==False:
            if self.bSelect==True:
                if len(self.ID2Select)>0:
                    self.vgpTree.SelectByID(self.ID2Select)
                self.ID2Select=''
                self.bSelect=False
        self.vgpTree.SetFocus()
        event.Skip()

    def OnLstElementsListItemSelected(self, event):
        #self.__procSelItem__()
        event.Skip()

    def OnLstElementsListItemDeselected(self, event):
        #self.__procSelItem__()
        event.Skip()
    def __procSelItem__(self):
        iLen=self.lstElements.GetItemCount()
        self.dAttr={}
        self.gProcess.SetValue(0)
        self.gProcess.SetRange(iLen)
        for i in range(iLen):
            self.gProcess.SetValue(i)
            state=self.lstElements.GetItemState(i,wx.LIST_STATE_SELECTED)
            if state == wx.LIST_STATE_SELECTED:
                it=self.lstElements.GetItem(i)
                for n in self.dElem[it.m_text]:
                    self.__procAttr__(n[1])
        self.__updateAttr__()
        self.gProcess.SetValue(0)
    def OnLstAttrListItemDeselected(self, event):
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        event.Skip()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        wx.CallAfter(self.__select__)
        evt.Skip()
    def __select__(self):
        if self.bSelect==True:
            if len(self.ID2Select)>0:
                self.vgpTree.SelectByID(self.ID2Select)
            self.ID2Select=''
            self.bSelect=False
        self.gProcess.SetValue(0)
