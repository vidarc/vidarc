#----------------------------------------------------------------------------
# Name:         InputValue.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: InputValue.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked as masked
import string

class InputValue:

    def __init__(self, parent, pos, size,shown=True):
        w=30
        id=wx.NewId()
        self.mskTxtValue = masked.TextCtrl(id=id,
              name=u'mskTxtValue', parent=parent, pos=pos,
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=size, style=0, value=0)
        self.mskTxtValue.Bind(wx.EVT_SIZE, self.OnSize, id=id)
        
        self.mskNumValue = masked.NumCtrl(id=wx.NewId(),
              name=u'mskNumValue', parent=parent, pos=pos,
              autoSize=False,
              size=size, style=0, value=0)
        self.mskCmbValue = masked.ComboBox(id=wx.NewId(),choiceRequired=True,
              name=u'mskCmbValue', parent=parent, pos=pos,
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=wx.Size(w,size.GetHeight()), style=0, value=0)
        self.mskTimeValue = masked.TimeCtrl(id=wx.NewId(),
              name=u'mskTimeValue', parent=parent, pos=pos,
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=size, style=0, value=0)
        self.mskIPValue = masked.IpAddrCtrl(id=wx.NewId(),
              name=u'mskIPValue', parent=parent, pos=pos,
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=size, style=0, value=0)
        
        #self.lblUnit = wx.StaticText(id=wx.NewId(), label=u'',
        #      name=u'lblUnit', parent=parent, pos=wx.Point(pos.x+size.GetWidth()-w+3, pos.y+3),
        #      size=wx.Size(w-5,size.GetHeight()-5), style=0)
        #self.mskTxtValue.Show(shown)
        self.Enable(True,0)
        self.iSel=0
    def OnSize(self,evt):
        size=self.mskTxtValue.GetSize()
        pos=self.mskTxtValue.GetPosition()
        self.mskNumValue.Move(pos)
        self.mskCmbValue.Move(pos)
        self.mskTimeValue.Move(pos)
        self.mskIPValue.Move(pos)
        self.mskNumValue.SetSize(size)
        self.mskCmbValue.SetSize(size)
        self.mskTimeValue.SetSize(size)
        self.mskIPValue.SetSize(size)
        
        #self.mskTxtValue.Refresh()
    def Set(self,dict,dictType):
        if dictType is None:
            dictType=dict
            try:
                sType=dict['type']
            except:
                sType='text'
        else:
            try:
                sType=dictType['type']
            except:
                sType='text'
        try:
            sUnit=dictType['unit']
        except:
            sUnit=''
        if sType in ['string','text']:
            self.iSel=0
            try:
                iLen=int(dict['len'])
            except:
                iLen=100
            sMask='*{%d}'%iLen
            self.mskTxtValue.ClearValue()
            self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='_')
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('_')
            
            #self.mskTxtValue.Show(True)
            #self.mskTxtValue.SetDefaultValue(dict['val'])
            self.mskTxtValue.SetValue(dict['val'][:iLen])
            self.mskTxtValue.SetInsertionPoint(0)
        elif sType in ['int','long']:
            self.iSel=4
            #self.mskValue.Show(True)
            try:
                sMin=long(dictType['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=long(dictType['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            iIntW=12
            self.mskNumValue.SetIntegerWidth(iIntW)
            #sMask='#{%d}'%iIntW
            #self.mskNumValue.SetMask(sMask)
            iFriW=0
            if sType=='int':
                self.fmt='%'+'%d'%(iIntW)+'d'
            elif sType=='long':
                self.fmt='%'+'%d'%(iIntW)+'d'
            else:
                self.fmt='%d'                
            self.mskNumValue.SetFractionWidth(iFriW)
            try:
                self.mskNumValue.SetValue(long(dict['val']))
            except:
                self.mskNumValue.SetValue(0)
        elif sType in ['float','double','pseudofloat']:
            self.iSel=4
            #self.mskValue.Show(True)
            try:
                sMin=float(dictType['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=float(dictType['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            try:
                iIntW=int(dictType['digits'])
            except:
                iIntW=8
            self.mskNumValue.SetIntegerWidth(iIntW)
            try:
                iFriW=int(dictType['comma'])
            except:
                iFriW=2
            self.mskNumValue.SetFractionWidth(iFriW)
            #sMask='#{%d}.#{%d}'%(iIntW,iFriW)
            self.mskNumValue.ClearValue()
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('-_,._>')
            self.mskNumValue.SetDefaultValue(0.0)
            self.fmt='%'+'%d'%iIntW+'.'+'%d'%iFriW+'f'
            #self.numValue.SetFractionWidth(iFriW)
            try:
                self.mskNumValue.SetValue(float(dict['val']))
            except:
                #self.mskTxtValue.SetValue('0.0')
                pass
        elif sType=='choice':
            self.iSel=1
            keys=dictType.keys()
            keys.sort()
            lst=[]
            iLen=0
            for k in keys:
                if k[:2]=='it':
                    lst.append(dictType[k])
                    iL=len(dictType[k])
                    if iL>iLen:
                        iLen=iL
            sMask='*{%d}'%iLen
            self.mskCmbValue.ClearValue()
            self.mskCmbValue.SetCtrlParameters(choices=lst,mask=sMask,)
            #self.mskCmbValue.SetChoices(lst)
            #self.mskCmbValue.SetMask(sMask)
            try:
                #print dict['val']
                #print dict['it%02d'%int(dict['val'])]
                #self.mskCmbValue.SetValue(dict['it%02d'%int(dict['val'])])
                self.mskCmbValue.SetValue(dict['val'])
            except:
                try:
                    #print dictType['val']
                    #print dictType['it%02d'%int(dictType['val'])]
                    #self.mskCmbValue.SetValue(dictType['it%02d'%int(dictType['val'])])
                    self.mskCmbValue.SetValue(dictType['it%02d'%int(dictType['val'])])
                except:
                    self.mskCmbValue.SetValue(dictType['it00'])
        #self.lblUnit.SetLabel(sUnit)
        #self.lblUnit.Show(True)
        self.Enable(True,self.iSel)
    def Enable(self,state,num=0):
        #self.strValue.Show(state)
        #self.numValue.Show(False)
        if num==0:
            self.mskTxtValue.Show(state)
        else:
            self.mskTxtValue.Show(False)
        if num==1:
            self.mskCmbValue.Show(state)
        else:
            self.mskCmbValue.Show(False)
        if num==2:
            self.mskTimeValue.Show(state)
        else:
            self.mskTimeValue.Show(False)
        if num==3:
            self.mskIPValue.Show(state)
        else:
            self.mskIPValue.Show(False)
        if num==4:
            self.mskNumValue.Show(state)
        else:
            self.mskNumValue.Show(False)
        pass
    def Clear(self):
        self.iSel=0
        self.mskTxtValue.ClearValue()
        self.mskNumValue.ClearValue()
        self.mskCmbValue.ClearValue()
        self.mskTimeValue.ClearValue()
        self.mskIPValue.ClearValue()
        #self.lblUnit.SetLabel('')
    def SetConstraints(self,lc):
        self.mskTxtValue.SetConstraints(lc)
        #self.mskNumValue.SetConstraints(lc)
        #self.mskCmbValue.SetConstraints(lc)
        #self.mskTimeValue.SetConstraints(lc)
        #self.mskIPValue.SetConstraints(lc)
        #self.lblUnit.SetConstraints(lc2)
        pass
    def Get(self):
        if self.iSel==0:
            return string.strip(self.mskTxtValue.GetValue())
        elif self.iSel==1:
            #return str(self.mskCmbValue.GetSelection())
            return string.strip(self.mskCmbValue.GetValue())
        elif self.iSel==4:
            return string.strip(self.fmt%self.mskNumValue.GetValue())
        