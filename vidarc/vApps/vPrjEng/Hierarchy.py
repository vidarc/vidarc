#----------------------------------------------------------------------------
# Name:         Hierarchy.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: Hierarchy.py,v 1.2 2006/01/08 15:12:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.Elements import ELEMENTS

import string

dictHierSkip=None
dictRelBase=None
dictRoot=None

def __setup__():
    global dictHierSkip
    global dictRelBase
    global dictRoot
    if dictHierSkip is None:
        dictHierSkip={}
        dictRelBase={}
        dictRoot={}
        for e in ELEMENTS:
            if e[1].has_key('__properties'):
                for prop in e[1]['__properties']:
                    if prop=='isSkipHier':
                        dictHierSkip[e[0]]=True
                    elif prop=='isRelBase':
                        dictRelBase[e[0]]=True
                    elif prop=='isRoot':
                        dictRoot[e[0]]=True
    else:
        pass

def getRelBaseNode(doc,node):
    global dictRelBase
    __setup__()
    if node is None:
        return None
    try:
        if dictRelBase.has_key(doc.getTagName(node)):
            return node
        else:
            return getRelBaseNode(doc,doc.getParent(node))
    except:
        return None

def getRootNode(doc,node):
    global dictRoot
    __setup__()
    if node is None:
        return None
    try:
        if dictRoot.has_key(doc.getTagName(node)):
            return node
        else:
            return getRootNode(doc,doc.getParent(node))
    except:
        return None
        
def getTagNamesWithRel(doc,baseNode,relativeNode,n):
    global dictHierSkip
    __setup__()
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if doc.IsNodeKeyValid(n)==False:
        return '>'
    if not(doc.isSame(baseNode,n)):
        #print doc.isSame(n,relativeNode),doc.getKey(n),doc.getKey(relativeNode)
        if doc.isSame(n,relativeNode):
            #print '  rel node found'
            return doc.getNodeText(n,'tag')
        else:
            if not(doc.isSame(baseNode,doc.getParent(n))):
                try:
                    if dictHierSkip.has_key(doc.getTagName(n)):
                        s=''
                    else:
                        s='.'+doc.getNodeText(n,'tag')
                except:
                    s='.'+doc.getNodeText(n,'tag')
                return getTagNamesWithRel(doc,baseNode,relativeNode,doc.getParent(n))+s#n.tagName
            else:
                return doc.getNodeText(n,'tag')
    else:
        return ''
def getTagNames(doc,baseNode,relativeNode,n):
    global dictHierSkip
    __setup__()
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if doc.IsNodeKeyValid(n)==False:
        return '>'
    if not(doc.isSame(baseNode,n)):
        if doc.isSame(n,relativeNode):
            return '_'
        else:
            if not(doc.isSame(baseNode,doc.getParent(n))):
                try:
                    if dictHierSkip.has_key(doc.getTagName(n)):
                        s=''
                    else:
                        s='.'+doc.getNodeText(n,'tag')
                except:
                    s='.'+doc.getNodeText(n,'tag')
                return getTagNames(doc,baseNode,relativeNode,doc.getParent(n))+s#n.tagName
            else:
                return doc.getNodeText(n,'tag')
    else:
        return ''
def getTagNamesWithBase(doc,baseNode,relativeNode,n):
    global dictHierSkip
    __setup__()
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if doc.IsNodeKeyValid(n)==False:
        return '>'
    if not(doc.isSame(baseNode,n)):
        if doc.isSame(n,relativeNode):
            return '_'
        else:
            if not(doc.isSame(baseNode,n)):
                try:
                    s='.'+doc.getNodeText(n,'tag')
                except:
                    s='.'+doc.getNodeText(n,'tag')
                return getTagNamesWithBase(doc,baseNode,relativeNode,doc.getParent(n))+s#n.tagName
            else:
                return doc.getNodeText(n,'tag')
    else:
        return doc.getNodeText(n,'tag')
def applyCalcNode(doc,node):
    try:
        return applyCalc(doc,doc.getNodeText(node,'__fid'),
                doc.getNodeText(node,'attr'),
                doc.getNodeText(node,'formular'))
    except:
        return 'fault'
def applyCalcDict(doc,dict):
    try:
        return applyCalc(doc,dict['__fid'],dict['attr'],
                dict['formular'])
    except:
        return 'fault'
def applyCalc(doc,id,attr,sProc):
    try:
        if doc is None:
            return 'fault'
        nodeElem=doc.getNodeById(id)
        if nodeElem is None:
            return 'fault'
        node=doc.getChild(nodeElem,attr)
        if node is None:
            return 'fault'
        sVal=doc.getAttributeVal(node)
        
        #if attr in ['tag','name','descrition']:
        #    sVal=doc.getText(node)
        #elif len(doc.getAttribute(node,'ih'))>0:
        #    sVal=doc.getText(node)
        #elif len(doc.getAttribute(node,'ih'))>0:
        #    sVal=doc.getText(node)
        #else:
        #    sVal=doc.getNodeText(node,'val')
        strs=string.split(sProc,',')
        lRes=[unicode(sVal)]
        iAct=1
        for s in strs:
            i=0
            while i>=0:
                i=string.find(s,'$',i)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>=0:
                        tmp=s[i+1:j]
                        try:
                            iLen=len(tmp)
                            try:
                                idx=int(tmp)
                                tmp=unicode(lRes[idx])
                            except:
                                if tmp==u'relative':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,
                                            root,
                                            getRelBaseNode(doc,nodeElem),
                                            nodeElem)+'.'+attr
                                elif tmp==u'hierarchy_relative':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,root,None,
                                            getRelBaseNode(doc,nodeElem))
                                elif tmp==u'absolute':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNames(doc,
                                            root,
                                            None,nodeElem)+'.'+attr
                                elif tmp==u'absolute_base':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,
                                            root,
                                            None,nodeElem)+'.'+attr
                            s=s[:i]+tmp+s[j+1:]
                        except:
                            return 'fault'
            lRes.append(str(eval(s)))
        return str(lRes[-1])
    except:
        return 'fault'