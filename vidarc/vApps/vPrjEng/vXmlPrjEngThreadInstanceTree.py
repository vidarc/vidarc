#----------------------------------------------------------------------------
# Name:         vXmlPrjEngThreadInstanceTree.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlPrjEngThreadInstanceTree.py,v 1.2 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import thread
import vidarc.vApps.vPrjEng.vElementsPanel as Elements

import vidarc.tool.log.vtLog as vtLog

import traceback

VERBOSE=1

# defined event for vgpXmlTree item selected
wxEVT_THREAD_PROC_INSTANCE=wx.NewEventType()
def EVT_THREAD_PROC_INSTANCE(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_PROC_INSTANCE,func)
class wxThreadProcInstance(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_PROC_INSTANCE(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_PROC_INSTANCE)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_PROC_INSTANCE_FINISHED=wx.NewEventType()
def EVT_THREAD_PROC_INSTANCE_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_PROC_INSTANCE_FINISHED,func)
class wxThreadProcInstanceFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_PROC_INSTANCE_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_PROC_INSTANCE_FINISHED)

class thdProcInstance:
    def __init__(self,par):
        self.tree=par
        self.doc=None
        self.running = False
        self.verbose=VERBOSE
    def SetEventWidget(self,widget):
        self.tree=widget
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def OnInstance(self,node,child):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s child id:%s'%(self.doc.getKey(node),self.doc.getKey(child)))
        self.node=node
        self.child=child
        self.StartInstance()
    def StartInstance(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.RunInstance, ())
        
    def ProcInstanced(self,node,nodeUpdate,procChilds=True,procAttr=True,update=False):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s '%(self.doc.getKey(node)))
        self.node=node
        self.nodeUpdate=nodeUpdate
        self.bProcChilds=procChilds
        self.bProcAttr=procAttr
        self.bUpdate=update
        self.Start()
    def Start(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def __add2Tree__(self,tiPar,parNode,tmpChild):
        return
        if tiPar is None:
            tiPar=self.tree.__findNode__(None,parNode)
        
        if self.doc.__isSkip__(tmpChild)==False:
            self.doc.acquire()
            tagName,infos=self.doc.getNodeInfos(tmpChild)
            try:
                tn=self.tree.__addElementByInfos__(tiPar,tmpChild,tagName,infos,True)
                tiPar=self.tree.__findNode__(None,tmpChild)
            except:
                traceback.print_tb()
                tn=None
                #self.Stop()
                self.doc.release()
                return -1
            self.doc.release()
            if tn is not None:
                for c in self.doc.getChildsAttr(tmpChild,self.doc.attr):
                    self.__add2Tree__(tn,tmpChild,c)
        
    def __procInstancedChilds__(self,c):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s'%(self.doc.getKey(c)))
        if 1==0:
            print '__procInstancedChilds__'
        # process instanced nodes 
        instNode=self.doc.getNodeById(self.doc.getAttribute(c,'iid'))
        if instNode is None:
            return
                
        ichilds=self.doc.getChilds(instNode)
        dInstChilds={}
        for it in ichilds:
            tagName=self.doc.getTagName(it)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            if tagName in ['tag','name','description']:
                continue
            id=self.doc.getAttribute(it,'iid')
            if self.doc.IsKeyValid(id)==False:
                continue
            dInstChilds[id]=it
                
        lTmplChilds=self.dTmplChilds.keys()
        lTmplChilds.sort()
        lInstChilds=dInstChilds.keys()
        lInstChilds.sort()
        if 1==0:
            print '---------------------'
            print dTmplChilds
            print '+++++++++++++++++++++'
            print dInstChilds
            print '====================='
        bAlign=False
        # find new child nodes
        vtLog.vtLogCallDepth(None,'find new child',verbose=1,callstack=False)
        for k in lTmplChilds:
            try:
                it=dInstChilds[k]
            except:
                # got new
                child=self.dTmplChilds[k]
                clonechild=self.doc.doClone(child)
                self.doc.doInstanceing(instNode,clonechild)
                self.doc.doStoreClone(child,clonechild)
                #if self.doc.updateAllStore(child):
                #    self.doc.setNodeFull(child)
                self.doc.setNode(child)
                self.doc.addNodeCorrectId(instNode,clonechild)
                
                #self.tree.__doInstanceing__(instNode,clonechild,ensureVis=False)
                self.__add2Tree__(None,instNode,clonechild)
                bAlign=True
        # find del attributes
        vtLog.vtLogCallDepth(None,'find del attr',verbose=1,callstack=False)
        for k in lInstChilds:
            try:
                val=self.dTmplChilds[k]
            except:
                # got del
                it=self.doc.getNodeById(k)
                self.doc.deleteNode(it,instNode)
                bAlign=True
        # check childs
        # do not process recursively WRO 050625
        #for k in lTmplChilds:
        #    self.__procInstanced__(dTmplChilds[k],doc)
        if bAlign:
            self.doc.AlignNode(instNode,iRec=0)
        cti=self.tree.__findNode__(self.tree.tiRoot,instNode)
        self.tree.__refreshLabel__(cti)
    def __procInstancedAttr__(self,c):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s'%(self.doc.getKey(c)))
        if 1==0:
            print '__procInstancedAttr__'
        # process instanced nodes 
        instNode=self.doc.getNodeById(self.doc.getAttribute(c,'iid'))
        if instNode is None:
            print c
            print 'xit'
            return
                
        ichilds=self.doc.getChilds(instNode)
        dInstChilds={}
        for it in ichilds:
            tagName=self.doc.getTagName(it)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            if tagName in ['tag','name','description']:
                continue
            dInstChilds[tagName]=it
                
        lTmplChilds=self.dTmplChilds.keys()
        lTmplChilds.sort()
        lInstChilds=dInstChilds.keys()
        lInstChilds.sort()
        if 1==0:
            print '---------------------'
            print dTmplChilds
            print '+++++++++++++++++++++'
            print dInstChilds
            print '====================='
        bAlign=False
        # find new attributes
        vtLog.vtLogCallDepth(None,'find new attr',verbose=1,callstack=False)
        for k in lTmplChilds:
            try:
                it=dInstChilds[k]
                fidC=self.doc.getAttribute(it,'fid')
                fid=self.doc.getAttribute(self.dTmplChilds[k][1],'fid')
                if len(fidC)>0:
                    # update predefined type link
                    if fidC!=fid:
                        #update needed
                        if len(fid)>0:
                            self.doc.setAttribute(it,'fid',fid)
                            self.doc.setText(it,self.dTmplChilds[k][0])
                        else:
                            self.doc.removeAttribute(it,'fid')
                            self.doc.setAttribute(it,'ih','1')
                            self.doc.setText(it,self.dTmplChilds[k][0])
                else:
                    if len(fid)>0:
                        # update needed
                        self.doc.removeAttribute(it,'ih')
                        self.doc.setAttribute(it,'fid',fid)
                        self.doc.setText(it,self.dTmplChilds[k][0])
            except:
                # got new
                a=self.doc.createSubNodeText(instNode,k,self.dTmplChilds[k][0])
                fid=self.doc.getAttribute(self.dTmplChilds[k][1],'fid')
                if len(fid)>0:
                    self.doc.setAttribute(a,'fid',fid)
                else:
                    self.doc.setAttribute(a,'ih','1')
                if 1==0:
                    parTmpl=self.doc.getParent(instNode)
                    parTmpl=self.doc.getParent(parTmpl)
                    s='new instance attr %s %s'%(self.doc.getNodeText(parTmpl,'tag'),
                            self.doc.getAttribute(parTmpl,'id'))
                            
                #instNode.appendChild(a)
                bAlign=True
        # find del attributes
        vtLog.vtLogCallDepth(None,'find del attr',verbose=1,callstack=False)
        for k in lInstChilds:
            try:
                val=self.dTmplChilds[k][0]
            except:
                # got del
                it=dInstChilds[k]#self.ids.GetIdStr(k)[1]
                self.doc.deleteNode(it,instNode)
                bAlign=True
        if bAlign:
            self.doc.AlignNode(instNode)
            from boa.vApps.vPrjEng.vgpAttr import CACHE_INH as CACHE_INH
            try:
                del CACHE_INH[self.node.getAttribute('id')]
            except:
                pass
        vtLog.vtLogCallDepth(None,'finished',verbose=1,callstack=False)
        cti=self.__findNode__(self.tiRoot,instNode)
        self.__refreshLabel__(cti)
        self.SortChildren(cti)
    def __equalizeInstanced__(self,node,instNode):
        if instNode is None:
            return
        if node is None:
            return
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s inst id:%s'%(self.doc.getKey(node),self.doc.getKey(instNode)))
        childs=self.doc.getChilds(node)
        instChilds=self.doc.getChilds(instNode)
        def __getAttrDict__(childs):
            d={}
            for c in childs:
                if len(self.doc.getAttribute(c,'id'))>0:
                    continue
                tagName=self.doc.getTagName(c)
                if tagName[0:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                    continue
                d[tagName]=c
            return d
        dTmpl=__getAttrDict__(childs)
        dInst=__getAttrDict__(instChilds)
        for k in dTmpl.keys():
            try:
                aInst=dInst[k]
                aNode=dTmpl[k]
                valNode=self.doc.getChild(aNode,'val')
                if valNode is None:
                    sVal=self.doc.getText(aNode)
                    self.doc.setText(aInst,sVal)
                    if self.verbose>0:
                        vtLog.vtLogCallDepth(self,'key:%s val:%s'%(k,sVal),
                                    callstack=False)
                else:
                    sVal=self.doc.getText(valNode)
                    self.doc.setText(aInst,sVal)
                    if self.verbose>0:
                        vtLog.vtLogCallDepth(self,'key:%s val:%s'%(k,sVal),
                                    callstack=False)
                if self.verbose>0:
                    vtLog.vtLogCallDepth(self,'   val:%s'%(self.doc.getText(aInst)),
                                callstack=False)
            except:
                pass
        #self.doc.updateNode(instNode)
        vtLog.vtLogCallDepth(self,'',callstack=False)
        self.doc.setNode(instNode)
        vtLog.vtLogCallDepth(self,'',callstack=False)
        ti=self.tree.__findNode__(self.tree.tiRoot,instNode)
        self.tree.__refreshLabel__(ti)
        vtLog.vtLogCallDepth(self,'',callstack=False)
        tiPar=self.tree.GetItemParent(ti)
        if self.tree.IsExpanded(tiPar):
            self.tree.SortChildren(tiPar)
    def __instancedUpdate__(self,node,bStart=False):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s'%(self.doc.getKey(node)))
        if 1==0:
            print 'instanced update'
        
        childs=self.doc.getChilds(node,'__node')
        if self.verbose>0:
            vtLog.vtLog(self,'len:%d'%(len(childs)),level=1)
        for c in childs:
            self.iAct+=1
            if self.verbose>0:
                vtLog.vtLog(self,'node:%s'%(c),level=1)
            if self.tree is not None:
                wx.PostEvent(self.tree,wxThreadProcInstance(self.iAct,self.iCount))
            if self.verbose>0:
                vtLog.vtLog(self,'node:%s'%(c),level=1)
            #if bStart==True:
            #    if self.gProcess is not None:
            #        self.gProcess.SetValue(iAct)
            iid=self.doc.getAttribute(c,'iid')
            if self.verbose>0:
                vtLog.vtLog(self,'id:%s iid:%s'%(self.doc.getKey(node),iid))
            if len(iid)<=0:
                continue
            instNode=self.doc.getNodeById(iid)
            self.__equalizeInstanced__(node,instNode)
        for c in self.doc.getChildsAttr(node,'id'):
            self.__instancedUpdate__(c)
        
    def Run(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s'%(self.doc.getKey(self.node)))
            vtLog.vtLogCallDepth(self,self.node)
        childs=self.doc.getChilds(self.node,'__node')
        iLen=len(childs)
        self.iCount=0
        self.iAct=0
        if self.bUpdate:
            if self.nodeUpdate is not None:
                self.iCount+=len(self.doc.getChildsAttr(self.nodeUpdate,'id'))
            else:
                self.bUpdate=False
        if iLen>0:
            if self.bProcChilds:
                self.iCount+=iLen
            if self.bProcAttr:
                self.iCount+=iLen
            self.__checkStoreClone__(self.node)
            
            tchilds=self.doc.getChilds(self.node)
            # process childs
            self.dTmplChilds={}
            for it in tchilds:
                tagName=self.doc.getTagName(it)
                if tagName[:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                #if tagName in ['Safe','Log']:
                    continue
                id=self.doc.getKey(it)
                if self.doc.IsKeyValid(id)==False:
                    continue
                self.dTmplChilds[id]=it
            if self.bProcChilds:
                for c in childs:
                    self.iAct+=1
                    if self.tree is not None:
                        wx.PostEvent(self.tree,wxThreadProcInstance(self.iAct,self.iCount))
                    self.__procInstancedChilds__(c)
            
            # process attributes
            tchilds=self.doc.getChilds(self.node)
            self.dTmplChilds={}
            for it in tchilds:
                tagName=self.doc.getTagName(it)
                if tagName[:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                #if tagName in ['Safe','Log']:
                    continue
                if tagName in ['tag','name','description']:
                    continue
                fid=self.doc.getAttribute(it,'fid')
                if len(fid)>0:
                    sVal=self.doc.getText(it)
                else:
                    sVal=self.doc.getNodeText(it,'val')
                self.dTmplChilds[tagName]=(sVal,it)
            if self.bProcAttr:
                for c in childs:
                    self.iAct+=1
                    if self.tree is not None:
                        wx.PostEvent(self.tree,wxThreadProcInstance(self.iAct,self.iCount))
                    try:
                        self.__procInstancedAttr__(c)
                    except:
                        traceback.print_exc()
            
        if self.bUpdate:
            self.__instancedUpdate__(self.nodeUpdate,True)
            #self.__instancedUpdate__(self.tree.nodeDupTmpl,True)
        self.keepGoing = self.running = False
        if self.tree is not None:
            wx.PostEvent(self.tree,wxThreadProcInstanceFinished())
    def RunInstance(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'id:%s'%(self.doc.getKey(self.node)))
        childs=self.doc.getChilds(self.node,'__node')
        iLen=len(childs)
        self.iCount=iLen
        self.iAct=0
        for c in childs:
            self.iAct+=1
            if self.tree is not None:
                wx.PostEvent(self.tree,wxThreadProcInstance(self.iAct,self.iCount))
            
            instNode=self.doc.getNodeById(self.doc.getAttribute(c,'iid'))
            clonechild=self.doc.doClone(self.child)
            self.doc.doInstanceing(instNode,clonechild)
            self.doc.doStoreClone(self.child,clonechild)
            self.doc.setNode(self.child)
            self.doc.addNodeCorrectId(instNode,clonechild)
            self.__add2Tree__(None,instNode,clonechild)
            
        self.keepGoing = self.running = False
        if self.tree is not None:
            wx.PostEvent(self.tree,wxThreadProcInstanceFinished())
