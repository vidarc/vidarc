#Boa:Dialog:vMcaCodeLetterDialog
#----------------------------------------------------------------------------
# Name:         vMcaCodeLetterDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vMCA_CodeLetterDialog.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from vidarc.vApps.vPrjEng.vElementsPanel import MCA_Code_Letter
import images
import sys,string

def create(parent):
    return vMcaCodeLetterDialog(parent)

[wxID_VMCACODELETTERDIALOG, wxID_VMCACODELETTERDIALOGGCBBADD, 
 wxID_VMCACODELETTERDIALOGGCBBAPPLY, wxID_VMCACODELETTERDIALOGGCBBCANCEL, 
 wxID_VMCACODELETTERDIALOGLBLMCA, wxID_VMCACODELETTERDIALOGLSTMCACODE, 
 wxID_VMCACODELETTERDIALOGTXTMCA, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vMcaCodeLetterDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMCACODELETTERDIALOG,
              name=u'vMcaCodeLetterDialog', parent=prnt, pos=wx.Point(44, 44),
              size=wx.Size(400, 287), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'MCA Code Letter')
        self.SetClientSize(wx.Size(392, 260))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMCACODELETTERDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(112, 224), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VMCACODELETTERDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMCACODELETTERDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(200, 224),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VMCACODELETTERDIALOGGCBBCANCEL)

        self.lstMcaCode = wx.ListView(id=wxID_VMCACODELETTERDIALOGLSTMCACODE,
              name=u'lstMcaCode', parent=self, pos=wx.Point(8, 48),
              size=wx.Size(376, 168), style=wx.LC_REPORT)

        self.txtMCA = wx.TextCtrl(id=wxID_VMCACODELETTERDIALOGTXTMCA,
              name=u'txtMCA', parent=self, pos=wx.Point(40, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblMCA = wx.StaticText(id=wxID_VMCACODELETTERDIALOGLBLMCA,
              label=u'MCA', name=u'lblMCA', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(23, 13), style=0)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMCACODELETTERDIALOGGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(160, 8), size=wx.Size(76, 30), style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VMCACODELETTERDIALOGGCBBADD)

    def __init__(self, parent):
        self._init_ctrls(parent)
    
        self.lstMcaCode.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,30)
        self.lstMcaCode.InsertColumn(1,u'1st letter',wx.LIST_FORMAT_LEFT,150)
        self.lstMcaCode.InsertColumn(2,u'following letter',wx.LIST_FORMAT_LEFT,150)

        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        self.__setMcaCode__()
    def __setMcaCode__(self):
        self.lstMcaCode.DeleteAllItems()
        keys=MCA_Code_Letter.keys()
        keys.sort()
        for k in keys:
            if k=='_order':
                continue
            index = self.lstMcaCode.InsertImageStringItem(sys.maxint, k, -1)
            self.lstMcaCode.SetStringItem(index,1,MCA_Code_Letter[k][0],-1)
            self.lstMcaCode.SetStringItem(index,2,MCA_Code_Letter[k][1],-1)
        
    def Get(self):
        return self.txtMCA.GetValue()
        pass

    def OnGcbbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnGcbbAddButton(self, event):
        idx=self.lstMcaCode.GetFirstSelected()
        if idx<0:
            return
        l=self.lstMcaCode.GetItemText(idx)
        str=self.txtMCA.GetValue()
        if len(str)==0:
            i=0
        else:
            i=1
        if len(MCA_Code_Letter[l][i])==0:
            return
        str+=l
        nstr=str[0]
        str=str[1:]
        for s in MCA_Code_Letter['_order']:
            i=string.find(str,s)
            if i>=0:
                nstr+=s
                str=str[:i]+str[i+1:]
        nstr+=str
        self.txtMCA.SetValue(nstr)
        event.Skip()
