#Boa:FramePanel:vActivitiesPanel
#----------------------------------------------------------------------------
# Name:         vActivitiesPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vActivitiesPanel.py,v 1.2 2006/08/29 10:06:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors

from vidarc.tool.time.vTimeDateLocal import *
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.evtDataChanged import *
import sys,string
import images

MAP_PRIOR_SORT={u'highest':'1',u'higher':'2',u'normal':'3',u'lower':'4',u'lowest':'5'}
MAP_SEVERITY_SORT={u'blocker':'1', u'critical':'2', u'major':'3',
              u'normal':'4', u'minor':'5', u'trivial':'6', u'enhancement':'7'}
MAP_STATUS_SORT={u'new':'1', u'accepted':'2',u'resolved':'3', u'deleted':'4',
              u'wrong':'5'}
[wxID_VACTIVITIESPANEL, wxID_VACTIVITIESPANELCBADDASSIGN, 
 wxID_VACTIVITIESPANELCBASSIGNDEL, wxID_VACTIVITIESPANELCHCASSIGNGRP, 
 wxID_VACTIVITIESPANELCHCASSIGNUSR, wxID_VACTIVITIESPANELCHCPRIOR, 
 wxID_VACTIVITIESPANELCHCSEVERITY, wxID_VACTIVITIESPANELCHCSTATUS, 
 wxID_VACTIVITIESPANELCHCUSR, wxID_VACTIVITIESPANELGCBBADD, 
 wxID_VACTIVITIESPANELGCBBSET, wxID_VACTIVITIESPANELLSTACT, 
 wxID_VACTIVITIESPANELLSTASSIGN, wxID_VACTIVITIESPANELTXTACTDESC, 
 wxID_VACTIVITIESPANELTXTACTNAME, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vActivitiesPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VACTIVITIESPANEL,
              name=u'vActivitiesPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(480, 300), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(472, 273))
        self.SetAutoLayout(True)

        self.lstAct = wx.ListView(id=wxID_VACTIVITIESPANELLSTACT,
              name=u'lstAct', parent=self, pos=wx.Point(8, 8), size=wx.Size(376,
              80), style=wx.LC_REPORT)
        self.lstAct.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstActListItemSelected, id=wxID_VACTIVITIESPANELLSTACT)
        self.lstAct.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstActListColClick,
              id=wxID_VACTIVITIESPANELLSTACT)
        self.lstAct.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstActListItemDeselected, id=wxID_VACTIVITIESPANELLSTACT)

        self.txtActName = wx.TextCtrl(id=wxID_VACTIVITIESPANELTXTACTNAME,
              name=u'txtActName', parent=self, pos=wx.Point(8, 104),
              size=wx.Size(176, 21), style=0, value=u'')

        self.chcUsr = wx.Choice(choices=[], id=wxID_VACTIVITIESPANELCHCUSR,
              name=u'chcUsr', parent=self, pos=wx.Point(192, 104),
              size=wx.Size(88, 21), style=0)

        self.txtActDesc = wx.TextCtrl(id=wxID_VACTIVITIESPANELTXTACTDESC,
              name=u'txtActDesc', parent=self, pos=wx.Point(8, 200),
              size=wx.Size(270, 62), style=wx.TE_MULTILINE, value=u'')

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VACTIVITIESPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(392, 16), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VACTIVITIESPANELGCBBADD)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VACTIVITIESPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(392, 56), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VACTIVITIESPANELGCBBSET)

        self.chcAssignGrp = wx.Choice(choices=[],
              id=wxID_VACTIVITIESPANELCHCASSIGNGRP, name=u'chcAssignGrp',
              parent=self, pos=wx.Point(288, 104), size=wx.Size(96, 21),
              style=0)

        self.chcAssignUsr = wx.Choice(choices=[],
              id=wxID_VACTIVITIESPANELCHCASSIGNUSR, name=u'chcAssignUsr',
              parent=self, pos=wx.Point(288, 138), size=wx.Size(96, 21),
              style=0)

        self.lstAssign = wx.ListCtrl(id=wxID_VACTIVITIESPANELLSTASSIGN,
              name=u'lstAssign', parent=self, pos=wx.Point(288, 168),
              size=wx.Size(176, 100), style=wx.LC_REPORT)
        self.lstAssign.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAssignListItemSelected,
              id=wxID_VACTIVITIESPANELLSTASSIGN)
        self.lstAssign.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAssignListItemDeselected,
              id=wxID_VACTIVITIESPANELLSTASSIGN)

        self.cbAddAssign = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VACTIVITIESPANELCBADDASSIGN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddAssign',
              parent=self, pos=wx.Point(392, 100), size=wx.Size(76, 30),
              style=0)
        self.cbAddAssign.Bind(wx.EVT_BUTTON, self.OnCbAddAssignButton,
              id=wxID_VACTIVITIESPANELCBADDASSIGN)

        self.cbAssignDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VACTIVITIESPANELCBASSIGNDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbAssignDel',
              parent=self, pos=wx.Point(392, 134), size=wx.Size(76, 30),
              style=0)
        self.cbAssignDel.Bind(wx.EVT_BUTTON, self.OnCbAssignDelButton,
              id=wxID_VACTIVITIESPANELCBASSIGNDEL)

        self.chcSeverity = wx.Choice(choices=[u'blocker', u'critical', u'major',
              u'normal', u'minor', u'trivial', u'enhancement'],
              id=wxID_VACTIVITIESPANELCHCSEVERITY, name=u'chcSeverity',
              parent=self, pos=wx.Point(8, 168), size=wx.Size(130, 21),
              style=0)
        self.chcSeverity.SetSelection(3)
        self.chcSeverity.SetLabel(u'')

        self.chcPrior = wx.Choice(choices=[u'highest', u'higher', u'normal',
              u'lower', u'lowest'], id=wxID_VACTIVITIESPANELCHCPRIOR,
              name=u'chcPrior', parent=self, pos=wx.Point(144, 168),
              size=wx.Size(130, 21), style=0)
        self.chcPrior.SetSelection(2)
        self.chcPrior.SetLabel(u'')

        self.chcStatus = wx.Choice(choices=[u'new', u'accepted', u'resolved',
              u'deleted', u'wrong'], id=wxID_VACTIVITIESPANELCHCSTATUS,
              name=u'chcStatus', parent=self, pos=wx.Point(8, 134),
              size=wx.Size(130, 21), style=0)
        self.chcStatus.SetSelection(0)
        self.chcStatus.SetLabel(u'')

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.Move(pos)
        self.SetSize(size)
        lc = wx.LayoutConstraints()
        #lc.centreX.SameAs   (self.panelA, wx.CentreX)
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs     (self, wx.Left, 2)
        lc.bottom.SameAs      (self, wx.Bottom, 175)
        lc.right.SameAs  (self, wx.Right, 90)
        self.lstAct.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self.lstLog, wx.Top, 68)
        lc.left.SameAs      (self.txtActDesc, wx.Right, 8)
        lc.bottom.SameAs (self.txtActDesc, wx.Bottom, 0)
        lc.right.SameAs  (self, wx.Right, 2)
        self.lstAssign.SetConstraints(lc)
            
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 5)
        lc.left.SameAs      (self.lstAct, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbAdd.SetConstraints(lc)
            
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbAdd, wx.Bottom, 5)
        lc.left.SameAs      (self.lstAct, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbSet.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lstAct, wx.Bottom, 5)
        lc.left.SameAs      (self.lstAct, wx.Left, 0)
        lc.height.AsIs      ()
        lc.width.PercentOf  (self, wx.Width, 40)
        self.txtActName.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lstAct, wx.Bottom, 5)
        lc.left.SameAs      (self.txtActName, wx.Right, 8)
        lc.height.AsIs      ()
        lc.width.PercentOf  (self, wx.Width, 20)
        self.chcUsr.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lstAct, wx.Bottom, 5)
        lc.left.SameAs      (self.chcUsr, wx.Right, 8)
        lc.height.AsIs      ()
        lc.right.SameAs     (self, wx.Right, 90)
        self.chcAssignGrp.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcAssignGrp, wx.Top, -10)
        lc.left.SameAs      (self.chcAssignGrp, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbAddAssign.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcAssignGrp, wx.Top, 24)
        lc.left.SameAs      (self.chcAssignGrp, wx.Left, 0)
        lc.height.AsIs      ()
        lc.right.SameAs     (self.chcAssignGrp, wx.Right, 0)
        self.chcAssignUsr.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcAssignUsr, wx.Top, 0)
        lc.left.SameAs      (self.chcAssignUsr, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbAssignDel.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.txtActName, wx.Top, 24)
        lc.left.SameAs      (self.txtActName, wx.Left, 0)
        lc.height.AsIs      ()
        lc.width.PercentOf  (self, wx.Width, 17)
        self.chcStatus.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        #lc.centreX.SameAs   (self.panelA, wx.CentreX)
        lc.top.SameAs      (self.chcStatus, wx.Top, 48)
        lc.left.SameAs     (self, wx.Left, 2)
        lc.bottom.SameAs   (self, wx.Bottom, 2)
        lc.right.SameAs    (self.chcUsr, wx.Right, 0)
        self.txtActDesc.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcStatus, wx.Top, 24)
        lc.left.SameAs      (self.chcStatus, wx.Left, 0)
        lc.height.AsIs      ()
        lc.width.PercentOf  (self.txtActDesc, wx.Width, 48)
        self.chcSeverity.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcSeverity, wx.Top, 0)
        lc.left.SameAs      (self.chcSeverity, wx.Right, 8)
        lc.height.AsIs      ()
        lc.right.SameAs     (self.txtActDesc, wx.Right, 0)
        self.chcPrior.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.cbAssignDel, wx.Bottom, 4)
        lc.left.SameAs      (self.txtActDesc, wx.Right, 8)
        lc.bottom.SameAs    (self,wx.Bottom,2)
        lc.right.SameAs     (self, wx.Right, 4)
        self.lstAssign.SetConstraints(lc)
        
        self.zStart=vTimeDateLocal(parent=self,pos=wx.Point(142,134),size=wxSize(70,24))
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcStatus, wx.Top, 0)
        lc.left.SameAs      (self.chcStatus, wx.Right, 8)
        lc.height.AsIs    ()
        lc.width.PercentOf  (self, wx.Width, 20)
        self.zStart.SetConstraints(lc)
        
        self.zEnd=vTimeDateLocal(parent=self,pos=wx.Point(212,134),size=wxSize(70,24))
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcStatus, wx.Top, 0)
        lc.left.SameAs      (self.zStart, wx.Right, 8)
        lc.height.AsIs    ()
        lc.width.PercentOf  (self, wx.Width, 20)
        self.zEnd.SetConstraints(lc)
        
        if 1==0:    
            lc = wx.LayoutConstraints()
            lc.top.SameAs       (self.gcbbSet, wx.Bottom, 8)
            lc.left.SameAs      (self.txtLogDesc, wx.Left, 2)
            #lc.right.SameAs     (self, wx.Right, 2)
            lc.width.PercentOf  (self, wx.Width, 35)
            lc.height.AsIs      ()
            self.txtLogName.SetConstraints(lc);
            
            lc = wx.LayoutConstraints()
            #lc.top.SameAs       (self.txtLogName, wx.Bottom, 2)
            lc.top.SameAs       (self.txtLogName, wx.Top, 0)
            #lc.left.SameAs      (self.txtLogDesc, wx.Left, 2)
            lc.left.SameAs      (self.txtLogName, wx.Right, 2)
            lc.right.SameAs     (self, wx.Right,2)
            lc.height.AsIs      ()
            #lc.width.AsIs       ()
            self.chcUsr.SetConstraints(lc);
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        self.cbAddAssign.SetBitmapLabel(img)
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.cbAssignDel.SetBitmapLabel(img)
        
        self.lstSort=[]
        self.bSortAsc=False
        self.iSortCol=-1
        self.selIdx=-1
        self.selAssign=-1
        self.humDoc=None
        self.grps=None
        self.colOrder=['status','nr','prev','name','assign','prior','severity',
                'date','start','end','usr']
        self.lstAct.InsertColumn(0,u'Status',wx.LIST_FORMAT_LEFT,40)
        self.lstAct.InsertColumn(1,u'Nr',wx.LIST_FORMAT_LEFT,40)
        self.lstAct.InsertColumn(2,u'Prev',wx.LIST_FORMAT_LEFT,40)
        self.lstAct.InsertColumn(3,u'Name',wx.LIST_FORMAT_LEFT,130)
        self.lstAct.InsertColumn(4,u'Assign',wx.LIST_FORMAT_LEFT,70)
        self.lstAct.InsertColumn(5,u'Prior',wx.LIST_FORMAT_LEFT,40)
        self.lstAct.InsertColumn(6,u'Severity',wx.LIST_FORMAT_LEFT,40)
        self.lstAct.InsertColumn(7,u'Date',wx.LIST_FORMAT_LEFT,120)
        self.lstAct.InsertColumn(8,u'Start',wx.LIST_FORMAT_LEFT,120)
        self.lstAct.InsertColumn(9,u'End',wx.LIST_FORMAT_LEFT,120)
        self.lstAct.InsertColumn(10,u'User',wx.LIST_FORMAT_LEFT,70)
        
        self.lstAssign.InsertColumn(0,u'',wx.LIST_FORMAT_LEFT,40)
        self.lstAssign.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,40)
        
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getGroupBitmap()
        self.imgLstTyp.Add(img)
        img=images.getUserBitmap()
        self.imgLstTyp.Add(img)
        self.lstAssign.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.bModified=False
    def SetAutoApply(self,flag):
        pass
    def __setModified__(self,state):
        self.bModified=state
    def Clear(self):
        self.__setModified__(False)
        self.lstAct.DeleteAllItems()
        self.__clear__()
    def __clear__(self):
        self.txtActName.SetValue('')
        self.txtActDesc.SetValue('')
        self.chcSeverity.SetSelection(3)
        self.chcPrior.SetSelection(2)
        self.chcStatus.SetSelection(0)
        self.chcUsr.SetSelection(0)
        self.chcAssignGrp.SetSelection(0)
        self.chcAssignUsr.SetSelection(0)
        self.lstAssign.DeleteAllItems()
        self.zStart.SetValue('')
        self.zEnd.SetValue('')
    #def SetHumans(self,humans,humDoc):
    #    self.humDoc=humDoc
    #    self.chcUsr.Clear()
    #    self.chcUsr.Append('author')
    #    self.humans=humans
    #    for i in self.humans.GetKeys():
    #        self.chcUsr.Append(i)
    #    self.chcUsr.SetSelection(0)
        
    #    self.chcAssignGrp.Clear()
    #    self.chcAssignGrp.Append('assign group')
    #    if self.grps is not None:
    #        del self.grps
    #        self.grps=None
    #    if self.humDoc is not None:
    #        self.grps=self.humDoc.getSortedNodeIds(['groups'],
    #                    'group','id',[('name','%s')])
    #        for i in self.grps.GetKeys():
    #            self.chcAssignGrp.Append(i)
    #    self.chcAssignGrp.SetSelection(0)
    #    self.__updateAssignGrp__()
    def SetNetDocHuman(self,netDoc):
        self.netDocHuman=netDoc
        EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnHumanChanged)
        self.OnHumanChanged(None)
    def OnHumanChanged(self,evt):
        self.chcUsr.Clear()
        hums=self.netDocHuman.GetSortedUsrIds()
        if hums is None:
            return
        self.chcUsr.Append('author')
        for i in hums.GetKeys():
            self.chcUsr.Append(i)
        self.chcUsr.SetSelection(0)
        
        self.chcAssignGrp.Clear()
        self.chcAssignGrp.Append('assign group')
        grps=self.netDocHuman.GetSortedGrpIds()
        for i in grps.GetKeys():
            self.chcAssignGrp.Append(i)
        self.chcAssignGrp.SetSelection(0)
        
        self.chcAssignUsr.Clear()
        self.chcAssignUsr.Append('assign user')
        for i in hums.GetKeys():
            self.chcAssignUsr.Append(i)
        self.chcAssignUsr.SetSelection(0)
        if evt is not None:
            evt.Skip()
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,name,node):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode(self.node)
        self.__setModified__(False)
        self.node=node
        self.name=name
        self.txtActName.SetValue('')
        self.txtActDesc.SetValue('')
        childs=self.doc.getChilds(self.node,self.name)
        self.lstAct.DeleteAllItems()
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
            pass
        self.__showInfo__()
        pass
    def GetNode(self):
        pass
    def Apply(self,bDoApply=False):
        
        self.__setModified__(False)
        return False
    def Cancel(self):
        self.__setModified__(False)
    def OnGcbbAddButton(self, event):
        if self.chcUsr.GetSelection()<1:
            return
        dt=wx.DateTime.Now()
        d={}
        if self.__guiData2NodeDict__(None,d)<0:
            return
        hums=self.netDocHuman.GetSortedUsrIds()
        usrID=hums.GetIdByName(d['usr'])[0]
        lst=[{'tag':'date','val':dt.FormatISODate()},
             {'tag':'time','val':dt.FormatISOTime()},
             {'tag':'status','val':d['status']},
             {'tag':'nr','val':d['nr']},
             {'tag':'name','val':d['name']},
             {'tag':'desc','val':d['desc']},
             {'tag':'start','val':d['start']},
             {'tag':'end','val':d['end']},
             {'tag':'assign_grp','val':d['assign_grp']},
             {'tag':'assign_usr','val':d['assign_usr']},
             {'tag':'severity','val':d['severity']},
             {'tag':'prior','val':d['prior']},
             {'tag':'usr','val':d['usr'],'attr':('fid',usrID)}]
        
        n=self.doc.createChildByLst(self.node,self.name,lst)
        #FIXME ids shall be removed
        self.doc.checkId(n)
        self.doc.processMissingId()
        self.doc.clearMissingId()
        self.doc.AlignNode(n)
        self.doc.addNode(self.node,n)
        self.__addInfo__(n)
        self.__showInfo__()
        wx.PostEvent(self,vgpDataChanged(self,self.node))
        event.Skip()
    def __addInfo__(self,node):
        childs=self.doc.getChilds(node)
        bSkip=False
        hums=self.netDocHuman.GetSortedUsrIds()
        d={}
        for c in childs:
            tagName=self.doc.getTagName(c)
            if tagName in ['usr']:
                sId=self.doc.getAttribute(c,'fid')
                if tagName=='usr':
                    if len(sId)>0:
                        sVal=hums.GetInfoById(sId)[1]
                    else:
                        sVal=self.doc.getText(c)
                d[tagName]=sVal
            else:
                d[tagName]=self.doc.getText(c)
            if tagName=='changed':
                bSkip=True
        if bSkip==False:
            id=self.doc.getAttribute(node,'id')
            self.logs[id]=(node,d)
        #sNr=self.doc.getNodeText(node,'nr')
        #sPrior=self.doc.getNodeText(node,'prior')
        #sSeverity=self.doc.getNodeText(node,'severity')
        #sDate=self.doc.getNodeText(node,'date')
        #sTime=self.doc.getNodeText(node,'time')
        #sName=self.doc.getNodeText(node,'name')
        #sStartDate=self.doc.getNodeText(node,'start')
        #sEndDate=self.doc.getNodeText(node,'end')
        #usrNode=self.doc.getChild(node,'usr')
        #sUsrId=self.doc.getAttribute(usrNode,'fid')
        #if len(sUsrId)>0:
        #    sUsr=self.humans.GetInfoById(sUsrId)[1]
        #else:
        #    sUsr=self.doc.getNodeText(node,'usr')
    def __showInfo__(self):
        self.lstAct.DeleteAllItems()
        self.lstSort=[]
        if self.iSortCol==-1:
            sortKeys=['severity','prior','status','date','name']
        else:
            sortKeys=[self.colOrder[self.iSortCol]]
        i=0
        for kLog in self.logs.keys():
            actTup=self.logs[kLog]
            keys=[]
            for k in sortKeys:
                sVal=actTup[1][k]
                if k is 'prior':
                    sVal=MAP_PRIOR_SORT[sVal]
                elif k is 'status':
                    sVal=MAP_STATUS_SORT[sVal]
                elif k is 'severity':
                    sVal=MAP_SEVERITY_SORT[sVal]
                keys.append(sVal)
            self.lstSort.append((keys,actTup[1],kLog))
            i+=1
        def compFunc(a,b):
            aKeys=a[0]
            bKeys=b[0]
            for ak,bk in zip(aKeys,bKeys):
                if self.bSortAsc:
                    iRes=cmp(ak,bk)
                else:
                    iRes=cmp(bk,ak)
                if iRes!=0:
                    return iRes
            return 0
        self.lstSort.sort(compFunc)
        iIdx=0
        for it in self.lstSort:
            d=it[1]
            index = self.lstAct.InsertImageStringItem(sys.maxint, d[self.colOrder[0]], -1)
            i=1
            for k in self.colOrder[1:]:
                try:
                    sVal=d[k]
                except:
                    sVal=''
                self.lstAct.SetStringItem(index,i, sVal, -1)
                i+=1
            self.lstAct.SetItemData(index,iIdx)
            iIdx+=1
    def __guiData2NodeDict__(self,n,d):
        dn={}
        try:
            dn['nr']=d['nr']
        except:
            dn['nr']=''
        dn['name']=self.txtActName.GetValue()
        dn['desc']=self.txtActDesc.GetValue()
        dn['usr']=self.chcUsr.GetStringSelection()
        dn['status']=self.chcStatus.GetStringSelection()
        dn['severity']=self.chcSeverity.GetStringSelection()
        dn['prior']=self.chcPrior.GetStringSelection()
        dn['start']=self.zStart.GetValue()
        dn['end']=self.zEnd.GetValue()
        
        lstGrp=[]
        lstUsr=[]
        iLen=self.lstAssign.GetItemCount()
        hums=self.netDocHuman.GetSortedUsrIds()
        grps=self.netDocHuman.GetSortedGrpIds()
        for i in range(iLen):
            it=self.lstAssign.GetItem(i,0)
            k=it.m_text
            if k=='g':
                it=self.lstAssign.GetItem(i,1)
                sGrp=it.m_text
                grpID=grps.GetIdByName(sGrp)[0]
                lstGrp.append(grpID)
            elif k=='u':
                it=self.lstAssign.GetItem(i,1)
                sUsr=it.m_text
                usrID=hums.GetIdByName(sUsr)[0]
                lstUsr.append(usrID)
        dn['assign_grp']=string.join(lstGrp,',')
        dn['assign_usr']=string.join(lstUsr,',')
        hums=self.netDocHuman.GetSortedUsrIds()
        for k in dn.keys():
            d[k]=dn[k]
            if n is not None:
                self.doc.setNodeText(n,k,dn[k])
                if k in ['usr']:
                    tmpNode=self.doc.getChild(n,k)
                    if k=='usr':
                        self.doc.setAttribute(tmpNode,'fid',
                                        hums.GetIdByName(dn[k]))
        if d['nr']=='':
            settingsNode=self.doc.getChild(self.doc.getRoot(),'settings')
            tNode=self.doc.getChild(settingsNode,'activities')
            if tNode is None:
                lst=[{'tag':'current','val':'0'},
                     {'tag':'max','val':'10'}
                    ]
                tNode=self.doc.createChildByLst(settingsNode,'activities',lst)
                self.doc.AlignNode(tNode)
            try:
                sVal=self.doc.getNodeText(tNode,'current')
                sMax=self.doc.getNodeText(tNode,'max')
                iVal=long(sVal)
                iMax=long(sMax)
                iVal+=1
                if iVal<iMax:
                    self.doc.setNodeText(tNode,'current',str(iVal))
                else:
                    return -2
                d['nr']=str(iVal)
                return 0
            except:
                return -1
        return 0
    def __setGuiData__(self,d):
        self.__clear__()
        if d.has_key('name'):
            self.txtActName.SetValue(d['name'])
        if d.has_key('desc'):
            self.txtActDesc.SetValue(d['desc'])
        if d.has_key('usr'):
            self.chcUsr.SetStringSelection(d['usr'])
        if d.has_key('status'):
            self.chcStatus.SetStringSelection(d['status'])
        if d.has_key('severity'):
            self.chcSeverity.SetStringSelection(d['severity'])
        if d.has_key('prior'):
            self.chcPrior.SetStringSelection(d['prior'])
        if d.has_key('start'):
            self.zStart.SetValue(d['start'])
        if d.has_key('end'):
            self.zEnd.SetValue(d['end'])
        grps=self.netDocHuman.GetSortedGrpIds()
        if d.has_key('assign_grp'):
            idStrs=string.split(d['assign_grp'],',')
            lst=[]
            for sID in idStrs:
                tup=grps.GetInfoById(sID)
                if tup[0] is not None:
                    lst.append(tup[1])
            lst.sort()
            for sGrp in lst:
                index = self.lstAssign.InsertImageStringItem(sys.maxint, 'g', 0)
                self.lstAssign.SetStringItem(index,1, sGrp, -1)
        hums=self.netDocHuman.GetSortedUsrIds()
        if d.has_key('assign_usr'):
            idStrs=string.split(d['assign_usr'],',')
            lst=[]
            for sID in idStrs:
                tup=hums.GetInfoById(sID)
                if tup[0] is not None:
                    lst.append(tup[1])
            lst.sort()
            for sUsr in lst:
                index = self.lstAssign.InsertImageStringItem(sys.maxint, 'u', 0)
                self.lstAssign.SetStringItem(index,1, sUsr, -1)
            
    def OnGcbbSetButton(self, event):
        if self.selIdx<0:
            return
        sortIdx=self.lstAct.GetItemData(self.selIdx)
        nr=self.lstSort[sortIdx][2]
        n,d=self.logs[nr]
        
        dt=wx.DateTime.Now()
        self.doc.setNodeText(n,'changed',
                dt.FormatISODate()+' '+dt.FormatISOTime())
        self.doc.AlignNode(n)
        
        if self.__guiData2NodeDict__(None,d)<0:
            return
        del self.logs[nr]
        hums=self.netDocHuman.GetSortedUsrIds()
        usrID=hums.GetIdByName(d['usr'])[0]
        lst=[{'tag':'date','val':dt.FormatISODate()},
             {'tag':'time','val':dt.FormatISOTime()},
             {'tag':'status','val':d['status']},
             {'tag':'nr','val':d['nr']},
             {'tag':'name','val':d['name']},
             {'tag':'desc','val':d['desc']},
             {'tag':'start','val':d['start']},
             {'tag':'end','val':d['end']},
             {'tag':'assign_grp','val':d['assign_grp']},
             {'tag':'assign_usr','val':d['assign_usr']},
             {'tag':'severity','val':d['severity']},
             {'tag':'prior','val':d['prior']},
             {'tag':'usr','val':d['usr'],'attr':('fid',usrID)}]
        
        n=self.doc.createChildByLst(self.node,self.name,lst)
        #FIXME
        self.doc.checkId(n)
        self.doc.processMissingId()
        self.doc.clearMissingId()
        self.doc.AlignNode(n)
        self.__addInfo__(n)
        self.__showInfo__()
        wx.PostEvent(self,vgpDataChanged(self,self.node))
        event.Skip()

    def OnLstActListItemSelected(self, event):
        self.chcStatus.Enable(True)
        
        idx=event.GetIndex()
        sortIdx=self.lstAct.GetItemData(idx)
        nr=self.lstSort[sortIdx][2]
        n,d=self.logs[nr]
        self.__setGuiData__(d)
        self.selIdx=idx
        event.Skip()

    def OnLstActListColClick(self, event):
        iCol=event.GetColumn()
        if iCol==self.iSortCol:
            if self.bSortAsc:
                self.bSortAsc=False
            else:
                self.iSortCol=-1
                self.bSortAsc=True
        else:
            self.iSortCol=iCol
            self.bSortAsc=True
        self.__showInfo__()
        event.Skip()

    def OnCbAddAssignButton(self, event):
        iGrp=self.chcAssignGrp.GetSelection()
        if iGrp>0:
            # add group
            sGrp=self.chcAssignGrp.GetStringSelection()
            #grpID=self.grps.GetIdByName(sGrp)[0]
            index = self.lstAssign.InsertImageStringItem(sys.maxint, 'g', 0)
            self.lstAssign.SetStringItem(index,1, sGrp, -1)
            self.chcAssignGrp.SetSelection(0)
        iUsr=self.chcAssignUsr.GetSelection()
        if iUsr>0:
            sUsr=self.chcAssignUsr.GetStringSelection()
            #usrID=self.humans.GetIdByName(sUsr)[0]
            index = self.lstAssign.InsertImageStringItem(sys.maxint, 'u', 1)
            self.lstAssign.SetStringItem(index,1, sUsr, -1)
            self.chcAssignUsr.SetSelection(0)
        event.Skip()

    def OnCbAssignDelButton(self, event):
        if self.selAssign>=0:
            self.lstAssign.DeleteItem(self.selAssign)
            self.selAssign=-1
        event.Skip()

    def OnLstActListItemDeselected(self, event):
        self.selIdx=-1
        self.chcStatus.SetSelection(0)
        self.chcStatus.Enable(False)
        self.__clear__()
        event.Skip()

    def OnLstAssignListItemSelected(self, event):
        self.selAssign=event.GetIndex()
        event.Skip()

    def OnLstAssignListItemDeselected(self, event):
        self.selAssign=-1
        event.Skip()
        
