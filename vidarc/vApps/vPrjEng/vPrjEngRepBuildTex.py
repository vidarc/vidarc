#----------------------------------------------------------------------------
# Name:         vPrjEngRepBuildTex.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngRepBuildTex.py,v 1.2 2006/01/08 15:12:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.report.vRepBuildTex import *
import vidarc.vApps.vPrjEng.DocBuildLatex as DocBuildLatex
import vidarc.tool.log.vtLog as vtLog

class vPrjEngRepBuildTex(vRepBuildTex):
    def __init__(self,par,verbose=0):
        vRepBuildTex.__init__(self,par,verbose)
        self.genObj={}
        self.node=None
    def SetGenObj(self,genObj):
        self.genObj=genObj
    def SetNode(self,node):
        self.node=node
    def __procInfo__(self,f,info):
        #vtLog.CallStack('')
        if self.node is None:
            return -1
        DocBuildLatex.generateNew(f,self.dictRepl,self.node,
                info,
                self.genObj)
        #vtLog.CallStack('')
        return 0
