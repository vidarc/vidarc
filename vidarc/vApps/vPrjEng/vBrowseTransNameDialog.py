#Boa:Dialog:vBrowseTransNameDialog
#----------------------------------------------------------------------------
# Name:         vBrowseActionNameDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vBrowseTransNameDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import images

def create(parent):
    return vBrowseTransNameDialog(parent)

[wxID_VBROWSETRANSNAMEDIALOG, wxID_VBROWSETRANSNAMEDIALOGGCBBAPPLY, 
 wxID_VBROWSETRANSNAMEDIALOGGCBBCANCEL, wxID_VBROWSETRANSNAMEDIALOGGPROCESS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vBrowseTransNameDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VBROWSETRANSNAMEDIALOG,
              name=u'vBrowseTransNameDialog', parent=prnt, pos=wx.Point(237,
              99), size=wx.Size(320, 381), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Transition Name')
        self.SetClientSize(wx.Size(312, 354))
        self.Bind(wx.EVT_ACTIVATE, self.OnVgdBrowseTransNameActivate)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSETRANSNAMEDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(72, 320), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VBROWSETRANSNAMEDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSETRANSNAMEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(168, 320),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VBROWSETRANSNAMEDIALOGGCBBCANCEL)

        self.gProcess = wx.Gauge(id=wxID_VBROWSETRANSNAMEDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(8, 294), range=100,
              size=wx.Size(294, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.selNode=None
        self.doc=None
        self.node=None
        self.relNode=None
        self.bSet=False
        self.bUpdate=False
        self.bSelect=False
        self.ID2Select=''
        
        self.vgpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(4,4),size=(304,280),style=0,name="vgpTree",gui_update=True)
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #self.vgpTree.SetProcessBar(self.gProcess,None)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        self.vgpTree.EnableCache()
        self.vgpTree.SetNetResponse(False)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        self.vgpTree.SetLanguage(lang)
    def SelectByID(self,sID):
        self.bSelect=True
        self.ID2Select=sID
    
    def SetRelNode(self,node):
        self.relNode=node
    def SetDoc(self,doc,bNet):
        self.doc=doc
        self.vgpTree.SetDoc(doc,bNet)
    def SetNode(self,node):
        if self.IsShown():
            self.vgpTree.SetNode(node,['templates'])
        else:
            self.node=node
    def GetSelNode(self):
        return self.selNode
    def GetTagName(self):
        if self.selNode is not None:
            return Hierarchy.getTagNames(self.doc,self.vgpTree.rootNode,self.relNode,self.selNode)
        return ''
    def GetId(self):
        if self.selNode is not None:
            return self.doc.getAttribute(self.selNode,'id')
        return ''
    def __getElementByTagName__(self,tagname):
        for e in ELEMENTS:
            if e[0]==tagname:
                return e
        return None
    def __checkPossible__(self,node):
        if self.node2inst is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.doc.getTagName(self.node2inst)
        sType=node.tagName
        elemNode=self.__getElementByTagName__(self.doc.getTagName(node))
        elemNode2Inst=self.__getElementByTagName__(sTypeNode2Inst)
        if elemNode is None:
            return -4
        if elemNode2Inst is None:
            return -3
        try:
            for t in elemNode[1]['__possible']:
                if t[:7]=='__type:':
                    strs=string.split(t,':')
                    if strs[1]==elemNode2Inst[1]['__type']:
                        return 1
                elif t==sTypeNode2Inst:
                    return 1
        except:
            return -5
        return 0
    def OnGcbbApplyButton(self, event):
        #ret=self.__checkPossible__(self.selNode)
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        ret=1
        if ret==1:
            self.EndModal(1)
            event.Skip()
            return
        elif ret==0:
            # adding not possible
            sMsg=u'Instancing at this position not allowed.'
        elif ret==-1:
            # no node to instance
            sMsg=u'No node to instance!'
        elif ret==-2:
            # no node selected
            sMsg=u'No target node selected.'
        elif ret==-3:
            # element to instance not found
            sMsg=u'No element of node to instance found!'
        elif ret==-4:
            # element not found
            sMsg=u'No element of target node found!'
        elif ret==-5:
            # error oocurred
            sMsg=u'A serious error has occurred.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnGcbbCancelButton(self, event):
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        self.EndModal(0)
        event.Skip()

    def OnVgdBrowseTransNameActivate(self, event):
        if not(self.vgpTree.IsNetResponse()):
            self.vgpTree.SetNetResponse(True)
            self.vgpTree.SetNode(None)#),['templates'])
            self.bSet=True
            
        if self.node is not None:
            #self.vgpTree.SetNode(self.node)#,['templates'])
            self.bSet=True
            self.node=None
        elif self.bUpdate==True:
            #self.vgpTree.SetNode(self.vgpTree.rootNode)#),['templates'])
            self.bSet=True
        if self.vgpTree.IsThreadRunning()==False:
            if self.bSelect==True:
                if len(self.ID2Select)>0:
                    self.vgpTree.SelectByID(self.ID2Select)
                self.ID2Select=''
                self.bSelect=False
        self.bUpdate=False
        event.Skip()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        wx.CallAfter(self.__select__)
        evt.Skip()
    def __select__(self):
        if self.bSelect==True:
            if len(self.ID2Select)>0:
                self.vgpTree.SelectByID(self.ID2Select)
            self.ID2Select=''
            self.bSelect=False
        self.gProcess.SetValue(0)
        
