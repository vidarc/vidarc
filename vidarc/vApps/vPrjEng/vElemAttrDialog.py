#Boa:Dialog:vElemAttrDialog
#----------------------------------------------------------------------------
# Name:         vDataPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElemAttrDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys,string
from vidarc.vApps.vPrjEng.InputValues import *
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import images

def create(parent):
    return vElemAttrDialog(parent)

[wxID_VELEMATTRDIALOG, wxID_VELEMATTRDIALOGCBBROWSE, 
 wxID_VELEMATTRDIALOGCHCBROWSEABLE, wxID_VELEMATTRDIALOGCHCINHERITANCE, 
 wxID_VELEMATTRDIALOGCHCTYPE, wxID_VELEMATTRDIALOGGCBBAPPLY, 
 wxID_VELEMATTRDIALOGGCBBCANCEL, wxID_VELEMATTRDIALOGGCBBDEL, 
 wxID_VELEMATTRDIALOGGCBBSET, wxID_VELEMATTRDIALOGLBLATTR, 
 wxID_VELEMATTRDIALOGLBLNAME, wxID_VELEMATTRDIALOGLBLPRETYPE, 
 wxID_VELEMATTRDIALOGLBLTYPE, wxID_VELEMATTRDIALOGLSTATTR, 
 wxID_VELEMATTRDIALOGTXTATTRNAME, wxID_VELEMATTRDIALOGTXTATTRVAL, 
 wxID_VELEMATTRDIALOGTXTNAME, wxID_VELEMATTRDIALOGTXTPRETYPE, 
] = [wx.NewId() for _init_ctrls in range(18)]

class vElemAttrDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VELEMATTRDIALOG,
              name=u'vElemAttrDialog', parent=prnt, pos=wx.Point(237, 99),
              size=wx.Size(400, 373), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Element Attribute')
        self.SetClientSize(wx.Size(392, 346))

        self.lblName = wx.StaticText(id=wxID_VELEMATTRDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VELEMATTRDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(72, 8),
              size=wx.Size(100, 21), style=wx.TE_RICH2, value=u'')

        self.chcInheritance = wx.CheckBox(id=wxID_VELEMATTRDIALOGCHCINHERITANCE,
              label=u'Inheritance', name=u'chcInheritance', parent=self,
              pos=wx.Point(216, 40), size=wx.Size(73, 13), style=0)
        self.chcInheritance.SetValue(False)
        self.chcInheritance.Bind(wx.EVT_CHECKBOX, self.OnChcInheritanceCheckbox,
              id=wxID_VELEMATTRDIALOGCHCINHERITANCE)

        self.chcBrowseable = wx.CheckBox(id=wxID_VELEMATTRDIALOGCHCBROWSEABLE,
              label=u'Browseable', name=u'chcBrowseable', parent=self,
              pos=wx.Point(216, 8), size=wx.Size(73, 13), style=0)
        self.chcBrowseable.SetValue(False)
        self.chcBrowseable.Bind(wx.EVT_CHECKBOX, self.OnChcBrowseableCheckbox,
              id=wxID_VELEMATTRDIALOGCHCBROWSEABLE)

        self.lblType = wx.StaticText(id=wxID_VELEMATTRDIALOGLBLTYPE,
              label=u'Type', name=u'lblType', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(24, 13), style=0)

        self.chcType = wx.Choice(choices=[u'predefined', u'bool', u'int',
              u'long', u'float', u'pseudofloat', u'text', u'string', u'choice',
              u'choiceint', u'datetime'], id=wxID_VELEMATTRDIALOGCHCTYPE,
              name=u'chcType', parent=self, pos=wx.Point(72, 40),
              size=wx.Size(130, 21), style=0)
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VELEMATTRDIALOGCHCTYPE)

        self.lblAttr = wx.StaticText(id=wxID_VELEMATTRDIALOGLBLATTR,
              label=u'Attributes', name=u'lblAttr', parent=self, pos=wx.Point(8,
              112), size=wx.Size(44, 13), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VELEMATTRDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 144),
              size=wx.Size(288, 160), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VELEMATTRDIALOGLSTATTR)

        self.txtAttrName = wx.TextCtrl(id=wxID_VELEMATTRDIALOGTXTATTRNAME,
              name=u'txtAttrName', parent=self, pos=wx.Point(64, 112),
              size=wx.Size(100, 21), style=0, value=u'')

        self.txtAttrVal = wx.TextCtrl(id=wxID_VELEMATTRDIALOGTXTATTRVAL,
              name=u'txtAttrVal', parent=self, pos=wx.Point(176, 112),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMATTRDIALOGGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(304, 144), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VELEMATTRDIALOGGCBBSET)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMATTRDIALOGGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(304, 184), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VELEMATTRDIALOGGCBBDEL)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMATTRDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(104, 312), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VELEMATTRDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMATTRDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(192, 312),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VELEMATTRDIALOGGCBBCANCEL)

        self.lblPreType = wx.StaticText(id=wxID_VELEMATTRDIALOGLBLPRETYPE,
              label=u'predefined', name=u'lblPreType', parent=self,
              pos=wx.Point(8, 72), size=wx.Size(50, 13), style=0)

        self.txtPreType = wx.TextCtrl(id=wxID_VELEMATTRDIALOGTXTPRETYPE,
              name=u'txtPreType', parent=self, pos=wx.Point(72, 72),
              size=wx.Size(224, 21), style=0, value=u'')

        self.cbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMATTRDIALOGCBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse', name=u'cbBrowse',
              parent=self, pos=wx.Point(304, 68), size=wx.Size(76, 30),
              style=0)
        self.cbBrowse.Bind(wx.EVT_BUTTON, self.OnCbBrowseButton,
              id=wxID_VELEMATTRDIALOGCBBROWSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.dlgBrowse=None
        
        self.inpValue=InputValues(parent=self,pos=wx.Point(176, 112),
                size=wx.Size(135,21))
        
        self.selAttrIdx=-1
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
                
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.cbBrowse.SetBitmapLabel(img)
        
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_RIGHT,150)

    def Set(self,doc,name,d,typeDict,dlgBrowse=None):
        if dlgBrowse is not None:
            self.dlgBrowse=dlgBrowse
        if doc is not None:
            self.doc=doc
        if name is not None:
            self.txtName.SetValue(name)
            self.txtName.SetStyle(0, len(name), wx.TextAttr("BLACK", "WHITE"))
        
        self.typeDict=typeDict
        self.selAttrIdx=-1
        self.lstAttr.DeleteAllItems()
        self.dictAttr=d
        keys=d.keys()
        keys.sort()
        self.chcInheritance.SetValue(0)
        self.chcType.Enable(True)
        self.txtAttrName.SetValue('')
        
        self.txtAttrVal.SetValue('')
        self.bBrowseable=False
        self.bPreDef=False
        if d.has_key('__browse'):
            self.bBrowseable=True
            self.chcBrowseable.SetValue(True)
            self.chcType.Enable(False)
            self.chcInheritance.Enable(False)
            self.txtAttrName.Enable(False)
            self.txtAttrVal.Enable(True)
            self.txtPreType.Enable(False)
            self.cbBrowse.Enable(False)
        elif d.has_key('__predef'):
            self.bPreDef=True
            self.txtPreType.SetValue(d['__predef'])
            self.chcType.SetStringSelection('predefined')
            self.chcInheritance.Enable(False)
            self.txtAttrName.Enable(False)
            self.txtAttrVal.Enable(False)
            self.txtPreType.Enable(True)
            self.cbBrowse.Enable(True)
            keys=[]
        else:
            self.bBrowseable=False
            self.chcBrowseable.SetValue(False)
            self.chcInheritance.Enable(True)
            self.txtAttrName.Enable(True)
            self.txtAttrVal.Enable(True)
            self.txtPreType.Enable(False)
            self.cbBrowse.Enable(False)
        self.__updateAttr__(d)
    def __updateAttr__(self,d):
        self.selAttrIdx=-1
        self.lstAttr.DeleteAllItems()
        self.dictAttr=d
        keys=d.keys()
        keys.sort()
        if self.bPreDef:
            keys=[]
        for k in keys:
            if d[k] is None:
                continue
            if self.bBrowseable:
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
                self.lstAttr.SetStringItem(index,1,d[k],-1)  
            else:
                if k == 'type':
                    for i in range(0,self.chcType.GetCount()):
                        s=self.chcType.GetString(i)
                        if s==d[k]:
                            self.chcType.SetSelection(i)
                elif k=='inheritance':
                    strs=string.split(d[k],',')
                    try:
                        idx=strs.index('type')
                        self.chcInheritance.SetValue(1)
                        self.chcType.Enable(False)
                        self.txtAttrName.SetValue('val')
                    except:
                        pass
                else:
                    index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
                    self.lstAttr.SetStringItem(index,1,d[k],-1)
        if self.typeDict is None:
            self.chcInheritance.SetValue(0)
            self.chcInheritance.Enable(False)
        else:
            if self.chcInheritance.GetValue()==0:
                self.typeDict=self.dictAttr
        self.__setAttrValue__()
        
    def Get(self):
        return self.txtName.GetValue(),self.dictAttr
    def OnGcbbSetButton(self, event):
        sAttr=self.txtAttrName.GetValue()
        if self.chcInheritance.GetValue():
            sVal=self.inpValue.Get()
        else:
            sVal=self.txtAttrVal.GetValue()
        index=self.lstAttr.FindItem(-1,sAttr,False)
        if index<0:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttr, -1)
            self.lstAttr.SetStringItem(index,1,sVal,-1)
        else:
            self.lstAttr.SetStringItem(index,1,sVal,-1)
        self.dictAttr[sAttr]=sVal
        event.Skip()

    def OnGcbbDelButton(self, event):
        it=self.lstAttr.GetItem(self.selAttrIdx,0)
        del self.dictAttr[it.m_text]
        self.lstAttr.DeleteItem(self.selAttrIdx)
        event.Skip()

    def OnGcbbApplyButton(self, event):
        sName=self.txtName.GetValue()
        if sName=="undefined":
            self.txtName.SetStyle(0, len(sName), wx.TextAttr("RED", "YELLOW"))
            return
        self.txtName.SetStyle(0, len(sName), wx.TextAttr("BLACK", "WHITE"))
        if self.dictAttr.has_key('type'):
            if self.dictAttr['type']=='predefined':
                if len(self.dictAttr['__predef'])==0:
                    return
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnChcTypeChoice(self, event):
        if self.chcBrowseable.GetValue():
            return
        i=event.GetSelection()
        sType=self.chcType.GetString(i)
        if sType=='int':
            d={'type':'int','val':'0','min':'0','max':'100'}
        elif sType=='long':
            d={'type':'long','val':'0','min':'0','max':'100'}
        elif sType=='float':
            d={'type':'float','val':'0.0','digits':'6','comma':'2','min':'0','max':'100'}
        elif sType=='pseudofloat':
            d={'type':'pseudofloat','val':'0.0','digits':'6','comma':'2','min':'0','max':'100'}
        elif sType=='text':
            d={'type':'text','val':'-','len':'8'}
        elif sType=='string':
            d={'type':'string','val':'-','len':'8'}
        elif sType=='choice':
            d={'type':'choice','val':'0','it00':'item  0','it01':'item  1',
                    'it02':'item  2','it03':'item  3','it04':'item  4',
                    'it05':'item  5','it06':'item  6','it07':'item  7',
                    'it08':'item  8','it09':'item  9','it10':'item 10',}
        elif sType=='choiceint':
            d={'type':'choiceint','val':'0','min':'0','max':'0'}
        elif sType=='datetime':
            d={'type':'datetime','val':'0','min':'0','max':'0'}
        elif sType=='predefined':
            d={'type':'predefined','val':'0','__fid':'','__predef':''}
        else:
            return
        self.Set(None,None,d,self.typeDict)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        self.txtAttrName.SetValue(it.m_text)
        self.selAttrIdx=it.m_itemId
        it=self.lstAttr.GetItem(idx,1)
        self.txtAttrVal.SetValue(it.m_text)
        self.__setAttrValue__()
        event.Skip()
    def __setAttrValue__(self):
        if self.chcBrowseable.GetValue():
            self.inpValue.Enable(False)
            self.txtAttrVal.Enable(True)
            pass
        else:
            if self.chcInheritance.GetValue():
                self.inpValue.Enable(True)
                self.txtAttrVal.Enable(False)
                self.inpValue.Set(self.dictAttr,self.typeDict)
            else:
                self.inpValue.Enable(False)
                self.txtAttrVal.Enable(True)
    def OnChcInheritanceCheckbox(self, event):
        if self.chcBrowseable.GetValue():
            return
        if self.chcInheritance.GetValue():
            self.chcType.Enable(False)
            try:
                s=self.dictAttr['inheritance']
                strs=string.split(s)
                try:
                    strs.index('type')
                except:
                    strs.append('type')
            except:
                strs=['type']
            self.dictAttr['inheritance']=string.join(strs,',')
            
            self.dictAttr['type']=self.typeDict['type']
            self.txtAttrName.SetValue('val')
            for k in self.dictAttr.keys():
                if k not in ['type','val','inheritance']:
                    self.dictAttr[k]=None
            self.__setAttrValue__()
            self.dictAttr['val']=self.inpValue.Get()
            self.Set(None,None,self.dictAttr,self.typeDict)
        else:
            self.chcType.Enable(True)
            try:
                s=self.dictAttr['inheritance']
                strs=string.split(s)
                try:
                    strs.remove('type')
                except:
                    pass
                self.dictAttr['inheritance']=string.join(strs,',')
            except:
                pass
            self.Set(None,None,self.dictAttr,self.typeDict)

    def OnChcBrowseableCheckbox(self, event):
        if self.chcBrowseable.GetValue():
            #self.chcType.Enable(False)
            #self.chcInheritance.Enable(False)
            #self.txtAttrName.Enable(False)
            #self.txtAttrVal.Enable(True)
            #self.txtPreType.Enable(False)
            #self.cbBrowse.Enable(False)
            
            self.dictAttr['type']={'__browse':'*','__fid':'','attr':'',
                                    'type':'','formular':'','val':''}
            self.txtAttrName.SetValue('val')
            self.__setAttrValue__()
            self.inpValue.Clear()
            self.dictAttr['val']=self.inpValue.Get()
            #self.bBrowseable=True
            #self.bPreDef=False
            #self.__updateAttr__(self.dictAttr['type'])
            self.Set(None,None,self.dictAttr['type'],self.typeDict)
        else:
            self.chcType.Enable(True)
            self.dictAttr['type']={'type':'string','val':'','len':'8'}
            self.chcInheritance.Enable(True)
            self.Set(None,None,self.dictAttr['type'],self.typeDict)
            
            #self.OnChcInheritanceCheckbox(None)
        event.Skip()

    def OnCbBrowseButton(self, event):
        if self.dlgBrowse is None:
            return
        self.dlgBrowse.Centre()
        dictAttr={'__browse':'type','__predef':'','__fid':'','val':'','type':''}
        dictAttr['__fid']=self.dictAttr['__fid']
        dictAttr['name']=self.txtName.GetValue()
        self.dlgBrowse.SetNode('type',dictAttr['__fid'],dictAttr,'typeContainer')
        ret=self.dlgBrowse.ShowModal()
        if ret==1:
            node=self.dlgBrowse.GetSelNode()
            rootNode=Hierarchy.getRootNode(self.doc,node)
            sTag=Hierarchy.getTagNamesWithRel(self.doc,rootNode,None,node)
            #dictAttr=self.dictAttributes[self.attrName]
            dictAttr['val']=self.doc.getNodeText(node,'tag')
            dictAttr['__fid']=self.doc.getAttribute(node,'id')
            self.dictAttr['__fid']=self.doc.getAttribute(node,'id')
            self.dictAttr['__predef']=sTag
            
            self.dictAttr['type']=dictAttr
            self.txtPreType.SetValue(sTag)
        event.Skip()
