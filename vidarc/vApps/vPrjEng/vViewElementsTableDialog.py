#Boa:Dialog:vViewElementsTableDialog
#----------------------------------------------------------------------------
# Name:         vViewElementsTableDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vViewElementsTableDialog.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import sys,string

import vidarc.vApps.vPrjEng.gridAttr as gridAttr

import images

def create(parent):
    return vViewElementsTableDialog(parent)

[wxID_VVIEWELEMENTSTABLEDIALOG, wxID_VVIEWELEMENTSTABLEDIALOGGCBBCANCEL, 
 wxID_VVIEWELEMENTSTABLEDIALOGGCBBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vViewElementsTableDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VVIEWELEMENTSTABLEDIALOG,
              name=u'vViewElementsTableDialog', parent=prnt, pos=wx.Point(39,
              40), size=wx.Size(859, 478), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Engineering View Elements Table')
        self.SetClientSize(wx.Size(851, 451))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VVIEWELEMENTSTABLEDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(316, 416), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VVIEWELEMENTSTABLEDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VVIEWELEMENTSTABLEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(472, 416),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VVIEWELEMENTSTABLEDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.bModified=False
        self.lstFilter=[]
        #self.lstDocs.InsertColumn(0,u'Doc')
        self.gdAttr=gridAttr.gridAttrGrid(self,wx.NewId(),pos=wx.Point(4,4),
            size=wx.Size(820,400))
        self.gdAttr.SetConstraints(LayoutAnchors(self.gdAttr, True,
              True, True, True))
        gridAttr.EVT_GRIDATTR_INFO_CHANGED(self.gdAttr, self.OnInfoChanged)
        

        #self.vgpElementInfos=vgpElementInfos(self,wx.NewId(),
        #    pos=(0,0),size=(400,400),style=0,name="vgpElementsInfo")
        #EVT_ELEMENT_INFOS_CHANGED(self.vgpElementInfos,
        #        self.OnElementInfoChanged)
        
        img=images.getApplyBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def __setModified__(self,state):
        self.bModified=state
        #if self.bModified:
        #    self.lblModified.SetLabel('*')
        #else:
        #    self.lblModified.SetLabel('')
    def SetNode(self,node,lst,doc):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you realy want to loose modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_NO:
                self.GetNode()
        self.doc=doc
        self.node=node
        self.bModified=False
        self.selElem=None
        self.selAttrIdx=-1
        self.lstNodes=lst
        oldGrid=self.gdAttr
        
        self.gdAttr=gridAttr.gridAttrGrid(self,wx.NewId(),pos=wx.Point(4,4),
            size=oldGrid.GetSize())
        self.gdAttr.SetConstraints(LayoutAnchors(self.gdAttr, True,
              True, True, True))
        gridAttr.EVT_GRIDATTR_INFO_CHANGED(self.gdAttr, self.OnInfoChanged)
        
        self.gdAttr.Setup(node,lst,self.lstFilter,doc)
        
        oldGrid.Destroy()
        
        self.__setModified__(False)
    def GetNode(self):
        self.gdAttr.Get()
        self.__setModified__(False)
    
    def OnInfoChanged(self,evt):
        self.__setModified__(True)
        pass
    
    def SetFilter(self,lst):
        self.lstFilter=lst
    #def SetNode(self,node,lst,doc=None):
    #    self.vgpElementInfos.SetNode(node,lst,doc)
    #def SetFilter(self,lst):
    #    self.vgpElementInfos.SetFilter(lst)
    def OnElementInfoChanged(self,event):
        pass
    def OnGcbbOkButton(self, event):
        self.GetNode()
        #wx.PostEvent(self,vgpElementInfosChanged(self,self.node))
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.gdAttr.Set()
        self.EndModal(0)
        event.Skip()

    
