#Boa:Dialog:vBrowseWithAttrDialog
#----------------------------------------------------------------------------
# Name:         vBrowseWithAttrDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vBrowseWithAttrDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.Elements as Elements
import vidarc.vApps.vPrjEng.vElementsPanel as ELEMENTS
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import sys,string,fnmatch,thread
import images

PROCESSING={u'--':'', u'original value':"'$0$'",
    u'first result':"'$0$'", 'slicing':'[:]', 
    u'next processing':',',u'+':'+',
    u'relative hierarchy':"'$relative$'",
    u'hierarchy to relative':"'$hierarchy_relative$'",
    u'absolute hierarchy':"'$absolute$'",
    u'absolute hierarchy with base':"'$absolute_base$'"}

def create(parent):
    return vBrowseWithAttrDialog(parent)

wxEVT_THREAD_FIND_ELEMENTS=wx.NewEventType()
def EVT_THREAD_FIND_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FIND_ELEMENTS,func)
class wxThreadFindElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FIND_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iStep,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.step=iStep
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_FIND_ELEMENTS)
    def GetStep(self):
        return self.step
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_FIND_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_FIND_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FIND_ELEMENTS_FINISHED,func)
class wxThreadFindElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FIND_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_FIND_ELEMENTS_FINISHED)
    

class thdFind:
    def __init__(self,par):
        self.tree=par
        self.lstSort=None
        self.running = False
    def Find(self,node,ids,sType,sFltHier,sFltTag,sFltName,id):
        self.node=node
        self.ids=ids
        self.sType=sType
        self.sFltHier=sFltHier
        self.sFltTag=sFltTag
        self.sFltName=sFltName
        self.bFind=True
        self.id=id
        self.Start()
        self.lstSort=None
    def Clear(self):
        self.iCount=0
        self.iFound=0
            
        self.dElem={}
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procNode__(self,node):
        if self.keepGoing==False:
            return
        bFound=True
        sTag=None
        sHier=None
        sTagName=None
        sName=None
        self.iAct+=1
        wx.PostEvent(self.tree,wxThreadFindElements(1,self.iAct))
        if self.sType:
            sTag=self.tree.doc.getTagName(node)
            if sTag!=self.sType:
                bFound=False
        if bFound and self.sFltHier:
            sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
            if fnmatch.fnmatch(sHier,self.sFltHier)==False:
                bFound=False
        if bFound and self.sFltTag:
            sTagName=self.tree.doc.getNodeText(node,'tag')
            if fnmatch.fnmatch(sTagName,self.sFltTag)==False:
                bFound=False
        if bFound and self.sFltName:
            sName=self.tree.doc.getNodeText(node,'name')
            if fnmatch.fnmatch(sName,self.sFltName)==False:
                bFound=False
        if bFound:
            self.iFound+=1
            if sTag is None:
                sTag=self.tree.doc.getTagName(node)
            if sHier is None:
                sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
            if sTagName is None:
                sTagName=self.tree.doc.getNodeText(node,'tag')
            if sName is None:
                sName=self.tree.doc.getNodeText(node,'name')
            try:
                lst=self.dElem[sTag]
                lst.append((sHier,sTagName,sName,node))
            except:
                self.dElem[sTag]=[(sHier,sTagName,sName,node)]
        self.iCount+=1
        childs=self.tree.doc.getChildsAttr(node,'id')
        for c in childs:
            self.__procNode__(c)
    def __procIDs__(self):
        id_keys=self.ids.GetIDs()
        for k in id_keys:
            if self.keepGoing==False:
                return
            node=self.ids.GetId(k)[1]
            bFound=True
            sTag=None
            sHier=None
            sTagName=None
            sName=None
            self.iAct+=1
            wx.PostEvent(self.tree,wxThreadFindElements(1,self.iAct))
            if self.sType:
                sTag=self.tree.doc.getTagName(node)
                if sTag!=self.sType:
                    bFound=False
            if bFound and self.sFltHier:
                sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
                if fnmatch.fnmatch(sHier,self.sFltHier)==False:
                    bFound=False
            if bFound and self.sFltTag:
                sTagName=self.tree.doc.getNodeText(node,'tag')
                if fnmatch.fnmatch(sTagName,self.sFltTag)==False:
                    bFound=False
            if bFound and self.sFltName:
                sName=self.tree.doc.getNodeText(node,'name')
                if fnmatch.fnmatch(sName,self.sFltName)==False:
                    bFound=False
            if bFound:
                self.iFound+=1
                if sTag is None:
                    sTag=self.tree.doc.getTagName(node)
                if sHier is None:
                    sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
                if sTagName is None:
                    sTagName=self.tree.doc.getNodeText(node,'tag')
                if sName is None:
                    sName=self.tree.doc.getNodeText(node,'name')
                try:
                    lst=self.dElem[sTag]
                    lst.append((sHier,sTagName,sName,node))
                except:
                    self.dElem[sTag]=[(sHier,sTagName,sName,node)]
            self.iCount+=1
    
    def __showNodes__(self):
        self.tree.lstItem.DeleteAllItems()
        keys=self.dElem.keys()
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
        for k in keys:
            lst=self.dElem[k]
            for tup in lst:
                self.iAct+=1
                self.lstSort.append((tup[self.tree.iCol],tup))
                wx.PostEvent(self.tree,wxThreadFindElements(2,self.iAct,self.iFound))
                if self.keepGoing==False:
                    return
        def cmpFunc(a,b):
            if self.tree.bOrder:
                return cmp(a[0],b[0])
            else:
                return cmp(b[0],a[0])
        self.lstSort.sort(cmpFunc)
        for it in self.lstSort:
            tup=it[1]
            index = self.tree.lstItem.InsertImageStringItem(sys.maxint, tup[0], -1)
            self.tree.lstItem.SetStringItem(index,1,tup[1],-1)
            self.tree.lstItem.SetStringItem(index,2,tup[2],-1)
            if self.tree.doc.getAttribute(tup[-1],'id')==self.id:
                self.tree.lstItem.SetItemState(index,
                        wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                self.tree.lstItem.EnsureVisible(index)
            wx.PostEvent(self.tree,wxThreadFindElements(3,index,self.iFound))
            if self.keepGoing==False:
                return
    def Run(self):
        if self.bFind:
            self.Clear()
            if self.ids is None:
                childs=self.tree.doc.getChilds(self.node)
                for c in childs:
                    self.__procNode__(c)
            else:
                self.__procIDs__()
        if self.keepGoing:
            self.__showNodes__()
        if self.keepGoing==False:
            self.tree.lstItem.DeleteAllItems()
        self.keepGoing = self.running = False
        wx.PostEvent(self.tree,wxThreadFindElements(0,0))
        wx.PostEvent(self.tree,wxThreadFindElementsFinished())
    def GetCount(self):
        return self.iCount
    def GetFound(self):
        return self.iFound
    def GetFoundElements(self):
        return self.dElem
    def GetNode(self,idx):
        if self.lstSort is None:
            return None
        try:
            return self.lstSort[idx][1][3]
        except:
            return None
    
[wxID_VBROWSEWITHATTRDIALOG, wxID_VBROWSEWITHATTRDIALOGCBAPPLY, 
 wxID_VBROWSEWITHATTRDIALOGCBCALC, wxID_VBROWSEWITHATTRDIALOGCBCANCEL, 
 wxID_VBROWSEWITHATTRDIALOGCBFIND, wxID_VBROWSEWITHATTRDIALOGCHCDEFAULT, 
 wxID_VBROWSEWITHATTRDIALOGCHCELEMENT, wxID_VBROWSEWITHATTRDIALOGCHCTYPE, 
 wxID_VBROWSEWITHATTRDIALOGGPROGRESS, wxID_VBROWSEWITHATTRDIALOGGSTEP, 
 wxID_VBROWSEWITHATTRDIALOGLBLCOUNT, wxID_VBROWSEWITHATTRDIALOGLBLFILTER, 
 wxID_VBROWSEWITHATTRDIALOGLBLFORM, wxID_VBROWSEWITHATTRDIALOGLBLRES, 
 wxID_VBROWSEWITHATTRDIALOGLBLTYPE, wxID_VBROWSEWITHATTRDIALOGLSTATTR, 
 wxID_VBROWSEWITHATTRDIALOGLSTITEM, wxID_VBROWSEWITHATTRDIALOGTXTCOUNT, 
 wxID_VBROWSEWITHATTRDIALOGTXTFILTERHIER, 
 wxID_VBROWSEWITHATTRDIALOGTXTFILTERNAME, 
 wxID_VBROWSEWITHATTRDIALOGTXTFILTERTAG, 
 wxID_VBROWSEWITHATTRDIALOGTXTFORMULAR, 
 wxID_VBROWSEWITHATTRDIALOGTXTNODECOUNT, wxID_VBROWSEWITHATTRDIALOGTXTRESULT, 
] = [wx.NewId() for _init_ctrls in range(24)]

class vBrowseWithAttrDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VBROWSEWITHATTRDIALOG,
              name=u'vBrowseWithAttrDialog', parent=prnt, pos=wx.Point(318, 76),
              size=wx.Size(400, 470), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vBrowse with Attributes Dialog')
        self.SetClientSize(wx.Size(392, 443))

        self.chcElement = wx.Choice(choices=[],
              id=wxID_VBROWSEWITHATTRDIALOGCHCELEMENT, name=u'chcElement',
              parent=self, pos=wx.Point(48, 4), size=wx.Size(130, 21), style=0)
        self.chcElement.Bind(wx.EVT_CHOICE, self.OnChcElementChoice,
              id=wxID_VBROWSEWITHATTRDIALOGCHCELEMENT)

        self.chcType = wx.Choice(choices=[],
              id=wxID_VBROWSEWITHATTRDIALOGCHCTYPE, name=u'chcType',
              parent=self, pos=wx.Point(200, 4), size=wx.Size(184, 21),
              style=0)
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VBROWSEWITHATTRDIALOGCHCTYPE)

        self.txtFilterHier = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTFILTERHIER,
              name=u'txtFilterHier', parent=self, pos=wx.Point(48, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.txtFilterTag = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTFILTERTAG,
              name=u'txtFilterTag', parent=self, pos=wx.Point(128, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.txtFilterName = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTFILTERNAME,
              name=u'txtFilterName', parent=self, pos=wx.Point(208, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.cbFind = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEWITHATTRDIALOGCBFIND,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Find', name=u'cbFind',
              parent=self, pos=wx.Point(304, 32), size=wx.Size(76, 30),
              style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VBROWSEWITHATTRDIALOGCBFIND)

        self.lstItem = wx.ListCtrl(id=wxID_VBROWSEWITHATTRDIALOGLSTITEM,
              name=u'lstItem', parent=self, pos=wx.Point(8, 88),
              size=wx.Size(376, 136), style=wx.LC_REPORT)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstItemListItemDeselected,
              id=wxID_VBROWSEWITHATTRDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstItemListItemSelected,
              id=wxID_VBROWSEWITHATTRDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstItemListColClick,
              id=wxID_VBROWSEWITHATTRDIALOGLSTITEM)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEWITHATTRDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(112, 408), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VBROWSEWITHATTRDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEWITHATTRDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(216, 408), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VBROWSEWITHATTRDIALOGCBCANCEL)

        self.txtNodeCount = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTNODECOUNT,
              name=u'txtNodeCount', parent=self, pos=wx.Point(48, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.txtCount = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTCOUNT,
              name=u'txtCount', parent=self, pos=wx.Point(128, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.lblCount = wx.StaticText(id=wxID_VBROWSEWITHATTRDIALOGLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(8,
              68), size=wx.Size(28, 13), style=0)

        self.lblFilter = wx.StaticText(id=wxID_VBROWSEWITHATTRDIALOGLBLFILTER,
              label=u'Filter', name=u'lblFilter', parent=self, pos=wx.Point(8,
              40), size=wx.Size(22, 13), style=0)

        self.lblType = wx.StaticText(id=wxID_VBROWSEWITHATTRDIALOGLBLTYPE,
              label=u'Type', name=u'lblType', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(24, 13), style=0)

        self.gProgress = wx.Gauge(id=wxID_VBROWSEWITHATTRDIALOGGPROGRESS,
              name=u'gProgress', parent=self, pos=wx.Point(254, 64), range=100,
              size=wx.Size(130, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.gStep = wx.Gauge(id=wxID_VBROWSEWITHATTRDIALOGGSTEP, name=u'gStep',
              parent=self, pos=wx.Point(208, 64), range=100, size=wx.Size(38,
              18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.lstAttr = wx.ListCtrl(id=wxID_VBROWSEWITHATTRDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(376, 80), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VBROWSEWITHATTRDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VBROWSEWITHATTRDIALOGLSTATTR)

        self.chcDefault = wx.Choice(choices=[u'--', u'original value',
              u'first result', 'slicing', u'next processing', u'+',
              u'relative hierarchy', u'hierarchy to relative',
              u'absolute hierarchy', u'absolute hierarchy with base'],
              id=wxID_VBROWSEWITHATTRDIALOGCHCDEFAULT, name=u'chcDefault',
              parent=self, pos=wx.Point(56, 320), size=wx.Size(180, 21),
              style=0)
        self.chcDefault.Bind(wx.EVT_CHOICE, self.OnChcDefaultChoice,
              id=wxID_VBROWSEWITHATTRDIALOGCHCDEFAULT)

        self.txtFormular = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTFORMULAR,
              name=u'txtFormular', parent=self, pos=wx.Point(56, 348),
              size=wx.Size(232, 21), style=0, value=u'')
        self.txtFormular.Bind(wx.EVT_TEXT_ENTER, self.OnTxtFormularTextEnter,
              id=wxID_VBROWSEWITHATTRDIALOGTXTFORMULAR)

        self.lblForm = wx.StaticText(id=wxID_VBROWSEWITHATTRDIALOGLBLFORM,
              label=u'Formular', name=u'lblForm', parent=self, pos=wx.Point(8,
              352), size=wx.Size(40, 13), style=0)

        self.cbCalc = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEWITHATTRDIALOGCBCALC,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Calc', name=u'cbCalc',
              parent=self, pos=wx.Point(304, 344), size=wx.Size(76, 30),
              style=0)
        self.cbCalc.Bind(wx.EVT_BUTTON, self.OnCbCalcButton,
              id=wxID_VBROWSEWITHATTRDIALOGCBCALC)

        self.lblRes = wx.StaticText(id=wxID_VBROWSEWITHATTRDIALOGLBLRES,
              label=u'Result', name=u'lblRes', parent=self, pos=wx.Point(16,
              384), size=wx.Size(30, 13), style=0)

        self.txtResult = wx.TextCtrl(id=wxID_VBROWSEWITHATTRDIALOGTXTRESULT,
              name=u'txtResult', parent=self, pos=wx.Point(56, 380),
              size=wx.Size(328, 21), style=wx.TE_READONLY, value=u'')

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.node=None
        self.doc=None
        self.vgpTree=None
        self.id=''
        self.selElem=-1
        self.selItem=-1
        self.thdFind=thdFind(self)
        EVT_THREAD_FIND_ELEMENTS_FINISHED(self,self.OnFindFin)
        EVT_THREAD_FIND_ELEMENTS(self,self.OnFindProgress)
        
        img=images.getApplyBitmap()
        self.cbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
        img=images.getFindBitmap()
        self.cbFind.SetBitmapLabel(img)
        
        img=images.getCalcBitmap()
        self.cbCalc.SetBitmapLabel(img)
        
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,180)
        self.lstItem.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,85)
        self.lstItem.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,90)
        
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,80)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_LEFT,145)
        
        self.gStep.SetRange(3)
        self.Reset()
        self.__procElementTypes__()
    def OnFindProgress(self,evt):
        self.gStep.SetValue(evt.GetStep())
        if evt.GetCount()>0:
            self.gProgress.SetRange(evt.GetCount())
        else:
            self.gProgress.SetRange(self.iSize)
        self.gProgress.SetValue(evt.GetValue())
    def OnFindFin(self,evt):
        self.txtNodeCount.SetValue(str(self.thdFind.GetCount()))
        self.txtCount.SetValue(str(self.thdFind.GetFound()))
        self.cbFind.Enable(True)
    def SetTree(self,vgpTree):
        self.vgpTree=vgpTree
    def __procElementTypes__(self):
        self.chcType.Clear()
        self.chcType.Append(u'*')
        self.chcType.Append(u'---')
        for tup in Elements.TYPES:
            self.chcType.Append(tup[0])
        self.chcType.SetSelection(3)
        self.__procElement__()
    def __procElement__(self,selElem=None):
        self.chcElement.Clear()
        self.chcElement.Append(u'*')
        sType=self.chcType.GetStringSelection()
        if sType!=u'*':
            bCheck=True
            if sType==u'---':
                bEmpty=True
            else:
                bEmpty=False
        else:
            bCheck=False
        iSel=0
        for e in Elements.ELEMENTS:
            if Elements.isProperty(e[0],'isSkip'):
                continue
            bFound=True
            if bCheck:
                if bEmpty:
                    if len(e[1]['__type'])!=0:
                        bFound=False
                else:
                    if e[1]['__type']!=sType:
                        bFound=False
            if bFound:
                self.chcElement.Append(e[1]['__desc'],e)
                if selElem is not None:
                    if selElem==e[0]:
                        iSel=self.chcElement.GetCount()-1
        self.chcElement.SetSelection(iSel)
    def Reset(self):
        self.thdFind.Clear()
        self.node=None
        self.id=''
        self.iCol=0
        self.bOrder=True
        
        self.selItem=-1
        self.lstItem.DeleteAllItems()
        self.txtNodeCount.SetValue('')
        self.txtCount.SetValue('')
    def IsThreadRunning(self):
        return self.thdFind.IsRunning()
    def SetDoc(self,doc):
        self.doc=doc
    def SetNode(self,browse,id,attr,formular):
        self.browse=browse
        self.attr=attr
        strs=string.split(self.browse,',')
        self.txtFormular.SetValue(formular)
        self.txtResult.SetValue('')
        bFind=False
        if self.id!=id:
            bFind=True
        if len(self.id)==0:
            bFind=True
        if bFind:
            self.id=id
            if len(strs)>3:
                try:
                    self.chcType.SetSelection(Elements.TYPES.index(strs[0])+1)
                except:
                    pass
                try:
                    self.__procElement__(strs[1])
                    self.txtFilterHier.SetValue(strs[2])
                    self.txtFilterTag.SetValue(strs[3])
                    self.txtFilterName.SetValue(strs[4])
                    #self.Find()
                except:
                    pass
        
    def Get(self):
        d={}
        lst=[]
        i=self.chcType.GetSelection()
        
        lst.append('*')
        i=self.chcElement.GetSelection()
        if i==0:
            lst.append('*')
        else:
            lst.append(self.chcElement.GetClientData(i)[0])
        lst.append(self.txtFilterHier.GetValue())
        lst.append(self.txtFilterTag.GetValue())
        lst.append(self.txtFilterName.GetValue())
        d['__browse']=string.join(lst,',')
        d['__fid']=self.id
        d['attr']=self.attr
        d['formular']=self.txtFormular.GetValue()
        self.OnCbCalcButton(None)
        d['val']=self.txtResult.GetValue()
        return d
    def Find(self):
        if self.chcElement.GetStringSelection()==u'*':
            sFilterType=None
        else:
            idx=self.chcElement.GetSelection()
            e=self.chcElement.GetClientData(idx)
            sFilterType=e[0]
        sFilterHier=self.txtFilterHier.GetValue()
        sFilterTag=self.txtFilterTag.GetValue()
        sFilterName=self.txtFilterName.GetValue()
        
        if sFilterHier==u'*':
            sFilterHier=None
        if sFilterTag==u'*':
            sFilterTag=None
        if sFilterName==u'*':
            sFilterName=None
        self.cbFind.Enable(False)
        if self.vgpTree is None:
            self.iSize=self.doc.GetElementAttrCount()
            ids=None
        else:
            self.iSize=self.vgpTree.GetCount()
            ids=self.vgpTree.GetIDs()
        self.txtNodeCount.SetValue(str(self.iSize))
        self.gProgress.SetRange(self.iSize)
        self.thdFind.Find(self.doc.getRoot(),ids,sFilterType,
                sFilterHier,sFilterTag,sFilterName,self.id)
            
    def OnCbCancelButton(self, event):
        if self.thdFind.IsRunning():
            self.thdFind.Stop()
        else:
            self.EndModal(0)
        event.Skip()

    def OnCbApplyButton(self, event):
        if self.thdFind.IsRunning():
            self.thdFind.Stop()
        else:
            self.EndModal(1)
        event.Skip()

    def OnLstItemListItemActivated(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnLstItemListItemDeselected(self, event):
        self.selItem=-1
        event.Skip()
    def __updateAttr__(self):
        self.lstAttr.DeleteAllItems()
        node=self.thdFind.GetNode(self.selItem)
        self.id=self.doc.getAttribute(node,'id')
        childs=self.doc.getChilds(node)
        lst=[]
        for c in childs:
            if len(self.doc.getAttribute(c,'id'))>0:
                continue
            tagName=self.doc.getTagName(c)
            if tagName[:2]=='__':
                continue
            if ELEMENTS.isProperty(tagName,'isSkip'):
                continue
            if self.doc.getChild(c,'__browse') is not None:
                continue
            if tagName in ['tag','name','descrition']:
                sVal=self.doc.getText(c)
            elif len(self.doc.getAttribute(c,'ih'))>0:
                sVal=self.doc.getText(c)
            elif len(self.doc.getAttribute(c,'ih'))>0:
                sVal=self.doc.getText(c)
            else:
                sVal=self.doc.getNodeText(c,'val')
            lst.append((tagName,sVal))
        def compFunc(a,b):
            return cmp(a[0],b[0])
        lst.sort(compFunc)
        for tup in lst:
            index = self.lstAttr.InsertImageStringItem(sys.maxint, tup[0], -1)
            self.lstAttr.SetStringItem(index,1,tup[1],-1)
            if self.attr==tup[0]:
                self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,
                                    wx.LIST_STATE_SELECTED)
                self.lstAttr.EnsureVisible(index)
    def OnLstItemListItemSelected(self, event):
        self.selItem=event.GetIndex()
        self.__updateAttr__()
        
        event.Skip()

    def OnCbFindButton(self, event):
        self.Find()
        event.Skip()

    def OnLstItemListColClick(self, event):
        col=event.GetColumn()
        if col>=0:
            if col!=self.iCol:
                self.iCol=col
                self.bOrder=True
            else:
                self.bOrder=not(self.bOrder)
            self.cbFind.Enable(False)
            self.thdFind.Show()
        event.Skip()

    def OnChcElementChoice(self, event):
        event.Skip()

    def OnChcTypeChoice(self, event):
        self.__procElement__()
        event.Skip()

    def OnLstItemLeftDclick(self, event):
        if self.selItem<0:
            return
        node=self.thdFind.GetNode(self.selItem)
        id=self.doc.getAttribute(node,'id')
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        event.Skip()

    def OnTxtFormularTextEnter(self, event):
        sRes=Hierarchy.applyCalc(self.doc,self.id,self.attr,
                self.txtFormular.GetValue())
        self.txtResult.SetValue(sRes)
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.attr=''
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        self.attr=self.lstAttr.GetItem(idx).m_text
        event.Skip()

    def OnCbCalcButton(self, event):
        sRes=Hierarchy.applyCalc(self.doc,self.id,self.attr,
                self.txtFormular.GetValue())
        self.txtResult.SetValue(sRes)
        if event is not None:
            event.Skip()

    def OnChcDefaultChoice(self, event):
        idx=self.chcDefault.GetSelection()
        if idx>0:
            sFormular=self.txtFormular.GetValue()
            s=self.chcDefault.GetStringSelection()
            sFormular+=PROCESSING[s]
            self.txtFormular.SetValue(sFormular)
            
        event.Skip()
    
