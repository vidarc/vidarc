#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------

VER_MAJOR        = 1      # The first three must match wxWidgets
VER_MINOR        = 3
VER_RELEASE      = 9
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.

DESCRIPTION      = "VIDARC PrjEng"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
MAINTAINER       = "Walter Obweger"
MAINTAINER_EMAIL = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "customized project"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "recipe"

LONG_DESCRIPTION = """\
MES module project engineering.
"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: MacOS X :: Carbon
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""
