import wx
#----------------------------------------------------------------------------
# Name:         evtDataChanged.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: evtDataChanged.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

# defined event for vgpData item selected
wxEVT_DATA_CHANGED=wx.NewEventType()
def EVT_DATA_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_DATA_CHANGED,func)
class vgpDataChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_DATA_CHANGED(<widget_name>, self.OnDataChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_DATA_CHANGED)
        self.obj=obj
        self.node=node
    def GetData(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpData item selected
wxEVT_DATA_APPLIED=wx.NewEventType()
def EVT_DATA_APPLIED(win,func):
    win.Connect(-1,-1,wxEVT_DATA_APPLIED,func)
class vgpDataApplied(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_DATA_APPLIED(<widget_name>, self.OnDataApplied)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_DATA_APPLIED)
        self.obj=obj
        self.node=node
    def GetData(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpData item selected
wxEVT_REVISION_CHANGED=wx.NewEventType()
def EVT_REVISION_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_REVISION_CHANGED,func)
class vgpRevisionChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_REVISION_CHANGED(<widget_name>, self.OnRevChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_REVISION_CHANGED)
        self.obj=obj
        self.node=node
    def GetData(self):
        return self.obj
    def GetNode(self):
        return self.node
