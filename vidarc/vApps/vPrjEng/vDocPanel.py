#Boa:FramePanel:vDocPanel
#----------------------------------------------------------------------------
# Name:         vDocPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vDocPanel.py,v 1.5 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.stc
from wx.lib.anchors import LayoutAnchors
import  wx.stc  as  stc

from vidarc.vApps.vPrjEng.vDocSelDialog  import *
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.evtDataChanged import *
#from xml.dom.minidom import Element
import sys,string,os,os.path,stat,time,shutil,codecs

import vidarc.vApps.vPrjEng.DocBuildLatex as DocBuildLatex
from vidarc.vApps.vPrjEng.vBrowseElement4DocDialog import *
from vidarc.vApps.vPrjEng.vBrowseElementAttr4DocDialog import *
from vidarc.vApps.vDraw.vDrawSFCCanvasOGL import vDrawSFCCanvasOGL
from vidarc.vApps.vDraw.vDrawS88stateCanvasOGL import vDrawS88stateCanvasOGL
from vidarc.vApps.vDraw.vDrawFlowChartCanvasOGL import vDrawFlowChartCanvasOGL
from vidarc.tool.draw.vtDrawTimingPanel import vtDrawTimingPanel
from vidarc.vApps.vPrjEng.vPrjEngCanvasPanel import vPrjEngCanvasPanel
from vidarc.vApps.vPrjEng.vFilterDialog import *

from vidarc.vApps.vPrjEng.vPrjEngRepBuildTex import *

import images

[wxID_VDOCPANEL, wxID_VDOCPANELCBZOOMIN, wxID_VDOCPANELCBZOOMOUT, 
 wxID_VDOCPANELCHCLATEX, wxID_VDOCPANELCHCREF, wxID_VDOCPANELCHCSPECIAL, 
 wxID_VDOCPANELGCBBBROWSE, wxID_VDOCPANELGCBBGENERATE, wxID_VDOCPANELGCBBOPEN, 
 wxID_VDOCPANELLBLDOCGRP, wxID_VDOCPANELSCTDATA, wxID_VDOCPANELTXTDOCGRPNAME, 
 wxID_VDOCPANELTXTDOCGRPNR, 
] = [wx.NewId() for _init_ctrls in range(13)]


PROCESSING_CMDS=[
    "browse",
    "\\add_contained",
    "\\all_logs",
    "\\all_revs",
    "\\child_attr",
    "\\childs_attr",
    "\\insert_attr",
    "\\drawing",
    "\\file_reference",
    "\\url",
    "\\headline",
    "\\hierarchy_down",
    "\\hierarchy_up",
    "\\log",
    "\\link",
    "\\link_tag",
    "\\link_name",
    "\\link_desc",
    "\\link_attr",
    "\\process_children",
    "\\process_doc",
    "\\process_PrjEngFig",
    "\\process_TimingFig",
    "\\process_FCFig",
    "\\process_FCTab",
    "\\process_S88",
    "\\process_S88Fig",
    "\\process_S88FigTab",
    "\\process_S88Tab",
    "\\process_SFCspec",
    "\\process_SFCspecFig",
    "\\process_SFCspecTab",
    "\\rev",
    "\\reference_childs_attr",
    "\\reference_doc",
    "\\reference_docfull",
    "\\reference_FCFig",
    "\\reference_FCTab",
    "\\reference_S88Fig",
    "\\reference_S88FigTab",
    "\\reference_S88Tab",
    "\\reference_SFCspecFig",
    "\\reference_SFCspecTab",
    "\\reference_PrjEngFig",
    "\\reference_TimingFig"
    ]

BROWSEABLE_CMDS=[
    "\\child_attr",
    "\\childs_attr",
    "\\insert_attr",
    "\\hierarchy_down",
    "\\hierarchy_up",
    "\\log",
    "\\link",
    "\\link_tag",
    "\\link_name",
    "\\link_desc",
    "\\link_attr",
    "\\process_doc",
    "\\process_PrjEngFig",
    "\\process_TimingFig",
    "\\process_FCFig",
    "\\process_FCTab",
    "\\process_S88",
    "\\process_S88Fig",
    "\\process_S88FigTab",
    "\\process_S88Tab",
    "\\process_SFCspec",
    "\\process_SFCspecFig",
    "\\process_SFCspecTab",
    "\\reference_childs_attr",
    "\\reference_doc",
    "\\reference_docfull",
    "\\reference_FCFig",
    "\\reference_FCTab",
    "\\reference_S88Fig",
    "\\reference_S88FigTab",
    "\\reference_S88Tab",
    "\\reference_SFCspecFig",
    "\\reference_SFCspecTab",
    "\\reference_PrjEngFig",
    "\\reference_TimingFig"]

PROCESSING_CMDS_VIS=[
    "browse",
    "\\add_contained",
    "\\all_logs",
    "\\all_revs",
    "\\child_attr[]{}",
    "\\childs_attr[]{}()",
    "\\insert_attr[]{}()",
    "\\drawing",
    "\\headline[y]{x}",
    "\\hierarchy_down[]{}",
    "\\hierarchy_up[]{}",
    "\\log[]{}",
    "\\link[]{}()",
    "\\link_tag[]{}()",
    "\\link_name[]{}()",
    "\\link_desc[]{}()",
    "\\link_attr[]{}()",
    "\\process_children",
    "\\process_doc[]{}()",
    "\\process_PrjEngFig[]{}",
    "\\process_TimingFig[]{}",
    "\\process_FCFig[]{}",
    "\\process_FCTab[]{}",
    "\\process_S88[]{}",
    "\\process_S88Fig[]{}",
    "\\process_S88FigTab[]{}",
    "\\process_S88Tab[]{}",
    "\\process_SFCspec[]{}",
    "\\process_SFCspecFig[]{}",
    "\\process_SFCspecTab[]{}",
    "\\rev[]{}",
    "\\reference_childs_attr[]{}",
    "\\reference_doc",
    "\\reference_docfull",
    "\\reference_PrjEngFig[]{}",
    "\\reference_TimingFig[]{}",
    "\\reference_FCFig[]{}",
    "\\reference_FCTab[]{}",
    "\\reference_S88Fig[]{}",
    "\\reference_S88FigTab[]{}",
    "\\reference_S88Tab[]{}",
    "\\reference_SFCspecFig[]{}",
    "\\reference_SFCspecTab[]{}"]

if wx.Platform == '__WXMSW__':
    faces = { 'times': 'Times New Roman',
              'mono' : 'Courier New',
              'helv' : 'Courier New',
              'other': 'Comic Sans MS',
              'size' : 10,
              'size2': 8,
             }
else:
    faces = { 'times': 'Times',
              'mono' : 'Courier',
              'helv' : 'Helvetica',
              'other': 'new century schoolbook',
              'size' : 12,
              'size2': 10,
             }

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

class vURLDropTarget(wx.PyDropTarget):
    def __init__(self, window):
        wx.PyDropTarget.__init__(self)
        self.window = window

        self.data = wx.URLDataObject();
        self.SetDataObject(self.data)

    def OnDragOver(self, x, y, d):
        return wx.DragLink

    def OnData(self, x, y, d):
        if not self.GetData():
            return wx.DragNone

        url = self.data.GetURL()
        self.window.AddURL(url)

        return d


class vDocPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDOCPANEL, name=u'vDocPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(600, 300),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(592, 273))
        self.SetAutoLayout(True)

        self.sctData = wx.stc.StyledTextCtrl(id=wxID_VDOCPANELSCTDATA,
              name=u'sctData', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(576, 200), style=0)
        self.sctData.SetConstraints(LayoutAnchors(self.sctData, True, True,
              True, True))
        self.sctData.SetLabel(u'Document Info')

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(320, 4),
              size=wx.Size(80, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VDOCPANELGCBBBROWSE)

        self.lblDocGrp = wx.StaticText(id=wxID_VDOCPANELLBLDOCGRP,
              label=u'Group', name=u'lblDocGrp', parent=self, pos=wx.Point(8,
              12), size=wx.Size(29, 13), style=0)

        self.txtDocGrpNr = wx.TextCtrl(id=wxID_VDOCPANELTXTDOCGRPNR,
              name=u'txtDocGrpNr', parent=self, pos=wx.Point(44, 8),
              size=wx.Size(48, 21), style=wx.TE_READONLY | wx.TE_CENTRE,
              value=u'')

        self.txtDocGrpName = wx.TextCtrl(id=wxID_VDOCPANELTXTDOCGRPNAME,
              name=u'txtDocGrpName', parent=self, pos=wx.Point(98, 8),
              size=wx.Size(208, 21), style=wx.TE_READONLY, value=u'')
        self.txtDocGrpName.SetConstraints(LayoutAnchors(self.txtDocGrpName,
              True, True, True, False))

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCPANELGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(412, 4),
              size=wx.Size(80, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VDOCPANELGCBBGENERATE)

        self.gcbbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDOCPANELGCBBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Open', name=u'gcbbOpen',
              parent=self, pos=wx.Point(504, 4), size=wx.Size(80, 30), style=0)
        self.gcbbOpen.Bind(wx.EVT_BUTTON, self.OnGcbbOpenButton,
              id=wxID_VDOCPANELGCBBOPEN)

        self.cbZoomIn = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VDOCPANELCBZOOMIN, name=u'cbZoomIn', parent=self,
              pos=wx.Point(528, 38), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbZoomIn.Bind(wx.EVT_BUTTON, self.OnCbZoomInButton,
              id=wxID_VDOCPANELCBZOOMIN)

        self.cbZoomOut = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VDOCPANELCBZOOMOUT, name=u'cbZoomOut', parent=self,
              pos=wx.Point(558, 38), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbZoomOut.Bind(wx.EVT_BUTTON, self.OnCbZoomOutButton,
              id=wxID_VDOCPANELCBZOOMOUT)

        self.chcLatex = wx.Choice(choices=[u'latex', u'new page', u'line break',
              u'paragraph', u'emphrase', u'bold', u'color', u'footnote',
              u'description', u'enumerate', u'itemize', u'longtable',
              u'quotation', u'tabular', u'verbatim', u'verbatim*', u'label',
              u'reference' , u'page reference'], id=wxID_VDOCPANELCHCLATEX,
              name=u'chcLatex', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(96, 21), style=0)
        self.chcLatex.SetConstraints(LayoutAnchors(self.chcLatex, True, True,
              False, False))
        self.chcLatex.SetSelection(0)
        self.chcLatex.Bind(wx.EVT_CHOICE, self.OnChcLatexChoice,
              id=wxID_VDOCPANELCHCLATEX)

        self.chcSpecial = wx.Choice(choices=[u'special', u'contained',
              u'all_logs', u'all_revisions', u'headline', u'child_attr',
              u'childs_attr', u'insert_attr', u'hierarchy_down',
              u'hierarchy_upw', u'logs', u'revisions', u'link', u'link tagname',
              u'link name', u'link description', u'link attribute',
              u'process_children', u'process_doc', u'FC figure' , u'FC table',
              u'S88 combined', u'S88 figure', u'S88 table', u'SFC',
              u'SFC figure', u'SFC table', u'engineering figure',
              u'Timing figure'], id=wxID_VDOCPANELCHCSPECIAL,
              name=u'chcSpecial', parent=self, pos=wx.Point(112, 40),
              size=wx.Size(96, 21), style=0)
        self.chcSpecial.SetConstraints(LayoutAnchors(self.chcSpecial, True,
              True, False, False))
        self.chcSpecial.SetSelection(0)
        self.chcSpecial.SetLabel(u'')
        self.chcSpecial.Bind(wx.EVT_CHOICE, self.OnChcSpecialChoice,
              id=wxID_VDOCPANELCHCSPECIAL)

        self.chcRef = wx.Choice(choices=[u'reference', u'child attributes',
              u'Document', u'Document full', u'FC figure' , u'FC table',
              u'S88 combined', u'S88 figure', u'S88 table', u'SFC figure',
              u'SFC table', u'engineering figure', u'Timing figure'],
              id=wxID_VDOCPANELCHCREF, name=u'chcRef', parent=self,
              pos=wx.Point(216, 40), size=wx.Size(96, 21), style=0)
        self.chcRef.SetConstraints(LayoutAnchors(self.chcRef, True, True, False,
              False))
        self.chcRef.SetSelection(0)
        self.chcRef.Bind(wx.EVT_CHOICE, self.OnChcRefChoice,
              id=wxID_VDOCPANELCHCREF)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.node=None
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs      (self.txtDocGrpNr, wx.Top, 0)
        lc.left.SameAs     (self.txtDocGrpNr, wx.Right, 2)
        lc.height.AsIs     ()
        lc.right.SameAs    (self.gcbbBrowse, wx.Left, 4)
        self.txtDocGrpName.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 4)
        lc.left.SameAs      (self.gcbbGenerate, wx.Left, -84)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbBrowse.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 4)
        lc.left.SameAs      (self.gcbbOpen, wx.Left, -84)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbGenerate.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 4)
        lc.left.SameAs      (self, wx.Right, -82)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbOpen.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.chcRef, wx.Top, -2)
        lc.left.SameAs      (self.chcRef, wx.Right, 12)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbZoomIn.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.cbZoomIn, wx.Top, 0)
        lc.left.SameAs      (self.cbZoomIn, wx.Right, 8)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbZoomOut.SetConstraints(lc);
        
        self.dlgDocSel=None
        self.dlgBrowseElement=None
        self.dlgBrowseElementAttr=None
        self.dlgFilter=None
        self.drawSFC=None
        self.drawS88state=None
        self.drawFC=None
        self.drawTiming=None
        self.vgpDrawPrjEng=None
        self.lang='en'
        self.netDocument=None
        self.netPrj=None
        self.netPrjDoc=None
        self.docFN=''
        self.prjDN=''
        
        self.texBuild=vPrjEngRepBuildTex(self)
        
        self.chcLatex.SetSelection(0)
        self.chcSpecial.SetSelection(0)
        
        self.sctData.CmdKeyAssign(ord('B'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMIN)
        self.sctData.CmdKeyAssign(ord('N'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMOUT)
        
        self.sctData.Bind(stc.EVT_STC_UPDATEUI, self.OnUpdateUI)
        self.sctData.Bind(stc.EVT_STC_MARGINCLICK, self.OnMarginClick)
        self.sctData.Bind(wx.EVT_KEY_DOWN, self.OnKeyPressed)
        self.sctData.Bind(stc.EVT_STC_DOUBLECLICK, self.OnDoubleClick)
        self.sctData.Bind(stc.EVT_STC_USERLISTSELECTION, self.OnUsrListSel)
        #self.sctData.Bind(stc.EVT_STC_MODIFIED, self.OnModified)

        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDEROPEN,    stc.STC_MARK_CIRCLEMINUS,          "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDER,        stc.STC_MARK_CIRCLEPLUS,           "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDERSUB,     stc.STC_MARK_VLINE,                "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDERTAIL,    stc.STC_MARK_LCORNERCURVE,         "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDEREND,     stc.STC_MARK_CIRCLEPLUSCONNECTED,  "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDEROPENMID, stc.STC_MARK_CIRCLEMINUSCONNECTED, "white", "#404040");
        self.sctData.MarkerDefine(stc.STC_MARKNUM_FOLDERMIDTAIL, stc.STC_MARK_TCORNERCURVE,         "white", "#404040");
        
        # Global default styles for all languages
        self.sctData.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d" % faces)
        #self.sctData.StyleClearAll()  # Reset all to be like the default
        
        dt = vFileDropTarget(self)
        self.sctData.SetDropTarget(dt)
        
        dt = vURLDropTarget(self)
        self.sctData.SetDropTarget(dt)
        
        self.dlgBrowseElement=vBrowseElement4DocDialog(self)
        self.dlgBrowseElementAttr=vBrowseElementAttr4DocDialog(self)
        self.bBrowseElem2set=True
        
        #self.__setLexer__('latex')
        self.__setLexer__('pyhton')
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getPdfBitmap()
        self.gcbbOpen.SetBitmapLabel(img)
        
        img=images.getZoomInBitmap()
        self.cbZoomIn.SetBitmapLabel(img)
        
        img=images.getZoomOutBitmap()
        self.cbZoomOut.SetBitmapLabel(img)
        
        self.bModified=False
        self.bAutoApply=False
    def Lock(self,flag):
        if flag:
            self.sctData.Enable(False)
            self.gcbbBrowse.Enable(False)
            self.txtDocGrpNr.Enable(False)
            self.txtDocGrpName.Enable(False)
            self.gcbbGenerate.Enable(False)
            self.gcbbOpen.Enable(False)
            self.chcLatex.Enable(False)
            self.chcSpecial.Enable(False)
            self.chcRef.Enable(False)
        else:
            self.sctData.Enable(True)
            self.gcbbBrowse.Enable(True)
            self.txtDocGrpNr.Enable(True)
            self.txtDocGrpName.Enable(True)
            self.gcbbGenerate.Enable(True)
            self.gcbbOpen.Enable(True)
            self.chcLatex.Enable(True)
            self.chcSpecial.Enable(True)
            self.chcRef.Enable(True)
    def __setLexer__(self,lexer):
        self.sctData.StyleClearAll()  # Reset all to be like the default
        self.sctData.SetMarginType(1, stc.STC_MARGIN_NUMBER)
        self.sctData.SetMarginWidth(1, 25)
        
        # Global default styles for all languages
        self.sctData.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d" % faces)
        self.sctData.StyleSetSpec(stc.STC_STYLE_LINENUMBER,  "back:#C0C0C0,face:%(helv)s,size:%(size2)d" % faces)
        self.sctData.StyleSetSpec(stc.STC_STYLE_CONTROLCHAR, "face:%(other)s" % faces)
        self.sctData.StyleSetSpec(stc.STC_STYLE_BRACELIGHT,  "fore:#0000FF,bold")
        self.sctData.StyleSetSpec(stc.STC_STYLE_BRACEBAD,    "fore:#FF0000,bold")
        
        #if os.linesep=='\r\n':
        #    self.sctData.SetEOLMode(stc.STC_EOL_CRLF)
        #elif os.linesep=='\n':
        #    self.sctData.SetEOLMode(stc.STC_EOL_LF)
        #elif os.linesep=='\r':
        #    self.sctData.SetEOLMode(stc.STC_EOL_CR)
        self.sctData.SetEOLMode(stc.STC_EOL_LF)
        
        if lexer=="latex":
            self.sctData.StyleSetSpec(stc.STC_TEX_DEFAULT,"fore:#000000,face:%(helv)s,size:%(size)d" % faces)
            self.sctData.StyleSetSpec(stc.STC_TEX_SPECIAL,"fore:#0000FF,face:%(helv)s,size:%(size)d" % faces)
            self.sctData.StyleSetSpec(stc.STC_TEX_GROUP,"fore:#880088,face:%(helv)s,size:%(size)d" % faces)
            self.sctData.StyleSetSpec(stc.STC_TEX_COMMAND,"fore:#AA0000,face:%(helv)s,size:%(size)d" % faces)
            self.sctData.StyleSetSpec(stc.STC_TEX_TEXT,"fore:#00AA00,face:%(helv)s,size:%(size)d" % faces)
        elif lexer=="pyhton":
            self.sctData.SetLexer(stc.STC_LEX_PYTHON)
            #self.sctData.SetKeyWords(0, " ".join(keyword.kwlist))
    
            self.sctData.SetProperty("fold", "1")
            self.sctData.SetProperty("tab.timmy.whinge.level", "1")
            self.sctData.SetMargins(0,0)
    
            self.sctData.SetViewWhiteSpace(False)
            #self.SetBufferedDraw(False)
            #self.SetViewEOL(True)
            #self.SetUseAntiAliasing(True)
            
            self.sctData.SetEdgeMode(stc.STC_EDGE_BACKGROUND)
            self.sctData.SetEdgeColumn(78)
    
            # Setup a margin to hold fold markers
            #self.SetFoldFlags(16)  ###  WHAT IS THIS VALUE?  WHAT ARE THE OTHER FLAGS?  DOES IT MATTER?
            self.sctData.SetMarginType(2, stc.STC_MARGIN_SYMBOL)
            self.sctData.SetMarginMask(2, stc.STC_MASK_FOLDERS)
            self.sctData.SetMarginSensitive(2, True)
            self.sctData.SetMarginWidth(2, 12)

            # Python styles
            # Default 
            self.sctData.StyleSetSpec(stc.STC_P_DEFAULT, "fore:#000000,face:%(helv)s,size:%(size)d" % faces)
            # Comments
            self.sctData.StyleSetSpec(stc.STC_P_COMMENTLINE, "fore:#007F00,face:%(other)s,size:%(size)d" % faces)
            # Number
            self.sctData.StyleSetSpec(stc.STC_P_NUMBER, "fore:#007F7F,size:%(size)d" % faces)
            # String
            self.sctData.StyleSetSpec(stc.STC_P_STRING, "fore:#7F007F,face:%(helv)s,size:%(size)d" % faces)
            # Single quoted string
            self.sctData.StyleSetSpec(stc.STC_P_CHARACTER, "fore:#7F007F,face:%(helv)s,size:%(size)d" % faces)
            # Keyword
            self.sctData.StyleSetSpec(stc.STC_P_WORD, "fore:#00007F,bold,size:%(size)d" % faces)
            # Triple quotes
            self.sctData.StyleSetSpec(stc.STC_P_TRIPLE, "fore:#7F0000,size:%(size)d" % faces)
            # Triple double quotes
            self.sctData.StyleSetSpec(stc.STC_P_TRIPLEDOUBLE, "fore:#7F0000,size:%(size)d" % faces)
            # Class name definition
            self.sctData.StyleSetSpec(stc.STC_P_CLASSNAME, "fore:#0000FF,bold,underline,size:%(size)d" % faces)
            # Function or method name definition
            self.sctData.StyleSetSpec(stc.STC_P_DEFNAME, "fore:#007F7F,bold,size:%(size)d" % faces)
            # Operators
            self.sctData.StyleSetSpec(stc.STC_P_OPERATOR, "bold,size:%(size)d" % faces)
            # Identifiers
            self.sctData.StyleSetSpec(stc.STC_P_IDENTIFIER, "fore:#000000,face:%(helv)s,size:%(size)d" % faces)
            # Comment-blocks
            self.sctData.StyleSetSpec(stc.STC_P_COMMENTBLOCK, "fore:#7F7F7F,size:%(size)d" % faces)
            # End of line where string is not closed
            self.sctData.StyleSetSpec(stc.STC_P_STRINGEOL, "fore:#000000,face:%(mono)s,back:#E0C0E0,eol,size:%(size)d" % faces)
    
            self.sctData.SetCaretForeground("BLACK")
            
            self.sctData.SetTabWidth(2)
        #self.ERROR_STYLE=9000
        #self.sctData.StyleSetSpec(self.ERROR_STYLE, "fore:#FF0000,italic,size:%(size)d" % faces)
    
        self.lexer=lexer
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def Clear(self):
        self.node=None
        self.__setModified__(False)
        self.sctData.Show(False)
        self.txtDocGrpNr.SetValue('')
        self.txtDocGrpName.SetValue('')
        self.sctData.ClearAll()
    def SetHumans(self,humans,humDoc):
        #self.chcUsr.Clear()
        self.humans=humans
        #for i in self.humans.GetKeys():
        #    self.chcUsr.Append(i)
    def SetNetDocHuman(self,netDoc):
        self.netDocHuman=netDoc
        #EVT_NET_XML_GOT_CONTENT(netDoc,self.OnHumanChanged)
    def SetNetDocPrj(self,netPrj):
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.netPrj=netPrj
    def SetLanguage(self,lang):
        self.lang=lang
        if self.dlgDocSel is not None:
            #self.dlgDocSel.Destroy()
            #self.dlgDocSel=None
            self.dlgDocSel.SetLanguage(self.lang)
    def SetNetDocument(self,doc):
        self.netDocument=doc
        self.texBuild.SetDocs(self.netDocument,self.netPrjDoc)
    def SetNetDocPrjDoc(self,doc):
        self.netPrjDoc=doc
        self.texBuild.SetDocs(self.netDocument,self.netPrjDoc)
    def SetDocFN(self,fn):
        self.docFN=fn
        if self.dlgDocSel is not None:
            self.dlgDocSel.SetDocFN(self.docFN)
    def SetPrjDN(self,dn):
        self.prjDN=dn
    def SetDoc(self,doc,bNet=False):
        #if self.doc is not None:
        #    self.doc.DelConsumer(self)
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        #self.doc.AddConsumer(self,vgtrXmlPrjEng.Clear)
        
        self.dlgBrowseElement.SetDoc(doc,bNet)
        self.dlgBrowseElementAttr.SetDoc(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,name,node,slient=False):
        if self.doc is None:
            return
        if self.bModified==True:
            if self.bAutoApply:
                self.__apply__()
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.__apply__()
        if self.bBrowseElem2set==True:
            if self.dlgBrowseElement is not None:
                #browseNode=vtXmlDomTree.getChild(doc.documentElement,'prjengs')
                #browseNode=vtXmlDomTree.getChild(browseNode,'instance')
                browseNode=Hierarchy.getRootNode(self.doc,node)
                self.dlgBrowseElement.SetNode(browseNode)
                self.bBrowseElem2set=False
            if self.dlgBrowseElementAttr is not None:
                #browseNode=vtXmlDomTree.getChild(doc.documentElement,'prjengs')
                #browseNode=vtXmlDomTree.getChild(browseNode,'instance')
                browseNode=Hierarchy.getRootNode(self.doc,node)
                self.dlgBrowseElementAttr.SetNode(browseNode)
                self.bBrowseElem2set=False
        relNode=Hierarchy.getRelBaseNode(self.doc,node)
        if self.dlgBrowseElement is not None:
            self.dlgBrowseElement.SetRelNode(relNode)
        if self.dlgBrowseElementAttr is not None:
            self.dlgBrowseElementAttr.SetRelNode(relNode)
        self.__setModified__(False)
        #vtLog.CallStack('')
        self.node=node
        self.name=name
        self.txtDocGrpNr.SetValue('')
        self.txtDocGrpName.SetValue('')
        self.sctData.Clear()
        bFound=False
        if self.node is not None:
            if self.doc.getTagName(self.node)=='Doc':
                bFound=True
                id=self.doc.getAttribute(node,'fid')
                if self.doc.IsKeyValid(id):
                    self.txtDocGrpNr.SetValue(id)
                self.txtDocGrpName.SetValue(self.doc.getNodeTextLang(node,
                                    '__title',self.lang))
                self.sctData.SetText(self.doc.getNodeText(self.node,'__data'))
                #if os.linesep=='\r\n':
                #    self.sctData.ConvertEOLs(stc.STC_EOL_CRLF)
                #elif os.linesep=='\n':
                #    self.sctData.ConvertEOLs(stc.STC_EOL_LF)
                #elif os.linesep=='\r':
                #    self.sctData.ConvertEOLs(stc.STC_EOL_CR)
        if bFound:
            self.sctData.Show(True)
        else:
            #self.sctData.Show(False)
            self.Clear()
        self.__setModified__(False)
        self.__updateLinks__(slient)
    def GetNode(self):
        self.Apply()
        pass
    def __apply__(self):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if self.node is not None:
            sText=self.sctData.GetText()
            #vtLog.vtLogCallDepth(None,sText,verbose=1)
            self.doc.setNodeText(self.node,'__data',sText)
            self.doc.AlignNode(self.node)
        self.__setModified__(False)
    def Apply(self,bDoApply=False):
        #print 'modified',self.bModified,bDoApply
        if self.bModified==True:
            if self.bAutoApply or bDoApply:
                self.__apply__()
                return True
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Document'+u'\n\nDo you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.__apply__()
                    return True
        return False
    def Cancel(self):
        self.__setModified__(False)
    def __getDocElementInfos__(self,iPos):
        d={'[':-1,']':-1,'{':-1,'}':-2,'(':-1,')':-2,'\n':-3,'\r':-3}
        iMax=self.sctData.GetLength()
        def __getValues__():
            if d['[']<d[']']:
                s1=self.sctData.GetTextRange(d['[']+1,d[']'])
            else:
                s1=''
            if d['{']<d['}']:
                s2=self.sctData.GetTextRange(d['{']+1,d['}'])
            else:
                s2=''
            if d['(']<d[')']:
                s3=self.sctData.GetTextRange(d['(']+1,d[')'])
            else:
                s3=''
            return (s1,s2,s3,d)
        while iPos<iMax:
            c=chr(self.sctData.GetCharAt(iPos))
            #cn=chr(self.sctData.GetCharAt(iPos+1))
            if d.has_key(c):
                if d[c]==-3:
                    return __getValues__()
                elif d[c]==-2:
                    cn=chr(self.sctData.GetCharAt(iPos+1))
                    try:
                        val=d[cn]
                        if val<-2:
                            d[c]=iPos
                            return __getValues__()
                        d[c]=iPos
                        iPos+=1
                        continue
                    except:
                        d[c]=iPos
                        return __getValues__()
                    #cn=chr(self.sctData.GetCharAt(iPos+2))
                    #print 'new char',iPos+2,cn
                    #try:
                    #    val=d[cn]
                    #    if val<-2:
                    #        return __getValues__()
                    #except:
                    #    pass
                        #return __getValues__()
                    d[c]=iPos
                elif d[c]<0:
                    d[c]=iPos
                    #if (c=='}') and (cn!='('):
                    #    return __getValues__()
                    #elif c==')':
                    #    return __getValues__()
            iPos+=1
        if d.has_key(c):
            return __getValues__()
        else:
            return ('','','',d)
    def AddFile(self,fn):
        self.sctData.AddText(u'\\file_reference{%s}'%fn)
        self.sctData.SetFocus()
        self.__setModified__(True)
    def AddURL(self,url):
        self.sctData.AddText(u'\\url{%s}'%url)
        self.sctData.SetFocus()
        self.__setModified__(True)
    def OnDoubleClick(self,evnt):
        sVal=self.sctData.GetTextRange(self.sctData.GetSelectionStart()-1,
                        self.sctData.GetSelectionEnd())
        bBrowse=False
        bBrowseAttr=False
        if sVal in BROWSEABLE_CMDS:
            if sVal in [u'\\childs_attr']:
                bBrowseAttr=True
            else:
                bBrowse=True
        if bBrowse:
            self.__doBrowseCmd__()
        if bBrowseAttr:
            self.__doBrowseAttrCmd__()
    def __doBrowseCmd__(self):
        iPos=self.sctData.GetCurrentPos()
        tup=self.__getDocElementInfos__(iPos)
        if self.dlgBrowseElement is None:
            self.dlgBrowseElement=vBrowseElement4DocDialog(self)
        self.dlgBrowseElement.Centre()
        if len(tup[0])>0:
            self.dlgBrowseElement.SetDocElementInfo(tup[0])
        else:
            try:
                parNode=self.doc.getParent(self.node)
                parNode=self.doc.getParent(parNode)
                id=self.doc.getAttribute(parNode,'id')
            except:
                id=''
            self.dlgBrowseElement.SetDocElementInfo(id)
        ret=self.dlgBrowseElement.ShowModal()
        if ret==1:
            tupInfos=self.dlgBrowseElement.GetDocElementInfo()
            iEnd=tup[-1]['}']
            if iEnd<0:
                iEnd=self.sctData.GetLength()
            else:
                iEnd+=1
            self.sctData.SetTargetStart(iPos)
            self.sctData.SetTargetEnd(iEnd)
            self.sctData.ReplaceTarget(u'[%s]{%s}'%tupInfos)
            self.__setModified__(True)
    def __doBrowseAttrCmd__(self):
        iPos=self.sctData.GetCurrentPos()
        tup=self.__getDocElementInfos__(iPos)
        if self.dlgBrowseElementAttr is None:
            self.dlgBrowseElementAttr=vBrowseElementAttr4DocDialog(self)
        self.dlgBrowseElementAttr.Centre()
        try:
            name=string.split(tup[1],'|')[1]
        except:
            name=''
        if len(tup[0])>0:
            self.dlgBrowseElementAttr.SetDocElementInfo(name,tup[0],tup[2])
        else:
            try:
                parNode=self.doc.getParent(self.node)
                parNode=self.doc.getParent(parNode)
                id=self.doc.getAttribute(parNode,'id')
            except:
                id=''
            self.dlgBrowseElementAttr.SetDocElementInfo(name,id,tup[2])
        ret=self.dlgBrowseElementAttr.ShowModal()
        if ret==1:
            tupInfos=self.dlgBrowseElementAttr.GetDocElementInfo()
            iEnd=tup[-1][')']
            if iEnd<0:
                iEnd=self.sctData.GetLength()
            else:
                iEnd+=1
            self.sctData.SetTargetStart(iPos)
            self.sctData.SetTargetEnd(iEnd)
            self.sctData.ReplaceTarget(u'[%s]{%s}(%s)'%tupInfos)
            self.__setModified__(True)
    def __updateLinks__(self,slient=False):
        if self.node is None:
            return
        if self.doc is None:
            return
        rootNode=Hierarchy.getRootNode(self.doc,self.node)
        iPos=0
        relNode=Hierarchy.getRelBaseNode(self.doc,self.node)
        lstMissingID=[]
        while iPos<self.sctData.GetLength():
            c=chr(self.sctData.GetCharAt(iPos))
            if c==u'\\':
                tup=self.__getDocElementInfos__(iPos)
                if tup[0]!='' and tup[1]!='':
                    d=tup[-1]
                    iEnd=d['[']
                    if iEnd>0:
                        if d['}']>0:
                            sVal=self.sctData.GetTextRange(iPos,iEnd)
                            if sVal in BROWSEABLE_CMDS:
                                tupID=self.doc.GetIdStr(tup[0])
                                if tupID[1] is None:
                                    #iPos=d['[']
                                    iEnd=d['}']
                                    if iEnd<0:
                                        iEnd=self.sctData.GetLength()
                                    else:
                                        iEnd+=1
                                    #self.sctData.StartStyling(98,0xff)
                                    #self.sctData.SetStyling(iPos,self.ERROR_STYLE)
                                    if slient==False:
                                        self.sctData.SetSelection(iPos,iEnd)
                                        dlg=wx.MessageDialog(self,u'At missing ID! Unable to update link.\n\n'
                                                u'Do you want to edit it now, and restart update after modification?',
                                                u'vgaPrjEng Document Update',
                                                wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                                        if dlg.ShowModal()==wx.ID_YES:
                                            self.sctData.SetCurrentPos(iPos+len(sVal))
                                            if d[')']>0:
                                                self.__doBrowseAttrCmd__()
                                            else:
                                                self.__doBrowseCmd__()
                                            self.__updateLinks__()
                                        else:
                                            lstMissingID.append((tup[0],tup[1]))
                                            break
                                    pass
                                else:
                                    node=tupID[1]
                                    sName=Hierarchy.getTagNames(self.doc,rootNode,
                                            relNode,node)
                                    try:
                                        sNameAdd=string.split(tup[1],'|')[1]
                                        sName += '|'+sNameAdd
                                    except:
                                        pass
                                    if sName!=tup[1]:
                                        iPos=d['[']
                                        iEnd=d['}']
                                        if iEnd<0:
                                            iEnd=self.sctData.GetLength()
                                        else:
                                            iEnd+=1
                                        self.sctData.SetTargetStart(iPos)
                                        self.sctData.SetTargetEnd(iEnd)
                                        self.sctData.ReplaceTarget(u'[%s]{%s}'%(tup[0],sName))
                                        self.__setModified__(True)
                            iPos=d['}']
            
            iPos+=1
        if len(lstMissingID):
            dlg=wx.MessageDialog(self,u'At least one ID is missing!' ,
                        u'vgaPrjEng Document Update',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                pass
            dlg.Destroy()
    def OnKeyPressed(self, event):
        if self.sctData.CallTipActive():
            self.sctData.CallTipCancel()
        key = event.KeyCode()
        self.__setModified__(True)
        if key == 32 and event.ControlDown():
            pos = self.sctData.GetCurrentPos()

            # Tips
            if event.ShiftDown():
                self.sctData.CallTipSetBackground("yellow")
                self.sctData.CallTipShow(pos, 'lots of of text: blah, blah, blah\n\n'
                                 'show some suff, maybe parameters..\n\n'
                                 'fubar(param1, param2)')
            # Code completion
            else:
                #lst = []
                #for x in range(50000):
                #    lst.append('%05d' % x)
                #st = " ".join(lst)
                #print len(st)
                #self.AutoCompShow(0, st)

                kw = []
                for cmd in PROCESSING_CMDS_VIS:
                    kw.append(cmd)
                
                kw.sort()  # Python sorts are case sensitive
                self.sctData.AutoCompSetIgnoreCase(False)  # so this needs to match
                #print keyword.kwlist
                # Images are specified with a appended "?type"
                #for i in range(len(kw)):
                #    if kw[i] in keyword.kwlist:
                #        kw[i] = kw[i] #+ "?1"

                self.sctData.AutoCompShow(0, " ".join(kw))
        else:
            event.Skip()


    def OnUpdateUI(self, evt):
        # check for matching braces
        braceAtCaret = -1
        braceOpposite = -1
        charBefore = None
        caretPos = self.sctData.GetCurrentPos()

        if caretPos > 0:
            charBefore = self.sctData.GetCharAt(caretPos - 1)
            styleBefore = self.sctData.GetStyleAt(caretPos - 1)

        # check before
        if charBefore and chr(charBefore) in "[]{}()" and styleBefore == stc.STC_P_OPERATOR:
            braceAtCaret = caretPos - 1

        # check after
        if braceAtCaret < 0:
            charAfter = self.sctData.GetCharAt(caretPos)
            styleAfter = self.sctData.GetStyleAt(caretPos)

            if charAfter and chr(charAfter) in "[]{}()" and styleAfter == stc.STC_P_OPERATOR:
                braceAtCaret = caretPos

        if braceAtCaret >= 0:
            braceOpposite = self.sctData.BraceMatch(braceAtCaret)

        if braceAtCaret != -1  and braceOpposite == -1:
            self.sctData.BraceBadLight(braceAtCaret)
        else:
            self.sctData.BraceHighlight(braceAtCaret, braceOpposite)
            #pt = self.PointFromPosition(braceOpposite)
            #self.Refresh(True, wxRect(pt.x, pt.y, 5,5))
            #print pt
            #self.Refresh(False)


    def OnMarginClick(self, evt):
        # fold and unfold as needed
        if evt.GetMargin() == 2:
            if evt.GetShift() and evt.GetControl():
                self.sctData.FoldAll()
            else:
                lineClicked = self.sctData.LineFromPosition(evt.GetPosition())

                if self.sctData.GetFoldLevel(lineClicked) & stc.STC_FOLDLEVELHEADERFLAG:
                    if evt.GetShift():
                        self.sctData.SetFoldExpanded(lineClicked, True)
                        self.sctData.Expand(lineClicked, True, True, 1)
                    elif evt.GetControl():
                        if self.sctData.GetFoldExpanded(lineClicked):
                            self.sctData.SetFoldExpanded(lineClicked, False)
                            self.sctData.Expand(lineClicked, False, True, 0)
                        else:
                            self.sctData.SetFoldExpanded(lineClicked, True)
                            self.sctData.Expand(lineClicked, True, True, 100)
                    else:
                        self.sctData.ToggleFold(lineClicked)


    def FoldAll(self):
        lineCount = self.sctData.GetLineCount()
        expanding = True

        # find out if we are folding or unfolding
        for lineNum in range(lineCount):
            if self.sctData.GetFoldLevel(lineNum) & stc.STC_FOLDLEVELHEADERFLAG:
                expanding = not self.sctData.GetFoldExpanded(lineNum)
                break;

        lineNum = 0

        while lineNum < lineCount:
            level = self.sctData.GetFoldLevel(lineNum)
            if level & stc.STC_FOLDLEVELHEADERFLAG and \
               (level & stc.STC_FOLDLEVELNUMBERMASK) == stc.STC_FOLDLEVELBASE:

                if expanding:
                    self.sctData.SetFoldExpanded(lineNum, True)
                    lineNum = self.sctData.Expand(lineNum, True)
                    lineNum = lineNum - 1
                else:
                    lastChild = self.sctData.GetLastChild(lineNum, -1)
                    self.sctData.SetFoldExpanded(lineNum, False)

                    if lastChild > lineNum:
                        self.sctData.HideLines(lineNum+1, lastChild)

            lineNum = lineNum + 1



    def Expand(self, line, doExpand, force=False, visLevels=0, level=-1):
        lastChild = self.sctData.GetLastChild(line, level)
        line = line + 1

        while line <= lastChild:
            if force:
                if visLevels > 0:
                    self.sctData.ShowLines(line, line)
                else:
                    self.sctData.HideLines(line, line)
            else:
                if doExpand:
                    self.sctData.ShowLines(line, line)

            if level == -1:
                level = self.sctData.GetFoldLevel(line)

            if level & stc.STC_FOLDLEVELHEADERFLAG:
                if force:
                    if visLevels > 1:
                        self.sctData.SetFoldExpanded(line, True)
                    else:
                        self.sctData.SetFoldExpanded(line, False)

                    line = self.sctData.Expand(line, doExpand, force, visLevels-1)

                else:
                    if doExpand and self.sctData.GetFoldExpanded(line):
                        line = self.sctData.Expand(line, True, force, visLevels-1)
                    else:
                        line = self.sctData.Expand(line, False, force, visLevels-1)
            else:
                line = line + 1;

        return line

    def OnUsrListSel(self,evt):
        print 'OnUsrListSel'
    def OnModified(self,evt):
        self.__setModified__(True)
    def OnGcbbBrowseButton(self, event):
        if self.dlgDocSel is None:
            self.dlgDocSel=vDocSelDialog(self)
            self.dlgDocSel.SetLanguage(self.lang)
            self.dlgDocSel.SetDoc(self.netDocument,True)
            #self.dlgDocSel.SetDocFN(self.docFN)
        
        self.dlgDocSel.Centre()
        ret=self.dlgDocSel.ShowModal()
        if ret==1:
            node=self.dlgDocSel.GetSelNode()
            if node is not None:
                self.txtDocGrpNr.SetValue(self.doc.getAttribute(node,'id'))
                self.txtDocGrpName.SetValue(self.doc.getNodeTextLang(node,
                                    'title',self.lang))
                #if len(self.docFN)>0:
                #    self.doc.setAttribute(self.node,'fn',self.docFN)
                self.doc.setAttribute(self.node,'fid',self.doc.getAttribute(node,'id'))
                for c in self.doc.getChilds(self.node,'__title'):
                    self.doc.deleteNode(c,self.node)
                for c in self.doc.getChilds(node,'title'):
                    child=self.doc.cloneNode(c,True)
                    self.doc.setTagName(child,'__title')
                    self.doc.appendChild(self.node,child)
                    
                lst=self.dlgDocSel.GetDocPath()
                sDocPath=string.join(lst,'/')
                self.doc.setNodeText(self.node,'__docpath',sDocPath)
                
                self.doc.AlignNode(self.node)
                
                self.__setModified__(True)
                wx.PostEvent(self,vgpDataChanged(self,self.node))
            pass
        event.Skip()

    def OnGcbbGenerateButton(self, event):
        if self.node is None:
            dlg=wx.MessageDialog(self,u'No document active!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        sRev=self.doc.getNodeText(self.node,'__rev')
        sPrevRev=self.doc.getNodeText(self.node,'__prev_rev')
        if len(sRev)==0:
            dlg=wx.MessageDialog(self,u'No revision defined!\n\nGo to Safe view and set value to current revision field.' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.Apply()
        sDocGrp=self.doc.getAttribute(self.node,'fid')
        if self.netDocument is not None:
            docNode=self.netDocument.getNodeById(sDocGrp)
        else:
            docNode=None
        sDocGrpTitle=self.doc.getNodeTextAttr(self.node,'__title','language',self.lang)
        sTitle=self.doc.getNodeText(self.node,'name')
        sDocTitle=sTitle
        def __genFNstr__(str):
            strs=string.split(str,' ')
            strs=map(string.capitalize,strs)
            str=string.join(strs,'')
            str=fnUtil.replaceSuspectChars(str)
            return str
        sTitle=__genFNstr__(sTitle)
        
        iRes,sFN=self.texBuild.GetFileName(
                sTitle,
                sRev,
                docNode,
                True,
                u'vRepBuildAppl',self)
        self.texBuild.SetReplaceInfo('doc_title',sDocTitle)
        self.texBuild.SetReplaceInfo('author','WRO')
        self.texBuild.SetReplaceInfo('author_company','VID')
        self.texBuild.SetReplaceInfo('reviewer','')
        self.texBuild.SetReplaceInfo('review_company','')
        self.texBuild.SetReplaceInfo('review_date','')
        self.texBuild.SetReplaceInfo('appoved_by','')
        self.texBuild.SetReplaceInfo('approve_company','')
        self.texBuild.SetReplaceInfo('approve_date','')
        self.texBuild.SetReplaceInfo('replace_revision',sPrevRev)
        self.doc.AlignNode(self.node)
        
        self.doc.setNodeText(self.node,'__fn',os.path.split(sFN)[1])
        sRelDN=os.path.split(self.texBuild.GetRelativeFN())[0]
        self.doc.setNodeText(self.node,'__docpath',sRelDN)
        try:
            sWater=self.doc.getNodeText(
                        self.doc.getChild(self.node,'watermark'),'val')
        except:
            sWater=''
        self.texBuild.SetReplaceInfo('watermark',sWater)
        try:
            sState=self.doc.getNodeText(
                        self.doc.getChild(self.node,'state'),'val')
        except:
            sState=''
        self.texBuild.SetReplaceInfo('doc_state',sState)
        
        if self.dlgFilter is None:
            self.dlgFilter=vFilterDialog(self)
            self.dlgFilter.SetDoc(self.doc)
        if self.drawSFC is None:
            self.drawSFC=vDrawSFCCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawSFC.SetDoc(self.doc,False)
            self.drawSFC.Show(False)
        if self.drawS88state is None:
            self.drawS88state=vDrawS88stateCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawS88state.SetDoc(self.doc,False)
            self.drawS88state.Show(False)
        if self.drawFC is None:
            self.drawFC=vDrawFlowChartCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawFC.SetDoc(self.doc,False)
            self.drawFC.Show(False)
        if self.drawTiming is None:
            self.drawTiming=vtDrawTimingPanel(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawTiming.SetDoc(self.doc,False)
            self.drawTiming.Show(False)
        if self.vgpDrawPrjEng is None:
            self.vgpDrawPrjEng=vPrjEngCanvasPanel(id=wx.NewId(),pos=wx.Point(4,4),
                    size=wx.Size(584,365),parent=self,bTex=True)
            self.vgpDrawPrjEng.SetDoc(self.doc,False)
            self.vgpDrawPrjEng.Show(False)
        genObj={'drawSFC':self.drawSFC,'ids':self.doc.getIds(),'doc':self.doc}
        genObj['drawS88state']=self.drawS88state
        genObj['drawFC']=self.drawFC
        genObj['drawTiming']=self.drawTiming
        genObj['drawPrjEng']=self.vgpDrawPrjEng
        genObj['dlgFilter']=self.dlgFilter
        genObj['DocumentGroupNumber']=sDocGrp
        genObj['prjDN']=self.prjDN
        self.texBuild.SetNode(self.node)
        self.texBuild.SetGenObj(genObj)
        #vtLog.CallStack('')
        #print self.texBuild.dictRepl
        try:
            sDir=os.path.split(sFN)
            os.makedirs(sDir[0])
        except:
            pass
        
        self.texBuild.BuildGui(PROCESSING_CMDS)
        
    def OnGcbbGenerateButtonOld(self, event):
        if self.node is None:
            dlg=wx.MessageDialog(self,u'No document active!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        sRev=self.doc.getNodeText(self.node,'__rev')
        sPrevRev=self.doc.getNodeText(self.node,'__prev_rev')
        if len(sRev)==0:
            dlg=wx.MessageDialog(self,u'No revision defined!\n\nGo to Safe view and set value to current revision field.' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.Apply()
        
        sDocGrp=self.doc.getAttribute(self.node,'fid')
        if len(sDocGrp)==0:
            dlg=wx.MessageDialog(self,u'No document group selected!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(self.prjDN)==0:
            dlg=wx.MessageDialog(self,u'No project path defined!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        sDocPath=string.strip(self.doc.getNodeText(self.node,'__docpath'))
        sDocGrpTitle=self.doc.getNodeTextAttr(self.node,'__title','language',self.lang)
        sTitle=self.doc.getNodeText(self.node,'name')
        sDocTitle=sTitle
        def __genFNstr__(str):
            strs=string.split(str,' ')
            strs=map(string.capitalize,strs)
            str=string.join(strs,'')
            str=fnUtil.replaceSuspectChars(str)
            return str
        sTitle=__genFNstr__(sTitle)
        
        #sTitleLst=string.split(sTitle,' ')
        #sTitle=''
        #for t in sTitleLst:
        #    sTitle+=string.capitalize(t)
        sDateCreate=self.doc.getNodeText(self.node,'__createdate')
        if len(sDateCreate)==0:
            dt=wx.DateTime.Today()
            sDateCreate=dt.FormatISODate()
            self.doc.setNodeText(self.node,'__createdate',sDateCreate)
        sDateRevision=''#vtXmlDomTree.getNodeText(self.node,'createdate')
        
        if len(sDateRevision)==0:
            dt=wx.DateTime.Today()
            sDateRevision=dt.FormatISODate()
            #vtXmlDomTree.setNodeText(self.node,'createdate',sDateCreate)
        
        sExt='tex'
        
        engNode=self.doc.getRoot()
        sPrjClientLong=''
        sPrjClient=''
        sPrjName=''
        sPrjShortCut=''
        sPrjNr=''
        sFacility=''
        sTmplLatexFN=''
        if engNode is not None:
            if self.netPrj is not None:
                prjid=self.doc.getAttribute(self.doc.getBaseNode(),'prjfid')
                infNode=self.netPrj.GetPrj(prjid)
            else:
                infNode=None
            #infNode=self.doc.getChild(engNode,'prjinfo')
            if infNode is not None:
                sPrjClientLong=self.netPrj.GetCltLong(infNode)
                sPrjClient=self.netPrj.GetCltShort(infNode)
                sPrjName=self.netPrj.GetPrjNameLong(infNode)
                sPrjShortCut=self.netPrj.GetPrjName(infNode)
                sFacility=self.netPrj.GetFacility(infNode)
                sPrjNr=self.netPrj.GetPrjNumber(infNode)
            stgNode=self.doc.getChild(engNode,'settings')
            if stgNode is not None:
                sTmplLatexFN=string.strip(self.doc.getNodeText(stgNode,'latexfn'))
                sDir=os.path.split(sTmplLatexFN)
                sTmplLatexFN=os.path.join(sDir[0],fnUtil.replaceSuspectChars(sDir[1]))
            if len(sTmplLatexFN)==0:
                dlg=wx.MessageDialog(self,u'No latex template defined!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.Destroy()
                return
        fn=string.capitalize(sPrjClient)
        fn=fn+string.capitalize(sPrjShortCut)+'_'+sDocGrp+'_'
        fn=fn+sTitle+'_'+sRev+'.'+sExt
        self.doc.setNodeText(self.node,'__fn',fn)
        
        sFN=os.path.join(self.prjDN,sDocPath,fn)
        
        try:
            mode=os.stat(sFN)
            sInfo=sFN+"\n"
            sInfo+='\ncreated at:'+time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(mode[stat.ST_CTIME]))
            sInfo+='\nlast modified at:'+time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(mode[stat.ST_MTIME]))
            dlg=wx.MessageDialog(self,u'Do you want to overwrite existing file?\n\n'+sInfo ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_NO:
                dlg.Destroy()
                return
            dlg.Destroy()
        except:
            pass
        
        self.doc.AlignNode(self.node)
        def __replaceLatexSpecialChars__(str):
            if str is None:
                return ''
            return string.replace(str,'_','\\_')
        dictRepl={}
        dictRepl['clientlong']=__replaceLatexSpecialChars__(sPrjClientLong)
        dictRepl['client']=__replaceLatexSpecialChars__(sPrjClient)
        dictRepl['project_name']=__replaceLatexSpecialChars__(sPrjName)
        dictRepl['project_shortcut']=__replaceLatexSpecialChars__(sPrjShortCut)
        dictRepl['project_number']=__replaceLatexSpecialChars__(sPrjNr)
        dictRepl['document_group_number']=__replaceLatexSpecialChars__(sDocGrp)
        dictRepl['title']=__replaceLatexSpecialChars__(sTitle)
        dictRepl['facility']=__replaceLatexSpecialChars__(sFacility)
        dictRepl['document_title']=__replaceLatexSpecialChars__(sDocGrpTitle)
        dictRepl['subdocument_title']=__replaceLatexSpecialChars__(sDocTitle)
        dictRepl['doc_name']=__replaceLatexSpecialChars__(sTitle)
        dictRepl['date_create']=sDateCreate
        dictRepl['date_revision']=sDateRevision
        dictRepl['revision']=__replaceLatexSpecialChars__(sRev)
        dictRepl['replace_revision']=__replaceLatexSpecialChars__(sPrevRev)
        dictRepl['author']='WRO'
        dictRepl['author_company']='VID'
        dictRepl['reviewer']=''
        dictRepl['review_company']=''
        dictRepl['review_date']=''
        dictRepl['appoved_by']=''
        dictRepl['approve_company']=''
        dictRepl['approve_date']=''
        try:
            sWater=self.doc.getNodeText(
                        self.doc.getChild(self.node,'watermark'),'val')
        except:
            sWater=''
        dictRepl['watermark']=sWater#self.txtWatermark.GetValue()
        try:
            sState=self.doc.getNodeText(
                        self.doc.getChild(self.node,'state'),'val')
        except:
            sState=''
        dictRepl['doc_state']=sState
        
        if self.dlgFilter is None:
            self.dlgFilter=vFilterDialog(self)
            self.dlgFilter.SetDoc(self.doc)
        if self.drawSFC is None:
            self.drawSFC=vDrawSFCCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawSFC.SetDoc(self.doc,False)
            self.drawSFC.Show(False)
        if self.drawS88state is None:
            self.drawS88state=vDrawS88stateCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawS88state.SetDoc(self.doc,False)
            self.drawS88state.Show(False)
        if self.drawFC is None:
            self.drawFC=vDrawFlowChartCanvasOGL(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawFC.SetDoc(self.doc,False)
            self.drawFC.Show(False)
        if self.drawTiming is None:
            self.drawTiming=vtDrawTimingPanel(id=wx.NewId(),pos=wx.Point(0,0),
                    size=wx.Size(0,0),parent=self)
            self.drawTiming.SetDoc(self.doc,False)
            self.drawTiming.Show(False)
        genObj={'drawSFC':self.drawSFC,'ids':self.doc.getIds(),'doc':self.doc}
        genObj['drawS88state']=self.drawS88state
        genObj['drawFC']=self.drawFC
        genObj['drawTiming']=self.drawTiming
        genObj['dlgFilter']=self.dlgFilter
        genObj['DocumentGroupNumber']=sDocGrp
        genObj['prjDN']=self.prjDN
        try:
            sDir=os.path.split(sFN)
            os.makedirs(sDir[0])
        except:
            pass
        fnUtil.shiftFile(sFN)
        e=u'ISO-8859-1'
        tmpl=codecs.open(sTmplLatexFN,'r',e)
        f=codecs.open(sFN,"w",e)
        # process output
        DocBuildLatex.generate(tmpl,f,dictRepl,self.node,PROCESSING_CMDS,genObj)
        f.close()
        tmpl.close()
        
        event.Skip()

    def OnGcbbOpenButton(self, event):
        if self.node is None:
            dlg=wx.MessageDialog(self,u'No document active!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(self.prjDN)==0:
            dlg=wx.MessageDialog(self,u'No project path defined!' ,
                        u'vgaPrjEng Elements Info',
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        
        fn=self.doc.getNodeText(self.node,'__fn')
        if len(fn)==0:
            return
        sDocPath=string.strip(self.doc.getNodeText(self.node,'__docpath'))
        
        sFN=os.path.join(self.prjDN,sDocPath,fn)
        
        sExts=string.split(sFN,'.')
        sExts[-1]='pdf'
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        #filename=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
        mime = fileType.GetMimeType()
        if mime is None:
            mime="File extension"
        vtLog.CallStack(string.join(sExts,'.'))
        cmd = fileType.GetOpenCommand(string.join(sExts,'.'), mime)
        wx.Execute(cmd)
        event.Skip()

    def OnChcWatermarkChoice(self, event):
        self.txtWatermark.SetValue(self.chcWatermark.GetStringSelection())
        event.Skip()

    def OnCbZoomInButton(self, event):
        self.sctData.ZoomIn()
        event.Skip()

    def OnCbZoomOutButton(self, event):
        self.sctData.ZoomOut()
        event.Skip()
    
    def OnChcLatexChoice(self, event):
        if event.GetSelection()>0:
            i=event.GetSelection()
            lst=[u'\\newpage',u'\\\\', 
                u'\\paragraph',
                u'\\emph{}', u'\\textbf{}',
                u'\\textcolor{blue}{}',u'\\footnote{}',
                u'\\begin{description}\n  \\item[]\n\\end{description}',
                u'\\begin{enumerate}\n  \\item \n\\end{enumerate}',
                u'\\begin{itemize}\n  \\item \n\\end{itemize}',
                u'\\begin{longtable}{|lr|}\\hline\n    & \\\\ \\hline\\endfirsthead\n    & \\\\ \\hline\\endhead\n    & \\\\ \\hline\n  \\caption{}\n  \\label{tab:}\n\\end{longtable}',
                u'\\begin{quotation}\n\n\\end{quotation}',
                u'\\begin{tabular}{|lr|}\\hline\n    & \\\\ \n\\end{tabular}',
                u'\\begin{verbatim}\n\n\\end{verbatim}',
                u'\\begin{verbatim*}\n\n\\end{verbatim*}',
                u'\\label{}',u'\\ref{}',u'\\pageref{}'
                ]
            self.sctData.AddText(lst[i-1])
            self.chcLatex.SetSelection(0)
            self.sctData.SetFocus()
            self.__setModified__(True)
            pass
        event.Skip()

    def OnChcSpecialChoice(self, event):
        if event.GetSelection()>0:
            i=event.GetSelection()
            lst=[u"\\add_contained[]{}",u'\\all_logs',u'\\all_revs',
                u'\\headline[]{}', 
                u'\\child_attr[]{}',
                u'\\childs_attr[]{}()', 
                u'\\insert_attr[]{}()',
                u'\\hierarchy_down[]{}',u'\\hierarchy_up[]{}', 
                u'\\log[]{}',u'\\rev[]{}',
                u'\\link[]{}()',
                u'\\link_tag[]{}()',u'\\link_name[]{}()',u'\\link_desc[]{}()',u'\\link_attr[]{}()',
                u'\\process_children',u'\\process_doc[]{}()',
                u'\\process_FCFig[]{}',u'\\process_FCTab[]{}',
                u'\\process_S88FigTab[]{}',u'\\process_S88Fig[]{}',u'\\process_S88Tab[]{}',
                u'\\process_SFCspec[]{}',u'\\process_SFCspecFig[]{}',u'\\process_SFCspecTab[]{}',
                u'\\process_PrjEngFig[]{}',u'\\process_TimingFig[]{}',]
            self.sctData.AddText(lst[i-1])
            self.chcSpecial.SetSelection(0)
            if i>4:
                iPos=self.sctData.GetCurrentPos()
                self.sctData.SetCurrentPos(iPos-(len(lst[i-1])-string.find(lst[i-1],'[')))
                if i in [6]:
                    self.__doBrowseAttrCmd__()
                else:
                    self.__doBrowseCmd__()
            self.sctData.SetFocus()
            self.__setModified__(True)
            pass
        event.Skip()

    def OnChcRefChoice(self, event):
        if event.GetSelection()>0:
            i=event.GetSelection()
            lst=[u'\\reference_childs_attr[]{}()', u'\\reference_doc[]{}',u'\\reference_docfull[]{}',
                u'\\reference_FCFig[]{}', u'\\reference_FCTab[]{}',
                u'\\reference_S88FigTab[]{}',u'\\reference_S88Fig[]{}',
                u'\\reference_S88Tab[]{}',
                u'\\reference_SFCspecFig[]{}',u'\\reference_SFCspecTab[]{}',
                u'\\reference_PrjEngFig[]{}',u'\\reference_TimingFig[]{}']
            self.sctData.AddText(lst[i-1])
            self.chcRef.SetSelection(0)
            if i>0:
                iPos=self.sctData.GetCurrentPos()
                self.sctData.SetCurrentPos(iPos-(len(lst[i-1])-string.find(lst[i-1],'[')))
                if i==1:
                    self.__doBrowseAttrCmd__()
                else:
                    self.__doBrowseCmd__()
            self.sctData.SetFocus()
            self.__setModified__(True)
            pass
        event.Skip()
