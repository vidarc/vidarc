#Boa:Frame:vPrjEngMainFrame
#----------------------------------------------------------------------------
# Name:         vPrjEngMainFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngMainFrame.py,v 1.6 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import wx.lib.ogl as ogl

import getpass,time


#from wxTool.time.wxDateGM import *
#from wxTool.time.wxTimeEdit import *

import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.tool.log.vtLogFileViewerFrame import *
from vidarc.vApps.vPrjEng.vNetPrjEng  import *
from vidarc.vApps.vPrjEng.vXmlPrjEngTree  import *
#from boa.vApps.vPrjDoc.vgpDocSel  import *
#from boa.vApps.vPrjDoc.vgpPrjDoc  import *
from vidarc.vApps.vPrjEng.vPrjEngSettingsDialog  import *
from vidarc.vApps.vPrjEng.vElemPanel import *
from vidarc.vApps.vPrjEng.vElemDataNoteBook import *
from vidarc.vApps.vPrjEng.evtDataChanged import *
from vidarc.vApps.vPrjEng.vViewElementsTableDialog import *
from vidarc.vApps.vPrjEng.vPrjEngElementsDialog import *
from vidarc.vApps.vPrjEng.vVsasSelDialog import *
from vidarc.vApps.vPrjEng.vElementFilterDialog import *
from vidarc.vApps.vPrjEng.vInstanceDialog import *
from vidarc.vApps.vPrjEng.vPrjEngGenTaskDialog import *
from vidarc.vApps.vPrjEng.vExportNodeDialog import *
from vidarc.vApps.vPrjEng.vImportNodeDialog import *
from vidarc.vApps.vPrjEng.vPrjEngExpEmailDialog import *
from vidarc.vApps.vPrjEng.vAnalyseGeneralDialog import *
from vidarc.vApps.vPrjEng.vFindDialog import *
from vidarc.vApps.vPrjEng.vFindActivitiesDialog import *
from vidarc.vApps.vPrjEng.vImportOfficeDialog import *
from vidarc.vApps.vPrjEng.vNetTaskExport import *

from vidarc.vApps.vDraw.vDrawS88stateCanvasOGL import EVT_VGPDRAWS88STATE_SFC_ADDED
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.net.vNetXmlGuiMaster import *
from vidarc.vApps.vPrj.vNetPrj import *
from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
from vidarc.vApps.vHum.vNetHum import *
from vidarc.vApps.vDoc.vNetDoc import *
from vidarc.vApps.vPrjEng.vNetTaskExport import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

import vidarc.tool.InOut.fnUtil as fnUtil

import images
import images_lang

def create(parent):
    return vPrjEngMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEMg_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wx.NewId(), range(4))

[wxID_VPRJENGMAINFRAME, wxID_VPRJENGMAINFRAMEPNINFO, 
 wxID_VPRJENGMAINFRAMEPNITEM, wxID_VPRJENGMAINFRAMEPNNAV, 
 wxID_VPRJENGMAINFRAMESBSTATUS, wxID_VPRJENGMAINFRAMESPWINFO, 
 wxID_VPRJENGMAINFRAMESPWMAIN, 
] = [wx.NewId() for _init_ctrls in range(7)]

[wxID_VPRJENGMAINFRAMEMNFILEITEMS_OPEN, wxID_VPRJENGMAINFRAMEMNFILEITEM_EXIT, 
 wxID_VPRJENGMAINFRAMEMNFILEITEM_NEW, 
 wxID_VPRJENGMAINFRAMEMNFILEITEM_OPEN_CFG, 
 wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE, wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVEAS, 
 wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE_CFG, 
 wxID_VPRJENGMAINFRAMEMNFILEITEM_SETTING, 
 wxID_VPRJENGMAINFRAMEMNFILEMN_FILE_SAVE_ALL, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(9)]

[wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_EMAIL, 
 wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_NODE, 
 wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_TASK, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(3)]

[wxID_VPRJENGMAINFRAMEMNTOOLSITEM_ALIGN, 
 wxID_VPRJENGMAINFRAMEMNTOOLSITEM_MNEXPORT, 
 wxID_VPRJENGMAINFRAMEMNTOOLSITEM_MN_IMPORT, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(3)]

[wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
 wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_ID_DOCGRP, 
 wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_NORMAL, 
 wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_GRP, 
 wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_SHORT_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(6)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4
VERBOSE=1


def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VPRJENGMAINFRAMEMNVIEWITEM_ELEMENT_FILTER, 
 wxID_VPRJENGMAINFRAMEMNVIEWITEM_FIND_SEARCH, 
 wxID_VPRJENGMAINFRAMEMNVIEWITEM_LANG, 
 wxID_VPRJENGMAINFRAMEMNVIEWITEM_SEARCH_ACTIVITIES, 
 wxID_VPRJENGMAINFRAMEMNVIEWITEM_VIEW_ELEMENTS, 
 wxID_VPRJENGMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(6)]

[wxID_VPRJENGMAINFRAMEMNLANGITEM_LANG_DE, 
 wxID_VPRJENGMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_NODE, 
 wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_OFFICE, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VPRJENGMAINFRAMEMNANALYSEITEM_ANALYSE_GENERAL] = [wx.NewId() for _init_coll_mnAnalyse_Items in range(1)]

[wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vPrjEngMainFrame(wx.Frame):
    def _init_coll_mnLang_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNLANGITEM_LANG_EN,
              kind=wx.ITEM_RADIO, text=u'English')
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNLANGITEM_LANG_DE,
              kind=wx.ITEM_RADIO, text=u'Deutsch')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VPRJENGMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_mnAnalyse_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNANALYSEITEM_ANALYSE_GENERAL,
              kind=wx.ITEM_NORMAL, text=u'General')
        self.Bind(wx.EVT_MENU, self.OnMnAnalyseItem_analyse_generalMenu,
              id=wxID_VPRJENGMAINFRAMEMNANALYSEITEM_ANALYSE_GENERAL)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_NEW,
              kind=wx.ITEM_NORMAL, text=_(u'&New'))
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNFILEITEMS_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'&Open'))
        parent.Append(help=u'', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_OPEN_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'  ... Config'))
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE,
              kind=wx.ITEM_NORMAL, text=_(u'&Save'))
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVEAS,
              kind=wx.ITEM_NORMAL, text=_(u'Save &As'))
        parent.Append(help=u'', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'  ... Config'))
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNFILEMN_FILE_SAVE_ALL,
              kind=wx.ITEM_NORMAL, text=_(u'Save All'))
        parent.AppendSeparator()
        parent.Append(help=u'', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SETTING,
              kind=wx.ITEM_NORMAL, text=_(u'Se&ttings'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_newMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_NEW)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_saveasMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVEAS)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_saveMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE)
        self.Bind(wx.EVT_MENU, self.OnMnFileItems_openMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEMS_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_settingMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SETTING)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_open_cfgMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_OPEN_CFG)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_save_cfgMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEITEM_SAVE_CFG)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_save_allMenu,
              id=wxID_VPRJENGMAINFRAMEMNFILEMN_FILE_SAVE_ALL)

    def _init_coll_mnExport_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_NODE,
              kind=wx.ITEM_NORMAL, text=_(u'Node'))
        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_TASK,
              kind=wx.ITEM_NORMAL, text=_(u'Task'))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_EMAIL,
              kind=wx.ITEM_NORMAL, text=_(u'eMail'))
        self.Bind(wx.EVT_MENU, self.OnMnExportItem_export_nodeMenu,
              id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_NODE)
        self.Bind(wx.EVT_MENU, self.OnMnImportItem_export_taskMenu,
              id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_TASK)
        self.Bind(wx.EVT_MENU, self.OnMnExportItem_export_emailMenu,
              id=wxID_VPRJENGMAINFRAMEMNEXPORTITEM_EXPORT_EMAIL)

    def _init_coll_mnView_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_FIND_SEARCH,
              kind=wx.ITEM_NORMAL, text=_(u'Search'))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_SEARCH_ACTIVITIES,
              kind=wx.ITEM_NORMAL, text=_(u'Activities'))
        parent.AppendMenu(help='', id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_VIEW_TREE,
              submenu=self.mnViewTree, text=_(u'Tree...'))
        parent.AppendMenu(help='', id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_LANG,
              submenu=self.mnLang, text=_(u'Language ...'))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_ELEMENT_FILTER,
              kind=wx.ITEM_NORMAL, text=_(u'Element Filter'))
        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_VIEW_ELEMENTS,
              kind=wx.ITEM_NORMAL, text=_(u'View Elements Table'))
        self.Bind(wx.EVT_MENU, self.OnMnViewItem_element_filterMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_ELEMENT_FILTER)
        self.Bind(wx.EVT_MENU, self.OnMnViewItem_view_elementsMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_VIEW_ELEMENTS)
        self.Bind(wx.EVT_MENU, self.OnMnViewItem_find_searchMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_FIND_SEARCH)
        self.Bind(wx.EVT_MENU, self.OnMnViewItem_search_activitiesMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_SEARCH_ACTIVITIES)

    def _init_coll_mnImport_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_NODE,
              kind=wx.ITEM_NORMAL, text=_(u'Node'))
        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_OFFICE,
              kind=wx.ITEM_NORMAL, text=_(u'Office'))
        self.Bind(wx.EVT_MENU, self.OnMnImportItem_import_nodeMenu,
              id=wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_NODE)
        self.Bind(wx.EVT_MENU, self.OnMnImportItem_import_officeMenu,
              id=wxID_VPRJENGMAINFRAMEMNIMPORTITEM_IMPORT_OFFICE)

    def _init_coll_mnViewTree_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP,
              kind=wx.ITEM_CHECK, text=_(u'Date User Grouping '))
        parent.Append(help=u'',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP,
              kind=wx.ITEM_CHECK, text=_(u'Short Date User Grouping '))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_GRP,
              kind=wx.ITEM_CHECK, text=_(u'User Date Grouping'))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_SHORT_GRP,
              kind=wx.ITEM_CHECK, text=_(u'User short Date Grouping'))
        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_NORMAL,
              kind=wx.ITEM_CHECK, text=_(u'Document Normal'))
        parent.Append(help='',
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_ID_DOCGRP,
              kind=wx.ITEM_CHECK, text=_(u'Document ID Group'))
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_date_usr_grpMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_usr_date_grpMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_GRP)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_NormalMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_NORMAL)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_IDDocGrpMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DOC_ID_DOCGRP)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_date_short_usr_grpMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP)
        self.Bind(wx.EVT_MENU, self.OnMnViewTreeItem_usr_date_short_grpMenu,
              id=wxID_VPRJENGMAINFRAMEMNVIEWTREEITEM_USR_DATE_SHORT_GRP)

    def _init_coll_mnTools_Items(self, parent):
        # generated method, don't edit

        parent.AppendMenu(help='', id=wxID_VPRJENGMAINFRAMEMNTOOLSITEM_MNEXPORT,
              submenu=self.mnExport, text=_(u'Export ...'))
        parent.AppendMenu(help='',
              id=wxID_VPRJENGMAINFRAMEMNTOOLSITEM_MN_IMPORT,
              submenu=self.mnImport, text=_(u'Import ...'))
        parent.AppendSeparator()
        parent.Append(help=u'', id=wxID_VPRJENGMAINFRAMEMNTOOLSITEM_ALIGN,
              kind=wx.ITEM_NORMAL, text=_(u'Align XML'))
        self.Bind(wx.EVT_MENU, self.OnMnToolsItem_alignMenu,
              id=wxID_VPRJENGMAINFRAMEMNTOOLSITEM_ALIGN)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'&View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'&Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'&Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnExport = wx.Menu(title='')

        self.mnImport = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnViewTree = wx.Menu(title=u'')

        self.mnLang = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title='')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnAnalyse_Items(self.mnAnalyse)
        self._init_coll_mnTools_Items(self.mnTools)
        self._init_coll_mnExport_Items(self.mnExport)
        self._init_coll_mnImport_Items(self.mnImport)
        self._init_coll_mnView_Items(self.mnView)
        self._init_coll_mnViewTree_Items(self.mnViewTree)
        self._init_coll_mnLang_Items(self.mnLang)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VPRJENGMAINFRAME,
              name=u'vPrjEngMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 552), style=wx.DEFAULT_FRAME_STYLE,
              title=_(u'VIDARC Project Engineering'))
        self._init_utils()
        self.SetClientSize(wx.Size(811, 525))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnVgfPrjTimerMainClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnVgfPrjEngMainActivate)

        self.spwMain = wx.SplitterWindow(id=wxID_VPRJENGMAINFRAMESPWMAIN,
              name=u'spwMain', parent=self, point=wx.Point(0, 0),
              size=wxSize(760, 363), style=wx.SP_3D|wx.SP_LIVE_UPDATE)
        self.spwMain.SetMinimumPaneSize(20)
        self.spwMain.SetAutoLayout(True)

        self.pnNav = wx.Panel(id=wxID_VPRJENGMAINFRAMEPNNAV, name=u'pnNav',
              parent=self.spwMain, pos=wx.Point(2, 2), size=wx.Size(198, 482),
              style=wx.TAB_TRAVERSAL)
        self.pnNav.SetAutoLayout(True)

        self.spwInfo = wx.SplitterWindow(id=wxID_VPRJENGMAINFRAMESPWINFO,
              name=u'spwInfo', parent=self.spwMain, point=wx.Point(127, 2),
              size=wx.Size(749, 360), style=wx.SP_3D|wx.SP_LIVE_UPDATE)
        self.spwInfo.SetMinimumPaneSize(20)
        self.spwInfo.SetAutoLayout(True)
        self.spwMain.SplitVertically(self.pnNav, self.spwInfo, 200)

        self.pnInfo = wx.Panel(id=wxID_VPRJENGMAINFRAMEPNINFO, name=u'pnInfo',
              parent=self.spwInfo, pos=wx.Point(2, 2), size=wx.Size(598, 108),
              style=wx.TAB_TRAVERSAL)

        self.pnItem = wx.Panel(id=wxID_VPRJENGMAINFRAMEPNITEM, name=u'pnItem',
              parent=self.spwInfo, pos=wx.Point(2, 117), size=wx.Size(598, 363),
              style=wx.TAB_TRAVERSAL)
        self.pnItem.SetAutoLayout(True)
        self.spwInfo.SplitHorizontally(self.pnInfo, self.pnItem, 110)

        self.sbStatus = wx.StatusBar(id=wxID_VPRJENGMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        self.bActivated=False
        
        self.bModified=False
        self.selNode=None
        self.dlgVsas=None
        self.dlgViewElementsTable=None
        self.dlgElementFilter=None
        self.dlgInstance=None
        self.dlgGenTask=None
        self.dlgExpNode=None
        self.dlgImpNode=None
        self.dlgAnalyseGen=None
        self.dlgFind=None
        self.dlgFindAct=None
        self.dlgImpOff=None
        
        self.baseDoc=None
        self.baseEng=None
        self.baseSettings=None
        self.basePrjInfo=None
        self.idPrev=''
        
        self.bAutoConnect=True
        self.bOpenCfg=False
        self.sOpenCfgFN=''
        
        self.domVsas=vtXmlDom.vtXmlDom()
        
        appls=['vPrjEng','vHum','vPrj','vDoc','vPrjDoc','vTask']
        
        self.xdCfg=vtXmlDom.vtXmlDom()
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vNetXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        self.netPrjEng=self.netMaster.AddNetControl(vNetPrjEng,'vPrjEng',synch=True,verbose=1)
        self.netTask=self.netMaster.AddNetControl(vNetTaskExport,'vTask',verbose=1)
        #self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDocuments,'vPrjDoc',verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',verbose=0)
        #self.netLoc=self.netMaster.AddNetControl(vNetLoc,'vLoc',verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',verbose=0)
            
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vgaPrjEng')
        EVT_NET_XML_MASTER_START(self.netMaster,self.OnMasterStart)
        
        EVT_NET_XML_OPEN_OK(self.netPrjEng,self.OnOpenOk)
        #EVT_NET_XML_OPEN_FAULT(self.netPrjEng,self.OnOpenFault)
        
        EVT_NET_XML_SYNCH_START(self.netPrjEng,self.OnSynchStart)
        EVT_NET_XML_SYNCH_PROC(self.netPrjEng,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netPrjEng,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netPrjEng,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netPrjEng,self.OnGotContent)
        
        self.netTask.SetPrjEngDoc(self.netPrjEng,True)
        
        #self.OpenCfgFile()
        
        
        ogl.OGLInitialize()

        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.__addLangMenu__()
        
        self.vgpTree=vXmlPrjEngTree(self.pnNav,wx.NewId(),
                pos=(0,0),size=(200,482),style=0,name="vgpTree",
                master=True,verbose=1)
        self.vgpTree.SetStoreExpand(True)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        EVT_VTXMLTREE_ITEM_EDITED(self.vgpTree,self.OnTreeItemEdit)
        EVT_VTXMLTREE_ITEM_ADDED(self.vgpTree,self.OnTreeItemAdd)
        EVT_VTXMLTREE_ITEM_DELETED(self.vgpTree,self.OnTreeItemDel)
        EVT_VTXMLTREE_ITEM_INSTANCE(self.vgpTree,self.OnTreeItemInstance)
        self.vgpTree.SetLanguages(self.languages,self.languageIds)
        self.vgpTree.EnableCache()
        
        self.vgpElem=vElemPanel(self.spwInfo,wx.NewId(),
            pos=(0,0),size=(400,400),style=0,name="vgpElem")
        self.spwInfo.ReplaceWindow(self.pnInfo,self.vgpElem)
        self.pnInfo.Destroy()
        #self.vgpElem=vgpElem(self.pnInfo,wx.NewId(),
        #    pos=(0,0),size=(400,400),style=0,name="vgpElem")
        EVT_ELEM_CHANGED(self.vgpElem,self.OnElemChanged)
        EVT_ELEM_APPLIED(self.vgpElem,self.OnElemApplied)
        EVT_ELEM_CANCELED(self.vgpElem,self.OnElemCanceled)
        self.vgpElem.SetDoc(self.netPrjEng,True)
        
        self.vgpElemData=vElemDataNoteBook(self.spwInfo,wx.NewId(),
            pos=(0,0),size=(400,400),style=wx.LB_DEFAULT,name="vgpElemData")
        self.spwInfo.ReplaceWindow(self.pnItem,self.vgpElemData)
        self.pnItem.Destroy()
        #self.vgpElemData=vglbElemData(self.pnItem,wx.NewId(),
        #    pos=(0,0),size=(400,400),style=wx.LB_DEFAULT,name="vgpElemData")
        #self.spwInfo.ReplaceWindow(self.pnItem,self.vgpElemData)
        #self.pnItem.Destroy()
        EVT_DATA_CHANGED(self.vgpElemData,self.OnElemDataChanged)
        EVT_DATA_APPLIED(self.vgpElemData,self.OnElemDataApplied)
        EVT_REVISION_CHANGED(self.vgpElemData,self.OnElemDataRevChanged)
        EVT_VGPDRAWS88STATE_SFC_ADDED(self.vgpElemData.GetS88draw(),
                self.OnSfcAdded)
        EVT_VTXMLTREE_ITEM_GOTO_TMPL(self.vgpElemData,self.OnTreeItemGotoTmpl)
        EVT_VTXMLTREE_ITEM_GOTO(self.vgpElemData,self.OnTreeItemGoto)
        
        self.vgpElemData.SetNetDocHuman(self.netHum)
        self.vgpElemData.SetNetDocument(self.netDoc)
        self.vgpElemData.SetNetDocPrj(self.netPrj)
        self.vgpElemData.SetNetDocPrjDoc(self.netPrjDoc)
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        mnItems=self.mnViewTree.GetMenuItems()
        mnItems[2].Check(True)
        mnItems[5].Check(True)
        
        mnItems=self.mnTools.GetMenuItems()
        mnItems[0].SetBitmap(images.getExportBitmap())
        mnItems[1].SetBitmap(images.getImportBitmap())
        for mnItem in mnItems:
            self.mnTools.Remove(mnItem.GetId())
        for mnItem in mnItems:
            self.mnTools.AppendItem(mnItem)
        
        mnItems=self.mnExport.GetMenuItems()
        mnItems[0].SetBitmap(images.getExportNodeBitmap())
        mnItems[1].SetBitmap(images.getTaskBitmap())
        mnItems[2].SetBitmap(images.geteMailBitmap())
        for mnItem in mnItems:
            self.mnExport.Remove(mnItem.GetId())
        for mnItem in mnItems:
            self.mnExport.AppendItem(mnItem)
        
        mnItems=self.mnImport.GetMenuItems()
        mnItems[0].SetBitmap(images.getImportNodeBitmap())
        mnItems[1].SetBitmap(images.getOfficeBitmap())
        for mnItem in mnItems:
            self.mnImport.Remove(mnItem.GetId())
        for mnItem in mnItems:
            self.mnImport.AppendItem(mnItem)
        self.__makeTitle__()
        
        #self.vgpTree.SetProcessBar(self.gProcess,self)
        self.dlgViewElementsTable=vViewElementsTableDialog(self)
        
        self.vgpTree.SetViewElementsTable(self.dlgViewElementsTable)
        #self.CreateNew()
        self.vgpTree.SetDoc(self.netPrjEng,True)
        self.vgpElemData.SetDoc(self.netPrjEng,True)
        
        wx.GetApp().SetTopWindow(self)
    def __addLangMenu__(self):
        self.mnView.Remove(wxID_VPRJENGMAINFRAMEMNVIEWITEM_LANG)
        
        self.mnLang=wx.Menu()
        self.langIds=[]
        self.languages=[u'English',u'Deutsch']
        self.languageIds=[u'en',u'de']
        for i in range(0,len(self.languages)):
            id=wx.NewId()
            self.langIds.append(id)
            item = wx.MenuItem(self.mnView,help='', id=id,
                kind=wx.ITEM_NORMAL, text=self.languages[i])
            f=getattr(images_lang, 'getLang%02dBitmap' % i)
            bmp=f()
            item.SetBitmap(bmp)
            self.mnLang.AppendItem(item)
            self.Bind(wx.EVT_MENU, self.OnLanguage, id=id)
        
        self.mnView.AppendMenu(help='', id=wxID_VPRJENGMAINFRAMEMNVIEWITEM_LANG,
              submenu=self.mnLang, text=u'Language ...')
        self.actLanguage='en'
    def SetStatusInfo(self,text):
        self.SetStatusText(text,STATUS_TEXT_POS)
    def OnLanguage(self,event):
        if event.GetId()==self.langIds[0]:
            self.actLanguage='en'
        elif event.GetId()==self.langIds[1]:
            self.actLanguage='de'
        else:
            self.actLanguage='en'
        self.SetStatusText(_('generate ... '), STATUS_TEXT_POS)
        #self.vgpElem.Clear()
        #self.vgpElemData.Clear()
        
        #self.vgpTree.SetLanguage(self.actLanguage)
        self.actLanguage=None
        self.vgpElem.SetLanguage(self.actLanguage)
        self.vgpElemData.SetLanguage(self.actLanguage)
        if self.dlgImpOff is not None:
            self.dlgImpOff.SetLanguage(self.actLanguage)
        
        self.SetStatusText(_('generate finished. '), STATUS_TEXT_POS)
        
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        #if self.bActivated==True:
        #    if len(self.sOpenCfgFN)>0:
        #        self.bOpenCfg=True
        #    if self.bOpenCfg==True:
        #        self.bOpenCfg=False
        #        self.OpenCfgFile(self.sOpenCfgFN)
        pass
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC Project Engineering")
        fn=self.netPrjEng.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.bModified:
            #if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)

    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    
    def OnMasterStart(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnMasterStart')
        if VERBOSE:
            vtLog.vtLngCallStack(self,loggin.DEBUG,'')
        self.SetStatusText(_('master started ...'), STATUS_TEXT_POS)
        self.__makeTitle__()
        self.vgpTree.Clear()
        self.vgpElem.Clear()
        self.vgpElemData.Clear()
        evt.Skip()
    def OnOpenStart(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnOpenStart')
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('file opened ...'), STATUS_TEXT_POS)
        self.vgpTree.Clear()
        self.vgpElem.Clear()
        self.vgpElemData.Clear()
        evt.Skip()
    def OnOpenOk(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnOpenOk')
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('file opened and parsed.'), STATUS_TEXT_POS)
        self.__getXmlBaseNodes__()
        self.__makeTitle__()
        evt.Skip()
    def OnOpenFault(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnOpenFault')
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('file open fault.'), STATUS_TEXT_POS)
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnSynchStart')
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()), STATUS_TEXT_POS)
        self.__makeTitle__()
        self.vgpElem.Clear()
        self.vgpElemData.Clear()
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.SetStatusText(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()), STATUS_TEXT_POS)
        evt.Skip()
    def OnSynchFinished(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnSynchFinished')
        self.SetStatusText(_('synch finished.'), STATUS_TEXT_POS)
        self.gProcess.SetValue(0)
        self.__getXmlBaseNodes__()
        if evt.IsChanged():
            self.__setModified__(True)
        evt.Skip()
    def OnSynchAborted(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnSynchAborted')
        self.SetStatusText(_('synch aborted.'), STATUS_TEXT_POS)
        self.__getXmlBaseNodes__()
        if evt.IsChanged():
            self.__setModified__(True)
        evt.Skip()
        
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.__makeTitle__()
        self.SetStatusText(_('connection established ... please wait'), STATUS_TEXT_POS)
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('connection lost.'), STATUS_TEXT_POS)
        evt.Skip()
    def OnLoggedin(self,evt):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.SetStatusText(_('get content ... '), STATUS_TEXT_POS)
        evt.Skip()
    def OnChanged(self,evt):
        #if VERBOSE:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.__setModified__(True)
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netPrjEng.ReConnect2Srv()==0:
            self.netHum.ReConnect2Srv()
            self.netDoc.ReConnect2Srv()
            self.netTask.ReConnect2Srv()
        
    def OnGotContent(self,evt):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        #self.vgpPrjDoc.Clear()
        if self.dlgFind is not None:
            self.dlgFind.Reset()
        self.SetStatusText(_('content got. generate ...'), STATUS_TEXT_POS)
        self.__getXmlBaseNodes__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.SetStatusText(_('generated.'), STATUS_TEXT_POS)
        evt.Skip()
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            nodeAppl=None
            node=self.vgpElem.Apply()
            if node is not None:
                print 'elem appl'
                nodeAppl=node
            node=self.vgpElemData.Apply()
            if node is not None:
                nodeAppl=node
            if nodeAppl is not None:
                self.netPrjEng.doEdit(nodeAppl)
                self.vgpTree.RefreshByID(self.netPrjEng.getKey(nodeAppl))
                self.__setModified__(True)
            self.selNode=event.GetTreeNodeData()
            self.vgpElem.SetNode(self.selNode)
            self.vgpElemData.SetNode(self.selNode)
            self.netPrjEng.endEdit(None) # 
            self.netPrjEng.startEdit(self.selNode)
            if self.dlgInstance is None:
                self.dlgInstance=vInstanceDialog(self)
                self.dlgInstance.SetDoc(self.netPrjEng,True)
                self.vgpTree.SetDlgInstance(self.dlgInstance)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    '%s'%self.netPrjEng.getKey(self.selNode))
        else:
            # grouping tree item selected
            pass
        event.Skip()
        pass
    def OnTreeItemGoto(self,evt):
        if evt.GetTreeNodeData() is not None:
            node=evt.GetTreeNodeData()
            id=evt.GetID()
            if self.netPrjEng.IsKeyValid(id):
                self.idPrev=''
                if node is not None:
                    self.idPrev=self.netPrjEng.getKey(node)
                else:
                    self.idPrev=''
                self.vgpTree.SelectByID(id)
            else:
                if len(self.idPrev)>0:
                    self.vgpTree.SelectByID(self.idPrev)
                    self.idPrev=''
                    if node is not None:
                        self.idPrev=self.netPrjEng.getKey(node)
        evt.Skip()
    def OnTreeItemGotoTmpl(self,evt):
        if evt.GetTreeNodeData() is not None:
            node=evt.GetTreeNodeData()
            if len(self.netPrjEng.getAttribute(node,'iid'))>0:
                self.vgpTree.SelectByID(self.netPrjEng.getAttribute(node,'iid'))
        evt.Skip()
    def OnElemChanged(self,event):
        #self.vgpTree.RefreshSelected()
        self.vgpTree.RefreshByID(self.netPrjEng.getKey(event.GetNode()))
        self.__setModified__(True)
        event.Skip()
    def OnElemApplied(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'key:%s'%self.netPrjEng.getKey(evt.GetNode()))
        self.vgpElemData.Apply(True)
        self.netPrjEng.doEdit(evt.GetNode())
        #print evt.GetNode()
        #self.vgpTree.RefreshSelected()
        evt.Skip()
    def OnElemCanceled(self,event):
        self.vgpElemData.Cancel()
        event.Skip()
    def OnElemDataApplied(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'key:%s'%self.netPrjEng.getKey(evt.GetNode()))
        #self.vgpElemData.Apply()
        #self.netPrjEng.doEdit(evt.GetNode())
        #self.vgpTree.RefreshSelected()
        evt.Skip()
    def OnElemDataChanged(self,event):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        #self.netPrjEng.doEdit(event.GetNode())
        #oldId=self.netPrjEng.getAttribute(self.vgpTree.objTreeItemSel,'id')
        #self.vgpTree.SelectByID(self.netPrjEng.getAttribute(event.GetNode(),'id'))
        #self.vgpTree.RefreshSelected()
        #self.vgpTree.SelectByID(oldId)
        #self.vgpTree.RefreshByID(self.netPrjEng.getKey(event.GetNode()))
        self.__setModified__(True)
        event.Skip()
    def OnElemDataRevChanged(self,event):
        self.vgpTree.RefreshSelectedDeep()
        self.__setModified__(True)
        event.Skip()
    def OnTreeItemInstance(self,evt):
        if evt.GetTreeNodeData() is not None:
            self.selNode=evt.GetTreeNodeData()
            self.vgpElem.SetNode(evt.GetTreeNodeData())
        self.__setModified__(True)
        evt.Skip()
    def OnTreeItemEdit(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        self.selNode=event.GetTreeNodeData()
        lst=self.vgpTree.GetIDNodes()
        self.vgpElem.SetNode(self.selNode)
        self.netPrjEng.doEdit(event.GetTreeNodeData())
        self.__setModified__(True)
        event.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def OnSfcAdded(self,event):
        self.vgpTree.RefreshSelectedDeep()
        self.__setModified__(True)
    def OnTreeItemDel(self,evt):
        self.selNode=evt.GetTreeNodeData()
        #self.vgpElem.SetNode(self.selNode)
        self.vgpElem.Clear()
        self.vgpElemData.Clear()
        #self.vgpElem.SetNode(None)
        #self.vgpElemData.SetNode(None)
        self.__setModified__(True)
        evt.Skip()
    def OnDocGrpChanged(self,event):
        self.vgpPrjDoc.RefreshDocuments()
        self.__setModified__(True)
    def OnDocChanged(self,event):
        self.__setModified__(True)
    def OnNetHumLoggedin(self,evt):
        #print 'human loggedin'
        pass
    def OnHumanChanged(self,evt):
        self.hums=self.netHum.getSortedNodeUsrIds(['users'],
                        'user','id',[('name','%s')])
        self.vgpElemData.SetHumans(self.hums,self.netHum)
    def OnHumanSetupChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OnNetDocumentLoggedin(self,evt):
        #print 'doc loggedin'
        pass
    def OnDocumentChanged(self,evt):
        #self.hums=self.netHum.getSortedNodeIds(['users'],
        #                'user','id',[('name','%s')])
        #self.vgpElemData.SetHumans(self.hums,self.netHum)
        pass
    def OnDocumentSetupChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        #self.domDoc.Close()
        #self.domHum.Close()
        #self.domLoc.Close()
        self.domVsas.Close()
        
        self.xmlDocFN=self.netPrjEng.getNodeText(self.baseSettings,'docfn')
        self.xmlVsasFN=self.netPrjEng.getNodeText(self.baseSettings,'vsasfn')
        self.xmlHumanFN=self.netPrjEng.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netPrjEng.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netPrjEng.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netPrjEng.getNodeText(self.baseSettings,'projectdn')
        self.xmlMode=self.netPrjEng.getNodeText(self.baseSettings,'mode')
        
        if self.netPrjEng.getNodeText(self.baseSettings,'auto_apply')=='1':
            self.vgpElem.SetAutoApply(True)
            self.vgpElemData.SetAutoApply(True)
        else:
            self.vgpElem.SetAutoApply(False)
            self.vgpElemData.SetAutoApply(False)
        
        
        self.xmlBaseTmplDN=string.strip(self.xmlBaseTmplDN)
        self.xmlProjectDN=string.strip(self.xmlProjectDN)
        self.xmlMode=string.strip(self.xmlMode)
        
        # networking
        #self.netHum.SetNode(self.dom.getChild(self.baseSettings,'human'))
        #self.netHum.SetSetupNode(self.baseSettings,['human'])
        #self.netPrjEng.SetSetupNode(None,['settings','server'])
        #self.netHum.SetSetupNode(self.netPrjEng.getRoot(),['settings','human'],self.netPrjEng)
        #self.netDoc.SetSetupNode(self.netPrjEng.getRoot(),['settings','document'],self.netPrjEng)
        #self.netHum.Connect2Srv()
        
        self.vgpElemData.SetPrjDN(self.xmlProjectDN)
        # get tree view settings
        sTreeViewGrp=self.netPrjEng.getNodeText(self.baseSettings,'treeviewgrp')
        sDocTreeView=self.netPrjEng.getNodeText(self.baseSettings,'doctreeview')
        mnItems=self.mnViewTree.GetMenuItems()
        
        #self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='dateusr':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,4)
            #self.vgpTree.SetGrpDateUsr()
        elif sTreeViewGrp=='dateshortusr':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,4)
            #self.vgpTree.SetGrpDateShortUsr()
        elif sTreeViewGrp=='usrdate':
            mnItems[2].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,2,0,4)
            #self.vgpTree.SetGrpUsrDate()
        elif sTreeViewGrp=='usrdateshort':
            mnItems[3].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,3,0,4)
            #self.vgpTree.SetGrpUsrDateShort()
        else:
            mnItems[2].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,2,0,4)
            #self.vgpTree.SetGrpUsrDate()
            
        if sDocTreeView=='normal':
            mnItems[5].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,5,5,2)
            #self.vgpTree.SetLblFull()
        elif sDocTreeView=='iddocgrp':
            mnItems[6].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,6,5,2)
            #self.vgpTree.SetLblDayTmePrj()
        else:
            # fallback
            mnItems[5].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,5,5,2)
            #self.vgpTree.SetLblTmePrj()
        #if self.domDoc.Open(self.xmlDocFN)>=0:
        #    self.docs=self.domDoc.getSortedNodeIds(['docnumbering'],
        #                'docgroup','id',[('name','%s')])
        #else:
        #    self.docs=vtXmlDom.SortedNodeIds()
        #if self.domHum.Open(self.xmlHumanFN)>=0:
        #    self.hums=self.domHum.getSortedNodeIds(['users'],
        #                'user','id',[('name','%s')])
        #else:
        #    self.hums=vtXmlDom.SortedNodeIds()
            
        #self.vgpElemData.SetHumans(self.hums,self.domHum)
        if self.domVsas.Open(self.xmlVsasFN)<0:
            pass
            
        if self.dlgVsas is None:
            self.dlgVsas=vVsasDialog(self)
        self.dlgVsas.SetDoc(self.domVsas,self.xmlVsasFN)
        self.vgpTree.SetDlgVsas(self.dlgVsas)
        
        if self.dlgElementFilter is None:
            self.dlgElementFilter=vElementFilterDialog(self)
            s=self.netPrjEng.getNodeText(self.baseSettings,'filter')
            if len(string.strip(s))>0:
                lst=string.split(s,',')
            else:
                lst=[]
            self.dlgElementFilter.SetFilter(lst)
            self.dlgViewElementsTable.SetFilter(lst)
        #self.hums=vtXmlDomTree.SortedNodeIds()
        #try:
        #    self.domHum.Open(self.xmlHumanFN)
            #self.hums=vPrjTask.GetXmlHum(self.domHum)
        #    self.hums.SetNode(self.domHum,'user','id',
        #                [('name','%s')])
        #except:
        #    pass
        #self.vgpPrjTimeInfo.SetPersons(self.hums)
        
        #self.locs=vtXmlDomTree.SortedNodeIds()
        #try:
        #    self.domLoc.Open(self.xmlLocationFN)
            #self.hums=vPrjTask.GetXmlHum(self.domHum)
        #    self.locs.SetNode(self.domLoc,'location','id',
        #                [('name','%s')])
        #except:
        #    pass
        #self.vgpPrjTimeInfo.SetLocs(self.locs)
    def __getPrjInfo__(self):
        self.xmlProjectShortcut=self.netPrjEng.getNodeText(self.basePrjInfo,'name')
        self.xmlProjectClient=self.netPrjEng.getNodeText(self.basePrjInfo,'clientshort')
        self.xmlProjectShortcut=string.strip(self.xmlProjectShortcut)
        self.xmlProjectClient=string.strip(self.xmlProjectClient)
        
        self.prjinfos={}
        sID=self.netPrjEng.getAttribute(self.basePrjInfo,'id')
        self.prjinfos[u'id']=sID
        nodes=self.netPrjEng.getChilds(self.basePrjInfo)
        for n in nodes:
            tagName=self.netPrjEng.getTagName(n)
            val=self.netPrjEng.getText(n)
            self.prjinfos[tagName]=val
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        self.baseTasks=None
        self.tasks=[]
        self.subtasks=[]
        root=self.netPrjEng.getRoot()
        self.baseDoc=self.netPrjEng.getChild(root,'prjengs')
            
        self.baseSettings=self.netPrjEng.getChild(root,'settings')
        if self.baseSettings is None:
            self.baseSettings=self.netPrjEng.createSubNode(self.netPrjEng.getRoot(),'settings',None)
        self.__getSettings__()
            
        self.basePrjInfo=self.netPrjEng.getChild(root,'prjinfo')
        if self.basePrjInfo is None:
            self.basePrjInfo=self.netPrjEng.createSubNode(self.netPrjEng.getRoot(),'prjinfo',None)
        self.__getPrjInfo__()
            
        #self.vgpPrjTimeInfo.SetTasks(self.tasks)
        #self.vgpPrjTimeInfo.SetSubTasks(self.subtasks)
    def OpenFile(self,fn):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.netPrjEng.ClearAutoFN()>0:
            self.netPrjEng.Open(fn)
            #self.netTask.Open()
            #self.netHum.Open()
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
            
            #self.vgpTree.SetLanguage(self.actLanguage)
            self.vgpTree.SetNode(None)
        
        self.SetStatusText(_('open file finished.'), STATUS_TEXT_POS)
        return
        if self.dlgFind is not None:
            self.dlgFind.Reset()
        try:
            self.SetStatusText('open file ....', STATUS_TEXT_POS)
            self.netPrjEng.Close()
            if self.netPrjEng.Open(fn)<0:
                self.SetStatusText('open fault. ', STATUS_TEXT_POS)
                self.CreateNew()
            else:
                self.SetStatusText('generate ... ', STATUS_TEXT_POS)
        except:
            self.SetStatusText('open fault. ', STATUS_TEXT_POS)
            self.CreateNew()
                
        self.__setModified__(False)
        self.__getXmlBaseNodes__()
        
        self.vgpTree.SetLanguage(self.actLanguage,True)
        self.vgpTree.SetNode(self.baseDoc)
        self.vgpElem.Clear()
        self.vgpElemData.Clear()
        
        if self.bAutoConnect:
            self.netPrjEng.Connect2SrvByNode(None,None)
            self.netDoc.Connect2SrvByNode(None,None)
            self.netHum.Connect2SrvByNode(None,None)

        self.SetStatusText('open file finished. ', STATUS_TEXT_POS)
    def OpenCfgFile(self,fn=None):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        #if self.bActivated==False:
        #    self.sOpenCfgFN=fn
        #    return
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            #self.netPrjEng.SetCfgNode()
            if iRet>=0:
                self.netMaster.SetCfgNode()
                #self.netPrjEng.AutoConnect2Srv()
                if VERBOSE:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
                self.__makeTitle__()
                #self.__setModified__(False)
                #self.__getXmlBaseNodes__()
                #self.vgpTree.SetNode(self.baseDoc)
                if VERBOSE:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            #self.netPrjEng.SetCfgNode()
            #self.netPrjEng.Setup()
            self.netMaster.SetCfgNode()
            
    def SaveFile(self,fn=None):
        self.SetStatusText(_('save file ...'), STATUS_TEXT_POS)
        sCurFN=self.netPrjEng.GetFN()
        if (sCurFN is None) and (fn is None):
            self.OnMnFileItem_saveasMenu(None)
            self.__setModified__(False)
            return
        self.SetStatusText(_('save file ...'), STATUS_TEXT_POS)
        
        if self.netPrjEng.Save(fn)<0:
            self.OnMnFileItem_saveasMenu(None)
            self.__setModified__(False)
            
        self.SetStatusText(_('save file finished.'), STATUS_TEXT_POS)
        
        self.__setModified__(False)
    def CreateNew(self):
        self.netPrjEng.New()
        
        self.CreateSetup()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
        #self.vgpPrjTimeInfo.SetRootNode(self.baseDoc)
        #self.vgpPrjTimeInfo.SetStarted(None)
        self.__setModified__(True)
    def CreateSetup(self):
        #if self.dom is None:
        #    self.CreateNew()
        rootNode=self.netPrjEng.getRoot()
        elem=self.netPrjEng.getChild(rootNode,'settings')
        # add project file
        elemPrjFN=self.netPrjEng.createSubNodeText(elem,'projectfn','---')
        # add project file
        elemVsasFN=self.netPrjEng.createSubNodeText(elem,'vsasfn','---')
        # add human file
        elemHumanFN=self.netPrjEng.createSubNodeText(elem,'humanfn','---')
        # add template dir
        elemTmplDN=self.netPrjEng.createSubNodeText(elem,'templatedn','---')
        # add project dir
        elemPrjDN=self.netPrjEng.createSubNodeText(elem,'projectdn','---')
        self.netPrjEng.setNodeText(elem,'lastprj','---')
        self.netPrjEng.setNodeText(elem,'lasttask','---')
        self.netPrjEng.setNodeText(elem,'lastsubtask','---')
        self.netPrjEng.setNodeText(elem,'lastdesc','---')
        self.netPrjEng.setNodeText(elem,'lastloc','---')
        self.netPrjEng.setNodeText(elem,'lastperson','---')
        self.netPrjEng.setNodeText(elem,'treeviewgrp','usrdate')
        self.netPrjEng.setNodeText(elem,'doctreeview','normal')
        
        self.netPrjEng.AlignNode(elem,iRec=2)
    def IsThreadRunning(self):
        if self.vgpTree.IsThreadRunning():
            return True
        if self.dlgFind is not None:
            if self.dlgFind.IsThreadRunning():
                return True
        return False
    def OnMnFileItems_openMenu(self, event):
        if self.netPrjEng.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        if self.IsThreadRunning():
            return
        
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxOPEN)
        try:
            fn=self.netPrjEng.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()


    def OnMnFileItem_saveMenu(self, event):
        if self.IsThreadRunning():
            return
        self.SaveFile()
        event.Skip()

    def OnMnFileItem_saveasMenu(self, event):
        if self.netPrjEng.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if self.IsThreadRunning():
            return
        dlg = wxFileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxSAVE)
        try:
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                #self.txtEditor.SaveFile(filename)
                self.SaveFile(filename)
        finally:
            dlg.Destroy()

    def OnMnFileItem_newMenu(self, event):
        if self.netPrjEng.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if self.IsThreadRunning():
            return
        self.SetStatusText(_('new ... '), STATUS_TEXT_POS)
        self.CreateNew()
        self.SetStatusText(_('new finished.'), STATUS_TEXT_POS)

        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        if self.IsThreadRunning():
            return
        dlg=vPrjEngSettingsDialog(self)
        self.baseSettings=self.netPrjEng.getChild(self.netPrjEng.getRoot(),'settings')
        
        dlg.SetXml(self.netPrjEng,self.baseSettings)
        dlg.Centre()
        ret=dlg.ShowModal()
        if ret>0:
            #checkInfos=vtXmlDom.NodeInfo(self.netPrjEng,self.baseSettings)
            dlg.GetXml(self.baseSettings)
            #res=checkInfos.checkDiff(self.baseSettings)
            #if len(res.keys())>0:
            #    self.__setModified__(True)
            self.__setModified__(True)
            self.__getSettings__()
            self.netPrjEng.AlignNode(self.baseSettings)
        dlg.Destroy()
        event.Skip()
    
    def OnMnImportItem_import_xmlMenu(self, event):
        #'open'
        if self.IsThreadRunning():
            return
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if self.humanFN is not None:
                dlg.SetDirectory(self.humanFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.SetFileHuman(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def __validEmptyTextNode__(self,txt):
        if len(string.strip(txt))==0:
            return '---'
        else:
            return txt
    def OnMnImportItem_import_nodeMenu(self, event):
        #'open'
        if self.IsThreadRunning():
            return
        if self.baseDoc is None:
            return
        if self.dlgImpNode is None:
            self.dlgImpNode=vImportNodeDialog(self)
            self.dlgImpNode.SetDoc(self.netPrjEng)
        self.dlgImpNode.Centre()
        self.dlgImpNode.SetNode(self.baseDoc,self.baseSettings)
        self.dlgImpNode.SetMainTree(self.vgpTree)
        ret=self.dlgImpNode.ShowModal()
        if ret>0:
            self.__setModified__(True)
            #self.vgpTree.SetNode(self.baseDoc)
            self.gProcess.SetValue(0)
            self.SetStatusText(_('import node file finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnMnExportItem_export_xmlMenu(self, event):
        event.Skip()

    def OnMnExportItem_export_nodeMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.baseDoc is None:
            return
        if self.dlgExpNode is None:
            self.dlgExpNode=vExportNodeDialog(self)
            self.dlgExpNode.SetDoc(self.netPrjEng,self.netPrj)
        self.dlgExpNode.Centre()
        self.dlgExpNode.SetNode(self.baseDoc,self.baseSettings)
        ret=self.dlgExpNode.ShowModal()
        if ret>0:
            pass
        event.Skip()
            
    def OnMnToolsItem_alignMenu(self, event):
        if self.IsThreadRunning():
            return
        self.SetStatusText(_('align ... '), STATUS_TEXT_POS)
        iCount=self.netPrjEng.GetNodeCount()
        self.gProcess.SetValue(0)
        self.gProcess.SetRange(iCount)
        self.netPrjEng.AlignDoc(gProcess=self.gProcess)
        self.SetStatusText(_('align finished.'), STATUS_TEXT_POS)
        self.gProcess.SetValue(0)
            
        self.__setModified__(True)
        event.Skip()

    def __EnsureOneCheckMenuItem__(self,mnItems,iAct,iStart,iCount):
        if mnItems[iAct].IsChecked():
            # uncheck all other
            for i in range(iStart,iStart+iCount):
                if i!=iAct:
                    mnItems[i].Check(False)
            return False
        else:
            bFound=False
            for i in range(iStart,iCount):
                if mnItems[i].IsChecked()==True:
                    bFound=True
            if bFound==False:
                mnItems[iAct].Check(True)
                return True
            return False
    def OnMnViewTreeItem_date_usr_grpMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,0,0,4):
            return
        self.vgpTree.SetGrpDateUsr()
        
        if self.dom is None:
            return    
        vtXmlDomTree.setNodeText(self.baseSettings,'treeviewgrp','dateusr')
        self.__setModified__(True)
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)

        event.Skip()
    def OnMnViewTreeItem_date_short_usr_grpMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,1,0,4):
            return
        self.vgpTree.SetGrpDateShortUsr()
        
        if self.dom is None:
            return    
        vtXmlDomTree.setNodeText(self.baseSettings,'treeviewgrp','dateshortusr')
        self.__setModified__(True)
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnMnViewTreeItem_usr_date_grpMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,2,0,4):
            return
        self.vgpTree.SetGrpUsrDate()
        
        if self.dom is None:
            return
        vtXmlDomTree.setNodeText(self.baseSettings,'treeviewgrp','usrdate')
        self.__setModified__(True)
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()
    def OnMnViewTreeItem_usr_date_short_grpMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,3,0,4):
            return
        self.vgpTree.SetGrpUsrDateShort()
        
        if self.dom is None:
            return
        vtXmlDomTree.setNodeText(self.baseSettings,'treeviewgrp','usrdateshort')
        self.__setModified__(True)
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnMnViewTreeItem_NormalMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,5,5,2):
            return
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpDocSel.SetNormalGrouping()
        
        if self.dom is not None:
            vtXmlDomTree.setNodeText(self.baseSettings,'doctreeview','normal')
        self.__setModified__(True)
        #self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnMnViewTreeItem_IDDocGrpMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,6,5,2):
            return
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpDocSel.SetIdGrouping()
        
        if self.dom is None:
            vtXmlDomTree.setNodeText(self.baseSettings,'doctreeview','iddocgrp')
        self.__setModified__(True)
        #self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnMnViewTreeItem_tme_prj_lblMenu(self, event):
        if self.IsThreadRunning():
            return
        mnItems=self.mnViewTree.GetMenuItems()
        if self.__EnsureOneCheckMenuItem__(mnItems,7,5,3):
            return
        self.vgpTree.SetLblTmePrj()
        
        if self.dom is None:
            return
        vtXmlDomTree.setNodeText(self.baseSettings,'treeviewlbl','tmeprj')
        self.__setModified__(True)
        self.SetStatusText(_('generate ...'), STATUS_TEXT_POS)
        self.vgpTree.SetNode(None)
        self.SetStatusText(_('generate finished.'), STATUS_TEXT_POS)
        event.Skip()

    def OnVgfPrjTimerMainClose(self, event):
        self.vgpTree.Stop()
        if self.bModified==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        _('vgaPrjEng Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.SaveFile()
                pass
        self.netMaster.ShutDown()
        event.Skip()

    def OnMnViewItem_element_filterMenu(self, event):
        if self.dlgElementFilter is None:
            self.dlgElementFilter=vElementFilterDialog(self)
        self.dlgElementFilter.Centre()
        ret=self.dlgElementFilter.ShowModal()
        if ret==1:
            #elf.vgpElementInfos.Setup(None)
            #lst=self.vgpTree.GetIDNodes()
            
            lstElemFilter=self.dlgElementFilter.GetFilter()
            self.dlgViewElementsTable.SetFilter(lstElemFilter)
            vtXmlDomTree.setNodeText(self.baseSettings,'filter',
                    string.join(lstElemFilter,','))
            self.__setModified__(True)
            #lst=self.vgpTree.GetIDNodes()
            pass
        event.Skip()

    def OnMnViewItem_view_elementsMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.dlgViewElementsTable is None:
            self.dlgViewElementsTable=vViewElementsTableDialog(self)
            self.vgpTree.SetViewElementsTable(self.dlgViewElementsTable)
        lst=self.vgpTree.GetIDNodes()
        self.dlgViewElementsTable.SetNode(self.selNode,lst)
        ret=self.dlgViewElementsTable.ShowModal()
        if ret==1:
            self.vgpTree.RefreshSelected()
            self.__setModified__(True)
            pass
        event.Skip()

    def OnMnImportItem_export_taskMenu(self, event):
        if self.netTask.IsRunning():
            return
        self.netTask.__walkPrjEngNodes__()
        return
        if self.IsThreadRunning():
            return
        if self.baseDoc is None or self.baseSettings is None:
            return
        if self.dlgGenTask is None:
            self.dlgGenTask=vPrjEngGenTaskDialog(self)
        self.dlgGenTask.SetNode(self.netPrjEng,self.netTask)
        self.dlgGenTask.Centre()
        if self.dlgGenTask.ShowModal()==1:
            self.__setModified__(True)
        
        event.Skip()

    def OnMnExportItem_export_emailMenu(self, event):
        if self.IsThreadRunning():
            vtLog.CallStack('thread is running.')
            return
        if self.baseDoc is None or self.baseSettings is None:
            vtLog.CallStack('doc:%d settings:%d'%(self.baseDoc is None,self.baseSettings is None))
            return
        dlg=vPrjEngExpEmailDialog(self)
        dlg.SetLang(self.actLanguage)
        if self.netPrjEng.getTagName(self.selNode)=='eMailBox':
            node=self.selNode
        else:
            node=None
        dlg.SetNode(self.netPrjEng,self.netPrjDoc,self.netDoc,
                self.baseDoc,self.baseSettings,node)
        dlg.Centre()
        if dlg.ShowModal()==1:
            #self.__setModified__(True)
            pass
        dlg.Destroy()
        event.Skip()

    def OnMnAnalyseItem_analyse_generalMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.dlgAnalyseGen is None:
            self.dlgAnalyseGen=vAnalyseGeneralDialog(self)
            self.dlgAnalyseGen.SetDoc(self.netPrjEng)
        if self.baseDoc is None:
            return
        self.dlgAnalyseGen.Centre()
        self.dlgAnalyseGen.SetTree(self.vgpTree)
        self.dlgAnalyseGen.SetNode(self.baseDoc)
        self.dlgAnalyseGen.Show(True)
        event.Skip()

    def OnMnFindItem_find_searchMenu(self, event):
        event.Skip()

    def OnMnImportItem_import_officeMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.dlgImpOff is None:
            self.dlgImpOff=vImportOfficeDialog(self)
            self.dlgImpOff.SetLanguages(self.languages,self.languageIds)
            self.dlgImpOff.SetLanguage(self.actLanguage)
            self.dlgImpOff.SetDoc(self.netPrjEng,False)
        self.dlgImpOff.Centre()
        ioNode=self.netPrjEng.getChild(self.netPrjEng.getRoot(),'import_export')
        if ioNode is None:
            ioNode=self.netPrjEng.createSubNode(self.netPrjEng.getRoot(),
                        'import_export')
        tmplNode=self.netPrjEng.getChild(None,'templates')
        
        self.dlgImpOff.SetNode(ioNode,tmplNode,['__node'])
        ret=self.dlgImpOff.ShowModal()
        if ret==1:
            self.SetStatusText(_('import ....'), STATUS_TEXT_POS)
            self.dlgImpOff.GetNode()
            nodes=self.dlgImpOff.GetNodes()
            self.vgpTree.InstanceNodes(nodes)
            self.SetStatusText(_('import finished.'), STATUS_TEXT_POS)
            pass
        event.Skip()

    def OnMnViewItem_find_searchMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.dlgFind is None:
            self.dlgFind=vFindDialog(self)
        if self.baseDoc is None:
            return
        self.dlgFind.Centre()
        self.dlgFind.SetTree(self.vgpTree)
        self.dlgFind.SetNode(self.netPrjEng,self.baseDoc)
        self.dlgFind.Show(True)
        event.Skip()

    def OnMnViewItem_search_activitiesMenu(self, event):
        if self.IsThreadRunning():
            return
        if self.dlgFindAct is None:
            self.dlgFindAct=vFindActivitiesDialog(self)
            self.dlgFindAct.SetDoc(self.netPrjEng,self.netHum,True)
        if self.baseDoc is None:
            return
        self.dlgFindAct.Centre()
        self.dlgFindAct.SetTree(self.vgpTree)
        self.dlgFindAct.SetNode(self.baseDoc)
        #self.dlgFindAct.SetHumans(self.netHum)
        self.dlgFindAct.Show(True)
        
        event.Skip()

    def OnVgfPrjEngMainActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.bActivated=True
                #wx.GetApp().SetTopWindow(self)
                self.netMaster.ShowPopup()
        event.Skip()

    def OnMnFileItem_open_cfgMenu(self, event):
        if self.netPrjEng.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        #'open'
        dlg = wxFileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxOPEN)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.OpenCfgFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_save_cfgMenu(self, event):
        if self.netPrjEng.IsActive():
            sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlg = wxFileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wxSAVE)
        try:
            fn=self.xdCfg.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wxID_OK:
                filename = dlg.GetPath()
                self.xdCfg.AlignDoc()
                self.xdCfg.Save(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileMn_file_save_allMenu(self, event):
        self.netMaster.Save()
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()


