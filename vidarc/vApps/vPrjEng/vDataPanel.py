#Boa:FramePanel:vDataPanel
#----------------------------------------------------------------------------
# Name:         vDataPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vDataPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

[wxID_VDATAPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vDataPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VDATAPANEL, name=u'vDataPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(600, 300),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(592, 273))

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.bModified=False
    def __setModified__(self,state):
        self.bModified=state
    def SetNode(self,name,node,ids,doc):
        if self.bModified==True:
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaPrjEng Elements Info',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode(self.node)
        self.__setModified__(False)
        self.doc=doc
        self.node=node
        self.name=name
        self.ids=ids
