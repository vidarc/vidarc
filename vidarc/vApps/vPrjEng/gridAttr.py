#----------------------------------------------------------------------------
# Name:         gridAttr.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: gridAttr.py,v 1.1 2005/12/13 13:19:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import  wx
import  wx.grid as gridlib
import string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS

import images_s88

#---------------------------------------------------------------------------
# defined event for vgpXmlTree item selected
wxEVT_GRIDATTR_INFO_CHANGED=wx.NewEventType()
def EVT_GRIDATTR_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_GRIDATTR_INFO_CHANGED,func)
class vgpGridAttrInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_GRIDATTR_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_GRIDATTR_INFO_CHANGED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetGridAttr(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col
        
class gridAttrData(gridlib.PyGridTableBase):
    def __init__(self,baseNode,lstIDNodes,lstFilter,doc,sort='types'):
        gridlib.PyGridTableBase.__init__(self)
        ids=doc.getIds()
        self.colLabels = ['','type','hierachy']
        self.dataTypes = [gridlib.GRID_VALUE_TEXT,gridlib.GRID_VALUE_TEXT,gridlib.GRID_VALUE_TEXT]
        self.data = []
        self.dataNodes = []
        datItem=None
        if lstIDNodes is None:
            self.colLabels = []
            self.dataTypes = []
            self.data=[[]]
            self.dataNodes=[[]]
            return
        d,lst=self.__sortNodes__(lstIDNodes,lstFilter,doc,sort)
        
        self.dictTypes=d
        self.lstNodes=lst
        self.dftDatItem=[]
        for item in lst:
            #print item
            if string.find(item[2],'int')==0:
                val=0
            elif string.find(item[2],'long')==0:
                val=0
            elif string.find(item[2],'float')==0:
                val=0.0
            elif string.find(item[2],'double')==0:
                val=0.0
            else:
                val=''
            self.dftDatItem.append(val)
            self.colLabels.append(item[0])
            #self.dataTypes.append(gridlib.GRID_VALUE_TEXT)
            self.dataTypes.append(item[2])
        #print self.dftDatItem
        iCount=len(lst)
        keys=d.keys()
        keys.sort()
        if len(keys)<=0:
            self.colLabels = []
            self.dataTypes = []
            self.data=[[]]
            self.dataNodes=[[]]
            return
        for k in keys:
            for nodeInfo in d[k]:
                #datItem=[]
                datNodes=[]
                for i in range(0,iCount):
                    #datItem.append('')
                    datNodes.append(None)
                datItem=copy.copy(self.dftDatItem)
                node=nodeInfo[0]
                for idxInfo in nodeInfo[1]:
                    info=lst[idxInfo]
                    k=info[0]
                    keys=string.split(k,':')
                    if len(keys)==2:
                        nc=doc.getChild(node,keys[-1])
                        if len(doc.getAttribute(nc,'ih'))>0:
                            sVal=doc.getText(nc)
                            datN=nc
                            iid=doc.getAttribute(node,'iid')
                            instNodeTup=ids.GetIdStr(iid)
                            sType=doc.getNodeText(doc.getChild(instNodeTup[1],doc.getTagName(nc)),'type')
                        elif len(doc.getAttribute(nc,'fid'))>0:
                            sVal=doc.getText(nc)
                            datN=nc
                            
                            iid=doc.getAttribute(nc,'fid')
                            instNodeTup=ids.GetIdStr(iid)
                            attrTmpl=doc.getChild(instNodeTup[1],nc.tagName)
                            sType=doc.getNodeText(attrTmpl,'type')
                        elif doc.getChild(nc,'__browse') is not None:
                            continue
                        else:
                            sVal=doc.getNodeText(nc,'val')
                            datN=doc.getChild(nc,'val')
                            sType=doc.getNodeText(nc,'type')
                        #print "   ",sType,sVal
                        if sType=='int':
                            try:
                                val=int(sVal)
                            except:
                                val=0
                        elif sType=='long':
                            try:
                                val=long(sVal)
                            except:
                                val=0
                        elif sType=='float':
                            try:
                                val=float(sVal)
                            except:
                                val=0
                        else:
                            val=sVal
                    else:
                        val=doc.getNodeText(node,k)
                        datN=doc.getChild(node,k)
                        
                    #datItem.insert(idxInfo,sVal)
                    #datItem.remove(idxInfo+1)
                    datItem1=datItem[:idxInfo]+[val]+datItem[idxInfo+1:]
                    datItem=datItem1
                    datNodes=datNodes[:idxInfo]+[datN]+datNodes[idxInfo+1:]
                sHierarchy=self.__getHierarchy__(doc,baseNode,node)
                nTag=doc.getTagName(node)
                self.data.append([nTag,nTag,sHierarchy]+datItem)
                self.dataNodes.append([None,None,None]+datNodes)
        datItem=[]
        datNodes=[]
        if datItem is not None:
            for i in range(0,iCount):
                datItem.append('')
                datNodes.append(None)
            self.data.append(['Sum','Sum','']+datItem)
            self.dataNodes.append([None,None,None]+datNodes)
        #print "+++++++++++++++"
        #print " col",len(self.colLabels)
        #print "type",len(self.dataTypes)
        #print "data",len(self.data)
        #for d in self.data:
        #    print "    ",len(d)
        #print "node",len(self.dataNodes)
        #for d in self.dataNodes:
        #    print "    ",len(d)
        #print "---------------"
        #print self.colLabels
    def __getHierarchy__(self,doc,baseNode,n):
        if baseNode is None:
            return ''
        if not(doc.isSame(baseNode,n)):
            return self.__getHierarchy__(doc,baseNode,doc.getParent(n))+'<'+ doc.getNodeText(n,'tag')+'>'#n.tagName
        else:
            return ''
    def __getGridType__(self,typeInfos):
        try:
            attrTyp=typeInfos['type']
        except:
            datTyp=gridlib.GRID_VALUE_STRING
            return datTyp
        if attrTyp=='datetime':
            datTyp=gridlib.GRID_VALUE_DATETIME
        elif attrTyp=='int':
            datTyp=gridlib.GRID_VALUE_NUMBER
            try:
                sMin=typeInfos['min']
            except:
                sMin=''
            try:
                sMax=typeInfos['max']
            except:
                sMax=''
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='float':
            datTyp=gridlib.GRID_VALUE_FLOAT
            try:
                sDigits=typeInfos['digits']
            except:
                sDigits=''
            try:
                sComma=typeInfos['comma']
            except:
                sComma=''
            if len(sDigits)>0 and len(sComma)>0:
                s=':'+sDigits+','+sComma
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='long':
            datTyp=gridlib.GRID_VALUE_LONG
            try:
                sMin=typeInfos['min']
            except:
                sMin=''
            try:
                sMax=typeInfos['max']
            except:
                sMax=''
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='choiceint':
            datTyp=gridlib.GRID_VALUE_CHOICEINT
        elif attrTyp=='choice':
            datTyp=gridlib.GRID_VALUE_CHOICE
            keys=typeInfos.keys()
            keys.sort()
            choices=[]
            for k in keys:
                if k[:2]=='it':
                    choices.append(typeInfos[k])
            if len(choices)>0:
                datTyp=datTyp+':'+string.join(choices,',')
        elif attrTyp=='string':
            datTyp=gridlib.GRID_VALUE_STRING
        elif attrTyp=='text':
            datTyp=gridlib.GRID_VALUE_TEXT
        else:
            datTyp=gridlib.GRID_VALUE_STRING
        return datTyp
    def __getAttrIdx__(self,lstAttr,key):
        idx=0
        for attr in lstAttr:
            if attr[0]==key:
                return idx
            idx=idx+1
    def __sortNodes__(self,lstIDNodes,lstFilter,doc,sort):
        ids=doc.getIds()
        d={}
        lstAttr=[]
        lstAttr=[(u'tag',{u'tag':''},gridlib.GRID_VALUE_STRING,[]),
                    (u'name',{u'name':''},gridlib.GRID_VALUE_STRING,[]),
                    (u'description',{u'description':''},gridlib.GRID_VALUE_STRING,[])]
        for n in lstIDNodes:
            if sort=='types':
                type=doc.getTagName(n)
                if lstFilter is not None:
                    if len(lstFilter)>0:
                        if type not in lstFilter:
                            continue
                #print "  ",type,n.getAttribute('id'),vtXmlDomTree.getNodeText(n,'tag')
                lstChildElem=[]
                inheritanceType={}
                for child in doc.getChilds(n):
                    if doc.getTagName(child)=='inheritance_type':
                        for t in doc.getChilds(child):
                            if doc.getTagName(t)!='val':
                                inheritanceType[doc.getTagName(t)]=doc.getText(t)
                for child in doc.getChilds(n):
                    if len(doc.getAttribute(child,'id'))>0:
                            continue
                    desc={}
                    tagName=doc.getTagName(child)
                    if tagName in ['tag','name','description']:
                        k=tagName
                        desc[k]=''#vtXmlDomTree.getText(child)
                    elif tagName in ['inheritance_type','type']:
                        continue
                    elif tagName[:2]=='__':
                        continue
                    else:
                        k=type+':'+tagName
                        if len(doc.getAttribute(child,'ih'))>0:
                            tag=tagName
                            sVal=doc.getText(child)
                            #self.dictAttributes[tag]={'val':sVal}
                            iid=doc.getAttribute(n,'iid')
                            instNodeTup=ids.GetIdStr(iid)
                            attrs=doc.getChilds(doc.getChild(instNodeTup[1],tag))
                            for t in attrs:
                                ttagName=doc.getTagName(t)
                                if ttagName!='val':
                                    desc[ttagName]=doc.getText(t)
                        elif len(doc.getAttribute(child,'fid'))>0:
                            tag=tagName
                            sVal=doc.getText(child)
                            iid=doc.getAttribute(n,'fid')
                            instNodeTup=ids.GetIdStr(iid)
                            attrTmpl=doc.getChild(instNodeTup[1],tag)
                            attrs=doc.getChilds(attrTmpl)
                            for t in attrs:
                                ttagName=doc.getTagName(t)
                                if ttagName!='val':
                                    desc[ttagName]=doc.getText(t)
                        else:
                            for t in doc.getChilds(child):
                                ttagName=doc.getTagName(t)
                                if ttagName!='val':
                                    desc[ttagName]=doc.getText(t)
                    typeInfos={}
                    try:
                        s=desc['inheritance']
                        strs=string.split(s,',')
                        idx=strs.index('type')
                        tkeys=inheritanceType.keys()
                        tkeys.sort()
                        for tk in tkeys:
                            typeInfos[tk]=inheritanceType[tk]
                        typeInfos['val']=desc['val']
                    except:
                        tkeys=desc.keys()
                        tkeys.sort()
                        for tk in tkeys:
                            typeInfos[tk]=desc[tk]
                    try:
                        idx=self.__getAttrIdx__(lstAttr,k)
                        if lstAttr[idx][1]==typeInfos:
                            lstAttr[idx][3].append(n)
                        else:
                            gridType=self.__getGridType__(typeInfos)
                            lstAttr.append((k,typeInfos,gridType,[n]))
                            idx=len(lstAttr)-1
                    except:
                        gridType=self.__getGridType__(typeInfos)
                        lstAttr.append((k,typeInfos,gridType,[n]))
                        idx=len(lstAttr)-1
                    lstChildElem.append(idx)
                try:
                    lst=d[type]
                except:
                    lst=[]
                    d[type]=lst
                lst.append((n,lstChildElem))
                
        return d,lstAttr
    #--------------------------------------------------
    # required methods for the wxPyGridTableBase interface

    def GetNumberRows(self):
        return len(self.data) + 1

    def GetNumberCols(self):
        return len(self.data[0])

    def IsEmptyCell(self, row, col):
        try:
            return not self.data[row][col]
        except IndexError:
            return True

    # Get/Set values in the table.  The Python version of these
    # methods can handle any data-type, (as long as the Editor and
    # Renderer understands the type too,) not just strings as in the
    # C++ version.
    def GetValue(self, row, col):
        try:
            return self.data[row][col]
        except IndexError:
            return ''

    def SetValue(self, row, col, value):
        try:
            if value!='---':
                sType=self.dataTypes[col]
                if string.find(sType,gridlib.GRID_VALUE_NUMBER)>=0:
                    try:
                        value=int(value)
                    except:
                        value=0
                elif string.find(sType,gridlib.GRID_VALUE_LONG)>=0:
                    try:
                        value=long(value)
                    except:
                        value=0
                elif string.find(sType,gridlib.GRID_VALUE_FLOAT)>=0:
                    try:
                        value=float(value)
                    except:
                        value=0
            self.data[row][col] = value
        except IndexError:
            # add a new row
            self.data.append([''] * self.GetNumberCols())
            self.SetValue(row, col, value)

            # tell the grid we've added a row
            msg = gridlib.GridTableMessage(self,            # The table
                    gridlib.GRIDTABLE_NOTIFY_ROWS_APPENDED, # what we did to it
                    1                                       # how many
                    )

            self.GetView().ProcessTableMessage(msg)


    #--------------------------------------------------
    # Some optional methods

    # Called when the grid needs to display labels
    def GetColLabelValue(self, col):
        return self.colLabels[col]

    # Called to determine the kind of editor/renderer to use by
    # default, doesn't necessarily have to be the same type used
    # natively by the editor/renderer if they know how to convert.
    def GetTypeName(self, row, col):
        return self.dataTypes[col]

    # Called to determine how the data can be fetched and stored by the
    # editor and renderer.  This allows you to enforce some type-safety
    # in the grid.
    def CanGetValueAs(self, row, col, typeName):
        colType = self.dataTypes[col].split(':')[0]
        if typeName == colType:
            return True
        else:
            return False

    def CanSetValueAs(self, row, col, typeName):
        return self.CanGetValueAs(row, col, typeName)

    def GetDataNodes(self):
        return self.dataNodes
    def GetDataTypes(self):
        return self.dataTypes
    


#---------------------------------------------------------------------------

class gridImageRenderer(gridlib.PyGridCellRenderer):
    def __init__(self):
        """
        Image Renderer Test.  This just places an image in a cell
        based on the row index.  There are N choices and the
        choice is made by  choice[row%N]
        """
        gridlib.PyGridCellRenderer.__init__(self)
        #self.imgDict = {}
        #for e in ELEMENTS:
        #    try:
        #        imgName=e[1]['__img']
        #        f = getattr(images_s88, 'get%sBitmap' % imgName)
        #        img = f()
        #        self.imgDict[e[0]]=img
        #    except Exception,list:
                #self.SetStatusText('open fault. (%s)'%list, 1)
        #        pass
        #imgName='Sum'
        #f = getattr(images_s88, 'get%sBitmap' % imgName)
        #img = f()
        #self.imgDict['Sum']=img
        

        self.colSize = None
        self.rowSize = None

    def Draw(self, grid, attr, dc, rect, row, col, isSelected):
        #choice = self.table.GetRawValue(row, col)
        choice = grid.GetCellValue(row,col)#self.table.GetRawValue(row, col)
        #print row,col,choice,self.imgDict
        try:
            if choice=='Sum':
                imgName=choice
            else:
                imgName='None'
                for e in ELEMENTS:
                    if e[0]==choice:
                        imgName=e[1]['__img']
                        break
            f = getattr(images_s88, 'get%sBitmap' % imgName)
            img = f()
        except:
            f = getattr(images_s88, 'get%sBitmap' % 'None')
            img = f()
            
        #self.imgDict['Sum']=img
        
        #bmp = self.imgDict[choice]
        image = wx.MemoryDC()
        image.SelectObject(img)

        # clear the background
        dc.SetBackgroundMode(wx.SOLID)

        if isSelected:
            dc.SetBrush(wx.Brush(wx.BLUE, wx.SOLID))
            dc.SetPen(wx.Pen(wx.BLUE, 1, wx.SOLID))
        else:
            dc.SetBrush(wx.Brush(wx.LIGHT_GREY, wx.SOLID))
            dc.SetPen(wx.Pen(wx.LIGHT_GREY, 1, wx.SOLID))
        dc.DrawRectangleRect(rect)


        # copy the image but only to the size of the grid cell
        width, height = img.GetWidth(), img.GetHeight()

        if width > rect.width-2:
            width = rect.width-2

        if height > rect.height-2:
            height = rect.height-2

        dc.Blit(rect.x+1, rect.y+1, width, height,
                image,
                0, 0, wx.COPY, True)


class gridAttrGrid(gridlib.Grid):
    def __init__(self, parent, id, pos, size):
        gridlib.Grid.__init__(self, parent, id,pos,size)
        self.table=None
        self.emptyEditor=None
        self.emptyRenderer=None
        self.imgRenderer=None
        self.lstIDNodes=None
        #table = gridAttrData()

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        #sself.SetTable(table, True)

        self.SetColLabelSize(20)
        self.SetRowLabelSize(35)
        self.SetMargins(0,0)
        self.AutoSizeColumns(True)
        self.AutoSizeRows(True)

        gridlib.EVT_GRID_CELL_LEFT_DCLICK(self, self.OnLeftDClick)
        gridlib.EVT_GRID_CELL_CHANGE(self, self.OnCellChange)

    # I do this because I don't like the default behaviour of not starting the
    # cell editor on double clicks, but only a second click.
    def OnLeftDClick(self, evt):
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
    def __calcSum__(self,col):
        dataNodes=self.table.GetDataNodes()
        if len(dataNodes)>0:
            def init(a):
                return 0
            sumLst=map(init,range(0,len(dataNodes[0])+2))
        else:
            return
        dataTypes=self.table.GetDataTypes()
        row=0
        for dataLstNode in dataNodes[:-1]:
            node=dataLstNode[col]
            try:
                #v=int(gridlib.Grid.GetCellValue(self,row,col))
                #sumLst[col]=sumLst[col]+v
                if string.find(dataTypes[col],gridlib.GRID_VALUE_NUMBER)>=0:
                    v=int(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                elif string.find(dataTypes[col],gridlib.GRID_VALUE_LONG)>=0:
                    v=long(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                elif string.find(dataTypes[col],gridlib.GRID_VALUE_FLOAT)>=0:
                    v=float(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                else:
                    pass
            except:
                pass
            row=row+1
        row=len(dataNodes)-1
        if sumLst[col] is not None:
            try:
                gridlib.Grid.SetCellValue(self,row,col,sumLst[col].__str__())
            except:
                pass

    def OnCellChange(self,evt):
        self.__calcSum__(evt.GetCol())
        font = self.GetFont()
        font.SetWeight(wx.BOLD)
        gridlib.Grid.SetCellFont(self,evt.GetRow(),evt.GetCol(),font)
                            
        wx.PostEvent(self,vgpGridAttrInfoChanged(self,evt.GetRow(),evt.GetCol()))
    def Setup(self,node,lstIDNodes,lstFilter,doc):
        self.doc=doc
        #if lstIDNodes is not None:
        #    self.lstIDNodes=lstIDNodes
        #else:
        #    lstIDNodes=self.lstIDNodes
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        #self.table = gridAttrData(None,self.lstFilter)
        #self.SetTable(self.table, True)
        gridlib.Grid.ClearGrid(self)
        self.table = gridAttrData(node,lstIDNodes,lstFilter,doc)
        #self.SetTable(self.table, True)
        self.SetTable(self.table, False)
        self.SetReadOnly()
        self.ForceRefresh()
    def Get(self):
        dataNodes=self.table.GetDataNodes()
        font = self.GetFont()
        row=0
        for dataLstNode in dataNodes[:-1]:
            col=0
            for node in dataLstNode:
                if node is not None:
                    val=self.GetCellValue(row,col)
                    try:
                        s=val.__str__()
                    except:
                        s=val
                    self.doc.setText(node,s)
                    gridlib.Grid.SetCellFont(self,row,col,font)
        
                col=col+1
            row=row+1
        self.ForceRefresh()
        return 1
    def Set(self):
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        dataNodes=self.table.GetDataNodes()
        row=0
        font = self.GetFont()
        font.SetWeight(wx.NORMAL)
        
        for dataLstNode in dataNodes:
            col=0
            for node in dataLstNode:
                if node is not None:
                    sVal=self.doc.getText(node)
                    self.table.SetValue(row,col,sVal)
                    gridlib.Grid.SetCellFont(self,row,col,font)
                col=col+1
            row=row+1
        if len(dataNodes)>0:
            col=0
            for dataLstNode in dataNodes[0]:
                self.__calcSum__(col)
                col=col+1
        self.ForceRefresh()
        return 1
    def SetReadOnly(self):
        dataNodes=self.table.GetDataNodes()
        dataTypes=self.table.GetDataTypes()
        iRowCount=len(dataNodes)
        if len(dataNodes)>0:
            def init(a):
                return 0
            sumLst=map(init,range(0,len(dataNodes[0])))
        else:
            return
        if self.GetNumberCols()>2:
            gridlib.Grid.SetColSize(self,0,20)
            gridlib.Grid.SetColSize(self,1,40)
            gridlib.Grid.SetColSize(self,2,130)

        row=0
        for dataLstNode in dataNodes[:-1]:
            col=0
            for node in dataLstNode:
                if node is None:
                    if col==0:
                        emptyEditor=gridlib.GridCellTextEditor()
                        imgRenderer=gridImageRenderer()
                        #imgRenderer=gridlib.GridCellStringRenderer()
                        gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                        gridlib.Grid.SetCellRenderer(self,row,col,imgRenderer)
                    else:
                        emptyEditor=gridlib.GridCellTextEditor()
                        emptyRenderer=gridlib.GridCellStringRenderer()
                        gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                        gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                        if col>2:
                            gridlib.Grid.SetCellValue(self,row,col,'---')
                    gridlib.Grid.SetReadOnly(self,row,col)
                    gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                else:
                    if gridlib.Grid.IsReadOnly(self,row,col)==False:
                        try:
                            if string.find(dataTypes[col],gridlib.GRID_VALUE_NUMBER)>=0:
                                v=int(gridlib.Grid.GetCellValue(self,row,col))
                                sumLst[col]=sumLst[col]+v
                            elif string.find(dataTypes[col],gridlib.GRID_VALUE_LONG)>=0:
                                v=long(gridlib.Grid.GetCellValue(self,row,col))
                                sumLst[col]=sumLst[col]+v
                            elif string.find(dataTypes[col],gridlib.GRID_VALUE_FLOAT)>=0:
                                v=float(gridlib.Grid.GetCellValue(self,row,col))
                                sumLst[col]=sumLst[col]+v
                            else:
                                pass
                        except:
                            sumLst[col]=None
                col=col+1
            row=row+1
        col=0
        row=iRowCount-1
        for sum in sumLst:
            if col==0:
                emptyEditor=gridlib.GridCellTextEditor()
                imgRenderer=gridImageRenderer()
                gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                gridlib.Grid.SetCellRenderer(self,row,col,imgRenderer)
                #
            else:
                if col>2:
                    if sumLst[col] is not None:
                        try:
                            #emptyEditor=gridlib.GridCellTextEditor()
                            #emptyRenderer=gridlib.GridCellStringRenderer()
                            #gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                            #gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                            gridlib.Grid.SetCellValue(self,row,col,sumLst[col].__str__())
                            font = self.GetFont()
                            font.SetWeight(wx.BOLD)
                            gridlib.Grid.SetCellFont(self,row,col,font)
                            gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_RIGHT,wx.ALIGN_BOTTOM)
                        except:
                            pass
                        
                    else:
                        emptyEditor=gridlib.GridCellTextEditor()
                        emptyRenderer=gridlib.GridCellStringRenderer()
                        gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                        gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                        gridlib.Grid.SetCellValue(self,row,col,'---')
            gridlib.Grid.SetReadOnly(self,row,col)
            gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
            col=col+1
        return 1

#---------------------------------------------------------------------------

class TestFrame(wx.Frame):
    def __init__(self, parent, log):

        wx.Frame.__init__(
            self, parent, -1, "Custom Table, data driven Grid  Demo", size=(640,480)
            )

        p = wx.Panel(self, -1, style=0)
        grid = CustTableGrid(p, log)
        b = wx.Button(p, -1, "Another Control...")
        b.SetDefault()
        self.Bind(wx.EVT_BUTTON, self.OnButton, b)
        b.Bind(wx.EVT_SET_FOCUS, self.OnButtonFocus)
        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(grid, 1, wx.GROW|wx.ALL, 5)
        bs.Add(b)
        p.SetSizer(bs)

    def OnButton(self, evt):
        print "button selected"

    def OnButtonFocus(self, evt):
        print "button focus"


#---------------------------------------------------------------------------

if __name__ == '__main__':
    import sys
    app = wx.PySimpleApp()
    frame = TestFrame(None, sys.stdout)
    frame.Show(True)
    app.MainLoop()


#---------------------------------------------------------------------------
