#Boa:Dialog:vBrowseLinkDialog
#----------------------------------------------------------------------------
# Name:         vBrowseElementAttr4DocDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vBrowseLinkDialog.py,v 1.2 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string
from vidarc.vApps.vPrjEng.vXmlPrjEngTree  import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import images

def create(parent):
    return vBrowseLinkDialog(parent)

[wxID_VBROWSELINKDIALOG, wxID_VBROWSELINKDIALOGGCBBAPPLY, 
 wxID_VBROWSELINKDIALOGGCBBCANCEL, wxID_VBROWSELINKDIALOGGPROCESS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vBrowseLinkDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VBROWSELINKDIALOG,
              name=u'vBrowseLinkDialog', parent=prnt, pos=wx.Point(237, 99),
              size=wx.Size(320, 384), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Browse Element to Link')
        self.SetClientSize(wx.Size(312, 357))
        self.Bind(wx.EVT_ACTIVATE, self.OnVgdBrowseActionNameActivate)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSELINKDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(72, 320), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VBROWSELINKDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSELINKDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(168, 320),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VBROWSELINKDIALOGGCBBCANCEL)

        self.gProcess = wx.Gauge(id=wxID_VBROWSELINKDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(8, 296), range=100,
              size=wx.Size(296, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        #self.selNode=None
        self.node=None
        self.relNode=None
        self.bSet=False
        self.bUpdate=False
        self.bSelect=False
        self.ID2Select=''
        
        self.vgpTree=vXmlPrjEngTree(self,wx.NewId(),
                pos=(4,4),size=(304,280),style=0,name="vgpTree",gui_update=True)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        #EVT_VGTRXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #self.vgpTree.SetProcessBar(self.gProcess,None)
        self.vgpTree.EnableCache()
        self.vgpTree.SetNetResponse(False)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    #def OnTreeItemSel(self,event):
    #    if event.GetTreeNodeData() is not None:
    #        self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        self.vgpTree.SetLanguage(lang)
    def SetRelNode(self,node):
        self.relNode=node
    def SelectByID(self,sID):
        self.bSelect=True
        self.ID2Select=sID
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'SelectByID select:%d id:%s'%(self.bSelect,self.ID2Select),
                origin=self.GetName())
        
    def SetDoc(self,doc,bNet):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'SetDoc net:%d'%(bNet),
                origin=self.GetName())
        self.doc=doc
        self.vgpTree.SetDoc(doc,bNet)
    def SetNode(self,node):
        if self.IsShown():
            if not(self.vgpTree.IsNetResponse()):
                self.vgpTree.SetNetResponse(True)
            self.vgpTree.SetNode(node)
        else:
            self.node=node
        pass
    def GetSelNode(self):
        return self.vgpTree.GetSelected()
        #return self.selNode
    def SetLinkInfo(self,sID):
        self.bSelect=True
        self.ID2Select=sID
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'SetLinkInfo select:%d id:%s'%(self.bSelect,self.ID2Select),
                origin=self.GetName())
        
    def GetLinkInfoById(self,id):
        #print self.vgpTree.GetIds().GetIdStr(id)
        node=self.doc.getIds().GetIdStr(id)[1]
        return self.GetLinkInfo(node)
    def GetLinkInfo(self,node=None):
        if node is None:
            node=self.GetSelNode()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,
        #        'GetLinkInfo id:%s'%(self.doc.getKey(node)),
        #        origin=self.GetName())
        if node is not None:
            rootNode=Hierarchy.getRootNode(self.doc,self.relNode)
            sName=Hierarchy.getTagNames(self.doc,rootNode,self.relNode,node)
            sID=self.doc.getAttribute(node,'id')
            return sID,sName
        return ('','')
    def GetId(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'GetId id:%s'%(self.doc.getKey(self.GetSelNode())),
                origin=self.GetName())
        if self.GetSelNode() is not None:
            return self.doc.getAttribute(self.GetSelNode(),'id')
        return ''
    def __getElementByTagName__(self,tagname):
        for e in ELEMENTS:
            if e[0]==tagname:
                return e
        return None
    def __checkPossible__(self,node):
        if self.node2inst is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.node2inst.tagName
        sType=node.tagName
        elemNode=self.__getElementByTagName__(node.tagName)
        elemNode2Inst=self.__getElementByTagName__(sTypeNode2Inst)
        if elemNode is None:
            return -4
        if elemNode2Inst is None:
            return -3
        try:
            for t in elemNode[1]['__possible']:
                if t[:7]=='__type:':
                    strs=string.split(t,':')
                    if strs[1]==elemNode2Inst[1]['__type']:
                        return 1
                elif t==sTypeNode2Inst:
                    return 1
        except:
            return -5
        return 0
    def OnGcbbApplyButton(self, event):
        #ret=self.__checkPossible__(self.selNode)
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        ret=1
        if ret==1:
            self.EndModal(1)
            event.Skip()
            return
        elif ret==0:
            # adding not possible
            sMsg=u'Instancing at this position not allowed.'
        elif ret==-1:
            # no node to instance
            sMsg=u'No node to instance!'
        elif ret==-2:
            # no node selected
            sMsg=u'No target node selected.'
        elif ret==-3:
            # element to instance not found
            sMsg=u'No element of node to instance found!'
        elif ret==-4:
            # element not found
            sMsg=u'No element of target node found!'
        elif ret==-5:
            # error oocurred
            sMsg=u'A serious error has occurred.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnGcbbCancelButton(self, event):
        if self.vgpTree.IsThreadRunning():
            self.vgpTree.Stop()
            return
        self.EndModal(0)
        event.Skip()
    def CheckNode2Set(self):
        if not(self.vgpTree.IsNetResponse()):
            self.vgpTree.SetNetResponse(True)
            self.vgpTree.SetNode(None)#),['templates'])
            self.bSet=True
        if self.node is not None:
            #self.vgpTree.SetNode(self.node)
            self.bSet=True
            self.node=None
        elif self.bUpdate==True:
            #self.vgpTree.SetNode(self.vgpTree.rootNode)
            self.bSet=True
        self.bUpdate=False
        if self.vgpTree.IsThreadRunning()==False:
            if self.bSelect==True:
                if len(self.ID2Select)>0:
                    self.vgpTree.SelectByID(self.ID2Select)
                self.ID2Select=''
                self.bSelect=False
    def OnVgdBrowseActionNameActivate(self, event):
        self.CheckNode2Set()
        self.vgpTree.SetFocus()
        event.Skip()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'OnAddElementsFin',
                origin=self.GetName())
        wx.CallAfter(self.__select__)
        evt.Skip()
    def __select__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                '__select__ select:%d id:%s'%(self.bSelect,self.ID2Select),
                origin=self.GetName())
        if self.bSelect==True:
            if len(self.ID2Select)>0:
                self.vgpTree.SelectByID(self.ID2Select)
            self.ID2Select=''
            self.bSelect=False
        self.gProcess.SetValue(0)
        
