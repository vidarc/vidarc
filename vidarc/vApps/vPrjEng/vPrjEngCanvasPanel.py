#----------------------------------------------------------------------------
# Name:         vPrjEngCanvasPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vPrjEngCanvasPanel.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.draw.vtDrawCanvasXmlPanel import *
from vidarc.tool.draw.vtDrawCanvasXmlGroup import *

from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import *
from vidarc.tool.draw.vtDrawCanvasBlock import *
from vidarc.tool.draw.vtDrawCanvasBlockIO import *

#from vPrjEngCanvasDialog import vPrjEngCanvasDialog
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog

#from boa.vApps.vPrjEng.vPrjEngCanvasDialog import *
from vidarc.tool.draw.vtDrawCanvasXmlDialog import *
from vidarc.vApps.vPrjEng.vXmlPrjEngAttrTreeList import *

import traceback

def xmlCMcreate(parent,canvas):
    r=vtDrawCanvasRectangle(id=-1,name='rect000',x=0,y=0,w=1,h=1,layer=parent.GetLayer())
    parent.AddXml(r)
    r=vtDrawCanvasRectangle(id=-1,name='rect001',x=1,y=1,w=1,h=1,layer=parent.GetLayer())
    parent.AddXml(r)
    iX,iY,iW,iH=parent.calcExtend(canvas)
    parent.SetWidth(iW)
    parent.SetHeight(iH)

def xmlBlkCreate(node,canvas):
    layer=0
    doc=canvas.GetDocExt()
    nodeExt=canvas.GetNodeExt()
    sName=vtXmlHierarchy.getTagNamesWithRel(doc,nodeExt,None,node)
    print sName
    id=doc.getKey(node)
    o=vtDrawCanvasBlock(id=long(id),name=sName,x=0,y=0,w=8,h=10,layer=layer)
    return o
def xmlBlkAddAttr(obj,node,attr,dfts,canvas):
    layer=0
    doc=canvas.GetDocExt()
    sAttrName=doc.getTagName(attr)
    sAttrVal=doc.getAttributeVal(attr)
    iTyp=vtDrawCanvasBlockIO.TYPE_IN
    iDsp=vtDrawCanvasBlockIO.DISPLAY_VAL
    obj.AddAttr(sAttrName,sAttrVal,defaults,iTyp,iDsp,canvas)
    pass
def xmlBlkDelAttr(o,node,attr,canvas):
    doc=canvas.GetDocExt()
    sAttrName=self.docExt.getTagName(attr)
    pass


class vPrjEngCanvasPanel(vtDrawCanvasXmlPanel):
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vPrjEngCanvas',bTex=False):
        vtDrawCanvasXmlPanel.__init__(self,parent,id,pos,size,style,name)
        
        obj=vtDrawCanvasObjectBase()
        if bTex:
            drv=obj.DRV_LATEX
        else:
            drv=obj.DRV_WX
        self.SetConfig(obj,drv,max=(100,100),grid=(20,20))
        
        self.RegisterObject(vtDrawCanvasLine())
        self.RegisterObject(vtDrawCanvasRectangle())
        self.RegisterObject(vtDrawCanvasText())
        self.RegisterObject(vtDrawCanvasTextMultiLine())
        self.RegisterObject(vtDrawCanvasGroup())
        self.RegisterObject(vtDrawCanvasXmlGroup())
        self.RegisterObject(vtDrawCanvasBlock())
        self.RegisterObject(vtDrawCanvasBlockIO())
        
        #self.RegisterXmlActions('CM',xmlCMcreate)
        for s in ['hw','sw','App','Mod','HW','uproc','proc','op','ph',
                    'ET','ST','AR','PRC','UT','EM','CM',
                    'Prc','PrcS','PrcO','PrcA',
                    'struct','class','array',
                    't_class','t_array','t_char','t_int',
                    't_long','t_float','t_string',
                    'method']:
            self.RegisterXmlActions(s,xmlBlkCreate,xmlBlkAddAttr,xmlBlkDelAttr)
        
    def GetDlg(self):
        if self.dlg is None:
            self.dlg=vtDrawCanvasXmlDialog(self,vXmlPrjEngAttrTreeList)
            self.dlg.SetName(self.name)
            self.dlg.Centre()
        return self.dlg
