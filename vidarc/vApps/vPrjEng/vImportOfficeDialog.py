#Boa:Dialog:vImportOfficeDialog
#----------------------------------------------------------------------------
# Name:         vImportOfficeDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vImportOfficeDialog.py,v 1.4 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import wx.lib.intctrl
import wx.lib.buttons
import sys,fnmatch
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import os,string,time
import thread,traceback
from vidarc.tool.office.vtOfficeSCalc import *
from vidarc.vApps.vPrjEng.vXmlPrjEngTreeList import *

import vidarc.tool.log.vtLog as vtLog

import images

def create(parent):
    return vImportOfficeDialog(parent)

[wxID_VIMPORTOFFICEDIALOG, wxID_VIMPORTOFFICEDIALOGCBADD, 
 wxID_VIMPORTOFFICEDIALOGCBADDINT, wxID_VIMPORTOFFICEDIALOGCBCLEAR, 
 wxID_VIMPORTOFFICEDIALOGCBDEL, wxID_VIMPORTOFFICEDIALOGCBDELINT, 
 wxID_VIMPORTOFFICEDIALOGCBDELTMPL, wxID_VIMPORTOFFICEDIALOGCHCAUTOOPEN, 
 wxID_VIMPORTOFFICEDIALOGCHCSHT, wxID_VIMPORTOFFICEDIALOGCHCTMPL, 
 wxID_VIMPORTOFFICEDIALOGGCBBBROWSEFN, wxID_VIMPORTOFFICEDIALOGGCBBBUILD, 
 wxID_VIMPORTOFFICEDIALOGGCBBCANCEL, wxID_VIMPORTOFFICEDIALOGGCBBIMPORT, 
 wxID_VIMPORTOFFICEDIALOGGCBBLINK, wxID_VIMPORTOFFICEDIALOGGCBBOK, 
 wxID_VIMPORTOFFICEDIALOGGPROC, wxID_VIMPORTOFFICEDIALOGINTCOLEND, 
 wxID_VIMPORTOFFICEDIALOGINTCOLSTART, wxID_VIMPORTOFFICEDIALOGINTROWSTART, 
 wxID_VIMPORTOFFICEDIALOGINTROWSTOP, wxID_VIMPORTOFFICEDIALOGLBLCMP, 
 wxID_VIMPORTOFFICEDIALOGLBLCOL, wxID_VIMPORTOFFICEDIALOGLBLFN, 
 wxID_VIMPORTOFFICEDIALOGLBLNODECOUNT, wxID_VIMPORTOFFICEDIALOGLBLROW, 
 wxID_VIMPORTOFFICEDIALOGLBLSHT, wxID_VIMPORTOFFICEDIALOGLBLTMPL, 
 wxID_VIMPORTOFFICEDIALOGLBLTMPLNAME, wxID_VIMPORTOFFICEDIALOGLSTELEMENT, 
 wxID_VIMPORTOFFICEDIALOGLSTINTERDATA, wxID_VIMPORTOFFICEDIALOGSPROW, 
 wxID_VIMPORTOFFICEDIALOGTXTCOMP, wxID_VIMPORTOFFICEDIALOGTXTFN, 
 wxID_VIMPORTOFFICEDIALOGTXTINTNAME, wxID_VIMPORTOFFICEDIALOGTXTNODECOUNT, 
 wxID_VIMPORTOFFICEDIALOGTXTTMELAPSED, wxID_VIMPORTOFFICEDIALOGTXTTMEST, 
 wxID_VIMPORTOFFICEDIALOGTXTTMPLNAME, 
] = [wx.NewId() for _init_ctrls in range(39)]

# defined event for vgpXmlTree item selected
wxEVT_IMPORT_OFFICE_THREAD_FINSHED=wx.NewEventType()
def EVT_IMPORT_OFFICE_THREAD_FINSHED(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_THREAD_FINSHED,func)
class wxImportOfficeThreadFinshed(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_THREAD_FINSHED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,appl):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_OFFICE_THREAD_FINSHED)
        self.appl=appl
    def GetAppl(self):
        return self.appl

# defined event for vgpXmlTree item selected
wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ=wx.NewEventType()
def EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ,func)
class wxImportOfficeThreadFinshedRead(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(<widget_name>, self.OnItemSel)
    """

    def __init__(self,infos):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ)
        self.infos=infos
    def GetInfos(self):
        return self.infos

# defined event for vgpXmlTree item selected
wxEVT_IMPORT_OFFICE_PROGRESS=wx.NewEventType()
def EVT_IMPORT_OFFICE_PROGRESS(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_PROGRESS,func)
class wxImportOfficeProgress(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_PROGRESS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,sElapsed,sEstimate,iVal,iCount):
        wxPyEvent.__init__(self)
        self.elapsed=sElapsed
        self.estimate=sEstimate
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_IMPORT_OFFICE_PROGRESS)
    def GetElapsed(self):
        return self.elapsed
    def GetEstimate(self):
        return self.estimate
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
        
class thdOpenFile:
    def __init__(self,par):
        self.parent=par
        self.running=False
    def SetFN(self,appl,fn,bStartImport):
        self.appl=appl
        self.fn=fn
        self.bStartImport=bStartImport
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def Run(self):
        self.appl.openFile(self.fn)
        wx.PostEvent(self.parent,wxImportOfficeThreadFinshed(self.appl))
        
class thdReadData:
    def __init__(self,par,verbose=0):
        self.parent=par
        self.running=False
        self.verbose=verbose
    def ReadData(self,fn,sheet,iRowStart,iRowEnd,iColStart,iColEnd):
        self.fn=fn
        self.sheet=sheet
        self.iRowStart=iRowStart
        self.iRowEnd=iRowEnd
        self.iColStart=iColStart
        self.iColEnd=iColEnd
        self.infos=[]
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def Run(self):
        try:
            import pythoncom
            pythoncom.CoInitialize()
            self.appl=vtOfficeSCalc()
            self.appl.openFile(self.fn)
            #vtLog.vtLogCallDepth(self,'fn:%s'%self.fn,callstack=False)
            self.appl.selSheet(self.sheet)
            #vtLog.vtLogCallDepth(self,'sheet:%s'%(self.sheet),callstack=False)
            
            start=time.clock()
            self.infos=[]
            self.__startTime__()
            iCount=self.iRowEnd-self.iRowStart
            self.__showTime__(0,iCount)
            
            for i in range(self.iRowStart,self.iRowEnd):
                #vtLog.vtLogCallDepth(self,'row:%4d %d'%(i,self.iRowEnd),callstack=False)
                if self.keepGoing==False:
                    break
                self.infos.append(self.appl.getCellRange(i,self.iColStart,i,self.iColEnd)[0])
                #vtLog.vtLogCallDepth(self,'proc:%4d %d'%(i,iCount),callstack=False)
                sElapsed,sEstimate=self.__showTime__(i-self.iRowStart,iCount)
                #vtLog.vtLogCallDepth(self,'ela:%s %s'%(sElapsed,sEstimate),callstack=False)
                wx.PostEvent(self.parent,
                        wxImportOfficeProgress(sElapsed,sEstimate,i,iCount))
            self.appl.closeFile()
            self.appl.close()
            if self.keepGoing==False:
                self.infos=None
        except:
            self.infos=None
            traceback.print_exc()
        self.keepGoing = self.running = False
        wx.PostEvent(self.parent,wxImportOfficeThreadFinshedRead(self.infos))
    def __startTime__(self):
        self.zStart=time.clock()
    def __showTime__(self,act,count):
        def __calcTime__(zVal):
            zSec=zVal%60
            zMin=zVal/60
            zVal-=zSec
            zHr=zMin/24
            zMin=zMin%24
            return u'%d:%02d:%02d'%(zHr,zMin,zSec)
        zAct=time.clock()
        zDiff=zAct-self.zStart
        sElapsed=__calcTime__(zDiff)
        if act==0:
            sEstimate=''
        else:
            zDiff=zDiff*count/act
            sEstimate=__calcTime__(zDiff)
        return sElapsed,sEstimate
class vImportOfficeDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VIMPORTOFFICEDIALOG,
              name=u'vImportOfficeDialog', parent=prnt, pos=wx.Point(201, 3),
              size=wx.Size(714, 559), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Import Office')
        self.SetClientSize(wx.Size(706, 532))

        self.lblFN = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLFN,
              label=u'Filename', name=u'lblFN', parent=self, pos=wx.Point(144,
              44), size=wx.Size(42, 13), style=0)

        self.lblTmpl = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLTMPL,
              label=u'Template', name=u'lblTmpl', parent=self, pos=wx.Point(8,
              8), size=wx.Size(44, 13), style=0)

        self.chcTmpl = wx.Choice(choices=[], id=wxID_VIMPORTOFFICEDIALOGCHCTMPL,
              name=u'chcTmpl', parent=self, pos=wx.Point(64, 4),
              size=wx.Size(130, 21), style=0)
        self.chcTmpl.Bind(wx.EVT_CHOICE, self.OnChcTmplChoice,
              id=wxID_VIMPORTOFFICEDIALOGCHCTMPL)

        self.lblTmplName = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLTMPLNAME,
              label=u'Name', name=u'lblTmplName', parent=self, pos=wx.Point(208,
              8), size=wx.Size(28, 13), style=0)

        self.txtTmplName = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTTMPLNAME,
              name=u'txtTmplName', parent=self, pos=wx.Point(248, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.cbDelTmpl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBDELTMPL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelTmpl',
              parent=self, pos=wx.Point(368, 0), size=wx.Size(76, 30), style=0)
        self.cbDelTmpl.Bind(wx.EVT_BUTTON, self.OnCbDelTmplButton,
              id=wxID_VIMPORTOFFICEDIALOGCBDELTMPL)

        self.lstElement = wx.ListView(id=wxID_VIMPORTOFFICEDIALOGLSTELEMENT,
              name=u'lstElement', parent=self, pos=wx.Point(8, 72),
              size=wx.Size(216, 174), style=wx.LC_REPORT)
        self.lstElement.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstElementListItemSelected,
              id=wxID_VIMPORTOFFICEDIALOGLSTELEMENT)
        self.lstElement.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstElementListItemDeselected,
              id=wxID_VIMPORTOFFICEDIALOGLSTELEMENT)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(220, 496), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(344, 496),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBCANCEL)

        self.txtFN = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTFN,
              name=u'txtFN', parent=self, pos=wx.Point(192, 40),
              size=wx.Size(424, 21), style=0, value=u'')

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBBROWSEFN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseFN', parent=self, pos=wx.Point(624, 36),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBBROWSEFN)

        self.intRowStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTOFFICEDIALOGINTROWSTART, limited=False, max=None,
              min=None, name=u'intRowStart', oob_color=wx.RED, parent=self,
              pos=wx.Point(424, 72), size=wx.Size(48, 21), style=0, value=0)

        self.lblRow = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLROW,
              label=u'Row', name=u'lblRow', parent=self, pos=wx.Point(400, 76),
              size=wx.Size(22, 13), style=0)

        self.intRowStop = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTOFFICEDIALOGINTROWSTOP, limited=False, max=None,
              min=None, name=u'intRowStop', oob_color=wx.RED, parent=self,
              pos=wx.Point(475, 72), size=wx.Size(38, 21), style=0, value=0)

        self.lblCol = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLCOL,
              label=u'Col', name=u'lblCol', parent=self, pos=wx.Point(520, 76),
              size=wx.Size(15, 13), style=0)

        self.intColStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTOFFICEDIALOGINTCOLSTART, limited=False, max=None,
              min=None, name=u'intColStart', oob_color=wx.RED, parent=self,
              pos=wx.Point(540, 73), size=wx.Size(38, 21), style=0, value=0)

        self.intColEnd = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTOFFICEDIALOGINTCOLEND, limited=False, max=None,
              min=None, name=u'intColEnd', oob_color=wx.RED, parent=self,
              pos=wx.Point(578, 73), size=wx.Size(38, 21), style=0, value=0)

        self.gcbbImport = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBIMPORT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Import',
              name=u'gcbbImport', parent=self, pos=wx.Point(624, 72),
              size=wx.Size(76, 30), style=0)
        self.gcbbImport.Bind(wx.EVT_BUTTON, self.OnGcbbImportButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBIMPORT)

        self.lstInterData = wx.ListCtrl(id=wxID_VIMPORTOFFICEDIALOGLSTINTERDATA,
              name=u'lstInterData', parent=self, pos=wx.Point(8, 288),
              size=wx.Size(200, 160), style=wx.LC_REPORT)
        self.lstInterData.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInterDataListItemSelected,
              id=wxID_VIMPORTOFFICEDIALOGLSTINTERDATA)
        self.lstInterData.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstInterDataListItemDeselected,
              id=wxID_VIMPORTOFFICEDIALOGLSTINTERDATA)

        self.txtComp = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTCOMP,
              name=u'txtComp', parent=self, pos=wx.Point(360, 396),
              size=wx.Size(150, 21), style=0, value=u'')
        self.txtComp.SetToolTipString(u'[?:][AI*,AI?,FI]')

        self.gcbbLink = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBLINK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Link', name=u'gcbbLink',
              parent=self, pos=wx.Point(216, 392), size=wx.Size(76, 30),
              style=0)
        self.gcbbLink.Bind(wx.EVT_BUTTON, self.OnGcbbLinkButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBLINK)

        self.gcbbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGGCBBBUILD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Build', name=u'gcbbBuild',
              parent=self, pos=wx.Point(624, 426), size=wx.Size(76, 30),
              style=0)
        self.gcbbBuild.Bind(wx.EVT_BUTTON, self.OnGcbbBuildButton,
              id=wxID_VIMPORTOFFICEDIALOGGCBBBUILD)

        self.chcSht = wx.Choice(choices=[], id=wxID_VIMPORTOFFICEDIALOGCHCSHT,
              name=u'chcSht', parent=self, pos=wx.Point(264, 72),
              size=wx.Size(128, 21), style=0)
        self.chcSht.Bind(wx.EVT_CHOICE, self.OnChcShtChoice,
              id=wxID_VIMPORTOFFICEDIALOGCHCSHT)

        self.lblSht = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLSHT,
              label=u'Sheet', name=u'lblSht', parent=self, pos=wx.Point(232,
              72), size=wx.Size(28, 13), style=0)

        self.cbAddInt = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBADDINT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddInt',
              parent=self, pos=wx.Point(216, 304), size=wx.Size(76, 30),
              style=0)
        self.cbAddInt.Bind(wx.EVT_BUTTON, self.OnCbAddIntButton,
              id=wxID_VIMPORTOFFICEDIALOGCBADDINT)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(96, 254), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VIMPORTOFFICEDIALOGCBDEL)

        self.txtIntName = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTINTNAME,
              name=u'txtIntName', parent=self, pos=wx.Point(216, 276),
              size=wx.Size(72, 21), style=0, value=u'')

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self, pos=wx.Point(8, 254), size=wx.Size(76, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VIMPORTOFFICEDIALOGCBADD)

        self.cbDelInt = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBDELINT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelInt',
              parent=self, pos=wx.Point(216, 344), size=wx.Size(76, 30),
              style=0)
        self.cbDelInt.Bind(wx.EVT_BUTTON, self.OnCbDelIntButton,
              id=wxID_VIMPORTOFFICEDIALOGCBDELINT)

        self.lblCmp = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLCMP,
              label=u'Compare', name=u'lblCmp', parent=self, pos=wx.Point(312,
              400), size=wx.Size(42, 13), style=0)

        self.spRow = wx.SpinCtrl(id=wxID_VIMPORTOFFICEDIALOGSPROW, initial=0,
              max=100, min=0, name=u'spRow', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)
        self.spRow.Bind(wx.EVT_SPINCTRL, self.OnSpRowSpinctrl,
              id=wxID_VIMPORTOFFICEDIALOGSPROW)

        self.gProc = wx.Gauge(id=wxID_VIMPORTOFFICEDIALOGGPROC, name=u'gProc',
              parent=self, pos=wx.Point(216, 432), range=100, size=wx.Size(400,
              18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.lblNodeCount = wx.StaticText(id=wxID_VIMPORTOFFICEDIALOGLBLNODECOUNT,
              label=u'Count', name=u'lblNodeCount', parent=self,
              pos=wx.Point(432, 460), size=wx.Size(28, 13), style=0)

        self.txtNodeCount = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTNODECOUNT,
              name=u'txtNodeCount', parent=self, pos=wx.Point(464, 456),
              size=wx.Size(128, 21), style=0, value=u'')

        self.txtTmElapsed = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTTMELAPSED,
              name=u'txtTmElapsed', parent=self, pos=wx.Point(216, 456),
              size=wx.Size(100, 21), style=wx.TE_READONLY, value=u'')

        self.txtTmEst = wx.TextCtrl(id=wxID_VIMPORTOFFICEDIALOGTXTTMEST,
              name=u'txtTmEst', parent=self, pos=wx.Point(320, 456),
              size=wx.Size(100, 21), style=wx.TE_READONLY, value=u'')

        self.cbClear = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTOFFICEDIALOGCBCLEAR,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Clear', name=u'cbClear',
              parent=self, pos=wx.Point(624, 456), size=wx.Size(76, 30),
              style=0)
        self.cbClear.Bind(wx.EVT_BUTTON, self.OnCbClearButton,
              id=wxID_VIMPORTOFFICEDIALOGCBCLEAR)

        self.chcAutoOpen = wx.CheckBox(id=wxID_VIMPORTOFFICEDIALOGCHCAUTOOPEN,
              label=u'auto open', name=u'chcAutoOpen', parent=self,
              pos=wx.Point(480, 8), size=wx.Size(73, 13), style=0)
        self.chcAutoOpen.SetValue(False)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.idxMap=-1
        self.selImp=-1
        self.selInt=-1
        self.selNode=None
        self.doc=None
        self.appl=None
        self.iTmplNodes=-1
        
        self.thdOpen=thdOpenFile(self)
        EVT_IMPORT_OFFICE_THREAD_FINSHED(self,self.OnOpenFin)
        self.thdRead=thdReadData(self,verbose=1)
        EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(self,self.OnReadFin)
        EVT_IMPORT_OFFICE_PROGRESS(self,self.OnReadProgress)
        #self.Bind(EVT_IMPORT_OFFICE_THREAD_FINSHED_READ,self.OnReadFin)
        #self.Bind(EVT_IMPORT_OFFICE_PROGRESS,self.OnReadProgress)
        
        
        self.trlstMap = vXmlPrjEngTreeList(id=wx.NewId(),
              name=u'trlstMap', parent=self, pos=wx.Point(298, 108),
              size=wx.Size(388, 268), style=wx.TR_HAS_BUTTONS,
              cols=[u'tag',u'value',u'calc',u'comp'],
              master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.trlstMap,self.OnTreeItemSel)
        self.trlstMap.SetColumnWidth(0, 150)
        self.trlstMap.SetColumnWidth(1, 40)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trlstMap,self.OnAddFinished)
        
        self.lstElement.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,20)
        self.lstElement.InsertColumn(1,u'Type',wx.LIST_FORMAT_LEFT,180)
        
        self.lstInterData.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,100)
        self.lstInterData.InsertColumn(1,u'Col',wx.LIST_FORMAT_LEFT,100)
        
        img=images.getApplyBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowseFN.SetBitmapLabel(img)
        
        img=images.getImportBitmap()
        self.gcbbImport.SetBitmapLabel(img)
        
        img=images.getLinkBitmap()
        self.gcbbLink.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbBuild.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.cbAdd.SetBitmapLabel(img)
        self.cbAddInt.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.cbDel.SetBitmapLabel(img)
        self.cbDelInt.SetBitmapLabel(img)
        self.cbDelTmpl.SetBitmapLabel(img)
        self.cbClear.SetBitmapLabel(img)
        
    def __updateElem__(self):
        self.lstElement.DeleteAllItems()
        for e in ELEMENTS:
            try:
                index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                self.lstElement.SetStringItem(index,1,e[1]['__type'],-1)
            except:
                pass
    def GetFilter(self):
        lst=[]
        for i in range(0,self.lstElement.GetItemCount()):
            if self.lstElement.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                it=self.lstElement.GetItem(i,0)
                lst.append(it.m_text)
        return lst
    def __getTag__(self,base,node,lst):
        childs=self.doc.getChilds(node)
        for c in childs:
            if len(base)>0:
                s=base+'>'+self.doc.getTagName(c)
            else:
                s=self.doc.getTagName(c)
            lst.append((s,c))
            self.__getTag__(s,c,lst)
    def SetLanguage(self,lang):
        self.trlstMap.SetLanguage(lang)
    def SetLanguages(self,languages,languageIds):
        self.trlstMap.SetLanguages(languages,languageIds)
    def __calcCount__(self,node,skip):
        i=1
        for c in self.doc.getChilds(node):
            if self.doc.getTagName(c) in skip:
                continue
            i+=self.__calcCount__(c,skip)
        return i
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
        self.trlstMap.SetDoc(doc,False)
    def SetNode(self,node,tmplNode,ids,skip=[]):
        vtLog.CallStack('')
        
        self.node=node
        print node
        self.tmplNode=tmplNode
        print tmplNode
        self.lstNewNodes=[]
        
        bSetNode=False
        i=self.__calcCount__(tmplNode,skip)
        print self.iTmplNodes,i
        if self.iTmplNodes<0:
            bSetNode=True
        else:
            if i!=self.iTmplNodes:
                bSetNode=True
        if bSetNode:
            self.trlstMap.SetNode(tmplNode)
        self.iTmplNodes=i
        
        self.chcTmpl.Clear()
        tmplNodes=self.doc.getChilds(node,'io_tmpl')
        for n in tmplNodes:
            sTag=self.doc.getNodeText(n,'name')
            self.chcTmpl.Append(sTag,n)
        
    def __getTmplData__(self,node):
        sName=self.doc.getNodeText(node,'name')
        sFN=self.doc.getNodeText(node,'fn')
        sRowStart=self.doc.getNodeText(node,'row_start')
        sRowEnd=self.doc.getNodeText(node,'row_stop')
        sColStart=self.doc.getNodeText(node,'col_start')
        sColEnd=self.doc.getNodeText(node,'col_stop')
        sSht=self.doc.getNodeText(node,'sheet')
        
        self.txtTmplName.SetValue(sName)
        self.txtFN.SetValue(sFN)
        self.__openOfficeFile__()
        self.chcSht.SetStringSelection(sSht)
        def __conv2Int__(s):
            try:
                return int(s)
            except:
                return 0
        
        self.intRowStart.SetValue(__conv2Int__(sRowStart))
        self.intRowStop.SetValue(__conv2Int__(sRowEnd))
        self.intColStart.SetValue(__conv2Int__(sColStart))
        self.intColEnd.SetValue(__conv2Int__(sColEnd))
        
        self.trlstMap.EnableCache()
        self.trlstMap.SetNode(self.tmplNode)
        self.lstInterData.DeleteAllItems()
        lst=[]
        self.lstNewNodes=[]
        interNode=self.doc.getChild(node,'internal')
        if interNode is not None:
            self.__getTag__('',interNode,lst)
            for item in lst:
                index = self.lstInterData.InsertImageStringItem(sys.maxint, item[0], -1)
                sVal=self.doc.getText(item[1])
                self.lstInterData.SetStringItem(index,1,sVal,-1)
        self.tmplNode=node
    def OnAddFinished(self,evt):
        node=self.tmplNode
        mapNode=self.doc.getChild(node,'map')
        if mapNode is not None:
            for c in self.doc.getChilds(mapNode,'node'):
                id=self.doc.getAttribute(c,'fid')
                res,node=self.trlstMap.__findNodeByID__(None,id)
                if node is None:
                    continue
                for ca in self.doc.getChilds(c):
                    ti=self.trlstMap.FindTreeItem(node)
                    if self.doc.getTagName(ca)=='__node':
                        attrNode=node
                    else:
                        attrNode=self.doc.getChild(node,self.doc.getTagName(ca))
                        ti=self.trlstMap.FindAttr(ti,attrNode)
                    if ti is None:
                        continue
                    sVal=self.doc.getText(ca)
                    sCmp=self.doc.getAttribute(ca,'cmp')
                    self.trlstMap.SetItemText(ti,sVal,2)
                    if len(sCmp)>0:
                        self.trlstMap.SetItemText(ti,sCmp,3)
        evt.Skip()
    def GetNode(self):
        idx=self.chcTmpl.GetSelection()
        sNewName=self.txtTmplName.GetValue()
        bCreate=False
        if idx>=0:
            tmplNode=self.chcTmpl.GetClientData(idx)
            sName=self.doc.getNodeText(tmplNode,'name')
            if sName!=sNewName:
                sName=self.txtTmplName.GetValue()
                bCreate=True
        else:
            bCreate=True
            for c in self.doc.getChilds(self.node,'io_tmpl'):
                sName=self.doc.getNodeText(c,'name')
                if sNewName==sName:
                    bCreate=False
                    tmplNode=c
                    break
        if bCreate:
            tmplNode=self.doc.createSubNode(self.node,'io_tmpl',False)
        sFN=self.txtFN.GetValue()
        sRowStart=str(self.intRowStart.GetValue())
        sRowEnd=str(self.intRowStop.GetValue())
        sColStart=str(self.intColStart.GetValue())
        sColEnd=str(self.intColEnd.GetValue())
        sSht=self.chcSht.GetStringSelection()
        
        self.doc.setNodeText(tmplNode,'name',sNewName)
        self.doc.setNodeText(tmplNode,'fn',sFN)
        self.doc.setNodeText(tmplNode,'sheet',sSht)
        self.doc.setNodeText(tmplNode,'row_start',sRowStart)
        self.doc.setNodeText(tmplNode,'row_stop',sRowEnd)
        self.doc.setNodeText(tmplNode,'col_start',sColStart)
        self.doc.setNodeText(tmplNode,'col_stop',sColEnd)
        
        interNode=self.doc.getChild(tmplNode,'internal')
        if interNode is not None:
            for n in self.doc.getChilds(interNode):
                self.doc.deleteNode(n,interNode)
        else:
            interNode=self.doc.createSubNode(tmplNode,'internal',False)
        iCount=self.lstInterData.GetItemCount()
        for i in range(0,iCount):
            sTag=self.lstInterData.GetItem(i,0).m_text
            sVal=self.lstInterData.GetItem(i,1).m_text
            self.doc.createSubNodeText(interNode,sTag,sVal,False)
        mapNode=self.doc.getChild(tmplNode,'map')
        if mapNode is not None:
            for n in self.doc.getChilds(mapNode):
                self.doc.deleteNode(n,mapNode)
        else:
            mapNode=self.doc.createSubNode(tmplNode,'map',False)
        
        lst=self.trlstMap.GetNodesAttrs(check=[2])
        for it in lst:
            n=it[0]
            id=self.doc.getAttribute(n,'id')
            newNode=self.doc.createSubNode(mapNode,'node')
            self.doc.setAttribute(newNode,'fid',id)
            ti=self.trlstMap.FindTreeItem(n)
            if ti is not None:
                sVal=self.trlstMap.GetItemText(ti,2)
                sComp=self.trlstMap.GetItemText(ti,3)
                if len(sVal)>0:
                    if len(sComp)>0:
                        self.doc.createSubNodeTextAttr(newNode,'__node',sVal,
                                'cmp',sComp,False)
                    else:
                        self.doc.createSubNodeText(newNode,'__node',sVal,
                                False)
            for attrTup in it[1]:
                sTag=self.doc.getTagName(attrTup[1])
                sVal=self.trlstMap.GetItemText(attrTup[0],2)
                sComp=self.trlstMap.GetItemText(attrTup[0],3)
                if len(sComp)>0:
                    self.doc.createSubNodeTextAttr(newNode,sTag,sVal,
                            'cmp',sComp,False)
                else:
                    self.doc.createSubNodeText(newNode,sTag,sVal,
                            False)
        self.doc.AlignNode(self.node)
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def OnGcbbLinkButton(self,event):
        if self.selNode is None:
            return
        ti=self.trlstMap.FindTreeItem(self.selNode)
        if self.selInt>=0:
            sTag=self.lstInterData.GetItem(self.selInt,0).m_text
            sVal=self.lstInterData.GetItem(self.selInt,1).m_text
        else:
            sTag=''
            sVal=''
        sComp=self.txtComp.GetValue()
        self.trlstMap.SetItemText(ti,sVal,2)
        self.trlstMap.SetItemText(ti,sComp,3)
        self.txtComp.SetValue('')
        pass
    def GetNodes(self):
        return self.lstNewNodes
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        if self.thdRead.IsRunning():
            self.thdRead.Stop()
        else:
            self.EndModal(0)
        event.Skip()

    def __openOfficeFile__(self,bForce=False,bStartImport=False):
        if bForce==False:
            if self.chcAutoOpen.GetValue()==0:
                self.appl=None
                return
        if self.appl is not None:
            return
        r=self.gProc.GetRange()/2
        self.gProc.SetValue(r)
        filename=self.txtFN.GetValue()
        self.appl=vtOfficeSCalc()
        if 1==0:
            # try threading
            self.thdOpen.SetFN(self.appl,filename,bStartImport)
        else:
            # none threading
            self.appl.openFile(filename)
            shts=self.appl.getSheetNames()
            self.chcSht.Clear()
            for s in shts:
                self.chcSht.Append(s)
            self.appl.selSheet()
            self.gProc.SetValue(0)
        
    def OnOpenFin(self,evt):
        self.appl=evt.GetAppl()
        shts=self.appl.getSheetNames()
        self.chcSht.Clear()
        for s in shts:
            self.chcSht.Append(s)
        self.appl.selSheet()
        self.gProc.SetValue(0)
        if self.thdOpen.bStartImport:
            self.__startImport__()
            pass
    def OnGcbbBrowseFNButton(self, event):
        dlg = wx.FileDialog(self, "Open", ".", "", "XLS files (*.xls)|*.xls|SXC files (*.sxc)|*.sxc|all files (*.*)|*.*", wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.txtFN.SetValue(filename)
                self.__openOfficeFile__()
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def __showRowInfo__(self):
        self.lstElement.DeleteAllItems()
        try:
            i=0
            for item in self.infos[self.spRow.GetValue()]:
                index = self.lstElement.InsertImageStringItem(sys.maxint, '%d'%i, -1)
                self.lstElement.SetStringItem(index,1,item,-1)
                i=i+1
        except:
            pass
    def OnGcbbImportButton(self, event):
        #self.__openOfficeFile__(True,True)
        self.__startImport__()
    def __startImport__(self):
        self.gcbbOk.Enable(False)
        self.gcbbBrowseFN.Enable(False)
        self.gcbbImport.Enable(False)
        self.gcbbLink.Enable(False)
        self.gcbbBuild.Enable(False)
        self.cbAdd.Enable(False)
        self.cbAddInt.Enable(False)
        self.cbDel.Enable(False)
        self.cbDelInt.Enable(False)
        self.cbDelTmpl.Enable(False)
        self.cbClear.Enable(False)
        
        iRowStart=self.intRowStart.GetValue()-1
        iRowEnd=self.intRowStop.GetValue()-1
        iColStart=self.intColStart.GetValue()-1
        iColEnd=self.intColEnd.GetValue()-1
        sSht=self.chcSht.GetStringSelection()
        fn=self.txtFN.GetValue()
        self.thdRead.ReadData(fn,sSht,iRowStart,iRowEnd,iColStart,iColEnd)
        
    def OnReadFin(self,evt):
        self.gcbbOk.Enable(True)
        self.gcbbBrowseFN.Enable(True)
        self.gcbbImport.Enable(True)
        self.gcbbLink.Enable(True)
        self.gcbbBuild.Enable(True)
        self.cbAdd.Enable(True)
        self.cbAddInt.Enable(True)
        self.cbDel.Enable(True)
        self.cbDelInt.Enable(True)
        self.cbDelTmpl.Enable(True)
        self.cbClear.Enable(True)

        self.infos=evt.GetInfos()
        self.txtTmEst.SetValue(u'')
        self.spRow.SetValue(0)
        self.gProc.SetValue(0)
        if self.infos is not None:
            self.__showRowInfo__()
            self.spRow.SetRange(0,len(self.infos))
        else:
            self.spRow.SetRange(0,0)
        evt.Skip()
    def OnReadProgress(self,evt):
        self.txtTmElapsed.SetValue(evt.GetElapsed())
        self.txtTmEst.SetValue(evt.GetEstimate())
        self.gProc.SetRange(evt.GetCount())
        self.gProc.SetValue(evt.GetValue())
    def __isParent__(self,node,baseNode):
        par=self.doc.getParent(node)
        if par is None:
            return False
        if self.doc.isSame(par,baseNode):
            return True
        else:
            return self.__isParent__(par,baseNode)
    def __getTreeItemValCmp__(self,ti):
        sVal=self.trlstMap.GetItemText(ti,2)
        sComp=self.trlstMap.GetItemText(ti,3)
        strs=string.split(sComp,':')
        if len(strs)==2:
            try:
                iRow=int(strs[0])
            except:
                iRow=0
            return (sVal,strs[1],iRow)
        return (sVal,sComp,0)
    def __startTime__(self):
        self.zStart=time.clock()
    def __showTime__(self,act,count):
        def __calcTime__(zVal):
            zSec=zVal%60
            zMin=zVal/60
            zVal-=zSec
            zHr=zMin/24
            zMin=zMin%24
            return u'%d:%02d:%02d'%(zHr,zMin,zSec)
        zAct=time.clock()
        zDiff=zAct-self.zStart
        self.txtTmElapsed.SetValue(__calcTime__(zDiff))
        if act==0:
            self.txtTmEst.SetValue('')
        else:
            zDiff=zDiff*count/act
            self.txtTmEst.SetValue(__calcTime__(zDiff))
        
    def OnGcbbBuildButton(self, event):
        baseNode=None
        lst=self.trlstMap.GetNodesAttrs(check=[2])
        lstInst=[]
        def __attr2dict__(attrs):
            d={}
            for a in attrs:
                tag=self.doc.getTagName(a[1])
                d[tag]=self.__getTreeItemValCmp__(a[0])
            return d
        self.gProc.SetValue(0)
        self.gProc.SetRange(len(lst))
        i=0
        for it in lst:
            i+=1
            self.gProc.SetValue(i)
            n=it[0]
            id=self.doc.getAttribute(n,'id')
            if baseNode is None:
                baseNode=n
                baseAttr=__attr2dict__(it[1])
                lstNodes=[]
            else:
                if self.__isParent__(n,baseNode)==True:
                    lstNodes.append((n,it[1]))
                else:
                    ti=self.trlstMap.FindTreeItem(baseNode)
                    tup=self.__getTreeItemValCmp__(ti)
                    d={'node':baseNode,'valCmp':tup,'attr':baseAttr,'lstNodes':lstNodes}
                    lstInst.append(d)
                    baseNode=n
                    baseAttr=__attr2dict__(it[1])
                    lstNodes=[]
        if baseNode is not None:
            ti=self.trlstMap.FindTreeItem(baseNode)
            tup=self.__getTreeItemValCmp__(ti)
            d={'node':baseNode,'valCmp':tup,'attr':baseAttr,'lstNodes':lstNodes}
            lstInst.append(d)
        def __procVal__(sVal,iRow):
            try:
                row=self.infos[iRow]
            except:
                return None
            strs=string.split(sVal,'+')
            lst=[]
            for s in strs:
                if len(s)>2:
                    if (s[0]=='$') and (s[-1]=='$'):
                        v=row[int(s[1:-1])]
                        lst.append(v)
                    else:
                        lst.append(s)
                else:
                    lst.append(s)
            return string.join(lst,'')
        def __isMatch__(val,cmp):
            if len(cmp)>0:
                for s in string.split(cmp,','):
                    if fnmatch.fnmatch(val,s)==True:
                        return True
                return False
            else:
                return True
        iMax=self.spRow.GetMax()
        self.lstNewNodes=[]
        self.lstMatched=[]
        self.lstMissed=[]
        self.__startTime__()
        self.__showTime__(0,iMax)
        self.gProc.SetValue(0)
        self.gProc.SetRange(self.spRow.GetMax())
        for iRow in range(0,iMax):
            self.gProc.SetValue(iRow+1)
            self.__showTime__(iRow,iMax)
            iRowAdd=0
            try:
                row=self.infos[iRow]
            except:
                continue
            bFound=False
            itc=None
            for it in lstInst:
                tupValCmp=it['valCmp']
                val=__procVal__(tupValCmp[0],iRow)
                if val is None:
                    continue
                if __isMatch__(val,tupValCmp[1]):
                    bFound=True
                    #itc=copy.deepcopy(it)
                    itc={}
                    itc['node']=it['node']
                    itc['attr']=copy.deepcopy(it['attr'])
                    itc['lstNodes']=it['lstNodes']
                    self.lstMatched.append(tupValCmp)
                    break
                else:
                    self.lstMissed.append(tupValCmp)
            if itc is not None:
                iRowAdd=0
                itc['val']=val
                d=itc['attr']
                for k in d.keys():
                    val=__procVal__(d[k][0],iRow+iRowAdd)
                    d[k]=val
                lst=[]
                for it in itc['lstNodes']:
                    ti=self.trlstMap.FindTreeItem(it[0])
                    tup=self.__getTreeItemValCmp__(ti)
                    val=__procVal__(tup[0],iRow+tup[2])
                    if val is None:
                        continue
                    if __isMatch__(val,tup[1]):
                        if tup[2]>iRowAdd:
                            iRowAdd=tup[2]
                        d=__attr2dict__(it[1])
                        for k in d.keys():
                            val=__procVal__(d[k][0],iRow+tup[2])
                            if val is not None:
                                if __isMatch__(val,d[k][1]):
                                    d[k]=val
                                else:
                                    del d[k]
                        lst.append((it[0],d))
                itc['nodes']=lst
                self.lstNewNodes.append(itc)
            iRow+=iRowAdd
        self.gProc.SetValue(0)
        self.txtTmEst.SetValue(u'')
        self.txtNodeCount.SetValue(str(len(self.lstNewNodes)))
        event.Skip()

    def OnChcShtChoice(self, event):
        idx=event.GetSelection()
        s=self.chcSht.GetString(idx)
        self.appl.selSheet(s)
        event.Skip()

    def OnLstElementListItemSelected(self, event):
        self.selImp=event.GetIndex()
        event.Skip()

    def OnLstElementListItemDeselected(self, event):
        self.selImp=-1
        event.Skip()

    def OnLstInterDataListItemSelected(self, event):
        self.selInt=event.GetIndex()
        event.Skip()
        
    def OnLstInterDataListItemDeselected(self, event):
        self.selInt=-1
        event.Skip()

    def OnCbAddIntButton(self, event):
        s=self.txtIntName.GetValue()
        if len(s)<=0:
            return
        if self.selInt>=0:
            # add 2 selected
            it=self.lstInterData.GetItem(self.selInt,1)
            if len(it.m_text)>0:
                sOp='+'
            else:
                sOp=''
            self.lstInterData.SetStringItem(self.selInt,1,it.m_text+sOp+s,-1)
        else:
            idx=self.lstInterData.InsertImageStringItem(sys.maxint, s, -1)
            pass
        event.Skip()
    def __getColAdr__(self,s,iStart=0):
        i=string.find(s,'$',iStart)
        if i>=0:
            j=string.find(s,'$',i+1)
            if j>0:
                return (i+1,j-1)
        return (-1,-1)
    def OnCbDelButton(self, event):
        sCol=''
        if self.selInt>=0:
            # add 2 selected
            it=self.lstInterData.GetItem(self.selInt,1)
            s=it.m_text
        if self.selImp>=0:
            it=self.lstElement.GetItem(self.selImp,0)
            sCol=it.m_text
            if len(sCol)<=0:
                return
            i=0
            iLen=len(s)
            while (i<iLen) and (i>=0):
                i,j=self.__getColAdr__(s,i)
                if i!=-1:
                    if s[i:j]==sCol:
                        if s[j+2]=='+':
                            j+=1
                        s=s[:i-1]+s[j+1]
                        i=iLen
        else:
            sCol=self.txtIntName.GetValue()
            strs=string.split(s,'+')
            try:
                strs.remove(sCol)
            except:
                pass
            s=string.join(strs,'+')
        self.lstInterData.SetStringItem(self.selInt,1,s,-1)
        event.Skip()

    def OnCbAddButton(self, event):
        if self.selImp<0:
            return
        if self.selInt>=0:
            it=self.lstElement.GetItem(self.selImp,0)
            sCol='$'+it.m_text+'$'
            # add 2 selected
            it=self.lstInterData.GetItem(self.selInt,1)
            if len(it.m_text)>0:
                sOp='+'
            else:
                sOp=''
            self.lstInterData.SetStringItem(self.selInt,1,it.m_text+sOp+sCol,-1)
            
        event.Skip()

    def OnCbDelIntButton(self, event):
        if self.selInt>=0:
            # delete
            self.lstInterData.DeleteItem(self.selInt)
            self.selInt=-1
        event.Skip()

    def OnSpRowSpinctrl(self, event):
        self.__showRowInfo__()
        event.Skip()

    def OnChcTmplChoice(self, event):
        idx=event.GetSelection()
        obj=self.chcTmpl.GetClientData(idx)
        self.__getTmplData__(obj)
        event.Skip()

    def OnCbDelTmplButton(self, event):
        idx=self.chcTmpl.GetSelection()
        if idx<0:
            return
        if self.node is None:
            return
        n=self.chcTmpl.GetClientData(idx)
        self.doc.deleteNode(n)
        self.chcTmpl.Delete(idx)
        event.Skip()

    def OnCbClearButton(self, event):
        self.lstNewNodes=[]
        self.txtNodeCount.SetValue('0')
        event.Skip()
        
