
from boa.tool.xml.vtXmlGrpTreeList import *

from boa.vApps.vPrjEng.vXmlPrjEngTree import IMG_LIST
from boa.vApps.vPrjEng.vXmlPrjEngTree import IMG_DICT
import boa.vApps.vPrjEng.vElementsPanel as Elements

import images
import images_s88

print '\n\n\n\n  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vPrjEngTreeList'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vPrjEngTreeList'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vPrjEngTreeList'
class vPrjEngTreeList(vtXmlGrpTreeList):
    def SetupImageList(self):
        global IMG_LIST
        global IMG_DICT
        if IMG_LIST is None:
            self.imgDict={'elem':{},'attr':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
            self.imgLstTyp=wxImageList(16,16)
            img=images.getElementBitmap()
            mask=wxMask(img,wxBLACK)
            img.SetMask(mask)
            self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
            
            img=images.getElementSelBitmap()
            mask=wxMask(img,wxBLACK)
            img.SetMask(mask)
            self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
            
            img=images.getPrjEngBitmap()
            self.imgDict['elem']['prjengs']=[self.imgLstTyp.Add(img)]
            img=images.getPrjEngSelBitmap()
            self.imgDict['elem']['prjengs'].append(self.imgLstTyp.Add(img))
            
            img=images.getDocGrpBitmap()
            self.imgDict['elem']['maingroup']=[self.imgLstTyp.Add(img)]
            img=images.getDocGrpSelBitmap()
            self.imgDict['elem']['maingroup'].append(self.imgLstTyp.Add(img))
            
            img=images.getSubDocGrpBitmap()
            self.imgDict['elem']['docgroup']=[self.imgLstTyp.Add(img)]
            img=images.getSubDocGrpSelBitmap()
            self.imgDict['elem']['docgroup'].append(self.imgLstTyp.Add(img))
            
            for e in Elements.ELEMENTS:
                try:
                    imgName=e[1]['__img']
                    f = getattr(images_s88, 'get%sBitmap' % imgName)
                    img = f()
                    f = getattr(images_s88, 'get%sImage' % imgName)
                    imgInst = f()
                    for i in range(0,imgInst.GetWidth()):
                        for j in range(0,imgInst.GetHeight()):
                            rgb=[imgInst.GetRed(i,j),imgInst.GetGreen(i,j),imgInst.GetBlue(i,j)]
                            if [1,0,0]!=rgb:
                                def chRgb(a):
                                    a=a|128
                                    return a
                                rgb=map(chRgb,rgb)
                                imgInst.SetRGB(i,j,rgb[0],rgb[1],rgb[2])
                    imgInst=imgInst.ConvertToBitmap()
                    
                    self.imgDict['elem'][e[0]]=[self.imgLstTyp.Add(img)]
                    self.imgDict['elem'][e[0]].append(self.imgLstTyp.Add(img))
                    self.imgDict['elem'][e[0]].append(self.imgLstTyp.Add(imgInst))
                except Exception,list:
                    #print list
                    #self.SetStatusText('open fault. (%s)'%list, 1)
                    pass
            
            img=images_s88.getAttributeBitmap()
            self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
            img=images_s88.getAttributeSelBitmap()
            self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
            
            img=images.getTextBitmap()
            #img=images.getElementBitmap()
            mask=wxMask(img,wxBLUE)
            img.SetMask(mask)
            self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
            
            img=images.getYearBitmap()
            mask=wxMask(img,wxBLUE)
            img.SetMask(mask)
            self.imgDict['grp']['YYYY']=[self.imgLstTyp.Add(img)]
            
            img=images.getMonthBitmap()
            mask=wxMask(img,wxBLUE)
            img.SetMask(mask)
            self.imgDict['grp']['MM']=[self.imgLstTyp.Add(img)]
            
            img=images.getDayBitmap()
            mask=wxMask(img,wxBLUE)
            img.SetMask(mask)
            self.imgDict['grp']['DD']=[self.imgLstTyp.Add(img)]
            
            img=images.getUserBitmap()
            mask=wxMask(img,wxBLACK)
            img.SetMask(mask)
            self.imgDict['grp']['manager']=[self.imgLstTyp.Add(img)]
            
            img=images.getClientBitmap()
            self.imgDict['grp']['attributes:clientshort']=[self.imgLstTyp.Add(img)]
            
            img=images.getTextSelBitmap()
            #img=images.getElementSelBitmap()
            mask=wxMask(img,wxBLUE)
            img.SetMask(mask)
            self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
            #icon=images.getApplicationIcon()
            #self.SetIcon(icon)
            IMG_LIST=self.imgLstTyp
            IMG_DICT=self.imgDict
        else:
            self.imgLstTyp=IMG_LIST
            self.imgDict=IMG_DICT
        self.SetImageList(self.imgLstTyp)
    
