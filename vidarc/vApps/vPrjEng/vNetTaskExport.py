#----------------------------------------------------------------------------
# Name:         vNetTaskExport.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vNetTaskExport.py,v 1.4 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
from vidarc.vApps.vTask.vNetTask import *
from vidarc.tool.net.vNetEditThread import *
from vidarc.tool.net.vNetAddThread import *
import vidarc.tool.log.vtLog as vtLog

import time

VERBOSE=1

#class thdModify(thdNetModifyThread):
#    def __getApplyItem__(self):
#        try:
#            idx=self.dApply['list'][self.dApply['iAct']]
#            return self.dApply['found_list'][idx]
#        except Exception,list:
#            if self.verbose:
#                vtLog.vtLngCallStack(self,vtLog.DEBUG,'exception:%s'%list)
#            return (None,0)
#    def __getApplyNode__(self):
#        print '__getApplyNode__'
#        if self.verbose:
#            vtLog.vtLngCallStack(self,vtLog.DEBUG,'iAct:%d iCount:%d'%(self.dApply['iAct'],self.dApply['iCount']))
#        return self.dApply['list'][self.dApply['iAct']]
    
class vNetTaskExport(vNetTask):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0):
        vNetTask.__init__(self,parent,appl,fn,attr,skip,pos,size,synch,verbose)
        self.netPrjEng=None
        self.prjTaskNode=None
        self.bBlockAdd=False
        self.thdAdd=thdNetAddThread(self,verbose=VERBOSE)
        EVT_NET_ADD_THREAD_ELEMENTS_ABORTED(self,self.OnAddAbort)
        EVT_NET_ADD_THREAD_ELEMENTS_FINISHED(self,self.OnAddFin)
        
        #self.thdModify=thdNetModifyThread(self,verbose=VERBOSE)
        #self.dModify={'iAct':0,'iCount':0,'list':[]}
        #EVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED(self,self.OnModifyAbort)
        #EVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED(self,self.OnModifyFin)
        self.thdEdit=thdNetEditThread(self,verbose=VERBOSE)
        EVT_NET_EDIT_THREAD_ELEMENTS_ABORTED(self,self.OnEditAbort)
        EVT_NET_EDIT_THREAD_ELEMENTS_FINISHED(self,self.OnEditFin)
        
        EVT_NET_XML_CONNECT(self,self.OnTaskConnect)
        EVT_NET_XML_GOT_CONTENT(self,self.OnGotTaskContent)
        
        self.prjId=''
        
    def __stop__(self):
        vNetTask.__stop__(self)
        self.thdAdd.Stop()
        self.thdEdit.Stop()
    def IsRunning(self):
        if vNetTask.__isRunning__(self):
            return True
        if self.thdAdd.IsRunning():
            return True
        if self.thdEdit.IsRunning():
            return True
        return False
    def __stopThreads__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'stop thds',origin='vNetTaskExport')
        self.thdAdd.Stop()
        self.thdEdit.Stop()
        iTime=100
        while (self.thdAdd.IsRunning() or self.thdEdit.IsRunning()):
            time.sleep(1)
            iTime-=1
            if iTime<0:
                return -1
        return 0
    def OnTaskConnect(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'task connected',origin='vNetTaskExport')
        self.__stopThreads__()
        evt.Skip()
    def OnModifyAbort(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'modify aborted',origin='vNetTaskExport')
        evt.Skip()
        
    def OnModifyFin(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'modify finished',origin='vNetTaskExport')
        evt.Skip()
    
    def OnEditAbort(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'edit aborted',origin='vNetTaskExport')
        evt.Skip()
        
    def OnEditFin(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'edit finished',origin='vNetTaskExport')
        evt.Skip()
    
    def OnAddAbort(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'add aborted',origin='vNetTaskExport')
        #self.bBlockAdd=False
        #self.__walkPrjEngNodes__()
        evt.Skip()
        
    def OnAddFin(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'add finished',origin='vNetTaskExport')
        #self.bBlockAdd=False
        #self.__walkPrjEngNodes__()
        self.GenerateSortedIds()
        self.thdEdit.Apply(True)
        evt.Skip()
        
    def __clear__(self):
        self.prjTaskNode=None
    def ClearPrjEng(self):
        self.prjId=''
        self.prjTaskNode=None
    def IsBusy(self):
        #if vNetTask.IsBusy(self):
        #    return True
        if self.thdAdd.IsRunning():
            return True
        if self.thdEdit.IsRunning():
            return True
        return False
    
    def __stop__(self):
        vNetTask.__stop__(self)
        self.thdAdd.Stop()
        self.thdEdit.Stop()
        pass
    
    def SetPrjEngDoc(self,netPrjEng,bNet=False):
        if self.netPrjEng is not None:
            self.netPrjEng.DelConsumer(self)
        self.netPrjEng=netPrjEng
        self.netPrjEng.AddConsumer(self,self.ClearPrjEng)
        if bNet==True:
            self.thdAdd.SetDoc(self)
            self.thdEdit.SetDoc(self)
            EVT_NET_XML_ADD_NODE_RESPONSE(self,self.OnTaskAddNode)
            EVT_NET_XML_ADD_NODE(self,self.OnTaskAddNodeResponse)
            EVT_NET_XML_ADD_NODE_RESPONSE(self.netPrjEng,self.OnAddNodeResponse)
            EVT_NET_XML_SET_NODE(self.netPrjEng,self.OnSetNode)
            EVT_NET_XML_GOT_CONTENT(self.netPrjEng,self.OnGotContent)
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'net:%d'%bNet)
    def OnGotTaskContent(self,evt):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjId:%s'%self.prjId,origin='vNetTaskExport')
        if self.__stopThreads__()<0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'thread :%d %d'%(self.thdAdd.IsRunning(),self.thdEdit.IsRunning()),origin='vNetTaskExport')
            evt.Skip()
            return
        #evt.Skip()
        #return
        self.prjTaskNode=None
        if self.netPrjEng.IsSettled()==False:
            return
        if len(self.prjId)>0:
            childs=self.getChilds(self.getRoot(),'prjtasks')
            for c in childs:
                if self.prjId==self.getAttribute(c,'fid'):
                    self.prjTaskNode=c
                    #self.__walkPrjEngNodes__()
                    break
            if self.prjTaskNode is None:
                #fidPrj=self.doc.getAttribute(self.doc.getBaseNode(),'prjfid')
                #prjInfo=self.netPrj.GetPrj(prjid)
                #prjInfo=self.netPrjEng.getChild(self.netPrjEng.getRoot(),'prjinfo')
                #if prjInfo is None:
                #    evt.Skip()
                #    return
                #fidPrj=self.netPrjEng.getAttribute(prjInfo,'fid')
                self.prjTaskNode=self.createSubNode(self.getRoot(),'prjtasks')
                self.setAttribute(self.prjTaskNode,'fid',self.prjId)
                self.thdAdd.ScheduleAdd(None,self.prjTaskNode)
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjtask added node:%s'%self.prjTaskNode,origin='vNetTaskExport')
                self.__walkPrjEngNodes__()
        else:
            self.prjTaskNode=None
        #self.__walkPrjEngNodes__()
        
        evt.Skip()
    def OnGotContent(self,evt):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'verbose:%d'%self.verbose,verbose=1,origin='vNetTaskExport')
        self.prjTaskNode=None
        #node=self.netPrjEng.getChild(self.netPrjEng.getRoot(),'prjinfo')
        self.prjId=self.netPrjEng.getAttribute(self.netPrjEng.getBaseNode(),'prjfid')
        #prjInfo=self.netPrj.GetPrj(prjid)
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjfid:%s'%self.prjId,origin='vNetTaskExport')
        
        #if node is not None:
        #    self.prjId=self.netPrjEng.getAttribute(node,'fid')
        #else:
        #    self.prjId=''
        #    evt.Skip()
        #    return
        if self.IsSettled()==False:
            return
        if len(self.prjId)<=0:
            evt.Skip()
            return
        if self.__stopThreads__()<0:
            evt.Skip()
            return
        if self.getRoot() is None:
            return
        childs=self.getChilds(self.getRoot(),'prjtasks')
        for c in childs:
            if self.prjId==self.getAttribute(c,'fid'):
                self.prjTaskNode=c
                #self.__walkPrjEngNodes__()
                break
        if self.prjTaskNode is None:
            n=self.createSubNodeDict(self.getRoot(),{'tag':'prjtasks',
                    'attr':('fid',self.prjId)})
            self.AlignNode(n,iRec=2)
            #self.addNode(parNode,n)
            self.thdAdd.ScheduleAdd(self.getRoot(),n)
            self.prjTaskNode=n
            self.thdAdd.Apply(True)
        if 1==0:
            iLen=len(self.dModify['list'])
            if iLen>0:
                self.dModify['iCount']=iLen
                self.dModify['iAct']=0
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'dict:%s'%self.dModify,origin='vNetTaskExport')
                self.thdModify.Apply(self.netTask,self.dModify)
        evt.Skip()
        
    def __walkPrjEngNodes__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'walk nodes',origin='vNetTaskExport')
        if self.prjTaskNode is None:
            return
        prjEngNode=self.netPrjEng.getBaseNode()
        for s in ['instance','project','admin']:
            node=self.netPrjEng.getChild(prjEngNode,s)
            if node is not None:
                accountNodes=self.netPrjEng.getChildsRec(node,'account')
                for account in accountNodes:
                    if self.bBlockAdd==True:
                        return
                    accTasks=self.netPrjEng.getChilds(account,'account_task')
                    for aTask in accTasks:
                        if self.bBlockAdd==True:
                            return
                        self.__addAccountTask__(aTask)
                        for aTask in accTasks:
                            if self.bBlockAdd==True:
                                return
                            accSubTasks=self.netPrjEng.getChilds(aTask,'account_subtask')
                            for aSubTask in accSubTasks:
                                if self.bBlockAdd==True:
                                    return
                                self.__addAccountSubTask__(aSubTask)
        self.thdAdd.Apply(True)
    def OnSetNode(self,evt):
        if len(self.prjId)<=0:
            evt.Skip()
            return
        id=evt.GetID()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'id:%s'%(id),origin='vNetTaskExport')
        node=self.netPrjEng.getNodeByIdNum(long(id))
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'node:%s'%node,origin='vNetTaskExport')
        tagName=self.netPrjEng.getTagName(node)
        if tagName=='account_task':
            self.__addAccountTask__(node)
            pass
        elif tagName=='account_subtask':
            self.__addAccountSubTask__(node)
            pass
        self.thdAdd.Apply(True)
        evt.Skip()
    
    def OnTaskAddNode(self,evt):
        idPar=evt.GetID()
        idNew=evt.GetNewID()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'idPar:%s idNew:%s'%(idPar,idNew),origin='vNetTaskExport')
            #vtLog.vtCallStack(3,VERBOSE_ADD_NODE)
        #self.bBlockAdd=False
        #self.GenerateTaskIds()
        #prjTaskIds=self.GetTaskIds()
        #print prjTaskIds.keys()
        #taskIds=prjTaskIds[self.prjId][1]
        #self.__walkPrjEngNodes__()
        evt.Skip()
        
    def OnTaskAddNodeResponse(self,evt):
        idPar=evt.GetID()
        idNew=evt.GetNewID()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'idPar:%s idNew:%s'%(idPar,idNew),origin='vNetTaskExport')
            #vtLog.vtCallStack(3,VERBOSE_ADD_NODE)
        evt.Skip()
    
    def OnAddNodeResponse(self,evt):
        if len(self.prjId)<=0:
            evt.Skip()
            return
        id=evt.GetID()
        idNew=evt.GetNewID()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'id:%s idNew:%s'%(id,idNew),origin='vNetTaskExport')
        node=self.netPrjEng.getNodeByIdNum(long(idNew))
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'node:%s'%node,origin='vNetTaskExport')
        tagName=self.netPrjEng.getTagName(node)
        if tagName=='account_task':
            self.__addAccountTask__(node)
            pass
        elif tagName=='account_subtask':
            self.__addAccountSubTask__(node)
            pass
        self.thdAdd.Apply(True)
        evt.Skip()
    
    def __getHierarchy__(self,baseNode,n):
        if baseNode is None:
            return ''
        if not self.netPrjEng.isSame(baseNode,n):
            return self.__getHierarchy__(baseNode,self.netPrjEng.getParent(n))+'<'+self.netPrjEng.getNodeText(n,'tag')+'>'#n.tagName
        else:
            return ''
    def __addAccountTask__(self,aTask):
        #if self.bBlockAdd==True:
        #    return
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'task:%s block:%d'%(self.getKey(aTask),self.bBlockAdd),origin='vNetTaskExport')
        node=self.__getTaskById__(self.netPrjEng.getKey(aTask))
        if 1==0:
            try:
                prjTaskIds=self.GetTaskIds()
                taskIds=prjTaskIds[self.prjId][1]
                id=self.netPrjEng.getKey(aTask)
                tup=taskIds[id]
                node=tup[0]
            except:
                node=None
        sn=self.netPrjEng.getChild(aTask,'task')
        if sn is not None:
            if len(self.netPrjEng.getAttribute(sn,'ih'))>0:
                sTask=self.netPrjEng.getText(sn)
            elif len(self.netPrjEng.getAttribute(sn,'fid'))>0:
                sTask=self.netPrjEng.getText(sn)
            else:
                sTask=self.netPrjEng.getNodeText(sn,'val')
        else:
            sTask=''
        val=Hierarchy.getTagNames(self.netPrjEng,self.netPrjEng.getBaseNode(),None,aTask)+u'.'+sTask
        
        fidTask=self.netPrjEng.getAttribute(aTask,'id')
        if node is None:
            # add node
            d={'tag':'name',
                'val':val}
            
            n=self.createSubNodeDict(self.prjTaskNode,{'tag':'task',
                    'attr':('fid',fidTask),
                    'lst':[d]})
            self.AlignNode(n,iRec=2)
            #self.addNode(parNode,n)
            self.thdAdd.ScheduleAdd(self.prjTaskNode,n)
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'task added node:%s n:%s'%(self.prjTaskNode,n),origin='vNetTaskExport')
            #self.bBlockAdd=True
        else:
            oldVal=self.getNodeText(node,'name')
            if oldVal!=val:
                self.setNodeText(node,'name',val)
                self.thdEdit.ScheduleEdit(node)
                #self.dModify['list'].append(node)
                #self.doEdit(node)
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'scheduled task edited node:%s'%(node),origin='vNetTaskExport')
    def __getTaskById__(self,id):
        if self.prjTaskNode is None:
            return None
        for c in self.getChilds(self.prjTaskNode,'task'):
            if id==self.getAttribute(c,'fid'):
                return c
        return None
        try:
            prjTaskIds=self.GetTaskIds()
            taskIds=prjTaskIds[self.prjId][1]
        except:
            return None
        try:
            tup=taskIds[id]
            node=tup[0]
        except:
            node=None
        if node is None:
            for par,n in self.thdAdd.lst:
                fid=self.getAttribute(n,'fid')
                if fid==id:
                    return n
        return node
    def __getSubTaskById__(self,id,subid):
        taskNode=self.__getTaskById__(id)
        if taskNode is None:
            return
        for c in self.getChilds(taskNode,'subtask'):
            if self.getAttribute(c,'fid')==subid:
                return c
        return None
        try:
            prjTaskIds=self.GetTaskIds()
            taskIds=prjTaskIds[self.prjId][1]
        except:
            return None
        try:
            subTaskIds=taskIds[id][1]
            node=subTaskIds[subid]
            node=tup[0]
        except:
            node=None
        if node is None:
            for n in self.thdAdd.lst:
                fid=self.getAttribute(n[1],'fid')
                if fid==id:
                    return n
        return None
    def __addAccountSubTask__(self,aSubTask):
        #return
        if self.bBlockAdd==True:
            return
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'task:%s block:%d'%(self.getKey(aSubTask),self.bBlockAdd),origin='vNetTaskExport')
        aTask=self.netPrjEng.getParent(aSubTask)
        id=self.netPrjEng.getKey(aTask)
        subid=self.netPrjEng.getKey(aSubTask)
        node=self.__getSubTaskById__(id,subid)
        if 1==0:
            try:
                prjTaskIds=self.GetTaskIds()
                taskIds=prjTaskIds[self.prjId][1]
            except:
                return
            try:
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjid:%s tasks:%s'%(self.prjId,taskIds),origin='vNetTaskExport')
                aTask=self.netPrjEng.getParent(aSubTask)
                id=self.netPrjEng.getKey(aTask)
                subTaskIds=taskIds[id][1]
                id=self.netPrjEng.getKey(aSubTask)
                node=subTaskIds[id]
            except Exception,list:
                node=None
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.ERROR,'exception:%s %s'%(Exception,list),origin='vNetTaskExport')
                
                #print prjTaskIds.keys()
                #self.bBlockAdd=True
                #return
        sSubTask=self.netPrjEng.getNodeText(aSubTask,'tag')
        fidTask=self.netPrjEng.getAttribute(aSubTask,'id')
        sn=self.netPrjEng.getChild(aSubTask,'subtask')
        if sn is not None:
            if len(self.netPrjEng.getAttribute(sn,'ih'))>0:
                val=self.netPrjEng.getText(sn)
            elif len(self.netPrjEng.getAttribute(sn,'fid'))>0:
                val=self.netPrjEng.getText(sn)
            else:
                val=self.netPrjEng.getNodeText(sn,'val')
        else:
            val=''
        val=string.replace(sSubTask,'.','_')+u'.'+string.replace(val,'.','_')
        if node is None:
            # add new one
            idPar=self.netPrjEng.getKey(self.netPrjEng.getParent(aSubTask))
            node=self.__getTaskById__(idPar)
            #print node
            #tup=taskIds[idPar]
            #node=tup[0]
            if node is None:
                return
            
            d={'tag':'name',
                'val':val}
            
            n=self.createSubNodeDict(node,{'tag':'subtask',
                    'attr':('fid',fidTask),
                    'lst':[d]})
            self.AlignNode(n,iRec=2)
            #print n
            #n=self.createSubNodeTextAttr(node,'subtask',val,'fid',fidTask)
            #self.AlignNode(n)
            #self.addNode(node,n)
            self.thdAdd.ScheduleAdd(node,n)
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'subtask added node:%s n:%s'%(node,n),origin='vNetTaskExport')
            #self.bBlockAdd=True
        else:
            oldVal=self.getNodeText(node,'name')
            if oldVal!=val:
                self.setNodeText(node,'name',val)
                self.thdEdit.ScheduleEdit(node)
                #self.doEdit(node)
                #self.dModify['list'].append(node)
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'schedule subtask edited node:%s'%(node),origin='vNetTaskExport')
        
