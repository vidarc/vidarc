#Boa:Dialog:vInstanceShowDialog
#----------------------------------------------------------------------------
# Name:         vInstanceShowDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vInstanceShowDialog.py,v 1.2 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.Elements as prjEngElements

import string,sys

import images

def create(parent):
    return vInstanceShowDialog(parent)

[wxID_VINSTANCESHOWDIALOG, wxID_VINSTANCESHOWDIALOGCBAPPLY, 
 wxID_VINSTANCESHOWDIALOGCBCANCEL, wxID_VINSTANCESHOWDIALOGCBDEL, 
 wxID_VINSTANCESHOWDIALOGCBGOTO, wxID_VINSTANCESHOWDIALOGCLSTNODES, 
 wxID_VINSTANCESHOWDIALOGLSTINST, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vInstanceShowDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VINSTANCESHOWDIALOG,
              name=u'vInstanceShowDialog', parent=prnt, pos=wx.Point(363, 73),
              size=wx.Size(400, 429), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vInstanced Nodes Dialog')
        self.SetClientSize(wx.Size(392, 402))

        self.lstInst = wx.ListCtrl(id=wxID_VINSTANCESHOWDIALOGLSTINST,
              name=u'lstInst', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(376, 240), style=wx.LC_REPORT)
        self.lstInst.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstInstListColClick,
              id=wxID_VINSTANCESHOWDIALOGLSTINST)
        self.lstInst.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstInstListItemDeselected,
              id=wxID_VINSTANCESHOWDIALOGLSTINST)
        self.lstInst.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInstListItemSelected,
              id=wxID_VINSTANCESHOWDIALOGLSTINST)
        self.lstInst.Bind(wx.EVT_LEFT_DCLICK, self.OnLstInstLeftDclick)

        self.cbGoTo = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCESHOWDIALOGCBGOTO,
              bitmap=wx.EmptyBitmap(16, 16), label=u'go to', name=u'cbGoTo',
              parent=self, pos=wx.Point(8, 256), size=wx.Size(76, 30), style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,
              id=wxID_VINSTANCESHOWDIALOGCBGOTO)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCESHOWDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'cbDel', parent=self,
              pos=wx.Point(264, 256), size=wx.Size(76, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VINSTANCESHOWDIALOGCBDEL)

        self.clstNodes = wx.CheckListBox(choices=[],
              id=wxID_VINSTANCESHOWDIALOGCLSTNODES, name=u'clstNodes',
              parent=self, pos=wx.Point(8, 296), size=wx.Size(376, 64),
              style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCESHOWDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(104, 368), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VINSTANCESHOWDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINSTANCESHOWDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(208, 368), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VINSTANCESHOWDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.iCol=0
        self.bOrder=True
        self.selItem=-1
        self.vgpTree=None
        self.doc=None
        
        img=images.getApplyBitmap()
        self.cbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
        img=images.getNavBitmap()
        self.cbGoTo.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.cbDel.SetBitmapLabel(img)
        
        self.lstInst.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,180)
        self.lstInst.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,85)
        self.lstInst.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,90)
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node,vgpTree):
        self.vgpTree=vgpTree
        self.node=node
        childs=self.doc.getChilds(node,'__node')
        self.clstNodes.Clear()
        self.instancedNodes=[]
        self.deinstNodes=[]
        if len(childs)<=0:
            return
        cNode=self.doc.GetIdStr(self.doc.getAttribute(childs[0],'iid'))[1]
        rootNode=Hierarchy.getRootNode(self.doc,cNode)
        for c in childs:
            try:
                cNode=self.doc.GetIdStr(self.doc.getAttribute(c,'iid'))[1]
                sHier=Hierarchy.getTagNames(self.doc,rootNode,None,cNode)
                sTag=self.doc.getNodeText(cNode,'tag')
                sName=self.doc.getNodeText(cNode,'name')
                self.instancedNodes.append((sHier,sTag,sName,cNode))
            except:
                pass
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        self.instancedNodes.sort(cmpFunc)
        self.__showInst__()
        pass
    def __showInst__(self):
        self.selItem=-1
        self.lstInst.DeleteAllItems()
        def cmpFunc(a,b):
            if self.bOrder:
                return cmp(a[self.iCol],b[self.iCol])
            else:
                return cmp(b[self.iCol],a[self.iCol])
        self.instancedNodes.sort(cmpFunc)
        for it in self.instancedNodes:
            index = self.lstInst.InsertImageStringItem(sys.maxint, it[0], -1)
            self.lstInst.SetStringItem(index,1,it[1],-1)
            self.lstInst.SetStringItem(index,2,it[2],-1)
            
    def __deInstanceCheck__(self,tmplNode,instNode):
        def filterFunc(n):
            tagName=self.doc.getTagName(n)
            if tagName in ['Safe','Log']:
                return False
            if tagName[:2]=='__':
                return False
            else:
                return True
        tmplChilds=filter(filterFunc,self.doc.getChilds(tmplNode))
        instChilds=filter(filterFunc,self.doc.getChilds(instNode))
        # perform instance check
        for tc,ic in zip(tmplChilds,instChilds):
            if self.doc.getTagName(tc)!=self.doc.getTagName(ic):
                #print 'check fault -1',tc,ic
                return -1
            iid=self.doc.getAttribute(ic,'iid')
            if len(iid)>0:
                if not self.doc.isSame(self.ids.GetIdStr(iid)[1],tc):
                    return -2
                if self.__deInstanceCheck__(tc,ic)<0:
                    return -1
            else:
                if self.doc.getTagName(tc) in ['tag','name','description']:
                    pass
                else:
                    if len(self.doc.getAttribute(ic,'ih'))<=0:
                        if len(self.doc.getAttribute(ic,'fid'))<=0:
                            return -3
        return 0
    def __deInstance__(self,tmplNode,instNode):
        def filterFunc(n):
            tagName=self.doc.getTagName(n)
            if tagName in ['Safe','Log']:
                return False
            if tagName[:2]=='__':
                return False
            else:
                return True
        tmplChilds=filter(filterFunc,self.doc.getChilds(tmplNode))
        instChilds=filter(filterFunc,self.doc.getChilds(instNode))
        # perform instance
        for tc,ic in zip(tmplChilds,instChilds):
            #if tc.tagName!=ic.tagName:
            #    return -1
            iid=self.doc.getAttribute(ic,'iid')
            if len(iid)>0:
                #if self.ids.GetIdStr(iid)[1]!=tc:
                #    return -2
                self.__deInstance__(tc,ic)
            else:
                if self.doc.getTagName(tc) in ['tag','name','description']:
                    pass
                else:
                    if len(self.doc.getAttribute(ic,'ih'))>0:
                        self.doc.removeAttribute(ic,'ih')
                        childs=self.doc.getChilds(tc)
                        for c in childs:
                            nc=self.doc.cloneNode(c,True)
                            self.doc.appendChild(ic,nc)
                        val=self.doc.getText(ic)
                        self.doc.setNodeText(ic,'val',val)
                        self.doc.setText(ic,'')
                    if len(self.doc.getAttribute(ic,'fid'))>0:
                        fid=self.doc.getAttribute(ic,'fid')
                        #fNode=self.ids.GetIdStr(fid)[1]
                        #self.doc.removeAttribute(ic,'fid')
                        #childs=self.doc.getChilds(fNode)
                        
        id=self.doc.getAttribute(instNode,'id')
        tmplChilds=self.doc.getChilds(tmplNode)
        for tc in tmplChilds:
            if self.doc.getTagName(tc)=='__node':
                #print tc.getAttribute('iid'),id
                if self.doc.getAttribute(tc,'iid')==id:
                    self.doc.deleteNode(tc,tmplNode)
        self.doc.removeAttribute(instNode,'iid')
        # remove revisions
        instChilds=self.doc.getChilds(instNode,'Safe')
        for ic in instChilds:
            self.doc.deleteNode(ic,instNode)
            
    def OnCbApplyButton(self, event):
        for i in range(self.clstNodes.GetCount()):
            if self.clstNodes.IsChecked(i)==True:
                if self.__deInstanceCheck__(self.node,self.deinstNodes[i][3])>=0:
                    self.__deInstance__(self.node,self.deinstNodes[i][3])
                    id=self.doc.getAttribute(self.deinstNodes[i][3],'id')
                    self.vgpTree.RefreshByID(id)
                    self.doc.AlignNode(self.deinstNodes[i][3])
                # delete instance
                pass
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnLstInstListColClick(self, event):
        col=event.GetColumn()
        if col>=0:
            if col!=self.iCol:
                self.iCol=col
                self.bOrder=True
            else:
                self.bOrder=not(self.bOrder)
        self.__showInst__()
        event.Skip()

    def OnLstInstListItemDeselected(self, event):
        self.selItem=-1
        event.Skip()

    def OnLstInstListItemSelected(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnCbGoToButton(self, event):
        if self.selItem<0:
            return
        node=self.instancedNodes[self.selItem][3]
        id=self.doc.getAttribute(node,'id')
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        event.Skip()

    def OnLstInstLeftDclick(self, event):
        if self.selItem<0:
            return
        node=self.instancedNodes[self.selItem][3]
        id=self.doc.getAttribute(node,'id')
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        event.Skip()

    def OnCbDelButton(self, event):
        if self.selItem<0:
            return
        it=self.instancedNodes[self.selItem]
        if self.clstNodes.FindString(it[0])<0:
            self.deinstNodes.append(it)
            self.clstNodes.Append(it[0])
            i=self.clstNodes.GetCount()-1
            self.clstNodes.Check(i)
        event.Skip()
