#Boa:Dialog:vFindDialog
#----------------------------------------------------------------------------
# Name:         vFindDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vFindDialog.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.vApps.vPrjEng.Elements as Elements
import vidarc.vApps.vPrjEng.vElementsPanel as ELEMENTS
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import sys,string,fnmatch,thread
import images

def create(parent):
    return vFindDialog(parent)

# defined event for vgpXmlTree item selected
wxEVT_THREAD_FIND_ELEMENTS=wx.NewEventType()
def EVT_THREAD_FIND_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FIND_ELEMENTS,func)
class wxThreadFindElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FIND_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iStep,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.step=iStep
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_FIND_ELEMENTS)
    def GetStep(self):
        return self.step
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_FIND_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_FIND_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FIND_ELEMENTS_FINISHED,func)
class wxThreadFindElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FIND_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_FIND_ELEMENTS_FINISHED)
    

class thdFind:
    def __init__(self,par):
        self.tree=par
        self.running = False
        self.lstSort=None
    def Find(self,node,ids,sType,sFltHier,sFltTag,sFltName):
        self.node=node
        self.ids=ids
        self.sType=sType
        self.sFltHier=sFltHier
        self.sFltTag=sFltTag
        self.sFltName=sFltName
        self.bFind=True
        self.Start()
        self.lstSort=None
    def Clear(self):
        self.iCount=0
        self.iFound=0
            
        self.dElem={}
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procNode__(self,node):
        if self.keepGoing==False:
            return
        bFound=True
        sTag=None
        sHier=None
        sTagName=None
        sName=None
        self.iAct+=1
        wx.PostEvent(self.tree,wxThreadFindElements(1,self.iAct))
        if self.sType:
            sTag=self.tree.doc.getTagName(node)
            if sTag!=self.sType:
                bFound=False
        if bFound and self.sFltHier:
            sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
            if fnmatch.fnmatch(sHier,self.sFltHier)==False:
                bFound=False
        if bFound and self.sFltTag:
            sTagName=self.tree.doc.getNodeText(node,'tag')
            if fnmatch.fnmatch(sTagName,self.sFltTag)==False:
                bFound=False
        if bFound and self.sFltName:
            sName=self.tree.doc.getNodeText(node,'name')
            if fnmatch.fnmatch(sName,self.sFltName)==False:
                bFound=False
        if bFound:
            self.iFound+=1
            if sTag is None:
                sTag=self.tree.doc.getTagName(node)
            if sHier is None:
                sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
            if sTagName is None:
                sTagName=self.tree.doc.getNodeText(node,'tag')
            if sName is None:
                sName=self.tree.doc.getNodeText(node,'name')
            try:
                lst=self.dElem[sTag]
                lst.append((sHier,sTagName,sName,node))
            except:
                self.dElem[sTag]=[(sHier,sTagName,sName,node)]
        self.iCount+=1
        childs=self.tree.doc.getChildsAttr(node,'id')
        for c in childs:
            self.__procNode__(c)
    def __procIDs__(self):
        id_keys=self.ids.GetIDs()
        for k in id_keys:
            if self.keepGoing==False:
                return
            node=self.ids.GetId(k)[1]
            bFound=True
            sTag=None
            sHier=None
            sTagName=None
            sName=None
            self.iAct+=1
            wx.PostEvent(self.tree,wxThreadFindElements(1,self.iAct))
            if self.sType:
                sTag=self.tree.doc.getTagName(node)
                if sTag!=self.sType:
                    bFound=False
            if bFound and self.sFltHier:
                sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
                if fnmatch.fnmatch(sHier,self.sFltHier)==False:
                    bFound=False
            if bFound and self.sFltTag:
                sTagName=self.tree.doc.getNodeText(node,'tag')
                if fnmatch.fnmatch(sTagName,self.sFltTag)==False:
                    bFound=False
            if bFound and self.sFltName:
                sName=self.tree.doc.getNodeText(node,'name')
                if fnmatch.fnmatch(sName,self.sFltName)==False:
                    bFound=False
            if bFound:
                self.iFound+=1
                if sTag is None:
                    sTag=self.tree.doc.getTagName(node)
                if sHier is None:
                    sHier=Hierarchy.getTagNames(self.tree.doc,self.node,None,node)
                if sTagName is None:
                    sTagName=self.tree.doc.getNodeText(node,'tag')
                if sName is None:
                    sName=self.tree.doc.getNodeText(node,'name')
                try:
                    lst=self.dElem[sTag]
                    lst.append((sHier,sTagName,sName,node))
                except:
                    self.dElem[sTag]=[(sHier,sTagName,sName,node)]
            self.iCount+=1
    
    def __showNodes__(self):
        self.tree.lstItem.DeleteAllItems()
        keys=self.dElem.keys()
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
        for k in keys:
            lst=self.dElem[k]
            for tup in lst:
                self.iAct+=1
                self.lstSort.append((tup[self.tree.iCol],tup))
                wx.PostEvent(self.tree,wxThreadFindElements(2,self.iAct,self.iFound))
                if self.keepGoing==False:
                    return
        def cmpFunc(a,b):
            if self.tree.bOrder:
                return cmp(a[0],b[0])
            else:
                return cmp(b[0],a[0])
        self.lstSort.sort(cmpFunc)
        for it in self.lstSort:
            tup=it[1]
            index = self.tree.lstItem.InsertImageStringItem(sys.maxint, tup[0], -1)
            self.tree.lstItem.SetStringItem(index,1,tup[1],-1)
            self.tree.lstItem.SetStringItem(index,2,tup[2],-1)
            wx.PostEvent(self.tree,wxThreadFindElements(3,index,self.iFound))
            if self.keepGoing==False:
                return
    def Run(self):
        if self.bFind:
            self.Clear()
            if self.ids is None:
                childs=self.tree.doc.getChilds(self.node)
                for c in childs:
                    self.__procNode__(c)
            else:
                self.__procIDs__()
        if self.keepGoing:
            self.__showNodes__()
        if self.keepGoing==False:
            self.tree.lstItem.DeleteAllItems()
        self.keepGoing = self.running = False
        wx.PostEvent(self.tree,wxThreadFindElements(0,0))
        wx.PostEvent(self.tree,wxThreadFindElementsFinished())
    def GetCount(self):
        return self.iCount
    def GetFound(self):
        return self.iFound
    def GetFoundElements(self):
        return self.dElem
    def GetNode(self,idx):
        if self.lstSort is None:
            return None
        try:
            return self.lstSort[idx][1][3]
        except:
            return None
    
[wxID_VFINDDIALOG, wxID_VFINDDIALOGCBFIND, wxID_VFINDDIALOGCBGOTO, 
 wxID_VFINDDIALOGCBOK, wxID_VFINDDIALOGCHCELEMENT, wxID_VFINDDIALOGCHCTYPE, 
 wxID_VFINDDIALOGGPROGRESS, wxID_VFINDDIALOGGSTEP, wxID_VFINDDIALOGLBLCOUNT, 
 wxID_VFINDDIALOGLBLFILTER, wxID_VFINDDIALOGLBLTYPE, wxID_VFINDDIALOGLSTITEM, 
 wxID_VFINDDIALOGTXTCOUNT, wxID_VFINDDIALOGTXTFILTERHIER, 
 wxID_VFINDDIALOGTXTFILTERNAME, wxID_VFINDDIALOGTXTFILTERTAG, 
 wxID_VFINDDIALOGTXTNODECOUNT, 
] = [wx.NewId() for _init_ctrls in range(17)]

class vFindDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VFINDDIALOG, name=u'vFindDialog',
              parent=prnt, pos=wx.Point(356, 120), size=wx.Size(400, 425),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vFind Dialog')
        self.SetClientSize(wx.Size(392, 398))

        self.chcElement = wx.Choice(choices=[], id=wxID_VFINDDIALOGCHCELEMENT,
              name=u'chcElement', parent=self, pos=wx.Point(48, 4),
              size=wx.Size(130, 21), style=0)
        self.chcElement.Bind(wx.EVT_CHOICE, self.OnChcElementChoice,
              id=wxID_VFINDDIALOGCHCELEMENT)

        self.chcType = wx.Choice(choices=[], id=wxID_VFINDDIALOGCHCTYPE,
              name=u'chcType', parent=self, pos=wx.Point(200, 4),
              size=wx.Size(184, 21), style=0)
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VFINDDIALOGCHCTYPE)

        self.txtFilterHier = wx.TextCtrl(id=wxID_VFINDDIALOGTXTFILTERHIER,
              name=u'txtFilterHier', parent=self, pos=wx.Point(48, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.txtFilterTag = wx.TextCtrl(id=wxID_VFINDDIALOGTXTFILTERTAG,
              name=u'txtFilterTag', parent=self, pos=wx.Point(128, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.txtFilterName = wx.TextCtrl(id=wxID_VFINDDIALOGTXTFILTERNAME,
              name=u'txtFilterName', parent=self, pos=wx.Point(208, 36),
              size=wx.Size(72, 21), style=0, value=u'*')

        self.cbFind = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFINDDIALOGCBFIND,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Find', name=u'cbFind',
              parent=self, pos=wx.Point(304, 32), size=wx.Size(76, 30),
              style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VFINDDIALOGCBFIND)

        self.lstItem = wx.ListCtrl(id=wxID_VFINDDIALOGLSTITEM, name=u'lstItem',
              parent=self, pos=wx.Point(8, 88), size=wx.Size(376, 260),
              style=wx.LC_REPORT)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstItemListItemDeselected, id=wxID_VFINDDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstItemListItemSelected, id=wxID_VFINDDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstItemListColClick,
              id=wxID_VFINDDIALOGLSTITEM)
        self.lstItem.Bind(wx.EVT_LEFT_DCLICK, self.OnLstItemLeftDclick)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFINDDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(152, 360), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VFINDDIALOGCBOK)

        self.cbGoTo = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VFINDDIALOGCBGOTO,
              bitmap=wx.EmptyBitmap(16, 16), label=u'go to', name=u'cbGoTo',
              parent=self, pos=wx.Point(8, 360), size=wx.Size(76, 30), style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,
              id=wxID_VFINDDIALOGCBGOTO)

        self.txtNodeCount = wx.TextCtrl(id=wxID_VFINDDIALOGTXTNODECOUNT,
              name=u'txtNodeCount', parent=self, pos=wx.Point(48, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.txtCount = wx.TextCtrl(id=wxID_VFINDDIALOGTXTCOUNT,
              name=u'txtCount', parent=self, pos=wx.Point(128, 64),
              size=wx.Size(72, 21), style=wx.TE_READONLY|wx.TE_RIGHT,
              value=u'')

        self.lblCount = wx.StaticText(id=wxID_VFINDDIALOGLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(8,
              68), size=wx.Size(28, 13), style=0)

        self.lblFilter = wx.StaticText(id=wxID_VFINDDIALOGLBLFILTER,
              label=u'Filter', name=u'lblFilter', parent=self, pos=wx.Point(8,
              40), size=wx.Size(22, 13), style=0)

        self.lblType = wx.StaticText(id=wxID_VFINDDIALOGLBLTYPE, label=u'Type',
              name=u'lblType', parent=self, pos=wx.Point(8, 8), size=wx.Size(24,
              13), style=0)

        self.gProgress = wx.Gauge(id=wxID_VFINDDIALOGGPROGRESS,
              name=u'gProgress', parent=self, pos=wx.Point(254, 64), range=100,
              size=wx.Size(130, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.gStep = wx.Gauge(id=wxID_VFINDDIALOGGSTEP, name=u'gStep',
              parent=self, pos=wx.Point(208, 64), range=100, size=wx.Size(38,
              18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.node=None
        self.doc=None
        self.vgpTree=None
        self.selElem=-1
        self.selItem=-1
        self.thdFind=thdFind(self)
        EVT_THREAD_FIND_ELEMENTS_FINISHED(self,self.OnFindFin)
        EVT_THREAD_FIND_ELEMENTS(self,self.OnFindProgress)
        
        img=images.getApplyBitmap()
        self.cbOk.SetBitmapLabel(img)
        
        img=images.getNavBitmap()
        self.cbGoTo.SetBitmapLabel(img)
        
        img=images.getFindBitmap()
        self.cbFind.SetBitmapLabel(img)
        
        self.lstItem.InsertColumn(0,u'Hier',wx.LIST_FORMAT_LEFT,180)
        self.lstItem.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,85)
        self.lstItem.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.gStep.SetRange(3)
        self.Reset()
        self.__procElementTypes__()
    def OnFindProgress(self,evt):
        self.gStep.SetValue(evt.GetStep())
        if evt.GetCount()>0:
            self.gProgress.SetRange(evt.GetCount())
        else:
            self.gProgress.SetRange(self.iSize)
        self.gProgress.SetValue(evt.GetValue())
    def OnFindFin(self,evt):
        self.txtNodeCount.SetValue(str(self.thdFind.GetCount()))
        self.txtCount.SetValue(str(self.thdFind.GetFound()))
        self.cbFind.Enable(True)
    def SetTree(self,vgpTree):
        self.vgpTree=vgpTree
    def __procElementTypes__(self):
        self.chcType.Clear()
        self.chcType.Append(u'*')
        self.chcType.Append(u'---')
        for tup in Elements.TYPES:
            self.chcType.Append(tup[0])
        self.chcType.SetSelection(3)
        self.__procElement__()
    def __procElement__(self):
        self.chcElement.Clear()
        self.chcElement.Append(u'*')
        sType=self.chcType.GetStringSelection()
        if sType!=u'*':
            bCheck=True
            if sType==u'---':
                bEmpty=True
            else:
                bEmpty=False
        else:
            bCheck=False
        for e in Elements.ELEMENTS:
            if Elements.isProperty(e[0],'isSkip'):
                continue
            bFound=True
            if bCheck:
                if bEmpty:
                    if len(e[1]['__type'])!=0:
                        bFound=False
                else:
                    if e[1]['__type']!=sType:
                        bFound=False
            if bFound:
                self.chcElement.Append(e[1]['__desc'],e)
        self.chcElement.SetSelection(0)
    def Reset(self):
        self.thdFind.Clear()
        self.node=None
        self.iCol=0
        self.bOrder=True
        
        self.selItem=-1
        self.lstItem.DeleteAllItems()
        self.txtNodeCount.SetValue('')
        self.txtCount.SetValue('')
    def IsThreadRunning(self):
        return self.thdFind.IsRunning()
    def SetNode(self,doc,node):
        self.doc=doc
        self.node=node
    def Find(self):
        if self.chcElement.GetStringSelection()==u'*':
            sFilterType=None
        else:
            idx=self.chcElement.GetSelection()
            e=self.chcElement.GetClientData(idx)
            sFilterType=e[0]
        sFilterHier=self.txtFilterHier.GetValue()
        sFilterTag=self.txtFilterTag.GetValue()
        sFilterName=self.txtFilterName.GetValue()
        
        if sFilterHier==u'*':
            sFilterHier=None
        if sFilterTag==u'*':
            sFilterTag=None
        if sFilterName==u'*':
            sFilterName=None
        self.cbFind.Enable(False)
        if self.vgpTree is None:
            self.iSize=self.doc.GetElementAttrCount()
            ids=None
        else:
            self.iSize=self.doc.GetElementAttrCount()
            ids=self.doc.getIds()
        self.txtNodeCount.SetValue(str(self.iSize))
        self.gProgress.SetRange(self.iSize)
        self.thdFind.Find(self.doc.getRoot(),ids,sFilterType,
                sFilterHier,sFilterTag,sFilterName)
            
    def OnCbGoToButton(self, event):
        if self.selItem<0:
            return
        node=self.thdFind.lstSort[self.selItem][1][3]
        id=self.doc.getAttribute(node,'id')
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        event.Skip()

    def OnCbOkButton(self, event):
        if self.thdFind.IsRunning():
            self.thdFind.Stop()
        else:
            self.Show(False)
        event.Skip()

    def OnLstItemListItemActivated(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnLstItemListItemDeselected(self, event):
        self.selItem=-1
        event.Skip()

    def OnLstItemListItemSelected(self, event):
        self.selItem=event.GetIndex()
        event.Skip()

    def OnCbFindButton(self, event):
        self.Find()
        event.Skip()

    def OnLstItemListColClick(self, event):
        col=event.GetColumn()
        if col>=0:
            if col!=self.iCol:
                self.iCol=col
                self.bOrder=True
            else:
                self.bOrder=not(self.bOrder)
            self.cbFind.Enable(False)
            self.thdFind.Show()
        event.Skip()

    def OnChcElementChoice(self, event):
        event.Skip()

    def OnChcTypeChoice(self, event):
        self.__procElement__()
        event.Skip()

    def OnLstItemLeftDclick(self, event):
        if self.selItem<0:
            return
        node=self.thdFind.GetNode(self.selItem)
        id=self.doc.getAttribute(node,'id')
        if self.vgpTree is not None:
            self.vgpTree.SelectByID(id)
        event.Skip()
    
