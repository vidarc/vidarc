#Boa:Dialog:vBrowseDialog
#----------------------------------------------------------------------------
# Name:         vBrowseDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vBrowseDialog.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors
import sys,string
#from boa.vApps.vPrjEng.vgtrXmlPrjEng  import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.tool.log.vtLog as vtLog

import images

VERBOSE=0

def create(parent):
    return vBrowseDialog(parent)

[wxID_VBROWSEDIALOG, wxID_VBROWSEDIALOGGCBBAPPLY, 
 wxID_VBROWSEDIALOGGCBBCANCEL, wxID_VBROWSEDIALOGLSTPOSSIBLE, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vBrowseDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VBROWSEDIALOG, name=u'vBrowseDialog',
              parent=prnt, pos=wx.Point(237, 99), size=wx.Size(383, 333),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Browse')
        self.SetClientSize(wx.Size(375, 306))

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEDIALOGGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(104, 272), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VBROWSEDIALOGGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VBROWSEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(200, 272),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VBROWSEDIALOGGCBBCANCEL)

        self.lstPossible = wx.ListView(id=wxID_VBROWSEDIALOGLSTPOSSIBLE,
              name=u'lstPossible', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(368, 248),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstPossible.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstPossibleListItemSelected,
              id=wxID_VBROWSEDIALOGLSTPOSSIBLE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.selNode=None
        self.relNode=None
        self.doc=None
        self.verbose=VERBOSE
        
        #self.vgpTree=vgtrXmlPrjEng(self.pnInfo,wx.NewId(),
        #        pos=(4,4),size=(200,252),style=0,name="vgpTree")
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        #EVT_VGTRXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.lstPossible.InsertColumn(0,u'Type',wx.LIST_FORMAT_LEFT,60)
        self.lstPossible.InsertColumn(1,u'Tag',wx.LIST_FORMAT_LEFT,60)
        self.lstPossible.InsertColumn(2,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstPossible.InsertColumn(3,u'Hierarchy',wx.LIST_FORMAT_LEFT,150)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        #self.vgpTree.SetLanguage(lang)
        pass
    def __getHierarchy__(self,baseNode,n):
        if baseNode is None:
            return ''
        if not self.doc.isSame(baseNode,n):
            return self.__getHierarchy__(baseNode,self.doc.getParent(n))+'<'+self.doc.getNodeText(n,'tag')+'>'#n.tagName
        else:
            return ''
    def __processChilds__(self,node,selID):
        bCheckID=False
        if len(selID)>0:
            bCheckID=True
        for c in self.doc.getChilds(node):
            id=self.doc.getKey(c)
            if self.doc.IsKeyValid(id):
                e=self.__getElementByTagName__(self.doc.getTagName(c))
                if e is not None:
                    try:
                        browseable=e[1]['__browseable']
                        bOk=False
                        for f in self.browseFilters:
                            if f=='*':
                                bOk=True
                                break
                            elif f==e[0]:
                                bOk=True
                                break
                        if bOk==True:
                            sName=self.doc.getNodeText(c,'name')
                            index = self.lstPossible.InsertImageStringItem(
                                        sys.maxint, e[0], -1)
                            self.lstPossible.SetStringItem(index,1,
                                        self.doc.getNodeText(c,'tag'),-1)
                            self.lstPossible.SetStringItem(index,2,sName,-1)
                            #sHier=self.__getHierarchy__(self.baseNode,c)
                            sHier=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,c)
                            self.lstPossible.SetStringItem(index,3,sHier,-1)
                            self.lstPossible.SetItemData(index,len(self.posNodes))
                            self.iPos+=1
                            self.posNodes.append(c)
                            bSel=False
                            if bCheckID:
                                if selID==id:
                                    bSel=True
                            else:
                                if self.name2find==sName:
                                    bSel=True
                            if bSel==True:
                                if self.selNode is None:
                                    self.selNode=c
                                self.lstPossible.SetItemState(index,
                                                wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                                self.lstPossible.EnsureVisible(index)
                    except:
                        pass
                    pass
                self.__processChilds__(c,selID)
    def SetRelNode(self,node):
        self.relNode=node
    def SetElements(self,ELEMENTS):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.ELEMENTS=ELEMENTS
        
    def SetDoc(self,doc):
        self.doc=doc
    def SetNode(self,elem2browse,selID,dictAttr,startNode='instance'):
        self.selNode=None
        self.iPos=0
        self.posNodes=[]
        self.baseNode=None
        try:
            self.name2find=dictAttr['name']
        except:
            self.name2find=''
        self.elem2browse=elem2browse
        self.lstPossible.DeleteAllItems()
        if self.doc is None:
            return
        child=self.doc.getChild(None,'prjengs')
        child=self.doc.getChild(child,startNode)
        self.baseNode=child
        self.browseFilters=string.split(dictAttr['__browse'],',')
        self.__processChilds__(child,selID)
        return self.selNode
    def GetSelNode(self):
        return self.selNode
    def __getElementByTagName__(self,tagname):
        for e in self.ELEMENTS:
            if e[0]==tagname:
                return e
        return None
    def OnGcbbApplyButton(self, event):
        if self.selNode is not None:
            self.EndModal(1)
            event.Skip()
            return
        else:
            # adding not possible
            sMsg=u'Nothing selected.'
        dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjEng instancing',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnLstPossibleListItemSelected(self, event):
        idx=event.GetIndex()
        iNode=self.lstPossible.GetItemData(idx)
        self.selNode=self.posNodes[iNode]
        event.Skip()
