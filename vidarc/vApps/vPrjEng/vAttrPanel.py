#Boa:FramePanel:vAttrPanel
#----------------------------------------------------------------------------
# Name:         vAttrPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vAttrPanel.py,v 1.7 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.maskededit
import wx.lib.masked.timectrl
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
from vidarc.vApps.vPrjEng.evtDataChanged import *
from vidarc.vApps.vPrjEng.vElemAttrDialog import *
#from vidarc.vApps.vPrjEng.InputValues import *
from vidarc.vApps.vPrjEng.InputValue import *
from vidarc.vApps.vPrjEng.vTypeInheritanceDialog import *
from vidarc.vApps.vPrjEng.vElementsPanel import ELEMENTS
import vidarc.vApps.vPrjEng.BaseTypes as BaseTypes
from vidarc.vApps.vPrjEng.vBrowseDialog import *
from vidarc.vApps.vPrjEng.vBrowseLinkDialog import *
from vidarc.vApps.vPrjEng.vBrowseWithAttrDialog import *
from vidarc.vApps.vPrjEng.vDocSelDialog  import *
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import vtXmlTreeItemGotoTmpl
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import wxEVT_VTXMLTREE_ITEM_GOTO_TMPL
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import EVT_VTXMLTREE_ITEM_GOTO_TMPL

import string,sys,copy,os,os.path,shutil,traceback
import vidarc.tool.InOut.fnUtil as fnUtil

import images

[wxID_VATTRPANEL, wxID_VATTRPANELGCBBADD, wxID_VATTRPANELGCBBBROWSE, 
 wxID_VATTRPANELGCBBDEL, wxID_VATTRPANELGCBBFILE, wxID_VATTRPANELGCBBLINK, 
 wxID_VATTRPANELGCBBOPEN, wxID_VATTRPANELGCBBSET, wxID_VATTRPANELGCBBTYPE, 
 wxID_VATTRPANELLBBURLDROPTARGET, wxID_VATTRPANELLSTATTR, 
] = [wx.NewId() for _init_ctrls in range(11)]

CACHE_INH={}

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

class vURLDropTarget(wx.PyDropTarget):
    def __init__(self, window):
        wx.PyDropTarget.__init__(self)
        self.window = window

        self.data = wx.URLDataObject();
        self.SetDataObject(self.data)

    def OnDragOver(self, x, y, d):
        return wx.DragLink

    def OnData(self, x, y, d):
        if not self.GetData():
            return wx.DragNone

        urlData = self.data.GetURL()
        strs=string.split(urlData,'\n')
        url=string.strip(strs[0])
        self.window.GetParent().AddUrl(url)

        return d


class vAttrPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VATTRPANEL, name=u'vAttrPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(494, 394),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(486, 367))
        self.SetAutoLayout(True)
        self.Bind(wx.EVT_SIZE, self.OnVgpAttrSize)

        self.lstAttr = wx.ListView(id=wxID_VATTRPANELLSTATTR, name=u'lstAttr',
              parent=self, pos=wx.Point(8, 48), size=wx.Size(376, 312),
              style=wx.LC_REPORT)
        self.lstAttr.SetConstraints(LayoutAnchors(self.lstAttr, True, True,
              True, True))
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VATTRPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_RIGHT_DOWN, self.OnLstAttrRightDown)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected, id=wxID_VATTRPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LEFT_DCLICK, self.OnLstAttrLeftDclick)

        self.gcbbType = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBTYPE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Type', name=u'gcbbType',
              parent=self, pos=wx.Point(400, 124), size=wx.Size(76, 30),
              style=0)
        self.gcbbType.Bind(wx.EVT_BUTTON, self.OnGcbbTypeButton,
              id=wxID_VATTRPANELGCBBTYPE)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(400, 48), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VATTRPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(400, 80), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VATTRPANELGCBBDEL)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(400, 8), size=wx.Size(76, 30), style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VATTRPANELGCBBSET)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(400, 8),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VATTRPANELGCBBBROWSE)

        self.gcbbLink = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBLINK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Link', name=u'gcbbLink',
              parent=self, pos=wx.Point(400, 164), size=wx.Size(76, 30),
              style=0)
        self.gcbbLink.Bind(wx.EVT_BUTTON, self.OnGcbbLinkButton,
              id=wxID_VATTRPANELGCBBLINK)

        self.gcbbFile = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBFILE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'File', name=u'gcbbFile',
              parent=self, pos=wx.Point(400, 204), size=wx.Size(76, 30),
              style=0)
        self.gcbbFile.Bind(wx.EVT_BUTTON, self.OnGcbbFileButton,
              id=wxID_VATTRPANELGCBBFILE)

        self.gcbbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VATTRPANELGCBBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Open', name=u'gcbbOpen',
              parent=self, pos=wx.Point(400, 236), size=wx.Size(76, 30),
              style=0)
        self.gcbbOpen.Bind(wx.EVT_BUTTON, self.OnGcbbOpenButton,
              id=wxID_VATTRPANELGCBBOPEN)

        self.lbbUrlDropTarget = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VATTRPANELLBBURLDROPTARGET, name=u'lbbUrlDropTarget',
              parent=self, pos=wx.Point(408, 272), size=wx.Size(32, 32),
              style=0)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 8)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbSet.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self.gcbbSet, wx.Bottom, 12)
        lc.top.SameAs       (self, wx.Top, 8)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbBrowse.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbBrowse, wx.Bottom, 4)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbAdd.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbAdd, wx.Bottom, 4)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbDel.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbDel, wx.Bottom, 8)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbType.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbType, wx.Bottom, 8)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbLink.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbLink, wx.Bottom, 8)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbFile.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbFile, wx.Bottom, 4)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbOpen.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbOpen, wx.Bottom, 8)
        lc.left.SameAs      (self, wx.Right, -60)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.lbbUrlDropTarget.SetConstraints(lc);
        
        self.doc=None
        self.netPrj=None
        self.node=None
        self.dlgAttr=None
        self.dlgType=None
        self.dlgLink=None
        self.dlgFile=None
        self.dlgDocSel=None
        self.dlgBrowse=None
        self.dlgBrowseWithAttr=None
        self.attrName=''
        self.dictAttributes={}
        self.selAttrIdx=-1
        self.netDocument=None
        self.prjDN=''
        self.docFN=''
        self.lang='en'
        self.lang=None
        
        self.cutNode=None
        self.copyNode=None
        self.cutDict=None
        self.copyDict=None
        
        dt = vURLDropTarget(self.lbbUrlDropTarget)
        self.lbbUrlDropTarget.SetDropTarget(dt)
        
        dt = vFileDropTarget(self)
        self.lstAttr.SetDropTarget(dt)
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_RIGHT,200)
        
        self.inpValue=InputValue(parent=self,pos=wx.Point(100, 12),
                size=wx.Size(200,21))
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 12)
        lc.left.SameAs      (self.lstAttr, wx.Left, 0)
        lc.right.SameAs     (self.lstAttr, wx.Right, 0)
        lc.height.AsIs      ()
        #lc2 = wx.LayoutConstraints()
        #lc2.top.SameAs       (self.gcbbSet, wx.Top, 4)
        #lc2.left.SameAs      (self.lstAttr, wx.Right, -56)
        #lc2.right.SameAs     (self.lstAttr, wx.Right, -4)
        #lc2.height.AsIs      ()
        self.inpValue.SetConstraints(lc)
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getEditBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getTypeBitmap()
        self.gcbbType.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        #self.gcbbBrowse.Enable(False)
        self.gcbbBrowse.Show(False)
        
        img=images.getLinkBitmap()
        self.gcbbLink.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbFile.SetBitmapLabel(img)
        
        img=images.getEditDocBitmap()
        self.gcbbOpen.SetBitmapLabel(img)
        
        self.dlgLink=vBrowseLinkDialog(self)
        self.bLink2set=True
        
        img=images.getSurfLargeBitmap()
        self.lbbUrlDropTarget.SetBitmap(img)
        
        self.bModified=False
        self.bAutoApply=False
        self.bLock=False
    def Lock(self,flag):
        self.bLock=flag
        if flag:
            self.lstAttr.Enable(False)
            self.inpValue.Enable(False)
            #self.gcbbSet.Enable(False)
            self.gcbbSet.Show(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDel.Enable(False)
            self.gcbbType.Enable(False)
            #self.gcbbBrowse.Enable(False)
            self.gcbbBrowse.Show(False)
            self.gcbbLink.Enable(False)
            self.gcbbFile.Enable(False)
            self.gcbbOpen.Enable(False)
        else:
            self.lstAttr.Enable(True)
            self.inpValue.Enable(True)
            #self.gcbbSet.Enable(True)
            self.gcbbSet.Show(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDel.Enable(True)
            self.gcbbType.Enable(True)
            #self.gcbbBrowse.Enable(False)
            self.gcbbBrowse.Show(False)
            self.gcbbLink.Enable(True)
            self.gcbbFile.Enable(True)
            self.gcbbOpen.Enable(True)
    def SetLanguage(self,lang):
        self.lang=lang
        self.lang=None
        if self.dlgDocSel is not None:
            self.dlgDocSel.SetLanguage(self.lang)
    def SetNetDocument(self,doc):
        self.netDocument=doc
    def SetNetDocPrj(self,netPrj):
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.netPrj=netPrj
    def SetDocFN(self,fn):
        self.docFN=fn
        if self.dlgDocSel is not None:
            self.dlgDocSel.SetDocFN(self.docFN)
    def SetPrjDN(self,dn):
        self.prjDN=dn
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def Clear(self):
        self.__setModified__(False)
        self.inpValue.Clear()
        self.lstAttr.DeleteAllItems()
    def __calcAttr__(self,attrs):
        dict={}
        bBrowse=False
        for a in attrs:
            if self.doc.getTagName(a)=='__browse':
                bBrowse=True
                break
        for a in attrs:
            if len(self.doc.getAttribute(a,'id'))>0:
                continue
                
            dict[self.doc.getTagName(a)]=self.doc.getText(a)
        if bBrowse:
            dict['type']=''
        return dict
    #def SetDoc(self,doc,netPrj,bNet=False):
    def SetDoc(self,doc,bNet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'SetDoc net:%d'%(bNet),
                origin=self.GetName())
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        if self.dlgLink is not None:
            self.dlgLink.SetDoc(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,name,node):
        if self.bModified==True:
            if self.bAutoApply:
                self.GetNode(self.node)
                wx.PostEvent(self,vgpDataChanged(self,self.node))
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
                    wx.PostEvent(self,vgpDataChanged(self,self.node))
        self.__setModified__(False)
        self.node=node
        self.name=name
        self.inpValue.Clear()
        self.inpValue.Enable(False,-1)
        #self.gcbbBrowse.Enable(False)
        #self.gcbbSet.Enable(True)
        self.gcbbBrowse.Show(False)
        self.gcbbSet.Show(True)
        
        self.dictAttr=None
        self.dictAttributes={}
        if node is not None:
            if self.doc.getTagName(node)=='filter':
                nodes=[]
            else:
                nodes=self.doc.getChilds(node)
            self.lstAttr.DeleteAllItems()
            for n in nodes:
                bSetVal=False
                id=self.doc.getKey(n)
                if self.doc.IsKeyValid(id):
                    continue
                tag=self.doc.getTagName(n)
                if tag in ['tag','name','description']:
                    continue
                if tag[:2]=='__':
                    continue
                global CACHE_INH
                if len(self.doc.getAttribute(n,'ih'))>0:
                    # inherated type
                    bSetVal=True
                    iid=self.doc.getAttribute(node,'iid')
                    dCache={}
                    try:
                        dCache=CACHE_INH[iid]
                        self.dictAttributes[tag]=dCache[tag]
                    except:
                        instNodeTup=self.doc.GetIdStr(iid)
                        attrs=self.doc.getChilds(self.doc.getChild(instNodeTup[1],tag))
                        self.dictAttributes[tag]=self.__calcAttr__(attrs)
                        dCache[tag]=self.dictAttributes[tag]
                        CACHE_INH[iid]=dCache
                    sVal=self.doc.getText(n)
                    self.dictAttributes[tag]['val']=sVal
                    self.gcbbAdd.Enable(False)
                    self.gcbbDel.Enable(False)
                    #print CACHE_INH[iid]
                elif len(self.doc.getAttribute(n,'fid'))>0:
                    self.gcbbAdd.Enable(True)
                    self.gcbbDel.Enable(True)
                    
                    fid=self.doc.getAttribute(n,'fid')
                    tmpl=self.doc.GetIdStr(fid)[1]
                    if tmpl is not None:
                        rootNode=Hierarchy.getRootNode(self.doc,tmpl)
                        s=Hierarchy.getTagNamesWithRel(self.doc,rootNode,None,tmpl)
                    else:
                        s=''
                    dCache={}
                    try:
                        dCache=CACHE_INH[fid]
                    except:
                        dCache={'type':'string','len':'8'}
                        if tmpl is not None:
                            sTag=self.doc.getNodeText(tmpl,'tag')
                            child=self.doc.getChild(tmpl,sTag)
                            dict={}
                            if child is not None:
                                dCache=self.__calcAttr__(self.doc.getChilds(child))
                                CACHE_INH[fid]=dCache
                    sVal=self.doc.getText(n)
                    self.dictAttributes[tag]={'val':sVal,'type':'predefined',
                            '__fid':fid,'__predef':s,'predef_type':dCache}
                else:
                    attrs=self.doc.getChilds(n)
                    self.gcbbAdd.Enable(True)
                    self.gcbbDel.Enable(True)
                    self.dictAttributes[tag]=self.__calcAttr__(attrs)
                    if self.dictAttributes[tag].has_key('__browse'):
                        sVal=Hierarchy.applyCalcDict(self.doc,self.dictAttributes[tag])
                    else:
                        sVal=self.doc.getNodeText(n,'val')
                        fidTmp=self.doc.getNodeText(n,'fid')
                        tmpl=self.doc.GetIdStr(fidTmp)[1]
                        if tmpl is not None:
                            rootNode=Hierarchy.getRootNode(self.doc,node)
                            relNode=Hierarchy.getRelBaseNode(self.doc,node)
                            s=Hierarchy.getTagNames(self.doc,rootNode,relNode,tmpl)
                            if s!=sVal:
                                self.__setModified__(True)
                                sVal=s
                    self.dictAttributes[tag]['val']=sVal
                #vtLog.CallStack('%s %s'%(sVal,self.doc.getAttributeVal(n)))
            self.__addElementAttr__()
            #if self.dictAttributes.has_key('inheritance_type'):
            #    self.gcbbType.Enable(True)
            #else:
            #    self.gcbbType.Enable(False)
        else:
            self.gcbbType.Enable(False)
                
    def GetNode(self,node,doc=None):
        global CACHE_INH
        if self.doc.getTagName(node)=='type':
            try:
                del CACHE_INH[self.doc.getAttribute(node,'id')]
            except:
                pass
        keys=self.dictAttributes.keys()
        keys.sort()
        for k in keys:   
            child=self.doc.getChild(node,k)
            if child is not None:
                nv=child
            else:
                nv=self.doc.createSubNode(node,k)
                child=nv
        
            d=self.dictAttributes[k]
            if d is None:
                self.doc.deleteNode(child,node)
                continue
            
            # clean up childs, no danger here (only attribute def node
            # are removed)
            for c in self.doc.getChilds(nv):
                self.doc.deleteNode(c,nv)
                
            val=d['val']
            if d.has_key('__predef'):
                self.doc.setAttribute(nv,'fid',d['__fid'])
                self.doc.setText(nv,val)
            else:
                try:
                    if string.find(self.doc.getTagName(nv),'link')<0:
                        self.doc.removeAttribute(nv,'fid')
                except:
                    pass
                if len(self.doc.getAttribute(nv,'ih'))<=0:
                    keys=d.keys()
                    keys.sort()
                    for k in keys:
                        if d[k] is None:
                            continue
                        child=self.doc.getChild(nv,k)
                        if child is not None:
                            self.doc.setNodeText(nv,k,d[k])
                        else:                
                            self.doc.createSubNodeText(nv,k,d[k])
                else:
                    self.doc.setText(nv,val)
        self.doc.AlignNode(node,iRec=1)
        self.__addElementAttr__()
        self.__setModified__(False)
    def Apply(self,bDoApply=False):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if self.bModified==True:
            if self.bAutoApply or bDoApply:
                self.GetNode(self.node)
                #wx.PostEvent(self,vgpDataChanged(self,self.node))
                return True
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
                    #wx.PostEvent(self,vgpDataChanged(self,self.node))
                    return True
        #self.GetNode(self.node,self.doc)
        return False
    def Cancel(self):
        self.__setModified__(False)
        self.SetNode(self.name,self.node)
    def __doUpdateTypeHeridatage__(self,attrName):
        if attrName=='type':
            attrKeys=self.dictAttributes.keys()
            newType=self.dictAttributes['type']['type']
            for attrKey in attrKeys:
                if attrKey=='type':
                    continue
                try:
                    attrDict=self.dictAttributes[attrKey]
                    val=attrDict['inheritance_type']
                    if attrDict['type']!=newType:
                        #do change type
                        oldType=attrDict['type']
                        attrDict['type']=newType
                except:
                    pass
    def getAttrValStr(self,key):
        d=self.dictAttributes[key]
        if d.has_key('__browse'):
            return d['val']
        df=d
        try:
            s=self.dictAttributes[key]['inheritance']
            strs=string.split(s,',')
            for s in strs:
                if s=='type':
                    df=self.dictAttributes['inheritance_type']
                    break
        except:
            pass
        try:
            #t=type(self.__getVal__(d,'val',''))
            val=self.__getVal__(d,'val','')
            fmt=self.__getFormat__(df)
            return fmt%val
        except:
            return ''
    def __getVal__(self,d,k,dftVal):
        sType=d['type']
        val=d[k]
        if sType == 'predefined':
            d=d['predef_type']
            sType=d['type']
        #print '__getVal__',sType
        try:
            if sType=='int':
                return int(val)
            elif sType=='long':
                return long(val)
            elif sType=='float':
                return float(val)
            elif sType=='pseudofloat':
                return float(val)
            elif sType=='text':
                return val
            elif sType=='string':
                return val
            elif sType=='choice':
                #print val
                #print d['it%02d'%int(val)]
                #return d['it%02d'%int(val)]
                return val
            elif sType=='choiceint':
                return int(val)
            elif sType=='datetime':
                return val
            else:
                return val
        except:
            return dftVal
    def __getFormat__(self,d):
        sType=d['type']
        if sType == 'predefined':
            d=d['predef_type']
            sType=d['type']
        try:
            if sType=='int':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='long':
                try:
                    s='%d'%d['len']
                    return '%'+s+'d'
                except:
                    return '%d'
            elif sType=='float':
                try:
                    #print 'float',d
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    #print sd,sc
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='pseudofloat':
                try:
                    sd='%d'%int(d['digits'])
                    sc='%d'%int(d['comma'])
                    return '%'+sd+'.'+sc+'f'
                except:
                    return '%f'
            elif sType=='text':
                return "%s"
            elif sType=='string':
                return "%s"
            elif sType=='choice':
                return "%s"
            elif sType=='choiceint':
                return "%d"
            elif sType=='datetime':
                return "%s"
            else:
                return "%s"
        except:
            return "%s"
    
    def __setVal__(self,d,k,val,dftVal):
        sType=d['type']
        if sType == 'predefined':
            dPre=d['predef_type']
            sType=dPre['type']
        
        try:
            if sType=='int':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='long':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='float':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='pseudofloat':
                d[k]=self.__getFormat__(d)%(val)
            elif sType=='text':
                d[k]=__str__(val)
            elif sType=='string':
                d[k]=__str__(val)
            elif sType=='choice':
                d[k]=__str__(val)
            elif sType=='choiceint':
                d[k]=self.__getFormat__(d)%val
            elif sType=='datetime':
                d[k]=val
            else:
                d[k]=val
        except:
            d[k]=dftVal
    def __getValues__(self,d):
        lst=[]
        sType=d['type']
        if sType in ['int','long','float','pseudofloat']:
            lst.append(('min','0'))
            lst.append(('max','100'))
            lst.append(('val','0'))
        elif sType in ['text','string','choice']:
            lst.append(('val',''))
        elif sType=='choiceint':
            lst.append(('val','0'),('min','0'),('max','100'))
        elif sType=='datetime':
            lst.append(('val',''))
        else:
            pass
        return lst
    def __cpValues__(self,oldDict,newDict):
        lst=self.__getValues__(oldDict)
        for k,v in lst:
            try:
                self.__setVal__(newDict,k,self.__getVal__(oldDict,k,v),v)
            except:
                pass
    def __setupType__(self):
        BaseTypes.setupType(self.dictAttributes)
        self.__addElementAttr__()
    def __setInput__(self,inpTup,k):
        try:
            s=self.dictAttributes[k]['inheritance']
            strs=string.split(s,',')
            strs.index('type')
            inheritanceType=self.dictAttributes['inheritance_type']
        except:
            inheritanceType=None
        if self.dictAttributes[k] is None:
            return -1
        inpTup[0].Show(True)
        inpTup[1].Set(self.dictAttributes[k],inheritanceType)
        return 0
    def __addElementAttr__(self):
        self.lstAttr.DeleteAllItems()
        self.selAttrIdx=-1
        keys=self.dictAttributes.keys()
        keys.sort()
        for k in keys:
            if string.find(k,'inheritance')==0:
                continue
            if self.dictAttributes[k] is None:
                continue
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
            try:
                if self.dictAttributes[k]['type']=='link':
                    tmpNode=self.doc.GetIdStr(self.dictAttributes[k]['fid'])[1]
                    relNode=Hierarchy.getRelBaseNode(self.doc,self.node)
                    s=Hierarchy.getTagNames(self.doc,rootNode,relNode,tmpl)
                    self.lstAttr.SetStringItem(index,1,s,-1)
                    continue
            except:
                pass
            sVal=self.getAttrValStr(k)
            self.lstAttr.SetStringItem(index,1,sVal,-1)
    def __isEditable__(self):
        try:
            if self.dictAttributes[self.attrName]['type']==u'link':
                return 0
            else:
                return 1
        except:
            return 0
    def __setAttrValue__(self):
        self.gcbbBrowse.Show(False)
        self.gcbbSet.Show(False)
        try:
            if self.__isEditable__()==0:
                self.inpValue.Clear()
                return
        except:
            pass
        try:
            s=self.dictAttributes[self.attrName]['inheritance']
            strs=string.split(s,',')
            strs.index('type')
            inheritanceType=self.dictAttributes['inheritance_type']
        except:
            inheritanceType=None
        if self.dictAttributes[self.attrName].has_key('__browse'):
            self.inpValue.Clear()
            self.inpValue.Enable(False,-1)
            #self.gcbbBrowse.Enable(True)
            #self.gcbbSet.Enable(False)
            self.gcbbBrowse.Show(True)
            self.gcbbSet.Show(False)
        else:
            if self.dictAttributes[self.attrName].has_key('__predef'):
                if self.dictAttributes[self.attrName].has_key('predef_type')==0:
                    
                    fid=self.dictAttributes[self.attrName]['__fid']
                    tmpl=self.doc.GetIdStr(fid)[1]
                    if tmpl is not None:
                        rootNode=Hierarchy.getRootNode(self.doc,tmpl)
                        s=Hierarchy.getTagNamesWithRel(self.doc,rootNode,None,tmpl)
                                    
                        sTag=self.doc.getNodeText(tmpl,'tag')
                        child=self.doc.getChild(tmpl,sTag)
                        dict={}
                        if child is not None:
                            dict=self.__calcAttr__(self.doc.getChilds(child))
                        self.dictAttributes[self.attrName]['predef_type']=dict

                dTmpl=self.dictAttributes[self.attrName]['predef_type']
                dTmpl['val']=self.dictAttributes[self.attrName]['val']
                self.inpValue.Set(dTmpl,None)
            else:
                self.inpValue.Set(self.dictAttributes[self.attrName],inheritanceType)
            #self.gcbbBrowse.Enable(False)
            #self.gcbbSet.Enable(True)
            self.gcbbBrowse.Show(False)
            self.gcbbSet.Show(True)
        
    def OnGcbbTypeButton(self, event):
        if self.dlgType is None:
            self.dlgType=vgdTypeInheritance(self)
        bEraseable=True
        try:
            dInherType=copy.deepcopy(self.dictAttributes['inheritance_type'])
            for d in self.dictAttributes.values():
                if d is None:
                    continue
                if d.has_key('inheritance'):
                    for s in string.split(d['inheritance'],','):
                        if s=='type':
                            bEraseable=False
                            break
                if bEraseable==False:
                    break
        except:
            dInherType=None
        
        self.dlgType.Centre()
        self.dlgType.Set(self.attrName,dInherType,bEraseable)
        ret=self.dlgType.ShowModal()
        if ret==1:
            sType,self.dictType=self.dlgType.Get()
            self.dictAttributes['inheritance_type']=self.dictType
            self.__setModified__(True)
        event.Skip()

    def OnGcbbAddButton(self, event):
        if self.dlgAttr is None:
            self.dlgAttr=vElemAttrDialog(self)
        if self.dlgBrowse is None:
            self.dlgBrowse=vBrowseDialog(self)
            self.dlgBrowse.SetElements(ELEMENTS.ELEMENTS)
            self.dlgBrowse.SetDoc(self.doc)
        if self.selAttrIdx<0:
            try:
                self.attrName='undefined'
                if self.dictAttributes.has_key('inheritance_type'):
                    d={'inheritance':'type','type':self.dictAttributes['type']['val'],'val':''}
                    self.dlgAttr.Set(self.doc,'undefined',d,
                                self.dictAttributes['inheritance_type'],self.dlgBrowse)
                else:
                    d={'type':'string','val':''}
                    self.dlgAttr.Set(self.doc,'undefined',d,None,self.dlgBrowse)
            except:
                traceback.print_exc()
                d={'type':'string','val':''}
                self.dlgAttr.Set(self.doc,'undefined',d,None,self.dlgBrowse)
                
        else:
            if self.__isEditable__()==0:
                return
            if self.dictAttributes.has_key('inheritance_type'):
                self.dlgAttr.Set(self.doc,self.attrName,
                        copy.deepcopy(self.dictAttributes[self.attrName]),
                        self.dictAttributes['inheritance_type'],
                        self.dlgBrowse)
            else:
                self.dlgAttr.Set(self.doc,self.attrName,
                        copy.deepcopy(self.dictAttributes[self.attrName]),
                        None,self.dlgBrowse)
        ret=self.dlgAttr.ShowModal()
        if ret==1:
            newAttrName,self.dictAttr=self.dlgAttr.Get()
            if newAttrName!=self.attrName:
                self.__setModified__(True)
                self.dictAttributes[newAttrName]=self.dictAttr
                self.__addElementAttr__()
            else:
                if self.dictAttributes[self.attrName]!=self.dictAttr:
                    self.__setModified__(True)
                    self.dictAttributes[self.attrName]=self.dictAttr
                    sVal=self.dictAttributes[self.attrName]['val']
                    if self.dictAttributes[self.attrName].has_key('predef_type'):
                        del self.dictAttributes[self.attrName]['predef_type']
                    self.lstAttr.SetStringItem(self.selAttrIdx,1,sVal,-1)
                        
                    self.__setAttrValue__()
            if newAttrName=='type':
                self.__setupType__()
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selAttrIdx<0:
            return
        if self.dictAttributes[self.attrName]['type']==u'link':
            sType=u'link'
            bDoRenumber=True
        else:
            bDoRenumber=False
        if self.dictAttributes[self.attrName]['type']==u'link':
            sType=u'file'
            bDoRenumber=True
        else:
            bDoRenumber=False
        self.dictAttributes[self.attrName]=None
        self.__setModified__(True)
        self.lstAttr.DeleteItem(self.selAttrIdx)
        self.selAttrIdx=-1
        self.attrName=''
        self.inpValue.Clear()
        if bDoRenumber:
            self.__doRenumber__(sType)
            self.__addElementAttr__()
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        if self.lstAttr.GetSelectedItemCount()>1:
            return
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        k=it.m_text
        self.selAttrIdx=it.m_itemId
        try:
            self.attrName=k
        except:
            self.attrName=''
        self.__setAttrValue__()
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selAttrIdx<0:
            return
        val=self.inpValue.Get()
        self.lstAttr.SetStringItem(self.selAttrIdx,1,val,-1)
        self.dictAttributes[self.attrName]['val']=val
        self.__setModified__(True)
        event.Skip()

    def OnGcbbBrowseButton(self, event):
        if self.selAttrIdx<0:
            return
        #selElem=None
        #for e in ELEMENTS:
        #    if e[0]==self.doc.getTagName(self.node):
        #        selElem=e
        #        break
        #if selElem is None:
        #    return
        if self.dlgBrowseWithAttr is None:
            self.dlgBrowseWithAttr=vBrowseWithAttrDialog(self)
            #self.dlgBrowseWithAttr.SetElements(ELEMENTS)
            self.dlgBrowseWithAttr.SetDoc(self.doc)
        self.dlgBrowseWithAttr.Centre()
        
        dictAttr=self.dictAttributes[self.attrName]
        sBrowse=u'*'
        sFid=u''
        sAttr=u''
        sFormular=u"'$0$'"
        try:
            sBrowse=dictAttr['__browse']
            sFid=dictAttr['__fid']
            sAttr=dictAttr['attr']
            sFormular=dictAttr['formular']
        except:
            pass
        self.dlgBrowseWithAttr.SetNode(sBrowse,sFid,sAttr,sFormular)
        ret=self.dlgBrowseWithAttr.ShowModal()
        if ret==1:
            d=self.dlgBrowseWithAttr.Get()
            #dictAttr=self.dictAttributes[self.attrName]
            dictAttr['val']=d['val']
            dictAttr['formular']=d['formular']
            dictAttr['attr']=d['attr']
            dictAttr['__fid']=d['__fid']
            dictAttr['__browse']=d['__browse']
            self.lstAttr.SetStringItem(self.selAttrIdx,1,dictAttr['val'],-1)
            self.__setModified__(True)
            pass
        event.Skip()

    def OnLstAttrRightDown(self, event):
        if not hasattr(self,'popupIdCopy'):
            self.popupIdCopy=wx.NewId()
            self.popupIdPaste=wx.NewId()
            self.popupIdGoto=wx.NewId()
            self.popupIdGotoTmpl=wx.NewId()
            self.popupIdGotoLast=wx.NewId()
            EVT_MENU(self,self.popupIdCopy,self.OnCopy)
            EVT_MENU(self,self.popupIdPaste,self.OnPaste)
            EVT_MENU(self,self.popupIdGotoTmpl,self.OnGotoTmpl)
            EVT_MENU(self,self.popupIdGotoLast,self.OnGotoLast)
            EVT_MENU(self,self.popupIdGoto,self.OnGoto)
        menu=wx.Menu()
        if len(self.doc.getAttribute(self.node,'iid'))>0:
            mnIt=wx.MenuItem(menu,self.popupIdGotoTmpl,'Goto ...')
            mnIt.SetBitmap(images.getGotoTmplBitmap())
            menu.AppendItem(mnIt)
        else:
            mnIt=wx.MenuItem(menu,self.popupIdCopy,'Copy')
            mnIt.SetBitmap(images.getCopyBitmap())
            menu.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdPaste,'Past')
            mnIt.SetBitmap(images.getPasteBitmap())
            menu.AppendItem(mnIt)
            if self.selAttrIdx>=0:
                attr=self.dictAttributes[self.attrName]
                if attr.has_key('__browse'):
                    mnIt=wx.MenuItem(menu,self.popupIdGoto,'Goto ...')
                    mnIt.SetBitmap(images.getGotoBitmap())
                    menu.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdGotoLast,'Goto Last')
            mnIt.SetBitmap(images.getGotoLastBitmap())
            menu.AppendItem(mnIt)
        self.PopupMenu(menu,wx.Point(event.GetX(), event.GetY()))
        menu.Destroy()
    def OnCut(self, evt):
        self.copyNode=None
        self.copyDict=None
        self.cutNode=self.node
        self.cutDict={}
        for idx in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(idx,0)
            k=it.m_text
            self.cutDict[k]=self.dictAttributes[k]
    def OnCopy(self, evt):
        self.cutNode=None
        self.cutDict=None
        self.copyNode=self.node
        self.copyDict={}
        for idx in range(0,self.lstAttr.GetItemCount()):
            if self.lstAttr.GetItemState(idx,wx.LIST_STATE_SELECTED)>0:
                it=self.lstAttr.GetItem(idx,0)
                k=it.m_text
                self.copyDict[k]=self.dictAttributes[k]
    def OnPaste(self, evt):
        if self.cutNode is not None:
            pass
        if self.copyNode is not None:
            attrKeys=self.copyDict.keys()
            attrKeys.sort()
            for attrk in attrKeys:
                if self.dictAttributes.has_key(attrk):
                    d=self.dictAttributes[attrk]
                    if d==self.copyDict[attrk]:
                        pass
                    else:
                        pass
                else:
                    self.dictAttributes[attrk]=self.copyDict[attrk]
            self.__addElementAttr__()
            self.__setModified__(True)
    def OnGotoTmpl(self,evt):
        wx.PostEvent(self,vtXmlTreeItemGotoTmpl(self,None,self.node))
        evt.Skip()
    def OnGoto(self,evt):
        id=self.dictAttributes[self.attrName]['__fid']
        wx.PostEvent(self,vtXmlTreeItemGoto(id,self.node))
        evt.Skip()
    def OnGotoLast(self,evt):
        wx.PostEvent(self,vtXmlTreeItemGoto('',self.node))
        evt.Skip()
    def __doRenumber__(self,attrBase):
        lst=[]
        for attrName in self.dictAttributes.keys():
            if attrName[:len(attrBase)]==attrBase:
                lst.append(self.dictAttributes[attrName])
                self.dictAttributes[attrName]=None
        iNum=0
        for item in lst:
            sName=u'%s%02d'%(attrBase,iNum)
            self.dictAttributes[sName]=item
    def __getLastUsed__(self,attrBase):
        iLastUsed=-1
        for attrName in self.dictAttributes.keys():
            if attrName[:len(attrBase)]==attrBase:
                if self.dictAttributes[attrName] is not None:
                    try:
                        i=int(attrName[len(attrBase):])
                        if i>iLastUsed:
                            iLastUsed=i
                    except:
                        pass
        return iLastUsed
    def __getNextFree__(self,attrBase):
        i=self.__getLastUsed__(attrBase)
        return u'%s%02d'%(attrBase,i+1)
    def OnGcbbLinkButton(self, event):
        if self.dlgLink is None:
            self.dlgLink=vgdBrowseLink(self)
            self.dlgLink.SetDoc(self.doc,True)
        if self.bLink2set==True:
            if self.dlgLink is not None:
                browseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
                browseNode=self.doc.getChild(browseNode,'instance')
                self.dlgLink.SetNode(browseNode)
                self.bLink2set=False
        relNode=Hierarchy.getRelBaseNode(self.doc,self.node)
        if self.dlgLink is not None:
            self.dlgLink.SetRelNode(relNode)
        self.dlgLink.CheckNode2Set()
        if self.dlgLink.GetSelNode() is None:
            try:
                try:
                    id=self.dictAttributes[self.attrName]['fid']
                except:
                    id=self.doc.getAttribute(self.doc.getParent(self.node),'id')
                self.dlgLink.SetLinkInfo(id)
            except:
                pass
        else:
            try:
                id=self.dictAttributes[self.attrName]['fid']
            except:
                id=self.doc.getAttribute(self.doc.getParent(self.node),'id')
            self.dlgLink.SetLinkInfo(id)
        self.dlgLink.Centre()
        ret=self.dlgLink.ShowModal()
        if ret==1:
            bNew=False
            if self.selAttrIdx<0:
                bNew=True
            else:
                if self.attrName[:len(u'link')]!=u'link':
                    bNew=True
            if bNew:
                # generate new link
                sAttr=self.__getNextFree__(u'link')
                tupVal=self.dlgLink.GetLinkInfo()
                self.dictAttributes[sAttr]={'type':'link','val':tupVal[1],
                            'fid':tupVal[0]}
                idx=self.lstAttr.InsertImageStringItem(sys.maxint, 
                                            sAttr, -1)
                self.lstAttr.SetStringItem(idx,1,tupVal[1],-1)
                self.__setModified__(True)
            else:
                tupVal=self.dlgLink.GetLinkInfo()
                self.lstAttr.SetStringItem(self.selAttrIdx,1,tupVal[1],-1)
                self.dictAttributes[self.attrName]['fid']=tupVal[0]
                self.dictAttributes[self.attrName]['val']=tupVal[1]
                self.__setModified__(True)
            pass
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.selAttrIdx=-1
        self.attrName=''
        wx.CallAfter(self.__hideValue__)
        event.Skip()
        
    def __hideValue__(self):
        if self.lstAttr.GetSelectedItemCount()>=1:
            return
        #self.gcbbBrowse.Enable(False)
        #self.gcbbSet.Enable(False)
        self.gcbbBrowse.Show(False)
        self.gcbbSet.Show(False)
        self.inpValue.Clear()
        self.inpValue.Enable(False,-1)
        
    def OnGcbbFileButton(self, event):
        if self.dlgFile is None:
            self.dlgFile=wx.FileDialog(self, "Open", ".", "", "all files (*.*)|*.*", wx.OPEN)
        #self.dlgFile.Centre()
        self.dlgFile.SetPath(self.prjDN+os.sep)
        ret=self.dlgFile.ShowModal()
        if ret==wx.ID_OK:
            bNew=False
            if self.selAttrIdx<0:
                bNew=True
            else:
                if self.attrName[:len(u'file')]!=u'file':
                    bNew=True
            self.AddFile(self.dlgFile.GetPath(),bNew)
        event.Skip()
    def AddFile(self,filename,bNew=True):
        filename=string.replace(filename,'\\','/')
        prjDN=string.replace(self.prjDN,'\\','/')
        tupVal=fnUtil.getFilenameRel2BaseDir(filename,prjDN)
        if tupVal[0]!=u'.':
            dlg=wx.MessageDialog(self,u'Do you to import data?' ,
                        u'vgaPrjEng Attributes File',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                if self.dlgDocSel is None:
                    self.dlgDocSel=vDocSelDialog(self)
                    self.dlgDocSel.SetLanguage(self.lang)
                    self.dlgDocSel.SetDoc(self.netDocument,True)
                    #self.dlgDocSel.SetDocFN(self.docFN)
                #self.dlgDocSel.Centre()
                ret=self.dlgDocSel.ShowModal()
                if ret==1:
                    node=self.dlgDocSel.GetSelNode()
                    if node is not None:
                        if self.netPrj is not None:
                            prjid=self.doc.getAttribute(self.doc.getBaseNode(),'prjfid')
                            prjnode=self.netPrj.GetPrj(prjid)
                        else:
                            prjnode=None
                        if prjnode is not None:
                            sShort=self.netPrj.GetPrjName(prjnode)
                            sClient=self.netPrj.GetCltShort(prjnode)
                        else:
                            sShort=u''
                            sClient=u''
                        docNr=self.doc.getAttribute(node,'id')
                        dn,fn=os.path.split(filename)
                        lst=self.dlgDocSel.GetDocPath()
                        sDocPath=string.join(lst,os.path.sep)
                        dn=os.path.join(prjDN,sDocPath)
                        try:
                            os.makedirs(dn)
                        except:
                            pass
                        nFN=os.path.join(dn,sClient+sShort+'_'+docNr+'_'+fn)
                        bOk=False
                        i=0
                        while bOk==False:
                            if i==0:
                                s=''
                            else:
                                s='%d.'%i
                            sExts=string.split(nFN,'.')
                            sExt=sExts[-1]
                            nFN=nFN[:-len(sExt)]+s+sExt
                            try:
                                os.stat(nFN)
                                i+=1
                            except:
                                bOk=True
                        shutil.copy(filename,nFN)
                        tupVal=fnUtil.getFilenameRel2BaseDir(nFN,prjDN)
            dlg.Destroy()
        if bNew:
            # generate new link
            sAttr=self.__getNextFree__(u'file')
            tupVal=string.replace(tupVal,'\\','/')
            self.dictAttributes[sAttr]={'type':'file','val':tupVal}
            idx=self.lstAttr.InsertImageStringItem(sys.maxint, 
                                            sAttr, -1)
            self.lstAttr.SetStringItem(idx,1,tupVal,-1)
            self.__setModified__(True)
        else:
            tupVal=string.replace(tupVal,'\\','/')
            self.lstAttr.SetStringItem(self.selAttrIdx,1,tupVal,-1)
            self.dictAttributes[self.attrName]['val']=tupVal
            self.__setModified__(True)
        
    def __openFile__(self, sFN):
        sPrjFN=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
        if sPrjFN is None:
            sPrjFN=sFN
        sPrjFN=string.replace(sPrjFN,'/',os.sep)
        sExts=string.split(sPrjFN,'.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        mime = fileType.GetMimeType()
        if mime is None:
            mime="File extension"
        cmd = fileType.GetOpenCommand(sPrjFN, mime)
        wx.Execute(cmd)
        pass
    def __openURL__(self, sUrl):
        fileType = wx.TheMimeTypesManager.GetFileTypeFromMimeType(u'text/html')
        mime = fileType.GetMimeType()
        if mime is None:
            mime="File extension"
        cmd = fileType.GetOpenCommand(sUrl, mime)
        wx.Execute(cmd)
        
    def AddUrl(self,url):
        if self.bLock:
            return
        # generate new link
        sAttr=self.__getNextFree__(u'url')
        self.dictAttributes[sAttr]={'type':'url','val':url}
        idx=self.lstAttr.InsertImageStringItem(sys.maxint, 
                                        sAttr, -1)
        self.lstAttr.SetStringItem(idx,1,url,-1)
        self.__setModified__(True)
    def OnLstAttrLeftDclick(self, event):
        if self.selAttrIdx<0:
            return
        if self.attrName[:4]=='file':
            self.__openFile__(self.dictAttributes[self.attrName]['val'])
        elif self.attrName[:3]=='url':
            self.__openURL__(self.dictAttributes[self.attrName]['val'])
        event.Skip()

    def OnGcbbOpenButton(self, event):
        if self.selAttrIdx<0:
            return
        if self.attrName[:4]=='file':
            self.__openFile__(self.dictAttributes[self.attrName]['val'])
        elif self.attrName[:3]=='url':
            self.__openURL__(self.dictAttributes[self.attrName]['val'])
        event.Skip()

    def OnVgpAttrSize(self, event):
        self.SetSize(event.GetSize())
        self.Layout()
        self.Refresh()
        event.Skip()
        
