import libxml2

import boa.tool.log.vtLog as vtLog

from boa.tool.xml.vtXmlDom import vtXmlDom
import boa.vApps.vPrjEng.Elements as Elements
import boa.vApps.vPrjEng.Hierarchy as Hierarchy
#from boa.vApps.vPrjEng.vgtrXmlPrjEngThreadInstance import *

VERBOSE=1

class vXmlDomPrjEng(vtXmlDom):
    def __init__(self,attr='id',skip=[],synch=False,verbose=0):
        vtXmlDom.__init__(self,attr,skip,synch,verbose)
        #self.thdProcInst=thdProcInstance(None)
        #self.thdProcInst.SetDoc(self)
        #self.verbose=verbose
        #EVT_THREAD_PROC_INSTANCE_FINISHED(self,self.OnProcInstanceFin)
        #EVT_THREAD_PROC_INSTANCE(self,self.OnProcInstanceProgress)
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'prjengs')
    def New(self,revision='1.0',root='projectengineerings',bConnection=False):
        vtXmlDom.New(self,revision,root)
        elem=self.createSubNode(self.getRoot(),'prjengs')
        #self.setAttribute(elem,self.attr,'')
        #self.checkId(elem)
        #self.processMissingId()
        #self.clearMissing()
        if bConnection==False:
            
            sub=self.createSubNode(elem,'instance')
            self.addNode(None,sub)
            self.setNodeText(sub,'name','instance')
            #self.setNodeTextAttr(sub,'name','Instance','language','en')
            #self.setNodeTextAttr(sub,'name','Instanz','language','de')
                            
            
            sub=self.createSubNode(elem,'templates')
            self.addNode(None,sub)
            self.setNodeText(sub,'name','templates')
            #self.setNodeTextAttr(sub,'name','Templates','language','en')
            #self.setNodeTextAttr(sub,'name','Vorlagen','language','de')

        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
    def __isSkip__(self,node):
        if vtXmlDom.__isSkip__(self,node):
            return True
        if Elements.isProperty(node.name,'isSkip'):
            if Elements.isProperty(node.name,'isAddIDs'):
                return False
            return True
        return False
    def IsSkip(self,node):
        if vtXmlDom.IsSkip(self,node):
            return True
        if Elements.isProperty(node.name,'isSkip'):
            return True
        return False
    def __isNode2CalcFingerPrint__(self,node):
        if Elements.isProperty(self.getTagName(node),'isSkip'):
            if Elements.isProperty(self.getTagName(node),'isAddIDs'):
                return True
            return True
        return False
    def __genIds__(self,c,node,ids):
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        if self.__isSkip__(node)==False:
            self.procChildsKeys(c,self.__genIds__,c,ids)
            return 0
        return 0
    def __buildAttrXmlDoc__(self,tn,node):
        for c in self.getChilds(node):
            if self.hasChilds(c):
                tc=self.cloneNode(c,False)
                tn.addChild(tc)
                self.__buildAttrXmlDoc__(tc,c)
            else:
                tn.addChild(self.cloneNode(c,True))
    def __buildXmlDoc__(self,tn,node,bIncludeNodes):
        for c in self.getChilds(node):
            #if c.name in self.skip:
            #    continue
            #if c.name[:2]=='__':
            #    continue
            a=c.hasProp(self.attr)
            if a is None:
                if self.hasChilds(c):
                    tc=self.cloneNode(c,False)
                    tn.addChild(tc)
                    self.__buildAttrXmlDoc__(tc,c)
                else:
                    tn.addChild(self.cloneNode(c,True))
                continue
            if Elements.isProperty(c.name,'isAddIDs'):
                tc=self.cloneNode(c,False)
                tn.addChild(tc)
                self.__buildXmlDoc__(tc,c,True)
                continue
            if bIncludeNodes:
                tc=self.cloneNode(c,False)
                tn.addChild(tc)
                self.__buildXmlDoc__(tc,c,bIncludeNodes)
    def GetNodeXmlContent(self,node=None,bFull=False,bIncludeNodes=False):
        if node is None:
            node=self.getRoot()
        tmpDoc=libxml2.newDoc('1.0')
        if bFull:
            tn=self.cloneNode(node,True)
        else:
            tn=self.cloneNode(node,False)
            self.__buildXmlDoc__(tn,node,bIncludeNodes)
        tmpDoc.setRootElement(tn)
        #content=tmpDoc.c14nMemory()
        content=tmpDoc.serialize('ISO-8859-1')
        tmp=content.decode('ISO-8859-1')
        del tmpDoc
        #return content
        return tmp.encode('ISO-8859-1')
    def SetNodeXmlContent(self,node,s):
        if node is None:
            node=self.getRoot()
        tmpDoc=libxml2.parseMemory(s,len(s))
        for c in self.getChilds(node):
            #if c.name in self.skip:
            #    continue
            #if c.name[:2]=='__':
            #    continue
            a=c.hasProp(self.attr)
            if a is None:
                #print '   delete node'
                self.deleteNode(c,node)
            else:
                if Elements.isProperty(c.name,'isAddIDs'):
                    self.deleteNode(c,node)
        tmp=tmpDoc.getRootElement()
        #tmp.unlinkNode()
        for c in self.getChilds(tmp):
            c.unlinkNode()
            node.addChild(c)
        tmpDoc.setRootElement(None)
        del tmpDoc
        self.AlignNode(node,iRec=2)
        return node
    
    #def addNode(self,par,node):
    #    vtXmlDom.addNode(self,par,node)
    #    self.onInstance(par,node)
        
    def doInstanceIDs(self,node,instance=True):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,self.getKey(node),
                        origin='vXmlDomPrjEng')
        for n in self.getChilds(node):
            id=self.getAttribute(n,'id')
            fID=self.getAttribute(n,'fid')
            if len(fID)>0:
                self.idDict['fid'].append((fID,n))
            if self.IsKeyValid(id):
                if instance==True:
                    self.setAttribute(n,'iid',id)
                oldID=self.getAttribute(n,'id')
                self.setAttribute(n,'id','')
                self.ids.CheckId(n)
                self.ids.ProcessMissing()
                self.ids.ClearMissing()
                newID=self.getAttribute(n,'id')
                self.idDict['old_id'][oldID]=((n,oldID,newID))
                self.doInstanceIDs(n,instance)
    def doInstanceing(self,par,child,instance=True):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'par:%s child:%s'%(self.getKey(par),self.getKey(child)),
                    origin='vXmlDomPrjEng')
        self.appendChild(par,child)
        self.idDict={'fid':[],'old_id':{},'new_id':{}}
        oldID=self.getAttribute(child,'id')
        if instance==True:
            id=self.getAttribute(child,'id')
            self.setAttribute(child,'iid',id)
        self.setAttribute(child,'id','')
        self.ids.CheckId(child)
        self.ids.ProcessMissing()
        self.ids.ClearMissing()
        newID=self.getAttribute(child,'id')
        self.idDict['old_id'][oldID]=((child,oldID,newID))
        
        self.doInstanceIDs(child,instance)
        for fID,fNode in self.idDict['fid']:
            if self.idDict['old_id'].has_key(fID):
                self.setAttribute(fNode,'fid',
                    self.getAttribute(self.idDict['old_id'][fID][0],'id'))
        self.idDict={'fid':[],'old_id':{},'new_id':{}}
        self.AlignNode(child,iRec=10)
    def doClone(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    self.getKey(node),origin='vXmlDomPrjEng')
        childs=self.getChilds(node)
        newNode=self.cloneNode(node,False)
        for c in childs:
            tagName=self.getTagName(c)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            id=self.getAttribute(c,'id')
            if self.IsKeyValid(id):
                nc=self.doClone(c)
                self.appendChild(newNode,nc)
            else:
                nc=self.cloneNode(c,False)
                self.appendChild(newNode,nc)
                subchilds=self.getChilds(c)
                bFound=False
                for sc in subchilds:
                    tagName=self.getTagName(sc)
                    if tagName=='val':
                        bFound=True
                        self.setText(nc,self.getText(sc))
                        self.setAttribute(nc,'ih','1')
                        break;
                if bFound==False:
                    self.setText(nc,self.getText(c))
            pass
        return newNode
    def doStoreCloneChilds(self,instNode):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        self.getKey(instNode),origin='vXmlDomPrjEng')
        childs=self.getChilds(instNode)
        for c in childs:
            if len(self.getAttribute(instNode,'iid'))>0:
                self.updateStoreClone(c)
                self.doStoreCloneChilds(c)
    def doStoreClone(self,node,instNode):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        'par:%s child:%s'%(self.getKey(node),self.getKey(instNode)),
                        origin='vXmlDomPrjEng')
        nodeTag=self.getNodeText(instNode,'tag')
        id=self.getAttribute(instNode,'id')
        self.setNodeTextAttr(node,'__node',nodeTag,'iid',id)
        self.updateStoreClone(instNode)
        self.doStoreCloneChilds(instNode)
        self.AlignNode(node,iRec=0)
    def updateStoreClone(self,instNode):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'id:%s iid:%s'%(self.getKey(instNode),
                    self.getAttribute(instNode,'iid')),
                    origin='vXmlDomPrjEng')
        iid=self.getAttribute(instNode,'iid')
        if len(iid)<=0:
            return None
        node=self.getNodeById(iid)
        if node is None:
            return None
        self.checkStoreClone(node)
        nodeTag=self.getNodeText(instNode,'tag')
        id=self.getAttribute(instNode,'id')
        n=self.getChildAttr(node,'__node','iid',id)
        if n is None:
            if VERBOSE:
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                        'add instanced store',verbose=VERBOSE,
                        origin='vXmlDomPrjEng')
            self.setNodeTextAttr(node,'__node',nodeTag,'iid',id)
            self.AlignNode(node,iRec=0)
            #self.setNodeFull(node)
            return node
        else:
            if VERBOSE:
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                        'set instanced store',verbose=VERBOSE,
                        origin='vXmlDomPrjEng')
            oldTag=self.getText(n)
            self.setText(n,nodeTag)
            if VERBOSE:
                print 'new',n
            if oldTag!=nodeTag:
                #self.setNodeFull(node)
                return node
            else:
                return None
    def updateAllStore(self,node):
        bChanged=False
        childs=self.getChilds(node,'__node')
        if len(childs)==0:
            return False
        for c in childs:
            iid=self.getAttribute(c,'iid')
            instNode=self.getNodeById(iid)
            if instNode is None:
                self.deleteNode(c,node)
                bChanged=True
                continue
            oldTag=self.getText(c)
            nodeTag=self.getNodeText(instNode,'tag')
            self.setText(c,nodeTag)
            if oldTag!=nodeTag:
                bChanged=True
        childs=self.getChildsAttr(node,'id')
        for c in childs:
            if self.updateAllStore(c):
                bChanged=True
        return bChanged
    def removeStoreClone(self,instNode):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'id:%s iid:%s'%(self.getKey(instNode),
                    self.getAttribute(instNode,'iid')),
                    origin='vXmlDomPrjEng')
        iid=self.getAttribute(instNode,'iid')
        if len(iid)<=0:
            return None
        node=self.ids.GetIdStr(iid)[1]
        if node is None:
            return None
        nodeTag=self.getNodeText(instNode,'tag')
        id=self.getAttribute(instNode,'id')
        n=self.getChildAttr(node,'__node','iid',id)
        self.deleteNode(n,node)
        
        childs=self.getChilds(instNode)
        for c in childs:
            self.removeStoreClone(c)
        return node
    def removeAllStoreClone(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        self.getKey(node),origin='vXmlDomPrjEng')
        childs=self.getChilds(node)
        for c in childs:
            if self.getTagName(c)=='__node':
                self.deleteNode(c,node)
                    
            self.removeAllStoreClone(c)
    def checkStoreClone(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        self.getKey(node),origin='vXmlDomPrjEng')
        childs=self.getChilds(node,'__node')
        for c in childs:
            iid=self.getAttribute(c,'iid')
            if len(iid)<=0:
                continue
            instNode=self.ids.GetIdStr(iid)[1]
            if instNode is None:
                self.deleteNode(c,node)
    def getInstBase(self,node):
        # search upwards
        if len(self.getAttribute(node,'iid'))<0:
            return None
        par=self.getParent(node)
        lastPar=node
        while (len(self.getAttribute(par,'iid'))>0):
            lastPar=par
            par=self.getParent(par)
        return lastPar
    def getTmplBase(self,node):
        # search upwards
        if self.getChild(node,'__node') is None:
            return None
        par=self.getParent(node)
        lastPar=node
        while self.getChild(par,'__node') is not None:
            lastPar=par
            par=self.getParent(par)
        return lastPar
    def __findInstPar__(self,node,posIds):
        # search upwards
        par=self.getParent(node)
        lastPar=node
        while (len(self.getAttribute(par,'iid'))>0):
            if self.getKey(par) in posIds:
                return par
            lastPar=par
            par=self.getParent(par)
        def searchDownWards(n):
            #print n
            for c in self.getChildsAttr(n,self.attr):
                #print c
                if len(self.getAttribute(c,'iid'))<=0:
                    continue
                if self.getKey(c) in posIds:
                    return c
                ret=searchDownWards(c)
                if ret is not None:
                    return ret
            return None
        return searchDownWards(lastPar)
        
    def procInstancedChilds(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        self.getKey(node),origin='vXmlDomPrjEng')
        if 1==0:
            print '__procInstancedChilds__'
        self.checkStoreClone(node)
        # process instanced nodes 
        childs=self.getChilds(node,'__node')
        
        tchilds=self.getChilds(node)
        dTmplChilds={}
        for it in tchilds:
            tagName=self.getTagName(it)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            id=self.getKey(it)
            if self.IsKeyValid(id)==False:
                continue
            dTmplChilds[id]=it
            
        iAct=0
        for c in childs:
            iAct+=1
            #if self.gProcess is not None:
            #    self.gProcess.SetValue(iAct)
            instNode=self.ids.GetIdStr(self.getAttribute(c,'iid'))[1]
            if instNode is None:
                continue
                
            ichilds=self.getChilds(instNode)
            dInstChilds={}
            for it in ichilds:
                tagName=self.getTagName(it)
                if tagName[:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                #if tagName in ['Safe','Log']:
                    continue
                if tagName in ['tag','name','description']:
                    continue
                id=self.getAttribute(it,'iid')
                if self.IsKeyValid(id)==False:
                    continue
                dInstChilds[id]=it
                
            lTmplChilds=dTmplChilds.keys()
            lTmplChilds.sort()
            lInstChilds=dInstChilds.keys()
            lInstChilds.sort()
            if 1==0:   # self.verbose>0:
                vtLog.vtLog(None,'---------------------',verbose=1)
                vtLog.vtLog(None,'tmpl childs:%s'%repr(dTmplChilds),verbose=1)
                vtLog.vtLog(None,'+++++++++++++++++++++',verbose=1)
                vtLog.vtLog(None,'inst childs:%s'%repr(dInstChilds),verbose=1)
                vtLog.vtLog(None,'=====================',verbose=1)
            bAlign=False
            # find new child nodes
            for k in lTmplChilds:
                try:
                    it=dInstChilds[k]
                except:
                    child=dTmplChilds[k]
                    #print child
                    #print instNode
                    bMoved=False
                    parTmpl=self.getParent(child)
                    posParNodes=self.getChilds(parTmpl,'__node')
                    posParIds=[]
                    for posParNode in posParNodes:
                        posParIds.append(self.getAttribute(posParNode,'iid'))
                    posNodes=self.getChilds(child,'__node')
                    for posNode in posNodes:
                        posChild=self.getNodeById(self.getAttribute(posNode,'iid'))
                        parInst=self.__findInstPar__(instNode,[self.getKey(posChild)])
                        if parInst is not None:
                            #print parInst
                            #print posParIds
                            parInst=self.__findInstPar__(parInst,posParIds)
                            if parInst is not None:
                                self.moveNode(parInst,posChild)
                                #vtLog.vtLog(None,'moved',verbose=1)
                                bMoved=True
                                break
                    if bMoved==False:
                        # got new
                        clonechild=self.doClone(child)
                        self.doInstanceing(instNode,clonechild)
                        self.doStoreClone(child,clonechild)
                        
                        self.setNode(child)  # maybe 050903
                        self.addNodeCorrectId(instNode,clonechild)   # maybe 050903
                
                    bAlign=True
            # find del attributes
            for k in lInstChilds:
                try:
                    val=dTmplChilds[k]
                except:
                    it=dInstChilds[k]
                    tmplChild=self.getNodeById(k)
                    if tmplChild is not None:
                        # is this movable?
                        parTmpl=self.getParent(tmplChild)
                        posParNodes=self.getChilds(parTmpl,'__node')
                        posParIds=[]
                        for posParNode in posParNodes:
                            posParIds.append(self.getAttribute(posParNode,'iid'))
                        parInst=self.__findInstPar__(it,posParIds)
                    else:
                        # he do move only
                        parInst=None
                    if parInst is not None:
                        self.moveNode(parInst,it)
                        #vtLog.vtLog(None,'moved',verbose=1)
                    else:
                        #print tmplChild
                        # got del
                        self.delNode(it)
                    #it=self.ids.GetIdStr(k)[1]
                    #self.deleteNode(it)
                    bAlign=True
            # check childs
            # do not process recursively WRO 050625
            #for k in lTmplChilds:
            #    self.__procInstanced__(dTmplChilds[k],doc)
            if bAlign:
                self.AlignNode(instNode,iRec=2)
    def procInstancedAttr(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                        self.getKey(node),origin='vXmlDomPrjEng')
        if 1==0:
            print '__procInstancedAttr__'
        # process instanced nodes 
        childs=self.getChilds(node,'__node')
        iLen=len(childs)
        if iLen==0:
            return
        tchilds=self.getChilds(node)
        dTmplChilds={}
        for it in tchilds:
            tagName=self.getTagName(it)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            if tagName in ['tag','name','description']:
                continue
            fid=self.getAttribute(it,'fid')
            if len(fid)>0:
                sVal=self.getText(it)
            else:
                sVal=self.getNodeText(it,'val')
            dTmplChilds[tagName]=(sVal,it)
        iAct=0
        for c in childs:
            iAct+=1
            #if self.gProcess is not None:
            #    self.gProcess.SetValue(iAct)
            instNode=self.ids.GetIdStr(self.getAttribute(c,'iid'))[1]
            if instNode is None:
                continue
                
            ichilds=self.getChilds(instNode)
            dInstChilds={}
            for it in ichilds:
                tagName=self.getTagName(it)
                if tagName[:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                #if tagName in ['Safe','Log']:
                    continue
                if tagName in ['tag','name','description']:
                    continue
                dInstChilds[tagName]=it
                
            lTmplChilds=dTmplChilds.keys()
            lTmplChilds.sort()
            lInstChilds=dInstChilds.keys()
            lInstChilds.sort()
            if 1==0:
                print '---------------------'
                print dTmplChilds
                print '+++++++++++++++++++++'
                print dInstChilds
                print '====================='
            bAlign=False
            # find new attributes
            for k in lTmplChilds:
                try:
                    it=dInstChilds[k]
                    fidC=self.getAttribute(it,'fid')
                    fid=self.getAttribute(dTmplChilds[k][1],'fid')
                    if len(fidC)>0:
                        # update predefined type link
                        if fidC!=fid:
                            #update needed
                            if len(fid)>0:
                                self.setAttribute(it,'fid',fid)
                                self.setText(it,dTmplChilds[k][0])
                            else:
                                self.removeAttribute(it,'fid')
                                self.setAttribute(it,'ih','1')
                                self.setText(it,dTmplChilds[k][0])
                    else:
                        if len(fid)>0:
                            # update needed
                            self.removeAttribute(it,'ih')
                            self.setAttribute(it,'fid',fid)
                            self.setText(it,dTmplChilds[k][0])
                except:
                    # got new
                    a=self.createSubNodeText(instNode,k,dTmplChilds[k][0])
                    fid=self.getAttribute(dTmplChilds[k][1],'fid')
                    if len(fid)>0:
                        self.setAttribute(a,'fid',fid)
                    else:
                        self.setAttribute(a,'ih','1')
                    if 1==0:
                        parTmpl=self.getParent(instNode)
                        parTmpl=self.getParent(parTmpl)
                        s='new instance attr %s %s'%(self.getNodeText(parTmpl,'tag'),
                            self.getAttribute(parTmpl,'id'))
                            
                    #instNode.appendChild(a)
                    bAlign=True
            # find del attributes
            for k in lInstChilds:
                try:
                    val=dTmplChilds[k][0]
                except:
                    # got del
                    it=dInstChilds[k]#self.ids.GetIdStr(k)[1]
                    self.deleteNode(it,instNode)
                    bAlign=True
            if bAlign:
                self.AlignNode(instNode,iRec=2)
                from boa.vApps.vPrjEng.vgpAttr import CACHE_INH as CACHE_INH
                try:
                    del CACHE_INH[node.getAttribute('id')]
                except:
                    pass
    def equalizeInstanced(self,node,instNode):
        if instNode is None:
            return
        if node is None:
            return
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'id:%s inst id:%s'%(self.getKey(node),self.getKey(instNode)),
                    origin='vXmlDomPrjEng')
        childs=self.getChilds(node)
        instChilds=self.getChilds(instNode)
        def __getAttrDict__(childs):
            d={}
            for c in childs:
                if len(self.getAttribute(c,'id'))>0:
                    continue
                tagName=self.getTagName(c)
                if tagName[0:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                    continue
                d[tagName]=c
            return d
        dTmpl=__getAttrDict__(childs)
        dInst=__getAttrDict__(instChilds)
        for k in dTmpl.keys():
            try:
                aInst=dInst[k]
                aNode=dTmpl[k]
                valNode=self.getChild(aNode,'val')
                if valNode is None:
                    sVal=self.getText(aNode)
                    self.setText(aInst,sVal)
                    if self.verbose>0:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                'key:%s val:%s'%(k,sVal),
                                origin='vXmlDomPrjEng')
                else:
                    sVal=self.getText(valNode)
                    self.setText(aInst,sVal)
                    if self.verbose>0:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                'key:%s val:%s'%(k,sVal),
                                origin='vXmlDomPrjEng')
                if self.verbose>0:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                            'val:%s'%(self.doc.getText(aInst)),
                            origin='vXmlDomPrjEng')
            except:
                pass
        #self.doc.updateNode(instNode)
        vtLog.vtLngCallStack(self,vtLog.DEBUG,'',callstack=False)
        self.setNode(instNode)
        vtLog.vtLngCallStack(self,vtLog.DEBUG,'',callstack=False)
        #ti=self.tree.__findNode__(self.tree.tiRoot,instNode)
        #self.tree.__refreshLabel__(ti)
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,'',callstack=False)
        #tiPar=self.tree.GetItemParent(ti)
        #if self.tree.IsExpanded(tiPar):
        #    self.tree.SortChildren(tiPar)
    def instancedUpdate(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'id:%s'%(self.getKey(node)),origin='vXmlDomPrjEng')
        if 1==0:
            print 'instanced update'
        
        childs=self.getChilds(node,'__node')
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'len:%d'%(len(childs)),origin='vXmlDomPrjEng')
        for c in childs:
            #self.iAct+=1
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'node:%s'%(c),origin='vXmlDomPrjEng')
            #if self.tree is not None:
            #    wx.PostEvent(self.tree,wxThreadProcInstance(self.iAct,self.iCount))
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'node:%s'%(c),origin='vXmlDomPrjEng')
            #if bStart==True:
            #    if self.gProcess is not None:
            #        self.gProcess.SetValue(iAct)
            iid=self.getAttribute(c,'iid')
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'id:%s iid:%s'%(self.getKey(node),iid),origin='vXmlDomPrjEng')
            if len(iid)<=0:
                continue
            instNode=self.getNodeById(iid)
            parNode=self.getParent(node)
            if self.getChild(parNode,'__node') is not None:
                self.equalizeInstanced(node,instNode)
        for c in self.getChildsAttr(node,'id'):
            self.instancedUpdate(c)
    
    def procInstancedElem(self,node):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    self.getKey(node),origin='vXmlDomPrjEng')
        if 1==0:
            print '__procInstancedElem__'
        # process instanced nodes 
        tchilds=self.getChilds(node)
        lTmplChilds=[]
        for it in tchilds:
            tagName=self.getTagName(it)
            if tagName[:2]=='__':
                continue
            if Elements.isProperty(tagName,'isSkip'):
            #if tagName in ['Safe','Log']:
                continue
            if len(self.getAttribute(it,'id'))>0:
                lTmplChilds.append(it)
        
        iAct=0
        for it in lTmplChilds:
            iAct+=1
            #if self.gProcess is not None:
            #    self.gProcess.SetValue(iAct)
            bAlign=False
            childs=self.getChilds(it,'__node')
            if len(childs)<=0:
                # got new
                for c in childs:
                    instNode=self.ids.GetIdStr(self.getAttribute(c,'iid'))[1]
                    # find new attributes
                    par=instNode
                    child=self.doClone(it)
                    oldChild=it
                    self.doInstanceing(par,child)
                    self.doStoreClone(oldChild,child)
                    bAlign=True
            if bAlign:
                self.doc.AlignNode(instNode,iRec=2)
        
        childs=self.getChilds(node,'__node')
        iAct=0
        for c in childs:
            iAct+=1
            instNode=self.ids.GetIdStr(self.getAttribute(c,'iid'))[1]
            if instNode is None:
                continue
            ichilds=self.getChilds(instNode)
            lInstChilds=[]
            for it in ichilds:
                tagName=self.getTagName(it)
                if tagName[:2]=='__':
                    continue
                if Elements.isProperty(tagName,'isSkip'):
                #if tagName in ['Safe','Log']:
                    continue
                if len(self.getAttribute(it,'id'))>0:
                    lInstChilds.append(it)
            
            bAlign=False
            # find del attributes
            for it in lInstChilds:
                iid=self.getAttribute(it,'iid')
                bDel=False
                try:
                    nn=self.ids.GetIdStr(iid)[1]
                    if nn is None:
                        bDel=True
                except:
                    # got del
                    bDel=True
                if bDel:
                    #cti=self.__findNode__(self.tiRoot,it)
                    #self.Delete(cti)
                    self.ids.RemoveId(self.getAttribute(it,'id'))
                    self.removeStoreClone(it)
                    self.deleteNode(it,instNode)
                    bAlign=True
            if bAlign:
                self.AlignNode(instNode)
    def procInstanced(self,node,update=False):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    self.getKey(node),origin='vXmlDomPrjEng')
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    node,origin='vXmlDomPrjEng')
        self.checkStoreClone(node)
        self.procInstancedChilds(node)
        self.procInstancedAttr(node)
        if update:
            self.instancedUpdate(node)
    def onInstance(self,node,child):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    self.getKey(node),origin='vXmlDomPrjEng')
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,node)
        childs=self.getChilds(node,'__node')
        iLen=len(childs)
        iCount=iLen
        iAct=0
        for c in childs:
            iAct+=1
            instNode=self.getNodeById(self.getAttribute(c,'iid'))
            clonechild=self.doClone(child)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'cloned',verbose=1,origin='vXmlDomPrjEng')
            self.doInstanceing(instNode,clonechild)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'instanced',verbose=1,origin='vXmlDomPrjEng')
            self.doStoreClone(child,clonechild)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'instanced stored',verbose=1,origin='vXmlDomPrjEng')
            self.setNode(child)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'template info stored',verbose=1,origin='vXmlDomPrjEng')
            self.addNodeCorrectId(instNode,clonechild)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'instanced added to doc',verbose=1,origin='vXmlDomPrjEng')
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'finished',verbose=1,origin='vXmlDomPrjEng')
    def getAttributeVal(self,node):
        tmpNode=self.getChild(node,'fid')
        if tmpNode is not None:
            fid=self.getText(tmpNode)
            tmpNode=self.getChild(node,'__browse')
            if tmpNode is not None:
                # calc val
                sVal=Hierarchy.applyCalcNode(self,node)
                return sVal
            tmpl=self.GetIdStr(fid)[1]
            rootNode=Hierarchy.getRootNode(self,node)
            relNode=Hierarchy.getRelBaseNode(self,node)
            s=Hierarchy.getTagNames(self,rootNode,relNode,tmpl)
            return s
        return vtXmlDom.getAttributeVal(self,node)
        