#----------------------------------------------------------------------------
# Name:         vXmlPrjEngTree.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlPrjEngTree.py,v 1.8 2007/08/21 18:16:42 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vPrjEng.vNetPrjEng  import *
import wx
from vidarc.vApps.vPrjEng.vPrjEngElementsDialog import *
from vidarc.vApps.vPrjEng.vPrjEngElementEditDialog import *
from vidarc.vApps.vPrjEng.vVsasSelDialog import *
from vidarc.vApps.vPrjEng.vInstanceShowDialog import *
from vidarc.vApps.vPrjEng.vFilterDialog import *
import vidarc.vApps.vPrjEng.vElementsPanel as Elements
from vidarc.vApps.vPrjEng.vXmlPrjEngThreadInstanceTree import *

import vidarc.tool.log.vtLog as vtLog

import traceback
import fnmatch,time
import thread,threading

import images
import images_s88

IMG_LIST=None
IMG_DICT=None

ATTRS=['title','key','path']

VERBOSE=0
VERBOSE_ADD_NODE=0
VERBOSE_ADD_NODE_RESP=0
VERBOSE_SET_NODE=0
VERBOSE_GET_NODE=0
VERBOSE_DEL_NODE=0
VERBOSE_REMOVE_NODE=0
VERBOSE_MOVE_NODE=0
VERBOSE_LOCK=0
VERBOSE_UNLOCK=0    

class thdAddElementsInst(thdAddElements):
    def Start(self):
        self.keepGoing = self.running = True
        self.updateProcessbar=self.tree.bMaster
        if self.tree.bGuiUpdate:
            self.updateProcessbar=True
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        
        thread.start_new_thread(self.Run, ())
        #thread.start_new_thread(thdAddElementsInst.Run, ((self)))
        #print 'threadexit'
        #self.StartScheduled()
    def __procElem__(self,node,*args):
        parent=args[0][0]
        #vtLog.vtLogCallDepth(None,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,callstack=True)
        self.iAct+=1
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct))
        self.tree.doc.acquire()
        #vtLog.vtLogCallDepth(None,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,callstack=False)
        try:
            tagName,infos=self.tree.doc.getNodeInfos(node,self.lang)
        except:
            traceback.print_exc()
            self.tree.doc.acquire()
            return -1
        #print tagName,infos
        if tagName in self.tree.skip:
            if self.updateProcessbar:
                self.iAct+=self.tree.doc.GetElementAttrCount(node,'id')
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct))
            self.tree.doc.release()
            return 0
        #print 'dddd'
        if self.tree.isElement2Skip(tagName):
            if self.updateProcessbar:
                self.iAct+=1
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct))
            self.tree.doc.release()
            return 0
        try:
            #print parent
            #tmpNode=self.tree.GetPyData(parent)
            #print tmpNode
            #print node
            #print 'add'
            tn=self.tree.__addElementByInfos__(parent,node,tagName,infos,True)
            #print 'added'
        except Exception,list:
            traceback.print_exc()
            #print 'exceptin'
            #print list
            self.tree.doc.release()
            #print
            #print
            #print
            #print 'fault'
            #print parent
            #print node
            #print tagName
            #print infos
            return -1
        #print 'eeee'
        try:
            if infos[self.sExpandInfo]=='1':
                bExpand=True
            else:
                bExpand=False
            #print 'ffff'
            if self.tiFirst is None:
                self.tiFirst=tn
        except:
            traceback.print_exc()
        #vtLog.vtLogCallDepth(None,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,callstack=True)
        self.tree.doc.release()
        self.tree.doc.procChildsAttr(node,'id',self.__procElem__,tn)
        if bExpand:
            self.tree.Expand(tn)
        #vtLog.vtLogCallDepth(None,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,callstack=True)
        return 0
    def Run(self):
        self.tree.iBlockTreeSel=1
        #vtLog.vtLogCallDepth(None,'acquire',1,callstack=False)
        self.tree.doc.acquire('thread_tree_add')
        self.tree.doc.acquire('thread')
        self.tree.doc.release('thread_tree_add')
        
        #vtLog.vtLogCallDepth(None,'process',1,callstack=False)
        self.tree.iBlockTreeSel=1
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        self.sExpandInfo='|'+self.tree.sExpandName
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        self.iAct=0
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct,self.tree.doc.GetElementAttrCount()))
        bBuildCache=False
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        if self.ti==self.tree.tiRoot:
            if self.ti is None:
                # build root
                self.tree.doc.acquire()
                tagName,infos=self.tree.doc.getNodeInfos(self.node,self.lang)
                tn=self.tree.__addRootByInfos__(self.node,tagName,infos)
                self.tree.doc.release()
                self.ti=tn
                self.tree.tiRoot=tn
            self.tiRoot=self.ti
            bBuildCache=True
        else:
            self.tiRoot=None
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        id=self.tree.doc.getKey(self.node)
        if self.tree.doc.IsKeyValid(id):
            self.tree.doc.procAttr(self.node,self.tree.doc.attr,self.__procElem__,self.ti)
        else:
            self.tiFirst=self.ti
            self.tree.doc.procChildsAttr(self.node,self.tree.doc.attr,self.__procElem__,self.ti)
        #self.tree.doc.procChildsAttr(self.node,'id',self.__procElem__,self.ti)
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        if self.tiRoot is not None:
            self.tree.Expand(self.tiRoot)
        #self.tree.__sortAllChilds__(self.ti)
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,0))
        self.running = False
        self.tree.doc.release('thread')
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        if self.keepGoing:
            if self.func is not None:
                self.func(*self.args[0])
            if self.updateProcessbar:
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElementsFinished(self.tree,self.tiFirst))
        else:
            if self.updateProcessbar:
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElementsAborted(self.tree,self.tiFirst))
        #vtLog.vtLogCallDepth(None,'',1,callstack=False)
        self.keepGoing = False
        self.StartScheduled()
        #vtLog.vtLogCallDepth(None,'finshed:%d'%(self.running),1,callstack=True)
        self.tree.iBlockTreeSel=0
        
# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_INSTANCE=wx.NewEventType()
def EVT_VTXMLTREE_ITEM_INSTANCE(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_INSTANCE,func)
class vtXmlTreeItemInstance(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_ITEM_INSTANCE(<widget_name>, self.OnItemInstance)
    """

    def __init__(self,obj,tn,data,parID):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_INSTANCE)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        self.parentID=parID
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        return self.treeNodeData
    def GetParentID(self):
        return self.parentID

# defined event for vgpXmlTree item goto template
wxEVT_VTXMLTREE_ITEM_GOTO_TMPL=wx.NewEventType()
def EVT_VTXMLTREE_ITEM_GOTO_TMPL(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_GOTO_TMPL,func)
class vtXmlTreeItemGotoTmpl(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_ITEM_GOTO_TMPL(<widget_name>, self.OnItemInstance)
    """

    def __init__(self,obj,tn,data):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_GOTO_TMPL)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        return self.treeNodeData

# defined event for vgpXmlTree item goto 
wxEVT_VTXMLTREE_ITEM_GOTO=wx.NewEventType()
def EVT_VTXMLTREE_ITEM_GOTO(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_GOTO,func)
class vtXmlTreeItemGoto(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_ITEM_GOTO(<widget_name>, self.OnItemInstance)
    """

    def __init__(self,id,data):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_GOTO)
        self.id=id
        self.treeNodeData=data
    def GetID(self):
        return self.id
    def GetTreeNodeData(self):
        return self.treeNodeData

class vXmlPrjEngTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,master=False,
                    controller=False,gui_update=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.ids=None #vtXmlDom.NodeIds(None)
        self.doc=None
        self.prjAttr=ATTRS
        self.nodeInfos=['tag','name','|id','|iid','|expand']
        self.nodeKey='|id'
        self.lang='en'
        self.languages=[]
        self.langIds=[]
        self.skip=[]
        self.dlgElem=None
        self.dlgElemEdit=None
        self.dlgViewElementsTable=None
        self.dlgInstance=None
        self.dlgVsas=None
        self.dlgInstShow=None
        self.dlgFilter=None
        self.nodeCut=None
        self.triCut=None
        self.nodeCopy=None
        self.nodeMoveTarget=None
        self.nodeDupTmpl=None
        self.gProcess=None
        self.frmMain=None
        self.thdAddElements=thdAddElementsInst(self)
        
        self.bLangMenu=False
        self.bGrpMenu=False
        
        self.bGuiUpdate=gui_update
        self.bAddElement=False
        #self.thdProcInst=thdProcInstance(self)
        #EVT_THREAD_PROC_INSTANCE_FINISHED(self,self.OnProcInstanceFin)
        #EVT_THREAD_PROC_INSTANCE(self,self.OnProcInstanceProgress)
        
        self.Bind(wx.EVT_TREE_BEGIN_DRAG , self.OnDragBegin)
        self.Bind(wx.EVT_TREE_END_DRAG   , self.OnDragEnd)
    def IsThreadRunning(self):
        bRunning=False
        if self.thdAddElements.IsRunning():
            bRunning=True
        return bRunning
    def Stop(self):
        if self.thdAddElements.IsRunning():
            self.thdAddElements.Stop()
        #while self.IsThreadRunning():
        #    time.sleep(1.0)
    def OnCompareItems(self,ti1,ti2):
        n1=self.GetPyData(ti1)
        n2=self.GetPyData(ti2)
        if (n1 is not None and n2 is not None):
            s1=self.doc.getTagName(n1)
            s2=self.doc.getTagName(n2)
            i=cmp(s1,s2)
            if i!=0:
                return i
            lst1=[]
            lst2=[]
            for l in self.label:
                try:
                    s1=self.doc.getNodeText(n1,l[0])
                except:
                    s1=''
                try:
                    s2=self.doc.getNodeText(n2,l[0])
                except:
                    s2=''
                lst1.append(s1)
                lst2.append(s2)
            for s1,s2 in zip(lst1,lst2):
                i=cmp(s1,s2)
                if i!=0:
                    return i
        else:
            if n1 is None:
                return 1
            else:
                l1=self.GetItemString(ti1)
                l2=self.GetItemString(ti2)
                return cmp(l1,l2)
        return 0
    def SetLanguage(self,lang,bBlock=False):
        self.language=lang
        self.lang=lang
        if bBlock==False:
            self.SetNode(None)
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[('tag',''),('name','')]
    def SetDlgInstance(self,dlg):
        self.dlgInstance=dlg
        if self.dlgInstance is not None:
            self.dlgInstance.SetMainTree(self)
    def SetViewElementsTable(self,dlg):
        self.dlgViewElementsTable=dlg
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            img=images.getPrjEngImage()
            imgSel=images.getPrjEngImage()
            self.__addElemImage2ImageList__('root',img,imgSel,True)
            img=images.getPrjEngImage()
            imgSel=images.getPrjEngImage()
            self.__addElemImage2ImageList__('prjengs',img,imgSel,True)
            for e in Elements.ELEMENTS:
                try:
                    imgName=e[1]['__img']
                    #f = getattr(images_s88, 'get%sBitmap' % imgName)
                    f = getattr(images_s88, 'get%sImage' % imgName)
                    img = f()
                    imgSel=f()
                    self.__addElemImage2ImageList__(e[0],img,imgSel,True)
                except Exception,list:
                    #print list
                    #self.SetStatusText('open fault. (%s)'%list, 1)
                    pass
            self.imgDict['attr']={}
            img=images_s88.getAttributeBitmap()
            self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
            img=images_s88.getAttributeSelBitmap()
            self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
            self.SetImageList(self.imgLstTyp)
        return
        global IMG_LIST
        global IMG_DICT
        if IMG_LIST is None:
            self.imgDict={'elem':{},'attr':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
            self.imgLstTyp=wx.ImageList(16,16)
            img=images.getElementBitmap()
            mask=wx.Mask(img,wx.BLACK)
            img.SetMask(mask)
            self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
            
            img=images.getElementSelBitmap()
            mask=wx.Mask(img,wx.BLACK)
            img.SetMask(mask)
            self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
            
            img=images.getPrjEngBitmap()
            self.imgDict['elem']['prjengs']=[self.imgLstTyp.Add(img)]
            self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
            img=images.getPrjEngSelBitmap()
            self.imgDict['elem']['prjengs'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            
            img=images.getDocGrpBitmap()
            self.imgDict['elem']['maingroup']=[self.imgLstTyp.Add(img)]
            img=images.getDocGrpSelBitmap()
            self.imgDict['elem']['maingroup'].append(self.imgLstTyp.Add(img))
            
            img=images.getSubDocGrpBitmap()
            self.imgDict['elem']['docgroup']=[self.imgLstTyp.Add(img)]
            img=images.getSubDocGrpSelBitmap()
            self.imgDict['elem']['docgroup'].append(self.imgLstTyp.Add(img))
            
            for e in Elements.ELEMENTS:
                try:
                    imgName=e[1]['__img']
                    f = getattr(images_s88, 'get%sBitmap' % imgName)
                    img = f()
                    f = getattr(images_s88, 'get%sImage' % imgName)
                    imgInst = f()
                    for i in range(0,imgInst.GetWidth()):
                        for j in range(0,imgInst.GetHeight()):
                            rgb=[imgInst.GetRed(i,j),imgInst.GetGreen(i,j),imgInst.GetBlue(i,j)]
                            if [1,0,0]!=rgb:
                                def chRgb(a):
                                    a=a|128
                                    return a
                                rgb=map(chRgb,rgb)
                                imgInst.SetRGB(i,j,rgb[0],rgb[1],rgb[2])
                    imgInst=imgInst.ConvertToBitmap()
                    
                    self.imgDict['elem'][e[0]]=[self.imgLstTyp.Add(img)]
                    self.imgDict['elem'][e[0]].append(self.imgLstTyp.Add(img))
                    self.imgDict['elem'][e[0]].append(self.imgLstTyp.Add(imgInst))
                except Exception,list:
                    #print list
                    #self.SetStatusText('open fault. (%s)'%list, 1)
                    pass
            
            img=images_s88.getAttributeBitmap()
            self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
            img=images_s88.getAttributeSelBitmap()
            self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
            
            img=images.getTextBitmap()
            #img=images.getElementBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
            
            img=images.getYearBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['grp']['YYYY']=[self.imgLstTyp.Add(img)]
            
            img=images.getMonthBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['grp']['MM']=[self.imgLstTyp.Add(img)]
            
            img=images.getDayBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['grp']['DD']=[self.imgLstTyp.Add(img)]
            
            img=images.getUserBitmap()
            mask=wx.Mask(img,wx.BLACK)
            img.SetMask(mask)
            self.imgDict['grp']['manager']=[self.imgLstTyp.Add(img)]
            
            img=images.getClientBitmap()
            self.imgDict['grp']['attributes:clientshort']=[self.imgLstTyp.Add(img)]
            
            img=images.getTextSelBitmap()
            #img=images.getElementSelBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
            #icon=images.getApplicationIcon()
            #self.SetIcon(icon)
            IMG_LIST=self.imgLstTyp
            IMG_DICT=self.imgDict
        else:
            self.imgLstTyp=IMG_LIST
            self.imgDict=IMG_DICT
        self.SetImageList(self.imgLstTyp)
    def __getValFormat__(self,g,val,infos=None):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    val=val[:4]
                elif sFormat=='MM':
                    val=val[4:6]
                elif sFormat=='DD':
                    val=val[6:8]
                elif sFormat=='YYYY-MM-DD':
                    val=val[:4]+'-'+val[4:6]+'-'+val[6:8]
                elif sFormat=='MM-DD':
                    val=val[4:6]+'-'+val[6:8]
            if g[0]=='starttime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
            if g[0]=='endtime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
                
        if len(val)<=0:
            val="---"
        return val
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='person':
            #if sFormat=='':
            img=self.imgDict['grp']['person'][0]
            imgSel=self.imgDict['grp']['person'][0]
            return (img,imgSel)
        elif g[0]=='attributes:clientshort':
            #if sFormat=='':
            img=self.imgDict['grp']['attributes:clientshort'][0]
            imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return (img,imgSel)
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)

    def isElement2Skip(self,tagName):
        return Elements.isProperty(tagName,'isSkip')
    def isElementProperty(self,tagName,isProperty):
        return Elements.isProperty(tagName,isProperty)
    def __addElements__(self,parent,obj,func=None,*args):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
            'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
            origin=self.GetName())
        if self.bMaster==False:
            checkIds=False
        #self.doc.setNodeInfos2Get(self.nodeInfos)
        self.thdAddElements.AddElements(parent,obj,func,args)
    def OnTcNodeExpanded(self,evt):
        vtXmlGrpTree.OnTcNodeExpanded(self,evt)
        self.SortChildren(evt.GetItem())
        evt.Skip()
    def __checkElements__(self,obj):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
            'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
            origin=self.GetName())
        self.__checkStoreClone__(obj)
        self.__updateStoreClone__(obj)
        for c in self.doc.getChilds(obj):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__checkElements__(c)
    def SetDoc(self,doc,bNet=False):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
            'parent:%s master:%d net response:%d net:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse,bNet),
            origin=self.GetName())
        if doc is None:
            return
        if self.doc is not None:
            self.doc.DelConsumer(self)
        # FIXME if called more than once
        if bNet==True:
            EVT_NET_XML_OPEN_START(doc,self.OnOpenStart)
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
            EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            EVT_NET_XML_ADD_NODE_RESPONSE(doc,self.OnAddNodeResp)
            EVT_NET_XML_MOVE_NODE(doc,self.OnMoveNode)
            EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        if self.bMaster:
            self.doc.setNodeInfos2Get(self.nodeInfos)
        if self.tiCache is not None:
            self.tiCache.SetDoc(doc)
            self.tiCache.Clear()
            self.tiCache.Build(self)
    def OnOpenStart(self,evt):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,'OnOpenStart',origin=self.GetName())
        self.Clear()
        evt.Skip()
    def OnGotContent(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnGotContent;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bNetResponse==False:
            evt.Skip()
            return
        self.SetNode(None)
    def OnGetNode(self,evt):
        evt.Skip()
        vtLog.vtLngCallStack(self,vtLog.INFO,
            'OnGetNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
            origin=self.GetName())
        if self.bNetResponse:
            id=evt.GetID()
            tup=self.__findNodeByID__(None,id)
            if tup is not None:
                self.__refreshLabel__(tup[0])
                ti=self.GetItemParent(tup[0])
                self.SortChildren(ti)
            if VERBOSE_GET_NODE:
                vtLog.vtSpace()
    def OnSetNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnSetNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bNetResponse:
            id=evt.GetID()
            tup=self.__findNodeByID__(None,id)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'id:%s %d'%(id,tup is not None),origin=self.GetName())
            if tup is not None:
                self.__refreshLabel__(tup[0])
                ti=self.GetItemParent(tup[0])
                self.SortChildren(ti)
    def OnAddNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnAddNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bNetResponse:
            idPar=evt.GetID()
            idNew=evt.GetNewID()
            child=self.doc.getNodeById(idNew)
            tup=self.__findNodeByID__(None,idPar)
            #print tup
            #print self.doc.getNodeById(idPar)
            
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'idPar:%s %d'%(idPar,tup is not None),origin=self.GetName())
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'id:%s %d'%(idNew,child is not None),origin=self.GetName())
            if (tup is not None) and (child is not None):
                self.bAddElement=True
                self.__addElements__(tup[0],child)
                #self.SortChildren(tup[0])
    def OnAddNodeResp(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnAddNodeResp;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        #if self.bMaster:
        #    evt.Skip()
        #    return
        if self.bNetResponse:
            id=evt.GetID()
            idNew=evt.GetNewID()
            child=self.doc.getNodeById(idNew)
            idPar=self.doc.getKey(self.doc.getParent(child))
            tup=self.__findNodeByID__(None,idPar)
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'idPar:%s %d'%(idPar,tup is not None),origin=self.GetName())
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'id:%s %d'%(idNew,child is not None),origin=self.GetName())
            if (tup is not None) and (child is not None):
                #ti=self.__addElementAndChilds__(tup[0],child,self.skip)
                #self.SortChildren(tup[0])
                self.bAddElement=True
                self.__addElements__(tup[0],child)
    def OnMoveNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnMoveNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bNetResponse:
            idPar=evt.GetParID()
            id=evt.GetID()
            
            parNode=self.doc.getNodeById(idPar)
            node=self.doc.getNodeById(id)
            sParID=self.doc.getKey(parNode)
            sID=self.doc.getKey(node)
            #self.doc.moveNode(parNode,node)
            tupPar=self.__findNodeByID__(None,sParID)
            tup=self.__findNodeByID__(None,sID)
            if tupPar is None:
                return
            if tup is None:
                return
            tiSel=self.triSelected
            self.Delete(tup[0])
            if self.tiCache is not None:
                self.tiCache.DelID(sID)
            if self.bMaster:
                self.doc.AlignNode(tup[1])
                
            #self.__addElementAndChilds__(tupPar[0],tup[1],self.skip,False)
            #self.SortChildren(tupPar[0])
            self.__addElements__(tupPar[0],tup[1],
                                self.__postMoved__,
                                tup[1],sParID,tiSel,False)
            
            #ti=self.__findNode__(self.tiRoot,self.nodeCut)
            #self.objTreeItemSel=self.nodeCut
            #self.triSelected=ti
            #self.EnsureVisible(ti)
            #self.SelectItem(ti)
            #wx.PostEvent(self,vtXmlTreeItemInstance(self,
            #                    self.triSelected,self.objTreeItemSel,parID))
            if VERBOSE_MOVE_NODE:
                vtLog.vtSpace()
    def OnDelNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnDelNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
            #vtLog.vtCallStack(3,VERBOSE)
        if self.bNetResponse==False:
            return
        id=evt.GetID()
        tup=self.__findNodeByID__(None,id)
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
            'id:%s %d'%(id,tup is not None),origin=self.GetName())
        if tup is not None:
            self.Delete(tup[0])
            if self.tiCache is not None:
                self.tiCache.DelID(id)
            #if self.bMaster:
            #    self.doc.deleteNode(tup[1])
    def OnRemoveNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnRemoveNode;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bNetResponse==False:
            return
        id=evt.GetID()
        tup=self.__findNodeByID__(None,id)
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
            'id:%s %d'%(id,tup is not None),origin=self.GetName())
        if tup is not None:
            self.Delete(tup[0])
            if self.tiCache is not None:
                self.tiCache.DelID(id)
            if self.bMaster:
                self.doc.deleteNode(tup[1])
    def OnLock(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnLock;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
    def OnUnLock(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnUnLock;parent:%s master:%d net response:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse),
                origin=self.GetName())
        if self.bMaster:
            if self.objTreeItemSel is not None:
                if self.doc.isSameKey(self.objTreeItemSel,evt.GetID()):
                    resp=evt.GetResponse()
                    if resp in  ['released']:
                        self.doc.startEdit(self.objTreeItemSel)
                else:
                    if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))>0:
                        # instanced node selected
                        instNode=self.doc.getInstBase(self.objTreeItemSel)
                        
                        node=self.doc.getNodeById(evt.GetID())
                        if self.doc.getChild(node,'__node') is not None:
                            nodeTmpl=self.doc.getTmplBase(node)
                            if self.doc.isSameKey(nodeTmpl,self.doc.getAttribute(instNode,'iid')):
                                self.doc.startEdit(self.objTreeItemSel)
    def SetNode(self,node,skip=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'SetNode;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        self.bAddElement=False
        if skip is not None:
            self.skip=skip
        vtXmlGrpTree.SetNode(self,node)
        return
        #self.ids=vtXmlDom.NodeIds(doc)
        self.groupItems={}
        self.rootNode=node
        if skip is not None:
            self.skip=skip
        self.tiRoot=None
        self.DeleteAllItems()
        if self.tiCache is not None:
            self.tiCache.Clear()
            self.tiCache.Build(self)
        if self.doc is None:
            return
        if node is None:
            node=self.doc.getChild(None,'prjengs')
        if node is None:
            return
        self.ids=self.doc.getIds()
        iCount=self.doc.GetElementAttrCount(attr='id')
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('generate tree ...')
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
            self.gProcess.SetRange(iCount)
        #self.ids.SetNode(vtXmlDomTree.__getDocument__(node).documentElement)
        tagName=self.doc.getTagName(node)
        r=self.AddRoot(tagName)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        self.SetItemImage(r,img,wxTreeItemIcon_Normal)
        self.SetItemImage(r,imgSel,wxTreeItemIcon_Expanded)
        self.tiRoot=r
        self.Expand(self.tiRoot)
        self.__addElements__(r,node)
        #self.ids.ClearMissing()
        if self.gProcess is not None:
            self.gProcess.SetValue(iCount)
        bIdChanged=False
        #bIdChanged=self.ids.ProcessMissing()
        #forget this
        #self.__checkElements__(node)
        self.Expand(r)
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('generate tree finished.')
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        
        return bIdChanged
    def __createMenuIds__(self):
        if not hasattr(self,'popupIdSort'):
            self.__createMenuEditIds__()
            self.popupIdAddElement=wx.NewId()
            self.popupIdAddVsas=wx.NewId()
            self.popupIdView=wx.NewId()
            self.popupIdFilter=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdInstance=wx.NewId()
            self.popupIdInstanceUpdate=wx.NewId()
            self.popupIdInstanceShow=wx.NewId()
            #self.popupIdTmpl=wx.NewId()
            self.popupIdCut=wx.NewId()
            self.popupIdCopy=wx.NewId()
            self.popupIdPaste=wx.NewId()
            self.popupIdDuplicate=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            self.popupIdMoveTar=wx.NewId()
            self.popupIdMoveClr=wx.NewId()
            self.popupIdFind=wx.NewId()
            self.popupIdSort=wx.NewId()
            self.popupIdAlign=wx.NewId()
            self.popupIdConfig=wx.NewId()
            self.popupIdMessages=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddElement,self.OnTreeAddElement)
            wx.EVT_MENU(self,self.popupIdAddVsas,self.OnTreeAddVsas)
            wx.EVT_MENU(self,self.popupIdView,self.OnTreeView)
            wx.EVT_MENU(self,self.popupIdFilter,self.OnTreeFilter)
            wx.EVT_MENU(self,self.popupIdInstance,self.OnTreeInstance)
            wx.EVT_MENU(self,self.popupIdInstanceUpdate,self.OnTreeInstanceUpdate)
            wx.EVT_MENU(self,self.popupIdInstanceShow,self.OnTreeInstanceShow)
            wx.EVT_MENU(self,self.popupIdCut,self.OnTreeCut)
            wx.EVT_MENU(self,self.popupIdCopy,self.OnTreeCopy)
            wx.EVT_MENU(self,self.popupIdPaste,self.OnTreePaste)
            wx.EVT_MENU(self,self.popupIdDuplicate,self.OnTreeDuplicate)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
            wx.EVT_MENU(self,self.popupIdMove,self.OnMove)
            wx.EVT_MENU(self,self.popupIdMoveTar,self.OnMoveTarget)
            wx.EVT_MENU(self,self.popupIdMoveClr,self.OnMoveClr)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
            wx.EVT_MENU(self,self.popupIdFind,self.OnTreeFind)
            wx.EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
            wx.EVT_MENU(self,self.popupIdAlign,self.OnTreeAlign)
            wx.EVT_MENU(self,self.popupIdConfig,self.OnTreeConfig)
            wx.EVT_MENU(self,self.popupIdMessages,self.OnTreeMessages)
            if self.bLangMenu:
                if len(self.languages)>0:
                    self.popupIdLang=wx.NewId()
                    self.popupLangIds={}
                    for lang in self.languages:
                        self.popupLangIds[lang]=wx.NewId()
                        EVT_MENU(self,self.popupLangIds[lang],self.OnTreeLang)
            if self.bGrpMenu:
                self.popupGrpIds={}
                for tup in self.grpMenu:
                    sGrpId,sGrpName=tup[0],tup[1]
                    id=wx.NewId()
                    self.popupGrpIds[sGrpId]=id
                    EVT_MENU(self,self.popupGrpIds[sGrpId],self.OnTreeGroup)
    
    def OnTcNodeRightDown(self, event):
        if self.bMaster==False:
            return
        self.x=event.GetX()
        self.y=event.GetY()
        self.__createMenuIds__()
        menu=wx.Menu()
        bMenu=False
        bMenu|=self.__addMenuLang__(menu)
        bMenu|=self.__addMenuGroup__(menu)
        bInst=False
        bInstTmpl=False
        bFilter=False
        try:
            if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))>0:
                bInst=True
        except:
            pass
        try:
            if self.doc.getChild(self.objTreeItemSel,'__node'):
                bInstTmpl=True
        except:
            pass
        try:
            if self.doc.getTagName(self.objTreeItemSel)=='filter':
                bFilter=True
        except:
            pass
        menu=wx.Menu()
        if bFilter==True:
            menu.Append(self.popupIdFilter,'Filter')
        if bFilter==False:
            if bInst==False:
                menu.Append(self.popupIdAddElement,'Add Element')
                menu.Append(self.popupIdAddVsas,'Add VSAS')
                #menu.Append(self.popupIdEdit,'Edit')
            menu.Append(self.popupIdView,'View')
            menu.AppendSeparator()
            if bInst==False:
                menu.Append(self.popupIdInstance,'Instance')
                menu.Append(self.popupIdInstanceUpdate,'  ... Update')
            if bInst or bInstTmpl:
                menu.Append(self.popupIdInstanceShow,'Instance Show')
        menu.AppendSeparator()
        menu.Append(self.popupIdCut,'Cut')
        menu.Append(self.popupIdCopy,'Copy')
        menu.Append(self.popupIdPaste,'Paste')
        menu.Append(self.popupIdDuplicate,'Duplicate')
        menu.AppendSeparator()
        if bInst==False:
            menu.Append(self.popupIdDel,'Delete')
        else:
            parNode=self.doc.getParent(self.objTreeItemSel)
            if len(self.doc.getAttribute(parNode,'iid'))<=0:
                menu.Append(self.popupIdDel,'Delete')
        menu.AppendSeparator()
        menu.Append(self.popupIdMove,'Move Node')
        if self.nodeMoveTarget is None:
            menu.Append(self.popupIdMoveTar,'Move Target')
        else:
            s=self.doc.getNodeText(self.nodeMoveTarget,'tag')
            menu.Append(self.popupIdMoveTar,'Move %s'%s)            
        menu.Append(self.popupIdMoveClr,' ...  Clear')
        #bMenu|=self.__addMenuEdit__(menu)
        bMenu|=self.__addMenuMisc__(menu)
        
        if bMenu:
            self.PopupMenu(menu,(self.x, self.y))
        menu.Destroy()
        event.Skip()
        return
        if not hasattr(self,'popupIdAddElement'):
            self.popupIdAddElement=wx.NewId()
            self.popupIdAddVsas=wx.NewId()
            self.popupIdView=wx.NewId()
            self.popupIdFilter=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdInstance=wx.NewId()
            self.popupIdInstanceUpdate=wx.NewId()
            self.popupIdInstanceShow=wx.NewId()
            #self.popupIdTmpl=wx.NewId()
            self.popupIdCut=wx.NewId()
            self.popupIdCopy=wx.NewId()
            self.popupIdPaste=wx.NewId()
            self.popupIdDuplicate=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            self.popupIdMoveTar=wx.NewId()
            self.popupIdMoveClr=wx.NewId()
            self.popupIdSort=wx.NewId()
            self.popupIdAlign=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddElement,self.OnTreeAddElement)
            wx.EVT_MENU(self,self.popupIdAddVsas,self.OnTreeAddVsas)
            wx.EVT_MENU(self,self.popupIdView,self.OnTreeView)
            wx.EVT_MENU(self,self.popupIdFilter,self.OnTreeFilter)
            wx.EVT_MENU(self,self.popupIdInstance,self.OnTreeInstance)
            wx.EVT_MENU(self,self.popupIdInstanceUpdate,self.OnTreeInstanceUpdate)
            wx.EVT_MENU(self,self.popupIdInstanceShow,self.OnTreeInstanceShow)
            wx.EVT_MENU(self,self.popupIdCut,self.OnTreeCut)
            wx.EVT_MENU(self,self.popupIdCopy,self.OnTreeCopy)
            wx.EVT_MENU(self,self.popupIdPaste,self.OnTreePaste)
            wx.EVT_MENU(self,self.popupIdDuplicate,self.OnTreeDuplicate)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelGrp)
            wx.EVT_MENU(self,self.popupIdMove,self.OnMove)
            wx.EVT_MENU(self,self.popupIdMoveTar,self.OnMoveTarget)
            wx.EVT_MENU(self,self.popupIdMoveClr,self.OnMoveClr)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
            wx.EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
            wx.EVT_MENU(self,self.popupIdAlign,self.OnTreeAlign)
        bInst=False
        bInstTmpl=False
        bFilter=False
        try:
            if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))>0:
                bInst=True
        except:
            pass
        try:
            if self.doc.getChild(self.objTreeItemSel,'__node'):
                bInstTmpl=True
        except:
            pass
        try:
            if self.doc.getTagName(self.objTreeItemSel)=='filter':
                bFilter=True
        except:
            pass
        menu=wx.Menu()
        if bFilter==True:
            menu.Append(self.popupIdFilter,'Filter')
        if bFilter==False:
            if bInst==False:
                menu.Append(self.popupIdAddElement,'Add Element')
                menu.Append(self.popupIdAddVsas,'Add VSAS')
                #menu.Append(self.popupIdEdit,'Edit')
            menu.Append(self.popupIdView,'View')
            menu.AppendSeparator()
            if bInst==False:
                menu.Append(self.popupIdInstance,'Instance')
                menu.Append(self.popupIdInstanceUpdate,'  ... Update')
            if bInst or bInstTmpl:
                menu.Append(self.popupIdInstanceShow,'Instance Show')
        menu.AppendSeparator()
        menu.Append(self.popupIdCut,'Cut')
        menu.Append(self.popupIdCopy,'Copy')
        menu.Append(self.popupIdPaste,'Paste')
        menu.Append(self.popupIdDuplicate,'Duplicate')
        menu.AppendSeparator()
        if bInst==False:
            menu.Append(self.popupIdDel,'Delete')
        else:
            parNode=self.doc.getParent(self.objTreeItemSel)
            if len(self.doc.getAttribute(parNode,'iid'))<=0:
                menu.Append(self.popupIdDel,'Delete')
        menu.AppendSeparator()
        menu.Append(self.popupIdMove,'Move Node')
        if self.nodeMoveTarget is None:
            menu.Append(self.popupIdMoveTar,'Move Target')
        else:
            s=self.doc.getNodeText(self.nodeMoveTarget,'tag')
            menu.Append(self.popupIdMoveTar,'Move %s'%s)            
        menu.Append(self.popupIdMoveClr,' ...  Clear')
        menu.AppendSeparator()
        menu.Append(self.popupIdSort,'Sort')
        mnIt=wx.MenuItem(menuTools,self.popupIdAlign,_(u'Align XML'))
        wx.EVT_MENU(obj,self.popupIdAlign,self.OnTreeAlign)
        
        self.PopupMenu(menu,wx.Point(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def OnTreeAddElement(self,event):
        if self.rootNode is None:
            return -1
        if self.objTreeItemSel is None:
            node=self.rootNode
        else:
            node=self.objTreeItemSel
        self.AddElement(node)
        
    def AddElement(self,node):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'add element',origin=self.GetName())
        if self.dlgElem is None:
            self.dlgElem=vPrjEngElementsDialog(self)
            self.dlgElem.SetDoc(self.doc)
        self.dlgElem.Centre()
        self.dlgElem.SetNode(node)
        ret=self.dlgElem.ShowModal()
        if ret==1:
            child=self.dlgElem.GetXml(None)
            if child is None:
                return
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    '%s added'%(self.doc.getTagName(child)),origin=self.GetName())
            self.doc.appendChild(node,child)
            self.doc.addNode(node,child)
            self.doc.AlignNode(child,iRec=2)
            par=self.triSelected
            parID=self.doc.getAttribute(self.objTreeItemSel,'id')
            self.bAddElement=True
            self.doc.onInstance(node,child)

    def SetDlgVsas(self,dlg):
        self.dlgVsas=dlg
    def OnTreeAddVsas(self,event):
        if self.rootNode is None:
            return -1
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if self.objTreeItemSel is None:
            node=self.rootNode
        else:
            node=self.objTreeItemSel
        if self.doc.getTagName(node)!='HW':
            return -2
        if self.dlgVsas is None:
            return -3
        self.dlgVsas.Centre()
        ret=self.dlgVsas.ShowModal()
        if ret==1:
            child=self.dlgVsas.GetNode(self.doc)
            if child is not None:
                self.doc.appendChild(node,child)
                #self.doc.checkId(child)
                #bIdChanged=self.doc.processMissingId()
                #self.doc.clearMissing()
                self.doc.AlignNode(child)
                self.doc.addNode(node,child)
                ti=self.__addElement__(self.triSelected,child,True)
                if self.tiCache is not None:
                    self.tiCache.Add(ti)
                # process instanced nodes 
                childs=self.doc.getChilds(node,'__node')
                for c in childs:
                    instNode=self.doc.getIdStr(self.doc.getAttribute(c,'iid'))[1]
                    clonechild=self.__doClone__(child)
                    self.__doInstanceing__(instNode,clonechild,ensureVis=False)
                    self.doStoreClone(child,clonechild)
                self.doc.addNodeCorrectId(node,child)
                #self.SelectItem(ti)
                #parID=self.doc.getAttribute(self.objTreeItemSel,'id')
                #self.triSelected=ti
                #self.objTreeItemSel=child
                #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
                pass
    def OnAddFinished(self,evt):
        #print
        #print
        #print
        #print
        #vtLog.vtLogCallDepth(None,'',1,callstack=True)
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
            'parent:%s master:%d net response:%d added:%d'%(self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse,self.bAddElement),
            origin=self.GetName())
        #vtLog.CallStack('added:%d'%self.bAddElement)
        if self.bAddElement:
            idPar=self.doc.getKey(self.rootNode)
            if self.groupItems.has_key(idPar):
                self.__sortAllGrpItems__(self.groupItems[idPar])
                #self.__sortAllGrpItems__(self.groupItems)
            self.SortChildren(self.tiRoot)
            
            try:
                ti=evt.GetTreeItem()
                self.objTreeItemSel=self.GetPyData(ti)
                parID=self.doc.getAttribute(self.doc.getParent(self.objTreeItemSel),'id')
                self.triSelected=ti
                self.SelectItem(ti)
            except:
                pass
            #wx.PostEvent(self,vtXmlTreeItemAdded(self,evt.GetTreeItem(),self.objTreeItemSel,parID))
            self.bAddElement=False
            #self.Expand(self.tiRoot)
        self.thdAddElements.StartScheduled()
        
        evt.Skip()
    def OnTreeView(self,event):
        if self.dlgViewElementsTable is not None:
            self.dlgViewElementsTable.Centre()
            lst=self.GetIDNodes()
            self.dlgViewElementsTable.SetNode(self.objTreeItemSel,
                        lst,self.doc)
            ret=self.dlgViewElementsTable.ShowModal()
            if ret==1:
                self.RefreshSelected()
                wx.PostEvent(self,vtXmlTreeItemEdited(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
    def OnTreeFilter(self,event):
        if self.rootNode is None:
            return -1
        if self.objTreeItemSel is None:
            return -1
        node=self.objTreeItemSel
        if self.doc.getTagName(node)!=u'filter':
            return -1
        if self.dlgFilter is None:
            self.dlgFilter=vFilterDialog(self)
            self.dlgFilter.SetDoc(self.doc,True)
        self.dlgFilter.SetNode(node)
        self.dlgFilter.Centre()
        ret=self.dlgFilter.ShowModal()
        if ret==1:
            wx.PostEvent(self,vtXmlTreeItemEdited(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
            pass
    def __getRootNode__(self,node):
        par=self.doc.getParent(node)
        if par is None:
            return None
        if Elements.isProperty(self.doc.getTagName(par),'isRoot'):
            return par
        else:
            return self.__getRootNode__(par)
        return None
    
    def OnTreeInstance(self,evt):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if self.objTreeItemSel is None:
            return
        if Elements.isProperty(self.doc.getTagName(self.objTreeItemSel),
                        'isRoot'):
            return
        else:
            root=self.__getRootNode__(self.objTreeItemSel)
            if root is not None:
                if self.doc.getTagName(root) in ['class','templates']:
                    if self.dlgInstance is not None:
                        self.dlgInstance.Centre()
                        self.dlgInstance.SetNode(self.rootNode,self.objTreeItemSel)
                        ret=self.dlgInstance.ShowModal()
                        if ret==1:
                            par=self.dlgInstance.GetSelNode()
                            child=self.doc.doClone(self.objTreeItemSel)
                            oldChild=self.objTreeItemSel
                            self.doc.doInstanceing(par,child,True)
                            self.doc.doStoreClone(oldChild,child)
                            if self.doc.updateAllStore(oldChild):
                                self.doc.setNodeFull(oldChild)
                            self.doc.addNodeCorrectId(par,child)
        
    def OnTreeInstanceUpdate(self,evt):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if self.objTreeItemSel is None:
            return
        parNode=self.doc.getParent(self.objTreeItemSel)
        tmpNode=self.doc.getChild(parNode,'__node')
        if tmpNode is None:
            bUpdate=False
        else:
            bUpdate=True
        self.doc.procInstanced(self.objTreeItemSel,update=bUpdate)
        #self.doc.thdProcInst.ProcInstanced(self.doc,
        #        None,self.objTreeItemSel,False,False,True)
        #self.__instancedUpdate__(self.objTreeItemSel,True)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event

    def __equalizeInstanced__(self,node,instNode):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if instNode is None:
            return
        if node is None:
            return
        childs=self.doc.getChilds(node)
        instChilds=self.doc.getChilds(instNode)
        def __getAttrDict__(childs):
            d={}
            for c in childs:
                if len(self.doc.getAttribute(c,'id'))>0:
                    continue
                tagName=self.doc.getTagName(c)
                if tagName[0:2]=='__':
                    continue
                d[tagName]=c
            return d
        dTmpl=__getAttrDict__(childs)
        dInst=__getAttrDict__(instChilds)
        for k in dTmpl.keys():
            try:
                aInst=dInst[k]
                aNode=dTmpl[k]
                valNode=self.doc.getChild(aNode,'val')
                if valNode is None:
                    self.doc.setText(aInst,self.doc.getText(aNode))
                else:
                    self.doc.setText(aInst,self.doc.getText(valNode))
            except:
                pass
        ti=self.__findNode__(self.tiRoot,instNode)
        self.__refreshLabel__(ti)
        tiPar=self.GetItemParent(ti)
        if self.IsExpanded(tiPar):
            self.SortChildren(tiPar)
    def __instancedUpdate__(self,node,bStart=False):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        childs=self.doc.getChilds(node,'__node')
        if bStart==True:
            if self.frmMain is not None:
                self.frmMain.SetStatusInfo('update instanced ...')
            #if self.gProcess is not None:
            #    self.gProcess.SetValue(0)
            #    self.gProcess.SetRange(len(childs))
        iAct=0
        for c in childs:
            iAct+=1
            #if bStart==True:
            #    if self.gProcess is not None:
            #        self.gProcess.SetValue(iAct)
            iid=self.doc.getAttribute(c,'iid')
            if len(iid)<=0:
                continue
            instNode=self.ids.GetIdStr(iid)[1]
            self.__equalizeInstanced__(node,instNode)
        for c in self.doc.getChildsAttr(node,'id'):
            self.__instancedUpdate__(c)
        if bStart==True:
            if self.frmMain is not None:
                self.frmMain.SetStatusInfo('update instanced finished.')
            #if self.gProcess is not None:
            #    self.gProcess.SetValue(0)
        
    def __getVal__(self,node,attr):
        if node is None:
            return None
        if attr in ['tag','name','description']:
            sVal=self.doc.getNodeText(node,attr)
        else:
            c=self.doc.getChild(attr)
            if len(self.doc.getAttribute(c,'ih'))>0:
                sVal=self.doc.getText(c)
            else:
                sVal=self.doc.getNodeText(c,'val')
        return sVal
    def __isMatch__(self,search,node):
        if node is None:
            return False
        bResult=False
        #vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%self.doc.getKey(node),self)
        for it in search:
            bRes=False
            for k in it[0].keys():
                sVal=self.__getVal__(node,k)
                #vtLog.vtLngCurWX(vtLog.DEBUG,'key:%s;val:%s;cmp:%s;res:%d'%(k,sVal,it[0][k],fnmatch.fnmatch(sVal,it[0][k])),self)
                if fnmatch.fnmatch(sVal,it[0][k]):
                    bRes=True
                elif it[1]=='and':
                    bRes=False
                    break
            if it[2]=='and':
                if bRes==False:
                    return False
                else:
                    bResult=True
            if it[2]=='nand':
                if bRes==True:
                    return False
                else:
                    bResult=True
            elif it[2]=='or':
                bResult|=bRes
                if bResult==True:
                    return True
            elif it[2]=='nor':
                bResult|=not(bRes)
                if bResult==True:
                    return True
        return False  # correct??
    def __getMatchedNodes__(self,search,node,lst,tmpIds=None):
        if tmpIds is None:
            if self.__isMatch__(search,node):
                lst.append(node)
            for c in self.doc.getChilds(node):
                if len(self.doc.getAttribute(c,'id'))>0:
                    self.__getMatchedNodes__(search,c,lst)
        else:
            keys=tmpIds.GetIDs()
            for k in keys:
                n=tmpIds.GetId(k)[1]
                if self.__isMatch__(search,n):
                    lst.append(n)
    def SearchNodes(self,search=[],node=None,tmpIds=None):
        """search is a list build like:
        [ ( {'tag':'FI*'} , 'and' , 'or' ) ]
            first element of tuple is a dictionary of infos to check with match
            second element is operator to combine first elements results
            third element is operator to combine results of list tuples
        """
        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        #vtLog.vtLngCurWX(vtLog.DEBUG,search,self)
        
        if node is None:
            node=self.rootNode
        if node is None:
            return []
        lst=[]
        self.__getMatchedNodes__(search,node,lst,tmpIds)
        return lst
    def InstanceNodes(self,nodes):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                    origin=self.GetName())
            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    'nodes:%s'%(nodes),
                    origin=self.GetName())
        tmpIds=vtXmlDom.NodeIds(self.doc)
        par=self.objTreeItemSel
        tmpIds.SetNode(par)
        lstError=[]
        bError=False
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('instance nodes ...')
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
            self.gProcess.SetRange(len(nodes))
        iAct=0
        for d in nodes:
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
            #        'dict:%s'%(d),
            #        origin=self.GetName())
            iAct+=1
            if self.gProcess is not None:
                self.gProcess.SetValue(iAct)
            node=d['node']
            child=None
            bDoInst=False
            if d['attr'].has_key('tag'):
                sTag=d['attr']['tag']
                lstNodes=self.SearchNodes([({'tag':sTag},'or','or')],par,tmpIds)
                if len(lstNodes)>0:
                    child=lstNodes[0]
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s found'%sTag,self)
                else:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s not found'%sTag,self)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
            #            'child node,%s'%(child),
            #            origin=self.GetName())
            if child is None:
                child=self.doc.doClone(node)
                if child is None:
                    bError=True
                    lstError.append(d)
                    continue
                bDoInst=True
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                #            'cloned node,%s'%(child),
                #            origin=self.GetName())
            def __procAttr__(node,d):
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                #        '__procAttr__ dict:%s,node:%s'%(d,node),
                #        origin=self.GetName())
                for k in d.keys():
                    #self.doc.setNodeText(node,k,d[k])
                    self.doc.setNodeAttributeVal(node,k,d[k])
            def __getNode__(node,iid,b2Inst):
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                #        '__getNode__ node:%s,iid:%s,inst:%d'%(node,iid,b2Inst),
                #        origin=self.GetName())
                for c in self.doc.getChilds(node):
                    if b2Inst:
                        id=self.doc.getAttribute(c,'id')
                    else:
                        id=self.doc.getAttribute(c,'iid')
                    if self.doc.IsKeyValid(id):
                        #if c.getAttribute('iid')==iid:
                        if id==iid:
                            return c
                        ret=__getNode__(c,iid,b2Inst)
                        if ret is not None:
                            return ret
                return None
            __procAttr__(child,d['attr'])
            for it in d['nodes']:
                c=__getNode__(child,self.doc.getAttribute(it[0],'id'),bDoInst)
                __procAttr__(c,it[1])
            if bDoInst:
                try:
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                    #        'doInst ,par:%s,node:%s,child:%s'%(par,node,child),
                    #        origin=self.GetName())
                    
                    self.doc.doInstanceing(par,child,True)
                    self.doc.doStoreClone(node,child)
                    if self.doc.updateAllStore(node):
                        self.doc.setNodeFull(node)
                    self.doc.addNodeCorrectId(par,child)
                    self.__doInstanceing__(par,child,ensureVis=True)
                            
                            #self.__doInstanceing__(par,child,ensureVis=False)
                    #self.doc.doStoreClone(node,child)
                    #self.doc.addNodeCorrectId(node,child)
                except:
                    bError=True
                    lstError.append(d)
            else:
                ti=self.__findNode__(self.tiRoot,child)
                self.__refreshLabel__(ti)
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('instance nodes finshed.')
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        del tmpIds
        wx.PostEvent(self,vtXmlTreeItemInstance(self,None,None,''))
        return (bError,lstError)
    def __getInstanceBase__(self,node):
        if node is None:
            return node
        parNode=self.doc.getParent(node)
        if len(self.doc.getAttribute(parNode,'iid'))>0:
            return self.__getInstanceBase__(parNode)
        return node
    def __getInstanceTmplBase__(self,node):
        if node is None:
            return node
        parNode=self.doc.getParent(node)
        if self.doc.getChild(parNode,'__node') is not None:
            return self.__getInstanceTmplBase__(parNode)
        return node
    def OnTreeInstanceShow(self,evt):
        if self.objTreeItemSel is None:
            return
        bFound=False
        instBase=self.__getInstanceBase__(self.objTreeItemSel)
        if not(self.doc.isSame(instBase,self.objTreeItemSel)):
            self.SelectByID(self.doc.getAttribute(instBase,'id'))
        if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))>0:
            try:
                node=self.doc.GetIdStr(self.doc.getAttribute(self.objTreeItemSel,'iid'))[1]
                bFound=True
            except:
                pass
        else:
            childs=self.doc.getChilds(self.objTreeItemSel,'__node')
            if len(childs)>0:
                bFound=True
                #node=self.objTreeItemSel
                node=self.__getInstanceTmplBase__(self.objTreeItemSel)
                if node!=self.objTreeItemSel:
                    self.SelectByID(self.doc.getAttribute(node,'id'))
        
        if bFound:
            if self.dlgInstShow is None:
                self.dlgInstShow=vInstanceShowDialog(self)
                self.dlgInstShow.SetDoc(self.doc,True)
            self.dlgInstShow.Centre()
            self.dlgInstShow.SetNode(node,self)
            ret=self.dlgInstShow.ShowModal()
            if ret==1:
                self.RefreshSelected()
                wx.PostEvent(self,vtXmlTreeItemEdited(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
                wx.PostEvent(self,vtXmlTreeItemSelected(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
    def OnTreeCut(self,evt):
        self.nodeCut=self.objTreeItemSel
        self.triCut=self.triSelected
    def OnTreeCopy(self,evt):
        self.nodeCopy=self.objTreeItemSel
        self.nodeCut=None
        self.triCut=None
    def __postAdded__(self,child,parID):
        ti=self.__findNode__(self.tiRoot,child)
        self.EnsureVisible(ti)
        self.SelectItem(ti)
        wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
    def __postMoved__(self,child,parID,tiSel,ensureVisible):
        ti=self.__findNode__(self.tiRoot,child)
        if ensureVisible:
            self.EnsureVisible(ti)
            self.SelectItem(ti)
        #else:
        #    self.triSelected=tiSel
        #    self.objTreeItemSel=self.GetPyData(self.triSelected)
        ti,node=self.__findNodeByID__(None,parID)
        self.SortChildren(ti)
        wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
                
    def __postInstanced__(self,child,parID):
        ti=self.__findNode__(self.tiRoot,child)
        self.objTreeItemSel=child
        self.triSelected=ti
        self.EnsureVisible(ti)
        self.SelectItem(ti)
        wx.PostEvent(self,vtXmlTreeItemInstance(self,
                            self.triSelected,self.objTreeItemSel,parID))
    def OnTreePaste(self,evt):
        root=self.__getRootNode__(self.objTreeItemSel)
        if self.nodeCut is not None:
            rootCut=self.__getRootNode__(self.nodeCut)
            if not(self.doc.isSame(root,rootCut)):
                if len(self.doc.getAttribute(self.nodeCut,'iid'))>0:
                    return
            if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))<=0:
                #parNode=self.doc.getParent(self.nodeCut)
                #parNode.removeChild(self.nodeCut)
                self.doc.moveNode(self.objTreeItemSel,self.nodeCut)
                self.Delete(self.triCut)
                if self.tiCache is not None:
                    self.tiCache.DelID(self.doc.getKey(self.nodeCut))

                self.doc.AlignNode(self.nodeCut)
                ti=self.GetItemParent(self.triSelected)
                parID=self.doc.getAttribute(self.objTreeItemSel,'id')
                #self.__addElementAndChilds__(self.triSelected,
                #                self.nodeCut,self.skip,False)
                #self.SortChildren(ti)
                self.bAddElement=True
                self.__addElements__(self.triSelected,self.nodeCut,
                        self.__postInstanced__,self.nodeCut,parID)
                
                
        elif self.nodeCopy is not None:
            rootCopy=self.__getRootNode__(self.nodeCopy)
            if not(self.doc.isSame(root,rootCopy)):
                if len(self.doc.getAttribute(self.nodeCopy,'iid'))>0:
                    return
            if len(self.doc.getAttribute(self.nodeCopy,'iid'))<=0:
                child=self.doc.cloneNode(self.nodeCopy,True)
                self.doc.doInstanceing(self.objTreeItemSel,child,False)
                self.doc.removeAllStoreClone(child)
                #self.__procInstanced__(child)
                #self.doc.procInstanced(self.doc.getParent(self.objTreeItemSel))
                self.doc.addNodeCorrectId(self.objTreeItemSel,child)
                parID=self.doc.getKey(self.objTreeItemSel)
                
                # add 2 tree
                #par=self.triSelected  # seems to be stupid
                #self.bAddElement=True
                #self.__addElements__(par,child,
                #            self.__postAdded__,child,parID)
                self.doc.onInstance(self.objTreeItemSel,child)
                
                #self.SortChildren(par)
                #self.doc.addNode(parNode,child)
                
                #ti=self.__findNode__(self.tiRoot,child)
                #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
            else:
                if len(self.doc.getAttribute(self.objTreeItemSel,'iid'))<=0:
                    child=self.doc.cloneNode(self.nodeCopy,True)
                    self.doc.doInstanceing(self.objTreeItemSel,child,False)
                    #child=self.__doClone__(self.nodeCopy)
                    oldChild=self.nodeCopy
                    #self.__doInstanceing__(self.objTreeItemSel,child)
                    iid=self.doc.getAttribute(self.nodeCopy,'iid')
                    if len(iid)>0:
                        oldChild=self.doc.GetIdStr(iid)[1]
                        self.doc.doStoreClone(oldChild,child)
                        if self.doc.updateAllStore(oldChild):
                            self.doc.setNodeFull(node)
                    #self.__procInstanced__(child)
                    #self.doc.procInstanced(self.doc.getParent(self.objTreeItemSel))
                    self.doc.addNodeCorrectId(self.objTreeItemSel,child)
                    parID=self.doc.getKey(self.objTreeItemSel)
                
                    # add 2 tree
                    #par=self.triSelected  # seems to be stupid
                    #self.bAddElement=True
                    #self.__addElements__(par,child,
                    #            self.__postAdded__,child,parID)
                    self.doc.onInstance(self.objTreeItemSel,child)
                    
                    #self.SortChildren(par)
                    #self.doc.addNode(parNode,child)
                    #ti=self.__findNode__(self.tiRoot,child)
                    #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
        self.nodeCopy=None
        self.nodeCut=None
        self.triCut=None
    def OnTreeDuplicate(self,evt):
        child=self.doc.cloneNode(self.objTreeItemSel,True)
            
        par=self.GetItemParent(self.triSelected)
        parNode=self.GetPyData(par)
        if parNode is not None:
            if len(self.doc.getAttribute(parNode,'iid'))<=0:
                oldChild=self.objTreeItemSel
                self.doc.doInstanceing(parNode,child,False)
                for c in self.doc.getChilds(child):
                    tagName=self.doc.getTagName(c)
                    if tagName[:2]=='__':
                        self.doc.deleteNode(c)
                    if Elements.isProperty(tagName,'isSkip'):
                    #if tagName in ['Safe','Log']:
                        self.doc.deleteNode(c)
                iid=self.doc.getAttribute(oldChild,'iid')
                if len(iid)>0:
                    oldChild=self.doc.GetIdStr(iid)[1]
                    self.doc.doStoreClone(oldChild,child)
                    if self.doc.updateAllStore(oldChild):
                        self.doc.setNodeFull(oldChild)
                #self.doc.procInstanced(parNode)
                self.nodeDupTmpl=child
                parID=self.doc.getAttribute(oldChild,'id')
                self.doc.addNodeCorrectId(parNode,child)
                
                par=self.GetItemParent(self.triSelected)  # seems to be stupid
                #self.bAddElement=True
                #self.__addElements__(par,child,
                #            self.__postAdded__,child,parID)
                self.doc.onInstance(parNode,child)
                
                #self.SortChildren(par)
                #self.doc.addNode(parNode,child)
                
                #ti=self.__findNode__(self.tiRoot,child)
                #wx.PostEvent(self,vtXmlTreeItemAdded(self,ti,child,parID))
            else:
                self.doc.deleteNode(child)
        else:
            self.doc.deleteNode(child)
        pass
    def OnDragBegin(self,evt):
        self.nodeCut=None
        self.nodeCopy=None
        self.dragStart=evt.GetItem()
        self.nodeCut=self.GetPyData(self.dragStart)
        if len(self.doc.getAttribute(self.doc.getParent(self.nodeCut),'iid'))<=0:
            evt.Allow()
        evt.Skip()
    def OnDragEnd(self,evt):
        ti=evt.GetItem()
        if ti is None:
            return
        node=self.GetPyData(ti)
        if node is None:
            return
        if ti==self.dragStart:
            return
        if self.__doNodeMove__(node,evt.GetItem()):
            evt.Allow()
        
    def OnMove(self,evt):
        if self.nodeMoveTarget is None:
            return
        par=self.doc.getParent(self.nodeMoveTarget)
        while par is not None:
            if self.doc.isSame(self.objTreeItemSel,par):
                return
            par=self.doc.getParent(par)
        self.dragStart=self.triSelected
        self.nodeCut=self.objTreeItemSel
        if self.tiMoveTarget==self.dragStart:
            return
        self.__doNodeMove__(self.nodeMoveTarget,self.tiMoveTarget,False)
    def OnMoveTarget(self,evt):
        self.nodeMoveTarget=self.objTreeItemSel
        self.tiMoveTarget=self.triSelected
    def OnMoveClr(self,evt):
        self.nodeMoveTarget=None
        self.tiMoveTarget=None
    def __doNodeMove__(self,node,tiTarget,ensureVis=True):
        if node is None:
            return
        if self.nodeCut is None:
            return
        root=self.__getRootNode__(node)
        if self.nodeCut is not None:
            rootCut=self.__getRootNode__(self.nodeCut)
            if not(self.doc.isSame(root,rootCut)):
                if len(self.doc.getAttribute(self.nodeCut,'iid'))>0:
                    return False
            #self.iBlockTreeSel=1
            if len(self.doc.getAttribute(self.doc.getParent(self.nodeCut),'iid'))>0:
                # no moving from instanced node
                return False
            if len(self.doc.getAttribute(node,'iid'))>0:
                # no moving to instanced node
                return False
            tmplBaseCut=self.doc.getTmplBase(self.nodeCut)
            if tmplBaseCut is not None:
                tmplBaseTar=self.doc.getTmplBase(node)
                if tmplBaseTar is None:
                    return False
                if not(self.doc.isSame(tmplBaseCut,tmplBaseTar)):
                    return False
            parNode=self.doc.getParent(self.nodeCut)
            self.doc.moveNode(node,self.nodeCut)
            if ensureVis:
                self.idManualMove=self.doc.getKey(self.nodeCut)
            else:
                self.idManualMove=''
            if tmplBaseCut is not None:
                self.doc.procInstanced(tmplBaseCut)
                
            if 1==1:
                    delID=self.doc.getAttribute(self.GetPyData(self.dragStart),'id')
                    wx.PostEvent(self,vtXmlTreeItemDeleted(self,
                                    self.dragStart,None,delID))
                    self.Delete(self.dragStart)
                    if self.tiCache is not None:
                        self.tiCache.DelID(delID)
    
                    #self.doc.appendChild(node,self.nodeCut)
                    
                    self.doc.AlignNode(self.nodeCut)
                    parID=self.doc.getAttribute(self.GetPyData(tiTarget),'id')
                    #if ensureVis:
                    #    self.bAddElement=True
                    #else:
                    #    self.bAddElement=False
                    tiSel=self.GetSelection()
                    self.__addElements__(tiTarget,self.nodeCut,
                                self.__postMoved__,
                                self.nodeCut,parID,tiSel,ensureVis)
                #ti=self.__findNode__(self.tiRoot,self.nodeCut)
                #self.objTreeItemSel=self.nodeCut
                #self.triSelected=ti
                #if ensureVis:
                    #self.EnsureVisible(ti)
                    #self.SelectItem(ti)
                #    pass
                #else:
                #    self.triSelected=self.GetSelection()
                #    self.objTreeItemSel=self.GetPyData(self.triSelected)
                #wx.PostEvent(self,vtXmlTreeItemAdded(self,
                #                self.triSelected,self.objTreeItemSel,parID))
                #self.iBlockTreeSel=0
            return True
        return False
    def OnTreeDelGrp(self,event):
        if self.rootNode is None:
            return -1
        if self.objTreeItemSel is None:
            return
        id=self.doc.getAttribute(self.objTreeItemSel,'id')
        iid=self.doc.getAttribute(self.objTreeItemSel,'iid')
        if len(iid)>0:
            nodeRet=self.doc.removeStoreClone(self.objTreeItemSel)
            if nodeRet is not None:
                self.doc.setNodeFull(nodeRet)
        self.doc.delNode(self.objTreeItemSel)
        
    def __refreshLabel__(self,ti):
        #vtLog.vtLogCallDepth(None,'',verbose=1,callstack=False)
        vtXmlGrpTree.__refreshLabel__(self,ti)
        #vtLog.vtLogCallDepth(None,'finshed',verbose=1,callstack=False)
        
        if self.bMaster:
            return
            try:
                #if VERBOSE:
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
                o=self.GetPyData(ti)
                iid=self.doc.getAttribute(o,'iid')
                if iid!='':
                    nodeRet=self.doc.getNodeById(iid)
                    if nodeRet is not None:
                        if self.doc.updateAllStore(nodeRet):
                            self.doc.setNodeFull(nodeRet)
                    #nodeRet=self.doc.updateStoreClone(o)
                    #if VERBOSE:
                    #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'update store %d'%(nodeRet is not None))
                    #if nodeRet is not None:
                    #    self.doc.setNodeFull(nodeRet)
                    #self.doc.setNodeFull(nodeRet)
            except:
                pass
    def SelectByID(self,findID):
        tup=self.__findNodeByID__(self.tiRoot,findID)
        if tup is not None:
            self.triSelected=tup[0]
            self.objTreeItemSel=tup[1]
            self.SelectItem(tup[0],True)
            self.EnsureVisible(tup[0])
    def OnProcInstanceFin(self,evt):
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced childs finshed.')
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        
    def OnProcInstanceProgress(self,evt):
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced childs...')
        if self.gProcess is not None:
            self.gProcess.SetRange(evt.GetCount())
            self.gProcess.SetValue(evt.GetValue())
            
    def __procInstancedChilds__(self,node):
        if 1==0:
            print '__procInstancedChilds__'
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced childs...')
        self.doc.procInstancedChilds(node)
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced childs finished.')
            
    def __procInstanced__(self,node):
        if 1==0:
            print '__procInstanced__'
        self.doc.procInstanced(node)
    def __procInstancedAttr__(self,node):
        if 1==0:
            print '__procInstancedAttr__'
        # process instanced nodes 
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced attributes ...')
        self.doc.procInstancedAttr(node)
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced attributes finished.')
        
    def __procInstancedElem__(self,node):
        if 1==0:
            print '__procInstancedElem__'
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced element ...')
        self.doc.procInstancedElem(node)
        if self.frmMain is not None:
            self.frmMain.SetStatusInfo('process instanced element finished.')
        #if self.gProcess is not None:
        #    self.gProcess.SetValue(0)
    def RefreshByID(self,id):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster))
        if self.IsThreadRunning():
            # FIXME
            if VERBOSE:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'thread is running')            
            return
        tup=self.__findNodeByID__(self.tiRoot,id)
        if tup is not None:
            self.__refreshLabel__(tup[0])
            self.SortChildren(self.GetItemParent(tup[0]))
    def AddByID(self,id,idChild):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster))
        if self.IsThreadRunning():
            # FIXME
            if VERBOSE:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'thread is running')            
            return
        tup=self.__findNodeByID__(self.tiRoot,id)
        if tup is not None:
            child=self.ids.GetIdStr(idChild)[1]
            self.bAddElement=True
            self.__addElements__(tup[0],child,False)
    def DeleteByID(self,id):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster))
        if self.IsThreadRunning():
            # FIXME
            if VERBOSE:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'thread is running')            
            return
        tup=self.__findNodeByID__(self.tiRoot,id)
        if tup is not None:
            self.Delete(tup[0])
            if self.tiCache is not None:
                self.tiCache.DelID(id)
            self.triSelected=self.GetSelection()
            try:
                self.objTreeItemSel=self.GetPyData(self.triSelected)
            except:
                self.objTreeItemSel=None
                self.triSelected=None
    def RefreshSelected(self):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster))
        if self.IsThreadRunning():
            # FIXME
            if VERBOSE:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'thread is running')            
            return
        if self.triSelected>=0:
            self.__refreshLabel__(self.triSelected)
            self.SortChildren(self.GetItemParent(self.triSelected))
            # FIXME:thread
            #self.doc.thdProcInst.ProcInstanced(self.doc,
            #        self.objTreeItemSel,self.nodeDupTmpl,False,True,True)
            #self.__procInstancedAttr__(self.objTreeItemSel)
            
            #if self.nodeDupTmpl is not None:
            #    self.__instancedUpdate__(self.nodeDupTmpl,True)
            self.nodeDupTmpl=None
            wx.PostEvent(self,vtXmlTreeItemEdited(self,
                    self.triSelected,self.objTreeItemSel))
    def __procInstancedDeep__(self,node):
        self.__procInstancedElem__(node)
        self.__procInstanced__(node)
        for c in self.doc.getChildsAttr(node,'id'):
            self.__procInstancedDeep__(c)
    def RefreshSelectedDeep(self):
        #pti=self.GetItemParent(self.triSelected)
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'node:%s'%self.objTreeItemSel)
        #self.DeleteChildren(self.triSelected)
        pti=self.GetItemParent(self.triSelected)
        self.__addElements__(pti,self.objTreeItemSel)
        self.Delete(self.triSelected)
        self.triSelected=None
        self.objTreeItemSel=None
        
        #self.__procInstancedDeep__(self.objTreeItemSel)
        #self.__refreshLabel__(self.triSelected)
        #self.objTreeItemSel=None
        #self.triSelected=None
        #self.SortChildren(pti)
        #wx.PostEvent(self,vtXmlTreeItemEdited(self,
        #        self.triSelected,self.objTreeItemSel))
    def SetLanguages(self,languages,languageIds):
        self.languages=languages
        self.languageIds=languageIds
        
    def __update_tree1__(self):
        # update tree
        triRoot=self.GetRootItem()
        triChild=self.GetFirstChild(triRoot)
        if self.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.GetNextChild(triChild[0],triChild[1])
            if self.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
        self.groupItems={}
        #self.DeleteChildren(triPar)
        self.DeleteChildren(triRoot)
        self.__addElements__(triRoot,self.rootNode)
        parTri=self.GetItemParent(triRoot)
        self.SortChildren(triRoot)
        #self.SelectItem(triPar)
        wx.PostEvent(self,vtXmlTreeItemAdded(self,
                            triPar,self.rootNode,''))
        return 0
    def __getIDNodes__(self,lstIDNodes,tiItem):
        tiItemTup=self.GetFirstChild(tiItem)
        while tiItemTup[0].IsOk():
            tiItem=tiItemTup[0]
            dat=self.GetPyData(tiItem)
            if dat is not None:
                id=string.strip(self.doc.getKey(dat))
                if self.doc.IsKeyValid(id):
                    lstIDNodes.append(dat)
                    self.__getIDNodes__(lstIDNodes,tiItem)
            tiItemTup=self.GetNextChild(tiItem,tiItemTup[1])
    def GetIDNodes(self,tiItem=None):
        if tiItem is None:
            tiItem=self.triSelected
        if tiItem is None:
            return []
        lstIDNodes=[]
        dat=self.GetPyData(tiItem)
        if dat is not None:
            id=string.strip(self.doc.getKey(dat))
            if self.doc.IsKeyValid(id):
                lstIDNodes.append(dat)
        
        self.__getIDNodes__(lstIDNodes,tiItem)
        return lstIDNodes
        
