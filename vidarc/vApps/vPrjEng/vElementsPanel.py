#Boa:FramePanel:vElementsPanel
#----------------------------------------------------------------------------
# Name:         vElementsPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElementsPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys,string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom

import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
from vidarc.vApps.vPrjEng.InputValue import *
from vidarc.vApps.vPrjEng.vTypeInheritanceDialog import *
import vidarc.vApps.vPrjEng.BaseTypes as BaseTypes
from vidarc.vApps.vPrjEng.vAttributeDialog import *
from vidarc.vApps.vPrjEng.Elements import *
from vidarc.vApps.vPrjEng.vBrowseDialog import *

import images

[wxID_VELEMENTSPANEL, wxID_VELEMENTSPANELGCBBBROWSE, 
 wxID_VELEMENTSPANELGCBBCHANGE, wxID_VELEMENTSPANELGCBBDEL, 
 wxID_VELEMENTSPANELLBLDESC, wxID_VELEMENTSPANELLBLNAME, 
 wxID_VELEMENTSPANELLBLPOSSIBLE, wxID_VELEMENTSPANELLBLTAGNAME, 
 wxID_VELEMENTSPANELLBLTYPE, wxID_VELEMENTSPANELLSTATTR, 
 wxID_VELEMENTSPANELLSTELEMENT, wxID_VELEMENTSPANELTXTDESC, 
 wxID_VELEMENTSPANELTXTNAME, wxID_VELEMENTSPANELTXTTAGNAME, 
 wxID_VELEMENTSPANELTXTTYPE, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vElementsPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VELEMENTSPANEL, name=u'vElementsPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(394, 424),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(386, 397))

        self.lblType = wx.StaticText(id=wxID_VELEMENTSPANELLBLTYPE,
              label=u'Type', name=u'lblType', parent=self, pos=wx.Point(29, 4),
              size=wx.Size(24, 13), style=0)

        self.txtType = wx.TextCtrl(id=wxID_VELEMENTSPANELTXTTYPE,
              name=u'txtType', parent=self, pos=wx.Point(56, 4),
              size=wx.Size(216, 21), style=0, value=u'')
        self.txtType.Enable(False)

        self.lblPossible = wx.StaticText(id=wxID_VELEMENTSPANELLBLPOSSIBLE,
              label=u'Possible', name=u'lblPossible', parent=self,
              pos=wx.Point(14, 32), size=wx.Size(39, 13), style=0)

        self.lstElement = wx.ListCtrl(id=wxID_VELEMENTSPANELLSTELEMENT,
              name=u'lstElement', parent=self, pos=wx.Point(56, 32),
              size=wx.Size(328, 104), style=wx.LC_REPORT)
        self.lstElement.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstElementListItemSelected,
              id=wxID_VELEMENTSPANELLSTELEMENT)
        self.lstElement.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstElementListItemDeselected,
              id=wxID_VELEMENTSPANELLSTELEMENT)

        self.lblTagName = wx.StaticText(id=wxID_VELEMENTSPANELLBLTAGNAME,
              label=u'Tagname', name=u'lblTagName', parent=self, pos=wx.Point(8,
              144), size=wx.Size(45, 13), style=0)

        self.txtTagname = wx.TextCtrl(id=wxID_VELEMENTSPANELTXTTAGNAME,
              name=u'txtTagname', parent=self, pos=wx.Point(56, 144),
              size=wx.Size(120, 21), style=0, value=u'')

        self.lblName = wx.StaticText(id=wxID_VELEMENTSPANELLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(25,
              172), size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VELEMENTSPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(56, 172),
              size=wx.Size(184, 21), style=0, value=u'')

        self.lblDesc = wx.StaticText(id=wxID_VELEMENTSPANELLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(0, 200), size=wx.Size(53, 13), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VELEMENTSPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(56, 200),
              size=wx.Size(320, 56), style=wx.TE_MULTILINE, value=u'')

        self.lstAttr = wx.ListCtrl(id=wxID_VELEMENTSPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 264),
              size=wx.Size(280, 126), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VELEMENTSPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected, id=wxID_VELEMENTSPANELLSTATTR)

        self.gcbbChange = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTSPANELGCBBCHANGE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Change',
              name=u'gcbbChange', parent=self, pos=wx.Point(300, 296),
              size=wx.Size(76, 30), style=0)
        self.gcbbChange.Bind(wx.EVT_BUTTON, self.OnGcbbChangeButton,
              id=wxID_VELEMENTSPANELGCBBCHANGE)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTSPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(300, 332), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VELEMENTSPANELGCBBDEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTSPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(300, 262),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGccbBrowseButton,
              id=wxID_VELEMENTSPANELGCBBBROWSE)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.selElem=None
        self.lang=None
        self.dlgAttr=None
        self.dlgType=None
        self.dlgBrowse=None
        self.attrName=''
        self.selAttrIdx=-1
        #self.lstDocs.InsertColumn(0,u'Doc')
        
        #self.inpValue=InputValue(parent=self, pos=wx.Point(152, 264),
        #      size=wx.Size(136, 21))
        
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'',wx.LIST_FORMAT_LEFT,20)
        self.lstAttr.InsertColumn(2,u'Value',wx.LIST_FORMAT_RIGHT,150)

        self.lstElement.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,60)
        self.lstElement.InsertColumn(1,u'Description',wx.LIST_FORMAT_LEFT,140)
        self.lstElement.InsertColumn(2,u'Type',wx.LIST_FORMAT_LEFT,100)

        img=images.getApplyBitmap()
        self.gcbbChange.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
    def SetDoc(self,doc):
        self.doc=doc
    def SetNode(self,node):
        self.node=node
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.selElem=None
        self.attrName=''
        self.dictAttr={}
        self.dictAttributes={}
        self.selAttrIdx=-1
        self.lstElement.Enable(True)
        if node is not None:
            self.txtType.SetValue(self.doc.getTagName(node))
            self.__updatePosElem__(self.doc.getTagName(node))
        else:
            self.txtType.SetValue('')
            self.__updatePosElem__(None)

    def __getElement__(self,elemName):
        for e in ELEMENTS:
            if e[0]==elemName:
                return e
        return None
    def __addElement__(self,k):
        posElem=self.__getElement__(k)
        if posElem is None:
            return
        idx=ELEMENTS.index(posElem)
        index = self.lstElement.InsertImageStringItem(sys.maxint, posElem[0], -1)
        try:
            self.lstElement.SetStringItem(index,1,posElem[1]['__desc'],-1)
        except:
            pass
        try:
            self.lstElement.SetStringItem(index,2,posElem[1]['__type'],-1)
        except:
            pass
        self.lstElement.SetItemData(index,idx)
    def __addTypeElements__(self,t):
        for e in ELEMENTS:
            if e[1].has_key('__type'):
                if e[1]['__type']==t:
                    idx=ELEMENTS.index(e)
                    index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                    self.lstElement.SetStringItem(index,1,e[1]['__desc'],-1)
                    self.lstElement.SetStringItem(index,2,e[1]['__type'],-1)
                    self.lstElement.SetItemData(index,idx)
                
    def __updatePosElem__(self,elemName):
        self.lstElement.DeleteAllItems()
        bAddRoot=True
        if elemName is not None:
            bAddRoot=False
            if elemName==u'prjengs':
                bAddRoot=True
        if bAddRoot==False:
            e=self.__getElement__(elemName)
            #self.selElem=e
            if e is None:
                return
            keys=e[1]['__possible']
            for k in keys:
                l=string.split(k,':')
                if len(l)==1:
                    self.__addElement__(l[0])
                elif l[0]=='__type':
                    possibleTypes=string.split(l[1],',')
                    for pType in possibleTypes:
                        self.__addTypeElements__(pType)
        else:
            for e in ELEMENTS:
                try:
                    e[1]['__properties'].index('isRoot')
                    idx=ELEMENTS.index(e)
                    index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                    self.lstElement.SetStringItem(index,1,e[1]['__desc'],-1)
                    self.lstElement.SetStringItem(index,2,e[1]['__type'],-1)
                    self.lstElement.SetItemData(index,idx)
            
                except:
                    pass
    def SetXml(self,node):
        self.node=node
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.selElem=None
        self.selAttrIdx=-1
        self.attrName=''
        self.dictAttr=None
        self.dictAttributes={}
        if node is not None:
            self.__updatePosElem__(self.doc.getTagName(node))
            self.txtType.SetValue(self.doc.getTagName(node))
            s=self.doc.getNodeText(node,'tag')
            self.txtTagname.SetValue(s)
            s=self.doc.getNodeText(node,'name')
            self.txtName.SetValue(s)
            s=self.doc.getNodeText(node,'description')
            self.txtDesc.SetValue(s)
            
            nodes=self.doc.getChilds(node)
            self.lstAttr.DeleteAllItems()
            if self.selElem is None:
                return
            d=self.selElem[1]
            keys=d.keys()
            #keys.sort()
            for n in nodes:
                id=self.doc.getKey(n)
                if self.doc.IsKeyValid(id):
                    continue
                tag=self.doc.getTagName(n)
                if tag in ['tag','name','description']:
                    continue
                if tag[:2]=='__':
                    continue
                i=self.lstAttr.FindItem(-1,tag,False)
                sVal=self.doc.getNodeText(n,'val')
                if i<0:
                    index = self.lstAttr.InsertImageStringItem(sys.maxint, tag, -1)
                    self.lstAttr.SetStringItem(index,1,sVal,-1)
                    self.dictAttributes[tag]={'val':sVal}
                else:
                    self.lstAttr.SetStringItem(i,1,sVal,-1)
                    self.dictAttributes[tag]={'val':sVal}
                attrs=self.doc.getChilds(n)
                for a in attrs:
                    self.dictAttributes[tag][self.doc.getTagName(a)]=self.doc.getText(a)
            self.lstElement.Enable(False)
        else:
            self.txtType.SetValue('')
            self.lstElement.Enable(True)
        #self.elements=[]
    def GetNode(self,node):
        if self.selElem is None:
            return None
        #n=doc.createElement(self.selElem[0])
        #n.setAttribute('id',0)
        self.doc.setNodeText(node,'tag',self.txtTagname.GetValue())
        self.doc.setNodeText(node,'name',self.txtName.GetValue())
        self.doc.setNodeText(node,'description',self.txtDesc.GetValue())
        
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i,0)
            sAttr=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sVal=it.m_text
            
            nv=self.doc.getChild(node,sAttr)
            if nv is None:
                nv=self.doc.createSubNode(node,sAttr)
            iVal=int(sVal)
            if iVal==0:
                d=self.dictAttributes[sAttr]['original']
            else:
                d=self.dictAttributes[sAttr]['predef']
            if iVal==1:
                self.doc.setAttribute(nv,'fid',d['__fid'])
                self.doc.setText(nv,d['val'],doc)
            else:
                keys=d.keys()
                keys.sort()
                for k in keys:
                    if d[k] is None:
                        continue
                    child=self.doc.getChild(nv,k)
                    if child is not None:
                        self.doc.setNodeText(nv,k,d[k])
                    else:                
                        self.doc.createSubNodeText(nv,k,d[k])
        self.doc.AlignNode(node)
        
    def GetXml(self,node):
        if self.selElem is None:
            return None
        n=self.doc.createNode(self.selElem[0])
        self.doc.createSubNodeText(n,'tag',self.txtTagname.GetValue())
        self.doc.createSubNodeText(n,'name',self.txtName.GetValue())
        self.doc.createSubNodeText(n,'description',self.txtDesc.GetValue())
        
        if 1==0:
            n=doc.createElement(self.selElem[0])
            #n.setAttribute('id',0)
            nv=doc.createElement('tag')
            nv.appendChild(doc.createTextNode(self.txtTagname.GetValue()))
            n.appendChild(nv)
            
            nv=doc.createElement('name')
            nv.appendChild(doc.createTextNode(self.txtName.GetValue()))
            n.appendChild(nv)
            
            nv=doc.createElement('description')
            nv.appendChild(doc.createTextNode(self.txtDesc.GetValue()))
            n.appendChild(nv)
        
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i,0)
            sAttr=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sVal=it.m_text
            nv=self.doc.createSubNode(n,sAttr)
            if 1==0:
                nv=doc.createElement(sAttr)
                #nv.appendChild(doc.createTextNode(sVal))
                n.appendChild(nv)
            iVal=int(sVal)
            if iVal==0:
                d=self.dictAttributes[sAttr]['original']
            else:
                d=self.dictAttributes[sAttr]['predef']
            if iVal==1:
                self.doc.setAttribute(nv,'fid',d['__fid'])
                self.doc.setText(nv,d['val'])
            else:
                keys=d.keys()
                keys.sort()
                for k in keys:
                    if d[k] is None:
                        continue
                    if 1==0:
                        a=doc.createElement(k)
                        a.appendChild(doc.createTextNode(d[k]))
                        nv.appendChild(a)
                    a=self.doc.createSubNodeText(nv,k,d[k])
        #vtXmlDomTree.vtXmlDomAlignNode(doc,n)
        return n

    def __addElementAttr__(self):
        if self.dlgBrowse is None:
            self.dlgBrowse=vBrowseDialog(self)
            self.dlgBrowse.SetElements(ELEMENTS)
            self.dlgBrowse.SetDoc(self.doc)
        self.lstAttr.DeleteAllItems()
        d=self.selElem[1]
        self.dictAttr=None
        self.attrName=''
        keys=d.keys()
        keys.sort()
        bFoundInher=False
        bFoundType=False
        for k in keys:
            if k[:2]!='__':
                dInfo={}
                infoKeys=d[k].keys()
                infoKeys.sort()
                for infoK in infoKeys:
                    dInfo[infoK]=d[k][infoK]
                self.dictAttributes[k]={'original':dInfo}
                if k=='inheritance_type':
                    bFoundInher=True
                if k=='type':
                    bFoundType=True
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
                
                sSel='0'
                sTypeName=''
                dBrowse={'__browse':'*','__fid':'','name':k}
                node=self.dlgBrowse.SetNode('type','',dBrowse,'typeContainer')
                if node is not None:
                    rootNode=Hierarchy.getRootNode(self.doc,node)
                    sTypeName=Hierarchy.getTagNamesWithRel(self.doc,rootNode,None,node)
                    c=self.doc.getChild(node,k)
                    if c is not None:
                        sVal=self.doc.getNodeText(c,'val')
                    else:
                        sVal=''
                    dBrowse['val']=sVal
                    dBrowse['__fid']=self.doc.getAttribute(node,'id')
                    dBrowse['__predef']=sTypeName
                    self.dictAttributes[k]['predef']=dBrowse
                    sSel='1'
                self.dictAttributes[k]['sel']=sSel
                self.lstAttr.SetStringItem(index,1,sSel,-1)
                self.lstAttr.SetStringItem(index,2,sTypeName,-1)
        
    def OnGcbbDelButton(self, event):
        event.Skip()

    def OnGcbbChangeButton(self, event):
        i=self.selAttrIdx
        #self.lstAttr.FindItem(-1,sAttr,False)
        keys=self.dictAttributes[self.attrName].keys()
        if len(keys)==3:
            it=self.lstAttr.GetItem(self.selAttrIdx,1)
            sVal=it.m_text
            i=int(sVal)
            i+=1
            if i>1:
                i=0
            self.lstAttr.SetStringItem(self.selAttrIdx,1,str(i),-1)
            self.dictAttributes[self.attrName]['sel']=sVal
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        k=it.m_text
        self.selAttrIdx=it.m_itemId
        try:
            d=self.selElem[1]
            self.attrName=k
            
        except:
            self.attrName=''
        event.Skip()

    def OnLstElementListItemSelected(self, event):
        idx=event.GetIndex()
        idxElem=self.lstElement.GetItemData(idx)
        self.selElem=ELEMENTS[idxElem]
        self.__addElementAttr__()
        event.Skip()

    def OnLstElementListItemDeselected(self, event):
        self.selAttrIdx=-1
        self.selAttrIdx=-1
        self.attrName=''
        self.lstAttr.DeleteAllItems()
        event.Skip()

    def OnGccbBrowseButton(self, event):
        if self.selAttrIdx<0:
            return
        if self.dlgBrowse is None:
            self.dlgBrowse=vBrowseDialog(self)
            self.dlgBrowse.SetElements(ELEMENTS)
            self.dlgBrowse.SetDoc(self.doc)
        self.dlgBrowse.Centre()
        if self.dictAttributes[self.attrName].has_key('link'):
            pass
        else:
            d={'__browse':'*','__fid':'','name':self.attrName}
            self.dlgBrowse.SetNode('type','',d,'typeContainer')
        ret=self.dlgBrowse.ShowModal()
        if ret==1:
            node=self.dlgBrowse.GetSelNode()
            rootNode=Hierarchy.getRootNode(self.doc,node)
            sTypeName=Hierarchy.getTagNamesWithRel(self.doc,rootNode,None,node)
            c=self.doc.getChild(node,self.attrName)
            if c is not None:
                sVal=self.doc.getNodeText(c,'val')
            else:
                sVal=''
            d['val']=sVal
            d['__fid']=self.doc.getAttribute(node,'id')
            d['__predef']=sTypeName
            self.dictAttributes[self.attrName]['predef']=d
            self.lstAttr.SetStringItem(self.selAttrIdx,2,d['__predef'],-1)
            pass
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.selAttrIdx=-1
        event.Skip()
