#----------------------------------------------------------------------------
# Name:         vXmlPrjEngTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlPrjEngTreeList.py,v 1.4 2006/05/01 14:32:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.vApps.vPrjEng.vXmlPrjEng import *
from vidarc.vApps.vPrjEng.vXmlPrjEngTree import *

import vidarc.tool.log.vtLog as vtLog

import traceback
import fnmatch,time
import thread,threading

class thdAddElementsTreeList:
    def __init__(self,par,verbose=0):
        self.tree=par
        self.schedule=[]
        self.running = False
        self.verbose=verbose
        self.timer=None
    def AddElements(self,ti,node,func=None,*args):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'func:%s args:%s'%(repr(func),repr(args)))
        if self.running:
            vtLog.vtLogCallDepth(None,'thread is still running %d %s'%(self.running,repr(self.schedule)),1,callstack=True)
            vtLog.vtLogCallDepth(None,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,callstack=False)
            self.schedule.append([ti,node,func,args])
            vtLog.vtLogCallDepth(None,'  scheduled %s'%(repr(self.schedule)),1,callstack=False)
            if self.timer is None:
                self.timer=threading.Timer(1.0,self.StartScheduled)
                self.timer.start()
            return
        self.ti=ti
        self.tiFirst=None
        self.node=node
        try:
            self.lang=self.tree.lang
        except:
            self.lang=None
        self.lastId=''
        self.lastNode=None
        self.lastTreeItem=None
        self.zStart=time.clock()
        self.func=func
        self.args=args
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'',callstack=False)
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        self.updateProcessbar=self.tree.bMaster
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'',callstack=False)
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procElem__(self,node,*args):
        parent=args[0][0]
        self.iAct+=1
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct,-1))
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'act:%d'%(self.iAct),callstack=False)
        self.tree.doc.acquire()
        tagName,infos=self.tree.doc.getNodeInfos(node,self.lang)
        if self.verbose>0:
            #vtLog.vtLogCallDepth(self,'node:%s'%node,callstack=False) 
            vtLog.vtLogCallDepth(self,'tag:%s infos:%s'%(tagName,infos),callstack=False)
        if tagName in self.tree.doc.skip:
            if self.updateProcessbar:
                self.iAct+=self.tree.doc.GetElementAttrCount(node,'id')
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct))
            self.tree.doc.release()
            return 0
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        try:
            tn=self.tree.__addElementByInfos__(parent,node,tagName,infos,True)
            self.tree.__addNodeAttr__(node,tn)
        except:
            traceback.print_exc()
            self.Stop()
            self.tree.doc.release()
            return -1
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'act:%d'%(self.iAct),callstack=False)
        if self.tiFirst is None:
            self.tiFirst=tn
        self.lastId=infos['|id']
        self.lastNode=node
        self.lastTreeItem=tn
        try:
            if infos[self.sExpandInfo]=='1':
                bExpand=True
            else:
                bExpand=False
        except:
            bExpand=False
        self.tree.doc.release()
        self.tree.doc.procChildsAttr(node,'id',self.__procElem__,tn)
        if bExpand:
            self.tree.Expand(tn)
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'act:%d'%(self.iAct),callstack=False)
        return 0
    def Run(self):
        self.tree.iBlockTreeSel=1
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        self.sExpandInfo='|'+self.tree.sExpandName
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'node:%s ti:%s'%(self.node,self.ti))
        self.iAct=0
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct,self.tree.doc.GetElementAttrCount()))
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'act:%d'%(self.iAct),callstack=False)
            #print '   ',self.tree.tiRoot,self.ti
        bBuildCache=False
        if self.ti==self.tree.tiRoot:
            if self.ti is None:
                # build root
                self.tree.doc.acquire()
                tagName,infos=self.tree.doc.getNodeInfos(self.node,self.lang)
                tn=self.tree.__addRootByInfos__(self.node,tagName,infos)
                self.tree.doc.release()
                self.ti=tn
                #self.tree.tiRoot=tn
            self.tiRoot=self.ti
            bBuildCache=True
        else:
            self.tiRoot=None
        
        if (self.node is not None) and (self.ti is not None):
            self.tiFirst=self.ti
            self.tree.doc.procChildsAttr(self.node,'id',self.__procElem__,self.ti)
        if self.tiRoot is not None:
            self.tree.Expand(self.tiRoot)
        if self.ti is not None:
            self.tree.__sortAllChilds__(self.ti)
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,0))
        
        self.running = False
        if self.keepGoing:
            if self.func is not None:
                self.func(*self.args[0])
            if self.updateProcessbar:
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElementsFinished(self.tree,self.tiFirst))
        else:
            if self.updateProcessbar:
                wx.PostEvent(self.tree,EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(self.tree,self.tiFirst))
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'kepp:%d running:%d'%(self.keepGoing , self.running),callstack=False)
        self.keepGoing = False
        self.StartScheduled()
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'',callstack=False)
        self.tree.iBlockTreeSel=0
    def StartScheduled(self):
        vtLog.vtLogCallDepth(None,'kepp:%d running:%d'%(self.keepGoing , self.running),1,callstack=True)
        vtLog.vtLogCallDepth(None,'scheduled %d'%(len(self.schedule)),1,callstack=False)
        if len(self.schedule)>0:
            vtLog.vtLogCallDepth(None,'scheduled',1,callstack=False)
            tup=self.schedule[0]
            vtLog.vtLogCallDepth(None,'tup:%s'%(repr(tup)),1,callstack=False)
            if self.verbose>0:
                vtLog.vtLogCallDepth(self,'start schedule:%s'%(repr(tup)),callstack=False)
            self.schedule=self.schedule[1:]
            self.AddElements(tup[0],tup[1],tup[2],*tup[3])
            vtLog.vtLogCallDepth(None,'scheduled started:%s'%(tup),1,callstack=False)
            self.timer=threading.Timer(1.0,self.StartScheduled)
            self.timer.start()
        else:
            self.timer=None
    def dummy(self):
        pass

class vXmlPrjEngTreeList(wx.gizmos.TreeListCtrl,vXmlPrjEngTree):
    def __init__(self, parent, id, pos, size, style, name,cols,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        if id<0:
            id=wx.NewId()
        wx.gizmos.TreeListCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=wx.TR_HAS_BUTTONS)
        
        #self.SetAutoLayout(True)
        wx.EVT_TREE_SEL_CHANGED(self, id,
              self.OnTcNodeTreeSelChanged)
        #EVT_RIGHT_DOWN(self, self.OnTcNodeRightDown)
        #if STORE_EXPAND==1:
        #    EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapes)
        #    EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
        self.doc=None
        self.rootNode=None
        self.objTreeItemSel=None
        self.triSelected=None
        self.dlgAdd=None
        self.dlgRename=None
        
        self.ids=None#vtXmlDomTree.NodeIds()
        self.tiCache=None
        #self.prjAttr=ATTRS
        self.rootNode=None
        self.label=[]
        self.nodeInfos=['tag','name','|id','|iid','|expand']
        self.nodeKey='|id'
        self.groupItems={}
        self.bGrouping=False
        
        self.language=None
        self.languages=[]
        self.langIds=[]
        self.dlgElem=None
        self.dlgElemEdit=None
        self.dlgViewElementsTable=None
        self.dlgInstance=None
        self.dlgVsas=None
        self.dlgInstShow=None
        self.dlgFilter=None
        self.nodeCut=None
        self.triCut=None
        self.nodeCopy=None
        self.tiRoot=None
        self.iBlockTreeSel=0
        self.bMaster=master
        self.bController=controller
        self.bNetResponse=True
        self.verbose=verbose
        
        self.thdAddElements=thdAddElementsTreeList(self,verbose=1)
        #self.thdAddElements=thdAddElementsInst(self)
        self.bGuiUpdate=gui_update
        self.bAddElement=False
        
        self.SetDftGrouping()
        
        self.bStoreExpand=False
        self.sExpandName='expand'
        
        self.cols=cols
        for it in cols:
            #wx.gizmos.TreeListCtrl.AddColumn(self,it)
            self.AddColumn(it)
        self.SetMainColumn(0)
        self.SetColumnWidth(0,130)
        self.SetupImageList()
    def OnTcNodeTreeSelChangedl(self, event):
        vXmlPrjEng.OnTcNodeTreeSelChanged(self, event)
    def __addElement__(self,parent,o,bRet=False):
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        infos={}
        for tagName in self.nodeInfos:
            tags=string.split(tagName,':')
            nodeTmpl=o
            for tag in tags[:-1]:
                nodeTmpl=self.doc.getChildLang(nodeTmpl,tag,self.lang)
            sub=string.split(tags[-1],'|')
            if len(sub)==2:
                if len(sub[0])>0:
                    nodeTmpl=self.doc.getChildLang(nodeTmpl,sub[0],self.lang)
                infos[tagName]=self.doc.getAttribute(nodeTmpl,sub[1])
            else:
                infos[tagName]=self.doc.getNodeTextLang(nodeTmpl,tags[-1],self.lang)
        parNode=self.GetPyData(parent)
        try:
            grpItem=self.groupItems[parNode]
        except:
            grpItem={}
            self.groupItems[parNode]=grpItem
        tnparent=parent
        for g in self.grouping:
            val=infos[g[0]]
            val=self.__getValFormat__(g,val,infos)
            imgTuple=self.__getImgFormat__(g,val)
            try:
                act=grpItem[val]
                grpItem=act[1]
                tnparent=act[0]
            except:
                tid=wx.TreeItemData()
                tid.SetData(None)
                
                tn=self.AppendItem(tnparent,val,-1,-1,tid)
                self.SetItemImage(tn,imgTuple[0],0,which=wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgTuple[0],0,
                                    which=wx.TreeItemIcon_Expanded)
                self.SetItemImage(tn,imgTuple[0],0,
                                    which=wx.TreeItemIcon_Selected)
                
                act=(tn,{})
                grpItem[val]=act
                grpItem=act[1]
                tnparent=tn
        sLabel=''
        for v in self.label:
            val=infos[v[0]]
            val=self.__getValFormat__(v,val)
            sLabel=sLabel+val+' '
        # check project entry type
        tid=wx.TreeItemData()
        tid.SetData(o)
        
        tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
        self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)
        o=self.GetPyData(parent)
        if o is not None:
            if self.doc.getNodeText(o,'__expand')=='1':
                self.Expand(parent)
        
        # add grouping sub nodes here
        self.tiLast=tn
        if bRet==True:
            return tn
    def __addNodeAttr__(self,node,tip):
        if tip is None:
            return
        tagName=self.doc.getTagName(node)
        try:
            img=self.imgDict['attr'][tagName][0]
        except:
            img=self.imgDict['attr']['dft'][0]
        try:
            imgSel=self.imgDict['attr'][tagName][1]
        except:
            imgSel=self.imgDict['attr']['dft'][1]
        for c in self.doc.getChilds(node):
            sLabel=self.doc.getTagName(c)
            if sLabel[:2]=='__':
                continue
            if len(self.doc.getAttribute(c,'id'))==0:
                
                tid=wx.TreeItemData()
                tid.SetData(c)
                
                tn=self.AppendItem(tip,sLabel,-1,-1,tid)
                self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
                self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)
                sVal=self.doc.getNodeText(c,'val')
                self.SetItemText(tn,sVal,1)
    def __findAttr__(self,ti,node2Find):
        if node2Find is None:
            return
        tag2Find=self.doc.getTagName(node2Find)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            o=self.GetPyData(triChild[0])
            if self.doc.getTagName(o)==tag2Find:
                return triChild[0]
            triChild=self.GetNextChild(ti,triChild[1])
        return None
    def FindAttr(self,ti,node2Find):
        if ti is None:
            return 
        return self.__findAttr__(ti,node2Find)
    def FindTreeItem(self,node2find):
        if len(self.doc.getAttribute(node2find,'id'))==0:
            parNode=self.doc.getParent(node2find)
            ti=vXmlPrjEngTree.FindTreeItem(self,parNode)
            ti=self.FindAttr(ti,node2find)
        else:
            ti=vXmlPrjEngTree.FindTreeItem(self,node2find)
        return ti
    def GetNodeAttrsTup(self,node,check=None):
        #ti=self.__findAttr__(self.tiRoot,node)
        ti=self.FindTreeItem(node)
        if ti is None:
            return []
        lst=[]
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            o=self.GetPyData(triChild[0])
            if o is None:
                continue
            if len(self.doc.getAttribute(o,'id'))==0:
                if check is None:
                    lst.append((triChild[0],o))
                else:
                    bFound=False
                    for num in check:
                        if len(self.GetItemText(triChild[0],num))>0:
                            bFound=True
                            break
                    if bFound:
                        lst.append((triChild[0],o))
            triChild=self.GetNextChild(ti,triChild[1])
        return lst
    def __getNodesAttrs__(self,node,lst,check=None):
        lstAttr=self.GetNodeAttrsTup(node,check)
        if len(lstAttr)>0:
            lst.append((node,lstAttr))
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
    def GetNodesAttrs(self,node=None,check=None):
        if node is None:
            node=self.rootNode
        if node is None:
            return []
        lst=[]
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
        return lst
    def SetNode(self,node,skip=[]):
        vXmlPrjEngTree.SetNode(self,node,skip)
        
        