import os,time,string,codecs
import wx
import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
from xml.dom.minidom import getDOMImplementation,parse,parseString,Element

dt=wx.DateTime()
    
GROUPS=[
    ('004','AP-Kleinverteiler Modularsystem'),
    ('005','UP-Kleinverteiler Modularsystem'),
    ('008','TSK und PTSK Anlagen'),
    ('009','Netzschraenke / 19" Schraenke'),
    ('011','Reihen- und Standschrank Stahl'),
    ('012','Reihen- und Standschrank Aluminium'),
    ('013','Schrank/Kasten Stahl rostfrei'),
    ('014','Schrank/Kasten Stahl'),
    ('015','Schrank/Kasten Aluminium'),
    ('016','Schrank/Kasten Kunststoff'),
    ('017','Schaltschrank-Zubehoer'),
    ('018','Apparaterost/Beruehrungsschutz'),
    ('019','Selbstbaurahmen aus Aluminium'),
    ('022','Sammelschienen/SS-Traeger'),
    ('023','Lastschalter/Hauptschalter'),
    ('024','Leistungsschalter'),
    ('025','NH-Sicherungs-Lasttrennschalter'),
    ('026','NH-Sicherungs-Lastschaltleisten'),
    ('027','Neutralleitertrenner > 100A'),
    ('028','NH-Sicherungseinsaetze/Trennmesser'),
    ('029','EW-Messungen'),
    ('031','Sicherungselements'),
    ('032','Leistungsschutzschalter'),
    ('033','Hochleistungsautomaten'),
    ('034','Geraeteschutzschalter'),
    ('035','Fehlerstrom/Leistungsschutzschalter'),
    ('036','Fehlerstromschutzschalter FI'),
    ('037','Zubehoer LS+FI'),
    ('038','Zubehoer Stecksockelsystem zu LS+FI'),
    ('039','Motorschutzschalter'),
    ('041','Lastschuetzzen'),
    ('042','Thermorelais/Starter/Thermisterrelais'),
    ('043','Hilfschuetzen'),
    ('044','Spezialschuetzen/Boilerschuetzen'),
    ('045','Schrittschalter/Treppenlichtautomation/Daemmerschalter'),
    ('046','Installationsschuetzen und Relais DN'),
    ('047','Zeitrelais'),
    ('048','Schaltuhren'),
    ('049','Ueberwachungsrelais Strom/Spannung'),
    ('050','Blitz- und Ueberspannungsschutz'),
    ('057','Steckrelais/Sockel'),
    ('058','Relais/Relaismodule/Koppelrelais'),
    ('059','Halbleiterrelais/Controller'),
    ('061','Messinstrumente/Anzeigegeraete'),
    ('062','Zaehler/Pruefklemmen (Privat)'),
    ('063','Betriebsstundenzaehler'),
    ('064','Stromwandler'),
    ('065','Messumformer'),
    ('066','Trafo fuer DN 35 Montage + Sonnerietrafo'),
    ('067','Trafo geschraubt'),
    ('068','Netzgeraet/Stromversorgung'),
    ('069','Laeutwerke/Summer/Sonnerie'),
    ('071','Drehschalter/Steuerschalter fuer DIN'),
    ('072','Drehschalter/Steuerschalter fuer Front'),
    ('073','Lampen/Taster/Druckschalter DIN'),
    ('074','Lampen/Taster/Druckschalter'),
    ('075','Diodenmodule'),
    ('076','Steckdosen fuer DIN 35 Montage'),
    ('078','Steckdosen/Industriesteckdosen'),
    ('082','Klemmen/Klemmenbloecke'),
    ('110','EIB-Komponenten'),
    ('112','Lichtsteuergeraete/Dimmer'),
    ('120','Klein-SPS/Logikmodul'),
    ('130','Blindstromkompensation'),
    ('140','Frequenzumformer/Sanftanlasser'),
    ('150','Notlicht'),
    ('160','Wohungszaehlerumschalter WZU'),
    ('200','Werkzeug'),
    ('300','Einbau+Verdrehtung angelierfert Apparate'),
    ('310','Regiestunden'),
    ('320','Spesen')
    ]
VENDORS=[
    ('00','Neutral'),
    ('001','VSAS'),
    ('010','ASS'),
    ('011','ABB'),
    ('012','Almatec'),
    ('013','ABB CMC'),
    ('014','Comat'),
    ('015','EAO'),
    ('016','Finder'),
    ('017','EHS Schaffhausen'),
    ('018','Elvatec'),
    ('019','Ghielmetti'),
    ('020','Elbro'),
    ('021','Hager'),
    ('022','Jean Mueller'),
    ('024','Moeller Electric'),
    ('025','Letrona'),
    ('026','Legrand'),
    ('027','Schneider electric'),
    ('028','Trielec'),
    ('029','Optec'),
    ('030','Phoenix Contact'),
    ('031','Rauscher+Stoecklin'),
    ('032','Rittal'),
    ('033','Goerlitz'),
    ('034','Bolliger CAP'),
    ('038','Rockwell'),
    ('042','Weber'),
    ('043','AWAG'),
    ('044','Wisar/Trielec'),
    ('045','Woertz'),
    ('049','Sidler SIPA'),
    ('050','SAIA Burgess'),
    ('051','Siemens'),
    ('052','Sarel'),
    ('053','Selectron Systems'),
    ('054','WAGO'),
    ('055','Weidmueller'),
    ('056','Leutron (Erico)'),
    ('057','GMC'),
    ('058','Schurter')
    ]
def convertCsv2Xml(d,l,grpIds,vendorIds,doc):
    node=doc.createElement('part')
    s="%03d"%int(l[0])
    sId=grpIds.GetIdByName(s)
    #print s,sId
    vtXmlDomTree.setNodeTextAttr(node,'group',s,'id',sId[0],doc)
    s="%03d"%int(l[1])
    vtXmlDomTree.setNodeTextAttr(node,'vendor',s,'id',vendorIds.GetIdByName(s)[0],doc)
    vtXmlDomTree.setNodeText(node,'part',"%03d"%int(l[2]),doc)
    vtXmlDomTree.setNodeText(node,'partnr',l[4],doc)
    vtXmlDomTree.setNodeText(node,'dim',"%03d"%int(l[3]),doc)
    #dt.ParseFormat(l[5],"%d.%m.%Y %H:%M:%S")
    dt.ParseFormat(l[5],"%m/%d/%Y %H:%M:%S")
    vtXmlDomTree.setNodeText(node,'date',dt.FormatISODate(),doc)
    #dt.ParseFormat(l[6],"%d.%m.%Y %H:%M:%S")
    dt.ParseFormat(l[6],"%m/%d/%Y %H:%M:%S")
    vtXmlDomTree.setNodeText(node,'changedate',dt.FormatISODate(),doc)
    vtXmlDomTree.setNodeText(node,'text',l[7],doc)
    vtXmlDomTree.setNodeText(node,'dimtext',l[8],doc)
    vtXmlDomTree.setNodeText(node,'longtext',l[9],doc)
    vtXmlDomTree.setNodeText(node,'ordernr',l[10],doc)
    #vtXmlDomTree.setNodeTextAttr(node,'price',l[12],'currency','CHF',doc)
    try:
        cur=float(l[12])
        vtXmlDomTree.setNodeTextAttr(node,'price',"%g"%cur,'currency','CHF',doc)
        vtXmlDomTree.setNodeTextAttr(node,'price',"%g"%(cur/1.5),'currency','EUR',doc)
    except:
        pass
    #vtXmlDomTree.setNodeTextAttr(node,'sellprice',l[20],'currency','CHF',doc)
    try:
        cur=float(l[20])
        vtXmlDomTree.setNodeTextAttr(node,'sellprice',"%g"%cur,'currency','CHF',doc)
        vtXmlDomTree.setNodeTextAttr(node,'sellprice',"%g"%(cur/1.5),'currency','EUR',doc)
    except:
        pass
    vtXmlDomTree.setNodeText(node,'time',l[18],doc)
    vtXmlDomTree.setNodeText(node,'auxmat',l[19],doc)
    if len(l[21])>0:
        vtXmlDomTree.setNodeText(node,'wireingpoints',l[21],doc)
    if l[22]!='Keiner':
        vtXmlDomTree.setNodeText(node,'crosssection',l[22],doc)
    
    return node

def createXml():
    doc=getDOMImplementation().createDocument(None, "VSAS", None)
    
    elem=doc.createElement('groups')
    doc.documentElement.appendChild(elem)
    vtXmlDomTree.vtXmlDomAlignNode(doc,elem)
    ids=vtXmlDomTree.NodeIds()
    for g in GROUPS:
        node=doc.createElement('group')
        vtXmlDomTree.setNodeText(node,'nr',"%03d"%int(g[0]),doc)
        vtXmlDomTree.setNodeText(node,'name',g[1],doc)
        ids.CheckId(node)
        elem.appendChild(node)
        vtXmlDomTree.vtXmlDomAlignNode(doc,node)
    ids.ProcessMissing()
    ids.ClearMissing()    
    grpIds=vtXmlDomTree.SortedNodeIds()
    grpIds.SetNode(elem,'group','id',[('nr',"%s")])
    
    elem=doc.createElement('vendors')
    doc.documentElement.appendChild(elem)
    vtXmlDomTree.vtXmlDomAlignNode(doc,elem)
    ids=vtXmlDomTree.NodeIds()
    for g in VENDORS:
        node=doc.createElement('vendor')
        vtXmlDomTree.setNodeText(node,'nr',"%03d"%int(g[0]),doc)
        vtXmlDomTree.setNodeText(node,'name',g[1],doc)
        ids.CheckId(node)
        elem.appendChild(node)
        vtXmlDomTree.vtXmlDomAlignNode(doc,node)
    ids.ProcessMissing()
    ids.ClearMissing()
    vendorIds=vtXmlDomTree.SortedNodeIds()
    vendorIds.SetNode(elem,'vendor','id',[('nr',"%s")])
    
    elem=doc.createElement('data')
    doc.documentElement.appendChild(elem)
    vtXmlDomTree.vtXmlDomAlignNode(doc,elem)
    
    return doc,elem,grpIds,vendorIds
def convertVSAS2xml(srcFN,destFN):
    masterdoc,masterelem,grpIds,vendorIds=createXml()
    doc=None
    #srcfile=open(srcFN)
    e=u'ISO-8859-1'
    srcfile=codecs.open(srcFN,"r",e)
    lines=srcfile.readlines()
    d={}
    d['grp']={'lief':{}}
    d['lief']={}
    d['art']={}
    d['dim']={}
    i=1
    s=''
    iL=0
    iLen=len(lines[1:])
    sGrp=''
    ids=vtXmlDomTree.NodeIds()
    startId=0
    for l in lines[1:]:
        #print "+++++++++++++++"
        #print l
        #print "--------------"
        s=string.split(l,'|')
        if s[0]!=sGrp:
            if doc is not None:
                ids.ProcessMissing()
                ids.ClearMissing()
                endId=ids.lastId
                e=u'ISO-8859-1'
                fn=destFN+'_%03d.xml'%int(sGrp)
                f=codecs.open(fn,"w",e)
                doc.writexml(f,encoding=e)
                f.close()
                doc.unlink()
                
                sId=grpIds.GetIdByName('%03d'%int(sGrp))
                #print s,sId
                elem=masterdoc.createElement('group')
                elem.setAttribute('id',sId[0])
                masterelem.appendChild(elem)
                vtXmlDomTree.setNodeText(elem,'nr','%03d'%int(sGrp),masterdoc)
                vtXmlDomTree.setNodeText(elem,'filename',fn,masterdoc)
                vtXmlDomTree.setNodeText(elem,'startid',"%08d"%startId,masterdoc)
                vtXmlDomTree.setNodeText(elem,'endid',"%08d"%endId,masterdoc)
                vtXmlDomTree.vtXmlDomAlignNode(masterdoc,elem)
                startId=endId
                #vtXmlDomTree.setNodeTextAttr(masterelem,'group',"%03d"%int(g[0]),doc)
            sGrp=s[0]
            doc,elem,grpIds,vendorIds=createXml()
        n=convertCsv2Xml(d,s,grpIds,vendorIds,doc)
        elem.appendChild(n)
        vtXmlDomTree.vtXmlDomAlignNode(doc,n)
        ids.CheckId(n)
        iL=iL+1
        if (iL%10)==0:
            print "%04d / %04d"%(iL,iLen)
        if 1==0:
            try:
                s=s+l
                if l[-3]=='|':
                    i=0
                #print l[-3:]
            except:
                i=100
                s=''
                break
            if i==0:
                n=convertCsv2Xml(d,string.split(s,'|'),grpIds,vendorIds,doc)
                elem.appendChild(n)
                vtXmlDomTree.vtXmlDomAlignNode(doc,n)
                ids.CheckId(n)
                s=''
            i=i+1
        
    ids.ProcessMissing()
    ids.ClearMissing()
    srcfile.close()
    
    e=u'ISO-8859-1'
    fn=destFN+'_%03d.xml'%int(sGrp)
    f=codecs.open(fn,"w",e)
    doc.writexml(f,encoding=e)
    f.close()
    
    endId=ids.lastId
    sId=grpIds.GetIdByName('%03d'%int(sGrp))
    #print s,sId
    elem=masterdoc.createElement('group')
    elem.setAttribute('id',sId[0])
    masterelem.appendChild(elem)
    vtXmlDomTree.setNodeText(elem,'nr','%03d'%int(sGrp),masterdoc)
    vtXmlDomTree.setNodeText(elem,'filename',fn,masterdoc)
    vtXmlDomTree.setNodeText(elem,'startid',"%08d"%startId,masterdoc)
    vtXmlDomTree.setNodeText(elem,'endid',"%08d"%endId,masterdoc)
    vtXmlDomTree.vtXmlDomAlignNode(masterdoc,elem)
    
    e=u'ISO-8859-1'
    fn=destFN+'.xml'
    f=codecs.open(fn,"w",e)
    masterdoc.writexml(f,encoding=e)
    f.close()
        
    pass
    
if __name__=='__main__':
    convertVSAS2xml('VSAS2004b.csv','vsas2004')