#Boa:FramePanel:vElementEditPanel
#----------------------------------------------------------------------------
# Name:         vElementEditPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElementEditPanel.py,v 1.1 2005/12/13 13:20:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import sys,string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom
#from xml.dom.minidom import getDOMImplementation,parse,parseString,Element

from vidarc.vApps.vPrjEng.vTypeInheritanceDialog import *
import vidarc.vApps.vPrjEng.BaseTypes as BaseTypes
from vidarc.vApps.vPrjEng.vAttributeDialog import *
from vidarc.vApps.vPrjEng.vBrowseDialog import *

import images

[wxID_VELEMENTEDITPANEL, wxID_VELEMENTEDITPANELGCBBADD, 
 wxID_VELEMENTEDITPANELGCBBBROWSE, wxID_VELEMENTEDITPANELGCBBDEL, 
 wxID_VELEMENTEDITPANELGCBBSET, wxID_VELEMENTEDITPANELGCBBTYPE, 
 wxID_VELEMENTEDITPANELLBLATTR, wxID_VELEMENTEDITPANELLBLDESC, 
 wxID_VELEMENTEDITPANELLBLNAME, wxID_VELEMENTEDITPANELLBLTAGNAME, 
 wxID_VELEMENTEDITPANELLBLTYPE, wxID_VELEMENTEDITPANELLSTATTR, 
 wxID_VELEMENTEDITPANELTXTATTRNAME, wxID_VELEMENTEDITPANELTXTATTRVAL, 
 wxID_VELEMENTEDITPANELTXTDESC, wxID_VELEMENTEDITPANELTXTNAME, 
 wxID_VELEMENTEDITPANELTXTTAGNAME, wxID_VELEMENTEDITPANELTXTTYPE, 
] = [wx.NewId() for _init_ctrls in range(18)]

class vElementEditPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VELEMENTEDITPANEL,
              name=u'vElementEditPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(394, 315), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(386, 288))

        self.lblType = wx.StaticText(id=wxID_VELEMENTEDITPANELLBLTYPE,
              label=u'Type', name=u'lblType', parent=self, pos=wx.Point(29, 4),
              size=wx.Size(24, 13), style=0)

        self.txtType = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTTYPE,
              name=u'txtType', parent=self, pos=wx.Point(56, 4),
              size=wx.Size(216, 21), style=0, value=u'')
        self.txtType.Enable(False)

        self.lblTagName = wx.StaticText(id=wxID_VELEMENTEDITPANELLBLTAGNAME,
              label=u'Tagname', name=u'lblTagName', parent=self, pos=wx.Point(8,
              32), size=wx.Size(45, 13), style=0)

        self.txtTagname = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTTAGNAME,
              name=u'txtTagname', parent=self, pos=wx.Point(56, 32),
              size=wx.Size(120, 21), style=0, value=u'')

        self.lblName = wx.StaticText(id=wxID_VELEMENTEDITPANELLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(25, 60),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(56, 60),
              size=wx.Size(184, 21), style=0, value=u'')

        self.lblDesc = wx.StaticText(id=wxID_VELEMENTEDITPANELLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(0, 88), size=wx.Size(53, 13), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(56, 88),
              size=wx.Size(320, 56), style=wx.TE_MULTILINE, value=u'')

        self.lblAttr = wx.StaticText(id=wxID_VELEMENTEDITPANELLBLATTR,
              label=u'Attributes', name=u'lblAttr', parent=self, pos=wx.Point(9,
              144), size=wx.Size(44, 13), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VELEMENTEDITPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(8, 178),
              size=wx.Size(280, 100), style=wx.LC_REPORT)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VELEMENTEDITPANELLSTATTR)

        self.txtAttrName = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTATTRNAME,
              name=u'txtAttrName', parent=self, pos=wx.Point(56, 152),
              size=wx.Size(96, 21), style=wx.TE_RICH2, value=u'')

        self.txtAttrVal = wx.TextCtrl(id=wxID_VELEMENTEDITPANELTXTATTRVAL,
              name=u'txtAttrVal', parent=self, pos=wx.Point(152, 152),
              size=wx.Size(136, 21), style=0, value=u'')

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTEDITPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(300, 184), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VELEMENTEDITPANELGCBBSET)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTEDITPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(300, 218), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VELEMENTEDITPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTEDITPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(300, 252), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VELEMENTEDITPANELGCBBDEL)

        self.gcbbType = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTEDITPANELGCBBTYPE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Type', name=u'gcbbType',
              parent=self, pos=wx.Point(280, 40), size=wx.Size(76, 30),
              style=0)
        self.gcbbType.Bind(wx.EVT_BUTTON, self.OnGcbbTypeButton,
              id=wxID_VELEMENTEDITPANELGCBBTYPE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMENTEDITPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(300, 150),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGccbBrowseButton,
              id=wxID_VELEMENTEDITPANELGCBBBROWSE)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.selElem=None
        self.lang=None
        self.dlgAttr=None
        self.dlgType=None
        self.attrName=''
        self.selAttrIdx=-1
        #self.lstDocs.InsertColumn(0,u'Doc')
                
        self.lstAttr.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,90)
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_RIGHT,150)
        
        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getTypeBitmap()
        self.gcbbType.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowse.Enable(False)
                    
    def SetNode(self,node,doc=None):
        self.doc=doc
        self.node=node
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtAttrName.SetValue('')
        self.gcbbBrowse.Enable(False)
        self.gcbbType.Enable(False)
        self.attrName=''
        self.dictAttr={}
        self.dictAttributes={}
        self.selAttrIdx=-1
        if node is not None:
            self.txtType.SetValue(node.tagName)
        else:
            self.txtType.SetValue('')
        
    def SetXml(self,node,doc=None):
        self.doc=doc
        self.node=node
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtAttrName.SetValue('')
        self.gcbbBrowse.Enable(False)
        self.gcbbType.Enable(False)
        self.selAttrIdx=-1
        self.attrName=''
        self.dictAttr=None
        self.dictAttributes={}
        if node is not None:
            self.txtType.SetValue(node.tagName)
            s=vtXmlDomTree.getNodeText(node,'tag')
            self.txtTagname.SetValue(s)
            s=vtXmlDomTree.getNodeText(node,'name')
            self.txtName.SetValue(s)
            s=vtXmlDomTree.getNodeText(node,'description')
            self.txtDesc.SetValue(s)
            
            nodes=vtXmlDomTree.getChilds(node)
            self.lstAttr.DeleteAllItems()
            
            #keys.sort()
            for n in nodes:
                id=n.getAttribute('id')
                if len(id)>0:
                    continue
                tag=n.tagName
                if tag in ['tag','name','description']:
                    continue
                if tag[:2]=='__':
                    continue
                i=self.lstAttr.FindItem(-1,tag,False)
                sVal=vtXmlDomTree.getNodeText(n,'val')
                if i<0:
                    #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        
                    index = self.lstAttr.InsertImageStringItem(sys.maxint, tag, -1)
                    self.lstAttr.SetStringItem(index,1,sVal,-1)
                    self.dictAttributes[tag]={'val':sVal}
                else:
                    self.lstAttr.SetStringItem(i,1,sVal,-1)
                    self.dictAttributes[tag]={'val':sVal}
                    #self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
                attrs=vtXmlDomTree.getChilds(n)
                for a in attrs:
                    self.dictAttributes[tag][a.tagName]=vtXmlDomTree.getText(a)
        else:
            self.txtType.SetValue('')
        #self.elements=[]
    def GetNode(self,node,doc=None):
        if doc is None:
            doc=vtXmlDomTree.__getDocument__(node)
        #n=doc.createElement(self.selElem[0])
        #n.setAttribute('id',0)
        vtXmlDomTree.setNodeText(node,'tag',self.txtTagname.GetValue())
        vtXmlDomTree.setNodeText(node,'name',self.txtName.GetValue())
        vtXmlDomTree.setNodeText(node,'description',self.txtDesc.GetValue())
        
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i,0)
            sAttr=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sVal=it.m_text
            
            child=vtXmlDomTree.getChild(node,sAttr)
            if child is not None:
                #vtXmlDomTree.setNodeText(node,sAttr,sVal)
                nv=child
            else:
                nv=doc.createElement(sAttr)
                #nv.appendChild(doc.createTextNode(sVal))
                node.appendChild(nv)
                child=nv
        
            d=self.dictAttributes[sAttr]
            keys=d.keys()
            keys.sort()
            for k in keys:
                if d[k] is None:
                    continue
                child=vtXmlDomTree.getChild(nv,k)
                if child is not None:
                    vtXmlDomTree.setNodeText(nv,k,d[k])
                else:                
                    a=doc.createElement(k)
                    a.appendChild(doc.createTextNode(d[k]))
                    nv.appendChild(a)
        vtXmlDomTree.vtXmlDomAlignNode(doc,node)
        
    def GetXml(self,node,doc=None):
        if self.selElem is None:
            return None
        n=doc.createElement(self.selElem[0])
        #n.setAttribute('id',0)
        nv=doc.createElement('tag')
        nv.appendChild(doc.createTextNode(self.txtTagname.GetValue()))
        n.appendChild(nv)
        
        nv=doc.createElement('name')
        nv.appendChild(doc.createTextNode(self.txtName.GetValue()))
        n.appendChild(nv)
        
        nv=doc.createElement('description')
        nv.appendChild(doc.createTextNode(self.txtDesc.GetValue()))
        n.appendChild(nv)
        
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i,0)
            sAttr=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sVal=it.m_text
            nv=doc.createElement(sAttr)
            #nv.appendChild(doc.createTextNode(sVal))
            n.appendChild(nv)
            d=self.dictAttributes[sAttr]
            keys=d.keys()
            keys.sort()
            for k in keys:
                if d[k] is None:
                    continue
                a=doc.createElement(k)
                a.appendChild(doc.createTextNode(d[k]))
                nv.appendChild(a)
        #vtXmlDomTree.vtXmlDomAlignNode(doc,n)
        return n

    def __addElementAttr__(self):
        self.lstAttr.DeleteAllItems()
        self.txtAttrVal.SetValue('')
        self.txtAttrName.SetValue('')
        self.gcbbBrowse.Enable(False)
        d=self.selElem[1]
        self.dictAttr=None
        self.attrName=''
        keys=d.keys()
        keys.sort()
        bFoundInher=False
        bFoundType=False
        for k in keys:
            if k[:2]!='__':
                dInfo={}
                infoKeys=d[k].keys()
                infoKeys.sort()
                for infoK in infoKeys:
                    dInfo[infoK]=d[k][infoK]
                self.dictAttributes[k]=dInfo
                if k=='inheritance_type':
                    bFoundInher=True
                if k=='type':
                    bFoundType=True
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, -1)
                self.lstAttr.SetStringItem(index,1,d[k]['val'],-1)
                #self.dictAttr=copy.deepcopy(d[k])
        if bFoundType and bFoundInher:
            self.gcbbType.Enable(True)
        else:
            self.gcbbType.Enable(False)
    def OnGcbbAddButton(self, event):
        if self.dlgAttr is None:
            self.dlgAttr=vgdAttribute(self)
        self.dlgAttr.Set(self.attrName,self.dictAttr)
        ret=self.dlgAttr.ShowModal()
        if ret==1:
            self.dictAttr=self.dlgAttr.Get()
            self.dictAttributes[self.attrName]=self.dictAttr
            
        if 1==0:
            sAttr=self.txtAttrName.GetValue()
            sVal=self.txtAttrVal.GetValue()
            i=self.lstAttr.FindItem(-1,sAttr,False)
            if i<0:
                self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            
                index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttr, -1)
                self.lstAttr.SetStringItem(index,1,sVal,-1)
            else:
                self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
            
        event.Skip()

    def OnGcbbDelButton(self, event):
        event.Skip()

    def OnGcbbSetButton(self, event):
        sAttr=self.txtAttrName.GetValue()
        i=self.selAttrIdx
        #self.lstAttr.FindItem(-1,sAttr,False)
        if self.dictAttr is None:
            return
        if i>=0:
            sVal=self.txtAttrVal.GetValue()
            self.lstAttr.SetStringItem(i,1,sVal,-1)
            #self.selElem=self.elements[i]
            #d=self.selElem[1]
            #keys=d.keys()
            #keys.sort()
            #d[keys[i]]=sVal
            self.dictAttr['val']=sVal
            self.dictAttributes[self.attrName]=self.dictAttr
            self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        else:
            self.txtAttrName.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        it=self.lstAttr.GetItem(idx,0)
        self.txtAttrName.SetValue(it.m_text)
        k=it.m_text
        self.selAttrIdx=it.m_itemId
        it=self.lstAttr.GetItem(idx,1)
        self.txtAttrVal.SetValue(it.m_text)
        try:
            self.dictAttr=copy.deepcopy(self.dictAttributes[k])
            self.attrName=k
            
            if self.dictAttr.has_key('__browse'):
                self.gcbbBrowse.Enable(True)
            else:
                self.gcbbBrowse.Enable(False)
        except:
            self.dictAttr=None
            self.attrName=''
        event.Skip()

    def OnGcbbTypeButton(self, event):
        try:
            dictInheri=self.selElem[1]['inheritance_type']
            t=self.dictAttributes['type']
        except:
            return
        if self.dlgType is None:
            self.dlgType=vgdTypeInheritance(self)
        self.dlgType.Centre()
        self.dlgType.Set(self.attrName,
                    copy.deepcopy(self.dictAttributes['inheritance_type']),
                    self.dictAttributes['type'])
        ret=self.dlgType.ShowModal()
        if ret==1:
            sType,self.dictType=self.dlgType.Get()
            print sType,self.dictType
            print self.dictAttributes['type']['val']
            print 
            print self.dictAttributes['inheritance_type']
            #self.dictAttributes['inheritance_type']=self.dictType
            print self.dictAttributes['inheritance_type']
            if sType!=self.dictAttributes['type']['val']:
                self.dictAttributes['type']['val']=sType
                #self.__setupType__()
                #self.dictAttributes['inheritance_type']=self.dictType
                print BaseTypes.setupType(self.dictAttributes)
                #self.inpValue.Clear()
                #self.__genInputs__()
                #self.__setModified__(True)
        event.Skip()

    def OnGccbBrowseButton(self, event):
        dlg=vgdBrowse(self)
        dlg.Centre()
        dlg.SetNode(self.doc,self.selElem,self.dictAttr,ELEMENTS)
        ret=dlg.ShowModal()
        if ret==1:
            node=dlg.GetSelNode()
            self.dictAttr['val']=vtXmlDomTree.getNodeText(node,'tag')
            self.dictAttr['__fid']=node.getAttribute('id')
            self.dictAttributes[self.attrName]=self.dictAttr
            self.lstAttr.SetStringItem(self.selAttrIdx,1,self.dictAttr['val'],-1)
            pass
        dlg.Destroy()
        event.Skip()
