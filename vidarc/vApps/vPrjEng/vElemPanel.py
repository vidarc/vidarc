#Boa:FramePanel:vElemPanel
#----------------------------------------------------------------------------
# Name:         vElemPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vElemPanel.py,v 1.2 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx as wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.masked.numctrl
import wx.lib.intctrl
import wx.lib.buttons
import sys,string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import *

from vidarc.vApps.vPrjEng.vElemAttrDialog import *
from vidarc.vApps.vPrjEng.InputValues import *
from vidarc.vApps.vPrjEng.vTypeInheritanceDialog import *
from vidarc.vApps.vPrjEng.vMCA_CodeLetterDialog import *

import images

[wxID_VELEMPANEL, wxID_VELEMPANELCBMCA, wxID_VELEMPANELGCBBAPPLY, 
 wxID_VELEMPANELGCBBCANCEL, wxID_VELEMPANELLBLDESC, wxID_VELEMPANELLBLID, 
 wxID_VELEMPANELLBLIID, wxID_VELEMPANELLBLNAME, wxID_VELEMPANELLBLTAGNAME, 
 wxID_VELEMPANELLBLTYPE, wxID_VELEMPANELTXTDESC, wxID_VELEMPANELTXTNAME, 
 wxID_VELEMPANELTXTTAGNAME, wxID_VELEMPANELTXTTYPE, 
] = [wx.NewId() for _init_ctrls in range(14)]

# defined event for vgpXmlTree item selected
wxEVT_ELEM_CHANGED=wx.NewEventType()
def EVT_ELEM_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_ELEM_CHANGED,func)
class vgpElemChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_ELEM_CHANGED(<widget_name>, self.OnElemChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_ELEM_CHANGED)
        self.obj=obj
        self.node=node
    def GetElem(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_ELEM_APPLIED=wx.NewEventType()
def EVT_ELEM_APPLIED(win,func):
    win.Connect(-1,-1,wxEVT_ELEM_APPLIED,func)
class vgpElemApplied(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_ELEM_APPLIED(<widget_name>, self.OnElemApplied)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_ELEM_APPLIED)
        self.obj=obj
        self.node=node
    def GetElem(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_ELEM_CANCELED=wx.NewEventType()
def EVT_ELEM_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_ELEM_CANCELED,func)
class vgpElemCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_ELEM_CANCELED(<widget_name>, self.OnElemCanceled)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_ELEM_CANCELED)
        self.obj=obj
        self.node=node
    def GetElem(self):
        return self.obj
    def GetNode(self):
        return self.node


class vElemPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VELEMPANEL, name=u'vElemPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(591, 143),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(583, 116))
        self.SetAutoLayout(True)

        self.lblType = wx.StaticText(id=wxID_VELEMPANELLBLTYPE, label=u'Type',
              name=u'lblType', parent=self, pos=wx.Point(474, 4),
              size=wx.Size(24, 13), style=0)
        self.lblType.SetConstraints(LayoutAnchors(self.lblType, False, True,
              True, False))

        self.txtType = wx.TextCtrl(id=wxID_VELEMPANELTXTTYPE, name=u'txtType',
              parent=self, pos=wx.Point(502, 2), size=wx.Size(76, 21), style=0,
              value=u'')
        self.txtType.Enable(False)
        self.txtType.SetConstraints(LayoutAnchors(self.txtType, False, True,
              True, False))

        self.lblTagName = wx.StaticText(id=wxID_VELEMPANELLBLTAGNAME,
              label=u'Tagname', name=u'lblTagName', parent=self, pos=wx.Point(6,
              4), size=wx.Size(45, 13), style=0)

        self.txtTagname = wx.TextCtrl(id=wxID_VELEMPANELTXTTAGNAME,
              name=u'txtTagname', parent=self, pos=wx.Point(56, 4),
              size=wx.Size(80, 21), style=0, value=u'')
        self.txtTagname.Bind(wx.EVT_TEXT, self.OnTxtTagnameText,
              id=wxID_VELEMPANELTXTTAGNAME)

        self.lblName = wx.StaticText(id=wxID_VELEMPANELLBLNAME, label=u'Name',
              name=u'lblName', parent=self, pos=wx.Point(225, 4),
              size=wx.Size(28, 17), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VELEMPANELTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(256, 4), size=wx.Size(168, 21), style=0,
              value=u'')
        self.txtName.Bind(wx.EVT_TEXT, self.OnTxtNameText,
              id=wxID_VELEMPANELTXTNAME)

        self.lblDesc = wx.StaticText(id=wxID_VELEMPANELLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(0, 30), size=wx.Size(53, 13), style=0)

        self.txtDesc = wx.TextCtrl(id=wxID_VELEMPANELTXTDESC, name=u'txtDesc',
              parent=self, pos=wx.Point(56, 30), size=wx.Size(368, 82),
              style=wx.TE_MULTILINE, value=u'')
        self.txtDesc.Bind(wx.EVT_TEXT, self.OnTxtDescText,
              id=wxID_VELEMPANELTXTDESC)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMPANELGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(493, 48), size=wx.Size(76, 30),
              style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VELEMPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VELEMPANELGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(492, 80),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VELEMPANELGCBBCANCEL)

        self.cbMCA = wx.Button(id=wxID_VELEMPANELCBMCA, label=u'MCA',
              name=u'cbMCA', parent=self, pos=wx.Point(180, 4), size=wx.Size(35,
              23), style=0)
        self.cbMCA.Bind(wx.EVT_BUTTON, self.OnCbMCAButton,
              id=wxID_VELEMPANELCBMCA)

        self.lblID = wx.StaticText(id=wxID_VELEMPANELLBLID, label=u'00000000',
              name=u'lblID', parent=self, pos=wx.Point(472, 24),
              size=wx.Size(48, 13), style=0)
        self.lblID.Enable(False)
        self.lblID.SetConstraints(LayoutAnchors(self.lblID, False, True, True,
              False))

        self.lblIID = wx.StaticText(id=wxID_VELEMPANELLBLIID, label=u'00000000',
              name=u'lblIID', parent=self, pos=wx.Point(528, 24),
              size=wx.Size(48, 13), style=0)
        self.lblIID.Enable(False)
        self.lblIID.SetConstraints(LayoutAnchors(self.lblIID, False, True, True,
              False))

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.lang='en'
        self.lang=None
        self.doc=None
        self.node=None
        self.bModified=False
        self.bAutoApply=False
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs      (self.lblTagName, wx.Right, 5)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.txtTagname.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs      (self.txtTagname, wx.Right, 5)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbMCA.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 4)
        lc.left.SameAs      (self.cbMCA, wx.Right, 5)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.lblName.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 2)
        lc.left.SameAs      (self.lblName, wx.Right, 5)
        lc.right.SameAs     (self.lblType, wx.Left, 5)
        lc.height.AsIs      ()
        self.txtName.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.txtName, wx.Bottom, 4)
        lc.left.SameAs      (self.lblDesc, wx.Right, 2)
        lc.right.SameAs     (self.txtName, wx.Right, 0)
        lc.bottom.SameAs    (self, wx.Bottom, 4)
        self.txtDesc.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.txtType, wx.Bottom, 20)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbApply.SetConstraints(lc);
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.gcbbApply, wx.Bottom, 5)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.gcbbCancel.SetConstraints(lc);
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def Lock(self,flag):
        if flag:
            self.txtTagname.Enable(False)
            self.txtName.Enable(False)
            self.txtDesc.Enable(False)
            self.cbMCA.Enable(False)
            self.gcbbApply.Enable(False)
            self.gcbbCancel.Enable(False)
        else:
            try:
                parNode=self.doc.getParent(self.node)
                if len(self.doc.getAttribute(parNode,'iid'))>0:
                    self.txtTagname.Enable(False)
                else:
                    self.txtTagname.Enable(True)
            except:
                self.txtTagname.Enable(True)
            self.txtName.Enable(True)
            self.txtDesc.Enable(True)
            self.cbMCA.Enable(True)
            self.gcbbApply.Enable(True)
            self.gcbbCancel.Enable(True)
    def SetLanguage(self,lang):
        self.lang=lang
        self.lang=None
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def IsBusy(self):
        return False
    def IsThreadRunning(self):
        bRunning=False
        return bRunning
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'Stop',
                origin=self.GetName())
        pass
    def Clear(self):
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.txtType.SetValue('')
        self.lblID.SetLabel('')
        self.lblIID.SetLabel('')
        self.__setModified__(False)
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.node=None
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node):
        if self.bModified==True:
            if self.bAutoApply:
                vtLog.vtLogCallDepth(None,'',verbose=1)
                self.GetNode(self.node)
                wx.PostEvent(self,vgpElemApplied(self,self.node))
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
                    wx.PostEvent(self,vgpElemApplied(self,self.node))
                else:
                    wx.PostEvent(self,vgpElemCanceled(self,self.node))
        self.__setModified__(False)
        self.node=node
        self.txtTagname.SetValue('')
        self.txtName.SetValue('')
        self.txtDesc.SetValue('')
        self.lblID.SetLabel(self.doc.getKey(node))
        self.lblIID.SetLabel(self.doc.getAttribute(node,'iid'))
        if node is not None:
            self.txtType.SetValue(self.doc.getTagName(node))
            s=self.doc.getNodeText(node,'tag')
            self.txtTagname.SetValue(s)
            s=self.doc.getNodeText(node,'name')
            self.txtName.SetValue(s)
            s=self.doc.getNodeText(node,'description')
            self.txtDesc.SetValue(s)
        else:
            self.txtType.SetValue('')
        try:
            parNode=self.doc.getParent(node)
            if len(self.doc.getAttribute(parNode,'iid'))>0:
                self.txtTagname.Enable(False)
                #self.txtName.Enable(False)
                #self.txtDesc.Enable(False)
            else:
                self.txtTagname.Enable(True)
                #self.txtName.Enable(True)
                #self.txtDesc.Enable(True)
        except:
            self.txtTagname.Enable(True)
            #self.txtName.Enable(True)
            #self.txtDesc.Enable(True)
        #self.elements=[]
        wx.CallAfter(self.__setModified__,False)
        
    def GetNode(self,node,doc=None):
        self.doc.setNodeText(node,'tag',self.txtTagname.GetValue())
        self.doc.setNodeText(node,'name',self.txtName.GetValue())
        self.doc.setNodeText(node,'description',self.txtDesc.GetValue())
        
        self.doc.AlignNode(node)
        self.__setModified__(False)
        wx.PostEvent(self,vgpElemChanged(self,self.node))
    def Apply(self):
        if self.bModified==True:
            if self.bAutoApply:
                vtLog.vtLogCallDepth(None,'',verbose=1)
                self.GetNode(self.node)
                #wx.PostEvent(self,vgpElemApplied(self,self.node))
                return self.node
            else:
                # ask
                dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                            u'vgaPrjEng Elements Info',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
                    #wx.PostEvent(self,vgpElemApplied(self,self.node))
                    return self.node
                else:
                    #wx.PostEvent(self,vgpElemCanceled(self,self.node))
                    return None
        return None
    def OnGcbbApplyButton(self, event):
        self.GetNode(self.node)
        wx.PostEvent(self,vgpElemApplied(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.__setModified__(False)
        self.SetNode(self.node)
        wx.PostEvent(self,vgpElemCanceled(self,self.node))
        event.Skip()

    def OnCbMCAButton(self, event):
        dlg=vMcaCodeLetterDialog(self)
        dlg.Centre()
        ret=dlg.ShowModal()
        if ret==1:
            val=dlg.Get()
            self.txtTagname.SetValue(self.txtTagname.GetValue()+val)
        event.Skip()

    def OnTxtTagnameText(self, event):
        #self.__setModified__(True)
        event.Skip()

    def OnTxtNameText(self, event):
        #self.__setModified__(True)
        event.Skip()

    def OnTxtDescText(self, event):
        #self.__setModified__(True)
        event.Skip()
