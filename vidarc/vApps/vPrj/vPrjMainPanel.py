#Boa:FramePanel:vPrjMainPanel
#----------------------------------------------------------------------------
# Name:         vPrjMainPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjMainPanel.py,v 1.11 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

try:
    #from wxPython.wx import *
    import time
    import vidarc.tool.xml.vtXmlNodeRegSelector
    
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vPrj.vNetPrj import *
    from vidarc.vApps.vContact.vNetContact import *
    
    from vidarc.vApps.vPrj.vXmlPrjGrpTree  import *
    #from vidarc.vApps.vPrj.vPrjInfoListBook import *
    #from vidarc.tool.xml.vtXmlAddDialog import *
    #from vidarc.tool.xml.vtXmlRenameDialog import *
    #from vidarc.vApps.vPrj.vPrjGenPrjDocDialog  import *
    #from vidarc.vApps.vPrj.vPrjGenPrjEngDialog  import *
    #import vidarc.vApps.vPrj.vPrjInfoPanel as vPrjInfoPanel
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    #from vidarc.tool.log.vtLogFileViewerFrame import *
    
    from vidarc.tool.xml.vtXmlNodeRegListBook import *
    
    from vidarc.vApps.vContact.vNetContact import *
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.vApps.vPrj.images as imgPrj
    
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vPrjMainPanel(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wx.NewId(), range(4))

[wxID_VPRJMAINPANEL, wxID_VPRJMAINPANELPNDATA, wxID_VPRJMAINPANELSLWNAV, 
 wxID_VPRJMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(4)]

[wxID_VPRJMAINPANELMNFILEITEMS_OPEN, wxID_VPRJMAINPANELMNFILEITEM_EXIT, 
 wxID_VPRJMAINPANELMNFILEITEM_NEW, wxID_VPRJMAINPANELMNFILEITEM_OPEN_CFG, 
 wxID_VPRJMAINPANELMNFILEITEM_SAVE, wxID_VPRJMAINPANELMNFILEITEM_SAVEAS, 
 wxID_VPRJMAINPANELMNFILEITEM_SAVE_AS_CFG, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(7)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPrj.getApplicationBitmap())
    return icon


[wxID_VPRJMAINPANELMNTOOLSITEM_ALIGN_XML, 
 wxID_VPRJMAINPANELMNTOOLSITEM_GEN_PRJ_ENG, 
 wxID_VPRJMAINPANELMNTOOLSITEM_GEN_PRJ_FILES, 
 wxID_VPRJMAINPANELMNTOOLSITEM_TOOLS_GEN_PRJDOC, 
 wxID_VPRJMAINPANELMNTOOLSMN_TOOLS_REQ, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(5)]

[wxID_VPRJMAINPANELMNHELPMN_HELP_ABOUT, wxID_VPRJMAINPANELMNHELPMN_HELP_HELP, 
 wxID_VPRJMAINPANELMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vPrjMainPanel(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)


    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJMAINPANEL, name=u'vPrjMainPanel',
              parent=prnt, pos=wx.Point(115, 48), size=wx.Size(568, 291),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(560, 264))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPRJMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              264), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPRJMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VPRJMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(207, 0), size=wx.Size(251, 264),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VPRJMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(239, 252), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bConnected=False
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.xdCfg=vtXmlDom(appl='vPrjCfg',audit_trail=False)
        appls=['vPrj','vContact','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0,
                                        audit_trail=True)
        self.netContact=self.netMaster.AddNetControl(vNetContact,'vContact',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        #self.netPrj.SetCfg(self.xdCfg,[],['vPrj'],'vPrj','vgaPrj')
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjNetMaster')
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        #self.netPrj.SetDftLanguages()
        self.bModified=False
        self.dlgGenPrjDoc=None
        self.dlgGenPrjEng=None
        
        self.vgpTree=vXmlPrjGrpTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,340),style=0,name="trPrj",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.EnableMoveMenu(True)
        self.vgpTree.EnableClipMenu(True)
        self.vgpTree.SetDoc(self.netPrj,True)
        
        self.nbPrj=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(276, 176),style=0,name="nbPrj")
        oPrj=self.netPrj.GetRegisteredNode('project')
        self.nbPrj.SetDoc(self.netPrj,True)
        self.viRegSel.AddWidgetDependent(self.nbPrj,oPrj)
        self.nbPrj.Show(False)
        
        pn=self.viRegSel.CreateRegisteredPanel('prjDepartment',self.netPrj)
        #pn.Show(False)
        
        self.viRegSel.SetDoc(self.netPrj,True)
        self.viRegSel.SetRegNode(oPrj,bIncludeBase=True)
        #self.viRegSel.SetRegNode(oContact,bIncludeBase=True)
        self.viRegSel.SetNetDocs(self.netPrj.GetNetDocs())
        
        #self.vgpPrjInfo=vPrjInfoPanel.vPrjInfoPanel(self.pnData,wx.NewId(),
        #        pos=(0,0),size=(551, 264),style=0,name="vgpInfo")
        #self.vgpPrjInfo.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpPrjInfo, True, True, True, True)
        #    )
        #vPrjInfoPanel.EVT_VPRJINFOPANEL_APPLY(self.vgpPrjInfo,self.OnProjectInfoApply)
        
        #self.vgpPrjInfo.SetDoc(self.netPrj,True)
        #self.vgpPrjInfo.SetDocCustomer(self.netCust,True)
        #self.vgpTree.SetDlgAdd(vtXmlAddDialog(self))
        #self.vgpTree.SetDlgRename(vtXmlRenameDialog(self))
        
        self.bAutoConnect=False
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netPrj
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OnProjectInfoApply(self,event):
        self.netPrj.doEdit(self.vgpTree.GetSelected())
        self.vgpTree.RefreshSelected()
        event.Skip()
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbPrj', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        #print event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        event.Skip()
        return
        self.netPrj.endEdit(None)
        node=self.vgpTree.GetSelected()
        self.netPrj.startEdit(node)
        self.vgpPrjInfo.SetNode(event.GetTreeNodeData())
        event.Skip()
        pass
    def OpenFile(self,fn):
        if self.netPrj.ClearAutoFN()>0:
            self.netPrj.Open(fn)
            self.__setModified__(False)
            #self.vgpTree.SetNode(self.netPrj.getChild(None,'projects'))

            self.PrintMsg(_('open file finished. '))
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netPrj.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        pass
    def CreateNew(self):
        self.netPrj.New()
        self.vgpTree.SetNode(self.netPrj.getChild(None,'projects'))
        self.__setModified__(True)
    def OnMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
                
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Project module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Project'),desc,version
