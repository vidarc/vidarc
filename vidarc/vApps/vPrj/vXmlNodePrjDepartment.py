#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060727
# CVS-ID:       $Id: vXmlNodePrjDepartment.py,v 1.4 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPrj.vXmlNodePrjDepartmentPanel import *
        from vidarc.vApps.vPrj.vXmlNodePrjDepartmentEditDialog import *
        from vidarc.vApps.vPrj.vXmlNodePrjDepartmentAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodePrjDepartment(vtXmlNodeTag):
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=[]
    def __init__(self,tagName='prjDepartment'):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project department')
    # ---------------------------------------------------------
    # specific
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smPrj')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xcaIDAT8\x8d\x8d\x91?hSQ\x14\xc6\x7f7)\x053\x14)\x89\xda\xf6%S\x9f\
\xd0\xa2.\xa5\xedbz\x1f\x88\x8a\x8a\x1d\xaa\x18\xe9P;\t\x0e!\xdd\xde\xd4\x7f\
\xa3\xb8\n\nR\xc8$F\xe2(\xa5C\xf3^Z\xb4R\x87\x0e\xb1\x08FP\x11\xc5F\xd0\xfa\
\xbf\xa5M\xafCH\xfan\xf2\x82~p\x86\xf3\x1d\xbes\xbes\x8e8q+\xa1\xf8\x07\nvV4\
\xab\x05\xfc\xc8\xf9\xd1\x0c\xf3\xa3\x99Z~\xfc\xe6\xe5\xa6C\x84\xd7\x81WT\
\x85\x11\r\n9\x14W_.\x1c\xf6uRs\xd0L\xec\xe4\xca\xcaq\\\xda\x1fo\xfc\xff\n^\
\xb1\x94\x95\xdcq\\\x9c\\Y\xc9\x9e\xb8\xb6N\xc0o\xfa\xb9\xfb\t\xa6\xa7\xe2JJ\
K\xe3%`\xf5\xba\x1a\xd7R\x15xQ\xb0\xb3b\xe4RY\x91\xb7\xa0w\x06".\xe4](5:m\
\xa9\x17j\xd5\x92[\x89C\x96\xaf\x18 P\xb0\xb3\xc2+L\xa7\x17\xd5\xfa\xfa\xae\
\xfe\xb6\x92n[v\\\xad\xd5kG,\xd8Y\x91N/\xaa\xfe~\xe9?\xaa\x8a\xcd.\x9c\xe1\
\x838\x17\x87U\xf7^\\\x05\x85\xa8\xf4\xb8rzR\x9d\x19\xbcF\xe4\x08\xb4\x1e\
\x80\xa3\'g\xb8q\xfb;\x89\xd9V\x04\xef\xc8\xaf\x1d\xc3\xfas\x1d\xabg\x84\'\
\x8f\x1e\xf0me\x89\r\xde\xea7\xf8\xfc\xbe\x12\x91(<}9\xc7\xc7O\xcf\x80\x0eV\
\x97\xe1\xde)\x13\x8c0o\x9e\xe7\xf8\xf0\xfa\x05\xa1\xfa\x15\xfc\x1a\x01Dc?\
\x01\xe8l\xeb\x84WE(\x16\x01\x08\x01\xbf\xa8\xfb\x82\x1f\x8c\xd8\x0f\x06\xb6\
\x074.\x04\xfc\x060\xcd}\x07\x99\x05x\xb80\xdb\xd0`gk\x87\xd5p\xb8\x81\xbfc\
\x9a\x9cO\xa6\x10"\x10\xd4\njoR%\xce\x82\x113\xc8\xaf\xdde|l\x1c\x80\xe4DJL\
\xf7\xf5\xa91\xc0\xde\xfc\x8aL\xa6HN\xa4\xc4_\xb40\xa3\xed+_\xaf\xac\x00\x00\
\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodePrjDepartmentEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(348, 174),'pnName':'pnDepartment',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodePrjDepartmentAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(360, 140),'pnName':'pnDepartment',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePrjDepartmentPanel
        else:
            return None

