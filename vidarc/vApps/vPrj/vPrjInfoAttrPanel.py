#Boa:FramePanel:vPrjInfoAttrPanel
#----------------------------------------------------------------------------
# Name:         vPrjInfoAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjInfoAttrPanel.py,v 1.6 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vCustomer.vXmlCustomerInputTree
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputAttrPanel
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM
import vidarc.vApps.vPrj.vXmlPrjGrpTree as vXmlPrjGrpTree

from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import vidarc.tool.log.vtLog as vtLog

[wxID_VPRJINFOATTRPANEL, wxID_VPRJINFOATTRPANELLBLCLT, 
 wxID_VPRJINFOATTRPANELLBLPRJNR, wxID_VPRJINFOATTRPANELTXTID, 
 wxID_VPRJINFOATTRPANELVIATTR, wxID_VPRJINFOATTRPANELVTICLT, 
] = [wx.NewId() for _init_ctrls in range(6)]


# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_ATTR_CHANGED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_ATTR_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_ATTR_CHANGED,func)
class vgpProjectInfoAttrChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_ATTR_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vPrjInfoAttrPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsPrjId_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjNr, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtId, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrjId, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.LEFT | wx.RIGHT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsClt, 0, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.viAttr, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.EXPAND)

    def _init_coll_bxsClt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblClt, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtiClt, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsPrjId = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsClt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsPrjId_Items(self.bxsPrjId)
        self._init_coll_bxsClt_Items(self.bxsClt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJINFOATTRPANEL,
              name=u'vPrjInfoAttrPanel', parent=prnt, pos=wx.Point(269, 256),
              size=wx.Size(377, 239), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(369, 212))
        self.SetAutoLayout(True)

        self.lblPrjNr = wx.StaticText(id=wxID_VPRJINFOATTRPANELLBLPRJNR,
              label=u'Project Number', name=u'lblPrjNr', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(84, 21), style=wx.ALIGN_RIGHT)

        self.txtId = wx.TextCtrl(id=wxID_VPRJINFOATTRPANELTXTID, name=u'txtId',
              parent=self, pos=wx.Point(92, 4), size=wx.Size(100, 21),
              style=wx.TE_RICH2, value=u'')
        self.txtId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VPRJINFOATTRPANELTXTID)

        self.lblClt = wx.StaticText(id=wxID_VPRJINFOATTRPANELLBLCLT,
              label=u'Client', name=u'lblClt', parent=self, pos=wx.Point(4, 33),
              size=wx.Size(84, 24), style=wx.ALIGN_RIGHT)

        self.vtiClt = vidarc.vApps.vCustomer.vXmlCustomerInputTree.vXmlCustomerInputTree(id=wxID_VPRJINFOATTRPANELVTICLT,
              name=u'vtiClt', parent=self, pos=wx.Point(92, 33),
              size=wx.Size(277, 24), style=0)
        self.vtiClt.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnVtiCltVtinputTreeChanged, id=wxID_VPRJINFOATTRPANELVTICLT)
        self.vtiClt.Bind(vidarc.vApps.vCustomer.vXmlCustomerInputTree.vEVT_VXMLCUSTOMER_INPUT_TREE_CHANGED,
              self.OnVtiCltVxmlCustomerInputTreeChanged,
              id=wxID_VPRJINFOATTRPANELVTICLT)

        self.viAttr = vidarc.tool.input.vtInputAttrPanel.vtInputAttrPanel(id=wxID_VPRJINFOATTRPANELVIATTR,
              name=u'viAttr', parent=self, pos=wx.Point(4, 65),
              size=wx.Size(361, 143), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        #self.doc=None
        #self.node=None
        self.selectedIdx=-1
        self.bModified=False
        
        # setup list
        #self.lstAttr.InsertColumn(0,u'Attribute')
        #self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_LEFT,270)
        #self.SetupImageList()
    def SetRegNode(self,obj):
        pass
    def SetNetDocs(self,d):
        pass
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def Clear(self):
        self.txtId.SetValue('')
        self.viAttr.Clear()
        self.vtiClt.Clear()
        vtXmlDomConsumer.Clear(self)
        self.SetModified(False)
        
    def Lock(self,flag):
        if flag:
            self.txtId.SetEditable(False)
            #self.viAttr.Enable(False)
        else:
            self.txtId.SetEditable(True)
            #self.viAttr.Enable(True)
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        vtXmlDomConsumer.SetDoc(self,doc)
        self.viAttr.SetDoc(doc,bNet)
        self.vtiClt.SetDoc(doc,bNet)
    def SetDocCustomer(self,doc,bNet=False):
        self.vtiClt.SetDocTree(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node):
        self.Clear()
        if (node is None) or (self.doc is None):
            return
        self.node=node
        #vtLog.CallStack('')
        #print self.node
        
        sAttrId=self.doc.getAttribute(node,'id')
        self.txtId.SetValue(sAttrId)
        self.viAttr.SetNode(node)
        self.vtiClt.SetNode(node)
        # setup attributes
        self.SetModified(False)
        pass
    def GetNode(self,node=None):
        return
        if node is None:
            node=self.node
        if node is None:
            return
        sAttrId=self.txtId.GetValue()
        ids=self.doc.getIds()
        if ids is not None:
            sOldId=self.doc.getAttribute(node,'id')
            ids.RemoveId(sOldId)
            ret,n=ids.GetIdStr(sAttrId)
            if ret<0:
                bOk=False
            else:
                if n is None:
                    bOk=True
                else:
                    if n==node:
                        bOk=True
                    else:
                        bOk=False
            if bOk==False:
                # fault id is not possible
                ids.AddNewNode(node)
                sAttr=ids.ConvertId2Str(node)
                self.txtId.SetValue(sAttr)
                self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            else:
                self.doc.setAttribute(node,'id',sAttrId)
                sAttr=ids.ConvertId2Str(node)
                self.txtId.SetValue(sAttr)
                self.doc.setAttribute(node,'id',sAttr)
                ids.CheckId(node)
                ids.ClearMissing()
        
        self.viAttr.GetNode(node)
        #vtLog.CallStack('')
        #print node
        self.vtiClt.GetNode(node)
        #print node
        self.SetModified(False)
        return
        attrsNode=self.doc.getChildForced(node,'attributes')
        attrs={}
        for o in self.doc.getChilds(attrsNode):
            tagName=self.doc.getTagName(o)
            attrs[tagName]=o
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sAttrVal=it.m_text
            self.doc.setNodeText(attrsNode,sAttrName,sAttrVal)
            try:
                del attrs[sAttrName]
            except:
                pass
        keys=attrs.keys()
        for k in keys:
            self.doc.deleteNode(o)
        self.SetModified(False)
        
    def OnGcbbSetButton(self,event):
        #index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, -1)
        self.lstAttr.SetStringItem(self.selectedIdx, 1, self.txtVal.GetValue())
        self.SetModified(True)
        #event.skip()

    def OnTxtIdText(self, event):
        if self.doc is None:
            return
        sAttr=self.txtId.GetValue()
        ids=self.doc.getIds()
        ret,n=ids.GetIdStr(sAttr)
        if ret<0:
            ids.AddNewNode(self.node)
            sId=ids.ConvertId2Str(self.node)
            self.txtId.SetValue(sId)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            return
        if n is None:
            bOk=True
        else:
            if self.doc.isSame(n,self.node):
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        self.SetModified(True)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        self.txtAttr.SetValue(sAttrName)
        self.txtVal.SetValue(sAttrVal)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.txtAttr.SetValue(u'')
        self.txtVal.SetValue(u'')
        self.selectedIdx=-1
        event.Skip()

    def OnGcbbAddButton(self, event):
        sAttrName=self.txtAttr.GetValue()
        sAttrVal=self.txtVal.GetValue()
        idx=self.lstAttr.FindItem(0,sAttrName)
        if idx==-1:
            # ok add new attribute
            imgId=-1
            idx = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrName, imgId)
            self.lstAttr.SetStringItem(idx, 1, sAttrVal)
            self.SetModified(True)
        
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedIdx!=-1:
            self.lstAttr.DeleteItem(self.selectedIdx)
            self.SetModified(True)
            self.selectedIdx=-1
        event.Skip()

    def OnVtiCltVtinputTreeChanged(self, event):
        event.Skip()

    def OnVtiCltVxmlCustomerInputTreeChanged(self, event):
        try:
            if event.GetNode() is not None:
                self.viAttr.SetVal(event.GetValue(),'client','')
                self.viAttr.SetVal(event.GetAbbr(),'clientshort','')
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    
    
