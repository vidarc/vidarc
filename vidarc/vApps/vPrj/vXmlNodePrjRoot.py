#----------------------------------------------------------------------------
# Name:         vXmlNodeDocRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vXmlNodePrjRoot.py,v 1.3 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrjRoot(vtXmlNodeRoot):
    def __init__(self,tagName='projects'):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project root')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x97IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\x13\xbf\xc0\xde\x95\x1b\x90x\
\x85\x13*n\xe1\x06V\x0e\xe0\x17V\x98@U\x13WE\x90\xd1\x89]7rD\\\x85\xb4\xda0\
\x9c\n\x04\xb0O\x8b\xa4\x98+\x1d\xb40\x00\x90\xdb\x01{\x95\xd5-h:xSY\x81\xfe\
\xf0\xe8\x0f\x9f\x17`\xd6c"\x13i\xc3pJ7\x8fJ\x80\x11\xd6f\xd3\x0eJ\xc9\xac\
\xf7\xff\x12-\x9b\xb9\x9e\x03tH\xac\xc1\x14{r\xc0\x12\x16\x13\x98\xab}Z\xa4\
\xa6~3\xc4\xb8>/Q\xack\x04\xf4m\xb0\xaf\x9a\x0e\xact\xb2\xc4^o\xb88\xbe\x1c\
\xaa\xcf!\x00\x00\x00\x00IEND\xaeB`\x82'
