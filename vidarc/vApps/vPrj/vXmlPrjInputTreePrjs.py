#----------------------------------------------------------------------------
# Name:         vXmlPrjInputTreePrjs.py
# Purpose:      input widget for project xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060725
# CVS-ID:       $Id: vXmlPrjInputTreePrjs.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputMultipleTree import vtInputMultipleTree
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vApps.vPrj.vXmlPrjGrpTree import vXmlPrjGrpTree

class vXmlPrjInputTreePrjs(vtInputMultipleTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        try:
            _kwargs['columns']
        except:
            _kwargs['columns']=[(_(u'name'),-1),(_(u'longName'),-1),('id',-1)]
        try:
            _kwargs['show_attrs']
        except:
            _kwargs['show_attrs']=['name','longName','|id']
        apply(vtInputMultipleTree.__init__,(self,) + _args,_kwargs)
        self.tagGrp=None
        self.tagName='project'
        self.tagNameInt='project'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vPrj')
    def __createTree__(*args,**kwargs):
        tr=vXmlPrjGrpTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id','country','region'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
