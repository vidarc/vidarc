#Boa:MDIChild:vPrjMDIFrame

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vPrj.images as imgPrj
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.vApps.vPrj.vPrjMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgPrj.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPrj.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vHumanMDIFrame(parent, id, name, pos, size)

DOMAINS=['core','vPrj']
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

[wxID_VHUMANMDIFRAME, wxID_VHUMANMDIFRAMEPANEL1, 
 wxID_VHUMANMDIFRAMESTATUSBAR1, wxID_VHUMANMDIFRAMETEXTCTRL1, 
 wxID_VHUMANMDIFRAMETOOLBAR1, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vPrjMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=-1,
              name=u'vProjectMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vProject')
        self.SetClientSize(wx.Size(392, 223))
        if 0:
            self.panel1 = wx.Panel(id=wxID_VHUMANMDIFRAMEPANEL1, name='panel1',
                  parent=self, pos=wx.Point(0, 28), size=wx.Size(392, 223),
                  style=wx.TAB_TRAVERSAL)
        
            self.textCtrl1 = wx.TextCtrl(id=wxID_VHUMANMDIFRAMETEXTCTRL1,
                  name='textCtrl1', parent=self.panel1, pos=wx.Point(24, 24),
                  size=wx.Size(100, 21), style=0, value='textCtrl1')
        
    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Project')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vPrjMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vPrjPanel')
            self.pn.OpenCfgFile('vPrjCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        #self.Layout()
        #self.Refresh()
    def __getPluginImage__(self):
        return imgPrj.getPluginImage()
    def __getPluginBitmap__(self):
        return imgPrj.getPluginBitmap()
