#----------------------------------------------------------------------------
# Name:         vNetPrj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetPrj.py,v 1.9 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPrj.vXmlPrj import vXmlPrj
from vidarc.tool.net.vNetXmlWxGui import *

class vNetPrj(vXmlPrj,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlPrj.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                    verbose=verbose,audit_trail=audit_trail)
        vXmlPrj.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlPrj.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlPrj.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlPrj.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlPrj.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Open',origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlPrj.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlPrj.Open(self,fn,True)
        else:
            iRet=vXmlPrj.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlPrj.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlPrj.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vXmlPrj.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'delNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.delNode(self,node)
        vXmlPrj.deleteNode(self,node)
    def moveNode(self,nodePar,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s;id:%s'%(self.getKey(nodePar),self.getKey(node)),self.appl)
        vNetXmlWxGui.moveNode(self,nodePar,node)
        vXmlPrj.moveNode(self,nodePar,node)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlPrj.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlPrj.__stop__(self)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyContent',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyGetNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyAddNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyRemoveNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedIds',origin=self.appl)
        self.prjs=self.getSortedNodeIds(['projects'],
                        'project','id',[('name','%s')])
    def GetSortedIds(self):
        try:
            return self.prjs
        except:
            self.GenerateSortedIds()
            return self.prjs
    def GenerateSortedCltIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedCltIds',origin=self.appl)
        self.clts=self.getSortedNodeIds(['projects'],
                        'project','id',[('attributes:clientshort','%s')])
    def GetSortedCltIds(self):
        try:
            return self.clts
        except:
            self.GenerateSortedCltIds()
            return self.clts
    def GenerateSortedCltLongIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedCltLongIds',origin=self.appl)
        self.cltsLong=self.getSortedNodeIds(['projects'],
                        'project','id',[('attributes:client','%s')])
    def GetSortedCltLongIds(self):
        try:
            return self.cltsLong
        except:
            self.GenerateSortedCltLongIds()
            return self.cltsLong
