#----------------------------------------------------------------------------
# Name:         vPrjReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060930
# CVS-ID:       $Id: vPrjReg.py,v 1.1 2006/10/09 11:30:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust
if vcCust.is2Import(__name__):
    import vidarc.tool.log.vtLog as vtLog
    vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    import wx
    import vidarc.tool.lang.vtLgBase as vtLgBase
    def funcCreatePrj(obj,sSrc,parent):
        from vidarc.vApps.vPrj.vXmlPrjInputTree import vXmlPrjInputTree
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        bxs=wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl=wx.StaticText(id=-1,
                  label=_(u'project'), name='lblPrj', parent=parent,
                  pos=wx.Point(0, 0), size=wx.DefaultSize, style=wx.ALIGN_RIGHT)
        lbl.SetMinSize((-1,-1))
        bxs.AddWindow(lbl, 1, border=4, flag=wx.EXPAND|wx.RIGHT)
        viTr=vXmlPrjInputTree(id=-1,name=u'%s_vitrPrj'%parent.GetName(), parent=parent, 
                    pos=wx.Point(60, 0), size=wx.Size(80, 30), style=0)
        viTr.SetMinSize((-1,-1))
        viTr.SetAppl('vPrj')
        viTr.SetEnableMark(False)
        bxs.AddWindow(viTr, 4, border=4, flag=wx.EXPAND)
        return bxs,viTr
else:
    def funcCreatePrj(obj,sSrc,parent):
        return None,None
def funcSetDocPrj(obj,viTr,doc,dNetDocs):
    viTr.SetDoc(doc)
    viTr.SetDocTree(dNetDocs['vPrj']['doc'],dNetDocs['vPrj']['bNet'])
    viTr.SetNodeTree(None)
def funcGetSelPrj(obj,viTr,doc):
    sVal=viTr.GetValue()
    sID=viTr.GetForeignID()
    return sVal,sID
def funcCmpId(doc,node,val,lMatch):
    if lMatch is not None:
        if val in lMatch:
            return True
        else:
            return False
    return True
def Register4Collector(self,tagNode,tagAttr,translation,tagName='dataCollector'):
    oDataColl=self.GetReg(tagName)
    dSrcWid={tagAttr:{
                    'create':funcCreatePrj,
                    'setDoc':funcSetDocPrj,
                    'getSel':funcGetSelPrj,
                    'translation':translation,
                    'match':funcCmpId},
            }
    oDataColl.UpdateSrcWidDict(dSrcWid)
    #oDataColl.UpdateDataProcDict(tagNode,dataProc)
