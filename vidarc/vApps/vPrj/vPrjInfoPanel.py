#Boa:FramePanel:vPrjInfoPanel
#----------------------------------------------------------------------------
# Name:         vPrjInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjInfoPanel.py,v 1.5 2006/05/30 10:40:36 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
import string,sys

#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import vidarc.vApps.vPrj.vXmlPrjGrpTree as vXmlPrjGrpTree
from vidarc.vApps.vPrj.vPrjInfoNoteBook import *

import images

[wxID_VGPPRJINFO, wxID_VGPPRJINFOGCBBAPPLY, wxID_VGPPRJINFOGCBBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]


# defined event for vgpXmlTree item selected
wxEVT_VPRJINFOPANEL_CHANGED=wx.NewEventType()
vEVT_VPRJINFOPANEL_CHANGED=wx.PyEventBinder(wxEVT_VPRJINFOPANEL_CHANGED,1)
def EVT_VPRJINFOPANEL_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VPRJINFOPANEL_CHANGED,func)
class vPrjInfoPanelChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VPRJINFOPANEL_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VPRJINFOPANEL_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

wxEVT_VPRJINFOPANEL_APPLY=wx.NewEventType()
vEVT_VPRJINFOPANEL_APPLY=wx.PyEventBinder(wxEVT_VPRJINFOPANEL_APPLY,1)
def EVT_VPRJINFOPANEL_APPLY(win,func):
    win.Connect(-1,-1,wxEVT_VPRJINFOPANEL_APPLY,func)
class vPrjInfoPanelApply(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VPRJINFOPANEL_APPLY(<widget_name>, self.OnInfoApply)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VPRJINFOPANEL_APPLY)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

wxEVT_VPRJINFOPANEL_CANCEL=wx.NewEventType()
vEVT_VPRJINFOPANEL_CANCEL=wx.PyEventBinder(wxEVT_VPRJINFOPANEL_CANCEL,1)
def EVT_VPRJINFOPANEL_CANCEL(win,func):
    win.Connect(-1,-1,wxEVT_VPRJINFOPANEL_CANCEL,func)
class vPrjInfoPanelCancel(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VPRJINFOPANEL_CANCEL(<widget_name>, self.OnInfoCancel)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VPRJINFOPANEL_CANCEL)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vPrjInfoPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGPPRJINFO, name=u'vgpPrjInfo',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(416, 206),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(408, 179))
        self.SetAutoLayout(True)
        
        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGPPRJINFOGCBBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'gcbbApply',
              parent=self, pos=wx.Point(328, 8), size=wx.Size(72, 30), style=0)
        self.gcbbApply.SetConstraints(LayoutAnchors(self.gcbbApply, False, True,
              True, False))
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VGPPRJINFOGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGPPRJINFOGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(328, 48),
              size=wx.Size(72, 30), style=0)
        self.gcbbCancel.SetConstraints(LayoutAnchors(self.gcbbCancel, False,
              True, True, False))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGPPRJINFOGCBBCANCEL)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.bModified=False
        
        self.vnbPrjInfo=vPrjInfoNoteBook(self,name='vnbPrjInfo',
                                    pos=wx.Point(0,0),size=wx.Size(450,380))
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top, 4)
        lc.left.SameAs      (self, wx.Left, 4)
        lc.bottom.SameAs    (self, wx.Bottom, 4)
        lc.right.SameAs     (self.gcbbCancel, wx.Left, 4)
        self.vnbPrjInfo.SetConstraints(lc)

        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def Clear(self):
        self.vnbPrjInfo.Clear()
        self.SetModified(False)
            
    def Lock(self,flag):
        self.vnbPrjInfo.Lock(flag)
        if flag:
            self.gcbbApply.Enable(False)
            self.gcbbCancel.Enable(False)
        else:
            self.gcbbApply.Enable(True)
            self.gcbbCancel.Enable(True)
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
        self.vnbPrjInfo.SetDoc(doc,bNet)
    def SetDocCustomer(self,doc,bNet=False):
        self.vnbPrjInfo.SetDocCustomer(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node):
        self.Clear()
        if (node is None) or (self.doc is None):
            return
        self.node=node
        self.vnbPrjInfo.SetNode(node)
        
    def GetNode(self):
        if self.node is None:
            return ''
        self.vnbPrjInfo.GetNode()
        
    def OnGcbbApplyButton(self, event):
        self.GetNode()
        wx.PostEvent(self,vPrjInfoPanelApply(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode(self.node)
        wx.PostEvent(self,vPrjInfoPanelCancel(self,self.node))
        event.Skip()    

    def OnVgpPrjInfoSize(self, event):
        #self.Update()
        self.Refresh()
        event.Skip()
