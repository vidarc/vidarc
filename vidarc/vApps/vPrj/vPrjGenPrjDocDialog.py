#Boa:Dialog:vPrjGenPrjDocDialog
#----------------------------------------------------------------------------
# Name:         vPrjGenPrjDocDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjGenPrjDocDialog.py,v 1.3 2006/02/07 12:57:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
#from wxPython.lib.buttons import *
#from wxPython.wx import wxFileDialog
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
#import cStringIO
import vidarc.vApps.vPrjDoc.vXmlPrjDoc as vXmlPrjDoc
import vidarc.tool.InOut.fnUtil as fnUtil
import codecs,os.path

import images

def create(parent):
    return vPrjGenPrjDocDialog(parent)

[wxID_VPRJGENPRJDOCDIALOG, wxID_VPRJGENPRJDOCDIALOGGCBBBROWSE, 
 wxID_VPRJGENPRJDOCDIALOGGCBBCANCEL, wxID_VPRJGENPRJDOCDIALOGGCBBGENERATE, 
 wxID_VPRJGENPRJDOCDIALOGLBLCLIENT, wxID_VPRJGENPRJDOCDIALOGLBLFILENAME, 
 wxID_VPRJGENPRJDOCDIALOGLBLPRJLONG, wxID_VPRJGENPRJDOCDIALOGLBLPRJSHORT, 
 wxID_VPRJGENPRJDOCDIALOGTXTCLIENT, wxID_VPRJGENPRJDOCDIALOGTXTPRJDOCFN, 
 wxID_VPRJGENPRJDOCDIALOGTXTPRJLONG, wxID_VPRJGENPRJDOCDIALOGTXTPRJSHORT, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vPrjGenPrjDocDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJGENPRJDOCDIALOG,
              name=u'vPrjGenPrjDocDialog', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(451, 210), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Generate Project Document')
        self.SetClientSize(wx.Size(443, 183))

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJDOCDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 144),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJGENPRJDOCDIALOGGCBBCANCEL)

        self.lblFileName = wx.StaticText(id=wxID_VPRJGENPRJDOCDIALOGLBLFILENAME,
              label=u'Project Document Filename', name=u'lblFileName',
              parent=self, pos=wx.Point(8, 94), size=wx.Size(130, 13), style=0)

        self.txtPrjDocFN = wx.TextCtrl(id=wxID_VPRJGENPRJDOCDIALOGTXTPRJDOCFN,
              name=u'txtPrjDocFN', parent=self, pos=wx.Point(8, 110),
              size=wx.Size(336, 21), style=wx.TE_RICH2, value=u'')
        self.txtPrjDocFN.Enable(True)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJDOCDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 146),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VPRJGENPRJDOCDIALOGGCBBGENERATE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJDOCDIALOGGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(360, 104),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VPRJGENPRJDOCDIALOGGCBBBROWSE)

        self.lblPrjShort = wx.StaticText(id=wxID_VPRJGENPRJDOCDIALOGLBLPRJSHORT,
              label=u'Project Shortcut', name=u'lblPrjShort', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(76, 13), style=0)

        self.txtPrjShort = wx.TextCtrl(id=wxID_VPRJGENPRJDOCDIALOGTXTPRJSHORT,
              name=u'txtPrjShort', parent=self, pos=wx.Point(8, 24),
              size=wx.Size(72, 21), style=0, value=u'')
        self.txtPrjShort.Enable(False)

        self.lblPrjLong = wx.StaticText(id=wxID_VPRJGENPRJDOCDIALOGLBLPRJLONG,
              label=u'Longname', name=u'lblPrjLong', parent=self,
              pos=wx.Point(112, 8), size=wx.Size(50, 13), style=0)

        self.txtPrjLong = wx.TextCtrl(id=wxID_VPRJGENPRJDOCDIALOGTXTPRJLONG,
              name=u'txtPrjLong', parent=self, pos=wx.Point(112, 24),
              size=wx.Size(320, 21), style=0, value=u'')
        self.txtPrjLong.Enable(False)

        self.txtClient = wx.TextCtrl(id=wxID_VPRJGENPRJDOCDIALOGTXTCLIENT,
              name=u'txtClient', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(72, 21), style=0, value=u'')
        self.txtClient.Enable(False)

        self.lblClient = wx.StaticText(id=wxID_VPRJGENPRJDOCDIALOGLBLCLIENT,
              label=u'Client', name=u'lblClient', parent=self, pos=wx.Point(8,
              48), size=wx.Size(26, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.doc=None
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.bGenerated=False
        
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node):
        self.bGenerated=False
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        self.gcbbCancel.SetLabel(u'Cancel')
        self.node=node
        if (node is None) or (self.doc is None):
            self.txtPrjShort.SetValue('')
            self.txtPrjLong.SetValue('')
            self.txtPrjDocFN.SetValue('')
            self.txtClient.SetValue('')
        else:
            sShort=self.doc.getNodeText(node,'name')
            sLong=self.doc.getNodeText(node,'longname')
            attrNode=self.doc.getChild(node,'attributes')
            if attrNode is not None:
                sClient=self.doc.getNodeText(attrNode,'clientshort')
            sPrjDocFN=self.doc.getNodeText(node,'documentfn')
            self.txtPrjShort.SetValue(sShort)
            self.txtPrjLong.SetValue(sLong)
            self.txtClient.SetValue(sClient)
            self.txtPrjDocFN.SetValue(sPrjDocFN)
            self.txtPrjDocFN.SetStyle(0, len(sPrjDocFN), wx.TextAttr("BLACK", "WHITE"))
            pass
        
    def OnGcbbCancelButton(self, event):
        if self.bGenerated:
            self.EndModal(1)
        else:
            self.EndModal(0)
        event.Skip()
        
    def OnGcbbGenerateButton(self, event):
        sShort=self.txtPrjShort.GetValue()
        sLong=self.txtPrjLong.GetValue()
        sClient=self.txtClient.GetValue()
        sPrjDocFN=self.txtPrjDocFN.GetValue()
        if len(sShort)<=0 and len(sLong)<=0:
            return
        if len(sPrjDocFN)<=0:
            sPrjDocFN='???'
            self.txtPrjDocFN.SetValue(sPrjDocFN)
            self.txtPrjDocFN.SetStyle(0, len(sPrjEngFN), wx.TextAttr("RED", "YELLOW"))
            return
        sFN=os.path.split(sPrjDocFN)[1]
        sTmp=fnUtil.replaceSuspectChars(sFN)
        if sTmp!=sFN:
            self.txtPrjDocFN.SetStyle(0, len(sPrjDocFN), wx.TextAttr("RED", "YELLOW"))
            return
        self.txtPrjDocFN.SetStyle(0, len(sPrjDocFN), wx.TextAttr("BLACK", "WHITE"))
        fn=self.txtPrjDocFN.GetValue()
        
        doc=vXmlPrjDoc.vXmlPrjDoc()
        try:
            if doc.Open(fn)<0:
                doc.New()
        except:
            doc.New()
        
        prjDocNode=doc.getChild(doc.getRoot(),'prjdocs')
        if prjDocNode is None:
            prjDocNode=doc.getChildForced(doc.getRoot(),'prjdocs')
        prjId=self.doc.getKey(self.node)
        doc.setAttribute(prjDocNode,'prjfid',prjId)
        
        prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
        if prjInfoNode is None:
            prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
            #doc.checkId(prjInfoNode)
            
        sAttr=self.doc.getAttribute(self.node,'id')
        for k in ['name','longname','description']:
            doc.setNodeText(prjInfoNode,k,
                self.doc.getNodeText(self.node,k))
        doc.setAttribute(prjInfoNode,'fid',sAttr)
        attrNode=doc.getChild(self.node,'attributes')
        attrs=doc.getChilds(attrNode)
        dict={}
        for a in attrs:
            dict[self.doc.getTagName(a)]=a
        keys=dict.keys()
        keys.sort()
        for k in keys:
            a=dict[k]
            doc.setNodeText(prjInfoNode,k,
                self.doc.getText(a))
        
        node=doc.getChild(doc.getRoot(),'settings')
        if node is None:
            node=doc.getChildForced(doc.getRoot(),'settings')
        node=doc.getChild(doc.getRoot(),'config')
        if node is None:
            node=doc.getChildForced(doc.getRoot(),'config')
        
        doc.AlignDoc()
        
        doc.Save(fn,encoding=u'ISO-8859-1')#self.dom.encoding)#'ISO-8859-1'
        doc.Close()
        self.doc.setNodeText(self.node,'documentfn',fn)
        self.doc.AlignNode(self.node)
        self.bGenerated=True
        img=images.getApplyBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        self.gcbbCancel.SetLabel(u'Ok')
        self.gcbbCancel.Refresh()
        event.Skip()
    
    def OnGcbbBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.txtPrjDocFN.GetValue()
            #fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if len(fn)>0:
                dlg.SetFilename(fn)
            else:
                sShort=self.txtPrjShort.GetValue()
                sClient=self.txtClient.GetValue()
                dlg.SetFilename('prjdoc_%s%s'%(sClient,sShort))
            #else:
            #    dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtPrjDocFN.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
