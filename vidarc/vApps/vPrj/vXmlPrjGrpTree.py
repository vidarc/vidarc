#----------------------------------------------------------------------------
# Name:         vXmlPrjGrpTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlPrjGrpTree.py,v 1.11 2010/03/03 02:17:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.tool.xml.vtXmlGrpTreeReg import *
#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
#from vidarc.vApps.vPrj.vPrjXmlPrjEditDialog import *
#from wxPython.wx import wxPostEvent

import vidarc.tool.log.vtLog as vtLog

import images

VERBOSE=1

ATTRS=['prjnumber','manager','client','clientshort','reference',
                'ordernr','orderdate','facility']

class vXmlPrjGrpTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=style,
              master=master,controller=controller,verbose=verbose)
        self.prjAttr=ATTRS
        #self.SetNodeInfos(['|id','name','startdatetime','prjnumber',
        #        'cltshort'])
        self.bGroupingDict=True
        self.bGrpMenu=True
        self.bGrouping=True
        #self.iGrp=1
        return
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [('closed','clsd'),('cltshort','')] , 
                            {None:[('name','')],
                            }),
                ('flat'   , _(u'flat'), [] , 
                            {None:[('prjnumber','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('client'   , _(u'client'), [('cltshort',''),] , 
                            {None:[('name',''),('prjnumber','',)],
                            }),
                ])
        
        #self.grpMenu=[
        #    ('normal',_(u'normal'),
                #[('person',''),('date','YYYY'),('date','MM'),('date','DD')],
        #        [],
        #        [('prjnumber',''),('name','')]
                #[('attributes:prjnumber',''),('name','')]
        #        ),
        #    ('client',_(u'client'),
                #[('person',''),('date','YYYY'),('date','MM'),('date','DD')],
        #        [('cltshort','')],
                #[('attributes:clientshort','')],
        #        [('name','')]
        #        ),
        #    ]
        #self.verbose=verbose
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['|id','name','startdatetime','prjnumber',
                'cltshort','closed'])
    def SetDftGroupingOld(self):
        self.grouping=[('attributes:clientshort','')]
        self.label=[('name','')]
        self.bGrouping=True
    def SetupImageList(self):
        if vtXmlGrpTreeReg.SetupImageList(self):
            img=images.getPluginBitmap()
            self.imgDict['elem']['projects']=[self.imgLstTyp.Add(img)]
            self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
            img=images.getPluginBitmap()
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPluginImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPluginImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPluginImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[4])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPluginImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[5])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            
            img=images.getPrjBitmap()
            self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
            img=images.getPrjSelBitmap()
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[5])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            
            
            img=images.getUserBitmap()
            mask=wx.Mask(img,wx.BLACK)
            img.SetMask(mask)
            self.imgDict['grp']['manager']=[self.imgLstTyp.Add(img)]
            
            img=images.getClientBitmap()
            self.imgDict['grp']['CltShort']=[self.imgLstTyp.Add(img)]
            self.imgDict['grp']['CltShort'].append(self.imgLstTyp.Add(img))
            #self.imgDict['grp']['attributes:clientshort'].append(self.imgLstTyp.Add(img))
            
            img=images.getStaplerBitmap()
            self.imgDict['grp']['Closed']=[self.imgLstTyp.Add(img)]
            self.imgDict['grp']['Closed'].append(self.imgLstTyp.Add(img))
            
            self.SetImageList(self.imgLstTyp)
    def __getValFormat__(self,g,val,infos=None):
        try:
            if g[1]=='clsd':
                if val:
                    return _('closed')
                if val in ['1','True']:
                    return _('closed')
                else:
                    return None
        except:
            pass
        return vtXmlGrpTreeReg.__getValFormat__(self,g,val,infos)
    def __getValFormatS__(self,g,val,infos=None):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    val=val[:4]
                elif sFormat=='MM':
                    val=val[4:6]
                elif sFormat=='DD':
                    val=val[6:8]
                elif sFormat=='YYYY-MM-DD':
                    val=val[:4]+'-'+val[4:6]+'-'+val[6:8]
                elif sFormat=='MM-DD':
                    val=val[4:6]+'-'+val[6:8]
            if g[0]=='starttime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
            if g[0]=='endtime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
                
        if len(val)<=0:
            val="---"
        return val
    def __getImgFormat__(self,g,val):
        try:
            if g[0]=='Closed':
                if val in ['1','True']:
                    img=self.imgDict['grp']['Closed'][0]
                    imgSel=self.imgDict['grp']['Closed'][1]
                    return (img,imgSel)
        except:
            pass
        return vtXmlGrpTreeReg.__getImgFormat__(self,g,val)
    def __getImgFormatS__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='person':
            #if sFormat=='':
            img=self.imgDict['grp']['person'][0]
            imgSel=self.imgDict['grp']['person'][0]
            return (img,imgSel)
        elif g[0]=='attributes:clientshort':
            #if sFormat=='':
            img=self.imgDict['grp']['attributes:clientshort'][0]
            imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return (img,imgSel)
        vtLog.CallStack(g)
        tagName=g[0]#self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)
    def __getImagesByInfosS__(self,tagName,infos):
        vtLog.CallStack(tagName)
        vtLog.pprint(self.imgDict)
        img,imgSel=vtXmlGrpTreeReg.__getImagesByInfos__(self,tagName,infos)
        return img,imgSel
    def __addProjects__(self,parent,o):
        tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        tid=wx.TreeItemData()
        tid.SetData(None)
        tn=self.AppendItem(parent,tagName,-1,-1,tid)
        self.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,
                                wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,
                                wx.TreeItemIcon_Selected)
        #if o.tagName=='projtimes':
        #    self.tnProjTimes=tn
        self.tiRoot=tn
        self.__addElements__(tn,o)

    def __addElementsOld__(self,parent,obj):
        childs=self.doc.getChilds(obj)
        for o in childs:
            tagName=self.doc.getTagName(o)
            bBlockRec=False
            if tagName in ['project']:
                #self.ids.CheckId(o)
                tid=wx.TreeItemData()
                tid.SetData(o)
                bBlockRec=True
                
                self.__addElement__(parent,o)
            elif tagName=='projects':
                self.__addProjects__(parent,o)
            else:
                tid=wx.TreeItemData()
                tid.SetData(None)
            
        self.SortChildren(parent)
    def OnGotContent(self,evt):
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),origin=self.GetName())
        self.SetNode(None)
        evt.Skip()
    def OnAddNodeOld(self,evt):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(self,vtLog.INFO,
                'OnAddNode;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                origin=self.GetName())
        if VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),origin=self.GetName())
        idPar=evt.GetID()
        idNew=evt.GetNewID()
        child=self.doc.getNodeById(idNew)
        tagName=self.doc.getTagName(child)
        tup=self.__findNodeByID__(None,idPar)
        if (tup is not None) and (child is not None):
            ti=self.__addElement__(tup[0],child,True)
            self.SortChildren(tup[0])
        evt.Skip()
    
    #def SetNode(self,node):
    #    vgtrXmlTreeGrp.SetNode(self,node)
    #    wxPostEvent(self,wxThreadAddElementsFinished())
    #    self.Expand(self.tiRoot)
    def __addMenuEditO__(self,menu):
        if self.bMaster:
            
            mnIt=wx.MenuItem(menu,self.popupIdAdd,_(u'Add'))
            mnIt.SetBitmap(images_treemenu.getAddBitmap())
            menu.AppendItem(mnIt)
            #nodePar=self.objTreeItemSel
            #if self.doc is not None:
            #    if not self.doc.hasPossibleNode(nodePar):
            #        mnIt.Enable(False)
            
            #mnIt=wx.MenuItem(menu,self.popupIdEdit,_(u'Edit'))
            #mnIt.SetBitmap(images_treemenu.getEditBitmap())
            #menu.AppendItem(mnIt)
            
            mnIt=wx.MenuItem(menu,self.popupIdDel,_(u'Delete'))
            mnIt.SetBitmap(images_treemenu.getDelBitmap())
            menu.AppendItem(mnIt)
            if self.GetSelected() is None:      # 080427 wro:use method
                mnIt.Enable(False)
            menu.AppendSeparator()
            return True
        return False

    def OnTcNodeRightDownD(self, event):
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdAddPrj'):
            self.popupIdAddPrj=wx.NewId()
            #self.popupIdAddUsr=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAddPrj,self.OnTreeAddPrj)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDelPrj)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        menu=wx.Menu()
        menu.Append(self.popupIdAddPrj,'Add Project')
        #menu.Append(self.popupIdAddUsr,'Add User')
        menu.Append(self.popupIdDel,'Delete Node')
        menu.Append(self.popupIdEdit,'Edit')
        menu.Append(self.popupIdMove,'Move Node')
        
        self.PopupMenu(menu,(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def OnTreeAddOld(self,event):
        if self.rootNode is None:
            return -1
        prjsNode=self.doc.getBaseNode()#self.doc.getChild(None,'projects')
        if prjsNode is not None:
            self.AddProjectNode(prjsNode)
        pass
    def OnTreeDelOld(self,event):
        if self.rootNode is None:
            return -1
        if self.GetSelected() is None:          # 080427 wro: use method
            return
        self.doc.delNode(self.GetSelected())    # 080427 wro: use method
        self.Delete(self.triSelected)
        if self.tiCache is not None:
            self.tiCache.DelID(self.doc.getKey(self.GetSelected()))
        self.objTreeItemSel=None
        self.triSelected=None
    def OnTreeEditOld(self,event):
        s=self.GetItemText(self.triSelected)
        
        if string.find(s,"project:")==0:
            # open group edit dialog
            try:
                d=self.dlgEditPrj
            except:
                self.dlgEditPrj=vgdXmlDlgEditPrj(self)
                self.dlgEditPrj.SetDoc(self.doc)
            self.dlgEditPrj.SetNode(self.GetSelected())
            ret=self.dlgEditPrj.ShowModal()
            if ret==1:
                # process dialog ok
                s=self.dlgEditPrj.GetNode(self.GetSelected())
                self.SetItemText(self.triSelected,'project:'+s)
                wxPostEvent(self,vgpXmlTreeItemEdited(self,
                    self.triSelected,self.objTreeItemSel))  # forward double click event
        
            #dlg.Destroy()
    def AddProjectNode(self,node):
        #vtLog.CallStack('')
        prj=self.doc.createSubNode(node,'project')
        # append name
        prjName=self.doc.createSubNodeText(prj,'name','---')
        # append longname
        prjLongName=self.doc.createSubNodeText(prj,'longname','---')
        # append description
        prjDesc=self.doc.createSubNodeText(prj,'description','---')
        # append start date time
        prjStartDT=self.doc.createSubNodeText(prj,'startdatetime','--------_------')
        # append end date time
        prjEndDT=self.doc.createSubNodeText(prj,'enddatetime','--------_------')
        # append attributes
        prjAttrs=self.doc.createSubNode(prj,'attributes')
        for attr in self.prjAttr:
            prjAttr=self.doc.createSubNodeText(prjAttrs,attr,'---')
        self.doc.AlignNode(prj,iRec=2)
        self.doc.addNode(self.rootNode,prj,self.SetIdManual)
        
        #self.idManualAdd=self.doc.getKey(prj)
        #ti=self.__addElement__(self.tiRoot,prj,True)
        #self.SelectItem(ti)
        #self.__update_tree__()
        #self.triSelected=ti
        #self.objTreeItemSel=prj
        #wxPostEvent(self,vtXmlTreeItemAdded(self,
        #                    ti,prj,self.doc.getKey(self.rootNode)))
    def __update_tree__(self):
        # update tree
        triRoot=self.GetRootItem()
        triChild=self.GetFirstChild(triRoot)
        if self.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.GetNextChild(triChild[0],triChild[1])
            if self.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
        self.groupItems={}
        #self.DeleteChildren(triPar)
        self.DeleteChildren(triRoot)
        self.__addElements__(triRoot,self.rootNode)
        parTri=self.GetItemParent(triRoot)
        self.SortChildren(triRoot)
        #self.SelectItem(triPar)
        wx.PostEvent(self,vgtrXmlTreeItemAdded(self,
                            triPar,self.rootNode))
        return 0
