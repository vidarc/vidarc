#----------------------------------------------------------------------------
# Name:         vXmlPrj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlPrj.py,v 1.11 2007/10/30 07:26:11 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vPrj.vXmlNodePrjRoot import vXmlNodePrjRoot
from vidarc.vApps.vPrj.vXmlNodePrjDepartment import vXmlNodePrjDepartment
from vidarc.vApps.vPrj.vXmlNodePrj import vXmlNodePrj
from vidarc.vApps.vPrj.vXmlNodePrjAttr import vXmlNodePrjAttr
#from vidarc.vApps.vPrj.vXmlNodePrjAttrOld import vXmlNodePrjAttrOld

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition

VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDAT8\x8d\xcd\x93\xc1\n\x80 \x10D\xdf\xd6\x07\x1bt\x97\xe8.\xd4\x17\
\xdb\xc9\x10\xdd\x8d\xb4\xa0\xdeq\xd0q\\fE\x86\x91\x12\xe7C\xacD`_g)\xb5\xe1\
\xeeAM\x03\x90\x94\xc0z\xd5"\x19\xaa\tZxn\xd0\x1a=\xc7\xf9\x10eZ\xb6n\x03\
\xf8\xc5\x0c^1\xb0Jr\xc5\xd9\x83\x9e\xcb\xb9\xc9\xf73\x10m\x1b\xa1\xde\r\xeb\
\xabj\x02\xad\x9dVc\x0f\xa0T!\xbct\xaaL\xcd\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlPrj(vtXmlDomReg):
    TAGNAME_REFERENCE='name'
    TAGNAME_ROOT='proj'
    APPL_REF=['vContact','vHum','vGlobals','vMsg']
    def __init__(self,attr='id',skip=[],synch=False,appl='vPrj',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,
                            verbose=verbose,audit_trail=audit_trail)
        self.verbose=verbose
        try:
            oRoot=vXmlNodePrjRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodePrjRoot()
            oPrj=vXmlNodePrj()
            oPrjDep=vXmlNodePrjDepartment()
            oPrjAttr=vXmlNodePrjAttr()
            #oPrjAttrOld=vXmlNodePrjAttrOld()
            oCfg=vtXmlNodeCfg()
            oLog=vtXmlNodeLog()
            
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oPrjDep,True)
            self.RegisterNode(oPrj,True)
            self.RegisterNode(oPrjAttr,False)
            #self.RegisterNode(oPrjAttrOld,False)
            
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            self.LinkRegisteredNode(oRoot,oPrjDep)
            self.LinkRegisteredNode(oRoot,oPrj)
            self.LinkRegisteredNode(oPrjDep,oPrjDep)
            self.LinkRegisteredNode(oPrjDep,oPrj)
            self.LinkRegisteredNode(oPrj,oPrjAttr)
            self.LinkRegisteredNode(oPrj,oLog)
            #self.LinkRegisteredNode(oPrj,oPrjAttrOld)
            
            vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
            flt={}#{None:[('GetSum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                    #    ('orderDt',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    #    ('deliveryDt',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    #    ]}
            oTransPrj=self.GetReg('transPrj',bLogFlt=False)
            if oTransPrj is None:
                oTransPrj=veStateTransition(tagName='transPrj',filter=flt)
                self.RegisterNode(oTransPrj,False)
            oSMprj=self.GetReg('smPrj',bLogFlt=False)
            if oSMprj is None:
                oSMprj=veStateStateMachine(tagName='smPrj',
                        tagNameState='state',tagNameTrans='transPrj',
                        filter=flt,
                        trans={None:{None:_(u'project'),
                                #'GetSum':_(u'sum'),
                                #'orderDt':_(u'ordering date'),
                                #'deliveryDt':_(u'delivery date'),
                                } },
                        cmd=oPrj.GetActionCmdDict())
            oStateMachineLinker=self.GetReg('stateMachineLinker',bLogFlt=False)
            if oStateMachineLinker is not None:
                oStateMachineLinker.AddPossible({
                        'project':[('prjState','smPrj')],
                        })
                self.RegisterNode(oSMprj,False)
                self.LinkRegisteredNode(oStateMachineLinker,oSMprj)
                oState=self.GetReg('state')
                self.LinkRegisteredNode(oSMprj,oState)
            else:
                vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)            
        except:
            vtLog.vtLngTB(self.appl)
        self.UpdateCmdDict()
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'projects')
    #def New(self,revision='1.0',root='proj',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elemRoot=self.createSubNode(self.getRoot(),'projects')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    elem=self.createSubNode(elemRoot,'cfg')
        #    self.checkId(elem)
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def GetPrj(self,id):
        if self.IsKeyValid(id)==False:
            return None
        for c in self.getChilds(self.getBaseNode(),'project'):
            if self.getAttribute(c,'id')==id:
                return c
        return None
    def GetPrjName(self,node):
        if node is None:
            return u''
        return self.getNodeText(node,'name')
    def GetCltShort(self,node):
        if node is None:
            return u''
        if self.getTagName(node)!='project':
            return u''
        return self.getNodeText(node,'cltshort')
        #oReg=self.GetReg('project')
        #return oReg.GetCltShort(node)
        #nodeAttr=self.getChild(node,'attributes')
        #return self.getNodeText(nodeAttr,'clientshort')
    def GetCltLong(self,node):
        if node is None:
            return u''
        if self.getTagName(node)!='project':
            return u''
        return self.getNodeText(node,'cltlong')
        #oReg=self.GetReg('project')
        #return oReg.GetCltShort(node)
        #nodeAttr=self.getChild(node,'attributes')
        #return self.getNodeText(nodeAttr,'client')
    def GetPrjNameLong(self,node):
        if node is None:
            return u''
        return self.getNodeText(node,'longname')
    def GetFacility(self,node):
        if node is None:
            return u''
        #nodeAttr=self.getChild(node,'attributes')
        nodeAttr=self.getChild(node,'prjAttr')
        return self.getNodeText(nodeAttr,'facility')
    def GetPrjNumber(self,node):
        if node is None:
            return u''
        return self.getNodeText(node,'prjnumber')
        #nodeAttr=self.getChild(node,'attributes')
        #return self.getNodeText(nodeAttr,'prjnumber')
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL,
                'project':self.ACL_MSK_NORMAL,
                },['all','project']
    def GetPossibleFilterOld(self):
        return {'project':[
                            ('name',self.FILTER_TYPE_STRING),
                            ('longname',self.FILTER_TYPE_STRING),
                            ('startdatetime',self.FILTER_TYPE_DATE_TIME),
                            ('enddatetime',self.FILTER_TYPE_DATE_TIME),
                            ('attributes:client',self.FILTER_TYPE_STRING),
                            ('attributes:clientshort',self.FILTER_TYPE_STRING),
                            ('attributes:reference',self.FILTER_TYPE_STRING),
                            ('attributes:facility',self.FILTER_TYPE_STRING),
                            ('attributes:orderdate',self.FILTER_TYPE_DATE)]}
    def GetTranslationOld(self,kind,name):
        _(u'name'),_(u'longname'),_(u'startdatetime'),_(u'enddatetime')
        _(u'attributes:client'),_(u'attributes:clientshort'),_(u'attributes:reference')
        _(u'attributes:facility'),_(u'attributes:orderdate')
        
        return _(name)
    def Build(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        vtXmlDomReg.Build(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        self.BuildLang()
    def BuildLang(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        vtXmlDomReg.BuildLang(self)
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def __initCmdDictOld__(self):
        vtXmlDomReg.__initCmdDict__(self)
        self.CMD_DICT.update({
            'genPrjDoc':(_(u'generate vPrjDoc alias'),0x206,None),
            'genPrjEng':(_(u'generate vPrjEng alias'),0x206,None),
            'genPrjPlan':(_(u'generate vPrjPlan alias'),0x206,None),
            })
    def DoCmd(self,action,**kwargs):
        """ return
            1       ... action executed correctly
            0       ... action unknown
           -1       ... fault occured during execution 
           -2       ... permission denied
           -3       ... action execution in process
        """
        try:
            if action.find(':')<0:
                ret=vtXmlDomReg.DoCmd(self,action,**kwargs)
                if ret!=0:
                    return ret
            else:
                strs=action.split(':')
                oReg=self.GetReg(strs[0])
                if oReg is not None:
                    return oReg.DoCmd(action,**kwargs)
            return 0
        except:
            vtLog.vtLngCur(vtLog.ERROR,'action:%s;kwargs:%s'%(action,vtLog.pformat(kwargs)),self.appl)
            vtLog.vtLngTB(self.appl)
            return -1
