
import wx
import images_data

import vidarc.vApps.vPrj.vPrjInfoGeneralPanel as vPrjInfoGeneralPanel
import vidarc.vApps.vPrj.vPrjInfoAttrPanel as vPrjInfoAttrPanel
#import vidarc.vApps.vPrj.vgpUserInfoAttr as vgpUserInfoAttr
#import vidarc.vApps.vPrj.vgpUserInfoAccess as vgpUserInfoAccess
#import vidarc.vApps.vPrj.vgpRessourceTimes as vgpRessourceTimes
import types
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0

class vPrjInfoNoteBook(wx.Notebook):
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)

    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        self._init_ctrls(parent,id, pos, size, style, name)
        
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGING, self.OnPageChanging)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        
        self.verbose=VERBOSE
        
        edit=[u'PrjInfoGeneral',u'PrjInfoAttr']
        editName=[u'Info',u'Attr']
        il = wx.ImageList(32, 32)
        for e in edit:
            s='v%sPanel'%e
            f=getattr(images_data, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='v%sPanel'%e
            f=getattr(eval('v%sPanel'%e),'v%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
        panel=self.lstPanel[0]
        #vgpPrjInfo.EVT_VGPPROJECT_INFO_APPLY(panel,self.OnPrjInfoApply)
        #vgpPrjInfo.EVT_VGPPROJECT_INFO_CANCEL(panel,self.OnPrjInfoCancel)
        
    def OnPrjInfoApply(self,evt):
        print 'apply'
        for pn in self.lstPanel:
            pn.GetNode()
        wx.PostEvent(self,vPrjInfoPanel.vPrjInfoPanelApply(self,self.node))
        evt.Skip()
    def OnPrjInfoCancel(self,evt):
        for pn in self.lstPanel:
            pn.SetNode(self.node)
        wx.PostEvent(self,vPrjInfoPanel.vPrjInfoPanelCancel(self,self.node))
        evt.Skip()
    def Clear(self):
        for pn in self.lstPanel:
            pn.Clear()
    def Lock(self,flag):
        for pn in self.lstPanel:
            pn.Lock(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    
    def SetDoc(self,doc,bNet=False):
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDoc(doc,bNet)
    def SetDocCustomer(self,doc,bNet=False):
        self.lstPanel[1].SetDocCustomer(doc,bNet)
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
        
    def SetNode(self,node):
        if self.__isModified__():
            # ask
            dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
                        u'vgaHum User',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode()
                #wx.PostEvent(self,vgpDataChanged(self,self.node))
        self.SetModified(False)
        self.node=node
        for pn in self.lstPanel:
            pn.SetNode(node)
    def GetNode(self):
        for pn in self.lstPanel:
            pn.GetNode()
            
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = self.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
    def OnSize(self,evt):
        #print 'onsize',evt.GetSize()
        evt.Skip()