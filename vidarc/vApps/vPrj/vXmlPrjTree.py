
#----------------------------------------------------------------------------
# Name:         vXmlPrjTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vXmlPrjTree.py,v 1.7 2008/04/28 11:49:50 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlTree import *
#import vidarc.vApps.xmlBase.vtXmlTree as vtXmlTree
from vidarc.vApps.vPrj.vPrjXmlPrjEditDialog import *
#from wxPython.wx import wx.PostEvent

import images


class vXmlPrjTree(vtXmlTree):
    
    def SetupImageList(self):
        self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=images.getPluginBitmap()
        self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
        self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
        img=images.getPluginBitmap()
        self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
        self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
        
        img=images.getPrjsBitmap()
        self.imgDict['elem']['projects']=[self.imgLstTyp.Add(img)]
        img=images.getPrjsSelBitmap()
        self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
        
        img=images.getTextBitmap()
        #img=images.getElementBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
        
        img=images.getTextSelBitmap()
        #img=images.getElementSelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
        #icon=images.getApplicationIcon()
        #self.SetIcon(icon)
        self.tcNode.SetImageList(self.imgLstTyp)
    
    def __addElements__(self,parent,obj):
        for o in obj.childNodes:
            if o.nodeType == Element.ELEMENT_NODE:
                #tn=self.treeCtrl1.AppendItem(parent,o.tagName,-1,-1,o)
                tid=wxTreeItemData()
                tid.SetData(o)
                try:
                    img=self.imgDict['elem'][o.tagName][0]
                except:
                    img=self.imgDict['elem']['dft'][0]
                try:
                    imgSel=self.imgDict['elem'][o.tagName][1]
                except:
                    imgSel=self.imgDict['elem']['dft'][1]
                s=o.tagName
                bBlockRec=False
                if o.tagName in ['project','user']:
                    bBlockRec=True
                    #sGrpName=vtXmlDomTree.getNodeText(node,'group')
                    #if len(sGrpName)>0:
                    #    s=s+":"+sGrpName
                    names=o.getElementsByTagName('name')
                    if len(names)>0:
                        try:
                            namesChild=names[0].childNodes[0]
                            if namesChild.nodeType==Element.TEXT_NODE:
                                s=s+":"+namesChild.nodeValue
                        except:
                            pass
                tn=self.tcNode.AppendItem(parent,s,-1,-1,tid)
                self.tcNode.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
                self.tcNode.SetItemImage(tn,imgSel,
                                        wx.TreeItemIcon_Expanded)
                self.tcNode.SetItemImage(tn,imgSel,
                                        wx.TreeItemIcon_Selected)
                if bBlockRec==False:
                    if len(o.childNodes)>0:
                        self.__addElements__(tn,o)
            elif o.nodeType == Element.TEXT_NODE:
                s=string.strip(o.nodeValue)
                if len(s)>2:
                    tid=wxTreeItemData()
                    tid.SetData(o)
                    try:
                        img=self.imgDict['txt'][o.nodeValue][0]
                    except:
                        img=self.imgDict['txt']['dft'][0]
                    try:
                        imgSel=self.imgDict['txt'][o.nodeValue][1]
                    except:
                        imgSel=self.imgDict['txt']['dft'][1]
                    tn=self.tcNode.AppendItem(parent,o.nodeValue,-1,-1,tid)
                    self.tcNode.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
                    self.tcNode.SetItemImage(tn,imgSel,
                                        wx.TreeItemIcon_Expanded)
                    self.tcNode.SetItemImage(tn,imgSel,
                                        wx.TreeItemIcon_Selected)
    def OnTcNodeRightDown(self, event):
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdAddPrj'):
            self.popupIdAddPrj=wxNewId()
            #self.popupIdAddUsr=wxNewId()
            self.popupIdDel=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdMove=wx.NewId()
            EVT_MENU(self,self.popupIdAddPrj,self.OnTreeAddPrj)
            #EVT_MENU(self,self.popupIdAddUsr,self.OnTreeAddUsr)
            EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
        menu=wx.Menu()
        menu.Append(self.popupIdAddPrj,'Add Project')
        #menu.Append(self.popupIdAddUsr,'Add User')
        menu.Append(self.popupIdDel,'Delete Node')
        menu.Append(self.popupIdEdit,'Edit')
        menu.Append(self.popupIdMove,'Move Node')
        
        self.PopupMenu(menu,(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def OnTreeAddPrj(self,event):
        if self.rootNode is None:
            return -1
        #doc=vtXmlDomTree.__getDocument__(self.rootNode)
        #prjs=self.rootNode.getElementsByTagName('projects')
        #if len(prjs)>0:
        #    self.AddProjectNode(prjs[0])
        self.AddProjectNode(self.doc.getBaseNode())
        pass
    def RefreshSelected(self):
        s=self.tcNode.GetItemText(self.triSelected)
        if string.find(s,"project:")==0:
            # process dialog ok
            s=vtXmlDomTree.getNodeText(self.GetSelected(),'name')       # 080427 wro: use method
            self.tcNode.SetItemText(self.triSelected,'project:'+s)
            wx.PostEvent(self,vgpXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        
        
    def OnTreeEdit(self,event):
        s=self.tcNode.GetItemText(self.triSelected)
        if string.find(s,"project:")==0:
            # open group edit dialog
            try:
                d=self.dlgEditPrj
            except:
                self.dlgEditPrj=vgdXmlDlgEditPrj(self)
            self.dlgEditPrj.SetNode(self.GetSelected())             # 080427 wro: use method
            ret=self.dlgEditPrj.ShowModal()
            if ret==1:
                # process dialog ok
                s=self.dlgEditPrj.GetNode(self.GetSelected())       # 080427 wro: use method
                self.tcNode.SetItemText(self.triSelected,'project:'+s)
                wx.PostEvent(self,vgpXmlTreeItemEdited(self,
                    self.triSelected,self.objTreeItemSel))  # forward double click event
        
            #dlg.Destroy()
    def AddProjectNode(self,node):
        doc=vtXmlDomTree.__getDocument__(node)
        prj=doc.createElement('project')
        node.appendChild(prj)
        # append name
        prjName=doc.createElement('name')
        prjName.appendChild(doc.createTextNode('???'))
        prj.appendChild(prjName)
        # append longname
        prjLongName=doc.createElement('longname')
        prjLongName.appendChild(doc.createCDATASection('???'))
        prj.appendChild(prjLongName)
        # append description
        prjDesc=doc.createElement('description')
        prjDesc.appendChild(doc.createCDATASection('???'))
        prj.appendChild(prjDesc)
        # append start date time
        prjStartDT=doc.createElement('startdatetime')
        prjStartDT.appendChild(doc.createTextNode('???'))
        prj.appendChild(prjStartDT)
        # append end date time
        prjEndDT=doc.createElement('enddatetime')
        prjEndDT.appendChild(doc.createTextNode('???'))
        prj.appendChild(prjEndDT)
        # append attributes
        prjAttrs=doc.createElement('attributes')
        for attr in ['manager','client','prjnumber']:
            prjAttr=doc.createElement(attr)
            prjAttr.appendChild(doc.createTextNode('---'))
            prjAttrs.appendChild(prjAttr)
        prj.appendChild(prjAttrs)
        
        # update tree
        triRoot=self.tcNode.GetRootItem()
        triChild=self.tcNode.GetFirstChild(triRoot)
        if self.tcNode.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.tcNode.GetNextChild(triChild[0],triChild[1])
            if self.tcNode.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
                
        self.tcNode.DeleteChildren(triPar)
        self.__addElements__(triPar,node)
        self.tcNode.Expand(triPar)
        wx.PostEvent(self,vgpXmlTreeItemAdded(self,
                            triPar,node))
        return 0
