#----------------------------------------------------------------------------
# Name:         vXmlNodePrj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060719
# CVS-ID:       $Id: vXmlNodePrj.py,v 1.8 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.ext.state.veStateAttr import veStateAttr

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vPrjInfoGeneralPanel import *
        #from vXmlNodePrjEditDialog import *
        #from vXmlNodePrjAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrj(vtXmlNodeBase,veStateAttr):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('LongName',None,'longname',None),
            ('Desc',None,'description',None),
            #('StartDT',None,'startdatetime',None),
            #('EndDT',None,'enddatetime',None),
        ]
    FUNCS_GET_SET_4_LST=['Name','LongName','Closed','CltShort']
    FUNCS_GET_4_TREE=['Name','LongName','CltShort','Closed']
    FMT_GET_4_TREE=[('Name',''),('LongName',''),('CltShort','')]
    GRP_GET_4_TREE=[('Closed','clsd'),('CltShort','')]
    def __init__(self,tagName='project'):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        vtXmlNodeBase.__init__(self,tagName)
        veStateAttr.__init__(self,'smPrj','prjState')
        self.oSMlinker=None
        self.GRP_MENU_4_TREE=[
                    ('normal'   , _(u'normal'), 
                                [('Closed','clsd'),('CltShort','')] , 
                                [('Name',''),('LongName',''),('CltShort','')], ),
                    ('flat'   , _(u'flat'),
                                [] , 
                                [('PrjNumber','',),('Name','')], ),
                    ('client'   , _(u'client'), 
                                [('CltShort',''),] , 
                                [('Name',''),('PrjNumber','',)],),
                    ]
        
    def GetDescription(self):
        return _(u'project')
    # ---------------------------------------------------------
    # specific
    def GetPrjNumber(self,node):
        return self.Get(node,'prjnumber')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetLongName(self,node,lang=None):
        return self.GetML(node,'longname',lang)
    def GetDesc(self,node):
        return self.Get(node,'description')
    def GetClient(self,node):
        return self.GetChildAttrStr(node,'client','fid')
    def GetCltShort(self,node):
        return self.Get(node,'client')
        fid=self.GetClient(node)
        netDoc,netNode=self.doc.GetNode(fid)
        if netDoc is None:
            return '???'
        if netNode is None:
            return '???'
        oNet=netDoc.GetRegByNode(netNode)
        return oNet.GetTag(netNode)
    def GetClosed(self,node):
        sVal=self.Get(node,'closed')
        return sVal in ['1','True']
    
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def SetLongName(self,node,val):
        self.Set(node,'longname',val)
    def SetDesc(self,node,val):
        self.Set(node,'description',val)
    def SetClient(self,node,val):
        self.SetChildAttrStr(node,'client','fid',val)
    def SetClosed(self,node,val):
        if val:
            sVal='1'
        else:
            sVal='0'
        self.Set(node,'closed',sVal)
    # ---------------------------------------------------------
    # state machine related
    def GetActionCmdDict(self):
        #return {
        #    'Clr':(_('clear'),None),
        #    'Go':(_('go on vacation'),None),
        #    'NoGo':(_('do not go on vacation'),None),
        #    }
        return {}
    def GetMsgTag(self,node,lang=None):
        return self.GetPrjNumber(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        return ''
    
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('longname',vtXmlFilterType.FILTER_TYPE_STRING),
            ('description',vtXmlFilterType.FILTER_TYPE_STRING),
            ('startdatetime',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
            ('enddatetime',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
        ]
    def GetTranslation(self,name):
        _(u'name'),_(u'longname'),_(u'description'),_(u'startdatetime')
        _(u'enddatetime')
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd3IDAT8\x8d}\x92\xcbk\x13Q\x18\xc5\x7f3\xed*\x05\x11\x8cJ\x9bi@\
\xe8\xac\x94.\x84\x0c(\xd8\\\xe8B\x8c\xe0\x03|\xadJ]\x97R\xdc\xd4U\xc9\xcc\
\x9f\xe0\x03Z\x08\x85V\\\xf8\xd8\'d\x11\xbd\xa1(\x92\x16DT\x04\'"\xa5D$YX\
\xf0\x81\xb50\x9f\x8bI\xc6\xd8Iz\xe0\xc0\xe5~\xf7\xfc\xee\xe1r1\xcc\x01z\x19\
\xb4(\xa5\x04\xb4\xf4;c\x98\x03\xf4\rk\x8d\x88 Z\xb3/h\xdfp\xb7\xb5F\xd4q\
\x15\x83\xc4\x00\xae\xabDD\xc5\x00\xa2\x11\xf7*1\x80\xc9^\xbd\xd7P\x05Z.\xa0\
\xc2\xbd*\xd0\x8c\x9d\x04`\xb0\xb3\x90\xa0"\x8e\xa3\x01\x17\x9a:\xf4\x11\x15\
B\x9a\xbaw\x1a\xc2\x06\x12Td\xf5\x01\xac\xac\xe6\xff\x9fv@]\xca\x0e\xdf\x10\
\t*\x12\x01:\xe1L&\xdb\xf7\x96H\xdb)\x9e_8\x88\xbex\x87\xb1\xe0\x8c\x00\x98\
\xf3\xf3\x1a\x95\xfd\x17n\x1du\xf1\x9e\xc6\xb3^!E~\xec\x12\x1c:\xc6\xceF\x8d\
\xd3\x84\xcd\xcc\xcd7\xb0Y\x83!\x13\x86\x1203\x93\xe7\xad\xdc\xc2\xb86\x11\
\x81\xbc\x87\'\xc8\x8f\xdf\x04\xeb$/\x1e\xad\xf0\xa7\xd1\xe0W\x1b<\x08\xd0\
\xda\n}x\x14^~X\xe6\xcb\xd7W\xc00\xb558\xbf\x06\x85I\x1b\xac$\x9f\xd7\x9f\
\xd1\xa8\xbf#\xb1\xf7\x11\xa3\xfam\x10\xc0h\xfa\x07\x00\x85\xc9\xcb\x8c\x1c\
\x18\x81\x8f>\xf8>\x00\t\xe0gw\x83^\xb2\xd2\xdfqv\x9c0\xdc\xa5\x04\x84\xf5m\
\x9b{\xb3sb>.\xc3\x93\xb2\x17\x03\xec\xfe\xde\xa5\x96L\xe2m\xac\xc7f\x8b\xb6\
Mnv\x0e\xbf\x9e\xc2\x08\xff?H\xb0 \xd7\xcf\x82\x95\xb6\xa8\xbe^bzj\x1a\x80b\
\xc9\xa1X*\xe2f\x8aL\x01\xb7\xb7\xbf\x91m\x87\xef\xde\xbfbD\x80\x8e$X\x90\
\xdc\xb9\x1c\x00~=E\xfdS\xda\xe85+\x95O\x19\x00\x7f\x01V\xe6\xd7\xed\xe5\x9e\
R\x08\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            #return vXmlNodePrjEditDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnPrj',
                }
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            #return vXmlNodePrjAddDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnPrj',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vPrjInfoGeneralPanel
        else:
            return None
    def GetCmdDict(self):
        dArgs={
            '|id':(_(u'id'),'long',{
                    'readonly':True,
                    }),
            'client':(_(u'client short'),'string',{
                    'readonly':True,
                    }),
            'name':(_(u'project short'),'string',{
                    'readonly':True,
                    }),
            #'dest':(_(u'project short'),'string',{
            #        'readonly':True,
            #        }),
            'dest':(_(u'destination'),'string',None),
            'destFN':(_(u'destination filename'),'string',None),
            'source':(_(u'source'),'string',{'default':'tmpl'}),
            'alias':(_(u'alias'),'string',{'default':''}),
            }
        return {
            'genEngineering':(_(u'generate vEngineering alias'),0x206,dArgs),
            'genPrjDoc':(_(u'generate vPrjDoc alias'),0x206,dArgs),
            #'genPrjEng':(_(u'generate vPrjEng alias'),0x206,dArgs),
            'genPrjPlan':(_(u'generate vPrjPlan alias'),0x206,dArgs),
            }
    def DoCmd(self,node,srv,action,**kwargs):
        """ return
            1       ... action executed correctly
            0       ... action unknown
           -1       ... fault occured during execution 
           -2       ... permission denied
           -3       ... action execution in process
        """
        try:
            if srv is None:
                return -4
            applAlias=self.doc.GetApplName()
            #print self.doc.
            #applAlias=':'.join([])
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'action:%s;kwargs:%s'%(action,vtLog.pformat(kwargs)),applAlias)
            sAppl=''
            sClt=self.GetCltShort(node)
            bAliasGen=False
            if 'alias' not in kwargs:
                bAliasGen=True
            else:
                sAlias=kwargs['alias']
                if len(sAlias)==0:
                    bAliasGen=True
            if bAliasGen:
                sAlias='_'.join([sClt,self.GetName(node)])
            
            fn=''
            sAliasTmp=self.doc.getNodeText(node,'alias')
            if len(sAliasTmp)>0:
                sAlias=sAliasTmp
            if action=='genPrjDoc':
                fn=self.doc.getNodeText(node,'documentfn')
                sAppl='vPrjDoc'
                applAliasDest=':'.join([sAppl,sAlias])
                #return 1
            elif action=='genPrjEng':
                fn=self.doc.getNodeText(node,'engineeringfn')
                sAppl='vPrjEng'
                applAliasDest=':'.join([sAppl,sAlias])
                #return 1
            elif action=='genEngineering':
                fn=self.doc.getNodeText(node,'engineeringfn')
                sAppl='vEngineering'
                applAliasDest=':'.join([sAppl,sAlias])
                #return 1
            elif action=='genPrjPlan':
                fn=self.doc.getNodeText(node,'planningfn')
                sAppl='vPrjPlan'
                applAliasDest=':'.join([sAppl,sAlias])
                #return 1
            else:
                return 0
            doc=srv.GetServedXmlDom(applAliasDest)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;aliasDest:%s'%(fn,applAliasDest),applAlias)
            bClone=False
            if doc is None:
                # cloneAlias
                bClone=True
            if bClone:
                kkwargs={}
                if len(fn)==0:
                    fn='/'.join(['.',sClt,applAliasDest.replace(':','_')])
                if 'source' not in kwargs:
                    kkwargs['source']='tmpl'
                else:
                    kkwargs['source']=kwargs['source']
                    try:
                        if len(kkwargs['source'])==0:
                            kkwargs['source']='tmpl'
                    except:
                        kkwargs['source']='tmpl'
                #return 1
                kkwargs['destFN']=fn
                kkwargs['dest']=sAlias
                
                if srv.__cloneAlias__(sAppl,**kkwargs)<0:
                    return -1
                vtLog.vtLngCur(vtLog.INFO,'aliasDest:%s;cloned'%(applAliasDest),applAlias)
                kkkargs={'source':kkwargs['dest']}
                iRet=srv.__start__(sAppl,**kkkargs)
                vtLog.vtLngCur(vtLog.INFO,'aliasDest:%s;started %d'%(applAliasDest,iRet),applAlias)
            doc=srv.GetServedXmlDom(applAliasDest)
            if doc is None:
                return -1
            
            if action=='genPrjDoc':
                # update vPrjDoc alias
                self.doc.setNodeText(node,'documentfn',fn)
                self.doc.setNodeText(node,'alias',sAlias)
                nodeBase=doc.getChild(doc.getRoot(),'prjdocs')
                if nodeBase is None:
                    nodeBase=doc.getChildForced(doc.getRoot(),'prjdocs')
                prjId=self.doc.getKey(node)
                srv.Update(applAlias,node)
            if action=='genPrjEng':
                # update vPrjDoc alias
                self.doc.setNodeText(node,'engineeringfn',fn)
                self.doc.setNodeText(node,'alias',sAlias)
                nodeBase=doc.getChild(doc.getRoot(),'prjengs')
                if nodeBase is None:
                    nodeBase=doc.getChildForced(doc.getRoot(),'prjengs')
                prjId=self.doc.getKey(node)
                srv.Update(applAlias,node)
            if action=='genEngineering':
                # update vPrjDoc alias
                self.doc.setNodeText(node,'engineeringfn',fn)
                self.doc.setNodeText(node,'alias',sAlias)
                nodeBase=doc.getChild(doc.getRoot(),'prjengs')
                if nodeBase is None:
                    nodeBase=doc.getChildForced(doc.getRoot(),'prjengs')
                prjId=self.doc.getKey(node)
                srv.Update(applAlias,node)
            if action=='genPrjPlan':
                # update vPrjDoc alias
                self.doc.setNodeText(node,'planningfn',fn)
                self.doc.setNodeText(node,'alias',sAlias)
                nodeBase=doc.getChild(doc.getRoot(),'prjengs')
                if nodeBase is None:
                    nodeBase=doc.getChildForced(doc.getRoot(),'PrjPlan')
                prjId=self.doc.getKey(node)
                srv.Update(applAlias,node)
            if action in ['genEngineering','genPrjDoc','genPrjEng','genPrjPlan']:
                doc.setAttribute(nodeBase,'prjfid',prjId)
                doc.setForeignKey(nodeBase,attr='fid',attrVal=prjId,appl='vPrj')
                doc.clearFingerPrint(nodeBase)
                doc.GetFingerPrintAndStore(nodeBase)
                srv.Update(applAliasDest,nodeBase)
                
                prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
                if prjInfoNode is None:
                    prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
                    #doc.checkId(prjInfoNode)
                    
                sAttr=self.doc.getAttribute(node,'id')
                for k in ['name','longname','description','prjnumber']:
                    doc.setNodeText(prjInfoNode,k,
                        self.doc.getNodeText(node,k))
                doc.setNodeText(prjInfoNode,'clientshort',
                        self.doc.getNodeText(node,'cltshort'))
                doc.setNodeText(prjInfoNode,'client',
                        self.doc.getNodeText(node,'cltlong'))
                doc.setAttribute(prjInfoNode,'fid',sAttr)
                attrNode=doc.getChild(node,'prjAttr')
                if attrNode is not None:
                    for k in ['facility']:
                        doc.setNodeText(prjInfoNode,k,
                            self.doc.getNodeText(attrNode,k))
                
                attrNode=doc.getChild(node,'attributes')
                if attrNode is not None:
                    attrs=doc.getChilds(attrNode)
                    dict={}
                    for a in attrs:
                        dict[self.doc.getTagName(a)]=a
                    keys=dict.keys()
                    keys.sort()
                    for k in keys:
                        a=dict[k]
                        doc.setNodeText(prjInfoNode,k,
                            self.doc.getText(a))
                doc.AlignNode(prjInfoNode)
            if action=='genPrjDoc':
                nodeAlias=doc.getChildByLst(doc.getRoot(),['config','vFileSrv','host','alias'])
                if nodeAlias is not None:
                    sAliasOld=doc.getText(nodeAlias)
                    doc.setText(nodeAlias,sAlias)
                    #nodeAlias=doc.getChild(nodeCfg,)
                    doc.AlignNode(nodeAlias)
                doc.Save()
            if action=='genPrjEng':
                nodeAlias=doc.getChildByLst(doc.getRoot(),['config','vPrjDoc','host','alias'])
                if nodeAlias is not None:
                    sAliasOld=doc.getText(nodeAlias)
                    doc.setText(nodeAlias,sAlias)
                    doc.AlignNode(nodeAlias)
                doc.Save()
            if action=='genEngineering':
                nodeAlias=doc.getChildByLst(doc.getRoot(),['config','vPrjDoc','host','alias'])
                if nodeAlias is not None:
                    sAliasOld=doc.getText(nodeAlias)
                    doc.setText(nodeAlias,sAlias)
                    doc.AlignNode(nodeAlias)
                doc.Save()
            if action=='genPrjPlan':
                nodeAlias=doc.getChildByLst(doc.getRoot(),['config','vPrjDoc','host','alias'])
                if nodeAlias is not None:
                    sAliasOld=doc.getText(nodeAlias)
                    doc.setText(nodeAlias,sAlias)
                    doc.AlignNode(nodeAlias)
                doc.Save()
            return 1
        except:
            vtLog.vtLngCur(vtLog.ERROR,'action:%s;kwargs:%s'%(action,vtLog.pformat(kwargs)),self.__class__.__name__)
            vtLog.vtLngTB(self.__class__.__name__)
            return -1
