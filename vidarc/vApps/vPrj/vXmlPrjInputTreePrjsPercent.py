#----------------------------------------------------------------------------
# Name:         vXmlPrjInputTreePrjsPercent.py
# Purpose:      input widget for project xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060725
# CVS-ID:       $Id: vXmlPrjInputTreePrjsPercent.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#from vidarc.tool.input.vtInputMultiplePercentTree import vtInputMultiplePercentTree
from vidarc.tool.input.vtInputDistributeTree import vtInputDistributeTree
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vApps.vPrj.vXmlPrjGrpTree import vXmlPrjGrpTree

class vXmlPrjInputTreePrjsPercent(vtInputDistributeTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        if 'columns' not in _kwargs:
            _kwargs['columns']=[(_(u'name'),-1),(_(u'longName'),-1),('id',-1)]
        if 'show_attrs' not in _kwargs:
            _kwargs['show_attrs']=['name','longName','|id']
        _kwargs['col_value']=_(u'percentage')
        apply(vtInputDistributeTree.__init__,(self,) + _args,_kwargs)
        self.tagGrp='projects'
        self.tagName='project'
        self.tagNameInt='project'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vPrj')
    def __createTree__(*args,**kwargs):
        tr=vXmlPrjGrpTree(**kwargs)
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
