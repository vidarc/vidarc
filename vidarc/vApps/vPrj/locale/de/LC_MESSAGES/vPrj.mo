��    _                   	   	          -     3     :     @     T     f  %   m     �     �     �     �     �     �  %   �     �     �  
   	     	     +	     2	     >	     X	     u	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  /   �	     
  +   "
     N
     U
  
   [
     f
     v
     {
     �
     �
  
   �
     �
  &   �
     �
     �
     �
     �
     
          +     C  
   F     Q     b     g     l     p  	   u          �     �     �  ,   �     �     �     �     �               (     9     ?  7   F     ~     �     �     �  	   �     �     �     �     �     �     �  
                    �  %     �     �     �  
   �     �               0     7     U     c     s     �     �  
   �  <   �     �  	   �  
   �     �               #     <     X     p     t     y     �  	   �     �     �     �     �  !   �  (   �       0   "     S     [     a     p  	   ~  	   �     �     �  
   �     �  %   �     �                    &     :     M  
   m     x     �     �     �     �     �     �     �     �     �     �  7         8     A     J     O     U     j     }     �     �  *   �     �     �     �          /     @     Q  	   V     `  !   i     �     �     �     �     �     1   ?      2       )   T          P   $                !   <          G   ,   V         >              Y   ^   *         A       3   W   H   N       C       M   D          K   	   S   X       /       +       -   &       ;   E   U                 5              F              .          "   7      B   L       R   %   0   Z                 :   =      #   '   4       @         9       O   I   6       _          J   ]   [                  Q   (   
   \                 8        (undef*) %b-%d-%Y   %H:%M:%S wk:%W &File &Tools &View --config <filename> --file <filename> --help --lang <language id according ISO639> --log <level> -c <filename> -f <filename> About Anal&yse Close Close connection to use this feautre. Config Connect Connection Connection still active! Discon E&xit	Alt+X Generate Project Document Generate Project Engineering Generate Project Files Log Logs Open Request Lock	F4 Save Save File As Settings Stop Synch Timeout on pausing Unsaved data present! Do you want to save data? VIDARC Project XML files (*.xml)|*.xml|all files (*.*)|*.* access alias align ...  align finished. appl blocked closed configuation connection connection closed connection established ... please wait connection lost. copy disconnected established file open fault. file opened ... file opened and parsed. fn generated. get content ...  help host msg name no access notify open open file finished.  open the <filename> at start open the <filename> to configure application opened paused port running save file ...  save file finished. show this screen state status stopping thread and closing connections please wait ... synch synch aborted. synch finished. synch started ... synchable thread stopped time unknown usr vNet XML Master Close Dialog vPrj vPrj Close valid valid flags: value Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-09-13 22:02+0200
PO-Revision-Date: 2005-09-20 14:49+0100
Last-Translator: Walter Obweger <walter.obweger@vidarc.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
  (unbekannt*) %d-%b-%Y   %H:%M:%S KW:%U &Datei &Werkzeuge &Ansicht --config <Dateinamen> --file <Dateinamen> --help --lang <Sprach id nach ISO639 --log <Ebene> -c <Dateinamen> -f <Dateinamen> �ber Anal&yse Schliessen Bauen Sie die Verbindung ab, um diese Funktion zu verwenden. Konfig. Verbinden Verbindung Verbindung aufgebaut! Trennen Be&enden	ALT+X erzeuge Projekt Dokument erzeuge Projekt Engineering erzeuge Projekt Dateien Log Logs �ffnen Sperre anfordern	F4 Speichern Speichern unter Einstellungen Stopp Synch Zeit�berschreitung beim Pausieren Nicht alle Daten gespeichert! Speichern? VIDARC Projekt XML Dateien (*.xml)|*.xml|alle Dateien (*.*)|*.* Zugriff Alias ausrichten ... ausgerichtet. Anwendung blockiert geschlossen Konfiguration Verbindung Verbindung geschlossen Verbindung aufgebaut ... bitte warten Verbindung abgebrochen. kopie trenned hergestellt Fehler beim �ffnen. Datei ge�ffnet ... Datei ge�ffnet und ausgewertet. Dateinamen erzeugt. hole Inhalt ... Hilfe Rechner Meldung Name kein Zugriff benachrichtigen �ffnen Datei ge�ffnet. �ffnet die <Dateiname> am Start �ffnet die <filename> als Konfiguration def Applikation ge�ffnet pausiert Port l�uft Datei speichern ...  Datei gespeichert. gibt diese Meldungen aus Status Status Stoppe Threads und trenne Verbindungen ... synch synchronisieren abgebrochen. synchronisieren abgeschlossen. synchronisieren gestartet ... synchronisierbar Thread gestopped Zeit unbekannt Anwender vNet XML Master Dialog Schliessen vPrj vPrj Schliessen g�ltig g�ltige Optionen: Wert 