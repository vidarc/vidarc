#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vPrjAppl.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjAppl.py,v 1.6 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt
import vidarc.tool.log.vtLog as vtLog
import wx
import vidarc.vApps.common.vSplashFrame as vSplashFrame
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import images_splash

modules ={u'vPrjMainFrame': [0, '', u'vPrjMainFrame.py']}

OPT_SHORT,OPT_LONG='l:f:c:h',['lang=','file=','config=','log=','help']

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        wx.App.__init__(self,num)
    def OnInit(self):
        vtLgBase.initAppl(OPT_SHORT,OPT_LONG,'vPrj')
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        d={'label':u'  import vPrjMainFrame ...',
            'eval':'__import__("vidarc.vApps.vPrj.vPrjMainFrame")'}
        actionList.append(d)
        d={'label':u'  Create Project ...',
            'eval':'self.res[0].vApps.vPrj.vPrjMainFrame'}
        actionList.append(d)
        d={'label':u'  Create Project ...',
            'eval':'getattr(self.res[1],"create")'}
        actionList.append(d)
        d={'label':u'  Create Project ...',
            'eval':'self.res[2](None)'}
        actionList.append(d)
        
        d={'label':u'  Finished.'}
        actionList.append(d)
        
        d={'label':u'  Open Config (%s)...'%self.cfgFN,
            'eval':'self.res[3].OpenCfgFile("%s")'%self.cfgFN}
        actionListPost.append(d)
        
        d={'label':u'  Open File (%s)...'%self.fn,
            'eval':'self.res[3].OpenFile("%s")'%self.fn}
        actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'MES','Prj',
            images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        return True
    def OpenFile(self,fn):
        self.fn=fn
    def OpenCfgFile(self,fn):
        self.cfgFN=fn
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[3]
        self.SetTopWindow(self.main)
        self.main.Show()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        self.main=self.splash.res[3]
        self.main.Destroy()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
        
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    

def main():
    fn=None
    cfgFN='vPrjCfg.xml'
    iLogLv=vtLog.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'f:c:h',['file=','config=','log=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLog.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLog.INFO
                elif o[1]=='2':
                    iLogLv=vtLog.WARN
                elif o[1]=='3':
                    iLogLv=vtLog.ERROR
                elif o[1]=='4':
                    iLogLv=vtLog.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLog.FATAL
    except:
        vtLgBase.initAppl(OPT_SHORT,OPT_LONG,'vPrj')
        showHelp()
    application = BoaApp(0,'vPrj',cfgFN,fn,iLogLv,60004)
    application.MainLoop()

if __name__ == '__main__':
    main()
