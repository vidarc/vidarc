#Boa:Dialog:vPrjGenPrjEngDialog
#----------------------------------------------------------------------------
# Name:         vPrjGenPrjEngDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjGenPrjEngDialog.py,v 1.4 2006/03/14 14:34:20 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
#from wxPython.lib.buttons import *
#from wxPython.wx import wxFileDialog
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
#import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.vXmlPrjEng as vXmlPrjEng
import vidarc.tool.InOut.fnUtil as fnUtil
import codecs,os.path

import images

def create(parent):
    return vPrjGenPrjEngDialog(parent)

[wxID_VPRJGENPRJENGDIALOG, wxID_VPRJGENPRJENGDIALOGCLSTOPTIONS, 
 wxID_VPRJGENPRJENGDIALOGGCBBBROWSE, wxID_VPRJGENPRJENGDIALOGGCBBCANCEL, 
 wxID_VPRJGENPRJENGDIALOGGCBBGENERATE, wxID_VPRJGENPRJENGDIALOGLBLCLIENT, 
 wxID_VPRJGENPRJENGDIALOGLBLFILENAME, wxID_VPRJGENPRJENGDIALOGLBLPRJLONG, 
 wxID_VPRJGENPRJENGDIALOGLBLPRJSHORT, wxID_VPRJGENPRJENGDIALOGTXTCLIENT, 
 wxID_VPRJGENPRJENGDIALOGTXTPRJENGFN, wxID_VPRJGENPRJENGDIALOGTXTPRJLONG, 
 wxID_VPRJGENPRJENGDIALOGTXTPRJSHORT, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vPrjGenPrjEngDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VPRJGENPRJENGDIALOG,
              name=u'vPrjGenPrjEngDialog', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(451, 355), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Project Generate Project Engineering')
        self.SetClientSize(wx.Size(443, 328))

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJENGDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 288),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJGENPRJENGDIALOGGCBBCANCEL)

        self.lblFileName = wx.StaticText(id=wxID_VPRJGENPRJENGDIALOGLBLFILENAME,
              label=u'Project Engineering Filename', name=u'lblFileName',
              parent=self, pos=wx.Point(8, 94), size=wx.Size(137, 13), style=0)

        self.txtPrjEngFN = wx.TextCtrl(id=wxID_VPRJGENPRJENGDIALOGTXTPRJENGFN,
              name=u'txtPrjEngFN', parent=self, pos=wx.Point(8, 110),
              size=wx.Size(336, 21), style=wx.TE_RICH2, value=u'')
        self.txtPrjEngFN.Enable(True)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJENGDIALOGGCBBGENERATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate',
              name=u'gcbbGenerate', parent=self, pos=wx.Point(120, 290),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VPRJGENPRJENGDIALOGGCBBGENERATE)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJGENPRJENGDIALOGGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(360, 104),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VPRJGENPRJENGDIALOGGCBBBROWSE)

        self.lblPrjShort = wx.StaticText(id=wxID_VPRJGENPRJENGDIALOGLBLPRJSHORT,
              label=u'Project Shortcut', name=u'lblPrjShort', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(76, 13), style=0)

        self.txtPrjShort = wx.TextCtrl(id=wxID_VPRJGENPRJENGDIALOGTXTPRJSHORT,
              name=u'txtPrjShort', parent=self, pos=wx.Point(8, 24),
              size=wx.Size(72, 21), style=0, value=u'')
        self.txtPrjShort.Enable(False)

        self.lblPrjLong = wx.StaticText(id=wxID_VPRJGENPRJENGDIALOGLBLPRJLONG,
              label=u'Longname', name=u'lblPrjLong', parent=self,
              pos=wx.Point(112, 8), size=wx.Size(50, 13), style=0)

        self.txtPrjLong = wx.TextCtrl(id=wxID_VPRJGENPRJENGDIALOGTXTPRJLONG,
              name=u'txtPrjLong', parent=self, pos=wx.Point(112, 24),
              size=wx.Size(320, 21), style=0, value=u'')
        self.txtPrjLong.Enable(False)

        self.txtClient = wx.TextCtrl(id=wxID_VPRJGENPRJENGDIALOGTXTCLIENT,
              name=u'txtClient', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(72, 21), style=0, value=u'')
        self.txtClient.Enable(False)

        self.lblClient = wx.StaticText(id=wxID_VPRJGENPRJENGDIALOGLBLCLIENT,
              label=u'Client', name=u'lblClient', parent=self, pos=wx.Point(8,
              48), size=wx.Size(26, 13), style=0)

        self.clstOptions = wx.CheckListBox(choices=[u'instance', u'templates',
              u'admin', u'types', u'research'],
              id=wxID_VPRJGENPRJENGDIALOGCLSTOPTIONS, name=u'clstOptions',
              parent=self, pos=wx.Point(8, 160), size=wx.Size(136, 112),
              style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.doc=None
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbGenerate.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
        self.clstOptions.Check(0,True)
        self.clstOptions.Check(1,True)
        self.clstOptions.Check(3,True)
        self.bGenerated=False
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node):
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        self.gcbbCancel.SetLabel(u'Cancel')
        self.bGenerated=False
        self.node=node
        if (node is None) or (self.doc is None):
            self.txtPrjShort.SetValue('')
            self.txtPrjLong.SetValue('')
            self.txtPrjEngFN.SetValue('')
            self.txtClient.SetValue('')
        else:
            sShort=self.doc.getNodeText(node,'name')
            sLong=self.doc.getNodeText(node,'longname')
            attrNode=self.doc.getChild(node,'attributes')
            if attrNode is not None:
                sClient=self.doc.getNodeText(attrNode,'clientshort')
            else:
                sClient=''
            sPrjEngFN=self.doc.getNodeText(node,'engineeringfn')
            self.txtPrjShort.SetValue(sShort)
            self.txtPrjLong.SetValue(sLong)
            self.txtClient.SetValue(sClient)
            self.txtPrjEngFN.SetValue(sPrjEngFN)
            self.txtPrjEngFN.SetStyle(0, len(sPrjEngFN), wx.TextAttr("BLACK", "WHITE"))
            pass
        
    def OnGcbbCancelButton(self, event):
        if self.bGenerated:
            self.EndModal(1)
        else:
            self.EndModal(0)
        event.Skip()
        
    def OnGcbbGenerateButton(self, event):
        #self.gcbbCancel.Enable(False)
        sShort=self.txtPrjShort.GetValue()
        sLong=self.txtPrjLong.GetValue()
        sClient=self.txtClient.GetValue()
        sPrjEngFN=self.txtPrjEngFN.GetValue()
        if len(sShort)<=0 and len(sLong)<=0:
            return
        
        if len(sPrjEngFN)<=0:
            sPrjEngFN='???'
            self.txtPrjEngFN.SetValue(sPrjEngFN)
            self.txtPrjEngFN.SetStyle(0, len(sPrjEngFN), wx.TextAttr("RED", "YELLOW"))
            return
        sFN=os.path.split(sPrjEngFN)[1]
        sTmp=fnUtil.replaceSuspectChars(sFN)
        if sTmp!=sFN:
            self.txtPrjEngFN.SetStyle(0, len(sPrjEngFN), wx.TextAttr("RED", "YELLOW"))
            return
        self.txtPrjEngFN.SetStyle(0, len(sPrjEngFN), wx.TextAttr("BLACK", "WHITE"))
        fn=self.txtPrjEngFN.GetValue()
        doc=vXmlPrjEng.vXmlPrjEng()
        try:
            if doc.Open(fn)<0:
                doc.New()
        except:
            doc.New()
        
        prjEngNode=doc.getChild(doc.getRoot(),'prjengs')
        if prjEngNode is None:
            prjEngNode=doc.getChildForced(doc.getRoot(),'prjengs')
            #doc.checkId(prjEngNode)
        prjId=self.doc.getKey(self.node)
        doc.setAttribute(prjEngNode,'prjfid',prjId)
        for i in range(0,self.clstOptions.GetCount()):
            if self.clstOptions.IsChecked(i):
                if i==0:
                    elm=doc.getChild(prjEngNode,'instance')
                    if elm is None:
                        elm=doc.createSubNode(prjEngNode,'instance')
                        doc.checkId(elm)
                        doc.setNodeText(elm,'name','instance')
                        #doc.setNodeTextAttr(elm,'name','Instance','language','en')
                        #doc.setNodeTextAttr(elm,'name','Instanz','language','de')
                        #doc.AlignNode(doc,elm)
                elif i==1:
                    elm=doc.getChild(prjEngNode,'templates')
                    if elm is None:
                        elm=doc.createSubNode(prjEngNode,'templates')
                        doc.checkId(elm)
                        doc.setNodeText(elm,'name','templates')
                        #doc.setNodeTextAttr(elm,'name','Templates','language','en')
                        #doc.setNodeTextAttr(elm,'name','Vorlagen','language','de')
                elif i==2:
                    elm=doc.getChild(prjEngNode,'admin')
                    if elm is None:
                        elm=doc.createSubNode(prjEngNode,'admin')
                        doc.checkId(elm)
                        doc.setNodeText(elm,'name','administration')
                        #doc.setNodeTextAttr(elm,'name','Administration','language','en')
                        #doc.setNodeTextAttr(elm,'name','Verwaltung','language','de')
                elif i==3:
                    elm=doc.getChild(prjEngNode,'typeContainer')
                    if elm is None:
                        elm=doc.createSubNode(prjEngNode,'typeContainer')
                        doc.checkId(elm)
                        doc.setNodeText(elm,'name','predefined types')
                        #doc.setNodeTextAttr(elm,'name','predefined types','language','en')
                        #doc.setNodeTextAttr(elm,'name','vordefinierte Typen','language','de')
                elif i==4:
                    elm=doc.getChild(prjEngNode,'research')
                    if elm is None:
                        elm=doc.createSubNode(prjEngNode,'research')
                        doc.checkId(elm)
                        doc.setNodeText(elm,'name','Research')
                        #doc.setNodeTextAttr(elm,'name','Research','language','en')
                        #doc.setNodeTextAttr(elm,'name','Forschung','language','de')
        doc.processMissingId()
        doc.clearMissingId()
        
        prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
        if prjInfoNode is None:
            prjInfoNode=doc.getChildForced(doc.getRoot(),'prjinfo')
            #doc.checkId(prjInfoNode)
            
        sAttr=self.doc.getAttribute(self.node,'id')
        for k in ['name','longname','description']:
            doc.setNodeText(prjInfoNode,k,
                self.doc.getNodeText(self.node,k))
        doc.setAttribute(prjInfoNode,'fid',sAttr)
        attrNode=doc.getChild(self.node,'attributes')
        attrs=doc.getChilds(attrNode)
        dict={}
        for a in attrs:
            dict[self.doc.getTagName(a)]=a
        keys=dict.keys()
        keys.sort()
        for k in keys:
            a=dict[k]
            doc.setNodeText(prjInfoNode,k,
                self.doc.getText(a))
        
        node=doc.getChild(doc.getRoot(),'settings')
        if node is None:
            node=doc.getChildForced(doc.getRoot(),'settings')
        node=doc.getChild(None,'config')
        if node is None:
            node=doc.getChildForced(doc.getRoot(),'config')
        
        doc.AlignDoc()
        
        doc.Save(fn,encoding=u'ISO-8859-1')#self.dom.encoding)#'ISO-8859-1'
        doc.Close()
        self.doc.setNodeText(self.node,'engineeringfn',fn)
        self.doc.AlignNode(self.node)
        self.bGenerated=True
        img=images.getApplyBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        self.gcbbCancel.SetLabel(u'Ok')
        self.gcbbCancel.Refresh()
        event.Skip()
    
    def OnGcbbBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.txtPrjEngFN.GetValue()
            #fn=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.tmplDN)
            if len(fn)>0:
                dlg.SetFilename(fn)
            else:
                sShort=self.txtPrjShort.GetValue()
                sClient=self.txtClient.GetValue()
                dlg.SetFilename('prjeng_%s%s'%(sClient,sShort))
            #else:
            #    dlg.SetDirectory(self.tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #filename=fnUtil.getFilenameRel2BaseDir(filename,self.tmplDN)
                self.txtPrjEngFN.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    
