#Boa:FramePanel:vPrjAttrPanel
#----------------------------------------------------------------------------
# Name:         vPrjAttrPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjAttrPanel.py,v 1.3 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
#from wxPython.wx import wxImageFromStream, wxBitmapFromImage
#from wxPython.wx import wxEmptyIcon
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
#from wxPython.wx import wxMask
#from wxPython.wx import wxBLUE
#from wxPython.wx import wxNewId
#from wxPython.wx import wx.NewEventType
#from wxPython.wx import wx.PyEvent
#from wxPython.wx import wxPostEvent
import string,sys

#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import images

[wxID_VPRJATTRPANEL, wxID_VPRJATTRPANELGCBBADD, wxID_VPRJATTRPANELGCBBDEL, 
 wxID_VPRJATTRPANELGCBBSET, wxID_VPRJATTRPANELLBLATTRNAME, 
 wxID_VPRJATTRPANELLBLATTRVAL, wxID_VPRJATTRPANELLSTATTR, 
 wxID_VPRJATTRPANELSTATICTEXT1, wxID_VPRJATTRPANELTXTATTR, 
 wxID_VPRJATTRPANELTXTID, wxID_VPRJATTRPANELTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(11)]


# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_CHANGED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_CHANGED,func)
class vgpProjectInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

class vPrjAttrPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJATTRPANEL, name=u'vPrjAttrPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(377, 256),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(369, 229))
        self.SetAutoLayout(True)

        self.txtId = wx.TextCtrl(id=wxID_VPRJATTRPANELTXTID, name=u'txtId',
              parent=self, pos=wx.Point(96, 4), size=wx.Size(100, 21),
              style=wx.TE_RICH2, value=u'')
        self.txtId.Bind(wx.EVT_TEXT, self.OnTxtIdText,
              id=wxID_VPRJATTRPANELTXTID)

        self.staticText1 = wx.StaticText(id=wxID_VPRJATTRPANELSTATICTEXT1,
              label=u'Project Number', name='staticText1', parent=self,
              pos=wx.Point(4, 8), size=wx.Size(73, 13), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VPRJATTRPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(4, 96),
              size=wx.Size(276, 120), style=wx.LC_REPORT)
        self.lstAttr.SetConstraints(LayoutAnchors(self.lstAttr, True, True,
              True, True))
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VPRJATTRPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected, id=wxID_VPRJATTRPANELLSTATTR)

        self.txtAttr = wx.TextCtrl(id=wxID_VPRJATTRPANELTXTATTR,
              name=u'txtAttr', parent=self, pos=wx.Point(96, 36),
              size=wx.Size(184, 21), style=0, value=u'')
        self.txtAttr.SetConstraints(LayoutAnchors(self.txtAttr, True, True,
              True, False))

        self.txtVal = wx.TextCtrl(id=wxID_VPRJATTRPANELTXTVAL, name=u'txtVal',
              parent=self, pos=wx.Point(96, 66), size=wx.Size(184, 21), style=0,
              value=u'')
        self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True, True,
              False))

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJATTRPANELGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(288, 66), size=wx.Size(72, 30),
              style=0)
        self.gcbbSet.SetConstraints(LayoutAnchors(self.gcbbSet, False, True,
              True, False))
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VPRJATTRPANELGCBBSET)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJATTRPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(288, 30), size=wx.Size(72, 30),
              style=0)
        self.gcbbAdd.SetConstraints(LayoutAnchors(self.gcbbAdd, False, True,
              True, False))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VPRJATTRPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJATTRPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(288, 116), size=wx.Size(72, 30),
              style=0)
        self.gcbbDel.SetConstraints(LayoutAnchors(self.gcbbDel, False, True,
              True, False))
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VPRJATTRPANELGCBBDEL)

        self.lblAttrName = wx.StaticText(id=wxID_VPRJATTRPANELLBLATTRNAME,
              label=u'Attribute', name=u'lblAttrName', parent=self,
              pos=wx.Point(4, 38), size=wx.Size(39, 13), style=0)

        self.lblAttrVal = wx.StaticText(id=wxID_VPRJATTRPANELLBLATTRVAL,
              label=u'Value', name=u'lblAttrVal', parent=self, pos=wx.Point(4,
              68), size=wx.Size(27, 13), style=0)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.selectedIdx=-1
        self.bModified=False
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)

        img=images.getApplyBitmap()
        self.gcbbSet.SetBitmapLabel(img)
        
        # setup list
        self.lstAttr.InsertColumn(0,u'Attribute')
        self.lstAttr.InsertColumn(1,u'Value',wx.LIST_FORMAT_LEFT,270)
        self.SetupImageList()
    
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def Clear(self):
        self.txtId.SetValue('')
        self.txtAttr.SetValue('')
        self.txtVal.SetValue('')
        self.lstAttr.DeleteAllItems()
        self.selectedIdx=-1
        self.SetModified(False)
        
    def Lock(self,flag):
        if flag:
            self.txtId.SetEditable(False)
            self.txtVal.Enable(False)
            self.txtAttr.Enable(False)
            self.lstAttr.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbDel.Enable(False)
            self.gcbbSet.Enable(False)
        else:
            self.txtId.SetEditable(True)
            self.txtVal.Enable(True)
            self.txtAttr.Enable(True)
            self.lstAttr.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbDel.Enable(True)
            self.gcbbSet.Enable(True)
    def SetupImageList(self):
        #self.imgDict={}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=hum_tree_images.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        pass
    def SetDoc(self,doc,bNet=False):
        if bNet:
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.doc=doc
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
    def SetNode(self,node):
        self.Clear()
        if (node is None) or (self.doc is None):
            return
        self.node=node
        
        sAttrId=self.doc.getAttribute(node,'id')
        self.txtId.SetValue(sAttrId)
        self.txtAttr.SetValue('')
        
        # setup attributes
        self.lstAttr.DeleteAllItems()
        self.selectedIdx=-1
        
        attrsNode=self.doc.getChild(node,'attributes')
        if attrsNode is not None:
            attrs={}
            for o in self.doc.getChilds(attrsNode):
                tagName=self.doc.getTagName(o)
                s=self.doc.getNodeText(attrsNode,tagName)
                attrs[tagName]=s
        else:
            attrs={}
            attrs['manager']=''
        keys=attrs.keys()
        keys.sort()
        for k in keys:
            #self.lstAttr.InsertStringItem(0,k)
            #self.lstAttr.InsertStringItem(1,attrs[k])
            try:
                imgId=self.imgDict[k]
            except:
                imgId=-1
            index = self.lstAttr.InsertImageStringItem(sys.maxint, k, imgId)
            self.lstAttr.SetStringItem(index, 1, attrs[k])
        self.SetModified(False)
        pass
    def GetNode(self):
        if self.node is None:
            return ''
        sAttrId=self.txtId.GetValue()
        ids=self.doc.getIds()
        if ids is not None:
            sOldId=self.doc.getAttribute(self.node,'id')
            ids.RemoveId(sOldId)
            ret,n=ids.GetIdStr(sAttrId)
            if ret<0:
                bOk=False
            else:
                if n is None:
                    bOk=True
                else:
                    if n==node:
                        bOk=True
                    else:
                        bOk=False
            if bOk==False:
                # fault id is not possible
                ids.AddNewNode(self.node)
                sAttr=ids.ConvertId2Str(self.node)
                self.txtId.SetValue(sAttr)
                self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            else:
                self.doc.setAttribute(self.node,'id',sAttrId)
                sAttr=ids.ConvertId2Str(self.node)
                self.txtId.SetValue(sAttr)
                self.doc.setAttribute(self.node,'id',sAttr)
                ids.CheckId(self.node)
                ids.ClearMissing()
        
        attrsNode=self.doc.getChildForced(self.node,'attributes')
        attrs={}
        for o in self.doc.getChilds(attrsNode):
            tagName=self.doc.getTagName(o)
            attrs[tagName]=o
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sAttrVal=it.m_text
            self.doc.setNodeText(attrsNode,sAttrName,sAttrVal)
            try:
                del attrs[sAttrName]
            except:
                pass
        keys=attrs.keys()
        for k in keys:
            self.doc.deleteNode(o,self.node)
        self.SetModified(False)
        
    def OnGcbbSetButton(self,event):
        #index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, -1)
        self.lstAttr.SetStringItem(self.selectedIdx, 1, self.txtVal.GetValue())
        self.SetModified(True)
        #event.skip()

    def OnTxtIdText(self, event):
        if self.doc is None:
            return
        sAttr=self.txtId.GetValue()
        ids=self.doc.getIds()
        ret,n=ids.GetIdStr(sAttr)
        if ret<0:
            ids.AddNewNode(self.node)
            sId=ids.ConvertId2Str(self.node)
            self.txtId.SetValue(sId)
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
            return
        if n is None:
            bOk=True
        else:
            if self.doc.isSame(n,self.node):
                bOk=True
            else:
                bOk=False
        if bOk:
            # id is not used yet -> ok
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("BLACK", "WHITE"))
        else:
            # id is USED -> fault
            self.txtId.SetStyle(0, len(sAttr), wx.TextAttr("RED", "YELLOW"))
        self.SetModified(True)
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        self.txtAttr.SetValue(sAttrName)
        self.txtVal.SetValue(sAttrVal)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        self.txtAttr.SetValue(u'')
        self.txtVal.SetValue(u'')
        self.selectedIdx=-1
        event.Skip()

    def OnGcbbAddButton(self, event):
        sAttrName=self.txtAttr.GetValue()
        sAttrVal=self.txtVal.GetValue()
        idx=self.lstAttr.FindItem(0,sAttrName)
        if idx==-1:
            # ok add new attribute
            imgId=-1
            idx = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrName, imgId)
            self.lstAttr.SetStringItem(idx, 1, sAttrVal)
            self.SetModified(True)
        
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedIdx!=-1:
            self.lstAttr.DeleteItem(self.selectedIdx)
            self.SetModified(True)
            self.selectedIdx=-1
        event.Skip()
    
    
