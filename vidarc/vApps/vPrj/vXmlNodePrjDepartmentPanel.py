#Boa:FramePanel:vXmlNodePrjDepartmentPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDepartmentPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060727
# CVS-ID:       $Id: vXmlNodePrjDepartmentPanel.py,v 1.5 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeTagPanel
import vidarc.vApps.vHum.vXmlHumInputTreeGrp
import vidarc.vApps.vHum.vXmlHumInputTree
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputAttrValue
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTree
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
from gettext import *
from vidarc.tool.xml.vtXmlTree import *

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.ext.state.veRoleDialog import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

#import vidarc.vApps.vPrj.__config__ as __config__

VERBOSE=0

[wxID_VXMLNODEPRJDEPARTMENTPANEL, wxID_VXMLNODEPRJDEPARTMENTPANELCBROLE, 
 wxID_VXMLNODEPRJDEPARTMENTPANELLBLAREA, 
 wxID_VXMLNODEPRJDEPARTMENTPANELLBLROLE, 
 wxID_VXMLNODEPRJDEPARTMENTPANELVITAGPN, 
 wxID_VXMLNODEPRJDEPARTMENTPANELVITRAREA, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vXmlNodePrjDepartmentPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTagPn, 0, border=8, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsArea, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.vitrArea, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRole, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbRole, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPRJDEPARTMENTPANEL,
              name=u'vXmlNodePrjDepartmentPanel', parent=prnt, pos=wx.Point(319,
              211), size=wx.Size(356, 160), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(348, 133))
        self.SetAutoLayout(True)

        self.viTagPn = vidarc.tool.xml.vtXmlNodeTagPanel.vtXmlNodeTagPanel(id=wxID_VXMLNODEPRJDEPARTMENTPANELVITAGPN,
              name=u'viTagPn', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(348, 95), style=0)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODEPRJDEPARTMENTPANELLBLAREA,
              label=_(u'department'), name=u'lblArea', parent=self,
              pos=wx.Point(4, 107), size=wx.Size(79, 26), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.vitrArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODEPRJDEPARTMENTPANELVITRAREA,
              name=u'vitrArea', parent=self, pos=wx.Point(87, 107),
              size=wx.Size(87, 26), style=0)

        self.lblRole = wx.StaticText(id=wxID_VXMLNODEPRJDEPARTMENTPANELLBLROLE,
              label=_(u'role'), name=u'lblRole', parent=self, pos=wx.Point(178,
              107), size=wx.Size(79, 26), style=wx.ALIGN_RIGHT)
        self.lblRole.SetMinSize(wx.Size(-1, -1))

        self.cbRole = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJDEPARTMENTPANELCBROLE,
              bitmap=vtArt.getBitmap(vtArt.Role), label=_(u'role'),
              name=u'cbRole', parent=self, pos=wx.Point(261, 107),
              size=wx.Size(87, 26), style=0)
        self.cbRole.SetMinSize(wx.Size(-1, -1))
        self.cbRole.Bind(wx.EVT_BUTTON, self.OnCbRoleButton,
              id=wxID_VXMLNODEPRJDEPARTMENTPANELCBROLE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.cbRole],
                lEvent=[])#,
                #iSecLvEdit=__config__.SEC_LEVEL_LIMIT_ADMIN)
                
        self.dlgRole=veRoleDialog(self)
        self.bRoleShowOnce=False
        self.vitrArea.SetTagNames('mainDepartment','name')
        self.vitrArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.vitrArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.vitrArea.SetTagNames2Base(['globalsdata','globalsDepartment'])
        self.vitrArea.SetShowFullName(False)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        self.viTagPn.SetNetDocs(d)
        if d.has_key('vHum'):
            dd=d['vHum']
            self.dlgRole.SetDocHum(dd['doc'],dd['bNet'])
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.vitrArea.SetDocTree(dd['doc'],dd['bNet'])
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.viTagPn.GetModified():
            return True
        if self.vitrArea.IsModified():
            return True
        return False
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.viTagPn.__Clear__()
            self.dRoles={}
            self.vitrArea.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viTagPn.SetDoc(doc,bNet)
        self.vitrArea.SetDoc(doc,bNet,bExternal=True)
        self.dlgRole.SetDoc(doc,bNet)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                self.dlgRole.SetDocHum(dd['doc'],dd['bNet'])
            if 'vGlobals' in dDocs:
                dd=dDocs['vGlobals']
                self.vitrArea.SetDocTree(dd['doc'],dd['bNet'])
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTagPn.__SetNode__(node)
            self.vitrArea.SetNode(self.node)
            self.dRoles=self.objRegNode.GetRolesDict(self.node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.viTagPn.__GetNode__(node)
            self.vitrArea.GetNode(node)
            self.objRegNode.SetRolesDict(node,self.dRoles)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTagPn.Close()
        self.vitrArea.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.vitrArea.Enable(False)
            self.cbRole.Enable(False)
        else:
            self.vitrArea.Enable(True)
            self.cbRole.Enable(True)
        self.viTagPn.Lock(flag)
    def OnCbRoleButton(self, event):
        try:
            if self.bRoleShowOnce==False:
                self.dlgRole.Centre()
            self.dlgRole.SetRolePossible(self.objRegNode.GetConfigurableRoleTranslationLst())
            self.dlgRole.SetRoles(self.dRoles)
            iRet=self.dlgRole.ShowModal()
            if iRet>0:
                self.dRoles=self.dlgRole.GetRoles()
                self.SetModified(True,self.cbRole)
        except:
            self.__logTB__()
        event.Skip()
