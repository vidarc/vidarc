#----------------------------------------------------------------------------
# Name:         vXmlPrjInputTree.py
# Purpose:      input widget for project xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060226
# CVS-ID:       $Id: vXmlPrjInputTree.py,v 1.2 2006/07/17 11:06:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vPrj.vXmlPrjGrpTree import vXmlPrjGrpTree

class vXmlPrjInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='project'
        #self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vPrj')
    def __createTree__(*args,**kwargs):
        tr=vXmlPrjGrpTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id','country','region'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
