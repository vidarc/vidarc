#Boa:FramePanel:vPrjInfoGeneralPanel
#----------------------------------------------------------------------------
# Name:         vPrjInfoGeneralPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vPrjInfoGeneralPanel.py,v 1.10 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.ext.state.veStateInput
import vidarc.tool.input.vtInputTree
import vidarc.vApps.vContact.vXmlContactInputTreeCustomer
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText
import vidarc.tool.time.vtTimeInputDateTime

import vidarc.tool.time.vTimeDateTimeGM as vTimeDateTimeGM #070530:wro FIXME:needed?
from vidarc.tool.time.vtTime import vtDateTime
import sys

import vidarc.vApps.vPrj.vXmlPrjGrpTree as vXmlPrjGrpTree

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

#import images

[wxID_VPRJINFOGENERALPANEL, wxID_VPRJINFOGENERALPANELCHKCLSD, 
 wxID_VPRJINFOGENERALPANELLBLCUST, wxID_VPRJINFOGENERALPANELLBLDURATION, 
 wxID_VPRJINFOGENERALPANELLBLENDDT, wxID_VPRJINFOGENERALPANELLBLPRJDESC, 
 wxID_VPRJINFOGENERALPANELLBLPRJLONG, wxID_VPRJINFOGENERALPANELLBLPRJNAME, 
 wxID_VPRJINFOGENERALPANELLBLPRJNR, wxID_VPRJINFOGENERALPANELLBLSTARTDT, 
 wxID_VPRJINFOGENERALPANELLBLSTATE, wxID_VPRJINFOGENERALPANELTXTDURATION, 
 wxID_VPRJINFOGENERALPANELVDTEND, wxID_VPRJINFOGENERALPANELVDTSTART, 
 wxID_VPRJINFOGENERALPANELVICUST, wxID_VPRJINFOGENERALPANELVICUSTLONG, 
 wxID_VPRJINFOGENERALPANELVICUSTSHORT, wxID_VPRJINFOGENERALPANELVIDESC, 
 wxID_VPRJINFOGENERALPANELVINAME, wxID_VPRJINFOGENERALPANELVINR, 
 wxID_VPRJINFOGENERALPANELVISTATE, wxID_VPRJINFOGENERALPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(22)]

class vPrjInfoGeneralPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjName, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viTag, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              2))
        parent.AddWindow(self.lblPrjNr, (0, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viNr, (0, 4), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblPrjLong, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viName, (1, 1), border=0, flag=wx.EXPAND, span=(1,
              4))
        parent.AddWindow(self.lblPrjDesc, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viDesc, (2, 1), border=0, flag=wx.EXPAND, span=(1,
              4))
        parent.AddWindow(self.lblCust, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCust, (3, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viCustShort, (3, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCustLong, (3, 3), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblState, (4, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viState, (4, 1), border=0, flag=wx.EXPAND,
              span=(1, 4))
        parent.AddWindow(self.lblStartDT, (5, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.vdtStart, (5, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblEndDT, (5, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.vdtEnd, (5, 4), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblDuration, (6, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtDuration, (6, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.chkClsd, (6, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJINFOGENERALPANEL,
              name=u'vPrjInfoGeneralPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(442, 244), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(434, 217))
        self.SetAutoLayout(True)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VPRJINFOGENERALPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(67, 0), size=wx.Size(179,
              21), style=0)

        self.lblPrjName = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLPRJNAME,
              label=_(u'project name'), name=u'lblPrjName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(63, 21), style=wx.ALIGN_RIGHT)
        self.lblPrjName.SetMinSize(wx.Size(-1, -1))

        self.lblPrjNr = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLPRJNR,
              label=_(u'number'), name=u'lblPrjNr', parent=self,
              pos=wx.Point(250, 0), size=wx.Size(50, 21), style=wx.ALIGN_RIGHT)
        self.lblPrjNr.SetMinSize(wx.Size(-1, -1))

        self.viNr = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VPRJINFOGENERALPANELVINR,
              name=u'viNr', parent=self, pos=wx.Point(304, 0), size=wx.Size(134,
              21), style=0)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VPRJINFOGENERALPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(67, 25),
              size=wx.Size(371, 30), style=0)

        self.lblPrjDesc = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLPRJDESC,
              label=_(u'description'), name=u'lblPrjDesc', parent=self,
              pos=wx.Point(0, 59), size=wx.Size(63, 34), style=wx.ALIGN_RIGHT)
        self.lblPrjDesc.SetMinSize(wx.Size(-1, -1))

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VPRJINFOGENERALPANELVIDESC,
              name=u'viDesc', parent=self, pos=wx.Point(67, 59),
              size=wx.Size(371, 34), style=0)

        self.lblCust = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLCUST,
              label=_(u'customer'), name=u'lblCust', parent=self,
              pos=wx.Point(0, 97), size=wx.Size(63, 24), style=wx.ALIGN_RIGHT)
        self.lblCust.SetMinSize(wx.Size(-1, -1))

        self.viCust = vidarc.vApps.vContact.vXmlContactInputTreeCustomer.vXmlContactInputTreeCustomer(id=wxID_VPRJINFOGENERALPANELVICUST,
              name=u'viCust', parent=self, pos=wx.Point(67, 97),
              size=wx.Size(75, 24), style=0)
        self.viCust.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViCustVtinputTreeChanged,
              id=wxID_VPRJINFOGENERALPANELVICUST)

        self.viCustShort = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VPRJINFOGENERALPANELVICUSTSHORT,
              name=u'viCustShort', parent=self, pos=wx.Point(146, 97),
              size=wx.Size(100, 24), style=0)

        self.viCustLong = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VPRJINFOGENERALPANELVICUSTLONG,
              name=u'viCustLong', parent=self, pos=wx.Point(250, 97),
              size=wx.Size(188, 24), style=0)

        self.lblPrjLong = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLPRJLONG,
              label=u'Long', name=u'lblPrjLong', parent=self, pos=wx.Point(0,
              25), size=wx.Size(63, 30), style=wx.ALIGN_RIGHT)
        self.lblPrjLong.SetMinSize(wx.Size(-1, -1))

        self.lblState = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLSTATE,
              label=_(u'state'), name=u'lblState', parent=self, pos=wx.Point(0,
              125), size=wx.Size(63, 24), style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize(wx.Size(-1, -1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VPRJINFOGENERALPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(67, 125),
              size=wx.Size(371, 24), style=0)

        self.lblStartDT = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLSTARTDT,
              label=_(u'start'), name=u'lblStartDT', parent=self,
              pos=wx.Point(0, 153), size=wx.Size(63, 24), style=wx.ALIGN_RIGHT)
        self.lblStartDT.SetMinSize(wx.Size(-1, -1))

        self.lblEndDT = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLENDDT,
              label=_(u'end'), name=u'lblEndDT', parent=self, pos=wx.Point(250,
              153), size=wx.Size(50, 24), style=wx.ALIGN_RIGHT)
        self.lblEndDT.SetMinSize(wx.Size(-1, -1))

        self.vdtStart = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VPRJINFOGENERALPANELVDTSTART,
              name=u'vdtStart', parent=self, pos=wx.Point(67, 153),
              size=wx.Size(179, 24), style=0)
        self.vdtStart.Bind(vidarc.tool.time.vtTimeInputDateTime.vEVT_VTTIME_INPUT_DATETIME_CHANGED,
              self.OnVdtStartVttimeInputDatetimeChanged,
              id=wxID_VPRJINFOGENERALPANELVDTSTART)

        self.vdtEnd = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VPRJINFOGENERALPANELVDTEND,
              name=u'vdtEnd', parent=self, pos=wx.Point(304, 153),
              size=wx.Size(134, 24), style=0)
        self.vdtEnd.Bind(vidarc.tool.time.vtTimeInputDateTime.vEVT_VTTIME_INPUT_DATETIME_CHANGED,
              self.OnVdtEndVttimeInputDatetimeChanged,
              id=wxID_VPRJINFOGENERALPANELVDTEND)

        self.chkClsd = wx.CheckBox(id=wxID_VPRJINFOGENERALPANELCHKCLSD,
              label=_(u'closed'), name=u'chkClsd', parent=self,
              pos=wx.Point(304, 181), size=wx.Size(134, 24), style=0)
        self.chkClsd.SetValue(False)

        self.lblDuration = wx.StaticText(id=wxID_VPRJINFOGENERALPANELLBLDURATION,
              label=_(u'duration [M]'), name=u'lblDuration', parent=self,
              pos=wx.Point(0, 181), size=wx.Size(63, 24), style=wx.ALIGN_RIGHT)

        self.txtDuration = wx.TextCtrl(id=wxID_VPRJINFOGENERALPANELTXTDURATION,
              name=u'txtDuration', parent=self, pos=wx.Point(67, 181),
              size=wx.Size(179, 24), style=wx.NO_3D | wx.NO_BORDER, value=u'0')
        self.txtDuration.SetEditable(False)
        self.txtDuration.Enable(False)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrj')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.chkClsd],
                lEvent=[(self.chkClsd,wx.EVT_CHECKBOX)])
        #self.doc=None
        #self.node=None
        self.bModified=False
        self.zStart=vtDateTime(True)
        self.zEnd=vtDateTime(True)
        
        self.viTag.SetTagName('name')
        self.viNr.SetTagName('prjnumber')
        self.viName.SetTagName('longname')
        self.viDesc.SetTagName('description')
        self.viCust.SetTagNames('client','tag')
        self.viCustShort.SetTagName('cltshort')
        self.viCustLong.SetTagName('cltlong')
        self.viState.SetNodeAttr('project','prjState')
        self.vdtStart.SetTagNames('startdatetime')
        self.vdtEnd.SetTagNames('enddatetime')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        self.gbsData.AddGrowableCol(1,1)
        self.gbsData.AddGrowableCol(2,1)
        self.gbsData.AddGrowableCol(4,2)
        self.gbsData.AddGrowableCol(5,1)
        self.gbsData.AddGrowableRow(2,1)
        self.gbsData.Layout()
        
    def GetModifiedNew(self):
        if self.viTag.IsModified():
            return True
        if self.viNr.IsModified():
            return True
        if self.viName.IsModified():
            return True
        if self.viDesc.IsModified():
            return True
        if self.viCust.IsModified():
            return True
        if self.viCustShort.IsModified():
            return True
        if self.viCustLong.IsModified():
            return True
        if self.vdtStart.IsModified():
            return True
        if self.vdtEnd.IsModified():
            return True
        return vtXmlNodePanel.GetModified(self)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.viTag.Clear()
            self.viNr.Clear()
            self.viName.Clear()
            self.viDesc.Clear()
            self.viCust.Clear()
            self.viCustShort.Clear()
            self.viCustLong.Clear()
            self.vdtStart.Clear()
            self.vdtEnd.Clear()
            self.viState.Clear()
            self.txtDuration.SetValue('0')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        # add code here
        if d.has_key('vContact'):
            dd=d['vContact']
            self.viCust.SetDocTree(dd['doc'],dd['bNet'])

    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viNr.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.viDesc.SetDoc(doc)
        self.viCust.SetDoc(doc,bNet)
        self.viCustShort.SetDoc(doc)
        self.viCustLong.SetDoc(doc)
        self.vdtStart.SetDoc(doc)#,bNet)
        self.vdtEnd.SetDoc(doc)#,bNet)
        self.viState.SetDoc(self.doc)
        if dDocs is not None:
            if 'vContact' in dDocs:
                dd=dDocs['vContact']
                self.viCust.SetDocTree(dd['doc'],dd['bNet'])
    def __enableCust__(self):
        bEn=self.viCust.IsEnabled()
        if bEn:
            id=self.viCust.GetID()
            try:
                if len(id)>0:
                    if long(id)>=0:
                        bEn=False
            except:
                pass
            self.viCustShort.Enable(bEn)
            self.viCustLong.Enable(bEn)
        else:
            self.viCustShort.Enable(False)
            self.viCustLong.Enable(False)
    def __showCust__(self):
        id=self.viCust.GetID()
        try:
            if len(id)>0:
                if long(id)>=0:
                    fid=self.viCust.GetForeignID()
                    netDoc,node=self.doc.GetNode(fid)
                    sTag=netDoc.getNodeText(node,'tag')
                    sName=netDoc.getNodeText(node,'name')
                    self.viCustShort.SetValue(sTag)
                    self.viCustLong.SetValue(sName)
        except:
            pass
    def __showDuration__(self):
        try:
            sStart=self.vdtStart.GetValue()
            sEnd=self.vdtEnd.GetValue()
            try:
                self.zStart.SetDateStr(sStart[:10])
                self.zEnd.SetDateStr(sEnd[:10])
            except:
                self.txtDuration.SetValue('0')
                return
            iYS,iMS=self.zStart.GetYear(),self.zStart.GetMonth()
            iYE,iME=self.zEnd.GetYear(),self.zEnd.GetMonth()
            iMon=0
            if iYS==iYE:
                if iMS==iME:
                    iMon=1
                elif iMS>iME:
                    iMon=0
                else:
                    iMon=iME-iMS
                    if self.zStart.GetDay() < self.zEnd.GetDay():
                        iMon+=1
            else:
                iMon=(12-iMS)+(iME)
                for i in xrange(iYS+1,iYE):
                    iMon+=12
                if self.zStart.GetDay() < self.zEnd.GetDay():
                    iMon+=1
            self.txtDuration.SetValue(str(iMon))
        except:
            self.txtDuration.SetValue('0')
            self.__logTB__()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.viTag.Enable(False)
            self.viNr.Enable(False)
            self.viName.Enable(False)
            self.viDesc.Enable(False)
            self.viCust.Enable(False)
            self.vdtStart.Enable(False)
            self.vdtEnd.Enable(False)
            self.viState.Enable(False)
            self.chkClsd.Enable(False)
        else:
            self.viTag.Enable(True)
            self.viNr.Enable(True)
            self.viName.Enable(True)
            self.viDesc.Enable(True)
            self.viCust.Enable(True)
            self.vdtStart.Enable(True)
            self.vdtEnd.Enable(True)
            self.viState.Enable(True)
            self.chkClsd.Enable(True)
        self.__enableCust__()
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            #self.viTag.SetNode(self.node)
            #sPrjName=self.doc.getNodeText(node,'name')
            #sPrjLong=self.doc.getNodeText(node,'longname')
            #sPrjDesc=self.doc.getNodeText(node,'description')
            
            self.viTag.SetNode(self.node)
            self.viNr.SetNode(self.node)
            self.viName.SetNode(self.node)
            self.viDesc.SetNode(self.node)
            self.viCust.SetNode(self.node)
            self.viCustShort.SetNode(self.node)
            self.viCustLong.SetNode(self.node)
            self.viState.SetNode(self.node)
            self.vdtStart.SetNode(self.node)
            self.vdtEnd.SetNode(self.node)
            self.chkClsd.SetValue(self.objRegNode.GetClosed(self.node))
            
            self.__showCust__()
            self.__showDuration__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            #self.viTag.GetNode(node)
            #sPrjName=self.txtPrjName.GetValue()
            #sPrjLong=self.txtPrjLong.GetValue()
            #sPrjDesc=self.txtPrjDesc.GetValue()
            #self.doc.setNodeText(node,'name',sPrjName)
            #self.doc.setNodeText(node,'longname',sPrjLong)
            #self.doc.setNodeText(node,'description',sPrjDesc)
            self.viTag.GetNode(node)
            self.viNr.GetNode(node)
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            self.viCust.GetNode(node)
            self.viCustShort.GetNode(node)
            self.viCustLong.GetNode(node)
            self.viState.GetNode(node)
            self.viState.Enable(True)
            self.viState.SetNode(node)      # Refresh
            self.vdtStart.GetNode(node)
            self.vdtEnd.GetNode(node)
            self.objRegNode.SetClosed(node,self.chkClsd.GetValue())
        except:
            self.__logTB__()
    def __Close__(self):
        # add code here
        self.viName.Close()
        self.viDesc.Close()
        self.viCust.Close()
        self.vdtStart.Close()
        self.vdtEnd.Close()
        self.viState.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def OnVgpPrjInfoSize(self, event):
        #self.Update()
        #self.Refresh()
        event.Skip()

    def OnViCustVtinputTreeChanged(self, event):
        event.Skip()
        self.__enableCust__()
        self.__showCust__()

    def OnVdtEndVttimeInputDatetimeChanged(self, event):
        event.Skip()
        self.__showDuration__()

    def OnVdtStartVttimeInputDatetimeChanged(self, event):
        event.Skip()
        self.__showDuration__()
