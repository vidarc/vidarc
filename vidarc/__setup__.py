#----------------------------------------------------------------------------
# Name:         __setup__.py
# Purpose:      package setup script for plugin generation.
#
# Author:       Walter Obweger
#
# Created:      20060717
# CVS-ID:       $Id: __setup__.py,v 1.2 2007/11/17 10:36:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys
from __init__ import *
from __config__ import *
from vidarc.build import *

version = '%s.%s.%s'%(VER_MAJOR,VER_MINOR,VER_RELEASE)
setup_args = {
    'name': DESCRIPTION,
    'version': version,
    'description': "%s %s " % (DESCRIPTION,version),
    'author': AUTHOR,
    'author_email': AUTHOR_EMAIL,
    'maintainer': MAINTAINER,
    'maintainer_email': MAINTAINER_EMAIL,
    'url': URL,
    'license': LICENSE,
    'long_description': LONG_DESCRIPTION,
    'zipfile':'vidarc.%s.zip'%version,
    'vidarcfile':'vidarc.%s.vidarc'%version,
    'releasefile':'vidarc.%s.zip'%version,
    'options':{'vidarc':{'pubkey':'../../install/step08/default.pub',
                        'licencedir':'../../../licences',
                        'mid':'test phrase'},
        },
    'packages': [
#        "mod01",
        ],
    'cmdclass': {
        'vidarc': build_vidarc_plugin,
        },
    }


if __name__ == '__main__':
    print sys.argv
    setup(**setup_args)
