#----------------------------------------------------------------------------
# Name:         vcCfg.py
# Purpose:      
# Author:       Walter Obweger
#
#   requirements:
#       mixin logging feature (__logDebug__,__logTB__ required for self)
#
# Created:      20100302
# CVS-ID:       $Id: vcCfg.py,v 1.2 2012/01/15 13:47:36 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,types
import vLogFallBack

class vcCfg:
    def __init__(self,lBase,dDft):
        self._dCfg={}
        self._dDft=dDft
        self._lBase=[s[:] for s in lBase]
    def setCfgDoc(self,docCfg):
        self.docCfg=docCfg
    def buildCfgDict(self):
        self.__logDebug__(''%())
        self._dCfg=self.getCfgDataDict(self._lBase)
        self.setCfgDft(self._dDft,self._dCfg)
    def setCfgDft(self,dDft,d=None):
        if d is None:
            d=self._dDft
        self.__logDebug__('dCfg;%s;dDft;%s'%(self.__logFmt__(self._dCfg),
                self.__logFmt__(dDft)))
        def _setCfgDft(d,dft):
            for k,v in dft.iteritems():
                if k not in d:
                    d[k]=v
                elif type(v)==types.DictType:
                    if type(d[k])==types.DictType:
                        _setCfgDft(d[k],v)
                    else:
                        d[k]=v
                        vLogFallBack.logWarn('k:%s replaced'%(k),__name__)
        _setCfgDft(d,dDft)
    def getCfgVal(self,l,d=None,fallback=None,funcConv=None,**kwargs):
        try:
            if d is None:
                d=self._dCfg
            self.__logDebug__('d;%s'%(self.__logFmt__(d)))
            for k in l:
                if k in d:
                    d=d[k]
                else:
                    return fallback
            if funcConv is None:
                return d
            else:
                return funcConv(d,**kwargs)
        except:
            vLogFallBack.logTB(__name__)
            return fallback
    def setCfgVal(self,v,l,d=None,fallback=None,funcConv=None,**kwargs):
        try:
            if d is None:
                d=self._dCfg
            dd=d
            self.__logDebug__('d;%s'%(self.__logFmt__(d)))
            for k in l:
                if k not in d:
                    d[k]={}
                dd=d
                d=d[k]
            if funcConv is None:
                dd[l[-1]]=v
            else:
                try:
                    dd[l[-1]]=funcConv(v,**kwargs)
                except:
                    if fallback is None:
                        del dd[l[-1]]
                    else:
                        dd[l[-1]]=fallback
            return 0
        except:
            vLogFallBack.logTB(__name__)
            return -1
    def _getCfgDataDict(self,l,node=None):
        try:
            if node is None:
                if l is not None:
                    node=self.docCfg.getChildByLstForced(None,l)
            else:
                if l is not None:
                    node=self.docCfg.getChildByLstForced(node,l)
            if node is None:
                return {}
            d={}
            lM=None
            sTag=None
            for c in self.docCfg.getChilds(node):
                    sTagName=self.docCfg.getTagName(c)
                    if lM is None:
                        if sTagName in d:
                            lM=[d]
                            sTag=sTagName
                            d={}
                    else:
                        if sTag==sTagName:
                            lM.append(d)
                            d={}
                    if self.docCfg.hasChilds(c):
                        dd=self._getCfgDataDict(None,c)
                        d[sTagName]=dd
                    else:
                        d[sTagName]=self.docCfg.getText(c)
            if lM is None:
                return d
            else:
                lM.append(d)
                return lM
        except:
            vLogFallBack.logTB(__name__)
            return {}
    def getCfgDataDict(self,l):
        try:
            self.__logDebug__(''%())
            if self.docCfg is None:
                return {}
            self.docCfg.acquire('dom')
            #node=self.docCfg.getChildByLstForced(None,self._lBase)
            d=self._getCfgDataDict(l,None)
            #cfgNode=self.docCfg.getChildByLstForced(None,l)
            #if cfgNode is not None:
            #    for c in self.docCfg.getChilds(cfgNode):
            #        sTagName=self.docCfg.getTagName(c)
            #        if self.docCfg.hasChilds(c):
            #            dd=self.__getPluginCfg__(l+[sTagName])
            #            d[sTagName]=dd
            #        else:
            #            d[sTagName]=self.docCfg.getText(c)
            self.docCfg.release('dom')
            return d
        except:
            vLogFallBack.logTB(__name__)
            self.docCfg.release('dom')
            return {}
    def setCfgData(self,l=None,d=None):
        try:
            if self.docCfg is None:
                return 0
            import vidarc.tool.InOut.fnUtil as fnUtil
            self.docCfg.acquire('dom')
            cfgNode=self.docCfg.getChildByLstForced(None,l or self._lBase)
            for c in self.docCfg.getChilds(cfgNode):
                self.docCfg.deleteNode(c,cfgNode)
            def setCfgData(cfgNode,d):
                keys=d.keys()
                keys.sort()
                for k in keys:
                    kr=fnUtil.replaceSuspectChars(k)
                    if type(d[k])==types.DictionaryType:
                        if len(d[k])>0:
                            #cc=self.docCfg.getChildForced(cfgNode,k)
                            cc=self.docCfg.createSubNode(cfgNode,k)
                            setCfgData(cc,d[k])
                    elif type(d[k])==types.ListType:
                        if len(d[k])>0:
                            cc=self.docCfg.getChildForced(cfgNode,k)
                            for dd in d[k]:
                                setCfgData(cc,dd)
                    else:
                        self.docCfg.setNodeText(cfgNode,kr,unicode(d[k]))
            setCfgData(cfgNode,d or self._dCfg)
            self.docCfg.release('dom')
            self.docCfg.AlignNode(cfgNode)
            #self.docCfg.Save()
            return 0
        except:
            vLogFallBack.logTB(__name__)
            self.docCfg.release('dom')
            #vtLog.vtLngTB(self.GetName())
            return -1
