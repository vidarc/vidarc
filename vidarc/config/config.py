
import sys, os, glob, fnmatch, tempfile
from distutils.core      import setup, Extension
from distutils.file_util import copy_file
from distutils.dir_util  import mkpath
from distutils.dep_util  import newer
from distutils.spawn     import spawn

import distutils.command.install
import distutils.command.install_data
import distutils.command.install_headers
import distutils.command.clean

#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------

VER_MAJOR        = 2      # The first three must match wxWidgets
VER_MINOR        = 6
VER_RELEASE      = 1
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.

DESCRIPTION      = "ABC Status Editor"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "customized project"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "recipe"

LONG_DESCRIPTION = """\
"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: MacOS X :: Carbon
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""

def build_options(sDN,*args,**kwargs):
    
    #----------------------------------------------------------------------
    # build options file
    #----------------------------------------------------------------------
    #try:
    #    i=VIDARC_IMPORT
    #except:
    #    VIDARC_IMPORT=1
    print kwargs
    keys=kwargs.keys()
    keys.sort()
    build_options_template = """"""
    for k in keys:
        build_options_template += """%s=%d
"""%(k,kwargs[k])
    #build_options_template = """
    #VIDARC_IMPORT=%d
    #""" % (VIDARC_IMPORT)
    
    #try: 
    #    from build_options import *
    #except:
    if 1:
        build_options_file = os.path.join(sDN, "build_options.py")
        if not os.path.exists(build_options_file):
            try:
                myfile = open(build_options_file, "w")
                myfile.write(build_options_template)
                myfile.close()
            except:
                print "WARNING: Unable to create build_options.py."
    

