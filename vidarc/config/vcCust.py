#----------------------------------------------------------------------------
# Name:         vcCust.py
# Purpose:      customize imports
#
# Author:       Walter Obweger
#
# Created:      20060604
# CVS-ID:       $Id: vcCust.py,v 1.5 2010/03/03 02:17:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

dImp={}
import sys

def set2Import(name,flag,what='__NODE_GUI__'):
    global dImp
    d=dImp
    strs=name.split('.')
    for s in strs:
        if not d.has_key(s):
            d[s]={}
        d=d[s]
    d[what]=flag
def is2Import(name,what='__NODE_GUI__',fallback=True):
    global dImp
    d=dImp
    bIs2Import=fallback
    strs=name.split('.')
    for s in strs:
        #print s,what,d
        #import vidarc.tool.log.vtLog as vtLog
        #vtLog.vtLngCur(vtLog.DEBUG,'keys:%s;what:%s;dict:%s'%(s,what,vtLog.pformat(d)),__name__)
        if s in d:
            d=d[s]
        else:
            break
        if what in d:
            bIs2Import=d[what]
    return bIs2Import
    try:
        return d[what]
    except:
        return fallback

NET_DFT_XML_SRV_HOST='vidarc'
NET_DFT_XML_SRV_PORT=50000
NET_DFT_FILE_SRV_HOST='vidarc'
NET_DFT_FILE_SRV_PORT=50006
NET_DFT_MSG_SRV_HOST='vidarc'
NET_DFT_MSG_SRV_PORT=50004
NET_DFT_PLGIN_SRV_HOST='vidarc'
NET_DFT_PLGIN_SRV_PORT=50002

USR_MODE_ADVANCED  = False
USR_LOGIN_EXTENDED = False
USR_LOCAL_DN       = None

def getDefaultHostPortPerNetAppl(appl):
    try:
        if appl=='vMsg':
            return NET_DFT_MSG_SRV_HOST,NET_DFT_MSG_SRV_PORT
        elif appl=='vPlugIn':
            return NET_DFT_PLGIN_SRV_HOST,NET_DFT_PLGIN_SRV_PORT
        elif appl=='vFile':
            return NET_DFT_FILE_SRV_HOST,NET_DFT_FILE_SRV_PORT
        else:
            return NET_DFT_XML_SRV_HOST,NET_DFT_XML_SRV_PORT
    except:
        return 'localhost',50000

def initDefaultLocalDN():
    global USR_LOCAL_DN
    import os
    try:
        import sys
        if sys.platform.startswith('win'):
            sDN=os.getenv('APPDATA')
            if len(sDN)>0:
                USR_LOCAL_DN=os.path.join(sDN,'VIDARC')
                return
    except:
        pass
    USR_LOCAL_DN=os.path.join(os.path.expanduser('~'),'VIDARC')
initDefaultLocalDN()

def getCmdLineOpt(sShort,sLong,sDft):
    try:
        def find(sOpt):
            iL=len(sOpt)
            for i,a in enumerate(sys.argv):
                if a.startswith(sOpt):
                    if sDft is not None:
                        if len(a)>iL:
                            if a[iL]=='=':
                                return a[iL+1:]
                        else:
                            s=sys.argv[i+1]
                            if s[0]!='-':
                                return s
                    else:
                        return True
            return None
        ret=find(sShort)
        if ret:
            return ret
        ret=find(sLong)
        if ret:
            return ret
    except:
        pass
    return sDft or False
