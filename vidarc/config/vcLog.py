#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20090519
# CVS-ID:       $Id: vcLog.py,v 1.1 2009/05/19 19:54:40 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

dLog={}
def set2Log(name,flag,what='__VERBOSE__'):
    global dLog
    d=dLog
    strs=name.split('.')
    for s in strs:
        if not d.has_key(s):
            d[s]={}
        d=d[s]
    d[what]=flag
def is2Log(name,what='__VERBOSE__',fallback=False):
    global dLog
    d=dLog
    bIs2=fallback
    strs=name.split('.')
    for s in strs:
        if s in d:
            d=d[s]
        else:
            break
        if what in d:
            bIs2=d[what]
    return bIs2
