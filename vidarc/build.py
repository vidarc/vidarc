from distutils.core import setup, Extension
from distutils.command.build_scripts import build_scripts
from distutils.command.install_data import install_data
from distutils.ccompiler import new_compiler
from distutils.errors import CompileError
from distutils.command.build_ext import build_ext
from distutils import sysconfig
_py_suffixes = ['.py', '.pyo', '.pyc', '.pyw']

import vidarc.install.licBuild.__build__ as vidBuild

import distutils.dist, distutils.core
class Distribution(distutils.dist.Distribution):
    def __init__(self, attrs):
        self.com_server = attrs.pop("com_server", [])
        self.service = attrs.pop("com_server", [])
        self.windows = attrs.pop("com_server", [])
        self.console = attrs.pop("com_server", [])
        self.isapi = attrs.pop("isapi", [])
        self.dist_dir = 'dist'
        self.zipfile = attrs.pop("zipfile", "library.zip")
        self.vidarcfile = attrs.pop("vidarcfile", "library.vidarc")
        self.releasefile = attrs.pop("releasefile", "release.zip")
        import pprint
        pp=pprint.PrettyPrinter(indent=2)
        pp.pprint(attrs)
        self.pubkey = attrs.pop("pubkey", "default.pub")
        pp.pprint(attrs)
        distutils.dist.Distribution.__init__(self, attrs)

distutils.core.Distribution = Distribution

version = '0.0.1'
setup_args = {
    'name': "VIDARC Tool",
    'version': version,
    'description': "VIDARC %s " % version,
    'author': "VIDARC Automation GmbH",
    'author_email': "office@vidarc.com",
    'maintainer': "Walter Obweger",
    'maintainer_email': "office@vidarc.com",
    'url': "http://www.vidarc.com/",
    'license': "VIDARC license",
    'long_description': """\
""",
    'dist_dir':'dist',
    'zipfile':'vidarc.tool.%s.zip'%version,
    'vidarcfile':'vidarc.tool.%s.vidarc'%version,
    'releasefile':'vidarc.tool.%s.zip'%version,
    'pubkey':'default.pub',
    'options':{'vidarc':{'pubkey':'default.pub',
                        'mid':'test phrase'},
        },
    'packages': [
        "mod01",
        ],
    'cmdclass': {
        'build_scripts': vidBuild.build_vidarc,
        },
    }

build_vidarc_plugin=vidBuild.build_vidarc_plugin

if __name__ == '__main__':
    setup(**setup_args)
