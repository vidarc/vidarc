��          |      �             !     =     T     i          �     �     �     �     �  �  �  �  �     !     =     [  !   t  ,   �  0   �     �  "        (     .  �  2                         
          	                        VIDARC Tools EmlThunderBird export as message file export as text plain export mailbox %s ... export mailbox %s done. export mailboxes done. exported %s %s find mailboxes at %s ... help log vEmlThunderBird module.

This module export messages in unix like mailboxes as single files.
Messages export formats:
  text file: text and attachments are exported to separate files
  message file: one single file per message (.eml file)

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
 Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-03-08 14:46+0100
PO-Revision-Date: 2005-09-20 14:49+0100
Last-Translator: Walter Obweger <walter.obweger@vidarc.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
 VIDARC Tools EmlThunderBird exportiere als Nachrichtdatei exportiere als Textdatei Exportiere Nachrichtenfach %s ... Exportiere Nachrichtenfach %s abgeschlossen. Exportieren der Nachrichtenf�cher abgeschlossen. exportiert %s %s Suche Nachrichtenf�cher auf %s ... Hilfe Log vEmlThunderBird Modul.

Dieses Modul exportiert Nachrichten in unix �hnlichen Nachrichtenf�chern als einzelne Dateien.
Nachrichten Exportformate:
  Textdatei: Text und Anh�nge werden separat in Dateien exportiert
  Nachrichtendatei: eine einzelne Datei pro Nachricht (.eml Datei)

Entworfen von VIDARC Automation GmbH, Walter Obweger.

Bitte besuchen Sie unsere Web Site http://www.vidarc.com.
Richten Sie Fragen und R�ckmeldungen an office@vidarc.com. 
Wir freuen uns von Ihnen zu h�ren.
 