#Boa:MDIChild:vEmlThunderBirdMDIFrame
#----------------------------------------------------------------------------
# Name:         vNetCfgMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080826
# CVS-ID:       $Id: vEmlThunderBirdMDIFrame.py,v 1.1 2008/12/23 12:30:04 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.tool.log.vtLog as vtLog

try:
    import vidarc.vTools.vEmlThunderBird.__init__ as plgInit
    from vidarc.vTools.vEmlThunderBird.vEmlThunderBirdMainPanel import vEmlThunderBirdMainPanel
    import vidarc.vTools.vEmlThunderBird.images as imgEmlThunderBird
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgEmlThunderBird.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgEmlThunderBird.getApplicationBitmap())
    return icon

DOMAINS=plgInit.DOMAINS
def getDomainTransLst(lang):
    vtLog.vtLngCur(vtLog.WARN,'use new plugin resolve style','import')
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    vtLog.vtLngCur(vtLog.WARN,'use new plugin resolve style','import')
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vEmlThunderBirdMDIFrame(parent)

[wxID_VEMLTHUNDERBIRDMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vEmlThunderBirdMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=wxID_VEMLTHUNDERBIRDMDIFRAME,
              name=u'vEmlThunderBirdMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 320), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vEmlThunderBird')
        self.SetClientSize(wx.Size(492, 293))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'EmlThunderBird',bToolBar=True)
        self.dCfg.update({'x':10,'y':10,'width':500,'height':300})
        
        try:
            self.pn=vEmlThunderBirdMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vContactPlugIn')
            #self.pn.OpenCfgFile('vCalendarCfg.xml')
        except:
            vtLog.vtLngTB(self.GetName())
    def __getPluginImage__(self):
        return imgEmlThunderBird.getPluginImage()
    def __getPluginBitmap__(self):
        return imgEmlThunderBird.getPluginBitmap()
    def __makeTitle__(self):
        pass
    def OpenFile(self,fn):
        pass
    def OpenCfgFile(self,fn=None):
        pass
    def OnMasterShutDown(self,evt):
        pass
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        if self.IsShown():
            self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
    def OnMainClose(self,event):
        #vMDIFrame.OnMainClose(self,event)
        event.Skip()
        self.GetCfgData()
