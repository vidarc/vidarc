#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20080826
# CVS-ID:       $Id: __init__.py,v 1.1 2008/12/23 12:30:04 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import images as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vEmlThunderBirdMDIFrame'
PLUGABLE_FRAME=True
DOMAINS=['tools','document','analysis']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
