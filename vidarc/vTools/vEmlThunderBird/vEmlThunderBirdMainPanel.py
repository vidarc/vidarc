#Boa:FramePanel:vEmlThunderBirdMainPanel
#----------------------------------------------------------------------------
# Name:         vEmlThunderBirdMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080826
# CVS-ID:       $Id: vEmlThunderBirdMainPanel.py,v 1.5 2016/02/05 08:03:52 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmThreadCtrl
import vidarc.tool.misc.vtmListCtrl
import wx.lib.filebrowsebutton
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import sys,os,os.path
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.InOut.fnUtil import shiftFile
    import vidarc.vTools.vEmlThunderBird.images as imgEmlThunderBird
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.vtThread import vtThreadWX
    import vidarc.tool.InOut.mailBox as mbox
    from vidarc.tool.InOut.listFiles import DirEntry
except:
    vtLog.vtLngTB('import')

def getAboutDescription():
    return _(u"""vEmlThunderBird module.

This module export messages in unix like mailboxes as single files.
Messages export formats:
  text file: text and attachments are exported to separate files
  message file: one single file per message (.eml file)

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")

def getPluginImage():
    return imgEmlThunderBird.getPluginImage()

[wxID_VEMLTHUNDERBIRDMAINPANEL, wxID_VEMLTHUNDERBIRDMAINPANELDBBEMAIL, 
 wxID_VEMLTHUNDERBIRDMAINPANELDBBOUT, wxID_VEMLTHUNDERBIRDMAINPANELLSTLOG, 
 wxID_VEMLTHUNDERBIRDMAINPANELTGEXPMSG, wxID_VEMLTHUNDERBIRDMAINPANELTGEXPTXT, 
 wxID_VEMLTHUNDERBIRDMAINPANELTHDCTRL, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vEmlThunderBirdMainPanel(wx.Panel):
    LST_CFGOBJ_MAP=[('dbbEMail','srcDN',False),
            ('dbbOut','outDN',False),
            ('tgExpTxt','exportAsTxt',True),
            ('tgExpMsg','exportAsMsg',True)
            ]
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsFiles_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstLog, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilesBt, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbEMail, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)
        parent.AddWindow(self.dbbOut, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.thdCtrl, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.ALIGN_TOP)
        parent.AddSizer(self.bxsFiles, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)

    def _init_coll_bxsFilesBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgExpTxt, 0, border=0, flag=0)
        parent.AddWindow(self.tgExpMsg, 0, border=4, flag=wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsFiles = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilesBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsFiles_Items(self.bxsFiles)
        self._init_coll_bxsFilesBt_Items(self.bxsFilesBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VEMLTHUNDERBIRDMAINPANEL,
              name=u'vEmlThunderBirdMainPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(342, 259), style=wx.TAB_TRAVERSAL)
        self.SetAutoLayout(True)

        self.dbbEMail = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose Thunderbird email directory',
              id=wxID_VEMLTHUNDERBIRDMAINPANELDBBEMAIL,
              labelText=u'Select email directory:', parent=self, pos=wx.Point(0,
              0), size=wx.Size(334, 29), startDirectory='.', style=0)

        self.dbbOut = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose export directory',
              id=wxID_VEMLTHUNDERBIRDMAINPANELDBBOUT,
              labelText=u'Select export directory:', parent=self,
              pos=wx.Point(0, 37), size=wx.Size(334, 29), startDirectory='.',
              style=0)

        self.lstLog = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VEMLTHUNDERBIRDMAINPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 114),
              size=wx.Size(299, 114), style=wx.LC_REPORT)

        self.thdCtrl = vidarc.tool.misc.vtmThreadCtrl.vtmThreadCtrl(id=wxID_VEMLTHUNDERBIRDMAINPANELTHDCTRL,
              name=u'thdCtrl', parent=self, pos=wx.Point(4, 66),
              size=wx.Size(326, 44), style=0)

        self.tgExpTxt = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Note),
              id=wxID_VEMLTHUNDERBIRDMAINPANELTGEXPTXT, name=u'tgExpTxt',
              parent=self, pos=wx.Point(303, 114), size=wx.Size(31, 30),
              style=0)
        self.tgExpTxt.SetToolTipString(_(u'export as text plain'))
        self.tgExpTxt.Bind(wx.EVT_BUTTON, self.OnTgExpTxtButton,
              id=wxID_VEMLTHUNDERBIRDMAINPANELTGEXPTXT)

        self.tgExpMsg = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.EMail),
              id=wxID_VEMLTHUNDERBIRDMAINPANELTGEXPMSG, name=u'tgExpMsg',
              parent=self, pos=wx.Point(303, 148), size=wx.Size(31, 30),
              style=0)
        self.tgExpMsg.SetToolTipString(_(u'export as message file'))
        self.tgExpMsg.Bind(wx.EVT_BUTTON, self.OnTgExpMsgButton,
              id=wxID_VEMLTHUNDERBIRDMAINPANELTGEXPMSG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vEmlThunderBird')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.lstLog.InsertColumn(0,'',format=wx.LIST_FORMAT_RIGHT,width=20)
        self.lstLog.InsertColumn(1,_(u'log'),format=wx.LIST_FORMAT_LEFT,width=80)
        self.lstLog.SetStretchLst([(1,-1)])
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        #self.dImg['qst']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        self.lstLog.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.widLog=vtLog.GetPrintMsgWid(self)
        
        self.xdCfg=vtXmlDom(appl='vEmlThunderBirdCfg',audit_trail=False)
        self.bBlock=True
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
        self.xdCfg.Save(fn)
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
        
        for sObjName,sCfgKey,bBoolean in self.LST_CFGOBJ_MAP:
            try:
                if sCfgKey in self.dCfg:
                    if bBoolean==True:
                        if self.dCfg[sCfgKey] in ['1','true']:
                            self.__dict__[sObjName].SetValue(True)
                        else:
                            self.__dict__[sObjName].SetValue(False)
                    else:
                        self.__dict__[sObjName].SetValue(self.dCfg[sCfgKey])
            except:
                vtLog.vtLngTB(self.GetName())
        #if self.tgExpMsg.GetValue():
        #    self.tgExpTxt.SetValue(False)
    def GetCfgData(self):
        for sObjName,sCfgKey,bBoolean in self.LST_CFGOBJ_MAP:
            try:
                if bBoolean==True:
                    if self.__dict__[sObjName].GetValue():
                        self.dCfg[sCfgKey]=1
                    else:
                        self.dCfg[sCfgKey]=0
                else:
                    self.dCfg[sCfgKey]=self.__dict__[sObjName].GetValue()
            except:
                vtLog.vtLngTB(self.GetName())
        
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetFN(self):
        try:
            iAct,d=self.__selCfg__()
            if iAct<0:
                return ''
            sSys=platform.system()
            sNode=platform.node()
            sDN=self.dbbOut.GetValue()
            d['outDN']=sDN
            sFN=u'_'.join([sNode,sSys,'.'.join([d['name'],'cfg'])])
            sFullFN=os.path.join(sDN,sFN)
            return sFullFN
        except:
            vtLog.vtLngTB(self.GetName())
            return sFullFN
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        return _(u'VIDARC Tools EmlThunderBird'),getAboutDescription(),version
    # ----------------------------------------
    # application
    # ----------------------------------------
    def AddLog(self,sLog,sKind='def'):
        try:
            if sLog is None:
                self.lstLog.DeleteAllItems()
                return
            if sKind in self.dImg:
                img=self.dImg[sKind]
            else:
                img=-1
            idx=self.lstLog.InsertImageStringItem(0,'',img)
            self.lstLog.SetStringItem(idx,1,sLog)
        except:
            vtLog.vtLngTB(self.GetName())
    def procDummy(self):
        pass
    def procMailBoxFind(self,sDN,l):
        try:
            iSkip=len(sDN)
            for sBaseDN,lDir,lFile in os.walk(sDN):
                sTmpDN=sBaseDN[iSkip:].replace('\\','/')
                for sFN in lFile:
                    if sFN.endswith('.msf'):
                        sFullFN='/'.join([sTmpDN,sFN])
                        l.append(sFullFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def procPostExport(self,sTitle,sFN):
        try:
            vtLog.PrintMsg(_(u'exported %s %s')%(sTitle,sFN),self.widLog)
            #self.AddLog(_(u'exported %s %s')%(sTitle,sFN),'ok')
        except:
            vtLog.vtLngTB(self.GetName())
    def procPostProgress(self,iPos,iSize):
        try:
            vtLog.SetProcBarVal(iPos,iSize,self.widLog)
        except:
            vtLog.vtLngTB(self.GetName())
    def procMailBoxExport(self,sSrcDN,sOutDN,l,iStyle):
        try:
            thd=self.thdCtrl.GetThread()
            if iStyle==0:
                #procExport=mbox.export
                procExport=mbox.exportPlain
            elif iStyle==1:
                procExport=mbox.exportEml
            else:
                def procExport(sFN,sDN,bDoNotOverride=False):
                    pass
            bDbg=vtLog.vtLngIsLogged(vtLog.DEBUG)
            l.sort()
            iRepeat=1
            if (iStyle & 0x1) != 0:
                iRepeat+=1
            if (iStyle & 0x2) != 0:
                iRepeat+=1
            iAct=0
            iCount=len(l)*max(1,iRepeat)
            
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #print l
                vtLog.vtLngCurWX(vtLog.DEBUG,'type l:%r'%(type(l)),self)
                #vtLog.vtLngCur(vtLog.DEBUG,'l:%s'%(vtLog.pformat(l)),self)
            thd.Post('proc',iAct,iCount)
            for sRelFN in l:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'l:%r'%(sRelFN),self)
                if thd.Is2Stop():
                    break
                sMailBoxFN=u''.join([sSrcDN,sRelFN[:-4]])
                sDN,sFN=os.path.split(sRelFN)
                vtLog.PrintMsg(_(u'export mailbox %s ...')%(sMailBoxFN),self.widLog)
                lTmpDN=sDN.split('/')
                lDN=[]
                for s in lTmpDN:
                    if s.endswith('.sdb'):
                        lDN.append(s[:-4])
                lDN.append(sFN[:-4])
                sRelDN=u'/'.join(lDN)
                vtLog.PrintMsg(_(u'export mailbox %s ...')%(sRelDN),self.widLog)
                thd.Post('proc',_(u'export mailbox %s ...')%(sRelDN),None)
                sDN=os.path.join(sOutDN,sRelDN)
                try:
                    os.makedirs(sDN)
                except:
                    pass
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;sMailBoxFN:%s'%(sDN,
                                sMailBoxFN),self)
                #procExport(sMailBoxFN,sDN,bDoNotOverride=False)
                iAct+=1
                thd.Post('proc',iAct,iCount)
                if (iStyle & 0x1) != 0:
                    mbox.exportPlain(sMailBoxFN,sDN,bDoNotOverride=False,
                            bDbg=0,
                            func=self.procPostExport,
                            funcProc=self.procPostProgress)
                    iAct+=1
                    thd.Post('proc',iAct,iCount)
                    vtLog.SetProcBarVal(0,100)
                if (iStyle & 0x2) != 0:
                    mbox.exportEml(sMailBoxFN,sDN,bDoNotOverride=False,
                            func=self.procPostExport,
                            funcProc=self.procPostProgress)
                    iAct+=1
                    thd.Post('proc',iAct,iCount)
                    vtLog.SetProcBarVal(0,100)
                
                self.AddLog(_(u'export mailbox %s done.')%(sMailBoxFN),'ok')
        except:
            vtLog.vtLngTB(self.GetName())
        vtLog.PrintMsg(_(u'export mailboxes done.'),self.widLog)
    def Start(self):
        try:
            self.lstLog.DeleteAllItems()
            thd=self.thdCtrl.GetThread()
            thd.Start()
            sDN=self.dbbEMail.GetValue()
            sDN=sDN.replace('\\','/')
            vtLog.PrintMsg(_(u'find mailboxes at %s ...')%(sDN),self.widLog)
            lEMail=[]
            self.thdCtrl.Do(self.procMailBoxFind,sDN,lEMail)
            sOutDN=self.dbbOut.GetValue()
            sOutDN=sOutDN.replace('\\','/')
            iStyle=0
            #iStyle=1
            if self.tgExpTxt.GetValue():
                iStyle|=0x1
            if self.tgExpMsg.GetValue():
                iStyle|=0x2
            self.thdCtrl.Do(self.procMailBoxExport,sDN,sOutDN,lEMail,iStyle)
        except:
            vtLog.vtLngTB(self.GetName())
    def Stop(self):
        try:
            #self.thdRead.Stop()
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OnThreadProc(self,evt):
        evt.Skip()
        #if evt.GetCount() is None:
        #    self.pnLog.AddLog(evt.GetAct())
    def OnThreadAborted(self,evt):
        evt.Skip()
    def OnThreadFin(self,evt):
        evt.Skip()

    def OnTgExpMsgButton(self, event):
        event.Skip()
        #if self.tgExpMsg.GetValue():
        #    self.tgExpTxt.SetValue(False)

    def OnTgExpTxtButton(self, event):
        event.Skip()
        #if self.tgExpMsg.GetValue():
        #    self.tgExpTxt.SetValue(False)
