#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vEmlThunderBirdAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090108
# CVS-ID:       $Id: vEmlThunderBirdAppl.py,v 1.3 2010/03/03 02:17:19 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback
import vSystem
import wx
import vidarc.vApps.common.vSplashFrame as vSplashFrame
import vtLogDef
import vtLgBase

import images_splash
modules ={u'vEmlThunderBirdMainFrame': [0, '', u'vEmlThunderBirdMainFrame.py']}

OPT_SHORT,OPT_LONG='l:f:c:h',['lang=','file=','config=','log=','help']

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        wx.App.__init__(self,num)
    def RedirectStdio(self, filename=None):
        """Redirect sys.stdout and sys.stderr to a file or a popup window."""
        if filename:
            sys.stderr = open(filename, 'w')
            sys.stdout = open(filename.replace('.err.','.out.'), 'w')
            #if self.iLogLv==vtLogDef.DEBUG or self.iLogLv==vtLogDef.INFO:
            #    sys.stdout = self.outputWindowClass()
            #else:
            #    sys.stdout = sys.stderr
        else:
            self.stdioWin = self.outputWindowClass()
            sys.stdout = sys.stderr = self.stdioWin
    def OnInit(self):
        self.RedirectStdio(vSystem.getErrFN(self.sAppl))
        
        vtLgBase.initAppl(OPT_SHORT,OPT_LONG,'vEmlThunderBirdDir')
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        import __init__
        if __init__.VIDARC_IMPORT:
            d={'label':_(u'  import library ...'),
                'eval':'__import__("vidImp")'}
            actionList.append(d)
        else:
            d={'label':_(_(u'  import library ...')),
                'eval':'0'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc',
                'eval':'__import__("vidarc",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool',
                'eval':'__import__("vidarc.tool",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.config',
                'eval':'__import__("vidarc.config",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.lang',
                'eval':'__import__("vidarc.tool.lang",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.InOut',
                'eval':'__import__("vidarc.tool.InOut",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.art',
                'eval':'__import__("vidarc.tool.art",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.vTools',
                    'eval':'__import__("vidarc.vApps",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':u'  import vEmlThunderBirdMain ...',
            #'eval':'__import__("vidarc.vTools.vBrowseDir.vBrowseDirMainFrame")'}
            'eval':'__import__("vidarc.vTools.vEmlThunderBird.vEmlThunderBirdMainFrame",globals(),locals(),["vEmlThunderBirdMainFrame"])'}
        actionList.append(d)
        #d={'label':u'  Create vBrowseDirMain ...',
        #    'eval':'self.res[1].vTools.vBrowseDir.vBrowseDirMainFrame'}
        #actionList.append(d)
        d={'label':u'  Create vEmlThunderBirdMain ...',
            #'eval':'getattr(self.res[-1],"create")'}
            'eval':'self.res[-1].create'}
        actionList.append(d)
        d={'label':u'  Create vEmlThunderBirdMain ...',
            'eval':'self.res[-1](None)'}
        actionList.append(d)
        
        d={'label':u'  Finished.'}
        actionList.append(d)
        self.iRes=len(actionList)-2
        
        d={'label':u'  Open Config (%s)...'%self.cfgFN,
            'eval':'self.res[%d].OpenCfgFile("%s")'%(self.iRes,self.cfgFN)}
        actionListPost.append(d)
        
        #d={'label':u'  Open File (%s)...'%self.fn,
        #    'eval':'self.res[%d].OpenFile("%s")'%(self.iRes,self.fn)}
        #actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'Tools','EmlThunderBird',
            images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        
        return True
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        #if evt.GetIdx()==4:
        #    self.main=self.splash.res[3]
        #    self.SetTopWindow(self.main)
        #    if self.fn is not None:
        #        self.main.OpenFile(self.fn)
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        try:
            import vidarc.tool.log.vtLog as vtLog
            vtLog.vtLngSetLevel(self.splash.GetLogLv())
            vtLogDef.LEVEL=self.splash.GetLogLv()
        except:
            pass
        try:
            self.main=self.splash.res[self.iRes]
            wx.CallAfter(self.SetTopWindow,self.main)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(vtLog.InitMsgWid)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(self.main.Show)
        except:
            traceback.print_exc()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[self.iRes]
            self.main.Destroy()
        except:
            traceback.print_exc()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main():
    fn=None
    cfgFN='vEmlThunderBirdCfg.xml'
    iLogLv=vtLogDef.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],OPT_SHORT,OPT_LONG)
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL

    except:
        vtLgBase.initAppl(OPT_SHORT,OPT_LONG,'vEmlThunderBird')
        showHelp()
        traceback.print_exc()
    vtLogDef.LEVEL=iLogLv
    application = BoaApp(0,'vEmlThunderBird',cfgFN,fn,iLogLv,60504)
    application.MainLoop()

if __name__ == '__main__':
    main()
