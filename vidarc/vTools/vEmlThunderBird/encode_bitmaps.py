#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080826
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2008/12/23 12:30:04 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/ThunderBird01_32.ico      images.py",
    "-a -u -n Plugin        img/ThunderBird01_16.png      images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

