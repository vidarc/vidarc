#----------------------------------------------------------------------------
# Name:         vExplorerFilePropertiesDlg
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100614
# CVS-ID:       $Id: vExplorerFilePropertiesDlg.py,v 1.2 2012/01/16 12:38:07 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import os.path
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.InOut.vtInOutFileInfo as vtiFI
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerFilePropertiesDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('lblRg',       (0,0),(1,1),{'name':'lblDN','label':_(u'directory')},None),
            ('txtRd',       (0,1),(1,2),{'name':'txtDN','value':u''},None),
            ('lblRg',       (1,0),(1,1),{'name':'lblFN','label':_(u'filename')},None),
            ('txtRd',       (1,1),(1,2),{'name':'txtFN','value':u''},None),
            ('lblRg',       (2,0),(1,1),{'name':'lblSize','label':_(u'size')},None),
            ('txtRdRg',     (2,1),(1,2),{'name':'txtSize','value':u''},None),
            ('lblRg',       (3,0),(1,1),{'name':'lblCreate','label':_(u'create')},None),
            ('txtRd',       (3,1),(1,2),{'name':'txtCreate','value':u''},None),
            ('lblRg',       (4,0),(1,1),{'name':'lblMod','label':_(u'modification')},None),
            ('txtRd',       (4,1),(1,2),{'name':'txtMod','value':u''},None),
            ('lblRg',       (5,0),(1,1),{'name':'lblAccess','label':_(u'access')},None),
            ('txtRd',       (5,1),(1,2),{'name':'txtAccess','value':u''},None),
            ('lblRg',       (6,0),(1,1),{'name':'lblMD5','label':_(u'MD5')},None),
            ('txtRd',       (6,1),(1,2),{'name':'txtMD5','value':u''},None),
            ('tgBmp',       (6,3),(1,1),{'name':'cbMD5','bitmap':vtArt.getBitmap(vtArt.Calc),},
                                            {'btn':self.OnCalcMD5}),
            ('lblRg',       (7,0),(1,1),{'name':'lblAttr','label':_(u'attribute')},None),
            ('lstExt',      (7,1),(2,2),{'name':'lstAttr',},None),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExplorerSynchResult',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='frmSngCls',
                bmp=bmp or imgExplorer.getPropertiesBitmap(),
                title=title or _(u'VIDARC Explorer File Properties'),
                pnPos=None,pnSz=(200,200),pnStyle=None,
                pnName='pnExplorerFileProperties',
                lGrowCol=[(1,1),(2,1)],lGrowRow=[(7,2)],
                bEnMark=False,bEnMod=False,lWid=lWid)
    def Clear(self):
        if self.IsMainThread()==False:
            return
        self.txtDN.SetValue('')
        self.txtMD5.SetValue('')
        self.txtCreate.SetValue('')
        self.txtMod.SetValue('')
        self.txtAccess.SetValue('')
        self.txtFN.SetValue('')
        self.txtSize.SetValue('')
    def SetFN(self,sFN,sDN=None):
        try:
            if self.IsMainThread()==False:
                return
            if sFN is None:
                self.Clear()
                return
            if sDN is None:
                sDN,sTmpFN=vtiFI.split(sFN)
            else:
                sFN=vtiFI.join([sDN,sFN])
            if vtiFI.exists(sFN)==False:
                self.Clear()
            d=vtiFI.getFileInfoFull(sFN)
            sDN,sFN=vtiFI.split(sFN)
            self.txtDN.SetValue(sDN)
            self.txtDN.SetInsertionPointEnd()
            self.txtFN.SetValue(sFN)
            self.txtSize.SetValue(vtiFI.getSizeStr(d['size'],2))
            #self.txtMD5.SetValue(f.md5)
            self.txtCreate.SetValue(vtiFI.ftime2str(d['zCreate']))
            self.txtMod.SetValue(vtiFI.ftime2str(d['zMod']))
            self.txtAccess.SetValue(vtiFI.ftime2str(d['zAccess']))
        except:
            self.__logTB__()
            self.Clear()
    def OnCalcMD5(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
