#Boa:FramePanel:vExplorerPanel
#----------------------------------------------------------------------------
# Name:         vExplorerPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vExplorerPanel.py,v 1.21 2010/07/19 09:48:35 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,os,stat,types
import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.InOut.vtInOutFileInfo as vtiLF
    from vidarc.tool.InOut.vtInOutReadThread import vtInOutReadThread
    from vidarc.tool.InOut.vtInOutNotify import vtInOutNotifyLg
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmListCtrl import vtmListCtrl
    from vidarc.tool.misc.vtmLabelCtrl import vtmLabelCtrl
    from vidarc.tool.gui.vtgBmpStatic import vtgBmpStatic
    
    from vidarc.vTools.vExplorer.vExplorerFilePropertiesDlg import vExplorerFilePropertiesDlg
except:
    vLogFallBack.logTB(__name__)

import wx

vLogFallBack.logDebug('import;done',__name__)

class vExplorerPanel(vtgPanel):
    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[]
        lWid.append(('szBoxHor',        (0,0),(1,1),{'name':'bxsTitle',},[
            (vtgBmpStatic,    0,4,{'name':'lblSrc',
                    'key':['empty','src1','src2',],
                    'bitmap':[
                        vtArt.getBitmap(vtArt.Empty),
                        imgExplorer.getSrc1Bitmap(),
                        imgExplorer.getSrc2Bitmap(),]},None),
            (vtmLabelCtrl, 1,0,{
                'name':'lblTitle','value':_(u'default')},{
                    'focusSet':        self.OnLblTitleSetFocus,
                }),
            ]))
        lWid.append((vtmListCtrl,  (1,0),(2,1),{
                'name':'lstData',
                'cols':[(_(u'name'),-1,-1,3),
                            (_(u'size'),-2,80,3),
                            (_(u'mod'),-1,80,3),
                            ]},{
                    'lst_item_sel':    self.OnLstDataListItemSelected,
                    'lst_item_desel':  self.OnLstDataListItemDeselected,
                    'lfDbl':           self.OnLstDataLeftDclick,
                    'rgDbl':           self.OnLstDataRightDclick,
                    'mdDbl':           self.OnLstDataMiddleDclick,
                    'focusSet':        self.OnLstDataSetFocus,
                    }))
        vtgPanel.__init__(self,parent=parent,id=id,pos=pos,size=size,
                style=style,name=name,bEnMark=False,bEnMod=False,
                lWid=lWid,lGrowCol=[(0,1)],lGrowRow=[(1,1)])
        #self.lstData.Bind(wx.EVT_LIST_ITEM_SELECTED,
        #      self.OnLstDataListItemSelected, self.lstData)
        #self._init_ctrls(parent)
        #self.SetName(name)
        #vtgPanelMixinWX.__init__(self)
        self.thdNot=vtInOutNotifyLg(None,notify=(self.notifyInOut,(),{}))
        self.thdRead=vtInOutReadThread(self,bPost=True)
        self.lblSrc.SetValue(2)
        #iAlignFlag=wx.ALIGN_RIGHT
        #self.lstData.SetColumnWidth(1,80)
        #self.lstData.SetStretchLst([(0,-1)])
        self.iCol=0
        self.bAscending=True
        self.iStyle=1
        self.iSel=-1
        self.iExplorerIdx=-1
        self.lHierDN=[]
        self.oReg=None
        self.dlgProp=None
        self.SetActive()
    def IsBusy(self):
        self.__logDebug__(''%())
        for i in self.__do_call__('IsBusy'):
            if i==True:
                return True
        return False
    def ShutDown(self):
        self.__logDebug__(''%())
        self.__do_call__('ShutDown')
        self.__do_call__('Stop')
    def notifyInOut(self,iNotify,sNotify,sFN,zClk):
        if self.__isLogDebug__():
            self.__logDebug__('iNotify:%d;sNotify:%s;sFN:%s'%(iNotify,
                        sNotify,sFN))
        wx.CallAfter(self.NotifyInOut,iNotify,sNotify,sFN,zClk)
    def NotifyInOut(self,iNotify,sNotify,sFN,zClk):
        try:
            bDbg,bLog=False,False
            if self.__isLogDebug__():
                bLog=True
                if VERBOSE>10:
                    self.__logDebug__('iNotify:%d;sNotify:%s;sFN:%s;dCon:%s'%(iNotify,
                        sNotify,sFN,self.__logFmt__(self.dContense)))
                    bDbg=True
                else:
                    self.__logDebug__('iNotify:%d;sNotify:%s;sFN:%s'%(iNotify,
                        sNotify,sFN))
            iFound=-1
            dInfo=None
            dDN=self.dContense['dDN']
            if sFN in dDN:
                self.__logDebug__('sFN:%s in dDN'%(sFN))
                iFound=0
                dInfo=dDN[sFN]
            else:
                dFN=self.dContense['dFN']
                if sFN in dFN:
                    self.__logDebug__('sFN:%s in dFN'%(sFN))
                    iFound=1
                    dInfo=dFN[sFN]
            if iNotify==2:      # deleted
                if iFound>=0:
                    self.updateListItem(iFound,sFN,None)
                return
            if iNotify==4:      # renamed (posted for old name)
                if iFound>=0:
                    self.updateListItem(iFound,sFN,None)
                return
            d=vtiLF.readInfo(sFN,self.dContense['root'],None)
            if d is None:
                self.__logError__('sFN:%s;sDN:%s;read info failed'%(sFN,
                        self.dContense['root']))
                return
            if bLog:
                self.__logDebug__('iFound:%d;d;%s;dInfo;%s'%(iFound,
                        self.__logFmt__(d),self.__logFmt__(dInfo)))
            if 'type' in d:
                if iFound==0:
                    if (dInfo['type']=='dir') and (d['type']=='dir'):
                        dInfo.update(d)
                    else:
                        self.__logCritical__('sFN:%s;type mismatch;d;%s;dInfo:%s'%(sFN,
                                self.__logFmt__(d),self.__logFmt__(dInfo)))
                if iFound==-1:
                    if d['type']=='dir':
                        dDN[sFN]=d
                        self.updateListItem(0,sFN,d,bFound=False)
                    else:
                        self.__logCritical__('sFN:%s;type unknown;d;%s'%(sFN,
                                self.__logFmt__(d)))
            else:
                if iFound==1:
                    dInfo.update(d)
                    self.updateListItem(iFound,sFN,dInfo)
                if iFound==-1:
                    dFN[sFN]=d
                    self.updateListItem(1,sFN,d,bFound=False)
        except:
            self.__logTB__()
    def updateListItem(self,iType,sName,dVal,bFound=None):
        try:
            if self.IsMainThread()==False:
                pass
            bDbg,bLog=False,False
            if self.__isLogDebug__():
                bLog=True
                if VERBOSE>10:
                    self.__logDebug__('iType:%d;sName:%s;dVal:%s'%(iType,sName,
                            self.__logFmt__(dVal)))
                    bDbg=True
                else:
                    self.__logDebug__('iType:%d;sName:%s'%(iType,sName))
            bUpdate=False
            dDat=self.lstData.GetCacheData()
            if bFound is None:
                bFound=False
                iCltData=0
                #for iT,sN in self.lDat:#.append((1,sFN))
                for k,t in dDat.iteritems():
                    iT,sN=t
                    if iT==iType:
                        if cmp(sN,sName)==0:
                            bFound=True
                            iCltData=k
                            break
                if bDbg==True:
                    self.__logDebug__('iType:%d;sName:%s;bFound:%d;iCltData:%d;dVal:%s'%(iType,sName,bFound,
                            iCltData,self.__logFmt__(dVal)))
            else:
                if bDbg==True:
                    self.__logDebug__('iType:%d;sName:%s;bFound:%d;dVal:%s'%(iType,sName,bFound,
                            self.__logFmt__(dVal)))
                
            if bFound:
                w=self.lstData
                iL=w.GetItemCount()
                for iOfs in xrange(iL):
                    iD=w.GetItemData(iOfs)
                    if iD==iCltData:
                        bUpdate=True
                        if dVal is None:
                            #iData=self.lstData.GetItemData(iOfs)
                            #t=self.lDat[iData]
                            #self.lDat[iData]=(-1,t[1])
                            del dDat[iCltData]
                            self.lstData.DeleteItem(iOfs)
                            return
                        else:
                            sSz=vtiLF.getSizeStr(dVal['size'],2)
                            self.lstData.SetStringItem(iOfs,1,sSz,-1)
                            sMod=vtiLF.ftime2str(dVal['zMod'])
                            self.lstData.SetStringItem(iOfs,2,sMod,-1)
                        break
            else:
                if dVal is None:
                    self.__logError__('you are not supposed to be here')
                    return
                #iData=len(self.lDat)
                if iType==0:
                    iIdx=self.imgDict['elem']['dir'][0]
                    idx=self.lstData.InsertImageStringItem(sys.maxint,sName,iIdx)
                    #self.lstData.SetItemData(idx,iData)
                    self.lstData.SetItem(idx,-1,(0,sName))
                    #self.lDat.append((0,sName))
                    #iData+=1
                else:
                    iIdx=self.imgDict['elem']['empty'][0]
                    idx=self.lstData.InsertImageStringItem(sys.maxint,sName,iIdx)
                    sSz=vtiLF.getSizeStr(dVal['size'],2)
                    self.lstData.SetStringItem(idx,1,sSz,-1)
                    sMod=vtiLF.ftime2str(dVal['zMod'])
                    self.lstData.SetStringItem(idx,2,sMod,-1)
                    #self.lstData.SetItemData(idx,iData)
                    self.lstData.SetItem(idx,-1,(1,sName))
                    #self.lDat.append((1,sName))
        except:
            self.__logTB__()
    def SetExplorerIdx(self,iIdx):
        self.iExplorerIdx=iIdx
    def GetExplorerIdx(self):
        return self.iExplorerIdx
    def SetActive(self,bFlag=True):
        if self.IsMainThread()==False:
            return
        self.__logDebug__('bFlag:%d'%(bFlag))
        self.lblTitle.SetActive(bFlag=bFlag)
    def SetSrc(self,iSrc):
        self.lblSrc.SetValue(iSrc)
    def IsActive(self):
        return self.lblTitle.IsActive()
    def SetFocus(self):
        self.lstData.SetFocus()
    def ShowProperties(self):
        try:
            if self.dlgProp is None:
                self.dlgProp=vExplorerFilePropertiesDlg(self)
            if self.dlgProp is None:
                self.__logError__(''%())
            self.dlgProp.Show()
            self.RefreshProp()
        except:
            self.__logTB__()
    def SetImageListDict(self,imgLstTyp,imgDict,fIdxDelta):
        self.fIdxDelta=fIdxDelta
        #print imgDict
        self.imgDict=imgDict
        self.lstData.SetImageList(imgLstTyp,which=wx.IMAGE_LIST_SMALL,
                iImgEmptyIdx=self.imgDict['elem']['empty'][0],
                iImgSortAsc=self.imgDict['elem']['asc'][0],
                iImgSortDesc=self.imgDict['elem']['desc'][0])
        self.__setHeader__(self.iStyle,bForce=True)
        #self.lstDat.SetStretchLst([(2,-1)])
    def __clr__(self):
        self.iSel=-1
        self.dContense={}
        self.lDat=[]
        self.RefreshProp()
    def OnCompareNameAscending(self,iA,iB):
        tA=self.lDat[iA]
        tB=self.lDat[iB]
        i=cmp(tA[0],tB[0])
        if i==0:
            return cmp(tA[1].lower(),tB[1].lower())
        else:
            return i
        return 0
    def OnCompareNameDecending(self,iA,iB):
        tA=self.lDat[iA]
        tB=self.lDat[iB]
        i=cmp(tA[0],tB[0])
        if i==0:
            return cmp(tB[1].lower(),tA[1].lower())
        else:
            return i
        return 0
    def OnCompareDir(self,tA,tB,iCol,iOrder):
        i=cmp(tA[0],tB[0])
        if i==0:
            if tA[0]==0:
                sK='dDN'
            else:
                sK='dFN'
            dA=self.dContense[sK][tA[1]]
            dB=self.dContense[sK][tB[1]]
            if iCol==1:
                if ('size' in dA) and ('size' in dB):
                    iVal=dA['size']-dB['size']
                    if iOrder>0:
                        return iVal
                    else:
                        return -iVal
            if iCol==2:
                if ('zMod' in dA) and ('zMod' in dB):
                    iVal=cmp(dA['zMod'],dB['zMod'])
                    if iOrder>0:
                        return iVal
                    else:
                        return -iVal
            iVal=cmp(tA[1].lower(),tB[1].lower())
            if iOrder>0:
                return iVal
            else:
                return -iVal
        else:
            return i
        return 0
    def GetDN(self):
        try:
            if self.IsMainThread()==False:
                return None
            if self.iStyle==1:
                return self.lblTitle.GetValue()
            else:
                return self.dParDef
        except:
            self.__logTB__()
        return None
    def Check2Clear(self,sMnt):
        try:
            if self.IsMainThread()==False:
                return False
            if sMnt is None:
                return False
            if self.iStyle==1:
                sDN=self.lblTitle.GetValue()
                sMnt=sMnt.replace('\\','/')
                self.__logDebug__('sMnt:%s; sDN:%s'%(sMnt,sDN))
                if sDN.startswith(sMnt):
                    self.SetDN(None)
                    return True
        except:
            self.__logTB__()
        return False
    def GetSelection(self):
        try:
            lDN=[]
            lFN=[]
            if self.IsMainThread()==False:
                return lDN,lFN
            w=self.lstData
            iL=w.GetItemCount()
            sel=wx.LIST_STATE_SELECTED
            for iOfs in xrange(iL):
                if w.GetItemState(iOfs,sel)==sel:
                    iD=w.GetItemData(iOfs)
                    t=w.GetData(iD)
                    if VERBOSE>0:
                        self.__logDebug__([t])
                    tt=type(t)
                    if tt==types.TupleType:
                        #t=self.lDat[iD]
                        if t[0]==0:
                            #sK='dDN'
                            #self.Post('chdir',''.join([self.lblTitle.GetValue(),t[1][:]]))
                            lDN.append(t[1][:])#+'/')
                        else:
                            #sK='dFN'
                            #self.Post('open',''.join([self.lblTitle.GetValue(),t[1][:]]))
                            lFN.append(t[1][:])
                    elif tt==types.DictType:
                        if '__reg' in t:
                            self.__logDebug__('handle registered node')
                            lDN.append(t)
                            break
                        else:
                            lDN.append(t[0][:-1])
                            break
                    else:
                        lDN.append(t[0][:-1])
                        break
        except:
            self.__logTB__()
        return lDN,lFN
    def GetStyle(self):
        return self.iStyle
    def __setHeader__(self,iStyle,bForce=False,lHdr=None):
        try:
            if (self.iStyle==iStyle) and (bForce==False):
                return
            if lHdr is None:
                dFmt={
                    0:(
                        [(_(u'name'),-1,-1,1),(_(u'size'),-2,80,0),(_('%'),-3,120,0)],
                        False,
                        {-1:{'iSort':2,'func':self.OnCompareDir}},
                        ),
                    1:(
                        [(_(u'name'),-1,-1,2),(_(u'size'),-2,80,0),(_('mod'),-1,120,0)],
                        True,
                        {-1:{'iSort':3,'func':self.OnCompareDir}},
                        ),
                    }
                if iStyle in dFmt:
                    lHdr,bSort,dSortDef=dFmt[iStyle]
                else:
                    lHdr,bSort,dSortDef=dFmt[1]
            self.lstData.SetColumns(lHdr)
            self.lstData.SetSortDef(dSortDef)
            self.lstData.SetSort(0,**dSortDef[-1])
            self.iStyle=iStyle
        except:
            self.__logTB__()
    def SetRegNode(self,oReg,node=None):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__('tagname:%s'%(oReg.GetTagName()))
            lHdr,dSortDef,l=oReg.GetData4List(node,100)
            
            self.lstData.SetColumns(lHdr)
            self.lstData.SetSortDef(dSortDef)
            self.lstData.SetValue(l)
            self.lstData.SetSort(0,**dSortDef[-1])
            self.iStyle=100
        except:
            self.__logTB__()
    def SetValue(self,lVal,lHdr=None,dSortDef=None,oReg=None,dParDef=None):
        try:
            if self.IsMainThread()==False:
                return
            if VERBOSE>0:
                self.__logDebug__(['lVal',lVal,'dParDef',dParDef])
            else:
                self.__logDebug__(''%())
            self.lstData.Freeze()
            self.__clr__()
            self.lstData.DeleteAllItems()
            #self.__setHeader__(0,lHdr=lHdr)
            self.thdNot.SetDN(None)
            self.dParDef=dParDef
            self.lstData.SetColumns(lHdr)
            self.lstData.SetSortDef(dSortDef)
            self.lstData.SetValue(lVal)
            if 0 in dSortDef:
                self.lstData.SetSort(0,**dSortDef[0])
            if -1 in dSortDef:
                self.lstData.SetSort(0,**dSortDef[-1])
            if oReg is None:
                self.lblTitle.SetValue(_(u'computer'))
                self.iStyle=100
            else:
                self.lblTitle.SetValue(oReg.GetDescription())
                self.iStyle=oReg.GetTagName()
            self.lstData.SetFocus()
            if 0:
                iData=0
                for l in lVal:
                    iOfs=0
                    for it in l:
                        if iOfs==0:
                            idx=self.lstData.InsertImageStringItem(sys.maxint,it[0],it[1])
                            self.lstData.SetItemData(idx,iData)
                            self.lDat.append((0,it[0][:2]))
                        else:
                            self.lstData.SetStringItem(idx,iOfs,it[0],it[1])
                        iOfs+=1
                    iData+=1
        except:
            self.__logTB__()
        self.lstData.Thaw()
    def SetDN(self,sDN,iSz=-1,sSel=None):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            self.__logDebug__('sDN:%s;iSz:%d'%(sDN,iSz))
            self.lstData.Freeze()
            self.__clr__()
            self.lstData.DeleteAllItems()
            self.__setHeader__(1)
            if sDN is None:
                self.lblTitle.SetValue(u'')
                self.thdNot.SetDN(None)
            else:
                if sDN[-1]!='/':
                    sDN=sDN+'/'
                sDN=unicode(sDN)
                self.lblTitle.SetValue(sDN)
                self.thdNot.SetDN(sDN)
                lDN=sDN.split(u'/')
                iL1=len(lDN)
                iL2=len(self.lHierDN)
                iL=max(iL1,iL2)
                j=-1
                if self.__isLogDebug__():
                    self.__logDebug__('lDN;%s;lHierDN;%s;sSel:%r'%(self.__logFmt__(lDN),
                                self.__logFmt__(self.lHierDN),sSel))
                if sSel is None:
                    if iL1<iL2:
                        sSel=self.lHierDN[iL1-1]
                for i in xrange(iL1-1):
                    if i<iL1:
                        if i<iL2:
                            if lDN[i]==self.lHierDN[i]:
                                j=i
                            else:
                                self.lHierDN=lDN
                                break
                        else:
                            self.lHierDN=lDN
                            break
                if self.__isLogDebug__():
                    self.__logDebug__('lDN;%s;lHierDN;%s;sSel:%r'%(self.__logFmt__(lDN),
                                self.__logFmt__(self.lHierDN),sSel))
                #self.dContense,iDirSz=self.ReadContense(sDN)
                self.dContense,iDirSz=vtiLF.readContense(sDN)
                if iSz<0:
                    fSz=float(iDirSz)
                else:
                    fSz=float(iSz)
                if self.dContense is None:
                    self.lstData.Thaw()
                    self.Post('chdir',None)
                    return
                dDN=self.dContense['dDN']
                iIdx=self.imgDict['elem']['dir'][0]
                iData=0
                if bDbg:
                    if VERBOSE>10:
                        self.__logDebug__(['dContense',self.dContense])
                for sDN,dVal in dDN.iteritems():
                    idx=self.lstData.InsertImageStringItem(sys.maxint,sDN,iIdx)
                    #self.lstData.SetItemData(idx,iData)
                    self.lstData.SetItem(idx,-1,(0,sDN))
                    #self.lDat.append((0,sDN))
                    if sSel==sDN:
                        self.lstData.SetItemState(idx,
                                wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,
                                wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED)
                    iData+=1
                dFN=self.dContense['dFN']
                #iIdx=self.imgDict['empty']
                
                for sFN,dVal in dFN.iteritems():
                    if iSz>0:
                        try:
                            fPer=(dVal['size']/fSz)*100.0
                            #fPer=(dVal['size']/fSz)*100.0
                            if fPer<0:
                                fPer=0.0
                            if fPer>100:
                                fPer=100.0
                            iPer=int(fPer*self.fIdxDelta)
                            if iPer<0:
                                iPer=0
                            if iPer>255:
                                iPer=255
                            if bDbg:
                                self.__logDebug__('fPer:%f;fIdxDelta:%f;iPer:%d'%(fPer,
                                        self.fIdxDelta,iPer))
                            iIdx=self.imgDict['percent'][iPer]
                        except:
                            self.__logTB__()
                            iIdx=self.imgDict['elem']['empty'][0]
                    else:
                        iIdx=self.imgDict['elem']['empty'][0]
                    idx=self.lstData.InsertImageStringItem(sys.maxint,sFN,iIdx)
                    sSz=vtiLF.getSizeStr(dVal['size'],2)
                    self.lstData.SetStringItem(idx,1,sSz,-1)
                    sMod=vtiLF.ftime2str(dVal['zMod'])
                    self.lstData.SetStringItem(idx,2,sMod,-1)
                    #self.lstData.SetItemData(idx,iData)
                    self.lstData.SetItem(idx,-1,(1,sFN))
                    #self.lDat.append((1,sFN))
                    iData+=1
                self.lstData.Sort()
        except:
            self.__logTB__()
        self.lstData.Thaw()
    def ReadContense(self,sDN,dContense=None):
        if dContense is None:
            dContense={'root':sDN,'dDN':{},'dFN':{}}
        dDN=dContense['dDN']
        dFN=dContense['dFN']
        M_IDX=stat.ST_MODE
        S_IDX=stat.ST_SIZE
        ZM_IDX=stat.ST_MTIME
        iSz=0
        for sN in os.listdir(sDN):
            sFN=os.path.join(sDN,sN)
            s=os.stat(sFN)
            m=s[M_IDX]
            if stat.S_ISDIR(m):
                dDN[sN]={'type':'dir','size':-1}#.append(sN)
            elif stat.S_ISREG(m):
                #lFN.append(sN)
                dFN[sN]={'size':s[S_IDX],'zMod':s[ZM_IDX]}
                iSz+=s[S_IDX]
            else:
                pass
        #lDN.sort()
        #lFN.sort()
        return dContense,iSz
    def OnLstDataListColClick(self, event):
        event.Skip()
        try:
            if self.iStyle==0:
                return
            self.__logDebug__(''%())
            iCol=event.GetColumn()
            if iCol!=self.iCol:
                self.iCol=iCol
                self.bAscending=True
            else:
                self.bAscending=not self.bAscending
            self.__setHeader__(self.iStyle,bForce=True)
            self.Sort()
        except:
            self.__logTB__()
    def Sort(self):
        try:
            if self.IsMainThread()==False:
                return
            if self.iCol==0:
                if self.bAscending:
                    self.lstData.SortItems(self.OnCompareNameAscending)
                else:
                    self.lstData.SortItems(self.OnCompareNameDecending)
            else:
                self.lstData.SortItems(self.OnCompare)
        except:
            self.__logTB__()
    def OnLstDataListColRightClick(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataListColBeginDrag(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataListColEndDrag(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataListColDragging(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataListItemRightClick(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def RefreshProp(self):
        try:
            if self.dlgProp is not None:
                t=self.lstData.GetData(self.iSel)
                self.__logDebug__('t:%s'%(self.__logFmt__(t)))
                tt=type(t)
                bClr=True
                if tt==types.TupleType:
                    if t[0]==0:
                        self.dlgProp.SetFN(t[1][:],self.lblTitle.GetValue())
                        bClr=False
                    elif t[0]==1:
                        self.dlgProp.SetFN(t[1][:],self.lblTitle.GetValue())
                        bClr=False
                if bClr:
                    self.dlgProp.Clear()
        except:
            self.__logTB__()
    def OnLstDataListItemSelected(self, event):
        event.Skip()
        try:
            iData=event.GetData()
            iIdx=event.GetIndex()
            self.__logDebug__('idx:%d;data:%d'%(iIdx,iData))
            self.iSel=iData
            self.RefreshProp()
        except:
            self.__logTB__()
    def OnLstDataListItemDeselected(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataLeftDclick(self, event):
        event.Skip()
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__('iSel:%d'%(self.iSel))
            if self.iSel>=0:
                t=self.lstData.GetData(self.iSel)
                self.__logDebug__('t:%s'%(self.__logFmt__(t)))
                #t=self.lDat[self.iSel]
                tt=type(t)
                if tt==types.TupleType:
                    if t[0]==0:
                        sK='dDN'
                        if self.iStyle==0:
                            self.Post('chdir',t[1][:])
                        elif self.iStyle==1:
                            self.Post('chdir',''.join([self.lblTitle.GetValue(),t[1][:]]))
                    elif t[0]==1:
                        sK='dFN'
                        self.Post('open',''.join([self.lblTitle.GetValue(),t[1][:]]))
                    else:
                        pass
                elif tt==types.DictType:
                    if 'root' in t:
                        self.Post('chdir',t['root'])
        except:
            self.__logTB__()
    def OnLstDataRightDclick(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLstDataMiddleDclick(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()

    def OnVExplorerPanelSetFocus(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnVExplorerPanelLeftDown(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnLblTitleSetFocus(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Post('focus')
        except:
            self.__logTB__()
    def OnLstDataSetFocus(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Post('focus')
        except:
            self.__logTB__()
    def measureCalcRes(self,sDN,iCntAll,iCntDir,iCntFile,lSz,iIdx,iType,sN,dVal):
        try:
            if self.__isLogDebug__():
                self.__logDebug__('sDN:%s;dVal;%s;count all:%d,dir:%d,file:%d;size:%d'%(sDN,
                        self.__logFmt__(dVal),iCntAll,iCntDir,iCntFile,lSz))
            if iIdx==-1:
                k=self.lstData.GetDataKey((iType,sN))
                if k>0:
                    iIdx=self.lstData.FindItemData(-1,k)
                    self.__logDebug__([k,iIdx])
                    if iIdx<0:
                        return
                    dVal['size']=lSz
                    sSz=vtiLF.getSizeStr(dVal['size'],2)
                    self.thdRead.CallBack(self.lstData.SetStringItem,iIdx,1,sSz,-1)
            else:
                if iType==0:
                    dVal['size']=lSz
                    sSz=vtiLF.getSizeStr(dVal['size'],2)
                    self.thdRead.CallBack(self.lstData.SetStringItem,iIdx,1,sSz,-1)
                
        except:
            self.__logTB__()
    def Measure(self,sDN=None):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__('sDN:%r'%(sDN))
            if sDN is None:
                lSel=self.lstData.GetSelected()
                if VERBOSE>5:
                    self.__logDebug__('lSel;%s'%(self.__logFmt__(lSel)))
                sDN=self.lblTitle.GetValue()
                bAll=False
                if lSel is None:
                    bAll=True
                elif len(lSel)==0:
                    bAll=True
                if bAll==True:
                    l=self.lstData.GetValue([-1],None)
                    if VERBOSE>5:
                        self.__logDebug__(self.dContense)
                        self.__logDebug__(l)
                    for sN,dVal in self.dContense['dDN'].iteritems():
                        sTmp=u'/'.join([sDN,sN])
                        self.thdRead.CalcSize(sTmp,None,100,0,
                                self.measureCalcRes,-1,0,sN,dVal)
                else:
                    for iIdx,tCltData in lSel:
                        iType=tCltData[0]
                        sN=tCltData[1]
                        dVal=None
                        if iType==0:
                            d=self.dContense['dDN']
                        else:
                            continue
                        if sN in d:
                            dVal=d[sN]
                        if dVal is None:
                            continue
                        sTmp=u'/'.join([sDN,sN])
                        self.thdRead.CalcSize(sTmp,None,100,0,
                                self.measureCalcRes,-1,iType,sN,dVal)
                        
        except:
            self.__logTB__()
