#----------------------------------------------------------------------------
# Name:         vExplorerCfgExtPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120116
# CVS-ID:       $Id: vExplorerCfgExtPanel.py,v 1.1 2012/01/15 22:39:09 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import types
    
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmListCtrl import vtmListCtrl
    from vidarc.tool.misc.vtmFileBrowser import vtmFileBrowser
    from vidarc.tool.input.vtInputTextPopup import vtInputTextPopup
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcLog as vcLog
    
    from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmdArgDlg import vXmlNodeExplorerLaunchCmdArgDlg
    
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerCfgExtPanel(vtgPanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append((vtmListCtrl,(0,0),(1,4),{
                'name':'lstExt',
                'cols':[
                    [u'',None,20,None],
                    [_(u'extension'),None,80,None],
                    [_(u'application'),None,-1,None],
                    [_(u'arguments'),None,80,None],
                    ],
                },None))
        lWid.append(('lblRg',           (1,0),(1,1),    {'name':'lblExt','label':_(u'extension')},None))
        lWid.append(('szBoxHor',        (1,1),(1,3),{'name':'bxsExt',},[
                ('txtEnt',      1,4,{'name':'txtExt',},{
                    'ent':self.OnTxtExtEnter
                    }),
                ('lblRg',       1,4,{'name':'lbl12','label':''},None),
                ('lblRg',       1,0,{'name':'lbl13','label':''},None),
                ]))
        lWid.append(('lblRg',           (2,0),(1,1),    {'name':'lblAppl','label':_(u'application')},None))
        lWid.append((vtmFileBrowser,    (2,1),(1,3),    {
                'name':'fbbAppl','bMulti':False,
                'bPosixFN':True, 'bSave':False,
                'sDftFN':u'','sFN':u'','sMsg':_(u'choose file'),
                'sWildCard':u'executable files|*.exe|all files|*.*'},None))
        lWid.append(('lblRg',           (3,0),(1,1),    {'name':'lblArg','label':_(u'arguments')},None))
        lWid.append((vtInputTextPopup,  (3,1),(1,3),    {
                'name':u'txtArg',},None))
        
        lWid.append(('szBoxHor',        (4,0),(1,4),{'name':'bxsExtBt',},[
                ('lblRg',       1,4,{'name':'lbl62','label':''},None),
                ('cbBmpLbl',       0,8,{
                    'name':'cbExtAdd',
                    'label':_(u'Apply'),
                    'bitmap':vtArt.getBitmap(vtArt.ApplyAttr)},{
                    'btn':self.OnExtAdd,
                    }),
                ('cbBmpLbl',       0,0,{
                    'name':'cbExtDel',
                    'label':_(u'Delete'),
                    'bitmap':vtArt.getBitmap(vtArt.DelAttr)},{
                    'btn':self.OnExtDel,
                    }),
                ('lblRg',       1,4,{'name':'lbl62','label':''},None),
                ]))
        self.dExt=None
        vtgPanel.__initCtrl__(self,lWid=lWid)
        self.lstExt.BindEvent('lst_item_sel',self.OnLstExtSel)
        dlg=vXmlNodeExplorerLaunchCmdArgDlg(self.txtArg)
        dlg.SetPossible(self.GetArgDict())
        self.txtArg.SetPopupWin(dlg)
        return [(1,1)],[(1,1),(3,1)]
    def GetArgDict(self):
        return {}
        return {
            '%dn%':         _(u'directory'),
            '%sdn%':        _(u'selected directory(s)'),
            '%sdn0%':       _(u'fist selected directory(s)'),
            '%sfn0%':       _(u'first selected file'),
            '%sfn%':        _(u'selected file(s)'),
            '%sel%':        _(u'selected'),
            '%dn_save%':    _(u'save directory'),
            #'%sel0%':_(u'first selected'),
            '%vdn0%':       _(u'current view directory'),
            '%vdn1%':       _(u'previous view directory'),
            '%view0%':      _(u'current view'),
            '%view1%':      _(u'previous view'),
            '%date%':       _(u'current date'),
            '%time%':       _(u'current time'),
            '%now%':        _(u'current date and time'),
            '%host%':       _(u'hostname'),
            '%hostname%':   _(u'full hostname'),
            }
    def GetArgMapFmtDict(self):
        return {
            '%dn%':         self.GetDN,
            '%sdn%':        ('%s','sdn'),
            '%sdn0%':       ('%s','sdn0'),
            '%sdn1%':       ('%s','sdn1'),
            '%sfn0%':       ('%s','sfn0'),
            '%sfn%':        ('%s','sfn'),
            '%sel%':        ('%s','sel'),
            '%dn_save%':    self.GetSaveDN,
            #'%sel0%': ('%s','sel0'),
            '%vdn0%':       ('%s','vdn0'),
            '%vdn1%':       ('%s','vdn1'),
            '%view0%':      ('%s','view0'),
            '%view1%':      ('%s','view1'),
            '%date%':       self.GetDateGM,
            '%time%':       self.GetTimeGM,
            '%now%':        self.GetDateTimeGM,
            '%host%':       self.GetHost,
            '%hostname%':   self.GetHostFull,
            }
    def SetExt(self,dExt):
        try:
            self.__logDebug__('dExt;%s'%(self.__logFmt__(dExt)))
            self.lExt=[]
            lVal=[]
            if type(dExt)==types.ListType:
                for it in dExt:
                    d=it['extension']
                    sExt=d.get('ext','')
                    if sExt in self.lExt:
                        continue
                    self.lExt.append(sExt)
                    lVal.append([it.copy(),['',sExt,d.get('app',''),d.get('arg','')]])
            self.lstExt.SetValue(lVal)
            self.txtExt.SetValue('')
            self.fbbAppl.SetValue('')
            self.txtArg.SetValue('')
        except:
            self.__logTB__()
    def GetExt(self):
        try:
            self.__logDebug__(''%())
            lExt=self.lstExt.GetValue([-1])
            self.__logDebug__('lExt;%s'%(self.__logFmt__(lExt)))
            lExtRet=[]
            for l in lExt:
                lExtRet.append(l[0])
            return lExtRet
        except:
            self.__logTB__()
    def doExtAdd(self,sExt,sCmd,sArg):
        try:
            self.__logDebug__('sExt:%s;sCmd:%s'%(sExt,sCmd))
            if sExt in self.lExt:
                bUpd=True
                t=self.lstExt.GetSelected()
                iIdx=t[0][0]
                d=t[0][1]
                dd=d['extension']
                dd['app']=sCmd
                dd['arg']=sArg
                self.lstExt.SetValue([[d,['',sExt,sCmd,sArg]]],iIdx)
            else:
                bUpd=False
                self.lExt.append(sExt)
                d={'extension':{
                    'ext':sExt,'app':sCmd,'arg':sArg}}
                self.lstExt.Insert([[d,['',sExt,sCmd,sArg]]])
        except:
            self.__logTB__()
    def OnTxtExtEnter(self,evt):
        try:
            sVal=self.txtExt.GetValue()
            self.__logDebug__('sVal:%s'%(sVal))
            self.doExtAdd(sVal,self.fbbAppl.GetValue(),
                    self.txtArg.GetValue())
        except:
            self.__logTB__()
    def OnExtAdd(self,evt):
        try:
            sVal=self.txtExt.GetValue()
            self.__logDebug__('sVal:%s'%(sVal))
            self.doExtAdd(sVal,self.fbbAppl.GetValue(),
                    self.txtArg.GetValue())
        except:
            self.__logTB__()
    def OnExtDel(self,evt):
        try:
            sVal=self.txtExt.GetValue()
            self.__logDebug__('sVal:%s'%(sVal))
            #self.doExtDel(sVal,self.fbbAppl.GetValue())
            t=self.lstExt.GetSelected()
            print t
            if t is not None:
                iIdx=t[0][0]
            
                self.lstExt.DeleteItem(iIdx)
        except:
            self.__logTB__()
    def OnLstExtSel(self,evt):
        try:
            self.__logDebug__(''%())
            t=self.lstExt.GetSelected([-1,1,2,3])
            self.__logDebug__('tup;%s'%(self.__logFmt__(t)))
            if len(t)>0:
                tup=t[0]
                self.txtExt.SetValue(tup[1])
                self.fbbAppl.SetValue(tup[2])
                self.txtArg.SetValue(tup[3])
        except:
            self.__logTB__()
