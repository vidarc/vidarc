#Boa:FramePanel:vXmlNodeExplorerVMwareDiskPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerVMwareDiskPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100216
# CVS-ID:       $Id: vXmlNodeExplorerVMwareDiskPanel.py,v 1.2 2010/04/03 21:40:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmFileBrowser
import wx.lib.buttons
import wx.lib.filebrowsebutton

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

[wxID_VXMLNODEEXPLORERVMWAREDISKPANEL, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELCHCMNT, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELFBBAPP, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELFBBSRC, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLAPPL, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLMNT, 
 wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLSRC, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeExplorerVMwareDiskPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSrc, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.fbbSrc, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              3))
        parent.AddWindow(self.lblMnt, (1, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.chcMnt, (1, 1), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblAppl, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.fbbApp, (2, 1), border=0, flag=wx.EXPAND, span=(1,
              3))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEEXPLORERVMWAREDISKPANEL,
              name=u'vXmlNodeExplorerVMwareDiskPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblSrc = wx.StaticText(id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLSRC,
              label=_(u'file:'), name=u'lblSrc', parent=self, pos=wx.Point(0,
              0), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.fbbSrc = vidarc.tool.misc.vtmFileBrowser.vtmFileBrowser(bMulti=False,
              bPosixFN=True, bSave=False,
              id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELFBBSRC, name=u'fbbSrc',
              parent=self, pos=wx.Point(60, 0), sDftFN=u'tmpl.vmdk',
              sFN=u'tmpl.vmdk', sMsg=u'choose file',
              sWildCard=_(u'VMware disk files|*.vmdk|all files|*.*'),
              size=wx.Size(177, 30), style=0)

        self.chcMnt = wx.Choice(choices=[],
              id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELCHCMNT, name=u'chcMnt',
              parent=self, pos=wx.Point(60, 34), size=wx.Size(79, 21), style=0)

        self.lblMnt = wx.StaticText(id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLMNT,
              label=_(u'mount point:'), name=u'lblMnt', parent=self,
              pos=wx.Point(0, 34), size=wx.Size(56, 24), style=wx.ALIGN_RIGHT)

        self.lblAppl = wx.StaticText(id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELLBLAPPL,
              label=_(u'application:'), name=u'lblAppl', parent=self,
              pos=wx.Point(0, 62), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.fbbApp = vidarc.tool.misc.vtmFileBrowser.vtmFileBrowser(bMulti=False,
              bPosixFN=True, bSave=False,
              id=wxID_VXMLNODEEXPLORERVMWAREDISKPANELFBBAPP, name=u'fbbApp',
              parent=self, pos=wx.Point(60, 62), sDftFN=u'', sFN=u'',
              sMsg=u'choose file',
              sWildCard=u'executable files|*.exe|all files|*.*',
              size=wx.Size(177, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        for i in xrange(65,65+26):
            self.chcMnt.Append('%s:\\'%(chr(i)))
        self.chcMnt.SetStringSelection('D:\\')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        #self.gbsData.AddGrowableCol(0,2)
        self.gbsData.AddGrowableCol(1,1)
        #self.gbsData.AddGrowableCol(2,1)
        self.gbsData.AddGrowableCol(3,1)
        self.gbsData.AddGrowableCol(4,1)
        self.gbsData.AddGrowableRow(1,1)
        self.gbsData.Layout()
        #self.gbsData.FitInside(self)
        self.gbsData.Fit(self)
        #self.fgsData.FitInside(self)

    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.fbbSrc.SetValue('')
            self.chcMnt.SetStringSelection('D:\\')
            self.fbbApp.SetValue('')
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.fbbSrc.SetValue(self.objRegNode.GetSrc(node))
            self.fbbApp.SetValue(self.objRegNode.GetApp(node))
            try:
                sMnt=self.objRegNode.GetMnt(node)
                self.chcMnt.SetStringSelection(sMnt)
            except:
                self.chcMnt.SetStringSelection('D:\\')
                self.__logTB__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetSrc(node,self.fbbSrc.GetValue())
            self.objRegNode.SetApp(node,self.fbbApp.GetValue())
            sMnt=self.chcMnt.GetStringSelection()
            self.objRegNode.SetMnt(node,sMnt)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnCbClrPwdButton(self, event):
        event.Skip()
