#----------------------------------------------------------------------------
# Name:         vExplorerCVSPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100531
# CVS-ID:       $Id: vExplorerCVSPanel.py,v 1.4 2010/07/19 09:48:35 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import os,types

import vSystem
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmTreeListSel import vtmTreeListSel
    from vidarc.tool.misc.vtmThreadCtrl import vtmThreadCtrl
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.vtProcess import vtProcess
    from vidarc.tool.vtThread import vtThreadWX
    from vidarc.tool.time.vtTime import vtDateTime,vtTimeDiff
    import vidarc.tool.InOut.vtInOutFileInfo as vtIOFI
    import vSystem
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerCVSPanel(vtgPanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        self.bEnMark=False
        self.bEnMod=False
        lWid=kwargs.get('lWid',[])
        lWid.append(('szBoxHor',(0,0),(1,4),{'name':'bxsSrc',},[
                (None,    (19,16),4,None,None),
                ('tgBmp',   0,4,{'name':'tgRec','bitmap':vtArt.getBitmap(vtArt.Recursion),},None),
                ('lblRg',   0,4,{'name':'lblSrc','label':_(u'source')},None),
                ('txtRd',   1,4,{'name':'txtSrc','value':u''},None),
                ('tgBmpLbl',0,4,{'name':'tgShowEqual','label':_(u'equal'),'bitmap':vtArt.getBitmap(vtArt.Apply),},None),
                ]))
        lWid.append((vtmThreadCtrl,(1,0),(1,4),
                    {'name':'thdCtrl',},None))
        lWidBt=[]
        lWidBt.append(('cbBmp',0,4,
                    {'name':'cbDel','bitmap':vtArt.getBitmap(vtArt.Del),},
                    {'btn':self.OnCbDelButton}))
        lWidBt.append(('tgBmp',0,4,
                    {'name':'tgAutoStart','bitmap':vtArtState.getBitmap(vtArtState.Start),},
                    #{'btn':self.OnTgAutoStartButton}
                    None))
        lWid.append(('szBoxHor',(2,0),(1,4),
                    {'name':'bxsProc',},[
                    ('lblRg',0,4,
                    {'name':'lblCur','label':_('processing')},None),
                    ('txtRdRg',2,4,
                    {'name':'txtCur','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtElapsed','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtSpeed','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtEstimate','value':''},None),
                    ]))
        lWid.append((vtmTreeListSel,(3,0),(1,4),
                    {'name':'trlstRes','bButtons':True,'lWidBt':lWidBt},None))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        
        self.trlstRes.AddColumn(text=_(u'name'))
        self.trlstRes.AddColumn(text=_(u'action'))
        self.trlstRes.AddColumn(text=_(u'local'))
        self.trlstRes.AddColumn(text=_(u'remote'))
        self.trlstRes.AddColumn(text=_(u'tag'))
        self.trlstRes.AddColumn(text=_(u'version'))
        self.trlstRes.AddColumn(text=_(''))
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdProc.SetValue('iSize',0)
        self.thdProc.SetValue('iCur',0)
        self.thdProc.SetValue('iCurStart',0)
        self.thdBth=vtThreadWX(self.trlstRes.GetMainWidget(),bPost=False)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        font=self.GetFontByName('fixed')
        self.trlstRes.SetFont(font)
        #self.trlstRes.SetAutoExpand(True)
        self.trlstRes.SetColumnWidth(1,60)
        self.trlstRes.SetColumnWidth(2,60)
        self.trlstRes.SetColumnWidth(3,60)
        self.trlstRes.SetColumnWidth(4,60)
        self.trlstRes.SetColumnWidth(5,60)
        self.trlstRes.SetColumnWidth(6,20)
        self.trlstRes.SetColumnAlignment(1,-2)
        self.trlstRes.SetStretchLst([(0,-2),])
        self.trlstRes.SetSelColumn(6,('X',''))
        self.trlstRes.SetMap([('name',0),('size',1),('res',2)])
        self.trlstRes.BindEvent('cmd',self.OnSelChanged)
        
        self.imgLst,self.dImg=self.trlstRes.CreateImageList([
            ('empty'        ,vtArt.getBitmap(vtArt.Invisible)),
            ('busy'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('ok'           ,vtArt.getBitmap(vtArt.Apply)),
            ('err'          ,vtArt.getBitmap(vtArt.Cancel)),
            ('test'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('pkg'          ,vtArt.getBitmap(vtArt.Element)),
            ('cp'           ,imgExplorer.getCp1To2Bitmap()),
            ('mv'           ,imgExplorer.getMv1To2Bitmap()),
            ('del'          ,vtArt.getBitmap(vtArt.Del)),
            ('bth'          ,vtArt.getBitmap(vtArt.Build)),
            ('cmd'          ,vtArt.getBitmap(vtArt.Missile)),
            ('mco'          ,imgExplorer.getRecordBitmap()),
            ('stp'          ,vtArt.getBitmap(vtArt.Stop)),
            ('Up-to-date'   ,vtArt.getBitmap(vtArt.Apply)),
            ('quest'        ,vtArt.getBitmap(vtArt.Question)),
            ('Needs Patch'  ,vtArt.getBitmap(vtArt.DownLoad)),
            ('Locally Modified' ,vtArt.getBitmap(vtArt.UpLoad)),
            ])
        
        self.trlstRes.SetImageList(self.imgLst)
        self.trlstRes.SetForceChildren(True)
        self.trlstRes.SetValue([None,[(_(u'batches'),self.dImg['bth'])]],None)
        
        #self.SetName(name)
        self.iBth=0
        self.dBth={}
        self.thdProc.SetValue('bAutoStart',kwargs.get('auto_start',True))
        self.thdProc.SetValue('bShowEqual',kwargs.get('show_equal',False))
        self.trlstRes.tgAutoStart.SetValue(self.thdProc.GetValue('bAutoStart'))
        self.tgRec.SetValue(False)
        self.zStart=vtDateTime()
        self.zNow=vtDateTime()
        self.zLastUpd=vtDateTime()
        self.zDiff=vtTimeDiff()
        self.__logSetProcBarVal__(0,100)
        #self.zLastUpt.Now()
        self.sAppl='cvs'
        self.oReg=None
        #return None,[(1,1),(3,1)]
        return [(3,4)],[(1,1),(3,1)]
    def SetValue(self,oReg,sAppl,sDN):
        try:
            self.__logDebug__('sAppl:%s;sDN  :%s'%(sAppl,sDN))
            self.txtSrc.SetValue(sDN)
            self.oReg=oReg
            self.sAppl=sAppl
        except:
            self.__logTB__()
    def Start(self):
        try:
            self.__logDebug__(''%())
            sDN=self.txtSrc.GetValue()
            self.thdProc.SetValue('bRec',self.tgRec.GetValue())
            self.dRes={}
            self.thdProc.Do(self.procCheck,sDN,self.dRes,self.thdProc,
                bRec=self.tgRec.GetValue(),
                bShowEqual=self.tgShowEqual.GetValue())
        except:
            self.__logTB__()
    def procDummy(self):
        pass
    def Stop(self):
        try:
            self.__logDebug__(''%())
            #self.thdProc.AddValue('iSize',-self.thdProc.GetValue('iCur'))
            #self.thdProc.SetValue('iCur',0)
            #self.__logProcDetails__()
        except:
            self.__logTB__()
    def OnThreadProc(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThreadAborted(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThreadFin(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCbDelButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnSelChanged(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def GetRepository(self,sDN):
        try:
            return self.oReg.GetRepository(sDN)
        except:
            self.__logTB__()
        return {'base':None}
    def IsBusy(self):
        if VERBOSE>0:
            self.__logDebug__('thdRead:%d;thdProc:%d'%(self.thdRead.IsBusy(),
                    self.thdProc.IsBusy()))
        if self.thdProc.IsBusy():
            return True
        return False
    def procCheck(self,sDN,dEntry,thd,bRec,bShowEqual):
        bDbg=False
        if self.__isLogDebug__():
            bDbg=True
        thd.CallBack(self.txtCur.SetValue,_(u'check CVS:%s')%sDN)
        thd.CallBack(self.trlstRes.DeleteAllItems)
        lVal=self.oReg.GetStatus(sDN,self.sAppl,
                bRec,bShowEqual,
                self.dImg)
        
        if bDbg:
            self.__logDebug__('lVal:%s'%(self.__logFmt__(lVal)))
        thd.CallBack(self.trlstRes.SetValue,
                [None,[(_(u'batches'),self.dImg['bth'])],False,lVal],None)
