#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerSynchPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vXmlNodeExplorerSynchPanel.py,v 1.1 2010/05/21 22:58:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.input.vtInputFloat import vtInputFloat
    from vidarc.tool.input.vtInputTextPopup import vtInputTextPopup
    from vidarc.tool.misc.vtmFileBrowser import vtmFileBrowser
    from vidarc.tool.misc.vtmDirBrowser import vtmDirBrowser
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    #from vidarc.vTools.vExplorer.vXmlNodeExplorerCmpCfgDlg import vXmlNodeExplorerCmpCfgDlg
    #from vidarc.vTools.vExplorer.vXmlNodeExplorerCmpLogDlg import vXmlNodeExplorerCmpLogDlg

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerSynchPanel(vtgPanel,vtXmlNodePanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append(('lblRg',           (0,0),(1,1),{'name':'lblSrc1DN','label':_(u'source 1')},None))
        lWid.append((vtmDirBrowser,     (0,1),(1,3),{
                'name':'fbbSrc1','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose source 1 directory')},None))
        lWid.append(('lblRg',           (1,0),(1,1),{'name':'lblSrc2DN','label':_(u'source 2')},None))
        lWid.append((vtmDirBrowser,     (1,1),(1,3),{
                'name':u'fbbSrc2','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose source 2 directory')},None))
        lWid.append(('lblCt',           (2,1),(1,1),{'name':'lblCalc','label':_(u'calculation')},None))
        lWid.append(('szBoxHor',       (3,1),(1,1),{'name':'bxsCalc0',},[
            ('szBoxVert',   0,0,{'name':'bxsCalc1',},[
                ('chk',     0,4,{'name':'chkCalcMD5','label':_(u'MD5'),},None),
                ('spInt',   0,4,{'name':'spRec','initial':0,'max':100,'min':-1,
                    'tip':_(u'recursion levels'),},None),
                ('chk',     0,0,{'name':'chkCalcAlwaysMD5','label':_(u'always'),},None),
                ]),
            (None,(8,8),4,None,None),
            ]))
        lWid.append(('lblCt',           (2,2),(1,1),{'name':'lblChk','label':_(u'check')},None))
        lWid.append(('szBoxHor',       (3,2),(1,1),{'name':'bxsCheck0',},[
            ('szBoxVert',   0,0,{'name':'bxsCheck1',},[
                ('chk',     0,4,{'name':'chkAccess','label':_(u'access time'),},None),
                ('chk',     0,4,{'name':'chkMod','label':_(u'modification time'),},None),
                ('chk',     0,0,{'name':'chkCreate','label':_(u'create time'),},None),
                ]),
            (None,(8,8),4,None,None),
            ]))
        lWid.append(('lblCt',           (2,3),(1,1),{'name':'lblShow','label':_(u'show')},None))
        lWid.append(('szBoxHor',       (3,3),(1,1),{'name':'bxsShow0',},[
            ('szBoxVert',   0,0,{'name':'bxsShow1',},[
                ('chk',     0,4,{'name':'chkAutoExpand','label':_(u'auto expand'),},None),
                ('chk',     0,4,{'name':'chkShowEqual','label':_(u'show equal'),},None),
                ('chk',     0,4,{'name':'chkFlat','label':_(u'flat'),},None),
                ]),
            (None,(8,8),4,None,None),
            ]))
        
        vtgPanel.__initCtrl__(self,lWid=lWid)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        return [(3,1)],[(1,1),(2,1),(3,1)]

    def SetRegNodeOld(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        dlg=vXmlNodeExplorerLaunchCmdArgDlg(self.txtArg)
        try:
            dlg.SetPossible(self.objRegNode.GetArgDict())
        except:
            self.__logTB__()
        self.txtArg.SetPopupWin(dlg)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.fbbSrc1.SetValue('')
            self.fbbSrc2.SetValue('')
            self.spRec.SetValue(-1)
            self.chkCalcMD5.SetValue(True)
            self.chkCalcAlwaysMD5.SetValue(False)
            self.chkAccess.SetValue(False)
            self.chkMod.SetValue(True)
            self.chkCreate.SetValue(False)
            self.chkAutoExpand.SetValue(True)
            self.chkShowEqual.SetValue(False)
            self.chkFlat.SetValue(False)
        except:
            self.__logTB__()
    def SetNetDocs(self,d):
        #if d.has_key('vHum'):
        #    dd=d['vHum']
        # add code here
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            # add code here
            self.fbbSrc1.SetValue(self.objRegNode.GetSrc1(node))
            self.fbbSrc2.SetValue(self.objRegNode.GetSrc2(node))
            self.spRec.SetValue(self.objRegNode.GetRecVal(node))
            
            iVal=self.objRegNode.GetCalcMD5Val(node)
            if iVal>0:
                self.chkCalcMD5.SetValue(True)
            else:
                self.chkCalcMD5.SetValue(False)
            iVal=self.objRegNode.GetCalcAlwaysMD5Val(node)
            if iVal>0:
                self.chkCalcAlwaysMD5.SetValue(True)
            else:
                self.chkCalcAlwaysMD5.SetValue(False)
            iVal=self.objRegNode.GetAccessVal(node)
            if iVal>0:
                self.chkAccess.SetValue(True)
            else:
                self.chkAccess.SetValue(False)
            iVal=self.objRegNode.GetModVal(node)
            if iVal>0:
                self.chkMod.SetValue(True)
            else:
                self.chkMod.SetValue(False)
            iVal=self.objRegNode.GetCreateVal(node)
            if iVal>0:
                self.chkCreate.SetValue(True)
            else:
                self.chkCreate.SetValue(False)
            iVal=self.objRegNode.GetAutoExpandVal(node)
            if iVal>0:
                self.chkAutoExpand.SetValue(True)
            else:
                self.chkAutoExpand.SetValue(False)
            iVal=self.objRegNode.GetShowEqualVal(node)
            if iVal>0:
                self.chkShowEqual.SetValue(True)
            else:
                self.chkShowEqual.SetValue(False)
            iVal=self.objRegNode.GetFlatVal(node)
            if iVal>0:
                self.chkFlat.SetValue(True)
            else:
                self.chkFlat.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def __GetNode__(self,node):
        try:
            self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetSrc1(node,self.fbbSrc1.GetValue())
            self.objRegNode.SetSrc2(node,self.fbbSrc2.GetValue())
            self.objRegNode.SetRecVal(node,self.spRec.GetValue())
            if self.chkCalcMD5.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetCalcMD5(node,sVal)
            if self.chkCalcAlwaysMD5.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetCalcAlwaysMD5(node,sVal)
            if self.chkAccess.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetAccess(node,sVal)
            if self.chkMod.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetMod(node,sVal)
            if self.chkCreate.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetCreate(node,sVal)
            if self.chkAutoExpand.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetAutoExpand(node,sVal)
            if self.chkShowEqual.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetShowEqual(node,sVal)
            if self.chkFlat.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetFlat(node,sVal)
        except:
            self.__logTB__()
        #vtXmlNodePanel.__GetNode__(self,node)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbSrc1.Close()
        self.fbbSrc2.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbSrc1.Cancel()
        self.fbbSrc2.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
