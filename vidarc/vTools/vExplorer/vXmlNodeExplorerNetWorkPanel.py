#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerNetWorkPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100403
# CVS-ID:       $Id: vXmlNodeExplorerNetWorkPanel.py,v 1.1 2010/04/03 21:40:55 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import os
    
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vidarc.tool.vtProcess import vtProcess
    from vSystem import isPlatForm
    if isPlatForm('win'):
        SYSTEM=1
    else:
        SYSTEM=0
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerNetWorkPanel(vtgPanel,vtXmlNodePanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append(('lblRg',   (0,0),(1,1),    {'name':'lblCfg','label':_(u'configuration')},None))
        lWid.append(('lst',     (0,1),(1,2),    {'name':'lstCfg',},None))
        lWid.append(('cbBmp',   (0,3),(1,1),
                    {'name':'cbSearch','bitmap':vtArt.getBitmap(vtArt.Find),},
                    {'btn':self.OnCbSearchButton}))
        lWid.append(('lblRg',   (1,0),(1,1),    {'name':'lblCur','label':_(u'current')},None))
        lWid.append(('txtRd',   (1,1),(1,2),    {'name':'txtCur','value':''},None))
        lWid.append(('cbBmp',   (1,3),(1,1),
                    {'name':'cbGet','bitmap':vtArt.getBitmap(vtArt.ImportFree),},
                    {'btn':self.OnCbGetButton}))
        lWid.append(('lblRg',   (2,0),(1,1),    {'name':'lblAdr','label':_(u'address')},None))
        lWid.append(('txtRd',   (2,1),(1,2),    {'name':'txtAdr','value':''},None))
        lWid.append(('cbBmp', (2,3),(1,1),
                    {'name':'cbSet','bitmap':vtArt.getBitmap(vtArt.Apply),},
                    {'btn':self.OnCbSetButton}))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        return [(1,1)],[(1,1),(2,1)]
    def OnCbSearchButton(self,evt):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__(''%())
                    bDbg=True
            if SYSTEM==1:
                sCmd='netsh -c interface dump'
                tPipes=os.popen3(sCmd)
                lOut=tPipes[1].readlines()
                lErr=tPipes[2].readlines()
                if bDbg==True:
                    self.__logDebug__('lOut;%s;lErr;%s'%(self.__logFmt__(lOut),
                            self.__logFmt__(lErr)))
                self.lstCfg.Clear()
                for sL in lOut:
                    s=sL.strip()
                    self.lstCfg.Append(s)
        except:
            self.__logTB__()
    def OnCbGetButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCbSetButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        else:
            self.__logDebug__(''%())
        # add code here
        if dDocs is not None:
            if 'vHum' in dDocs:
                d=dDocs['vHum']
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if flag:
                # add code here
                pass
            else:
                # add code here
                pass
        except:
            self.__logTB__()
