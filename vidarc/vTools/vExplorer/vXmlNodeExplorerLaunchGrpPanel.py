#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchGrpPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100222
# CVS-ID:       $Id: vXmlNodeExplorerLaunchGrpPanel.py,v 1.2 2012/01/15 10:19:28 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys
import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.gui.vtgPanel import vtgPanel
    
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)

import wx

[wxID_VXMLNODEEXPLORERLAUNCHGRPPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vXmlNodeExplorerLaunchGrpPanel(vtgPanel,vtXmlNodePanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        
        vtgPanel.__initCtrl__(self,lWid=lWid)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        return None,None
    def __Clear__(self):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def SetNetDocs(self,d):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            #if d.has_key('vHum'):
            #    dd=d['vHum']
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            self.__logDebug__(''%())
            # add code here
            #self.objRegNode.Get(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def __GetNode__(self,node):
        try:
            self.__logDebug__(''%())
            # add code here
            #self.objRegNode.SetMnt(node,sMnt)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        try:
            self.__logDebug__(''%())
            # add code here
            if flag:
                # add code here
                pass
            else:
                # add code here
                pass
        except:
            self.__logTB__()
