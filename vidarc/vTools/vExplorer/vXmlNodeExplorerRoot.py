#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vXmlNodeExplorerRoot.py,v 1.9 2010/05/11 15:57:15 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,os,stat
if sys.platform.startswith('win'):
    SYSTEM=1
    import win32file
    import win32con
    import win32api
elif sys.platform.startswith('linux'):
    SYSTEM=0

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)


from vidarc.tool.xml.vtXmlNodeRoot import vtXmlNodeRoot
try:
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    import vidarc.config.vcCust as vcCust
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.InOut.vtInOutFileInfo as vtiLF
    import vidarc.tool.InOut.vtInOutFileTools as vtiFT
    import vidarc.tool.InOut.vtInOutNetTools as vtiNT
    GUI=0
    #if vcCust.is2Import(__name__):
    #    GUI=1
    #    from vXmlNodeExplorerRootPanel import vXmlNodeExplorerRootPanel
    #    vLogFallBack.logDebug('GUI objects imported',__name__)
    #else:
    #    GUI=0
    #    vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerRoot(vtXmlNodeRoot):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            #('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='ExplorerRoot'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeRoot.__init__(self,tagName)
    def GetDescription(self):
        return _(u'computer')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.GetDescription()
    def GetName(self,node):
        return ''
    def GetSystem(self):
        return SYSTEM
    #def GetInit(self,node):
    #    return self.GetChildAttrStr(node,'init','fid')
    #def GetInitNum(self,node):
    #    return self.GetChildAttr(node,'init','fid')
    #def SetInit(self,node,val):
    #    self.SetChildAttrStr(node,'init','fid',val)
    #def SetInitNum(self,node,val):
    #    self.SetChildAttr(node,'init','fid',val)
    def ConvToOS(self,s):
        return s.replace('/',os.path.sep)
    def ConvToInternal(self,s):
        return s.replace(os.path.sep,'/')
    def SplitFN(self,sFN):
        return os.path.split(sFN)
    def GetApplExec(self,sAppl):
        try:
            if SYSTEM==1:
                sPrgDftPath=os.getenv('ProgramFiles')
                if sAppl=='appl_truecrypt':
                    sExec=os.path.sep.join([sPrgDftPath,'TrueCrypt','TrueCrypt.exe'])
                    if os.path.exists(sExec):
                        return sExec
        except:
            self.__logTB__()
        return None
    def GetNetHostInfo4List(self,sName):
        d=vtiNT.getHostInfo(sName)
        try:
            l=[[(k,-1),(unicode(v),-1),('',-1)] for k,v in d[None].iteritems()]
        except:
            self.__logTB__()
            l=[]
        l.sort()
        l.insert(0,[('_'*10,-1),('_'*10,-1),('',-1)])
        l.insert(0,[(','.join(vtiNT.getHostDiskLst(d['host'])),-1),('_'*10,-1),('',-1)])
        l.insert(0,[('___ '+_(u'disks'),-1),('',-1),('',-1)])
        dShare=vtiNT.getHostShareDict(d['host'])
        lK=dShare.keys()
        lK.sort()
        lK.reverse()
        for k in lK:
            if k is None:
                pass
            else:
                l.insert(0,[(k,-1),('',-1),('',-1)])
        l.insert(0,[('___ '+_(u'share'),-1),('_'*10,-1),('',-1)])
        for k in ['os_ver','host','domain']:
            l.insert(0,[(k,-1),(unicode(d[k]),-1),('',-1)])
        return l
    def GetNetShareInfo4List(self,dVal):
        l=[]
        lK=dVal.keys()
        lK.sort()
        for k in lK:
            if k.startswith('__'):
                continue
            v=dVal[k]
            l.append([(k,-1),(unicode(v),-1),('',-1)])
        return l
    def NetShareExec(self,sName):
        try:
            self.__logDebug__('%s'%(sName))
        except:
            self.__logTB__()
    def NetShareUnMnt(self,sName):
        try:
            self.__logDebug__('%s'%(sName))
        except:
            self.__logTB__()
    def GetNetHostDict(self):
        dNet={}
        lNet=vtiNT.getHostLst(None,None)
        for dVal in lNet:
            sLbl=dVal.get('name','???')
            dVal['type']='host'
            dVal['__funcData']=(self.GetNetHostInfo4List,(dVal['name'],),{})
            dShare=vtiNT.getHostShareDict(dVal['name'])
            for k,dd in dShare.iteritems():
                if k is not None:
                    dd['__funcData']=(self.GetNetShareInfo4List,(dShare[None][k],),{})
                    dd['__funcExec']=(self.NetShareExec,(k,),{})
                    dd['__funcUnMnt']=(self.NetShareUnMnt,(k,),{})
                    dd['__reg']='ExplorerNetProtocolSmb'
            dVal['__share']=dShare
            dNet[sLbl]=dVal
        return dNet
    def GetNetHost4List(self):
        l=[]
        lNet=vtiNT.getHostLst(None,None)
        for dVal in lNet:
            l.append([(dVal.get('name','???'),-1),('',-1),('',-1)])
        l.sort()
        return l
    def GetRootLst(self):
        lRoot=[]
        if SYSTEM==1:
            iDrv=win32file.GetLogicalDrives()
            iMsk=1
            for i in xrange(26):
                if (iDrv & iMsk) == iMsk:
                    lRoot.append('%s:%s'%(chr(65+i),os.sep))
                iMsk<<=1
        else:
            lRoot.append('/')
        return lRoot
    def GetMntDict(self):
        return vtiNT.getMntDict(None)
    def GetRootLstFree(self):
        lRoot=[]
        if SYSTEM==1:
            iDrv=win32file.GetLogicalDrives()
            iMsk=1
            for i in xrange(26):
                if (iDrv & iMsk) == 0:
                    lRoot.append('%s:%s'%(chr(65+i),os.sep))
                iMsk<<=1
        else:
            lRoot.append('/')
        return lRoot
    def GetRootInfoDict(self,sRoot,bForce=False):
        dRoot={}
        dRoot['root']=sRoot
        self.UpdateRootInfoDict(dRoot,bFull=True,bForce=False)
        return dRoot
    def GetRootShort(self,sRoot):
        if SYSTEM==1:
            return sRoot[:-1]
        return sRoot
    def UpdateRootInfoDict(self,dRoot,bFull=False,bForce=False):
        if SYSTEM==1:
            if bFull==True:
                iType=win32file.GetDriveType(dRoot['root'])
                if iType==win32file.DRIVE_FIXED:
                    sType='hdd'
                    bUpdateAlways=True
                elif iType==win32file.DRIVE_CDROM:
                    sType='cdrom'
                    bUpdateAlways=False
                elif iType==win32file.DRIVE_REMOTE:
                    sType='net'
                    bUpdateAlways=True
                elif iType==win32file.DRIVE_REMOVABLE:
                    sType='removable'
                    bUpdateAlways=False
                elif iType==win32file.DRIVE_UNKNOWN:
                    sType='unknown'
                    bUpdateAlways=False
                else:
                    sType='unknown'
                    bUpdateAlways=False
                dRoot['type']=sType
                dRoot['update']=bUpdateAlways
                if 0:
                    try:
                        if iType==win32file.DRIVE_FIXED:
                            t=win32api.GetVolumeInformation(dRoot['root'])
                            sVolId=win32file.GetVolumeNameForVolumeMountPoint(dRoot['root'])
                            dRoot['volID']=sVolId
                    except:
                        self.__logError__('root:%s'%(dRoot['root']))
                        self.__logTB__()
            if dRoot['update']==True or bForce==True:
                try:
                    t=win32api.GetVolumeInformation(dRoot['root'])
                    dRoot['label']=t[0][:]
                    dRoot['serial']=t[1]
                    dRoot['fs']=t[4][:]
                except:
                    self.__logError__('root:%s'%(dRoot['root']))
                    self.__logTB__()
                try:
                    t=win32file.GetDiskFreeSpaceEx(dRoot['root'])
                    dRoot.update({'size':t[1],'used':t[1]-t[0],
                        'percent':((t[1]-t[0])/float(t[1]))*100.0})
                except:
                    dRoot.update({'size':-1,'used':-1,'percent':-1})
            if bFull==True:
                if self.__isLogDebug__():
                    self.__logDebug__('root:%s;%s'%(dRoot['root'],self.__logFmt__(dRoot)))
        else:
            pass
    def GetRootLabel(self,dVal):
        sRoot=dVal['root']
        if dVal.has_key('label')==False:
            sLbl=''
        else:
            sLbl=dVal['label']
        if len(sLbl)==0:
            return  sRoot
        else:
            return ''.join([sRoot,' (',sLbl,')'])
    def ReadContense(self,sDN,dContense=None):
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        if bDbg:
            self.__logDebug__('sDN:%s'%(sDN))
        if dContense is None:
            dContense={'root':sDN,'dDN':{},'dFN':{}}
        dDN=dContense['dDN']
        dFN=dContense['dFN']
        #if sDN is None:
        #    return dContense
        M_IDX=stat.ST_MODE
        S_IDX=stat.ST_SIZE
        for sN in os.listdir(sDN):
            sFN=os.path.join(sDN,sN)
            try:
                s=os.stat(sFN)
                m=s[M_IDX]
                if stat.S_ISDIR(m):
                    dDN[sN]={'type':'dir','size':-1}#.append(sN)
                elif stat.S_ISREG(m):
                    #lFN.append(sN)
                    dFN[sN]={'size':s[S_IDX]}
                else:
                    pass
            except:
                self.__logError__('sFN:%s'%(sFN))
                self.__logTB__()
        #lDN.sort()
        #lFN.sort()
        if bDbg:
            self.__logDebug__(['sDN',sDN,'dContense',dContense])
        return dContense
    def GetCurDN(self):
        return vtiLF.getcwd()
    def MakeDirs(self,sDN):
        return vtiFT.makedirs(sDN)
    def IsExists(self,sFN):
        return vtiLF.exists(sFN)
    # ---------------------------------------------------------
    # inheritance
    def GetData(self,node,dImg={},thd=None,**kwargs):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        try:
            self.__logDebug__(''%())
            lHdr=[  (_(u'Name'),    -1,None,1),
                    (_(u'Size'),    -2,80,1),
                    (_(u'%'),       -1,120,1),
                ]
            dSortDef={-1:{'iSort':2}}
            lData=[]
            lRoot=self.GetRootLst()
            iOfs=0
            dMnt=self.GetMntDict()
            if dImg is None:
                dImg={}
            dPer=dImg.get('percent',{})
            fIdxDelta=dPer.get('fIdxDelta',0)
            for sRoot in lRoot:
                dVal=self.GetRootInfoDict(sRoot)
                if sRoot in dMnt:
                    dVal['label']=dMnt[sRoot][:]
                if 'used' in dVal:
                    iSz=dVal.get('used',0)
                    if iSz>=0:
                        sSz=vtiLF.getSizeStr(iSz,2)
                    else:
                        sSz=''
                else:
                    sSz=''
                    iSz=0
                fPer=0.0
                sPer=''
                if 'percent' in dVal:
                    fPer=dVal.get('percent',0.0)
                    if fPer>=0:
                        sPer='%5.1f'%fPer
                        iPer=dPer[int(fPer*fIdxDelta)]
                    else:
                        iPer=-1
                else:
                    iPer=-1
                #imgTup=self.__getImagesByInfos__(dVal.get('type','unknown'),{})
                dVal.update({0:dVal.get('root',''),1:iSz,2:fPer})
                l=[dVal,[
                    (self.GetRootLabel(dVal),self.__getImgFromDict__(dImg,'elem',dVal.get('type','unknown'))),
                    (sSz,-1),
                    (sPer,iPer),
                    ]]
                lData.append(l)
            #if func is not None:
            #    self.CallBack(func,lData,lHdr,dSortDef,*args,**kwargs)
            return (lData,lHdr,dSortDef,False)
        except:
            self.__logTB__()
        return (None,None,None,False)
    def getImageData(self):
        return  \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xaf0\xc2\x13}\x01\x89\x89?8\xa1\
\xe2\rV^\xe0\nF\x8e\x81\tT5qU\x04\x19\x9d\xd8u#G\xc4UH\xab\r\xfd\xa9@\x00\
\xfb\xb4J\x8a\xb9\xd2A\x0b\x03\x00\xb9\x1d\xb0WY\xdd\x82\xa6\x837\x95\x15\
\x18\xfd\x81\xd1\x1fy\x01f=&2\x916\xf4\xa7t\xf3\xa0\x04\x18a\xd9\x1a\xed\xa0\
\x94\xccz\xff/\xd1\xb2\x99\xeb9@\x87\xc4\x1aL\xb1'\x07,a1\x81\xb9\xda\xa7Uj\
\xea7C\x8c\xeb\xf3\x12\xc5\xbaF@\xdf\x06\xfb\xaa\xe9\xc0J'K\xec\x05\xafK8\
\xb4%M\x8aV\x00\x00\x00\x00IEND\xaeB`\x82"
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerRootPanel
        else:
            return None
