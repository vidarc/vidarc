#----------------------------------------------------------------------------
# Name:         vExplorerReNameDlg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100402
# CVS-ID:       $Id: vExplorerReNameDlg.py,v 1.3 2010/04/03 19:11:22 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time,os,types
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.tool.InOut.vtInOutFileInfo as vtioFI
    import vidarc.tool.InOut.vtInOutFileTools as vtioFT
    
    import vidarc.vTools.vExplorer.imgExp as img
    from vSystem import isPlatForm
    WIN_FILE=0
    if isPlatForm('win'):
        SYSTEM=1
        try:
            import win32file
            WIN_FILE=1
        except:
            pass
    else:
        SYSTEM=0
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerReNameDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('lblCt',   (0,1),(1,1),{'name':'lblCur','label':_(u'current')},None),
            ('lblCt',   (0,2),(1,1),{'name':'lblPreView','label':_(u'preview')},None),
            ('lstMulti',(1,1),(2,1),{'name':'lstCur',},None),
            ('lst',     (1,2),(2,1),{'name':'lstPreView',},None),
            ('lblRg',   (3,0),(1,1),{'name':'lblNew','label':_(u'new name')},None),
            ('txt',     (3,1),(1,2),{'name':'txtNew','value':u''},None),
            ('lblRg',   (4,0),(1,1),{'name':'lblReplace','label':_(u'replace')},None),
            ('txt',     (4,1),(1,1),{'name':'txtMatch','value':''},None),
            ('txt',     (4,2),(1,1),{'name':'txtReplace','value':''},None),
            ('lblRg',   (5,0),(1,1),{'name':'lblInsert','label':_(u'insert')},None),
            ('txt',     (5,1),(1,1),{'name':'txtInsLeft','value':''},None),
            ('txt',     (5,2),(1,1),{'name':'txtInsRight','value':''},None),
            ('lblRg',   (6,0),(1,1),{'name':'lblCrop','label':_(u'crop')},None),
            ('txt',     (6,1),(1,1),{'name':'txtCropLeft','value':'0'},{'txt':self.OnCropLeftText}),
            ('txt',     (6,2),(1,1),{'name':'txtCropRight','value':'0'},{'txt':self.OnCropRightText}),
            ('lblRg',   (7,0),(1,1),{'name':'lblSuff','label':_(u'suffix')},None),
            ('chk',     (7,1),(1,1),{'name':'chkDate','label':_(u'date'),},None),
            ('chk',     (7,2),(1,1),{'name':'chkTime','label':_(u'time'),},None),
            ('chk',     (8,1),(1,1),{'name':'chkRestict','label':_(u'restrictive'),},None),
            #('lblRg', (1,0),(1,1),{'name':'lblExec','label':_(u'executable')},None),
            #('txt',   (1,1),(1,2),{'name':'txtExec','value':''},None),
            ('cbBmp',   (2,3),(1,1),{'name':'cbPreView','bitmap':vtArt.getBitmap(vtArt.Glass),},
                {'btn':self.OnPreView}),
            ('szBoxVert',(3,3),(4,1),
                    {'name':'bxsBt',},[
                    ('cbBmp',   0,4,{'name':'cbErase','bitmap':vtArt.getBitmap(vtArt.Erase),},
                        {'btn':self.OnErase}),
                    ])
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExpRen',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='frame',
                bmp=None or img.getPluginBitmap(),
                title=None or _(u'VIDARC Explorer rename'),
                pnPos=None,pnSz=(300,400),pnStyle=None,
                pnName='pnExpRen',
                lGrowCol=[(1,1),(2,1)],lGrowRow=[(1,2)],
                bEnMark=False,bEnMod=False,lWid=lWid)
    def OnErase(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            pn.txtNew.SetValue(u'')
            pn.txtMatch.SetValue(u'')
            pn.txtReplace.SetValue(u'')
            pn.txtInsLeft.SetValue(u'')
            pn.txtInsRight.SetValue(u'')
            pn.txtCropLeft.SetValue(u'0')
            pn.txtCropRight.SetValue(u'0')
            pn.chkDate.SetValue(False)
            pn.chkTime.SetValue(False)
            pn.chkRestict.SetValue(False)
        except:
            self.__logTB__()
    def __getReplaceDict__(self):
        try:
            d={}
            pn=self.GetPanel()
            s=pn.txtNew.GetValue()
            if len(s)>0:
                d['new']=s
            s=pn.txtMatch.GetValue()
            if len(s)>0:
                d['match']=s
            s=pn.txtReplace.GetValue()
            if len(s)>0:
                d['repl']=s
            s=pn.txtInsLeft.GetValue()
            if len(s)>0:
                d['insLeft']=s
            s=pn.txtInsRight.GetValue()
            if len(s)>0:
                d['insRight']=s
            try:
                d['cropLeft']=int(pn.txtCropLeft.GetValue())
            except:
                pass
            try:
                d['cropRight']=int(pn.txtCropRight.GetValue())
            except:
                pass
            zNow=time.time()
            tNow=time.gmtime(zNow)
            bFlag=pn.chkDate.GetValue()
            if bFlag==True:
                d['date']=time.strftime("_%Y%m%d",tNow)
            bFlag=pn.chkTime.GetValue()
            if bFlag==True:
                d['time']=time.strftime("_%H%M%S",tNow)
            bFlag=pn.chkRestict.GetValue()
            if bFlag==True:
                d['rest']=bFlag
        except:
            self.__logTB__()
        return d
    def __getRenameNew__(self,sOld,d,bDbg=False):
        try:
            sN=sOld[:]
            def insAtExt(sN,sA):
                iExt=sN.rfind('.')
                if iExt>0:
                    sN=sN[:iExt]+sA+sN[iExt:]
                else:
                    sN=sN+sA
                return sN
            if 'new' in d:
                sN=d['new'][:]
            if 'match' in d:
                sN=sN.replace(d['match'],d.get('repl',u''))
            if 'insLeft' in d:
                sN=d['insLeft']+sN
            if 'insRight' in d:
                sN=insAtExt(sN,d['insRight'])
            if 'date' in d:
                sN=insAtExt(sN,d['date'])
            if 'time' in d:
                sN=insAtExt(sN,d['time'])
            iFound=vtioFI.exists(sN)
            if bDbg==True:
                self.__logDebug__('old:%s;new:%s;found:%d'%(sOld,sN,iFound))
            return sN
        except:
            self.__logTB__()
        return None
    def __getReplaceLst__(self):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            l=[]
            d=self.__getReplaceDict__()
            pn=self.GetPanel()
            lSel=pn.lstCur.GetSelections()
            if bDbg==True:
                self.__logDebug__('lSel;%s'%(self.__logFmt__(lSel)))
            if len(lSel)==0:
                lSel=range(pn.lstCur.GetCount())
            for iIdx in lSel:
                sOld=pn.lstCur.GetString(iIdx)
                sNew=self.__getRenameNew__(sOld,d,bDbg)
                if sNew is not None:
                    l.append((sOld,sNew))
        except:
            self.__logTB__()
        return l
    def OnPreView(self,evt):
        try:
            self.__logDebug__(''%())
            l=self.__getReplaceLst__()
            pn=self.GetPanel()
            pn.lstPreView.Clear()
            for t in l:
                sNew=t[1]
                pn.lstPreView.Append(sNew)
        except:
            self.__logTB__()
    def OnCropLeftText(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCropRightText(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def DoModal(self,sBase,lDN,lFN):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__('sBase:%r;lDN;%s;lFN:%s'%(sBase,
                        self.__logFmt__(lDN),self.__logFmt__(lFN)))
            else:
                self.__logDebug__('sBase:%r'%(sBase))
            bVol=False
            if type(sBase)==types.DictType:
                bVol=True
            else:
                sBase=vtioFI.fn2posix(sBase)
            pn=self.GetPanel()
            self.lName=lDN+lFN
            self.lName.sort()
            pn.lstPreView.Clear()
            pn.lstCur.Clear()
            for sN in self.lName:
                pn.lstCur.Append(sN)
            iRet=self.ShowModal()
            if iRet>0:
                l=self.__getReplaceLst__()
                if bDbg==True:
                    self.__logDebug__('l;%s'%(self.__logFmt__(l)))
                for t in l:
                    if bVol==True:
                        if SYSTEM==1:
                            if WIN_FILE==1:
                                try:
                                    iType=win32file.GetDriveType(t[0])
                                    if iType in [win32file.DRIVE_FIXED,win32file.DRIVE_REMOVABLE]:
                                        win32file.SetVolumeLabel(t[0],t[1])
                                except:
                                    self.__logError__('root:%s;label:%s'%(t[0],t[1]))
                                    self.__logTB__()
                            else:
                                self.__logError__('volume rename not possible, win32file missing'%())
                        else:
                            self.__logError__('volume rename not available on this platform'%())
                    else:
                        if sBase[-1]=='/':
                            sOld=vtioFI.join([sBase,t[0]])
                            sNew=vtioFI.join([sBase,t[1]])
                        else:
                            sOld=u''.join([sBase,t[0]])
                            sNew=u''.join([sBase,t[1]])
                        iOld=vtioFI.exists(sOld)
                        iNew=vtioFI.exists(sNew)
                        if bDbg==True:
                            self.__logDebug__('old(%d):%s;new(%d):%s'%(iOld,
                                    sOld,iNew,sNew))
                        try:
                            if iOld==1:
                                if iNew==0:
                                    os.rename(sOld,sNew)
                                    time.sleep(0.1)     # hack to allow vtInOutNotify to catch up
                        except:
                            self.__logError__('old(%d):%s;new(%d):%s'%(iOld,
                                    sOld,iNew,sNew))
                            self.__logTB__()
            return iRet
        except:
            self.__logTB__()
