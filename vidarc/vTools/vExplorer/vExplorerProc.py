#----------------------------------------------------------------------------
# Name:         vExplorerProc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100517
# CVS-ID:       $Id: vExplorerProc.py,v 1.6 2012/01/12 10:06:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vTimeStamp import getTimeStamp4FN
import os,types

import vSystem
try:
    if vcCust.is2Import(__name__):
        from vExplorerProcPipesFrame import vExplorerProcPipesFrame
        import vidarc.tool.InOut.vtInOutFileInfo as vtioFI
        from vidarc.tool.vtThread import vtThread
        from vidarc.tool.vtProcess import vtProcess
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vExplorerProc:
    def __init__(self,sName,sCmd,parent,sDN='',sPipesDN='',
                bDetach=False,bShell=False,bSavePipes=False,bWatch=False,
                timeout=2,lGeometry=None,zClose=None):
        self.iAlmCount=0
        self.frmPipes=None
        sCwd=os.getcwdu()
        try:
            self.tiSel=None
            self.funcNotify=None
            try:
                if vtioFI.exists(sDN)>0:
                    os.chdir(sDN)
            except:
                vLogFallBack.logTB(__name__)
            self.ident=u':'.join([sName,u'???'])
            if bDetach or GUI==0:
                self.p=vtProcess(sCmd,detached=True)
            else:
                tPos=None
                tSz=(500,400)
                try:
                    if lGeometry is not None:
                        tPos=(lGeometry[0],lGeometry[1])
                        tSz=(lGeometry[2],lGeometry[3])
                except:
                    tPos=None
                    tSz=(500,400)
                self.frmPipes=vExplorerProcPipesFrame(parent,pos=tPos,sz=tSz,
                        zClose=zClose)
                self.thdExec=vtThread(self.frmPipes,bPost=True)
                self.thdExec.BindEvents(funcProc=self.OnExecProc,
                        funcAborted=self.OnExecAborted,funcFinished=self.OnExecFin)
                #self.thdExecParent=thdExec
                self.p=vtProcess(sCmd,shell=bShell,timeout=timeout)
                self.res=[]
                self.p.SetCB(1,'\n',self.handleStdOut)
                self.p.SetCB(2,'\n',self.handleStdErr)
                #self.p.SetCB(2,None,self.handleStdErr)
                
                self._pid=self.p._p.pid
                self.ident=u':'.join([sName,str(self.p._p.pid)])
                if self.frmPipes is not None:
                    self.frmPipes.SetProc(self)
                    self.frmPipes.SetState('running')
                    self.frmPipes.SetTitle(self.__str__())
                    
                if bWatch==True:
                    self.frmPipes.Show()
                    #self.frmPipes.SetCmd(cmd)
                
                sFN=u'.'.join([sName,getTimeStamp4FN(),str(self.p._p.pid),'txt'])
                if len(sPipesDN)>0:
                    if sPipesDN[-1]=='/':
                        sFN=vtioFI.join([sPipesDN[:-1],sFN])
                    else:
                        sFN=vtioFI.join([sPipesDN,sFN])
                self.bWrite2File=bSavePipes
                if self.bWrite2File:
                    self.fOut=open(vtioFI.posix2fn(sFN),'wb')
                else:
                    self.fOut=None
                self.thdExec.Do(self.p.communicateLoop)
                if self.bWrite2File:
                    self.thdExec.Do(self.fOut.close)
        except:
            vLogFallBack.logTB(__name__)
        os.chdir(sCwd)
    def GetIdent(self):
        return self.ident
    def GetPID(self):
        return self._pid
    def BindEvent(self,name,func,par=None):
        if self.frmPipes is not None:
            self.frmPipes.BindEvent(name,func,par=par)
    def SetTreeItem(self,ti):
        self.tiSel=ti
    def SetFuncNotify(self,func):
        self.funcNotify=func
    def Show(self,flag):
        if self.frmPipes is not None:
            self.frmPipes.Show(flag)
            if flag:
                try:
                    if self.frmPipes.IsIconized():
                        self.frmPipes.Iconize(False)
                except:
                    pass
                self.frmPipes.Raise()
    def ClrFrm(self):
        self.frmPipes=None
    def Close(self):
        if self.frmPipes is not None:
            self.frmPipes.Close()
            self.frmPipes=None
    def SetFocus(self):
        if self.frmPipes is not None:
            self.frmPipes.SetFocus()
    def __str__(self):
        return self.ident
    def OnExecProc(self,evt):
        evt.Skip()
    def OnExecFin(self,evt):
        evt.Skip()
        self.frmPipes.SetState('stopped')
    def OnExecAborted(self,evt):
        evt.Skip()
        self.frmPipes.SetState('aborted')
    def HasAlmHigh(self):
        return self.iAlmCount>0
    def ResetAlmHigh(self):
        self.iAlmCount=0
    def UpdateTI(self,tr,ti):
        if self.HasAlmHigh():
            tr.SetItemText(ti,str(self.iAlmCount),1)
        else:
            tr.SetItemText(ti,u'',1)
    #def handleStdErr(self,s1):
    #    s=s1.replace('\r','')
    def handleStdErr(self,s):
        self.res.append(s)
        if self.bWrite2File:
            self.fOut.write(s)
        #self.fOut.write('\n')
        #self.fOut.write()
        self.iAlmCount+=1
        #if self.tr is not None:
        #    self.thdExec.CallBack(self.UpdateTI,self.tr,self.tiSel)
        if self.frmPipes is not None:
            self.thdExec.CallBack(self.frmPipes.AddStdErr,s)
        if self.funcNotify is not None:
            self.thdExec.CallBack(self.funcNotify,1)
    #def handleStdOut(self,s1):
    #    s=s1.replace('\r','')
    def handleStdOut(self,s):
        if self.bWrite2File:
            self.fOut.write(s)
        #self.fOut.write('\n')
        #os.write(self.fOut,s)
        #os.write(self.fOut,'\n')
        if self.frmPipes is not None:
            self.thdExec.CallBack(self.frmPipes.AddStdOut,s)
    def WriteStdIn(self,s,bLineSep=False,bEnter=True):
        self.p._p.stdin.write(s)
        if bLineSep:
            self.p._p.stdin.write(os.linesep)
        if bEnter:
            self.p._p.stdin.write('\n')
        self.p._p.stdin.flush()
    def IsRunning(self):
        return self.p.isRunning()
    def Kill(self):
        try:
            return self.p.Kill()
        except:
            vLogFallBack.logTB(__name__)
            return -100

