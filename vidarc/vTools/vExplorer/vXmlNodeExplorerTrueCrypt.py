#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerTrueCrypt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vXmlNodeExplorerTrueCrypt.py,v 1.7 2010/03/01 14:11:31 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerTrueCryptPanel import vXmlNodeExplorerTrueCryptPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerTrueCrypt(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerTrueCrypt'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer true crypt')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=0):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    
    def GetExec(self,node,sFN=None):
        #sTag=self.GetTag(node)
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['appl_truecrypt','root','pwd']
        else:
            return sTag,sFN,'"'+sApp+'" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['root','pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /d %s /s /q',\
                    ['appl_truecrypt','root']
        else:
            return sTag,sFN,'"'+sApp+' /d %s /s /q',\
                    ['root']
    def GetApplName(self):
        return 'appl_truecrypt'
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02rIDAT8\x8de\x92KkTY\x14F\xd7>\xe7\xdc{\xabL\x15\x86D\x13\xd3>:\xc1\
\x96\xa0\xe8L\x10\xfd\x07\xa2\x08\n=q\xdc\xf4Ht$\xfd\x1b\x9c\xf4\xa4\xe9\x99\
Cq\xa8\x93\x86\x1e5\xa83\x9d((\n>bL\x91\x87U11\xb7\xac\xc7\xbdu\xce\xd9=\xb8\
\x18:\xd5\x1b\xf6t}\x1fko\x11cI\x17~\xd6\xfa\xc9_@\x03\x82\xa0\x00\x08"\x82j\
\xc0\xb8\x1a\x1a\x031\x14\x88\x18@\xd8\xf9\xfb\xa2\x008\x00\x8d\x81\xa4\xd6\
\xa4\xfc\xd6!\x84\x92\xc9\x83\xc7\xf1!\x90$\x8e\xd1\xa8\x04"\xbd\x9d\xcf\x08\
\x02\xceQ\xe4\xeb|\x1fW\x85\t\xa8""\xfcp\xe2\x02\x7f\xfe:\xc5\xdb5\xe1\xf2Y\
\xc7\xe3W\x81,\x8d\xdcy\xf0\x8d\x8dv\x9bAw\x034\xee\x05\x88\x08\x88ejz\x86\
\xc5\xd9\xc8\xed\xbb\xeb\xe4\xdd\x9c{\xff\x1c\xa0\xbd\xb9E\xa31\xc1\xe9\xf9:\
\xa1HX\xe9\nb\xec.\xc0T\r\x0c\x02,\x1e\x9b\xe4\xc6\x95\x19\xfc\xa8\xa0\x9fw\
\xc8\xbb=\x8a\xa2\x87\'\xe1\xd2\xb9)\xe6\xe7\x1a\xb8t\x02\x1do\x80*\xaa\x81\
\xe7\xef\xb6\xb9}\xb7\xce\xadkGhu\x0e\xf1\xd3\xe1\x8c\x17K\xb3\x9c:\x96\xf2\
\xfb\x83MV\xd6V\x19\r\xbe\xa2q\x0c\xa0\xaa(J9\xd8\xe1K\xbb\xe4\xaf\xa7\x81^\
\x99R\xcfR^,\rXZ\xcdi\xb5\x96)\x87=\xc4&{\x1c\x98\xca\x81\x01\x0c\xa3\xb2O\
\xda\x9c\xe3\xd9\xeb/\xe4}\xe1\xea\x85\x944\xb1\xbco\xed05}\x08\xe3\x1c\x8a\
\x10\xa3\x1f\x93h,\xa2\x11\r#\xb6\xd6_C\xd1\xa5\x9d\xc0\xfd\xc7\x93\xbc[j\
\xb1\xddY&\xc6\x88\x0f\x8a\xab\xed\x87\x18\xc6\x1c D\x8d\x80\x92d\r\x10\x98H\
\r\x8b\x87-\x89U\x12\x9b\xa0\xce\xa0>\x12\xa3G\xac\x1bw\x10\x00\xc5$5\x1a\
\xcd\x03Xm0}p\x8e\x85Y\xc3\xcc\xf4~\x06\x83\x01.\xc9\x88\xdd>>\x14\xa8\xeax\
\x83J\xa4\x1fv\xd9\xdc\xf8\xc0\xf1\x85\x1f\x19\x16C\x1e\xbd\xf2|\xf8\xf8\x89\
\xed\xf62\xaa\x80M\xc8\x9a\xb3\xfcwv\x1fI\xc4`\xd3}\x1c\x99?\xc3o\xd7\xe7(C\
\xca\xf9E\xcb\xc3\'Gq\xaeF\x10K7\xdf\xaa\xd2\xff\xef@A\x03\xc6\xd5Y[y\xc3\
\xcd?:h\x8c\x18\x971\xccW\xd1\xe8\xd1\xe81\xae\x86\x98\xa4:\xe5^\x80\xa1\xb7\
\xfe\x12\x1d\xf5Aa\xa0\x1e\x11\x0bb\xd1\xe8\xab3\x8b\x82V_K\xd6\x18\x93\x18=\
\x18\x8b\xf7\x05\xa8V+#\x88\xbe\xaa,U\xc9J\xf4>\xd2\xac\xb9\x0b\xf8\x17P\xb8\
-U\x1c\xde\xdd\xd4\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerTrueCryptPanel
        else:
            return None
    def Build(self):
        self.__logDebug__(''%())
        self.doc.RegisterExtension('TC',self)
