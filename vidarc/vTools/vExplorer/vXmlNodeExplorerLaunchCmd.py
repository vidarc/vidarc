#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchCmd.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100222
# CVS-ID:       $Id: vXmlNodeExplorerLaunchCmd.py,v 1.7 2012/01/12 10:08:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import os,types

import vSystem
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    from vExplorerProc import vExplorerProc
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerLaunchCmdPanel import vXmlNodeExplorerLaunchCmdPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vXmlNodeExplorerLaunchCmd(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerLaunchCmp'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
        self.zNow=vtDateTime()
    def GetDescription(self):
        return _(u'explorer launch command')
    # ---------------------------------------------------------
    # specific
    def GetDateGM(self,node):
        self.zNow.Convert2GM()
        return self.zNow.GetDateSmallStr()
    def GetTimeGM(self,node):
        self.zNow.Convert2GM()
        return self.zNow.GetTimeSmallStr()
    def GetDateTimeGM(self,node):
        self.zNow.Convert2GM()
        return self.zNow.GetDateSmallStr()+self.zNow.GetTimeSmallStr()
    def GetHost(self,node):
        return vSystem.getHost()
    def GetHostFull(self,node):
        return vSystem.getHostFull()
    def GetArgDict(self):
        return {
            '%dn%':         _(u'directory'),
            '%sdn%':        _(u'selected directory(s)'),
            '%sdn0%':       _(u'fist selected directory(s)'),
            '%sfn0%':       _(u'first selected file'),
            '%sfn%':        _(u'selected file(s)'),
            '%sel%':        _(u'selected'),
            '%dn_save%':    _(u'save directory'),
            #'%sel0%':_(u'first selected'),
            '%vdn0%':       _(u'current view directory'),
            '%vdn1%':       _(u'previous view directory'),
            '%view0%':      _(u'current view'),
            '%view1%':      _(u'previous view'),
            '%date%':       _(u'current date'),
            '%time%':       _(u'current time'),
            '%now%':        _(u'current date and time'),
            '%host%':       _(u'hostname'),
            '%hostname%':   _(u'full hostname'),
            }
    def GetArgMapFmtDict(self):
        return {
            '%dn%':         self.GetDN,
            '%sdn%':        ('%s','sdn'),
            '%sdn0%':       ('%s','sdn0'),
            '%sdn1%':       ('%s','sdn1'),
            '%sfn0%':       ('%s','sfn0'),
            '%sfn%':        ('%s','sfn'),
            '%sel%':        ('%s','sel'),
            '%dn_save%':    self.GetSaveDN,
            #'%sel0%': ('%s','sel0'),
            '%vdn0%':       ('%s','vdn0'),
            '%vdn1%':       ('%s','vdn1'),
            '%view0%':      ('%s','view0'),
            '%view1%':      ('%s','view1'),
            '%date%':       self.GetDateGM,
            '%time%':       self.GetTimeGM,
            '%now%':        self.GetDateTimeGM,
            '%host%':       self.GetHost,
            '%hostname%':   self.GetHostFull,
            }
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetShell(self,node):
        return self.Get(node,'shell')
    def GetShellVal(self,node,fallback=0):
        return self.GetVal(node,'shell',int,fallback=fallback)
    def GetWatch(self,node):
        return self.Get(node,'watch')
    def GetWatchVal(self,node,fallback=1):
        return self.GetVal(node,'watch',int,fallback=fallback)
    def GetDetach(self,node):
        return self.Get(node,'detach')
    def GetDetachVal(self,node,fallback=0):
        return self.GetVal(node,'detach',int,fallback=fallback)
    def GetSave(self,node):
        return self.Get(node,'save')
    def GetSaveVal(self,node,fallback=0):
        return self.GetVal(node,'save',int,fallback=fallback)
    def GetAuto(self,node):
        return self.Get(node,'auto')
    def GetAutoVal(self,node,fallback=0):
        return self.GetVal(node,'auto',int,fallback=fallback)
    def GetDly(self,node):
        return self.Get(node,'delay')
    def GetDlyVal(self,node,fallback=0):
        return self.GetVal(node,'delay',float,fallback=fallback)
    def GetDlyCls(self,node):
        return self.Get(node,'dlycls')
    def GetDlyClsVal(self,node,fallback=-1.0):
        return self.GetVal(node,'dlycls',float,fallback=fallback)
    def GetArg(self,node):
        return self.Get(node,'arg')
    def GetDN(self,node):
        return self.Get(node,'dn')
    def GetSaveDN(self,node):
        return self.Get(node,'dn_save')
    def GetGeometry(self,node):
        try:
            l=[int(s) for s in self.Get(node,'geometry').split(',')]
            return l
        except:
            return None
    def GetSched(self,node):
        return self.Get(node,'sched')
    def GetSchedVal(self,node,fallback=0):
        return self.GetVal(node,'sched',int,fallback=fallback)
    def GetImg(self,node):
        return self.Get(node,'img')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetShell(self,node,val):
        self.Set(node,'shell',val)
    def SetWatch(self,node,val):
        self.Set(node,'watch',val)
    def SetDetach(self,node,val):
        self.Set(node,'detach',val)
    def SetSave(self,node,val):
        self.Set(node,'save',val)
    def SetAuto(self,node,val):
        self.Set(node,'auto',val)
    def SetArg(self,node,val):
        self.Set(node,'arg',val)
    def SetDN(self,node,val):
        self.Set(node,'dn',val)
    def SetSaveDN(self,node,val):
        self.Set(node,'dn_save',val)
    def SetDly(self,node,val):
        self.Set(node,'delay',val)
    def SetDlyCls(self,node,val):
        self.Set(node,'dlycls',val)
    def SetGeometry(self,node,x,y,w,h):
        self.Set(node,'geometry','%d,%d,%d,%d'%(x,y,w,h))
    def SetSched(self,node,val):
        self.Set(node,'sched',val)
    def SetImg(self,node,val):
        self.Set(node,'img',val)
    
    def GetExec(self,node,sFN=None):
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        sTag=self.GetTag(node)
        self.__logDebug__(''%())
        sApp=''
        sArg=''
        if sFN is None:
            sFN=''#self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
            sArg=self.GetArg(node)
        if len(sApp)>0:
            self.zNow.Now()
            def conv(sArg):
                iLen=len(sArg)
                iOfs=iLen
                dArg=self.GetArgMapFmtDict()
                lFmt=[]
                while iOfs>=0:
                    iE=sArg.rfind('%',0,iOfs)
                    if iE>=0:
                        iS=sArg.rfind('%',0,iE-1)
                        if bDbg:
                            self.__logDebug__('%s;iS:%8d;iE:%8d'%(sArg[iS:iE+1],iS,iE))
                        if iS>=0:
                            if (iS+1)!=iE:
                                sK=sArg[iS:iE+1]
                                if sK in dArg:
                                    v=dArg[sK]
                                    t=type(v)
                                    #if iS>0:
                                    #    iS-=1
                                    if t==types.TupleType:
                                        sArg=sArg[:iS]+v[0]+sArg[iE+1:]
                                        lFmt.append(v[1])
                                    elif t==types.MethodType:
                                        sArg=sArg[:iS]+v(node)+sArg[iE+1:]
                                    else:
                                        pass
                                    if bDbg:
                                        self.__logDebug__('sArg:%s'%(sArg))
                                else:
                                    s=os.getenv(sK[1:-1])
                                    if s is None:
                                        self.__logError__('key:%s not found'%(sK[1:-1]))
                                        s=''
                                    sArg=sArg[:iS]+s+sArg[iE+1:]
                                iOfs=iS#-1
                            else:
                                iOfs=iS-1
                        else:
                            iOfs=-1
                    else:
                        iOfs=-1
                lFmt.reverse()
                return sArg,lFmt
            sApp,lApp=conv(sApp)
            sArg,lFmt=conv(sArg)
            if self.GetDetachVal(node)>0:
                lFmt.append('detach')
            if self.GetShellVal(node)>0:
                lFmt.append('shell')
            sDN=self.GetDN(node)
            if sDN>0:
                lFmt.append(('path',sDN))
            sDN=self.GetSaveDN(node)
            if sDN>0:
                lFmt.append(('savedn',sDN))
            if bDbg:
                self.__logDebug__('sTag:%s; sFN:%s;sApp:%s;sArg:%s;lFmt:%s'%(sTag,sFN,
                        sApp,sArg,self.__logFmt__(lFmt)))
            return sTag,sFN,'"%s" %s'%(sApp,sArg),lFmt
        else:
            return sTag,sFN,'',[]
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /d %s /s /q',\
                    ['appl_truecrypt','root']
        else:
            return sTag,sFN,'"'+sApp+' /d %s /s /q',\
                    ['root']
    def GetApplName(self):
        return None#'appl_truecrypt'
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar,**kwargs):
        self.__logDebug__(''%())
        sCmd=kwargs.get('sCmd',None)
        if sCmd is None:
            return
        sTag=self.GetTag(node)
        sApp=''
        sArg=''
        if node is not None:
            sApp=self.GetApp(node)
            sArg=self.GetArg(node)
        if len(sApp)<=0:
            return 
        try:
            self.__logInfo__(''%())
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__('kwargs:%s'%(self.__logFmt__(kwargs)))
            
            bDetach=self.GetDetachVal(node)>0
            bShell=self.GetShellVal(node)>0
            bSave=self.GetSaveVal(node)>0
            bWatch=self.GetWatchVal(node)>0
            sDN=self.GetDN(node)
            sSaveDN=self.GetSaveDN(node)
            zClose=self.GetDlyClsVal(node)
            if zClose<0:
                zClose=None
            lGeometry=self.GetGeometry(node)
            oProc=vExplorerProc(sTag,sCmd,parent,
                    sDN=sDN,sPipesDN=sSaveDN,
                    bDetach=bDetach,bShell=bShell,bSavePipes=bSave,bWatch=bWatch,
                    lGeometry=lGeometry,zClose=zClose)
            return oProc
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02xIDAT8\x8du\x93?O\xdbP\x14\xc5\x7f\xcf\x8e\x00yp\x8cEj\x11T\x9aH\xd0\
\xc6i\xc2\x82"\xe1\xa1\xe5\x8f\xb20"\x96\xb6[\x90\xb2\xf3\x11\xf8\x16m\x176\
\xbe\x03\x03\x03m\x07##5\x15 \x19$:B\x9a&H\x0e\xc1\x06\x81\x9c\xd7\xa5q\xc5\
\x9f\xde\xe9\xdd\xa3{\x8e\x8e\xee}G\x08E\xe5a\xc9~,\xab\xd5*\x96e\x01\xd0j\
\xb5\xd8\xd9\xd9A(\xaax8\x9bz\xc4\x06FGG\xa9T*I?99\x89\xe7y\xf4\xae\xc2G\xb3\
\xca\xe0Q.\xbd\x96\xb2\x1fK\xd9\x8f\xe5\xfc\xfc<\x86a\x90\xcf\xe7\xc9\xe7\
\xf3\x18\x86\xc1\xd2\xd2\x12\xb2\x1f\xcb\x0f\xef\xdf\xc9\x17\x93\xcf\xe5\x80\
\'\x84\xa22\xff\xf6\x8dt\x1c\x87\x9b\x9b\x1bL\xd3dff\x86\xb3\xb33...\x12\x07\
\x86a\xe0y\x1e\xcdf\x13\xcb\xb2p]\x97/_\xbf\taY\x96\\[[\x03@\xd34\xc6\xc7\
\xc7\x89\xe3\x98\xb9\xb9\xb9{V\xf7\xf6\xf6PU\x95f\xb3I\x14E\x00lnn\x92\xb2m;\
\x19\xd2u\x9d\x89\x89\tt]\xe7\xea\xea\xea\x9e@\xa9T\xe2\xf2\xf2\x920\x0c\x13\
\x01\xdb\xb6Q\x1a\x8d\x06\xadV\x8b8\x8e\xc9f\xb3\xe4r9L\xd3\xa4\xd7\xebQ\xaf\
\xd7\xa9\xd7\xeb\xf4z=L\xd3$\x97\xcb\x91\xcdf\x89\xe3\x98\x83\x83\x03\x1a\
\x8d\x06\x08EejjJ\xd6j5\xe9y\x9e\x0c\xc3P\x86a(\x8b\xc5\xa2\x14\x8a\x8aPT\
\x8a\xc5b\x82{\x9e\'k\xb5\x9a\xd4u]\nE\xfdwFM\xd3\x08\xc3\x90v\xbb\xfd\xd4e\
\x13<\x0cC4MKp5\xad\xebryy\x99L&\x83\xae\xeb\xdc\xdd\xdd\x11\x04\x01\xd3\xd3\
\xd3\xfc<=\xdd\xc8\x8c\x8dm\xac\xaf\xaf\xa3\xaa*A\x10\xd0\xe9t\x08\x82\x80\
\xe1\xe1aZ\xad_\x1bbaaA:\x8e\x03@\xa1P@\x08A\xbf\xdf\x7f\xd2\x85\xa2(H)9>>\
\x06\xc0u]R\xbe\xef3\x108??\xa7P(\xe0\xfb>\xb7\xb7\xb7\xf7\xc8CCC\xd8\xb6\
\x9d\x90\x01|\xdf\'\xf5\xbb\xdd\x11\xae\xebJ\xc7q8::\xe2\xfa\xfa\x9a\xc5\xc5\
E\xa2(\xe2\xf0\xf0\x10\x80r\xb9\x8c\xa6i\xb8\xaeK\xbb\xddfdd\x04\xd7u\xf9\
\xdd\xee\x081\x08\xd3\xb3\xcc\x98\xacV\xablmm\xb1\xba\xbaJ\xa5R!\x9dN\x03\
\xd0\xedv\xd9\xde\xdefww\x17\x80\xd9\xd9Y\xbe7~\x08\xf8\xfb\x95\x1f\xd6\xab\
\x97\xd3ree%\xe9\xbb\xdd.\x1f?}~\x94D\xf8O\x1a\xa3(b\x7f\x7f?\x89\xf3\xc9\
\xc9\xc9\x93K\x05\xf8\x03\xed\x05\x12KZ\x8e\xff*\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerLaunchCmdPanel
        else:
            return None
