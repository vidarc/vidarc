#----------------------------------------------------------------------------
# Name:         vExplorerSynchCfgDlg
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100530
# CVS-ID:       $Id: vExplorerSynchCfgDlg.py,v 1.2 2010/06/10 09:03:50 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vXmlNodeExplorerSynchPanel import vXmlNodeExplorerSynchPanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerSynchCfgDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtgDlg.__init__(self,parent,vXmlNodeExplorerSynchPanel,name=name or 'dlgExplorerSynchResult',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='popup',
                bmp=bmp,
                title=title or _(u'VIDARC Explorer Synch Configuration'),
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pnExplorerSynchCfg',
                #lGrowCol=[(0,1),],lGrowRow=[(0,1),],
                bEnMark=False,bEnMod=False)
    def Clear(self):
        self.pn.__Clear__()
    def SetValue(self,sDN1='',sDN2='',iRec=-1,bMD5=True,bMD5Always=False,
                    bAccess=False,bMod=True,bCreate=True,bAutoExpand=True,
                    bShowEqual=False,bFlat=False):
        try:
            self.__logDebug__(''%())
            self.Clear()
            self.fbbSrc1.SetValue(sDN1)
            self.fbbSrc2.SetValue(sDN2)
            self.spRec.SetValue(iRec)
            self.chkCalcMD5.SetValue(bMD5)
            self.chkCalcAlwaysMD5.SetValue(bMD5Always)
            self.chkAccess.SetValue(bAccess)
            self.chkMod.SetValue(bMod)
            self.chkCreate.SetValue(bCreate)
            self.chkAutoExpand.SetValue(bAutoExpand)
            self.chkShowEqual.SetValue(bShowEqual)
            self.chkFlat.SetValue(bFlat)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            d={
                'sDN1':         self.fbbSrc1.GetValue(),
                'sDN2':         self.fbbSrc2.GetValue(),
                'iRec':         self.spRec.GetValue(),
                'bMD5':         self.chkCalcMD5.GetValue(),
                'bMD5Always':   self.chkCalcAlwaysMD5.GetValue(),
                'bAccess':      self.chkAccess.GetValue(),
                'bMod':         self.chkMod.GetValue(),
                'bCreate':      self.chkCreate.GetValue(),
                'bAutoExpand':  self.chkAutoExpand.GetValue(),
                'bShowEqual':   self.chkShowEqual.GetValue(),
                'bFlat':        self.chkFlat.GetValue(),
                }
            return d
        except:
            self.__logTB__()
