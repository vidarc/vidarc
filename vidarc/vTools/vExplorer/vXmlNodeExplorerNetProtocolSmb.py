#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerNetProtocolSmb.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100225
# CVS-ID:       $Id: vXmlNodeExplorerNetProtocolSmb.py,v 1.3 2010/06/14 08:42:24 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.InOut.vtInOutNetTools as vtiNT

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerNetProtocolSmbPanel import vXmlNodeExplorerNetProtocolSmbPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerNetProtocolSmb(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerNetProtocolSmb'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'net protocol samba')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=1):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    
    def procMnt(self,sSrc,sMnt,t,thd=None):
        try:
            self.__logDebug__('lSrc:%s;sMnt:%s;thd:%r'%(sSrc,sMnt,thd))
            sUsr=None
            sPwd=None
            iL=len(t)
            if iL>0:
                sUsr=t[0]
            if iL>1:
                sPwd=t[1]
            if iL>2:
                sPwd=t[2](sPwd)
            return vtiNT.SambaMnt(sSrc,sMnt,sUsr,sPwd,thd=thd)
        except:
            self.__logTB__()
            return -1
    def procUnMnt(self,sMnt,thd=None):
        try:
            self.__logDebug__('sMnt:%s'%(sMnt))
            # perform several times due notify hook
            for iTry in xrange(120):
                ret=vtiNT.SambaUnMnt(sMnt,iForce=0,thd=thd)
                if ret>=0:
                    return ret
            return ret
        except:
            self.__logTB__()
            return -1
    def GetExec(self,node,sFN=None):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return u''.join(['smb:',sFN]),sFN,(self.procMnt,(sFN,),{'thd':'self'}),['drive','usr_pwd']
        ##return sHost,sFN,(self.procMnt,(sFN,),{'thd':'self'}),['drive','usr_pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return sHost,sFN,(self.procUnMnt,(),{'thd':'self'}),['drive']
    def GetApplName(self):
        return '__method'
    # ---------------------------------------------------------
    # inheritance
    def GetData(self,node,dImg={},dPar={},**kwargs):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        try:
            self.__logDebug__('dPar:%s'%(self.__logFmt__(dPar)))
            lHdr=[
                (_(u'Name'),-1,None,1),
                (_(u'Value'),-2,None,1),
                ]
            dSortDef={-1:{'iSort':2}}
            lData=[]
            dShare=vtiNT.getHostShareDict(dPar['info']['host'])
            sTagName=self.GetTagName()
            sMnt=dPar['info']['mnt']
            if sMnt in dShare[None]:
                dd=dShare[None][sMnt]
                for k,v in dd.iteritems():
                    if k is None:
                        continue
                    self.__logDebug__('k:%s;v:%s'%(k,self.__logFmt__(v)))
                    sLbl=dd.get('name','???')
                    l=[{0:k,1:v},[
                        k,
                        unicode(v),
                        ]]
                    lData.append(l)
            return (lData,lHdr,dSortDef,True)
        except:
            self.__logTB__()
        return (None,None,None,True)
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02BIDAT8\x8d\xa5\x92\xcfK\x93q\x1c\xc7_\xdfm\xba\xa7\xcdj\x8c\xb9=\xfb\
\x11\xd1\xa3.\xe8$\r\x0f\x89(!4\x10\x0c\xf1\x14\xe59\xfau\xe8\xd6?\x10t1\x08\
\xac\xc3\xaaS t\xa9K\xe1\xc1\xa2N\x1d6\x1d\xcd\xa90\xdc\x9c\xe8\xda\x0eC\xd4\
\xed1\xf5y\xa6\xcf\xd3a%\xe6\xe6!z\xdf\xbe\x9f\xcf\xe7\xfd\xe6\xfdy\x7f\xbeB\
X\xac\xfc\x0flG\x1fc\xb7n\x9a\x83\x83\x83\xb8\\.\x14E\xc1\xe3\xf1\x90\xc9d(\
\x95JLLL\x90\x98\x99\x15\xc7\x05\xc4\xf8\xf8\xb8\xd9\xd7\xd7G\xa1P \x99L\xa2\
\xaa*\xc3\xc3\xc3\xf4\xf6\xf6\xe2p8\xd8\xda\xdaBUUdY&\x99L2??O*\x95\xe2\xe5\
\xab\xd7\x02\xc0\xe6\xf3\xf9p:\x9dD"\x11B\xa1\x10\x81@\x00\xc30\x9a\xda\r\
\x87/!I\x01Tu\x8f\x07\xf7\x1f\x9a\xcf_<\x13\xb6\x95\x95\x154M\xc3\xef\xf7\
\xe3v\xbb\x01\xb0\xdb\xed\xe4\xf3y$IB\xd34\xaa\xd5*\xd3\xd3\x9f\xc8\xe7\xd7\
\x98\x99\x89\xd3\xdf\x7f\x9dZ\xad\xa5\x9e\x81\xaa\xaa\xb8\xdd\xee\xc3\x15\
\x14E\xa1\xbd\xbd\x1dY\x96\x99\x9a\x9a\xa2\\.\xa3\xaa*^\xaf\x97r\xb9H\xa5\
\xb2\x81,w\xb2\xbe\xbeQ\xcf@X\xac\x84\xbb:\xcd\xd1\xd1QFFFXZZ"\x97\xcba\xb5Z\
\xd14\x9dtz\x8e\x8e\x8e\x0e\xda\xda\xda\xa8Tv\xa9V\xf7I\xa7\xe3,,$\xd9?0\x84\
8z\xc6\xb3gN\x9bCCCtww\x13\x0c\x06I$\xbe3;\x9b@\x92\x1c\x18\x86\x85lv\x91b\
\xe9\xc7_\x97\x10\xcd\xfe\x81\xbd\xb5\xc5\x8cF\xa3\\\x8e\x0c\xf0\xfe\xdd$\
\x99\xcc\x02zm\xbf\xe1\x84\x00\x96fEM\xaf\x89H$\x82\xe7|\x88bq\xf5D\xf2\x89\
\x0e\xe6&\x15\xb3\xd5\x11D7%\xb67\x0bX\xec~\xae\x8c}m*"b\xb1\x98y\xbc8p\xee#\
\xbe\x9e\xa7\xb8<]\xecl.\x12\x7fs\x95\xec\xa9\xc7\r\xe4;w\xef\t\x1b\xc0\xb5\
\x1b\xb7\xebj\xbf\x1b??\xc7@_\x85=\x95\x83\x9d5L\xd3v8\xf3\x07\xd1\x9e\x8buN\
8\x1cnp\xf0\xe1\xc9&n\x8f\x97\xed]\x13\xa7d\xf0\xed\xcb2\x8f\xde^hp\x90\xcd-\
\x0b[6\xb7\xdc\xb0[*n\x98zm\x1d\x80\x16\x9b@\xd7M\x9a\xcd\xc1\t!\xfe\x0b~\
\x01\xf1\x90\xe5D1\x85\xf7\xe7\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerNetProtocolSmbPanel
        else:
            return None
    def Build(self):
        self.__logDebug__(''%())
        self.doc.RegisterNetProtocol('smb',self)
