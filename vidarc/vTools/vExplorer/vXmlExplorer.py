#----------------------------------------------------------------------------
# Name:         vXmlExplorer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vXmlExplorer.py,v 1.6 2010/02/28 21:56:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,stat

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vTools.vExplorer.vXmlNodeExplorerRoot import vXmlNodeExplorerRoot
from vidarc.vTools.vExplorer.__register__ import RegisterNodes as RegNodeExplorer

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

class vXmlExplorer(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='testprgroot'
    APPL_REF=[]#['vHum','vGlobals','vMsg']
    def __init__(self,*kw,**kwargs):
        if 'skip' in kwargs:
            skip=kwargs['skip']
            for s in ['config']:
                try:
                    idx=skip.index(s)
                except:
                    skip.append(s)
        try:
            vtXmlDomReg.__init__(self,*kw,**kwargs)
            self._dRegExt={}
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            
            oRoot=vXmlNodeExplorerRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeExplorerRoot()
            self.RegisterNode(oRoot,False)
            RegNodeExplorer(self,bRegAsRoot=True,oRoot=oRoot)
            
            #oSrc=self.GetReg('ExplorerSrc')
            #oLauncher=self.GetReg('ExplorerLauncher')
            #oWr=self.GetReg('ExplorerWrite')
            #oChk=self.GetReg('ExplorerCheck')
            #oStep=self.GetReg('ExplorerStep')
            #oBreak=self.GetReg('ExplorerBreakPoint')
            
            #self.LinkRegisteredNode(oRoot,oSrc)
            #self.LinkRegisteredNode(oRoot,oLauncher)
            #self.LinkRegisteredNode(oRoot,oWr)
            #self.LinkRegisteredNode(oRoot,oChk)
            #self.LinkRegisteredNode(oRoot,oStep)
            #self.LinkRegisteredNode(oRoot,oBreak)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'ExplorerRoot')
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'ExplorerRoot')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
    def Build(self):
        self._dRegExt={}
        self._dRegProto={}
        vtXmlDomReg.Build(self)
    def RegisterExtension(self,sExt,o):
        try:
            sTag=o.GetTagName()
            self.__logDebug__('sExt:%s;sTag:%s'%(sExt,sTag))
            if sExt in self._dRegExt:
                self.__logError__('sExt:%s;sTag:%s'%(sExt,sTag))
            else:
                self._dRegExt[sExt]=sTag[:]
        except:
            self.__logTB__()
    def GetRegByExtension(self,sExt):
        try:
            if sExt in self._dRegExt:
                return self.GetReg(self._dRegExt[sExt])
        except:
            self.__logTB__()
        return None
    def GetRegisteredExtension(self):
        try:
            l=[]
            for sTag in self._dRegExt.itervalues():
                o=self.GetReg(sTag)
                l.append((sTag,o.GetDescription(),o))
            return l
        except:
            self.__logTB__()
        return None
    def RegisterNetProtocol(self,sProto,o):
        try:
            sTag=o.GetTagName()
            self.__logDebug__('sProto:%s;sTag:%s'%(sProto,sTag))
            if sProto in self._dRegProto:
                self.__logError__('sProto:%s;sTag:%s'%(sProto,sTag))
            else:
                self._dRegProto[sProto]=sTag[:]
        except:
            self.__logTB__()
    def GetRegisteredNetProtocol(self):
        try:
            l=[]
            for sTag in self._dRegProto.itervalues():
                o=self.GetReg(sTag)
                l.append((sTag,o.GetDescription(),o))
            return l
        except:
            self.__logTB__()
        return None
