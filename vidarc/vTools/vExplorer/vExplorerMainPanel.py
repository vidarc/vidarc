#Boa:FramePanel:vExplorerMainPanel
#----------------------------------------------------------------------------
# Name:         vExplorerMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vExplorerMainPanel.py,v 1.41 2012/01/16 12:38:07 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import vidarc.tool.misc.vtmTreeListCtrl
import wx.lib.buttons
from wx.lib.splitter import MultiSplitterWindow
import time,sys,types,os,fnmatch
import subprocess
#if sys.platform.startswith('win'):
#    SYSTEM=1
#elif sys.platform.startswith('linux'):
#    SYSTEM=0


import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.vApps.common.vSystem as vSystem
    import vidarc.tool.misc.vtmThreadCtrl
    from vidarc.tool.gui.vtgDialog import vtgDialog
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgFrame import vtgFrame
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.gui.vtgPanelMixinWX import vtgPanelMixinWX
    from vidarc.tool.vtProcess import vtProcess
    from vidarc.tool.sec.vtSecAes import vtSecAes
    #from vp.vCore import vcsCrypto
    
    from vExplorerPanel import vExplorerPanel
    from vExplorerWinDrivePanel  import vExplorerWinDrivePanel
    from vExplorerActionPanel import vExplorerActionPanel
    from vExplorerRunPanel import vExplorerRunPanel
    from vExplorerCfgExtPanel import vExplorerCfgExtPanel
    from vidarc.vTools.vExplorer.vXmlExplorerNetSim import vXmlExplorerNetSim
    from vidarc.vTools.vExplorer.vXmlExplorerNetSim import vXmlExplorerNetSimWX
    from vidarc.vTools.vExplorer.vXmlExplorerNetSimTree import vXmlExplorerNetSimTree
    
    from vidarc.vTools.vExplorer.vExplorerCreateNewDlg import vExplorerCreateNewDlg
    from vidarc.vTools.vExplorer.vExplorerReNameDlg import vExplorerReNameDlg
    from vidarc.vTools.vExplorer.vExplorerSynchFrame import vExplorerSynchFrame
    from vidarc.vTools.vExplorer.vExplorerCVSFrame import vExplorerCVSFrame
    #from vidarc.vTools.vBrowse.vBrowseDirLogPanel import vBrowseDirLogPanel
    #from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    #from vidarc.vApps.vXXX.vNetXXX import *
    #from vidarc.vApps.vXXX.vXmlXXXTree import *
    
    #from vidarc.vApps.vHum.vNetHum import *
    #from vidarc.vApps.vGlobals.vNetGlobals import *
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    from vLoginDialog import vLoginDialog
    import vMESCenterTools
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    from vidarc.tool.misc.vtmPwd import vtmPwdDlg
    from vidarc.tool.misc.vtmUsrPwd import vtmUsrPwdDlg
    
    import vidarc.tool.InOut.vtInOutFileInfo as vtIOFI
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

def getAboutDescription():
    return _(u"""Explorer module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")

def create(parent):
    return vExplorerMainPanel(parent)

[wxID_VEXPLORERMAINPANEL, wxID_VEXPLORERMAINPANELCBADD, 
 wxID_VEXPLORERMAINPANELCHCSRC, wxID_VEXPLORERMAINPANELPNDATA, 
 wxID_VEXPLORERMAINPANELPNSRC, wxID_VEXPLORERMAINPANELSLWNAV, 
] = [wx.NewId() for _init_ctrls in range(6)]

def getPluginImage():
    return imgExplorer.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(imgExplorer.getPluginBitmap())
    return icon

class vExplorerMainPanel(wx.Panel,vtgPanelMixinWX):
    APPL_DFT=['appl_viewer_directory','appl_viewer_file',
        'appl_editor_directory','appl_editor_file',
        'appl_CVS',
        ]
    def _init_coll_fgsSrc_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsSrc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcSrc, 0, border=0, flag=0)
        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)

    def _init_coll_fgsSrc_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSrc, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsSrc = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsSrc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsSrc_Items(self.fgsSrc)
        self._init_coll_fgsSrc_Growables(self.fgsSrc)
        self._init_coll_bxsSrc_Items(self.bxsSrc)

        self.pnData.SetSizer(self.bxsData)
        self.pnSrc.SetSizer(self.fgsSrc)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VEXPLORERMAINPANEL,
              name=u'vExplorerMainPanel', parent=prnt, pos=wx.Point(115, 28),
              size=wx.Size(665, 200), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(657, 173))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VEXPLORERMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.SetConstraints(LayoutAnchors(self.slwNav, True, True, True,
              True))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VEXPLORERMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VEXPLORERMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(440, 160),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.pnSrc = wx.Panel(id=wxID_VEXPLORERMAINPANELPNSRC, name=u'pnSrc',
              parent=self.slwNav, pos=wx.Point(8, 8), size=wx.Size(176, 152),
              style=wx.TAB_TRAVERSAL)
        self.pnSrc.SetConstraints(LayoutAnchors(self.pnSrc, True, True, True,
              True))

        self.chcSrc = wx.Choice(choices=[], id=wxID_VEXPLORERMAINPANELCHCSRC,
              name=u'chcSrc', parent=self.pnSrc, pos=wx.Point(0, 0),
              size=wx.Size(130, 21), style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VEXPLORERMAINPANELCBADD, name=u'cbAdd', parent=self.pnSrc,
              pos=wx.Point(130, 0), size=wx.Size(23, 21), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VEXPLORERMAINPANELCBADD)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        self._init_ctrls(parent)
        self.SetName(name)
        vtgPanelMixinWX.__init__(self)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.xdCfg=vtXmlDom(appl='vExplorerCfg',audit_trail=False)
        
        #appls=['vExplorer','vHum','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        bNet=True
        self.netMaster=None
        #self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
        #            size=(rect.width-4, rect.height-4),verbose=0)
        #self.netExplorer=self.netMaster.AddNetControl(vNetExplorer,'vExplorer',synch=True,verbose=0)
        #self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        #self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        #self.netExplorer.SetLang(lang=vtLgBase.getPluginLang())
        #self.netMaster.AddNetControl(None,None,verbose=0)
        #self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        #self.netMaster.SetCfg(self.xdCfg,[],appls,'vExplorerMaster')
        self.netExplorer=vXmlExplorerNetSimWX(parent.sbStatus,appl='vExplorer',
                pos=(rect.x+2, rect.y+2),
                audit_trail=False,size=(rect.width-4, rect.height-4))
        
        self.trMaster=vXmlExplorerNetSimTree(self.pnSrc,wx.NewId(),
                pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,name="trexplorer",
                master=True,verbose=0)
        #EVT_VTXMLTREE_ITEM_SELECTED(self.trMaster,self.OnTreeItemSel)
        self.trMaster.BindEvent('selected',self.OnTreeItemSel)#,self.trMaster)
        self.trMaster.BindEvent('build',self.OnTreeItemBuild)
        self.trMaster.BindEvent('exec',self.OnTreeItemExec)
        #self.trMaster.GetMainWindow().Bind(wx.EVT_KEY_DOWN, self.OnTreeMasterKeyDown)
        #self.trMaster.GetMainWindow().Bind(wx.EVT_KEY_UP, self.OnTreeMasterKeyUp)
        self.trMaster.SetDoc(self.netExplorer,bNet)
        self.fgsSrc.AddWindow(self.trMaster, 0, border=0, flag=wx.EXPAND)
        #self.trMaster.UpdateRootInfoDict()
        # ++++++++++++++
        # add stuff here
        self.oCypInt=None#vtSecAes(IV='')
        self.oCypInt=vtSecAes(getAboutDescription().replace(' ','').replace('\n','')[:32],IV='')
        #sPhrase=getAboutDescription().replace(' ','').replace('\n','')[:32]
        #print sPhrase,type(str(sPhrase))
        #self.oCypInt=vcsCrypto(str(sPhrase),'')
        self._sReCallPhrase=None
        self.dlgUsrPwd=None
        self.dlgPwd=None
        self.dlgReCallAsk=None
        self.dlgReCallMng=None
        self.dlgRoot=None
        self.dlgLogin=None
        self.dlgAction=None
        self.dlgRun=None
        self.dlgKill=None
        self.dlgAppl=None
        self.dlgExtCfg=None
        self.dAppl={}
        self.dExt={}
        self.lExt=[]
        self.dlgApplCfg=None
        self.dlgCreateNew=None
        self.dlgReName=None
        self.dlgChooseCmd4Mime=None
        self.oCryptoStore=None
        self._dMntPending={}
        self._dUnMnt={}
        self._dReCall={}
        self._dStore={}
        self.iExpPrv=-1
        self.iExpSel=-1
        self.iExp=0
        self.lExp=[]
        self.dExp={}
        self.bBlockSel=False
        self.zBlockSel=-1
        self.splMain = MultiSplitterWindow(self.pnData, style=0)#wx.SP_LIVE_UPDATE)
        bxsExplorer = wx.BoxSizer(wx.HORIZONTAL)
        #sizer.Add(cp)
        bxsExplorer.Add(self.splMain, 1, wx.EXPAND)
        self.pnData.SetSizer(bxsExplorer)
        
        if 0:
            #self.splMain
            self.pnSrc = wx.Panel(id=wxID_VEXPLORERMAINPANELPNSRC, name=u'pnSrc',
                  parent=self.splMain, pos=wx.Point(8, 8), size=wx.Size(176, 152),
                  style=wx.TAB_TRAVERSAL)

            self.chcSrc = wx.Choice(choices=[], id=wxID_VEXPLORERMAINPANELCHCSRC,
                  name=u'chcSrc', parent=self.pnSrc, pos=wx.Point(0, 0),
                  size=wx.Size(130, 21), style=0)

            self.cbAdd = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Add),
                  id=wxID_VEXPLORERMAINPANELCBADD, name=u'cbAdd', parent=self.pnSrc,
                  pos=wx.Point(130, 0), size=wx.Size(23, 21), style=0)
            self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
                  id=wxID_VEXPLORERMAINPANELCBADD)
            bxs = wx.BoxSizer(wx.HORIZONTAL)
            bxs.AddWindow(self.chcSrc, 0, border=0, flag=0)
            bxs.AddWindow(self.cbAdd, 0, border=0, flag=0)
            fgsSrc = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
            fgsSrc.AddSizer(bxs, 0, border=0, flag=wx.EXPAND)
            fgsSrc.AddGrowableRow(1)
            fgsSrc.AddGrowableCol(0)
        
            self.trMaster=vXmlExplorerNetSimTree(self.pnSrc,wx.NewId(),
                    pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,name="trexplorer",
                    master=True,verbose=0)
            #EVT_VTXMLTREE_ITEM_SELECTED(self.trMaster,self.OnTreeItemSel)
            self.trMaster.BindEvent('selected',self.OnTreeItemSel)#,self.trMaster)
            self.trMaster.SetDoc(self.netExplorer,bNet)
            fgsSrc.AddWindow(self.trMaster, 0, border=0, flag=wx.EXPAND)
            #self.fgsSrc.AddWindow(self.trMaster, 0, border=0, flag=wx.EXPAND)
            self.splMain.AppendWindow(self.pnSrc, 140)
            self.pnSrc.SetSizer(fgsSrc)
            self.fgsSrc=fgsSrc
        
        self.AddExplorerView()
        
        self.oLocalRoot=self.netExplorer.GetRegisteredNode('ExplorerRoot')
        self.__logDebug__('root:%s'%(self.__logFmt__(self.oLocalRoot.GetRootLst())))
        #self.AddExplorerView()
        # add stuff here
        # --------------
        #self.thdFindQuick=vtXmlFindQuickThread(self.trMaster,bPost=True,
        #        lFindTagName=['tag','name','description'],origin='vExplorer:thdFindQuick')
        #EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        #self.idQuickFoundLast=-1
        
        if hasattr(parent ,'AddMenuItems'):
            parent.AddMenuItems('mnFile',0,[
                (u'Login\tCtrl+L',vtArt.getBitmap(vtArt.Safe),self.MenuLogin,(),{}),
                #(u'Unmount All\tCtrl+u',vtArt.getBitmap(vtArt.StaplesRemover),None,(),{},[
                    #(u'Unmount All\tCtrl+u',vtArt.getBitmap(vtArt.StaplesRemover),self.MenuUnMnt,(-1,),{}),
                    (u'Unmount \tCtrl+u',imgExplorer.getDirStopBitmap(),self.MenuUnMnt,(0,),{}),
                    (u'Unmount force\tCtrl+f',imgExplorer.getDirShutDownBitmap(),self.MenuUnMnt,(1,),{}),
                #    ]),
                None,
                (u'Open\tCtrl+O',vtArt.getBitmap(vtArt.Open),self.MenuOpenFile,(),{}),
                (u'Save\tCtrl+S',vtArt.getBitmap(vtArt.Save),self.MenuSaveFile,(1,),{}),
                (u'Save As\tCtrl+Shift+S',vtArt.getBitmap(vtArt.SaveAs),self.MenuSaveFile,(-1,),{}),
                (u'Save Config\tCtrl+Alt+S',vtArt.getBitmap(vtArt.Save),self.MenuSaveFile,(0,),{}),
                ])
            parent.AddMenuItems('mnAction',0,[
                (u'Parent Directory \tBack',vtArt.getBitmap(vtArt.Up),self.MenuAction,(-3,),{}),
                (u'Execute \tEnter',vtArt.getBitmap(vtArt.Build),self.MenuAction,(-2,),{}),
                (u'View \tF3',vtArt.getBitmap(vtArt.Glass),self.MenuAction,(3,),{}),
                (u'Edit \tF4',vtArt.getBitmap(vtArt.Edit),self.MenuAction,(4,),{}),
                None,
                (u'Copy to ... \tF5',imgExplorer.getCp1To2Bitmap(),self.MenuAction,(5,),{}),
                (u'Move to ... \tF6',imgExplorer.getMv1To2Bitmap(),self.MenuAction,(6,),{}),
                (u'Rename ... \tF2',imgExplorer.getMv1To2Bitmap(),self.MenuAction,(2,),{}),
                (u'Delete ... \tDel',vtArt.getBitmap(vtArt.Del),self.MenuAction,(0,),{}),
                (u'Insert ... \tIns',vtArt.getBitmap(vtArt.New),self.MenuAction,(30,),{}),
                None,
                (u'New File \tF7',vtArt.getBitmap(vtArt.New),self.MenuAction,(7,),{}),
                (u'New Directory\tF8',vtArt.getBitmap(vtArt.New),self.MenuAction,(8,),{}),
                
                ])
            parent.CallBack(parent.AddMenuItems,'mnView',0,[
                (u'Add View\tCtrl+A',vtArt.getBitmap(vtArt.Glass),self.MenuAddExp,(),{}),
                (u'Del View\tCtrl+D',vtArt.getBitmap(vtArt.Close),self.MenuDelExp,(),{}),
                (u'View Action\tCtrl+T',vtArt.getBitmap(vtArt.Build),self.MenuAction,(-1,),{}),
                (u'View Run\tCtrl+R',vtArt.getBitmap(vtArt.Missile),self.MenuAction,(-102,),{}),
                (u'View Properties',vtArt.getBitmap(vtArt.Properties),self.MenuViewProp,(),{}),
                None,
                (u'Prev View\tCtrl+Left',vtArt.getBitmap(vtArt.Left),self.MenuSelExp,(-1,),{}),
                (u'Next View\tCtrl+Right',vtArt.getBitmap(vtArt.Right),self.MenuSelExp,(1,),{}),
                #(u'Steps\tCtrl+T',imgEngSysS7Plc.getStepBitmap(),self.MenuViewStep,(),{}),
                #(u'Program\tCtrl+R',vtArt.getBitmap(vtArt.Build),self.MenuViewPrg,(),{}),
                None,])
            parent.CallBack(parent.AddMenuItems,'mnTools',0,[
                (u'Application',vtArt.getBitmap(vtArt.Settings),self.ConfigApplications,(),{}),
                (_(u'Extension'),vtArt.getBitmap(vtArt.Settings),self.ConfigExtensions,(),{}),
                (_(u'Recall'),vtArt.getBitmap(vtArt.Settings),self.ManageReCall,(),{}),
                None,])
            parent.CallBack(parent.AddMenuItems,'mnAnalyse',0,[
                (_(u'Synchronise'),vtArt.getBitmap(vtArt.Synch),self.MenuAction,(-200,),{}),
                (_(u'CVS'),vtArt.getBitmap(vtArt.Synch),self.MenuAction,(-300,),{}),
                None,
                ])
        if hasattr(parent,'AddToolBar'):
            parent.AddToolBar(0,[])
            self.PopulateToolBar(parent)
            parent.AddToolBar(12,[
                (u'read','bt',vtArt.getBitmap(vtArt.ImportFree),_(u'read data from PLC'),self.Read,(),{}),
                None,
                (u'showAll','bt',vtArt.getBitmap(vtArt.Refresh),_(u'update all'),self.ShowAll,(),{}),
                #(u'showSel','bt',vtArt.getBitmap(vtArt.Glass),_(u'update sel'),self.ShowSel,(),{}),
                #None,
                #(u'unmark','bt',vtArt.getBitmap(vtArt.Erase),_(u'unmark'),self.UnMark,(),{}),
                None,
                (u'dirUp','bt',vtArt.getBitmap(vtArt.Up),_(u'directory up'),self.DirUp,(),{}),
                (u'dirMeasure','bt',vtArt.getBitmap(vtArt.Measure),_(u'directory measure'),self.DirMeasure,(),{}),
                #(u'autoRefresh','tg',vtArt.getBitmap(vtArt.Synch),_(u'auto update'),self.AutoUpdate,(),{}),
                ])#,sName=sNameTB)
            parent.AddToolBar(22,[
                #None,
                #(u'searchDir','tg',vtArt.getBitmap(vtArt.Search),_(u'search direction'),self.SearchDir,(),{}),
                (u'search','bt',vtArt.getBitmap(vtArt.Find),_(u'search'),self.Search,(),{}),
                None,
                (u'makroRec','tg',imgExplorer.getRecordBitmap(),_(u'search direction'),self.MarkoRec,(),{}),
                ])#,sName=sNameTB)
        #self.Move(pos)
        #self.SetSize(size)
        self.SetLogin('','',None,None)
    def GetDN(self,iIdx):
        try:
            self.__chkActive__()
            w=self.lExp[iIdx]
            sDN=w.GetDN()
            self.__logDebug__('i:%d;sDN:%r'%(iIdx,sDN))
            return sDN 
        except:
            self.__logTB__()
            self.__logError__('i:%d'%(iIdx))
            return None
    def SetDN(self,iIdx,sDN,sSel=None):
        try:
            self.__logDebug__('i:%d;sDN:%s;sSel:%r'%(iIdx,sDN,sSel))
            self.__chkActive__()
            w=self.lExp[iIdx]
            w.SetDN(sDN,sSel=sSel)
        except:
            self.__logTB__()
            self.__logError__('i:%d'%(iIdx))
    def SetValueExplorer(self,lData,lHdr,dSortDef,bCheck4Reg,iIdx=-1,
                ti=None,oReg=None,dParDef=None):
        try:
            if VERBOSE>0:
                self.__logDebug__('i:%d;lData;%s;lHdr;%s;dSortDef;%s'%(iIdx,
                    self.__logFmt__(lData),
                    self.__logFmt__(lHdr),
                    self.__logFmt__(dSortDef)))
            else:
                self.__logDebug__('i:%d'%(iIdx))
            self.__chkActive__()
            w=self.lExp[iIdx]
            w.SetValue(lData,lHdr,dSortDef,oReg,dParDef)
            if ti is not None:
                self.trMaster.SetValue(lData,ti,bCheck4Reg=bCheck4Reg)
        except:
            self.__logTB__()
            self.__logError__('i:%d'%(iIdx))
    def SetComp(self,iIdx,l):
        self.__logCritical__(''%())
        self.__logError__(''%())
        self.__chkActive__()
        #    self.__logDebug__('i:%d;l:%s'%(iIdx,self.__logFmt__(l)))
        #    w=self.lExp[iIdx]
        #    w.SetValue(l,lHdr)
    def SetValueBySelected(self,iIdx):
        try:
            self.__logDebug__('iIdx:%d'%(iIdx))
            self.__chkActive__()
            ti=self.trMaster.GetSelectedTreeItem()
            try:
                if ti is not None:
                    td=self.trMaster.GetPyData(ti)
                else:
                    td=None
            except:
                self.__logTB__()
            self.SetValueByVal(iIdx,td,ti)
        except:
            self.__logTB__()
    def SetValueByVal(self,iIdx,val,ti=None):
        try:
            self.__logDebug__('iIdx:%d;val;%s'%(iIdx,self.__logFmt__(val)))
            self.__chkActive__()
            if type(val)==types.DictType:
                if '__reg' in val:
                    try:
                        oReg=self.netExplorer.GetReg(val['__reg'])
                        self.netExplorer.GetRegData(oReg,None,
                                    dImg=self.trMaster.GetImageDict(),
                                    func=self.SetValueExplorer,args=(),
                                    kwargs={'iIdx':iIdx,'ti':ti,'oReg':oReg,'dParDef':val},
                                    dPar=val)
                    except:
                        self.__logTB__()
                        td=None
                if '__funcData' in val:
                    try:
                        func,args,kwargs=td['__funcData']
                        lHdr=None
                        l=func(*args,**kwargs)
                    except:
                        self.__logTB__()
                        val=None
                else:
                    val=None
        except:
            self.__logTB__()
    def GetSelection(self,iIdx):
        try:
            w=self.lExp[iIdx]
            return w.GetSelection()
        except:
            self.__logTB__()
            return [],[]
    def GetSel(self,iIdx):
        try:
            self.__logDebug__('iIdx:%d'%(iIdx))
            sSrcDN=self.GetDN(iIdx)
            self.__logDebug__(['iIdx',iIdx,'sSrcDN',sSrcDN,type(sSrcDN)])
            if type(sSrcDN)==types.DictType:
                sSrcDN=None
                return None,[],[]
            lDN,lFN=self.GetSelection(iIdx)
            self.__logDebug__(['iIdx',iIdx,'sSrcDN',sSrcDN,
                        'lDN',lDN,'lFN',lFN])
            if (len(lDN)==1) and (len(lFN)==0):
                if type(lDN[0])==types.DictType:
                    return None,[],[]
            return sSrcDN,lDN,lFN
        except:
            self.__logTB__()
            return None,[],[]
    def GetSelDN(self,iIdx,bPosix=True):
        try:
            self.__logDebug__('iIdx:%d'%(iIdx))
            sSrcDN,lDN,lFN=self.GetSel(iIdx)
            if sSrcDN is None:
                return []
            if sSrcDN[-1]=='/':
                l=[u''.join([sSrcDN,sDN]) for sDN in lDN]
            else:
                l=[u'/'.join([sSrcDN,sDN]) for sDN in lDN]
            if bPosix==False:
                ll=[vtIOFI.posix2fn(s) for s in l]
                return ll
            return l
        except:
            self.__logTB__()
            return []
    def GetSelFN(self,iIdx,bPosix=True):
        try:
            self.__logDebug__('iIdx:%d'%(iIdx))
            sSrcDN,lDN,lFN=self.GetSel(iIdx)
            if sSrcDN is None:
                return []
            if sSrcDN[-1]=='/':
                l=[u''.join([sSrcDN,sFN]) for sFN in lFN]
            else:
                l=[u'/'.join([sSrcDN,sFN]) for sFN in lFN]
            if bPosix==False:
                ll=[vtIOFI.posix2fn(s) for s in l]
                return ll
            return l
        except:
            self.__logTB__()
            return []
    def ChooseCmd4Mime(self,fileType,filename,mime):
        try:
            if self.dlgChooseCmd4Mime is None:
                lWid=[
                    ('lblRg', (0,0),(1,2),{'name':'lblCmd','label':_(u'command')},None),
                    ('lst',   (1,1),(1,2),{'name':'lstCmd',},None),
                    #('cbBmp', (1,3),(1,1),{'name':'cbChoose','bitmap':vtArt.getBitmap(vtArt.Browse),},
                    #    {'btn':self.OnConfigApplicationsChoose}),
                
                ]
                self.dlgChooseCmd4Mime=vtgDlg(self,vtgPanel,name='dlgChooseCmd4Mime',
                    title=_(u'vExplorer Choose Command'),
                    bmp=imgExplorer.getPluginBitmap(),sz=(300,400),
                    pnName='pnCmd',
                    lGrowCol=[(1,1)],lGrowRow=[(1,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            tAllCmd=fileType.GetAllCommands(filename, mime or '')
            lCmd=map(None,tAllCmd[0],tAllCmd[1])
            self.__logDebug__(['lCmd',lCmd])
            bFound=False
            if len(lCmd)==1:
                return lCmd[0][1]
            lCmd.sort()
            iOfs=0
            pn=self.dlgChooseCmd4Mime.GetPanel()
            pn.lstCmd.Clear()
            for sKey,sCmd in lCmd:
                pn.lstCmd.Append(sKey,-1)
            iRet=self.dlgChooseCmd4Mime.ShowModal()
            if iRet<=0:
                return 0
            iIdx=pn.lstCmd.GetSelection()
            if iIdx>=0:
                return lCmd[iIdx][1]
        except:
            self.__logTB__()
        return None
    def __openFile__(self,sDN,sFN):
        try:
            sExts=sFN.split('.')
            sExt=sExts[-1]
            if sDN is not None:
                filename='/'.join([sDN,sFN])
            else:
                filename=sFN
            filename=vtIOFI.posix2fn(filename)
            o=self.netExplorer.GetRegByExtension(sExt)
            if o is not None:
                self.__logDebug__('object for extension:%s found'%(sExt))
                self.__execByReg__(o,None,None,None,None,sFN=filename)
                return
            fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
            try:
                bShell=False
                #bDetached=False
                bDetached=True
                if fileType is None:
                    self.__logError__('fn:%s;mime type not found'%(filename))
                    cmd=filename
                    bShell=True
                    bDetached=True
                else:
                    mime = fileType.GetMimeType()
                    cmd = fileType.GetOpenCommand(filename, mime or '')
                    if cmd is None:
                        cmd=self.ChooseCmd4Mime(fileType,filename,mime)
                        if cmd==0:
                            return
                    if cmd is None:
                        sMsg=_(u"Command not found for\n\n%s!")%(filename)
                        dlg=vtmMsgDialog(self,sMsg ,
                                            u'vExplorer',
                                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                        dlg.ShowModal()
                        dlg.Destroy()
                        return
                    if cmd[-1]=='*':
                        cmd=filename
                        bShell=True
                        bDetached=True
                try:
                    self.__logDebug__('cmd:%s'%(cmd))
                except:
                    pass
                if cmd.startswith('WX_DDE#'):
                    wx.Execute(cmd)
                    self.__logPrintMsg__(_(u'execute %s by DDE done.')%sFN)
                    return
                frmMain=frmMain=self.GetParent()
                self.__logPrintMsg__(_(u'execute %s ...')%sFN)
                p=vtProcess(cmd,shell=bShell,detached=bDetached)
                if bDetached==True:
                    pass
                else:
                    frmMain.Do(self.doExec,p,_(u'execute %s done.')%sFN)
            except:
                self.__logTB__()
                sMsg=_(u"Cann't open %s!")%(filename)
                dlg=vtmMsgDialog(self,sMsg ,
                                    u'vExplorer',
                                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION)
                dlg.ShowModal()
                dlg.Destroy()
        except:
            self.__logTB__()
    def __getValueParent__(self,dVal):
        if '__reg' in dVal:
            sReg=dVal['__reg']
            if sReg=='ExplorerNetProtocolSmb':
                d={
                    '__reg':'ExplorerNetHost',
                    'host':dVal.get('info',{}).get('host',None)
                    }
                return d
            elif sReg=='ExplorerNetHost':
                d={
                    '__reg':'ExplorerNetWork',
                    }
                return d
        return {'__reg':'ExplorerRoot'}
    def MenuAction(self,iAction):
        try:
            self.__logDebug__('iAction:%d;sel:%d;prv:%d'%(iAction,
                    self.iExpSel,self.iExpPrv))
            self.__chkActive__()
            if iAction in [-102,-101,-100]:
                if self.dlgRun is None:
                    self.dlgRun=vtgFrame(self,
                            title=_(u'vExplorer Run'),
                            applName=_(u'vExplorer Run'),
                            pnCls=vExplorerRunPanel,
                            sz=(200,200),
                            iArrange=10,
                            name='vExplorerRun',
                            bmp=imgExplorer.getPluginBitmap())
                if self.dlgRun is None:
                    self.__logError__(''%())
                if iAction==-102:
                    self.dlgRun.Show()
                
            if iAction in [-100,-1,0,5,6]:
                if self.dlgAction is None:
                    self.dlgAction=vtgFrame(self,
                            title=_(u'vExplorer Action'),
                            pnCls=vExplorerActionPanel,
                            sz=(200,200),
                            iArrange=10,
                            name='vExplorerAction',
                            bmp=imgExplorer.getPluginBitmap())
                if self.dlgAction is None:
                    self.__logError__(''%())
                    return
                if self.dlgAction.GetAutoStart()==False:
                    self.dlgAction.SetFocus()
                if iAction==-1:
                    self.dlgAction.Show()
            if iAction==-200:
                if self.iExpPrv==-1:
                    sSrc2DN=''
                else:
                    sSrc2DN=self.GetDN(self.iExpPrv)
                if self.iExpSel==-1:
                    sSrc1DN=''
                else:
                    sSrc1DN=self.GetDN(self.iExpSel)
                frm=vExplorerSynchFrame(self,funcOpen=self.__openFile__)
                frm.SetValue(sSrc1DN,sSrc2DN,
                    10,True,False,False,True,False,True,False,False)
                frm.Show()
                return
            if iAction==-300:
                if self.iExpSel==-1:
                    sSrcDN=''
                else:
                    sSrcDN=self.GetDN(self.iExpSel)
                sAppl=self.oLocalRoot.GetApplExec('appl_CVS')
                if sAppl is None:
                    sAppl=self.ConfigAppl('appl_CVS')
                if sAppl is None:
                    vtLog.PrintMsg(_(u'application not resolved %s.')%(s),wid=self)
                    return
                oReg=self.netExplorer.GetReg('ExplorerLaunchCVS')
                frm=vExplorerCVSFrame(self)
                frm.SetValue(oReg,sAppl,sSrcDN)
                frm.Show()
                return
            if iAction<-100:
                return
            if iAction in [5,6]:
                if self.iExpPrv==-1:
                    sDstDN=None
                else:
                    sDstDN=self.GetDN(self.iExpPrv)
                if self.iExpSel==-1:
                    sSrcDN=None
                else:
                    sSrcDN=self.GetDN(self.iExpSel)
                    lDN,lFN=self.GetSelection(self.iExpSel)
            if iAction==-3:
                sSrcDN=self.GetDN(self.iExpSel)
                if type(sSrcDN)==types.DictType:
                    self.SetValueByVal(self.iExpSel,
                            self.__getValueParent__(sSrcDN))
                else:
                    sSrcDN=sSrcDN[:-1]
                    lDN=sSrcDN.split('/')
                    sNewDN='/'.join(lDN[:-1])
                    self.__logDebug__('sDN:%s;lDN:%s'%(sSrcDN,self.__logFmt__(lDN)))
                    if len(sNewDN)>0:
                        self.SetDN(self.iExpSel,sNewDN,sSel=None)
                    else:
                        #self.SetValueBySelected(self.iExpSel)
                        self.SetValueByVal(self.iExpSel,{'__reg':'ExplorerRoot'})
            elif iAction==-2:
                sSrcDN=self.GetDN(self.iExpSel)
                lDN,lFN=self.GetSelection(self.iExpSel)
                if (len(lDN)==1) and (len(lFN)==0):
                    if type(sSrcDN)==types.DictType:
                        sSrcDN=None
                    if type(lDN[0])==types.DictType:
                        self.SetValueByVal(self.iExpSel,lDN[0])
                    elif sSrcDN is not None:
                        self.SetDN(self.iExpSel,sSrcDN+lDN[0])
                    else:
                        self.SetDN(self.iExpSel,lDN[0])
                else:
                    try:
                        sCurDN=os.getcwd()
                        os.chdir(sSrcDN)
                        for iOfs in xrange(0,min(10,len(lFN))):
                            self.__openFile__(sSrcDN,lFN[iOfs])
                    except:
                        self.__logTB__()
                    os.chdir(sCurDN)
            elif iAction==0:
                sSrcDN=self.GetDN(self.iExpSel)
                lDN,lFN=self.GetSelection(self.iExpSel)
                self.dlgAction.AddAction(iAction,sSrcDN,None,lDN,lFN)
            elif iAction==1:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==2:
                sBase=self.GetDN(self.iExpSel)
                lDN,lFN=self.GetSelection(self.iExpSel)
                if self.__isLogDebug__():
                    self.__logDebug__('sBase:%r;lDN;%s;lFN;%s'%(sBase,
                            self.__logFmt__(lDN),self.__logFmt__(lFN)))
                if self.dlgReName is None:
                    self.dlgReName=vExplorerReNameDlg(self)
                iRet=self.dlgReName.DoModal(sBase,lDN,lFN)
                #self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                #self.__logError__('action %d not implemented'%(iAction))
            elif iAction==3:
                sBase=self.GetDN(self.iExpSel)
                lDN,lFN=self.GetSelection(self.iExpSel)
                if len(lDN)>0:
                    sAppl=self.ConfigAppl('appl_viewer_directory')
                    if sAppl is not None:
                        for sDN in lDN[:10]:
                            self.ProcExec(_(u'view directory %s')%sDN,
                                    ''.join(['"',sAppl,'" "',sBase+sFN,'"']))
                if len(lFN)>0:
                    iOfs=0
                    for sFN in lFN[:10]:
                        sCmd=self.GetExec(sFN,sBase)
                        self.ProcExec(_(u'view file %s')%sFN,sCmd)
                    if 0:
                        sAppl=self.ConfigAppl('appl_viewer_file')
                        if sAppl is not None:
                            for sFN in lFN[:10]:
                                self.ProcExec(_(u'view file %s')%sFN,
                                        ''.join(['"',sAppl,'" "',sBase+sFN,'"']))
            elif iAction==4:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==5:
                self.__logDebug__('cp;src:%s;dst:%s'%(sSrcDN,sDstDN))
                if VERBOSE>0:
                    self.__logDebug__('lDN;%s;lFN:%s'%(self.__logFmt__(lDN),
                            self.__logFmt__(lFN)))
                self.dlgAction.AddAction(iAction,sSrcDN,sDstDN,lDN,lFN)
            elif iAction==6:
                self.__logDebug__('mv;src:%s;dst:%s'%(sSrcDN,sDstDN))
                if VERBOSE>0:
                    self.__logDebug__('lDN;%s;lFN:%s'%(self.__logFmt__(lDN),
                            self.__logFmt__(lFN)))
                self.dlgAction.AddAction(iAction,sSrcDN,sDstDN,lDN,lFN)
            elif iAction==7:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==8:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==9:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==10:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==11:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==13:
                self.__logPrintMsg__(_(u'action %d not implemented yet.')%(iAction))
                self.__logError__('action %d not implemented'%(iAction))
            elif iAction==30:
                sBase=self.GetDN(self.iExpSel)
                self.CreateNew(sBase,bFile=True)
        except:
            self.__logTB__()
    def ProcExecSuccess(self,sMsg,sCmd):
        try:
            self.__logPrintMsg__(sMsg+_(u' ...'))
            self.__logDebug__('sMsg:%s;sCmd:%s'%(sMsg,sCmd))
        except:
            self.__logTB__()
    def ProcExecFailure(self,sMsg,sCmd):
        try:
            self.__logPrintMsg__(sMsg+_(u' failure!'))
            self.__logError__('sMsg:%s;sCmd:%s'%(sMsg,sCmd))
        except:
            self.__logTB__()
    def ProcExec(self,sMsg,sCmd):
        try:
            frmMain=self.GetParent()
            self.__logPrintMsg__(sMsg+_(u' ...'))
            p=vtProcess(sCmd.replace('/','\\'),shell=False)
            frmMain.DoBranch(\
                        (self.doExec,(p,sMsg+_(u' done.')),{}),
                        (self.ProcExecSuccess,(sMsg,sCmd,),{}),
                        (self.ProcExecFailure,(sMsg,sCmd),{}))
        except:
            self.__logTB__()
    def GetExec(self,sFN,sDN=None):
        try:
            if sDN is None:
                sFullFN=sFN
            else:
                sFullFN=sDN+sFN
            iExt=sFN.rfind('.')
            sExt=sFN[iExt+1:]
            if sExt in self.dExt:
                dExt=self.dExt[sExt]
            elif '' in self.dExt:
                dExt=self.dExt['']
            else:
                dExt=None
            if dExt is not None:
                #
                sAppl=dExt.get('app',None)
                sArg=dExt.get('arg','')
                
                if sAppl is not None:
                    return ''.join(['"',sAppl,'" ',sArg,'"',sFullFN,'"'])
                
            sAppl=self.ConfigAppl('appl_viewer_file')
            if sAppl is not None:
                return ''.join(['"',sAppl,'" "',sFullFN,'"'])
        except:
            self.__logTB__()
        return None
    def IsLoggedIn(self):
        return self._sReCallPhrase is not None
    def MenuLogin(self,iDly=0,bClr=False):
        try:
            self.__logDebug__(''%())
            if bClr==True:
                self._dReCall=None
                self._sReCallPhrase=None
            sReCallPhrase=self._sReCallPhrase
            if self.__setupReCall__()<1:
                return
            self._dReCall={}
            s=self.GetCfgVal(['recall'])
            if VERBOSE>5:
                self.__logDebug__(s)
            l=[k.strip() for k in s.split('\n')]
            s=''.join(l)
            if VERBOSE>5:
                self.__logDebug__(s)
            if len(s)==0:
                return
            if self._sReCallPhrase is not None:
                if len(s)==0:
                    bFlt=False
                    dReCall={}
                else:
                    sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                    o=vtSecAes(sPhrase)
                    #o=vcsCrypto(sPhrase)
                    sDec=o.DecryptHex(s)
                    l=sDec.split('\x00')
                    bFlt=False
                    dReCall={}
                    iL=len(l)
                    if self.__isLogDebug__():
                        self.__logDebug__('iL:%d;s;%s;%s;l;%s'%(iL,s,sDec,self.__logFmt__(l)))
                    if iL==0:
                        self.__logError__(''%())
                        bFlt=True
                    #elif iL==1:
                    #    bFlt=False
                    else:
                        for sM in l:
                            lM=sM.split('\x01')
                            if len(lM)==2:
                                kM=lM[0]
                                sN=lM[1]
                                lN=sN.split('\x03')
                                d={}
                                for sD in lN:
                                    lD=sD.split('\x02')
                                    if len(lD)==2:
                                        d[lD[0]]=lD[1]
                                    else:
                                        bFlt=True
                                        break
                                d['_store']=True
                                dReCall[kM]=d
                            else:
                                bFlt=True
                                break
                if bFlt==True:
                    self.__logError__('syntax error;possible wrong password'%())
                    dlg=vtmMsgDialog(self,_(u'Login failed.\nTry again?') ,
                                            _(u'vExplorer Login'),
                                            bmp=vtArt.getBitmap(vtArt.Error),
                                            style=wx.YES_NO|wx.NO_DEFAULT)
                    ret=dlg.ShowModal()
                    dlg.Destroy()
                    self.__logDebug__('ret:%r,%r'%(ret,wx.ID_YES))
                    if ret==wx.ID_YES:
                        self._sReCallPhrase=sReCallPhrase
                        self.__logDebug__('old phrase;%r'%(self._sReCallPhrase))
                        self.MenuLogin(iDly+1)
                    else:
                        self._sReCallPhrase=None
                        self._dReCall=None
                        self.SetCfgVal('',['recall'])
                        #self.MenuLogin(iDly+1,bClr=True)
                        if self.__setupReCall__()>=0:
                            self._dReCall={}
                else:
                    self._dReCall=dReCall
                    self.__logDebug__('dReCall;%s'%(self.__logFmt__(self._dReCall)))
        except:
            self.__logTB__()
    def SetLogin(self,sLogin,sPasswd,oCrypto,oCryptoStore):
        try:
            self.__logInfo__('sLogin:%s'%(sLogin))
            #self.cfg.readFN(self.CFG_PLUGIN_FN)
            #self.__setCfg__()
            self.sLogin=sLogin
            self.sPasswd=sPasswd
            #if len(self.sPasswd)==0:
            #    self.lbbLogin.SetBitmap(images.getSecUnlockedBitmap())
            #else:
            #    self.lbbLogin.SetBitmap(images.getSecLockedBitmap())
            
            self.oCrypto=oCrypto
            self.oCryptoStore=oCryptoStore
        except:
            self.__logTB__()
    def MenuOpenFile(self):
        try:
            self.__logInfo__(''%())
            if 'explorer_fn' in self.dCfg:
                sFN=self.dCfg['explorer_fn']
            else:
                sFN='vExplorer.xml'
            par=self.GetParent()
            sFN=par.ChooseFile(sFN)
            if sFN is not None:
                self.OpenFile(sFN)
                self.dCfg['explorer_fn']=sFN
        except:
            self.__logTB__()
    def MenuSaveFile(self,iMode=0):
        try:
            self.__logInfo__('iMode:%d'%(iMode))
            par=self.GetParent()
            bChoose=False
            sKey=None
            sFN=self.netExplorer.GetFN()
            if iMode==1:
                sKey='explorer_fn'
                if sFN is None:
                    bChoose=True
                    sFN='vExplorer.xml'
            elif iMode==-1:
                sKey='explorer_fn'
                bChoose=True
                if sFN is None:
                    sFN='vExplorer.xml'
            elif iMode==0:
                par.SaveCfgFile(None)
                return
            if bChoose==True:
                sFN=par.ChooseFile(sFN,bSave=True)
            if sFN is not None:
                self.SetCfgVal(sFN,[sKey])
                iRet=self.netExplorer.Save(sFN)
                if iRet<0:
                    if iMode==1:
                        sMsg=_(u"Save %s failed!\n\nreturn code:%d")%(sFN,iRet)
                        self.__logPrintMsg__(sMsg)
                        dlg=vtmMsgDialog(self,sMsg ,
                                    u'vExplorer',
                                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION)
                        dlg.ShowModal()
                        dlg.Destroy()
                        self.MenuSaveFile(iMode=-1)
        except:
            self.__logTB__()
    def MenuAddExp(self):
        try:
            self.__logInfo__(''%())
            self.AddExplorerView()
        except:
            self.__logTB__()
    def MenuDelExp(self):
        try:
            self.__logInfo__('iExpSel:%d;iL:%d'%(self.iExpSel,len(self.lExp)))
            self.__chkActive__()
            if len(self.lExp)>1:
                self.iExpPrv=-1
                if self.iExpSel<0:
                    self.iExpSel=len(self.lExp)-1
                w=self.lExp[self.iExpSel]
                w.SetActive(False)
                w.SetDN(None)
                self.splMain.DetachWindow(w)
                
                del self.lExp[self.iExpSel]
                w.Destroy()
                iIdx=0
                for w in self.lExp:
                    w.SetExplorerIdx(iIdx)
                    iIdx+=1
                self.iExp=iIdx
                self.iExpSel-=1
                if self.iExpSel<0:
                    self.iExpSel=0
                w=self.lExp[self.iExpSel]
                w.SetActive(True)
                self.__logDebug__('iExpSel:%d;iL:%d'%(self.iExpSel,len(self.lExp)))
                self.__chkActive__()
        except:
            self.__logTB__()
    def MenuSelExp(self,iD):
        try:
            self.__chkActive__()
            iL=len(self.lExp)
            if iL>1:
                w=self.lExp[self.iExpSel]
                if iD<0:
                    self.iExpPrv=self.iExpSel
                    self.iExpSel-=1
                    if self.iExpSel<0:
                        self.iExpSel=iL-1
                    w.SetActive(False)
                    w.SetActive(2)
                    w=self.lExp[self.iExpSel]
                    w.SetActive(True)
                    w.SetActive(1)
                    w.SetFocus()
                elif iD>0:
                    self.iExpPrv=self.iExpSel
                    self.iExpSel+=1
                    if self.iExpSel>=iL:
                        self.iExpSel=0
                    w.SetActive(False)
                    w=self.lExp[self.iExpSel]
                    w.SetActive(True)
                    w.SetFocus()
            self.__chkActive__()
        except:
            self.__logTB__()
    def MenuViewProp(self):
        try:
            self.__logDebug__(''%())
            w=self.lExp[self.iExpSel]
            w.ShowProperties()
        except:
            self.__logTB__()
    def Read(self):
        pass
    def ShowAll(self):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            self.__logDebug__(''%())
            self.__chkActive__()
            iIdx=-1
            for w in self.lExp:
                iIdx+=1
                sDN=w.GetDN()
                if bDbg==True:
                    self.__logDebug__('sDN;%s'%(self.__logFmt__(sDN)))
                bRst=False
                if sDN is None:
                    self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
                elif type(sDN)==types.DictType:
                    self.SetValueByVal(iIdx,sDN)#{'__reg':sDN['__reg']})
                else:
                    w.SetDN(w.GetDN())
            self.trMaster.SetNode(None)
        except:
            self.__logTB__()
    def DirUp(self):
        try:
            self.__chkActive__()
            if self.iExpSel<0:
                self.__logDebug__(''%())
                return
            w=self.lExp[self.iExpSel]
            sDN=w.GetDN()
            if sDN is None:
                return
            lDN=sDN[:-1].split('/')
            self.__logDebug__('dn:%s;lDN:%s'%(sDN,self.__logFmt__(lDN)))
            
            if len(lDN)>1:
                sCurDN='/'.join(lDN[:-1])
                ti,dRoot,sSelDN=self.trMaster.GetSelectedDN()
                if cmp(sCurDN,sSelDN)==0:
                    self.OnTreeItemSel(None)
                else:
                    self.trMaster.SelectDN(sCurDN)
            else:
                #self.SetValueBySelected(self.iExpSel)
                self.SetValueByVal(self.iExpSel,{'__reg':'ExplorerRoot'})
        except:
            self.__logTB__()
    def DirMeasure(self):
        try:
            self.__logDebug__(''%())
            self.__chkActive__()
            if self.iExpSel>=0:
                w=self.lExp[self.iExpSel]
                w.Measure()
        except:
            self.__logTB__()
    def SearchDir(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def Search(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def MarkoRec(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def AddExplorerView(self,iW=-1,sDN=None):
        try:
            id=wx.NewId()
            iIdx=len(self.lExp)
            iExpSel=self.iExpSel
            self.iExpPrv=iExpSel
            if self.iExpSel>=0:
                self.lExp[self.iExpSel].SetActive(False)
            w=vExplorerPanel(self.splMain,id=id,pos=wx.DefaultPosition,
                    size=wx.DefaultSize,style=wx.TAB_TRAVERSAL,name='pnExp%02d'%(self.iExp))
            
            imgDict=self.trMaster.imgDict
            imgLstTyp=self.trMaster.imgLstTyp
            fIdxDelta=self.trMaster.fIdxDelta
            w.SetImageListDict(imgLstTyp,imgDict,fIdxDelta)
            w.BindEvent('cmd',self.OnExplorerCmd)
            w.SetExplorerIdx(iIdx)
            if iW<0:
                iW=1
            iW=1
            self.splMain.AppendWindow(w,1)# iW)
            self.lExp.append(w)
            self.iExp+=1
            self.iExpSel=iIdx
            if sDN is None:
                self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
            else:
                self.netExplorer.CallBack(w.SetDN,sDN)
            self.__chkActive__()
        except:
            self.__logTB__()
    def GetDocMain(self):
        return self.netExplorer
    def GetNetMaster(self):
        return None#self.netExplorer
        #return self.netMaster
    def GetTreeMain(self):
        return self.trMaster
    def PopulateToolBar(self,parent):
        #id=wx.NewId()
        #sz=self.GetClientSize()
        #self.tbMain = wx.ToolBar(id=id,
        #        name=u'tbContact', parent=parent, pos=wx.Point(0,0),
        #        size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain=parent.GetToolBar()
        #self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.trMaster.PopulateToolBar(parent,self.tbMain,self.dToolBar,iStyle=0xffff)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            self.__logInfo__('id:%08d'%(evt.GetID()))
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            self.__logTB__()
            self.__logError__('id:%08d'%(evt.GetID()))
    def OnExplorerCmd(self,evt):
        evt.Skip()
        try:
            sCmd=evt.GetCmd()
            sDat=evt.GetData()
            o=evt.GetEventObject()
            iIdx=o.GetExplorerIdx()
            self.__logDebug__('idx:%d;cmd:%s;dat:%s'%(iIdx,repr(sCmd),
                        repr(sDat)))
            self.__chkActive__()
            if self.iExpSel!=iIdx:
                self.lExp[self.iExpSel].SetActive(False)
                self.iExpPrv=self.iExpSel
                self.lExp[iIdx].SetActive(True)
                self.iExpSel=iIdx
            if sCmd=='chdir':
                if sDat is None:
                    self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
                    #lComp=self.trMaster.GetRootInfoLst()
                    #self.__logDebug__('lComp:%s'%(self.__logFmt__(lComp)))
                    #self.SetComp(iIdx,lComp)
                    return
                else:
                    self.trMaster.SelectDN(sDat)
            elif sCmd=='open':
                try:
                    sCurDN=os.getcwd()
                    sDN,sFN=os.path.split(sDat[:sDat.find(' ')])
                    os.chdir(sDN)
                    self.__openFile__(None,sDat)
                except:
                    self.__logTB__()
                os.chdir(sCurDN)
            self.__chkActive__()
        except:
            self.__logTB__()
    def __chkActive__(self):
        try:
            lAct=[w.IsActive() for w in self.lExp]
            if VERBOSE>0:
                self.__logDebug__('sel:%d;prv:%d;lAct;%s'%(self.iExpSel,
                        self.iExpPrv,self.__logFmt__(lAct)))
            iVal=reduce(lambda x,y:x+y,lAct)
            if iVal!=1:
                self.__logCritical__('sel:%d;lAct;%s'%(self.iExpSel,self.__logFmt__(lAct)))
            iAct=-1
            iPrv=-1
            if self.iExpPrv==-1:
                self.iExpPrv=self.iExpSel+1
                if self.iExpPrv>=len(self.lExp):
                    self.iExpPrv=0
                    if self.iExpPrv==self.iExpSel:
                        self.iExpPrv=-1
            i=0
            for w in self.lExp:
                if self.iExpSel==i:
                    if VERBOSE>0:
                        self.__logDebug__('i:%d;src0'%(i))
                    w.SetSrc(1)
                elif self.iExpPrv==i:
                    if VERBOSE>0:
                        self.__logDebug__('i:%d;src1'%(i))
                    w.SetSrc(2)
                else:
                    if VERBOSE>0:
                        self.__logDebug__('i:%d;none'%(i))
                    w.SetSrc(0)
                    w.SetActive(False)
                i+=1
        except:
            self.__logTB__()
    def OnCmdExecProcCmd(self,evt):
        evt.Skip()
        try:
            sCmd=evt.GetCmd()
            dDat=evt.GetData()
            o=evt.GetEventObject()
            self.__logDebug__('cmd:%s;dDat:%s'%(repr(sCmd),
                        self.__logFmt__(dDat)))
            if sCmd=='state':
                if 'msg' in dDat:
                    self.__logPrintMsg__(dDat['msg'])
        except:
            self.__logTB__()
    def CheckMntSuccess(self):
        try:
            self.__logDebug__('dMntPending;%s'%(self.__logFmt__(self._dMntPending)))
            self.trMaster.UpdateRootInfoDict()
            lRoot=self.oLocalRoot.GetRootLstFree()
            sMntFirst=None
            for sMnt,tVal in self._dMntPending.iteritems():
                if sMnt in lRoot:
                    sK=tVal[0]
                    iId=tVal[1]
                    sMsg=_(u"Execute %s to %s failed!")%(sK,sMnt)
                    self.__logPrintMsg__(sMsg)
                    dlg=vtmMsgDialog(self,sMsg ,
                                    u'vExplorer',
                                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
                else:
                    self._dUnMnt[sMnt]=tVal
                    if sMntFirst is None:
                        sMntFirst=sMnt
                    if None in self._dReCall:
                        t=self._dReCall[None]
                        sK=t[0]
                        dReCall=t[1]
                        if dReCall is not None:
                            if len(t)>1:
                                sKeyReCall=t[2]
                                if sKeyReCall is not None:
                                    self._dReCall[sKeyReCall]=dReCall
                            self._dReCall[sK]=dReCall
                        self.__logDebug__('sK:%s;_dReCall;%s;_dStore;%s'%(sK,
                                self.__logFmt__(self._dReCall),
                                self.__logFmt__(self._dStore)))
                        del self._dReCall[None]
                        self.__logDebug__('sK:%s;_dReCall;%s;_dStore;%s'%(sK,
                                self.__logFmt__(self._dReCall),
                                self.__logFmt__(self._dStore)))
            lComp=None
            iIdx=-1
            self.__chkActive__()
            for w in self.lExp:
                iIdx+=1
                if self.iExpSel==iIdx:
                    if sMntFirst is not None:
                        w.SetDN(sMntFirst.replace('\\','/'))
                        sMntFirst=None
                        continue
                if w.GetStyle()=='ExplorerRoot':
                    self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
            if sMntFirst is not None:
                self.__logError__('you are not supposed to be here;'\
                        'mnt:%s;sel:%d'%(sMntFirst,self.iExpSel))
        except:
            self.__logTB__()
        self._dMntPending={}
    def CheckUnMntSuccess(self):
        try:
            self.__logDebug__('dUnMnt;%s'%(self.__logFmt__(self._dUnMnt)))
            self.trMaster.UpdateRootInfoDict()
            lRoot=self.oLocalRoot.GetRootLstFree()
            l=[]
            for sMnt in lRoot:
                if sMnt in self._dUnMnt:
                    l.append(sMnt)
            for sMnt in l:
                del self._dUnMnt[sMnt]
            iIdx=0
            self.__chkActive__()
            for w in self.lExp:
                if w.GetStyle()=='ExplorerRoot':
                    self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
                iIdx+=1
        except:
            self.__logTB__()
    def OnTreeMasterKeyUp(self,evt):
        evt.Skip()
        try:
            if VERBOSE>5:
                self.__logDebug__('bBlockSel:%d;zBlockSel:%f'%(self.bBlockSel,self.zBlockSel))
            self.zBlockSel=time.clock()
            self.bBlockSel=False
            parent=self.GetParent()
            parent.CallBackDelayed(1.0,self.OnTreeItemSel,None)
            if VERBOSE>5:
                self.__logDebug__('bBlockSel:%d;zBlockSel:%f'%(self.bBlockSel,self.zBlockSel))
        except:
            self.__logTB__()
    def OnTreeMasterKeyDown(self,evt):
        evt.Skip()
        try:
            if VERBOSE>5:
                self.__logDebug__('bBlockSel:%d;zBlockSel:%f'%(self.bBlockSel,self.zBlockSel))
            self.bBlockSel=True
            self.zBlockSel=0
            if VERBOSE>5:
                self.__logDebug__('bBlockSel:%d;zBlockSel:%f'%(self.bBlockSel,self.zBlockSel))
        except:
            self.__logTB__()
    def OnTreeItemSel(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            if VERBOSE>5:
                self.__logDebug__('bBlockSel:%d;zBlockSel:%f'%(self.bBlockSel,self.zBlockSel))
            if self.bBlockSel==True:
                return
            if self.zBlockSel>0:
                if (self.zBlockSel+0.9)<time.clock():
                    self.zBlockSel=-1
                else:
                    return
            #node=evt.GetTreeNodeData()
            ti,dRoot,sDN=self.trMaster.GetSelectedDN()
            if self.trMaster.IsItemDN(ti)==False:
                return
            self.trMaster.EnsureVisible(ti)
            if self.__isLogDebug__():
                self.__logDebug__('dRoot:%s;sDN:%s'%(self.__logFmt__(dRoot),
                        sDN))
            w=self.lExp[self.iExpSel]
            if dRoot is not None:
                iSz=dRoot.get('used',-1)
            else:
                iSz=-1
            if dRoot is None:
                self.SetValueBySelected(self.iExpSel)
            elif sDN is not None:
                w.SetDN(sDN,iSz)
        except:
            self.__logTB__()
    def OnTreeItemBuild(self,evt):
        try:
            ti=evt.GetTreeNode()
            data=evt.GetTreeNodeData()
            id=evt.GetTreeNodeIDNum()
            idPar=evt.GetParentID()
            if self.__isLogDebug__():
                self.__logDebug__('ti:%r;data:%r;id:%r;idPar:%r'%(ti,data,id,idPar))
        except:
            self.__logTB__()
    def OnTreeItemExec(self,evt):
        try:
            ti=evt.GetTreeNode()
            data=evt.GetTreeNodeData()
            iId=evt.GetTreeNodeIDNum()
            idPar=evt.GetParentID()
            if self.__isLogDebug__():
                self.__logDebug__('ti:%r;data:%r;id:%r;idPar:%r'%(ti,data,id,idPar))
            if type(data)==types.DictType:
                node=None
                o=self.netExplorer.GetReg(data['__reg'])
                if o is None:
                    vtLog.PrintMsg(_(u'execute reg:%s not possible, no object registered.')%(data['__reg']),wid=self)
                    return
                sSrc=data['mnt']
            else:
                if self.netExplorer.IsKeyValid(iId)==False:
                    vtLog.PrintMsg(_(u'execute not possible, no node selected.'),wid=self)
                    return
                node=self.netExplorer.getNodeByIdNum(iId)
                o=self.netExplorer.GetRegByNode(node)
                sSrc=None
                if o is None:
                    vtLog.PrintMsg(_(u'execute node:%08d not possible, no object registered.')%(iId),wid=self)
                    return
            for sMnt,tVal in self._dUnMnt.iteritems():
                iIdMnt=tVal[1]
                if iId==iIdMnt:
                    sFN=tVal[2]
                    self.__killByReg__(o,node,iId,sMnt,sFN)
                    return
            self.__execByReg__(o,node,iId,idPar,ti,sFN=sSrc)
        except:
            self.__logTB__()
    def OnConfigApplChoose(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.dlgAppl.GetPanel()
            sFN=pn.txtExec.GetValue()
            parent=self.GetParent()
            sFN=parent.ChooseFile(sFN,bPosixFN=True,sDftFN='vExplorer.exe',
                    sWildCard=_(u'executable files|*.exe|all files|*.*'))
            if sFN is not None:
                pn.txtExec.SetValue(sFN)
        except:
            self.__logTB__()
    def ConfigAppl(self,sAppl,bSel=False):
        try:
            self.__logDebug__('sAppl:%s;bSel:%d'%(sAppl,bSel))
            if sAppl in self.dAppl:
                if bSel==False:
                    return self.dAppl[sAppl]
            if self.dlgAppl is None:
                lWid=[
                    ('lblRg', (0,0),(1,1),{'name':'lblAppl','label':_(u'application')},None),
                    ('txt',   (0,1),(1,3),{'name':'txtAppl','value':''},None),
                    ('lblRg', (1,0),(1,1),{'name':'lblExec','label':_(u'executable')},None),
                    ('txt',   (1,1),(1,2),{'name':'txtExec','value':''},None),
                    ('cbBmp', (1,3),(1,1),{'name':'cbChoose','bitmap':vtArt.getBitmap(vtArt.Browse),},
                        {'btn':self.OnConfigApplChoose}),
                    ]
                self.dlgAppl=vtgDlg(self,vtgPanel,name='dlgAppl',
                    title=_(u'vExplorer Application'),
                    bmp=imgExplorer.getPluginBitmap(),
                    pnName='pnKill',
                    lGrowCol=[(1,1)],lGrowRow=[(1,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            pn=self.dlgAppl.GetPanel()
            pn.txtAppl.SetValue(sAppl)
            pn.txtExec.SetValue(self.dAppl.get(sAppl,''))
            iRet=self.dlgAppl.ShowModal()
            if iRet>0:
                self.dAppl[sAppl]=pn.txtExec.GetValue()
                self.__logDebug__('sAppl:%s;bSel:%d;dAppl:%s'%(sAppl,bSel,
                        self.__logFmt__(self.dAppl)))
                return self.dAppl[sAppl]
        except:
            self.__logTB__()
        return None
    def OnConfigApplicationsChoose(self,evt):
        try:
            pn=self.dlgApplCfg
            s=pn.lstAppl.GetStringSelection()
            sExec=self.ConfigAppl(s,bSel=True)
            self.__logDebug__('sExec:%s'%(self.__logFmt__(sExec)))
        except:
            self.__logTB__()
    def ConfigApplications(self):
        try:
            if self.dlgApplCfg is None:
                lWid=[
                    ('lblRg', (0,0),(1,2),{'name':'lblAppl','label':_(u'application')},None),
                    ('lst',   (1,1),(1,2),{'name':'lstAppl',},None),
                    ('cbBmp', (1,3),(1,1),{'name':'cbChoose','bitmap':vtArt.getBitmap(vtArt.Browse),},
                        {'btn':self.OnConfigApplicationsChoose}),
                
                ]
                self.dlgApplCfg=vtgDlg(self,vtgPanel,name='dlgApplCfg',
                    title=_(u'vExplorer Applications'),
                    bmp=imgExplorer.getPluginBitmap(),sz=(300,400),
                    pnName='pnAppl',
                    lGrowCol=[(1,1)],lGrowRow=[(1,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            dAppl={}
            dAppl.update(self.dAppl)
            pn=self.dlgApplCfg.GetPanel()
            pn.lstAppl.Clear()
            iOfs=0
            for s in self.APPL_DFT:
                pn.lstAppl.Append(s,-1)
            lExt=self.netExplorer.GetRegisteredExtension()
            if lExt is not None:
                for t in lExt:
                    o=t[-1]
                    if hasattr(o,'GetApplName'):
                        pn.lstAppl.Append(o.GetApplName())
            iRet=self.dlgApplCfg.ShowModal()
            if iRet<=0:
                self.dAppl=dAppl
                self.__logDebug__('dAppl:%s'%(self.__logFmt__(self.dAppl)))
        except:
            self.__logTB__()
    def ConfigExtensions(self):
        try:
            self.__logDebug__(''%())
            if self.dlgExtCfg is None:
                self.dlgExtCfg=vtgDlg(self,
                            title=_(u'vExplorer Configure Extensions'),
                            #kind='frmSngCls',
                            pnCls=vExplorerCfgExtPanel,
                            pnSz=(200,200),
                            #iArrange=-1,
                            name='vExplorerCfgExt',
                            bmp=vtArt.getBitmap(vtArt.Settings))
            if self.dlgExtCfg is None:
                self.__logError__(''%())
                return
            self.__logDebug__('lExt:%s'%(self.__logFmt__(self.lExt)))
            self.dlgExtCfg.SetExt(self.lExt)
            if self.dlgExtCfg.ShowModal()>0:
                self.lExt=self.dlgExtCfg.GetExt()
                self.doCalcExt()
        except:
            self.__logTB__()
    def KillChooseAppl(self,ti=None,dRoot=None,sDN=None):
        try:
            if self.dlgKill is None:
                lWid=[
                    ('lblRg', (0,0),(1,1),{'name':'lblRoot','label':_(u'mount point')},None),
                    ('txt',   (0,1),(1,3),{'name':'txtRoot','value':''},None),
                    ('lblRg', (1,0),(1,1),{'name':'lblAppl','label':_(u'application')},None),
                    ('lst',   (1,1),(1,3),{'name':'lstAppl',},None),
                    ('chk',   (2,1),(1,1),{'name':'chkForce','label':_(u'force'),},None),
                    
                    ]
                self.dlgKill=vtgDlg(self,vtgPanel,name='dlgKill',
                    title=_(u'vExplorer Kill'),
                    bmp=imgExplorer.getPluginBitmap(),
                    pnName='pnKill',
                    lGrowCol=[(1,1)],lGrowRow=[(1,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            pn=self.dlgKill.GetPanel()
            pn.txtRoot.SetValue(sDN)
            pn.chkForce.SetValue(False)
            lExt=self.netExplorer.GetRegisteredExtension()
            pn.lstAppl.Clear()
            iOfs=0
            for t in lExt:
                o=t[-1]
                if hasattr(o,'GetKill'):
                    pn.lstAppl.Append(t[1],iOfs)
                iOfs+=1
            iExt=iOfs
            lProto=self.netExplorer.GetRegisteredNetProtocol()
            for t in lProto:
                o=t[-1]
                if hasattr(o,'GetKill'):
                    pn.lstAppl.Append(t[1],iOfs)
                iOfs+=1
            if iOfs==0:
                return -1
            iRet=self.dlgKill.ShowModal()
            if iRet>0:
                iSel=pn.lstAppl.GetSelection()
                iOfs=pn.lstAppl.GetClientData(iSel)
                bForce=pn.chkForce.GetValue()
                if iOfs<iExt:
                    o=lExt[iOfs][-1]
                else:
                    o=lProto[iOfs-iExt][-1]
                self.__logDebug__('iSel:%d;iOfs:%d;sTag:%s'%(iSel,
                        iOfs,o.GetTagName()))
                return o,bForce
        except:
            self.__logTB__()
        return None,False
    def CreateNew(self,sDN=None,bFile=True):
        try:
            self.__logDebug__(''%())
            if self.dlgCreateNew is None:
                if sDN is None:
                    sDN=self.oLocalRoot.GetCurDN()
                self.dlgCreateNew=vExplorerCreateNewDlg(self)
            self.dlgCreateNew.DoModal(sDN,bFile)
        except:
            self.__logTB__()
    def __chkExplorerPanel2UnMnt__(self,sMnt):
        try:
            self.__logDebug__('sMnt:%s'%(sMnt))
            iIdx=-1
            self.__chkActive__()
            for w in self.lExp:
                iIdx+=1
                sDN=w.GetDN()
                bRst=False
                style=w.GetStyle()
                if sDN is None:
                    bRst=True
                elif type(sDN)==types.DictType:
                    bRst=False
                elif style==1:
                    if sDN.startswith(sMnt):
                        bRst=True
                if bRst==True:
                    self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
        except:
            self.__logTB__()
    def MenuUnMnt(self,iKind=-1):
        """ iKind
        -1 ... all
         0 ... selected
         1 ... selected forced
        """
        try:
            self.__logDebug__('iKind:%d;dUnMnt:%s'%(iKind,
                    self.__logFmt__(self._dUnMnt)))
            if iKind==-1:
                l=[]
                for sMnt,tVal in self._dUnMnt.iteritems():
                    iId=tVal[1]
                    sFN=tVal[2]
                    if type(iId)==types.StringType:
                        self.netExplorer.GetReg(iId)
                        node=None
                    elif type(iId)==types.DictType:
                        o=self.netExplorer.GetReg(iId['__reg'])
                        node=None
                    else:
                        node=self.netExplorer.getNodeByIdNum(iId)
                        if node is not None:
                            o=self.netExplorer.GetRegByNode(node)
                    l.append((o,node,iId,sMnt,sFN))
                for args in l:
                    #self.__chkExplorerPanel2UnMnt__(args[3])
                    self.__killByReg__(*args)
            elif iKind==0 or iKind==1:
                ti,dRoot,sDN=self.trMaster.GetSelectedDN()
                if dRoot is None:
                    self.__logError__('no unmountable root selected'%())
                    return
                self.__logDebug__('sDN:%s;dUnMnt:%s;dRoot:%s'%(sDN,
                            self.__logFmt__(self._dUnMnt),
                            self.__logFmt__(dRoot)))
                sRoot=dRoot['root']
                for sMnt,tVal in self._dUnMnt.iteritems():
                    if sMnt==sRoot:
                        self.__logDebug__('sMnt:%s;tVal:%s'%(sMnt,
                                self.__logFmt__(tVal)))
                        iId=tVal[1]
                        sFN=tVal[2]
                        if iId is None:
                            self.__logWarn__('sMnt:%s;uncertain what to do;tVal:%s'%(sMnt,
                                    self.__logFmt__(tVal)))
                            break
                        elif type(iId)==types.StringType:
                            o=self.netExplorer.GetReg(iId)
                            node=None
                        elif type(iId)==types.DictType:
                            o=self.netExplorer.GetReg(iId['__reg'])
                            node=None
                        else:
                            node=self.netExplorer.getNodeByIdNum(iId)
                            if node is not None:
                                o=self.netExplorer.GetRegByNode(node)
                        #self.__chkExplorerPanel2UnMnt__(sMnt)
                        self.__killByReg__(o,node,iId,sMnt,sFN,bForce=(iKind==1))
                        return
                o,bForce=self.KillChooseAppl(ti,dRoot,sDN)
                if o is not None:
                    #self.__chkExplorerPanel2UnMnt__(sDN)
                    self.__killByReg__(o,None,None,sDN,None,bForce=bForce or (iKind==1))
                pass
        except:
            self.__logTB__()
    def __killByReg__(self,o,node,iId,sMnt=None,sFN=None,bForce=False):
        try:
            sK,sSrc,sFmt,lFmt=o.GetKill(node,sFN=sFN,bForce=bForce)
            bFlt=False
            l=[]
            sUsr=''
            sRoot=None
            for s in lFmt:
                v=''
                if s.startswith('appl_'):
                    v=self.oLocalRoot.GetApplExec(s)
                    if v is None:
                        v=self.ConfigAppl(s)
                    if v is None:
                        vtLog.PrintMsg(_(u'application not resolved %s.')%(s),wid=self)
                        bFlt=True
                elif s in ['root','drive']:
                    if sMnt is not None:
                        sRoot=sMnt
                    if sRoot is None:
                        if self.oLocalRoot.GetSystem()==1:#SYSTEM==1:
                            if self.dlgRoot is None:
                                self.dlgRoot=vtgDialog(self,
                                    vExplorerWinDrivePanel,_(u'choose drive'),name='dlgDrive')
                            self.dlgRoot.SetRootLst(lRoot)
                            ret=self.dlgRoot.ShowModal()
                            if ret==wx.ID_OK:
                                sRoot=self.dlgRoot.GetRootSel()
                                if len(sRoot)==0:
                                    return
                                v=sRoot
                            elif ret==wx.ID_CANCEL:
                                vtLog.PrintMsg(_(u'no root defined.'),wid=self)
                                return
                            else:
                                return
                    if s=='drive':
                        v=sRoot[:2]
                    else:
                        v=sRoot
                else:
                    bFlt=True
                l.append(v)
            if bFlt==False:
                try:
                    iIdx=-1
                    for w in self.lExp:
                        iIdx+=1
                        if w.Check2Clear(sRoot):
                            self.SetValueByVal(iIdx,{'__reg':'ExplorerRoot'})
                except:
                    self.__logTB__()
                t=type(sFmt)
                if t in [types.StringType,types.UnicodeType]:
                    sCmd=sFmt%tuple(l)
                    self.__logPrintMsg__(_(u'execute %s ...')%sK)
                    p=vtProcess(sCmd,shell=False)
                    tFunc=(self.doExec,(p,_(u'execute %s done.')%sK),{})
                elif t in [types.FunctionType,types.MethodType]:
                    tFunc=(sFmt,tuple(l),{})
                elif t==types.TupleType:
                    frmMain=self.GetParent()
                    func,args,kwargs=sFmt
                    args=args+tuple(l)
                    tFunc=(func,args,kwargs)
                else:
                    tFunc=None
                if tFunc is not None:
                    frmMain=self.GetParent()
                    frmMain.DoBranch(\
                            tFunc,
                            (self.CheckUnMntSuccess,(),{}),
                            (self.trMaster.UpdateRootInfoDict,(),{}))
        except:
            self.__logTB__()
    def __askReCall__(self,sK):
        try:
            if self.dlgReCallAsk is None:
                lWid=[
                    ('lblRg', (0,0),(1,1),{'name':'lblReCall','label':_(u'source')},None),
                    ('txtRd', (0,1),(1,3),{'name':'txtSrc','value':''},None),
                    ('lblRg', (1,0),(1,1),{'name':'lblReCallAs','label':_(u'recall as')},None),
                    ('txt',   (1,1),(1,3),{'name':'txtKey','value':''},None),
                    ('chk',   (2,1),(1,1),{'name':'chkReCall','label':_(u'recall password'),},None),
                    
                    ]
                self.dlgReCallAsk=vtgDlg(self,vtgPanel,name='dlgAskReCall',
                    title=_(u'vExplorer Recall'),
                    bmp=imgExplorer.getPluginBitmap(),
                    pnName='pnReCall',
                    lGrowCol=[(1,1)],#lGrowRow=[(1,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            pn=self.dlgReCallAsk.GetPanel()
            pn.txtSrc.SetValue(sK)
            sDft=u''
            if sK.startswith('smb:\\\\'):
                iStartExt=sK.find('\\',6)
                if iStartExt>0:
                    sDft=sK[:iStartExt+1]+u'*'
            else:
                iStartExt=sK.find('.')
                iStartSep=sK.find('_')
                if iStartSep>0:
                    if iStartExt>0:
                        iStart=min(iStartSep,iStartExt)
                        sDft=sK[:iStart]+u'*'
                    else:
                        sDft=sK[:iStartSep]+u'*'
                else:
                    if iStartExt>0:
                        sDft=sK[:iStartExt]+u'*'
            pn.txtKey.SetValue(sDft)
            pn.chkReCall.SetValue(True)
            iRet=self.dlgReCallAsk.ShowModal()
            if iRet>0:
                sKey=pn.txtKey.GetValue()
                if len(sKey)==0:
                    sKey=None
                bReCall=pn.chkReCall.GetValue()
                if bReCall==True:
                    return 1,sKey
                else:
                    return 0,None
            return -2,None
        except:
            self.__logTB__()
        return -1,None
    def OnManageReCallSel(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.dlgReCallMng.GetPanel()
            k=pn.lstReCall.GetStringSelection()
            self.__logDebug__('k:%s;dReCall:%s'%(k,
                    self.__logFmt__(self._dReCall[k])))
            pn.txtSrc.SetValue(k)#self._dReCall[k])
        except:
            self.__logTB__()
    def OnManageReCallSelDel(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.dlgReCallMng.GetPanel()
            k=pn.txtSrc.GetValue()
            if k in self._dReCall:
                self._dReCall[-1].append(k)
            pn.txtSrc.SetValue(u'')
            k=pn.lstReCall.GetSelection()
            if k>=0:
                pn.lstReCall.Delete(k)
        except:
            self.__logTB__()
    def OnManageReCallSelEdit(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.dlgReCallMng.GetPanel()
            kNew=pn.txtSrc.GetValue()
            k=pn.lstReCall.GetSelection()
            if k>=0:
                kOld=pn.lstReCall.GetStringSelection()
                if len(kNew)>0:
                    self._dReCall[-2].append((kOld,kNew))
                    pn.lstReCall.SetString(k,kNew)
            #pn.txtSrc.SetValue(u'')
        except:
            self.__logTB__()
    def ManageReCall(self):
        try:
            self.__logDebug__('_dReCall;%s;_dStore;%s'%(\
                                self.__logFmt__(self._dReCall),
                                self.__logFmt__(self._dStore)))
            if self.dlgReCallMng is None:
                lWidBt=[
                    ('cbBmp', 0,4,{'name':'cbDel','bitmap':vtArt.getBitmap(vtArt.Del)},{
                            'btn':self.OnManageReCallSelDel}),
                    ]
                lWid=[
                    ('lblRg', (0,0),(1,1),{'name':'lblReCall','label':_(u'key')},None),
                    ('lst',   (0,1),(1,3),{'name':'lstReCall',},{
                            'sel':self.OnManageReCallSel}),
                    ('szBoxVert',(0,4),(1,1),{'name':'bxsBt'},lWidBt),
                    ('lblRg', (1,0),(1,1),{'name':'lblSrc','label':_(u'name')},None),
                    ('txt',   (1,1),(1,3),{'name':'txtSrc','value':''},None),
                    ('cbBmp', (1,4),(1,1),{'name':'cbEdit','bitmap':vtArt.getBitmap(vtArt.Edit)},{
                            'btn':self.OnManageReCallSelEdit}),
                    ]
                self.dlgReCallMng=vtgDlg(self,vtgPanel,name='dlgReCallMng',
                    title=_(u'vExplorer Recall Manage'),
                    bmp=imgExplorer.getPluginBitmap(),
                    pnName='pnReCallMng',
                    lGrowCol=[(1,1)],lGrowRow=[(0,1)],
                    bEnMark=False,bEnMod=False,lWid=lWid)
            pn=self.dlgReCallMng.GetPanel()
            #pn.txtRoot.SetValue(sDN)
            #lExt=self.netExplorer.GetRegisteredExtension()
            pn.lstReCall.Clear()
            pn.txtSrc.SetValue(u'')
            iOfs=0
            if self._dReCall is not None:
                keys=self._dReCall.keys()
                keys.sort()
                for k in keys:
                    t=type(k)
                    if t in [types.StringType,types.UnicodeType]:
                        pn.lstReCall.Append(k)
                        iOfs+=1
            #if iOfs==0:
            #    return -1
            self._dReCall[-1]=[]
            self._dReCall[-2]=[]
            iRet=self.dlgReCallMng.ShowModal()
            lEdit=self._dReCall[-2]
            del self._dReCall[-2]
            lDel=self._dReCall[-1]
            del self._dReCall[-1]
            if iRet>0:
                iSel=pn.lstReCall.GetSelection()
                self.__logDebug__('iSel:%d;lDel:%s;lEdit:%s'%(iSel,
                        self.__logFmt__(lDel),self.__logFmt__(lEdit)))
                for kOld,kNew in lEdit:
                    if kOld in self._dReCall:
                        v=self._dReCall[kOld]
                        del self._dReCall[kOld]
                        self._dReCall[kNew]=v
                for k in lDel:
                    if k in self._dReCall:
                        del self._dReCall[k]
                self.__logDebug__('_dReCall;%s;_dStore;%s'%(\
                                self.__logFmt__(self._dReCall),
                                self.__logFmt__(self._dStore)))
                return 1
            return 0
        except:
            self.__logTB__()
        return None,False
    def __setupReCall__(self,bAlways=True):
        try:
            self.__logDebug__('loggedin:%d;_sReCallPhrase;%r'%(self.IsLoggedIn(),
                        self._sReCallPhrase))
            if self.IsLoggedIn():
                if bAlways==False:
                    return -6
                sPwd,Enc=self.__getPwd__(_(u'Password Container'))
                if sPwd is None:
                    return -4
                if self._sReCallPhrase is not None:
                    if VERBOSE>5:
                        self.__logDebug__('phrase:%s;dReCall;%s'%(self._sReCallPhrase,
                                self.__logFmt__(self._dReCall)))
                    sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                    sP=self.oCypInt.EncryptHex(sPwd)
                    self._sReCallPhrase=sP
                    for k,d in self._dReCall.iteritems():
                        if k is None:
                            continue
                        if 'pwd' in d:
                            o=vtSecAes(sPhrase)
                            #o=vcsCrypto(sPhrase)
                            c=o.DecryptHex(d['pwd'])
                            o=vtSecAes(sPwd)
                            #o=vcsCrypto(sPwd)
                            e=o.EncryptHex(c)
                            d['pwd']=e
                    if VERBOSE>5:
                        self.__logDebug__('phrase:%s;dReCall;%s'%(self._sReCallPhrase,
                                self.__logFmt__(self._dReCall)))
                    return -5
                return -3
            sPwd,Enc=self.__getPwd__(_(u'Password Container'))
            if sPwd is None:
                self.__logDebug__(''%())
                return -2
            self._sReCallPhrase=self.oCypInt.EncryptHex(sPwd)
            if VERBOSE>5:
                self.__logDebug__('phrase:%s'%(self._sReCallPhrase))
            if self._dReCall is not None:
                if VERBOSE>5:
                    self.__logDebug__('phrase:None;dReCall;%s'%(self.__logFmt__(self._dReCall)))
                for k,d in self._dReCall.iteritems():
                    if k is None:
                        continue
                    if 'pwd' in d:
                        sPhrase=self.oCypInt.DecryptHex(d['pwd'])
                        o=vtSecAes(sPwd)
                        #o=vcsCrypto(sPwd)
                        e=o.EncryptHex(sPhrase)
                        d['pwd']=e
                if VERBOSE>5:
                    self.__logDebug__('phrase:%s;dReCall;%s'%(self._sReCallPhrase,
                                self.__logFmt__(self._dReCall)))
            sReCall=self.GetCfgVal(['recall'],fallback=None)
            if sReCall is None:
                #self._dReCall={}
                return 0
            return 1
        except:
            self._sReCallPhrase=None
            self.__logTB__()
            return -1
    def __getReCall__(self,dReCall,node,sK):
        try:
            self.__logDebug__('sK:%s;dReCall:%s'%(sK,self.__logFmt__(self._dReCall)))
            if node is None:
                pass
            elif dReCall is None:
                return None,{}
            s=None
            if sK in self._dReCall:
                try:
                    s=self._dReCall[sK].get('pwd',None)
                    self.__logDebug__('k:%s;%s'%(sK,s))
                except:
                    self.__logTB__()
                    return None,{}
            for k,d in self._dReCall.iteritems():
                if k is None:
                    continue
                if fnmatch.fnmatchcase(sK,k)==True:
                    s=d.get('pwd',None)
                    self.__logDebug__('k:%s;%s'%(k,s))
                    break
            if s is None:
                return None,{}
            if self.IsLoggedIn():
                sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                o=vtSecAes(sPhrase)
                #o=vcsCrypto(sPhrase)
                c=o.DecryptHex(s)
                return c,d
            else:
                c=self.oCypInt.DecryptHex(s)
                return c,d
        except:
            self.__logTB__()
        return None,{}
    def __decrypt__(self,s):
        try:
            if self.IsLoggedIn():
                sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                o=vtSecAes(sPhrase)
                #o=vcsCrypto(sPhrase)
                c=o.DecryptHex(s)
                return c
            else:
                c=self.oCypInt.DecryptHex(s)
                return c
        except:
            self.__logTB__()
        return ''
    def __encrypt__(self,s):
        try:
            if self.IsLoggedIn():
                sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                o=vtSecAes(sPhrase)
                #o=vcsCrypto(sPhrase)
                sEnc=o.EncryptHex(s)
            else:
                sEnc=self.oCypInt.EncryptHex(s)
            return sEnc
        except:
            self.__logTB__()
        return ''
    def __getPwd__(self,sK):
        try:
            self.__logDebug__('sK:%s'%(sK))
            if self.dlgPwd is None:
                self.dlgPwd=vtmPwdDlg(self,name='dlgPwd',
                        title=_(u'vExplorer Password for %s')%(sK),
                        bmp=imgExplorer.getPluginBitmap(),
                        sz=(400,-1))
            else:
                self.dlgPwd.SetTitle(_(u'vExplorer Password for %s')%(sK))
            self.dlgPwd.Centre()
            iRet=self.dlgPwd.ShowModal()
            if iRet>0:
                #sUsr='me'
                sPwd=self.dlgPwd.GetValue()
                if self._sReCallPhrase is not None:
                    sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                    o=vtSecAes(sPhrase)
                    #o=vcsCrypto(sPhrase)
                    sEnc=o.EncryptHex(sPwd)
                else:
                    sEnc=self.oCypInt.EncryptHex(sPwd)
                return sPwd,sEnc
        except:
            self.__logTB__()
        return None,None
    def GetPwd(self,dReCall,node,o,sK):
        try:
            v=None
            sKeyReCall=None
            v,dVal=self.__getReCall__(dReCall,node,sK)
            if v is None:
                v=''
                iClrPwd=o.GetClrPwdVal(node)
                v,sEnc=self.__getPwd__(sK)
                if v is None:
                    bFlt=True
                    sPwd=''
                    return -2,'',None
                if node is None:
                    iTmp,sKeyReCall=self.__askReCall__(sK)
                    if iTmp>0:
                        if iTmp>0:
                            iClrPwd=0
                        else:
                            iClrPwd=1
                else:
                    if o.GetReCallVal(node)==False:
                        dReCall[None]=False
                dReCall['_store']=iClrPwd==0
                dReCall['pwd']=sEnc
                if iClrPwd==0:
                    self._dReCall[1]=dReCall
                    iRet=self.__setupReCall__(bAlways=False)
                    if iRet>=0:
                        dReCall=self._dReCall[1]
                    del self._dReCall[1]
                    self.__logDebug__('setup recall:%d'%(iRet))
                return 0,v,sKeyReCall
            dReCall.update(dVal)
            return 0,v,sKeyReCall
        except:
            self.__logTB__()
        return -1,'',None
    def GetUsrPwd(self,dReCall,node,o,sK):
        try:
            v=None
            sKeyReCall=None
            sPwd,dVal=self.__getReCall__(dReCall,node,sK)
            if sPwd is None:
                if self.dlgUsrPwd is None:
                    self.dlgUsrPwd=vtmUsrPwdDlg(self,name='dlgUsrPwd',
                        title=_(u'vExplorer User/Password for %s')%(sK),
                        bmp=imgExplorer.getPluginBitmap(),
                        sz=(400,-1))
                self.dlgUsrPwd.Centre()
                iRet=self.dlgUsrPwd.ShowModal()
                if iRet>0:
                    sUsr,sPwd=self.dlgUsrPwd.GetValue()
                    if self._sReCallPhrase is not None:
                        sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                        o=vtSecAes(sPhrase)
                        #o=vcsCrypto(sPhrase)
                        sEnc=o.EncryptHex(sPwd)
                    else:
                        sEnc=self.oCypInt.EncryptHex(sPwd)
                    iClrPwd=1
                    if node is None:
                        iTmp,sKeyReCall=self.__askReCall__(sK)
                        #if iTmp>0:
                        #    dReCall={'usr':sUsr,'pwd':sPwd}
                        if iTmp>0:
                            if iTmp>0:
                                iClrPwd=0
                            else:
                                iClrPwd=1
                    dReCall['_store']=iClrPwd==0
                    dReCall['usr']=sUsr
                    dReCall['pwd']=sEnc
                    if iClrPwd==0:
                        self._dReCall[2]=dReCall
                        iRet=self.__setupReCall__(bAlways=False)
                        if iRet>=0:
                            dReCall=self._dReCall[2]
                        del self._dReCall[2]
                        self.__logDebug__('setup recall:%d'%(iRet))
                    v=(dReCall['usr'],sPwd)
                    return 0,v,sKeyReCall
                else:
                    return -2,None,sKeyReCall
            dReCall.update(dVal)
            #if dReCall is not None:
            #    dReCall['usr']=dVal.get('usr','')
            #    dReCall['pwd']=dVal.get('pwd','')
            v=(dReCall['usr'],sPwd)
            return 0,v,sKeyReCall
        except:
            self.__logTB__()
        return -1,(None,None),None
    def __execByReg__(self,o,node,iId,idPar,ti,sFN=None):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            lRoot=self.oLocalRoot.GetRootLstFree()
            sK,sSrc,sFmt,lFmt=o.GetExec(node,sFN=sFN)
            if (sFmt is None) and (lFmt is None):
                if type(sSrc)==types.ListType:
                    for iIdChild in sSrc:
                        if bDbg:
                            self.__logDebug__('check child id:%08d'%(iIdChild))
                        nodeChild=self.netExplorer.getNodeByIdNum(iIdChild)
                        if nodeChild is None:
                            self.__logError__('node:%d not found'%(iIdChild))
                            continue
                        oChild=self.netExplorer.GetRegByNode(nodeChild)
                        self.__execByReg__(oChild,nodeChild,iIdChild,idPar,ti,sFN)
                        #sK,sSrc,sFmt,lFmt=o.GetExec(node,sFN=None)
                return
            dReCall={}
            bFlt=False
            l=[]
            sUsr=''
            sRoot=None
            sKeyReCall=None
            iClrPwd=1
            bShell=False
            bDetach=False
            fDly=-1.0
            sPath=None
            sSaveDN=None
            for s in lFmt:
                v=''
                if type(s)==types.TupleType:
                    if s[0]=='path':
                        sPath=s[1]
                    elif s[0]=='savedn':
                        sSaveDN=s[1]
                    continue
                elif s.startswith('appl_'):
                    v=self.oLocalRoot.GetApplExec(s)
                    if v is None:
                        v=self.ConfigAppl(s)
                    if v is None:
                        vtLog.PrintMsg(_(u'application not resolved %s.')%(s),wid=self)
                        bFlt=True
                elif s in ['root','drive']:
                    if node is not None:
                        sRoot=o.GetMnt(node)
                    bSel=False
                    if sRoot is None:
                        bSel=True
                    else:
                        sRoot=self.oLocalRoot.ConvToOS(sRoot)
                        
                        bSel=True
                        for sR in lRoot:
                            if sR==sRoot:
                                bSel=False
                                break
                    if bSel==True:
                        if self.oLocalRoot.GetSystem()==1:#SYSTEM==1:
                            if self.dlgRoot is None:
                                self.dlgRoot=vtgDialog(self,
                                    vExplorerWinDrivePanel,_(u'choose drive'),name='dlgDrive')
                            self.dlgRoot.SetRootLst(lRoot)
                            ret=self.dlgRoot.ShowModal()
                            if ret==wx.ID_OK:
                                sRoot=self.dlgRoot.GetRootSel()
                                if len(sRoot)==0:
                                    return
                                v=sRoot
                            elif ret==wx.ID_CANCEL:
                                vtLog.PrintMsg(_(u'no root defined.'),wid=self)
                                return
                            else:
                                return
                    else:
                        v=sRoot
                    try:
                        if iId is None:
                            self._dMntPending[sRoot]=(sK,{'__reg':o.GetTagName()},sFN)
                        else:
                            self._dMntPending[sRoot]=(sK,iId,sFN)
                    except:
                        self.__logTB__()
                        self._dMntPending[sRoot]=(sK,iId,sFN)
                    if s=='drive':
                        v=sRoot[:2]
                    else:
                        v=sRoot
                elif s=='pwd':
                    iRet,v,sKeyReCall=self.GetPwd(dReCall,node,o,sK)
                elif s=='usr_pwd':
                    iRet,v,sKeyReCall=self.GetUsrPwd(dReCall,node,o,sK)
                elif s=='vdn0':
                    s=self.GetDN(self.iExpSel)
                    if type(s)!=types.UnicodeType:
                        self.__logDebug__('vdn0:%s'%(repr(s)))
                        bFlt=True
                        v=''
                    else:
                        v=s[:-1].replace('/',os.path.sep)
                    self.__logDebug__('vdn0:%s'%(v))
                elif s=='vdn1':
                    s=self.GetDN(self.iExpPrv)
                    if type(s)!=types.UnicodeType:
                        self.__logDebug__('vdn1:%s'%(repr(s)))
                        bFlt=True
                        v=''
                    else:
                        v=s[:-1].replace('/',os.path.sep)
                    self.__logDebug__('vdn1:%s'%(v))
                elif s=='view0':
                    s=self.GetDN(self.iExpSel)
                    if type(s)!=types.UnicodeType:
                        self.__logDebug__('view0:%s'%(repr(s)))
                        bFlt=True
                        v=''
                    else:
                        v=s[:].replace('/',os.path.sep)
                    self.__logDebug__('view0:%s'%(v))
                elif s=='view1':
                    s=self.GetDN(self.iExpPrv)
                    if type(s)!=types.UnicodeType:
                        self.__logDebug__('view1:%s'%(repr(s)))
                        bFlt=True
                        v=''
                    else:
                        v=s[:].replace('/',os.path.sep)
                    self.__logDebug__('view1:%s'%(v))
                elif s=='sdn0':
                    lDN=self.GetSelDN(self.iExpSel,bPosix=False)
                    self.__logDebug__(['sdn0',lDN])
                    if len(lDN)>0:
                        v=lDN[0][:]
                    else:
                        bFlt=True
                        v=''
                    self.__logDebug__('sdn0:%s'%(v))
                elif s=='sdn':
                    lDN=self.GetSelDN(self.iExpSel,bPosix=False)
                    self.__logDebug__(['sdn',lDN])
                    if len(lDN)>0:
                        v=u' '.join(lDN)
                    else:
                        bFlt=True
                        v=''
                    self.__logDebug__('sdn:%s'%(v))
                elif s=='sfn0':
                    lFN=self.GetSelFN(self.iExpSel,bPosix=False)
                    self.__logDebug__(['sfn0',lFN])
                    if len(lFN)>0:
                        v=lFN[0][:]
                    else:
                        bFlt=True
                        v=''
                    self.__logDebug__('sfn0:%s'%(v))
                elif s=='sfn':
                    lFN=self.GetSelFN(self.iExpSel,bPosix=False)
                    self.__logDebug__(['sfn',lFN])
                    if len(lFN)>0:
                        v=u' '.join(lFN)
                    else:
                        bFlt=True
                        v=''
                    self.__logDebug__('sfn:%s'%(v))
                elif s=='sel':
                    lDN=self.GetSelDN(self.iExpSel,bPosix=False)
                    lFN=self.GetSelFN(self.iExpSel,bPosix=False)
                    self.__logDebug__(['sel',lDN,lFN])
                    v=''
                    iLenDN=len(lDN)
                    iLenFN=len(lFN)
                    if iLenDN>0:
                        if iLenFN>0:
                            v=u' '.join(lDN+lFN)
                        else:
                            v=u' '.join(lDN)
                    elif iLenFN>0:
                        v=u' '.join(lFN)
                    if len(v)==0:
                        bFlt=True
                    self.__logDebug__('sel:%s'%(v))
                elif s=='shell':
                    bShell=True
                    continue
                elif s=='detach':
                    bDetach=True
                    continue
                else:
                    bFlt=True
                l.append(v)
            if iClrPwd==0:
                if self.IsLoggedIn()==False:
                    self.MenuLogin()
            if bFlt==True:
                self.__logInfo__('sK:%r;bFlt=%d'%(sK,bFlt))
                return -2
            self.__logDebug__('sK:%r;dReCall;%s'%(sK,self.__logFmt__(dReCall)))
            if None in dReCall:
                dReCall=None
        except:
            self.__logTB__()
            return -1
        try:
            t=type(sFmt)
            tFunc=None
            if t in [types.StringType,types.UnicodeType]:
                if hasattr(o,'DoExec'):
                    if hasattr(o,'GetDlyVal'):
                        sCmd=sFmt%tuple(l)
                        fDly=o.GetDlyVal(node)
                        if o.GetSchedVal(node)>0:
                            self.MenuAction(-100)
                            
                            self.dlgAction.AddCmd(100,sK,fDly,
                                    #self.netExplorer.DoSafe,
                                    self.doExecCmd,iId,-1,ti=ti,sCmd=sCmd)
                        else:
                            if fDly>0.0:
                                self.CallTopWid('CallBackDelayed',fDly,
                                        self.doExecCmd,iId,-1,ti=ti,sCmd=sCmd)
                            else:
                                self.MenuAction(-101)
                                self.doExecCmd(iId,-1,ti=ti,sCmd=sCmd)
                        return 0
                    elif sFmt is None:
                        o.DoExec(self,node,None,ti=ti,sCmd=sCmd)
                try:
                    sCwd=None
                    if self._dReCall is not None:
                        self._dReCall[None]=(sK,dReCall,sKeyReCall)
                    sCmd=sFmt%tuple(l)
                    self.__logPrintMsg__(_(u'execute %s ...')%sK)
                    self.__logDebug__('detach:%d;shell:%d'%(bDetach,bShell))
                    if sPath is not None:
                        sCwd=os.getcwdu()
                        try:
                            os.chdir(sPath)
                        except:
                            self.__logTB__()
                            self.__logError__('change to dn:%s failed'%(sPath))
                    if bDetach==True:
                        p=subprocess.Popen(sCmd,shell=False,
                                stdin=None,stdout=None,stderr=None)
                        self.__logPrintMsg__(_(u'execute %s done.')%sK)
                        return 0
                    else:
                        p=vtProcess(sCmd,shell=bShell)
                    tFunc=(self.doExec,(p,_(u'execute %s done.')%sK),{})
                except:
                    self.__logTB__()
                    self.__logPrintMsg__(_(u'execute %s failed.')%sK)
                if sCwd is not None:
                    os.chdir(sCwd)
            elif t in [types.FunctionType,types.MethodType]:
                if self._dReCall is not None:
                    self._dReCall[None]=(sK,dReCall)
                tFunc=(sFmt,tuple(l),{})
            elif t==types.TupleType:
                if self._dReCall is not None:
                    self._dReCall[None]=(sK,dReCall,sKeyReCall)
                func,args,kwargs=sFmt
                args=args+tuple(l)
                tFunc=(func,args,kwargs)
            if tFunc is not None:
                frmMain=self.GetParent()
                frmMain.DoBranch(\
                            tFunc,
                            (self.CheckMntSuccess,(),{}),
                            (self.trMaster.UpdateRootInfoDict,(),{}))
            return 0
        except:
            self.__logTB__()
            frmMain=self.GetParent()
            frmMain.CallBack(self.CheckMntSuccess)
        return -2
    def doExecCmd(self,iId,iIdPar,ti=None,sCmd=None):
        try:
            node=self.netExplorer.getNodeByIdNum(iId)
            if node is None:
                self.__logError__('node:%d not found'%(iId))
                return
            o=self.netExplorer.GetRegByNode(node)
            sK,sSrc,sFmt,lFmt=o.GetExec(node,sFN=None)
            
            self.__logPrintMsg__(_(u'execute %s ...')%sK)
            oProc=o.DoExec(self,node,None,ti=ti,sCmd=sCmd)
            if oProc is None:
                self.__logError__('execute %s failed.'%(sK))
                self.__logPrintMsg__(_(u'execute %s failed.')%sK)
            else:
                oProc.BindEvent('cmd',self.OnCmdExecProcCmd)
                self.__logPrintMsg__(_(u'execute %s done.')%sK)
                try:
                    if self.dlgRun is not None:
                        sTag=self.netExplorer.GetTagNamesWithRel(node)
                        lTag=sTag.split(u'.')
                        self.dlgRun.AddProc(oProc,lTag,sK)
                except:
                    self.__logTB__()
                return oProc
        except:
            self.__logTB__()
        return None
    def doExec(self,p,sMsg):
        try:
            iRet=0
            lErr=[]
            lOut=[]
            def handleStdErr(s,res):
                res.append(s)
            def handleStdOut(s,res):
                res.append(s)
            p.SetCB(1,'\n',handleStdOut,lOut)
            p.SetCB(2,'\n',handleStdErr,lErr)
            self.__logInfo__('proc started'%())
            p.communicateLoop()
            self.__logInfo__('proc finished'%())
            if len(lErr)>0:
                self.__logPrintMsg__(sMsg+u' '+_(u'failure.'))
                iRet=-1
            else:
                self.__logPrintMsg__(sMsg+u' '+_(u'successful.'))
            self.__logDebug__('lOut;%s;lErr;%s'%(self.__logFmt__(lOut),
                        self.__logFmt__(lErr)))
            return 0
        except:
            self.__logTB__()
            iRet=-1
        return iRet
    def OpenFile(self,fn):
        if self.netExplorer.ClearAutoFN()>0:
            self.netExplorer.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                if self.netMaster is not None:
                    self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            if self.netMaster is not None:
                self.netMaster.SetCfgNode()
    def __setCfg__(self):
        try:
            self.__logDebug__(''%())
            self.BuildCfgDict()
            self.SetCfgData()
        except:
            self.__logTB__()
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            self.__logTB__()
    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.GetCfgData()
            wx.CallAfter(self.splMain.SizeWindows)
        except:
            self.__logTB__()
    def OnMove(self, event):
        event.Skip()
        try:
            pos=self.GetParent().GetPosition()
        except:
            self.__logTB__()

    def SetCfgData(self):
        vtgPanelMixinWX.SetCfgData(self)
        v=self.GetCfgVal(['nav_sash'],fallback=200,funcConv=int)
        self.slwNav.SetDefaultSize((v, 1000))
        v=self.GetCfgVal(['explorer_fn'],fallback=u'vExplorer.xml')
        self.OpenFile(v)
        try:
            iCnt=self.GetCfgVal(['views'],fallback=1,funcConv=int)
            iLen=len(self.lExp)
            #dPath=self.GetCfgVal(['paths'],fallback={})
            for i in xrange(0,iCnt):
                sK='path_%02d'%i
                sDN=self.GetCfgVal(['paths',sK])
                if i<iLen:
                    w=self.lExp[i]
                    if type(sDN)==types.DictType:
                        self.SetValueByVal(i,sDN)
                    else:
                        self.netExplorer.CallBack(w.SetDN,sDN)
                else:
                    if type(sDN)==types.DictType:
                        self.AddExplorerView(sDN=None)
                        self.SetValueByVal(i,sDN)
                    else:
                        self.AddExplorerView(sDN=sDN)
            #for i in xrange(0,iCnt):
            #    sK='path_%02d'%i
            #    sDN=self.GetCfgVal(['paths',sK])
            #    w=self.lExp[i]
            #    w.SetDN(sDN)
            self.iExpPrv=-1
            for w in self.lExp:
                w.SetActive(False)
            #self.lExp[self.iExpSel].SetActive(False)
            #self.iExpPrv=self.iExpSel
            self.iExpSel=0
            self.lExp[self.iExpSel].SetActive(True)
        except:
            self.__logTB__()
        try:
            self.dAppl=self.GetCfgVal(['appls'],fallback={})
            self.__logDebug__('dAppl;%s'%(self.__logFmt__(self.dAppl)))
            lAppl=[]
            for k,sAppl in self.dAppl.iteritems():
                if k.startswith('appl_'):
                    if os.path.exists(sAppl)==False:
                        lAppl=k
            for k in lAppl:
                del self.dAppl[k]
            self.__logDebug__('dAppl;%s'%(self.__logFmt__(self.dAppl)))
            self._dReCall={}
            #if self.GetCfgVal(['recall'],fallback=None) is not None:
            #    self._dReCall=None
            #    wx.CallAfter(self.MenuLogin)
        except:
            self.__logTB__()
        try:
            self.lExt=self.GetCfgVal(['extensions'],fallback={})
            self.doCalcExt()
            self.__logDebug__('dExt;%s'%(self.__logFmt__(self.dExt)))
        except:
            self.__logTB__()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnSrc.Refresh()
        self.pnData.Refresh()
    def doCalcExt(self):
        try:
            self.dExt={}
            for dd in self.lExt:
                d=dd.get('extension',None)
                if d is not None:
                    sExt=d.get('ext','')
                    sApp=d.get('app','')
                    sArg=d.get('arg','')
                    if sExt in self.dExt:
                        self.__logError__('sExt:%s already in dict'%(self.dExt))
                    else:
                        self.dExt[sExt]=d#(sApp,sArg)
        except:
            self.__logTB__()
    def GetCfgData(self):
        try:
            vtgPanelMixinWX.GetCfgData(self)
            self.__logDebug__(''%())
            self.SetCfgVal(self.netExplorer.GetFN(),['explorer_fn'])
            iCnt=len(self.lExp)
            self.SetCfgVal(iCnt,['views'])
            d={}
            for i in xrange(0,iCnt):
                sK='path_%02d'%i
                sDN=self.lExp[i].GetDN()
                if sDN is not None:
                    if type(sDN)==types.DictType:
                        if '__reg' in sDN:
                            d[sK]={'__reg':sDN['__reg']}
                        else:
                            d[sK]=sDN
                    else:
                        d[sK]=sDN
            self.SetCfgVal(d,['paths'])
        except:
            self.__logTB__()
        try:
            lAppl=[]
            d={}
            for k,sAppl in self.dAppl.iteritems():
                d[k]=sAppl
            self.SetCfgVal(d,['appls'])
        except:
            self.__logTB__()
        try:
            self.__logDebug__('lExt;%s'%(self.__logFmt__(self.lExt)))
            self.SetCfgVal(self.lExt,['extensions'])
        except:
            self.__logTB__()
        try:
            self.__logDebug__('_dReCall;%s;_dStore;%s'%(\
                                self.__logFmt__(self._dReCall),
                                self.__logFmt__(self._dStore)))
            if self._dReCall is None:
                self.SetCfgVal('',['recall'])
            else:
                self.SetCfgVal('',['recall'])
                l=[]
                for k,dReCall in self._dReCall.iteritems():
                    if k is None:
                        continue
                    if dReCall.get('_store',False)==True:
                        ll=['\x02'.join([kk,v]) for kk,v in dReCall.iteritems() if kk[0]!='_']
                        ll.sort()
                        self.__logDebug__('ll;%s'%(self.__logFmt__(ll)))
                        l.append('\x01'.join([k,'\x03'.join(ll)]))
                s='\x00'.join(l)
                self.__logDebug__(s)
                if self._sReCallPhrase is not None:
                    sPhrase=self.oCypInt.DecryptHex(self._sReCallPhrase)
                    o=vtSecAes(sPhrase)
                    #o=vcsCrypto(sPhrase)
                    sEnc=o.EncryptHex(s)
                    iL=len(sEnc)
                    l=['']
                    for iOfs in xrange(0,iL,32):
                        l.append(sEnc[iOfs:iOfs+32])
                    l.append('')
                    if len(l)>0:
                        self.SetCfgVal('\n'.join(l),['recall'])
        except:
            self.__logTB__()
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        return _(u'VIDARC Explorer'),getAboutDescription(),version

    def OnCbAddButton(self, event):
        event.Skip()

    def OnTrSrcTreeSelChanged(self, event):
        event.Skip()
    def ShutDown(self):
        try:
            bDone=True
            if self.oCryptoStore is not None:
                self.oCryptoStore.Save()
                self.oCryptoStore.Close()
                self.oCryptoStore.ShutDown()
                if self.oCryptoStore.IsShutDown()==False:
                    bDone=False
            for w in self.lExp:
                w.ShutDown()
            for w in self.lExp:
                if w.IsBusy():
                    return True
        except:
            self.__logTB__()
        return 0
