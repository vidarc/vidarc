#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchCmdPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vXmlNodeExplorerLaunchCmdPanel.py,v 1.6 2012/01/12 10:08:43 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.input.vtInputFloat import vtInputFloat
    from vidarc.tool.input.vtInputTextPopup import vtInputTextPopup
    from vidarc.tool.misc.vtmFileBrowser import vtmFileBrowser
    from vidarc.tool.misc.vtmDirBrowser import vtmDirBrowser
    
    import sys
    import binascii,tempfile,os
    #import cStringIO
    import wx.tools.img2img as img2img
    from wx import BITMAP_TYPE_PNG as BMP_PNG
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase

    from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmdArgDlg import vXmlNodeExplorerLaunchCmdArgDlg

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

lArtIDs = ['---','-->']
if 1:
    for s in vtArt.lNames:
        lArtIDs.append('vtArt.'+s[3:-6])
    for s in vtArtState.lNames:
        lArtIDs.append('vtArtState.'+s[3:-6])
if 0:
    for s in [ "wx.ART_ADD_BOOKMARK",
               "wx.ART_DEL_BOOKMARK",
               "wx.ART_HELP_SIDE_PANEL",
               "wx.ART_HELP_SETTINGS",
               "wx.ART_HELP_BOOK",
               "wx.ART_HELP_FOLDER",
               "wx.ART_HELP_PAGE",
               "wx.ART_GO_BACK",
               "wx.ART_GO_FORWARD",
               "wx.ART_GO_UP",
               "wx.ART_GO_DOWN",
               "wx.ART_GO_TO_PARENT",
               "wx.ART_GO_HOME",
               "wx.ART_FILE_OPEN",
               "wx.ART_FILE_SAVE",
               "wx.ART_FILE_SAVE_AS",
               "wx.ART_PRINT",
               "wx.ART_HELP",
               "wx.ART_TIP",
               "wx.ART_REPORT_VIEW",
               "wx.ART_LIST_VIEW",
               "wx.ART_NEW_DIR",
               "wx.ART_HARDDISK",
               "wx.ART_FLOPPY",
               "wx.ART_CDROM",
               "wx.ART_REMOVABLE",
               "wx.ART_FOLDER",
               "wx.ART_FOLDER_OPEN",
               "wx.ART_GO_DIR_UP",
               "wx.ART_EXECUTABLE_FILE",
               "wx.ART_NORMAL_FILE",
               "wx.ART_TICK_MARK",
               "wx.ART_CROSS_MARK",
               "wx.ART_ERROR",
               "wx.ART_QUESTION",
               "wx.ART_WARNING",
               "wx.ART_INFORMATION",
               "wx.ART_MISSING_IMAGE",
               "wx.ART_COPY",
               "wx.ART_CUT",
               "wx.ART_PASTE",
               "wx.ART_DELETE",
               "wx.ART_NEW",
               "wx.ART_UNDO",
               "wx.ART_REDO",
               "wx.ART_QUIT",
               "wx.ART_FIND",
               "wx.ART_FIND_AND_REPLACE",
               ]:
           lArtIDs.append(s)

class vXmlNodeExplorerLaunchCmdPanel(vtgPanel,vtXmlNodePanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append(('lblRg',           (0,0),(1,1),    {'name':'lblAppl','label':_(u'application')},None))
        lWid.append((vtmFileBrowser,    (0,1),(1,3),    {
                'name':'fbbApp','bMulti':False,
                'bPosixFN':True, 'bSave':False,
                'sDftFN':u'','sFN':u'','sMsg':_(u'choose file'),
                'sWildCard':u'executable files|*.exe|all files|*.*'},None))
        lWid.append(('lblRg',           (1,0),(1,1),    {'name':'lblExecDN','label':_(u'directory')},None))
        lWid.append((vtmDirBrowser,     (1,1),(1,3),    {
                'name':u'fbbExecDN','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose execute directory')},None))
        lWid.append(('lblRg',           (2,0),(1,1),    {'name':'lblArg','label':_(u'arguments')},None))
        lWid.append((vtInputTextPopup,  (2,1),(1,3),    {
                'name':u'txtArg',},None))
        lWid.append(('lblRg',           (3,0),(1,1),    {'name':'lblLaunch','label':_(u'launch')},None))
        lWid.append(('szBoxHor',        (3,1),(1,3),{'name':'bxsStart',},[
                (vtInputFloat,      1,4,    {
                    'name':u'txtDly','default':0.0,'fraction_width':1,
                    'integer_width':4,'maximum':3600,'minimum':0.0},None),
                ('tgBmpLbl',    1,4,{'name':'tgSched',
                    'label':_(u'batch'),'bitmap':vtArt.getBitmap(vtArt.Build),},None),
                ('tgBmpLbl',    1,4,{'name':'tgAuto',
                    'label':_(u'auto'),'bitmap':vtArt.getBitmap(vtArt.AirPlane)},None),
                ('lblRg',       1,0,{'name':'lbl43','label':''},None),
                ]))
        lWid.append(('lblRg',           (4,0),(1,1),    {'name':'lblOpt','label':_(u'options')},None))
        lWid.append(('szBoxHor',        (4,1),(1,3),{'name':'bxsOptions',},[
                ('tgBmpLbl',    1,4,{'name':'tgShell',
                    'label':_(u'shell'),'bitmap':vtArt.getBitmap(vtArt.Shell),},None),
                ('tgBmpLbl',    1,4,{'name':'tgDetach',
                    'label':_(u'detach'),'bitmap':vtArt.getBitmap(vtArt.NetDisConnected)},None),
                ('tgBmpLbl',    1,4,{'name':'tgWatch',
                    'label':_(u'watch'),'bitmap':vtArt.getBitmap(vtArt.Eye),},None),
                ('tgBmpLbl',    1,0,{'name':'tgSave',
                    'label':_(u'save'),'bitmap':vtArt.getBitmap(vtArt.Save),},None),
                ]))
        lWid.append(('lblRg',           (5,0),(1,1),    {'name':'lblSave','label':_(u'save')},None))
        lWid.append((vtmDirBrowser,     (5,1),(1,3),    {
                'name':u'fbbSaveDN','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose save directory')},None))
        lWid.append(('lblRg',           (6,0),(1,1),    {'name':'lblClose','label':_(u'close')},None))
        lWid.append(('szBoxHor',        (6,1),(1,3),{'name':'bxsClose',},[
                (vtInputFloat,      1,4,    {
                    'name':u'txtDlyCls','default':0.0,'fraction_width':1,
                    'integer_width':4,'maximum':3600,'minimum':-1.0},None),
                ('lblRg',       1,4,{'name':'lbl61','label':''},None),
                ('lblRg',       1,4,{'name':'lbl62','label':''},None),
                ('lblRg',       1,0,{'name':'lbl63','label':''},None),
                ]))
        lWid.append(('lblRg',           (7,0),(1,1),    {'name':'lblImage','label':_(u'image')},None))
        lWid.append(('szBoxHor',        (7,1),(1,3),{'name':'bxsImage',},[
                ('chc',         2,4,{'name':'chcImage','choices':lArtIDs},
                    {'choice':self.OnChcImageChoice}),
                ('lblBmp',      1,4,{'name':'lbbImage',
                    'bitmap':vtArt.getBitmap(vtArt.Build)},None),
                #('cbBmpLbl',    1,4,{'name':'cbImageBrowse',
                #    'label':_(u'browse'),'bitmap':vtArt.getBitmap(vtArt.Drop)},
                #    {'btn':self.OnCbImageBrowseButton}),
                ('lblRg',       1,0,{'name':'lbl73','label':''},None),
                ]))
        
        vtgPanel.__initCtrl__(self,lWid=lWid)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        return None,[(1,1),(3,1)]

    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        dlg=vXmlNodeExplorerLaunchCmdArgDlg(self.txtArg)
        try:
            dlg.SetPossible(self.objRegNode.GetArgDict())
        except:
            self.__logTB__()
        self.txtArg.SetPopupWin(dlg)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.fbbApp.SetValue('')
            self.fbbExecDN.SetValue('')
            self.txtArg.SetValue('')
            self.txtDly.SetValue(0.0)
            self.txtDlyCls.SetValue(0.0)
            
            self.tgShell.SetValue(False)
            self.tgAuto.SetValue(False)
            self.tgSched.SetValue(False)
            self.tgWatch.SetValue(True)
            #self.tgDetach.SetValue(True)
            self.tgSave.SetValue(False)
            self.fbbSaveDN.SetValue('')
        except:
            self.__logTB__()
    def SetNetDocs(self,d):
        #if d.has_key('vHum'):
        #    dd=d['vHum']
        # add code here
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            # add code here
            self.fbbApp.SetValue(self.objRegNode.GetApp(node))
            self.fbbExecDN.SetValue(self.objRegNode.GetDN(node))
            self.fbbSaveDN.SetValue(self.objRegNode.GetSaveDN(node))
            self.txtArg.SetValue(self.objRegNode.GetArg(node))
            self.txtDly.SetValue(self.objRegNode.GetDlyVal(node))
            self.txtDlyCls.SetValue(self.objRegNode.GetDlyClsVal(node))
            sImg=self.objRegNode.GetImg(node)
            
            if sImg is None:
                sImg='---'
            elif sImg=='':
                sImg='---'
            self.chcImage.SetStringSelection(sImg)
            
            iVal=self.objRegNode.GetShellVal(node)
            if iVal>0:
                self.tgShell.SetValue(True)
            else:
                self.tgShell.SetValue(False)
            iVal=self.objRegNode.GetSaveVal(node)
            if iVal>0:
                self.tgSave.SetValue(True)
            else:
                self.tgSave.SetValue(False)
            iVal=self.objRegNode.GetWatchVal(node)
            if iVal>0:
                self.tgWatch.SetValue(True)
            else:
                self.tgWatch.SetValue(False)
            iVal=self.objRegNode.GetDetachVal(node)
            if iVal>0:
                self.tgDetach.SetValue(True)
            else:
                self.tgDetach.SetValue(False)
            iVal=self.objRegNode.GetAutoVal(node)
            if iVal>0:
                self.tgAuto.SetValue(True)
            else:
                self.tgAuto.SetValue(False)
            iVal=self.objRegNode.GetSchedVal(node)
            if iVal>0:
                self.tgSched.SetValue(True)
            else:
                self.tgSched.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def __GetNode__(self,node):
        try:
            self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetDN(node,self.fbbExecDN.GetValue())
            self.objRegNode.SetSaveDN(node,self.fbbSaveDN.GetValue())
            self.objRegNode.SetApp(node,self.fbbApp.GetValue())
            self.objRegNode.SetArg(node,self.txtArg.GetValue())
            if self.tgShell.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetShell(node,sVal)
            if self.tgWatch.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetWatch(node,sVal)
            if self.tgDetach.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetDetach(node,sVal)
            if self.tgSave.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetSave(node,sVal)
            if self.tgAuto.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetAuto(node,sVal)
            if self.tgSched.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetSched(node,sVal)
            self.objRegNode.SetDly(node,unicode(self.txtDly.GetValue()))
            self.objRegNode.SetDlyCls(node,unicode(self.txtDlyCls.GetValue()))
            sVal=self.chcImage.GetStringSelection()
            if sVal.startswith('vtArt.'):
                pass
            elif sVal.startswith('vtArt.'):
                pass
            else:
                sVal=''
            self.objRegNode.SetImg(node,sVal)
        except:
            self.__logTB__()
        #vtXmlNodePanel.__GetNode__(self,node)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbApp.Close()
        self.fbbExecDN.Close()
        self.fbbSaveDN.Close()
        self.txtArg.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbApp.Cancel()
        self.fbbExecDN.Cancel()
        self.fbbSaveDN.Cancel()
        self.txtArg.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def OnChcImageChoice(self, evt):
        if VERBOSE>0:
            self.__logDebug__(''%())
        evt.Skip()
        try:
            if self.IsMainThread()==False:
                return
            if self.chcImage.GetSelection()==0:
                self.lbbImage.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
                self.lbbImage.Refresh()
                return
            elif self.chcImage.GetSelection()==1:
                return
            s=self.chcImage.GetStringSelection()
            self.sImg=s
            if s.startswith('wx.'):
                bmp = wx.ArtProvider_GetBitmap(eval(s), eval('wx.ART_TOOLBAR'), (16,16))
            elif s.startswith('vtArt.'):
                bmp=vtArt.getBitmap(eval(s))
            elif s.startswith('vtArtState.'):
                bmp=vtArtState.getBitmap(eval(s))
            self.lbbImage.SetBitmapLabel(bmp)
            self.lbbImage.Refresh()
            #self.cbImageBrowse.SetBitmapLabel(bmp)
            #self.cbImageBrowse.Refresh()
        except:
            self.__logTB__()
    def doAddFile(self,iOfs,iCnt,sFN):
        try:
            self.__logDebug__('iOfs:%d;iCnt:%d;sFN:%s'%(iOfs,iCnt,sFN))
            compressed = 0
            maskClr = None
            tfname = tempfile.mktemp()
            ok, msg = img2img.convert(sFN, maskClr, None, tfname, 
                            BMP_PNG, ".png")
            if not ok:
                self.__logError__('msg:%s'%(msg))
            data = open(tfname, "rb").read()
            sImg=binascii.hexlify(data)
            #self.oCmd.img=sImg
            os.unlink(tfname)
        except:
            self.__logTB__()
    def OnCbImageBrowseButton(self, evt):
        evt.Skip()
        try:
            if self.IsMainThread()==False:
                return
            self.ChooseFile(self.OPEN,_(u'Import Image'),
                    u'PNG files (*.png)|*.png',
                    None,None,self.doAddFile)
        except:
            self.__logTB__()
