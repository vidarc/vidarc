#----------------------------------------------------------------------------
# Name:         vXmlExplorerNetSim.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vXmlExplorerNetSim.py,v 1.1 2010/01/17 00:36:41 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
#from vidarc.tool.vtThread import vtThreadWX
from vidarc.tool.xml.vtXmlDomNetSim import vtXmlDomNetSim
from vidarc.tool.xml.vtXmlDomNetSimWX import vtXmlDomNetSimWX
from vidarc.vTools.vExplorer.vXmlExplorer import vXmlExplorer

class vXmlExplorerNetSim(vXmlExplorer,vtXmlDomNetSim):
    def __init__(self,appl,fn='vExplorer.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        #self.thdProc=vtThreadWX(None,bPost=False,
        #        origin=u':'.join(['vXmlExplorerNetSim',u'thdProc']))
        vtXmlDomNetSim.__init__(self,appl,pos,size,synch=synch,verbose=verbose)
        vXmlExplorer.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                    verbose=verbose,audit_trail=audit_trail)
        vXmlExplorer.SetFN(self,fn)

class vXmlExplorerNetSimWX(vXmlExplorer,vtXmlDomNetSimWX):
    def __init__(self,parent,appl,fn='vExplorer.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
        vtXmlDomNetSimWX.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlExplorer.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                    verbose=verbose,audit_trail=audit_trail)
        vXmlExplorer.SetFN(self,fn)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
        vXmlExplorer.__del__(self)
        vtXmlDomNetSimWX.__del__(self)
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vtXmlDomNetSimWX.New(self)
        vXmlExplorer.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        sFN=vtXmlDomNetSimWX.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlExplorer.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if self.__isLogInfo__():
            self.__logInfo__('file:%s;slient:%d;thread:%d'%(repr(fn),silent,bThread))
        if fn is None:
            sFN=vtXmlDomNetSimWX.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlExplorer.New(self)
            return -2
        #self.StopConsumer()
        #self.WaitConsumerNotRunning(bThread=bThread)
        #self.ClearConsumer(bThread=bThread)
        if bThread:
            self.silent=silent
            iRet=vXmlExplorer.Open(self,fn,True)
        else:
            iRet=vXmlExplorer.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        #sFN=vtXmlDomNetSimWX.GetFN(self)
        #if sFN is not None:
        #    if fn is None:
        #        fn=sFN
        vtXmlDomNetSimWX.SaveApplDocs(self,encoding=encoding)
        return vXmlExplorer.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlExplorer.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        idOld=self.getKey(node)
        vXmlExplorer.addNode(self,par,node)
        idNew=self.getKey(node)
        kkwargs=kwargs.copy()
        kkwargs['__idOld__']=idOld
        kkwargs['__idNew__']=idNew
        vtXmlDomNetSimWX.addNode(self,par,node,func,**kkwargs)
    def delNode(self,node):
        vtXmlDomNetSimWX.delNode(self,node)
        vXmlExplorer.delNode(self,node)
    def IsRunning(self):
        if vtXmlDomNetSimWX.__isRunning__(self):
            return True
        if vXmlExplorer.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vtXmlDomNetSimWX.__stop__(self)
        vXmlExplorer.__stop__(self)

