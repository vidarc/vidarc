#----------------------------------------------------------------------------
# Name:         vExplorerSynchResultDlg
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vExplorerSynchResultDlg.py,v 1.1 2010/05/21 22:58:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import os.path
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.InOut.listFiles as listFiles
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerSynchResultDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('lblCt',       (0,1),(1,1),{'name':'lblSrc1','label':_(u'1')},None),
            ('lblCt',       (0,2),(1,1),{'name':'lblSrc2','label':_(u'2')},None),
            ('lblRg',       (1,0),(1,1),{'name':'lblBaseDN','label':_(u'base directory')},None),
            ('txtRdWrp',    (1,1),(1,1),{'name':'txtBaseDN1','value':u''},None),
            ('txtRdWrp',    (1,2),(1,1),{'name':'txtBaseDN2','value':u''},None),
            ('lblRg',       (2,0),(1,1),{'name':'lblDN','label':_(u'directory')},None),
            ('txtRdWrp',    (2,1),(1,1),{'name':'txtDN1','value':u''},None),
            ('txtRdWrp',    (2,2),(1,1),{'name':'txtDN2','value':u''},None),
            ('lblRg',       (3,0),(1,1),{'name':'lblFN','label':_(u'filename')},None),
            ('txtRd',       (3,1),(1,1),{'name':'txtFN1','value':u''},None),
            ('txtRd',       (3,2),(1,1),{'name':'txtFN2','value':u''},None),
            ('lblRg',       (4,0),(1,1),{'name':'lblSize','label':_(u'size')},None),
            ('txtRd',       (4,1),(1,1),{'name':'txtSize1','value':u''},None),
            ('txtRd',       (4,2),(1,1),{'name':'txtSize2','value':u''},None),
            ('lblRg',       (5,0),(1,1),{'name':'lblMD5','label':_(u'MD5')},None),
            ('txtRd',       (5,1),(1,1),{'name':'txtMD51','value':u''},None),
            ('txtRd',       (5,2),(1,1),{'name':'txtMD52','value':u''},None),
            ('lblRg',       (6,0),(1,1),{'name':'lblCreate','label':_(u'create time')},None),
            ('txtRd',       (6,1),(1,1),{'name':'txtCreate1','value':u''},None),
            ('txtRd',       (6,2),(1,1),{'name':'txtCreate2','value':u''},None),
            ('lblRg',       (7,0),(1,1),{'name':'lblMod','label':_(u'modification time')},None),
            ('txtRd',       (7,1),(1,1),{'name':'txtMod1','value':u''},None),
            ('txtRd',       (7,2),(1,1),{'name':'txtMod2','value':u''},None),
            ('lblRg',       (8,0),(1,1),{'name':'lblAccess','label':_(u'access time')},None),
            ('txtRd',       (8,1),(1,1),{'name':'txtAccess1','value':u''},None),
            ('txtRd',       (8,2),(1,1),{'name':'txtAccess2','value':u''},None),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExplorerSynchResult',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='dlg',
                bmp=bmp,
                title=title or _(u'VIDARC Explorer Synch Result'),
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pnExplorerSynchResult',
                lGrowCol=[(1,1),(2,1)],lGrowRow=[(1,1),(2,1)],
                bEnMark=False,bEnMod=False,lWid=lWid)
    def SetSrc1(self,f,baseDN):
        if f is None:
            self.txtBaseDN1.SetValue('')
            self.txtDN1.SetValue('')
            self.txtMD51.SetValue('')
            self.txtCreate1.SetValue('')
            self.txtMod1.SetValue('')
            self.txtAccess1.SetValue('')
            self.txtFN1.SetValue('')
            self.txtSize1.SetValue('')
            return
        self.txtBaseDN1.SetValue(baseDN)
        self.txtBaseDN1.SetInsertionPointEnd()
        dn,fn=os.path.split(f.fullname)
        self.txtDN1.SetValue(dn)
        self.txtDN1.SetInsertionPointEnd()
        #self.txtFN1.SetValue(f.name)
        self.txtFN1.SetValue(fn)
        self.txtSize1.SetValue(listFiles.getSizeStr(f.size,2))
        self.txtMD51.SetValue(f.md5)
        self.txtCreate1.SetValue(listFiles.ftime2str(f.stat_access))
        self.txtMod1.SetValue(listFiles.ftime2str(f.mod_access))
        self.txtAccess1.SetValue(listFiles.ftime2str(f.last_access))
    def SetSrc2(self,f,baseDN):
        if f is None:
            self.txtBaseDN2.SetValue('')
            self.txtDN2.SetValue('')
            self.txtMD52.SetValue('')
            self.txtCreate2.SetValue('')
            self.txtMod2.SetValue('')
            self.txtAccess2.SetValue('')
            self.txtFN2.SetValue('')
            self.txtSize2.SetValue('')
            return
        self.txtBaseDN2.SetValue(baseDN)
        self.txtBaseDN2.SetInsertionPointEnd()
        dn,fn=os.path.split(f.fullname)
        self.txtDN2.SetValue(dn)
        self.txtDN2.SetInsertionPointEnd()
        #self.txtFN2.SetValue(f.name)
        self.txtFN2.SetValue(fn)
        self.txtSize2.SetValue(listFiles.getSizeStr(f.size,2))
        self.txtMD52.SetValue(f.md5)
        self.txtCreate2.SetValue(listFiles.ftime2str(f.stat_access))
        self.txtMod2.SetValue(listFiles.ftime2str(f.mod_access))
        self.txtAccess2.SetValue(listFiles.ftime2str(f.last_access))
