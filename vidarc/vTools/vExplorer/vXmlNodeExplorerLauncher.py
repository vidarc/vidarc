#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLauncher.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100222
# CVS-ID:       $Id: vXmlNodeExplorerLauncher.py,v 1.1 2010/05/02 20:33:35 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag

try:
    if vcCust.is2Import(__name__):
    #    from vXmlNodeExplorerSrcPanel import vXmlNodeExplorerSrcPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
    #    vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerLauncher(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerLauncher'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer launcher')
    # ---------------------------------------------------------
    # specific
    def GetSrc(self,node):
        return self.Get(node,'src')
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsRequired(self):
        "node has to be present"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xdaIDAT8\x8d\x95\x90=h\x13a\x18\xc7\x7f\xef\x05\xb2\xfb\xd1)\xed\
\x1d\x013\x94\x12\xec\xd2&`\xcf\xbcD\x11tu\x14'7\x1d\x1c\x05Ex\x0f\xa1\xe8*d\
Rp\x91:\x88\x06\x17\x051\xf1\xa5w\x0e]\x15\\\x82=zrtJ\xe3\x18\x1b\xc8\xe3pi\
\x9a\xa4~\xd4?\xbc\xc3\xcb\xf3\xffz\x1e\x94\x93\xe3\xb8\x0f\xac\x80\x91|>/\
\x9e\xe7\x89rr\xfc\x87\xd8\xc82H\xb5R\x95j\xa5*\xc0\xf1\r\x96\xd1\xf2\x15\
\xa4\xa7\xb5\xbc\xca\xe7\x05\x100\xa2\x9c\x1c\x0e\xff\xc0\xd9\xa1/\x1bX\x8aZ\
\xd3\x07\xbc\xfd}J\x00h\x80\xbf\x1b\xc8\xb0%w\xb0\x9c\x18\x89\xbb\xd6\xf2m$V\
\xce\x05\x05\xfcy\x05\xb0\xf2\x02dWk\xe9i-\xbb o@n\x83\x80\x9517#\x1fEe\xe8\
\xcb\xbb\xac%=k\xf9\x02l\x01\xeb\xd8\xc3\xf4\xc9\x06`\xc5\xac\xac\x8aYY\x95\
\x12H\xb3P\x90\xa6\xd6\x12\x83|\x00\xb9;\x9b<\xd9@\x86-i\xa2)\x01O\x00](\xd0\
\x01\xfai\xca\x02\xf0\x12x;\x9b|P 3\xb8/\xd9\xd7r\xb5\xbcGe\xafK\x9a\xa60\
\xaa\xbd\xe5\xe4\xa6\x84\xfe\xb9\xf72eppq\xbdd(\xfd\xe8\xf0\xba\xdf\xe7d\xb7\
\xcb\x95\x11\xe9\xb1\x93S\xeeBG\xce\xaf\xb5q\xddxl\x94$\xc5\xc3\x15\xac\xd1\
\xd4nf\x83\xd3\x8b\xa7(\x97\xcb<\xb5\x96\x00\xd8\xf1[\xf8k\xed#\x87~\xbeq#3\
\xa8-\xfa\xf2\xb1aa\x0e\x02\x03\x833\xf7\x08\xa3\x90\x8b\xe1&\x9dk\xd7q]wJ\
\x18Fu\xc2O\x97\x14\x80\x02+\xb6\xa1\xa9-A\xd0\x00\xef\xf23\\\xd7%\x08\x02<\
\xd7\x1b\x8b\x93\xa4\xc8fT'\xf9^\x9a\xba\x87\x02#\xe6\x96\x819\xc3|a\x9ex;f\
\x16\x93\x89\xb3p\x94\xf3@\xb5?\xb7\x18\xfc\x1c\x10o\xc7\x199\xaa\xb3\xfe\
\xe8\xa1\n\xa3:\x00;I\xf1wZ\x00~\x01\x98\xb1\xc7n\x96\x97\xce|\x00\x00\x00\
\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {}
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    #def GetPanelClass(self):
    #    if GUI:
    #        return vXmlNodeExplorerSrcPanel
    #    else:
    #        return None
