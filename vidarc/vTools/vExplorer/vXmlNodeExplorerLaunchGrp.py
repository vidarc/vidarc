#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchGrp.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100222
# CVS-ID:       $Id: vXmlNodeExplorerLaunchGrp.py,v 1.3 2012/01/15 10:19:39 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerLaunchGrpPanel import vXmlNodeExplorerLaunchGrpPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerLaunchGrp(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerLaunchGrp'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer launch group')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=1):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    
    def __procNodeExec__(self,n,lNodes):
        try:
            sTag=self.doc.getTagName(n)
            if sTag!=self.tagName:
                o=self.doc.GetRegByNode(n)
                if o is not None:
                    if hasattr(o,'GetExec'):
                        lNodes.append(self.doc.getKeyNum(n))
        except:
            pass
        return 0
    def GetExec(self,node,sFN=None):
        #sTag=self.GetTag(node)
        lNodes=[]
        self.doc.procChildsKeys(node,self.__procNodeExec__,lNodes)
        sTag=self.GetTag(node)
        return sTag,lNodes,None,None
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['appl_truecrypt','root','pwd']
        else:
            return sTag,sFN,'"'+sApp+'" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['root','pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /d %s /s /q',\
                    ['appl_truecrypt','root']
        else:
            return sTag,sFN,'"'+sApp+' /d %s /s /q',\
                    ['root']
    def GetApplName(self):
        return 'appl_truecrypt'
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02]IDAT8\x8d\xa5\x93\xbdkSa\x14\xc6\x7f\xef\xfdHn\xccM\x93\xde\xdc\xb6\
\xa4\xad\xd5JQ\x04)(R\xc8\xe0\xdcI\x07\x07uq\xd6A\x87\x0e\x82\x8b\xe0"\xb8\
\xe8$\xfd\x07\x14Qp\x13\xa4\xe0\xe6Rpp\xb0\x16l-\x05\tmJ\xdb\xc4&\xb9\xc9\
\xfd|\xef\xebP\xdaZl\x07\xf1\xac\xe7\x9c\x87s\x9e\x0f!4\x9d\xff)\xe3\xb8\xc6\
\xfc\x9b\xa7\xca\xdb^\xa3\xd1h\xa0)\xc9\x8d\xc7o\xc5Qs\xe2\xcf\x0b\xde?\xbb\
\xa3\xc20@\xa5)\x05\xc7e\xd061\xec\x01\xba\x91D\x86!\xf5\xda\nB\xd3\xb0K\x0e\
\xce\xe89\xa6\xae\xdd\x15\xe2\xd5\xa3\x9b*\x97\xcfS.\x0f0<2F\x9a\x84hF\x16\
\xb3P\xa6\xaf\xe4B\xd2%\xf6;\xa8\xa8\x8b.4B\xdfci\xf5\'a\xd0\xa3\xf9\xab\x85\
\xa6\x80+\xd5*\xc5\x9c\xc6\xe7\xf9Ot\xb4\x02\x13S\xd3\xb8\x83#\x90t\x91~\x8b\
\xb8\xbd\xc5\xea\xf2w\xbe..\xb2\xb4\xf8\x85\xf1\xbe\x88\tG\x1dp\x10\xfa\x1e\
\x8e;\xc4\xf4\xc8(=\x7f\x9b\x0f/\x9f\xd3\xef\x0e\xa3\xa4\x8f\x88z\x00\xb8\
\xa5Qr\x99\x1d\x9am\x93^\xe8\xa1)\t\x80\xb6\xc7\x8c\x86\xa2\xe7\xb7\x89;)\
\x97\xcf\x9f\xa2^[\xe1\xec\xf8i,1I>;FJ\x8bn/\xc66\x122\x19\x1d\xc3\xd8\xe5\
\xceP\xc0\xf2\xf22\x96e\xa1\xeb:\xc3\x95a\xba\xed-\x02\xdf\xc7\xf3C\x8aC\rLC\
\x019\x12/\x06,\x90\xf1>\xf1\x1a\x80\xe3\x94\x08\x82\x00)%\x0b\xdf\x16\xc8\
\xe5O\xa0\x80\x8ca\x91\xcdd!:XPIpHF\r\xa0\xe2\x96(\x97lR\x19\x92\xcfeH\xe2\
\x08\x00\xdd\x90\xa8T!26q\'\xdd\xd5\xdd\xb0\x0e\x1bI\x00a\x10R\xb2\xb3\x94\
\xec,2\x91\xc4Q\x02\x80Lt\x92P\'\x8e\x13\xac\xac$\x89v/\xd0u\x13)C\xc4\x9e\n\
\xf5F\x9b\x81\x82I\x9a\xc6\x88T\xee\xa3\xc7]\x93(\xd0\x11zB\x1c\xea\x08\x01J\
\xd3iu<v\xda\xbb\xaf\x18\xb7\x9f\xbc\x13\x1f_\xdcW\xb5\xb5uF\x07\x8b\x0c\x14\
\xcc}\x804\x85\xd871\xb2\t\xed\x9eG\xab\xd7\xa4\xbe\xbdC\xbfS\xe6\xea\x83\
\xd7\xe2/+\xcf\xcd\xce\xa8\xf6\xe6:\'\xdd\x1c?\xea\x1e\x93\x17.\xb1\xb1\xbe\
\xceVs\x83\xfe\xfe"\xe53\x17\xa9^\xbfw(\x13\xe2\xa84\xce\xcd\xce\xa8\xcd\xb5\
\x1a\x95J\x05\xdb\x1d\xa1z\xeb\xe1\x91A:\x16\xe0_\xea7\xcbJ\x02M)\x12\x16\
\xd0\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerLaunchGrpPanel
        else:
            return None
