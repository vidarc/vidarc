#Boa:Frame:vExplorerMainFrame
#----------------------------------------------------------------------------
# Name:         vExplorerMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2010116
# CVS-ID:       $Id: vExplorerMainFrame.py,v 1.10 2012/01/15 22:38:41 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.tool.gui.vtgFrame import vtgFrame
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    import vidarc.config.vcCust as vcCust
    from vidarc.config.vcCfg import vcCfg
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    from vidarc.vTools.vExplorer.vExplorerMainPanel import vExplorerMainPanel
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

def getPluginImage():
    return imgExplorer.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgExplorer.getApplicationBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vEXplorerMain'):
    return vExplorerMainFrame(parent,id,pos,size,style,name)

[wxID_VEXPLORERMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vExplorerMainFrame(vtgFrame,vMDIFrame,vcCfg):
    STATUS_CLK_POS=4
    STATUS_LOG_POS=3
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VEXPLORERMAINFRAME,
              name=u'vExplorerMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 520), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vExplorer')
        self.SetClientSize(wx.Size(492, 493))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        self._bLayout=False
        vtLog.vtLogOriginWX.__init__(self)
        vMDIFrame.__init__(self,u'vExplorer',iNotifyTime=250)
        #wx.CallAfter(self.Bind,wx.EVT_ACTIVATE, self.OnMainActivate)
        self.bFirstShown=False
        #vCfg.__init__(self)
        #self.dCfg.update({'x':10,'y':10,'width':500,'height':500})
        vcCfg.__init__(self,['vExplorerGui'],
                {'x':10,'y':10,'width':500,'height':500})
        #self.setCfgDft()
        
        try:
            self.mnAction = wx.Menu(title=u'')
            self.mnbMain.Insert(2,menu=self.mnAction, title=_(u'Ac&tion'))
            
            self.pn=vExplorerMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'pnMain')
            #self.GetCmdLineOpt('-h','--help',None)
            self.setCfgDoc(self.pn.xdCfg)
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            #trMain=self.pn.GetTreeMain()
            self.netDocMain.BindEvent('openStart',self.OnOpenStart)
            #EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            #EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            #EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            if self.netMaster is not None:
                self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            #EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
            
            self.AddMenus(self , self.trMain , None,#self.netMaster,
                None , self.mnView , self.mnTools , self.mnAnalyse)

            sCfgFN=vcCust.getCmdLineOpt('-c','--config','vExplorerCfg.xml')
            self.OpenCfgFile(sCfgFN)
            #self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.dCfg.update(self._getCfgData(['vExplorerGui']))
        #self.pn.SetCfgData(self._setCfgData,['vExplorerGui'],self.dCfg)
        #self.__setCfg__()
        #wx.CallAfter(self.__setCfg__)
        #if pos!=wx.DefaultPosition:
        #    self.Move(pos)
        #if size!=wx.DefaultSize:
        #    self.SetSize(size)

    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s="VIDARC "+self.appl
        fn=self.netDocMain.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            try:
                if fn.startswith(vcCust.USR_LOCAL_DN):
                    i=len(vcCust.USR_LOCAL_DN)
                    fn=u''.join([u'...',fn[i:]])
            except:
                pass
            s=s+" ("+fn
            if self.netDocMain.IsModified():
            #if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        if sOldTitle!=s:
            self.SetTitle(s)
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def OpenPlcCfg(self,fn):
        self.pn.OpenPlcCfg(fn)
    def OpenCfgFile(self,fn=None):
        try:
            self.pn.OpenCfgFile(fn)
            #self.buildCfgDict()
        except:
            self.__logTB__()
        #self.dCfg.update(self._getCfgData(['vExplorerGui']))
        #self.pn.SetCfgData(self._setCfgData,['vExplorerGui'],self.dCfg)
        #self.__setCfg__()
    def SaveCfgFile(self,fn=None):
        try:
            self.pn.GetCfgData()
            self.setCfgData()
            #if self.pn.xdCfg is not None:
            #    self._setCfgData(['vExplorerGui'],self.dCfg)
            self.pn.xdCfg.Save(fn)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetCfgData(self):
        self.__logDebug__(''%())
        try:
            #print self.dCfg
            iX=vcCfg.getCfgVal(self,['x'],funcConv=int)
            iY=vcCfg.getCfgVal(self,['y'],funcConv=int)
            iWidth=vcCfg.getCfgVal(self,['width'],funcConv=int)
            iHeight=vcCfg.getCfgVal(self,['height'],funcConv=int)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            vtLog.vtLngTB(self.GetName())
    def GetCfgData(self):
        self.__logDebug__(''%())
        try:
            pos=self.GetPositionTuple()
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            #iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            vcCfg.setCfgVal(self,iX,['x'])
            vcCfg.setCfgVal(self,iY,['y'])
            #self.dCfg['x']=str(iX)
            #self.dCfg['y']=str(iY)
            sz=self.GetSize()
            iW=sz[0]
            iH=sz[1]
            vcCfg.setCfgVal(self,iW,['width'])
            vcCfg.setCfgVal(self,iH,['height'])
            #iWidth-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            if self.IsMaximized():
                #self.dCfg['maximized']='1'
                vcCfg.setCfgVal(self,'1',['maximized'])
            else:
                vcCfg.setCfgVal(self,'0',['maximized'])
                #self.dCfg['width']=str(iWidth)
                #self.dCfg['height']=str(iHeight)
                #self.dCfg['maximized']='0'
        except:
            self.__logTB__()
    def __setCfg__(self):
        self.__logCritical__(''%())
        return
        try:
            #print self.dCfg
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            #print iX,iY,iWidth,iHeight
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            vtLog.vtLngTB(self.GetName())
        self.pn.SetCfgData(self._setCfgData,['vExplorerGui'],self.dCfg)
    def _getCfgData(self,l):
        self.__logCritical__(''%())
        return
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        self.__logCritical__(''%())
        return
        
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def OnMasterShutDown(self,evt):
        pass
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            if self.IsShown():
                self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
        except:
            self.__logTB__()
    def Show(self,bFlag=True):
        try:
            wx.Frame.Show(self,bFlag)
            if self.bFirstShown==False:
                self.bFirstShown=True
                if self.pn is None:
                    return
                sReCall=self.pn.GetCfgVal(['recall'],fallback=None)
                if sReCall is not None:
                    if len(sReCall)>0:
                        self.CallBack(self.MenuLogin,bClr=True)
        except:
            self.__logTB__()
    def OnMainClose(self,event):
        try:
            self.SaveCfgFile()
            #self.pn.GetCfgData()
            #self.pn.MenuSaveFile(0)
            #if self.pn.xdCfg is not None:
            #    self._setCfgData(['vExplorerGui'],self.dCfg)
            #vMDIFrame.OnMainClose(self,event)
            if self.pn.ShutDown()>0:
                if event.CanVeto():
                    event.Veto()
                    self.CallBackDelayed(2,self.Close)
                    return
                else:
                    event.Skip()
            def closeXml(doc):
                try:
                    doc.ShutDown()
                    doc.Close()
                    doc.StopConsumer()
                except:
                    vtLog.vtLngTB(self.GetName())
            closeXml(self.pn.xdCfg)
            doc=self.pn.GetDocMain()
            #if doc is not None:
            #    closeXml(doc)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def SetFocus(self):
        wx.Frame.SetFocus(self)
