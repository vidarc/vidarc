#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerNetHost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100227
# CVS-ID:       $Id: vXmlNodeExplorerNetHost.py,v 1.2 2010/03/03 12:35:24 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.InOut.vtInOutNetTools as vtiNT

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerNetProtocolSmbPanel import vXmlNodeExplorerNetProtocolSmbPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerNetHost(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerNetHost'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'network host')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=1):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    
    def procMnt(self,sSrc,sMnt,t,thd=None):
        try:
            self.__logDebug__('lSrc:%s;sMnt:%s;thd:%r'%(sSrc,sMnt,thd))
            sUsr,sPwd=t
            return vtiNT.SambaMnt(sSrc,sMnt,sUsr,sPwd,thd=thd)
        except:
            self.__logTB__()
            return -1
    def procUnMnt(self,sMnt,thd=None):
        try:
            self.__logDebug__('sMnt:%s'%(sMnt))
            # perform several times due notify hook
            for iTry in xrange(120):
                ret=vtiNT.SambaUnMnt(sMnt,iForce=0,thd=thd)
                if ret>=0:
                    return ret
            return ret
        except:
            self.__logTB__()
            return -1
    def GetExec(self,node,sFN=None):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return sHost,sFN,(self.procMnt,(sFN,),{'thd':'self'}),['drive','usr_pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return sHost,sFN,(self.procUnMnt,(),{'thd':'self'}),['drive']
    def GetApplName(self):
        return '__method'
    # ---------------------------------------------------------
    # inheritance
    def GetData(self,node,dImg={},dPar={},**kwargs):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        try:
            self.__logDebug__('dPar:%s'%(self.__logFmt__(dPar)))
            lHdr=[(_(u'Name'),-1,None,1)]
            dSortDef={-1:{'iSort':2}}
            lData=[]
            dShare=vtiNT.getHostShareDict(dPar['host'])
            sTagName=self.GetTagName()
            keys=dShare.keys()
            keys.sort()
            #for k,dd in dShare.iteritems():
            for k in keys:
                if k is None:
                    continue
                dd=dShare[k]
                self.__logDebug__('k:%s;dd:%s'%(k,self.__logFmt__(dd)))
                sLbl=dd.get('name','???')
                l=[{0:sLbl,'__reg':'ExplorerNetProtocolSmb','mnt':k,'info':dd},[
                    (sLbl,self.__getImgFromDict__(dImg,'elem','ExplorerNetProtocolSmb')),
                    ]]
                lData.append(l)
            return (lData,lHdr,dSortDef,True)
        except:
            self.__logTB__()
        return (None,None,None,True)
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xafIDAT8\x8dm\x93[o\x1bU\x14\x85\xbf3W\'c;\xf6\xd8j\x8c5!ii!\xadDD\
\xd5\x90\x96\x9b\x04R%\xfe\x05/H\xfc\x97>\xf5\xa7\x00\x8f\x08^\xb8\x08\xb5R\
\x88\x88\x88\x10jK\x9b4\x08_\x18\xdb\xc9x<3g.\xe7\xf0`\x04E\xcdzY{K[k\xef\
\xad\xbd\x97\x10\x86\xc9E\xf8\xfe\xbb\x9f\xb5e9\xd8\xb6K\xdd3\xd9\xbe\xb1%.\
\xaa\x13\xfb\xfb?i0X[kqt\xf4\x14\xc7Y%K\x15e%Hs\x98e\x8a\xd5"\xe3\xda\x1b}66\
\xda\xbc\xbe\x1d\xfcOH<x\xf0P\'II8K\x89\x94\xc9\x8a\xbf\xc6Z\xaf\x83\xd3i\
\x91\xac4\xf9\xe3\xf8\x9c\xf5G\xa7\x04A\x8b$\x91\xa4i\xc6x8\xa5\xd3\xf5\xb8\
\xb5\xbb\x85\x95KxX\xf9\xd4\xdf\xde\xa6\xebA\xbb\x06\xae\t\xb2\x82$\x85r\xd5\
C)\xe8\xf5<\x9a\xcd\x0e\xa3Q\x86\x94\x1e\xbf\x1c\xcd\xb1\xec\x08\xeb\xf8\xe9\
\x88\xd5\xab\xaf\x10\xe5`\x1aPT`\x9bK\x8es\x88\xb1\x18\xcf\n\x9e<\t\x19\x8dr\
\x0e\x0e"\xaa*\xc1\xb22\x0c\xe3:\xd6\xce[\xd798+9M\xa1\xd2\x90\x14`\x88\xff\
\xe2H\x820j\x00\xd868\xce\x84~\xdf\'I4\xa6\xa9\xb1&a\x84\x91\xb9\x84\x1e\xc8\
\x12\\\x0b\x84\x00\xa5\x97y\x9c\x03\x95\x03(l[\xe08\x82\xc1`\x00\x80ij\x8c\
\xbb\x1f\xef\tf\x11\x93\xb9b\x18\xc3 \x86Q\x0c\xc3\x7f\xf8\xaf\x04\x86\xe5r\
\x02!4J\x15\xf8\xbeO\xb7\xdb\xc50\x14\x16@\x1a\xe7d\xd3\x98\xf3F\x13\xdb\\\
\xae\xa04\xb4\xaa\x9c\x8daHP\x9c\x036B@\xbd^\xc30\x04i\x1a\xa1Tw)p\xfb\xce5>\
?\x94\x8c\x044l\xb8\xed,h\x9f\x0c\x90\xbf\x8f\xd9\xdbkS\xd5\x14\xf3y\x81a\
\x80\x10\t\x93\xc9\x14!\xc04Y\n,b\xc9\x9bv\xc9\x1dwJ0\x1d\xf1\xfcqD\x18\x82\
\xd6&\xfd~\r\xa54gg9Q\x943\x9f\'\x04\x81\x87{e\x86\xe3\x80\xf9\xe5\x17\x87zs\
\xabAc\x91\xf1\xce\xab\x8an\xab@\x08\x88\xa2sl[\xe1\xba\n\xcb\xd2dY\x81\xe7\
\xb94\x1ag\xc8Lr\xf5\xd2\r>\xfchWX\xae[\xd1\xeb\xd5\xd9\xdc\xec\xb0X,\x98\
\xcdJ\x9aM\x89\xef\'H\xa91\x8c\x06\xdf~s\xc8\xbd\xfb\x9f\x88\x1f\x7f\xf8M\
\xfb\xed+\xbc\xf7\xc1\xee\xbf\xefl\x9d\x1c\x8f\xd9\xd9YGJ\x89\x949e\xa9\xb0m\
M\x10\x08\x9e=\x9b\xa3\x94\xe6\xf2k\xeb\x00\xbc\xfb\xfe\xf6K\x86\x12\xbf\x1e\
\x9d\xe8?\x07!~g\x85Z\xcdAJE\x18\xa6\x9c\x9e\xce\x00\x1bT\xce\xa7\x9f\xdd\
\xbd\xd0\x89\xcb\xd3\xbe`\xe7\xaf\xbf\xda\xd7\xadv\x03\xa5`<\x9c\x12\x04mn\
\xee\xbe\xdc\xf5E\xfc\r\xe3\xcc:\xc2\x99\x8f\x115\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerNetProtocolSmbPanel
        else:
            return None
    #def Build(self):
    #    self.__logDebug__(''%())
    #    self.doc.RegisterNetProtocol('smb',self)
