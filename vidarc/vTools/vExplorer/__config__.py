#----------------------------------------------------------------------------
# Name:         __config__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: __config__.py,v 1.13 2013/03/23 22:07:49 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------
VER_MAJOR        = 1      # The first three must match wxWidgets
VER_MINOR        = 0
VER_RELEASE      = 3
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.
VERSION          = u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE)

DESCRIPTION      = "VIDARC Explorer"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
MAINTAINER       = "Walter Obweger"
MAINTAINER_EMAIL = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "VIDARC license"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "MES,CRM,project management"

LONG_DESCRIPTION = """\

"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000/XP
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""


INST_PATH="vidarc/tools"
GROUP_BASE="VIDARC\Tools"

APP='vExplorer'
ICONS=[(1,'img/vExplorer01_32.ico')]
APP_PY='vExplorerAppl.py'
APP_INCL=['vidarc.vTools.vExplorer.vExplorerMainFrame']
APP_EXCL=[]
LANG=['en','de']

def getAppl():
    return [
        [0,'vExplorer',
            u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE),
            'vExplorerAppl.py',
            ICONS,
            None
        ],
        ]
