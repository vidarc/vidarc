#----------------------------------------------------------------------------
# Name:         vExplorerSynchFrame
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vExplorerSynchFrame.py,v 1.5 2010/06/14 10:07:17 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time,os,types,sys
    import wx
    
    #from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgFrame import vtgFrame
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.gui.vtgPopupButton import vtgPopupButton
    from vidarc.tool.misc.vtmTreeListCtrl import vtmTreeListCtrl
    from vidarc.tool.misc.vtmThreadCtrl import vtmThreadCtrl
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.vTools.vExplorer.imgExp as img
    
    from vidarc.tool.InOut.fnUtil import *
    import vidarc.tool.InOut.listFiles as listFiles
    from vidarc.tool.InOut.genFN import getCurDateTimeFN
    from vidarc.tool.InOut.listFilesXml import *
    from vidarc.tool.InOut.vtInOutComp import vtInOutCompDirEntry
    from vidarc.tool.InOut.vtInOutReadThread import *
    from test.tool.InOut.vtInOutFileTimes import setFileTimes
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    import vidarc.tool.misc.vtmTreeListCtrl
    from vidarc.tool.vtThread import vtThreadWX
    
    from vExplorerSynchResultDlg import vExplorerSynchResultDlg
    from vExplorerSynchLogDlg import vExplorerSynchLogDlg
    from vExplorerSynchCfgDlg import vExplorerSynchCfgDlg
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

ICON_ABORTED=None
ICON_RUNNING=None
ICON_STOPPED=None
ICON_ERROR=None

def getIconByName(s):
    global ICON_ABORTED
    global ICON_RUNNING
    global ICON_STOPPED
    global ICON_ERROR
    if ICON_ABORTED is None:
        ICON_ABORTED=vtArt.getIcon(img.getAbortedBitmap())
        ICON_RUNNING=vtArt.getIcon(img.getRunningBitmap())
        ICON_STOPPED=vtArt.getIcon(img.getStoppedBitmap())
        ICON_ERROR=vtArt.getIcon(img.getErrorBitmap())
        
    if s=='ICON_ABORTED':
        return ICON_ABORTED
    if s=='ICON_RUNNING':
        return ICON_RUNNING
    if s=='ICON_STOPPED':
        return ICON_STOPPED
    if s=='ICON_ERROR':
        return ICON_ERROR
    return ICON_ERROR

class vExplorerSynchFrame(vtgFrame):
    LST_CFGOBJ_MAP=[('chkCalcMD5','calc_md5'),
            ('chkCalcAlwaysMD5','calc_always_md5'),
            ('chkAccess','check_time_access'),
            ('chkMod','check_time_mod'),
            ('chkCreate','check_time_create'),
            ('chkAutoExpand','show_auto_expand'),
            ('chkFlat','show_flat'),
            ('chkSelProc','sel_processing'),
            ('chkShowEqual','show_equal'),
            ('tgTargetSrc1','target_src1'),
            ('tgTargetSrc2','target_src2'),
            ]
    ACT_UNDEF=0
    ACT_CP1TO2=10
    ACT_MV1TO2=11
    ACT_DEL1=12
    ACT_CP2TO1=20
    ACT_MV2TO1=21
    ACT_DEL2=22
    ACT_TARGET1=100
    ACT_TARGET2=200
    def __init__(self, parent,dPos=None,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None,zClose=None,zClsSleep=0.5,
                funcOpen=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('szBoxHor',(0,0),(1,1),{'name':'bxsSrc1',},[
                ('lblRg',   0,4,{'name':'lblSrc1','label':_(u'source 1')},None),
                ('txtRd',   1,0,{'name':'txtSrc1','value':u''},None),
                ]),
            ('cbBmp',   (0,1),(1,1),{'name':'cbReadSrc1',
                        'tip':_(u'read source 1'),
                        'bitmap':vtArt.getBitmap(vtArt.Measure)},
                        {'btn':self.OnCbReadSrc1Button}),
            ('cbBmp',   (0,2),(1,1),{'name':'cbClrSrc1',
                        'tip':_(u'clear source 1'),
                        'bitmap':vtArt.getBitmap(vtArt.Erase)},
                        {'btn':self.OnCbClrSrc1Button}),
            ('szBoxHor',(1,0),(1,1),{'name':'bxsSrc2',},[
                ('lblRg',   0,4,{'name':'lblSrc2','label':_(u'source 2')},None),
                ('txtRd',   1,0,{'name':'txtSrc2','value':u''},None),
                ]),
            ('cbBmp',   (1,1),(1,1),{'name':'cbReadSrc2',
                        'tip':_(u'read source 2'),
                        'bitmap':vtArt.getBitmap(vtArt.Measure)},
                        {'btn':self.OnCbReadSrc2Button}),
            ('cbBmp',   (1,2),(1,1),{'name':'cbClrSrc2',
                        'tip':_(u'clear source 2'),
                        'bitmap':vtArt.getBitmap(vtArt.Erase)},
                        {'btn':self.OnCbClrSrc2Button}),
            (vtmTreeListCtrl,(2,0),(1,1),{
                            'name':'trlstRes','bButtons':True,
                            },None),
            ('szBoxVert',   (2,1),(1,1),{'name':'bxsBtSrc1',},[
                (vtgPopupButton,   0,4,{'name':'cbLog','bmp':vtArt.getBitmap(vtArt.Log),'size':(32,32),
                                'tip':_(u'show log information'),},
                                None),
                ('tgBmp',   0,4,{'name':'tgTargetSrc1','bitmap':img.getSrc1Bitmap(),'size':(32,32),
                                'tip':_(u'target source 1 active'),},
                                {'btn':self.OnTgTarget1Button}),
                ('cbBmp',   0,4,{'name':'cbCpTo1','bitmap':img.getCp2To1Bitmap(),'size':(32,32),
                                'tip':_(u'copy source 2 to 1'),},
                                {'btn':self.OnCbCpTo1Button}),
                ('cbBmp',   0,4,{'name':'cbMvTo1','bitmap':img.getMv2To1Bitmap(),'size':(32,32),
                                'tip':_(u'move source 2 to 1'),},
                                {'btn':self.OnCbMvTo1Button}),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbDel1','bitmap':img.getDel1Bitmap(),'size':(32,32),
                                'tip':_(u'delete source 1'),},
                                {'btn':self.OnCbDel1Button}),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbOpenSrc1','bitmap':img.getOpenSrc1Bitmap(),'size':(32,32),
                                'tip':_(u'open source file 1'),},
                                {'btn':self.OnCbOpenSrc1Button}),
                (vtgPopupButton,   0,4,{'name':'cbRes','bmp':vtArt.getBitmap(vtArt.Down),'size':(32,32),
                                'tip':_(u'show detail file informations'),},
                                #{'btn':self.OnPopupButton}
                                None),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbClrCmp','bitmap':vtArt.getBitmap(vtArt.Erase),'size':(32,32),
                                'tip':_(u'clear compare'),},
                                {'btn':self.OnCbClrCmpButton}),
                ]),
            ('szBoxVert',   (2,2),(1,1),{'name':'bxsBtSrc2',},[
                (vtgPopupButton,   0,4,{'name':'cbCfg','bmp':vtArt.getBitmap(vtArt.Config),'size':(32,32),
                                'tip':_(u'show configuration'),},
                                None),
                ('tgBmp',   0,4,{'name':'tgTargetSrc2','bitmap':img.getSrc2Bitmap(),'size':(32,32),
                                'tip':_(u'target source 2 active'),},
                                {'btn':self.OnTgTargetSrc2Button}),
                ('cbBmp',   0,4,{'name':'cbCpTo2','bitmap':img.getCp1To2Bitmap(),'size':(32,32),
                                'tip':_(u'copy source 1 to 2'),},
                                {'btn':self.OnCbCpTo2Button}),
                ('cbBmp',   0,4,{'name':'cbMvTo2','bitmap':img.getMv1To2Bitmap(),'size':(32,32),
                                'tip':_(u'move source 1 to 2'),},
                                {'btn':self.OnCbMvTo2Button}),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbDel2','bitmap':img.getDel2Bitmap(),'size':(32,32),
                                'tip':_(u'delete source 2'),},
                                {'btn':self.OnCbDel2Button}),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbOpenSrc2','bitmap':img.getOpenSrc2Bitmap(),'size':(32,32),
                                'tip':_(u'open source file 2'),},
                                {'btn':self.OnCbOpenSrc2Button}),
                ('cbBmp',   0,4,{'name':'cbRemove','bitmap':vtArt.getBitmap(vtArt.Del),'size':(32,32),
                                'tip':_(u'remove selected tree item'),},
                                {'btn':self.OnCbRemoveButton}),
                (None,     (8,8),4,None,None),
                ('cbBmp',   0,4,{'name':'cbClrAll','bitmap':vtArt.getBitmap(vtArt.Clear),'size':(32,32),
                                'tip':_(u'clear all'),},
                                {'btn':self.OnCbClrAllButton}),
                ]),
            #        ]),
            (vtmThreadCtrl,(3,0),(1,1),{'name':'thdCtrl',},None),
            ]
        vtgFrame.__init__(self,parent,_(u'VIDARC Explorer Synchronise'),
                vtgPanel,name=name or 'dlgExpSynch',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                bmp=None or img.getPluginBitmap(),
                pnPos=None,pnSz=(300,400),pnStyle=None,
                pnName='pnExpSynch',
                lGrowCol=[(0,1)],lGrowRow=[(2,1)],
                bEnMark=False,bEnMod=False,
                lWid=lWid)
        self.pn.applName=_(u'VIDARC Explorer Synchronise')
        self.imgLst,self.dImg=self.trlstRes.CreateImageList([
                ('empty',   vtArt.getBitmap(vtArt.Invisible)),
                ('idxNrm',  img.getDirCloseBitmap()),
                ('idxSel',  img.getDirOpenBitmap()),
                ('ok',      vtArt.getBitmap(vtArt.Apply)),
                ('err',     vtArt.getBitmap(vtArt.Cancel)),
                ('qst',     vtArt.getBitmap(vtArt.Question)),
                ('wrn',     vtArt.getBitmap(vtArt.Warning)),
                ('file',    vtArt.getBitmap(vtArt.Element)),
                ('Cal',     vtArt.getBitmap(vtArt.Measure)),
                ('Cmp',     vtArt.getBitmap(vtArt.Report)),
                ('Cp2To1',  img.getCp2To1Bitmap()),
                ('Cp1To2',  img.getCp1To2Bitmap()),
                ('Mv2To1',  img.getMv2To1Bitmap()),
                ('Mv1To2',  img.getMv1To2Bitmap()),
                ('Src1',    img.getSrc1Bitmap()),
                ('Src2',    img.getSrc2Bitmap()),
                ('Srcs',    img.getSrcsBitmap()),
                ('Del1',    img.getDel1Bitmap()),
                ('Del2',    img.getDel2Bitmap()),
                ])
        self.trlstRes.SetImageList(self.imgLst)
        for s in [_(u'name'),_(u'size'),u'',u'',
                _(u'S'),_(u'M'),_(u'tA'),_(u'tM'),_(u'tC')]:
            self.trlstRes.AddColumn(s)
        self.trlstRes.SetColumnAlignment(1,-2)
        self.trlstRes.SetColumnAlignment(3,-2)
        self.trlstRes.SetColumnAlignment(4,-2)
        self.trlstRes.SetColumnAlignment(5,-2)
        self.trlstRes.SetColumnAlignment(6,-2)
        self.trlstRes.SetColumnAlignment(7,-2)
        self.trlstRes.SetColumnWidth(1,80)
        self.trlstRes.SetColumnWidth(2,24)
        self.trlstRes.SetColumnWidth(3,30)
        self.trlstRes.SetColumnWidth(4,30)
        self.trlstRes.SetColumnWidth(5,30)
        self.trlstRes.SetColumnWidth(6,30)
        self.trlstRes.SetColumnWidth(7,30)
        self.trlstRes.SetColumnWidth(8,30)
        #self.trlstRes.SetColumnWidth(9,30)
        self.trlstRes.SetStretchLst([(0,-1)])
        self.trlstRes.BindEvent('tr_item_selected',self.OnTrlstResTreeSelChanged)
        self.trlstRes.BindEvent('tr_item_expanding',self.OnTrlstResTreeItemExpanding)
        #self.lstPipes.SetColumnWidth(2,30)
        #self.lstPipes.SetStretchLst([(1,-1.0)])
        
        self.lDN=['','']
        self.tiSel=None
        self.tiResSel=None
        self.dirEntrySrc1=None
        self.dirEntrySrc2=None
        self.dirComp=None
        self.tCount1=(0,0,0)
        self.tCount2=(0,0,0)
        self.iTarget=0
        self.iAct=0
        self.iCount=0
        self.thdRead=vtInOutReadThread(self.trlstRes,True)
        self.thdRead.BindEvents(self.OnInOutReadProc,
                            self.OnInOutReadFin,
                            self.OnInOutReadAbort)
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        #self.trDirSnap.SetImageList(self.imgLst)
        self.dImg['Dir']=[self.dImg['idxNrm'],self.dImg['idxSel']]
        t=self.dImg['Dir']
        #self.trDirSnap.AddRoot('Root',t[0],t[1])
        
        
        self.dImgAct={
            self.ACT_CP2TO1:    self.dImg['Cp2To1'],
            self.ACT_CP1TO2:    self.dImg['Cp1To2'],
            self.ACT_MV2TO1:    self.dImg['Mv2To1'],
            self.ACT_MV1TO2:    self.dImg['Mv1To2'],
            self.ACT_DEL1:      self.dImg['Del1'],
            self.ACT_DEL2:      self.dImg['Del2'],
            }
        
        bmp=vtArt.getBitmap(vtArt.Build)
        self.dImg['proc']=self.imgLst.Add(bmp)
        
        self.pnRes=vExplorerSynchResultDlg(self)
        self.pnLog=vExplorerSynchLogDlg(self)
        self.pnCfg=vExplorerSynchCfgDlg(self)
        self.pnCfg.BindEvent('ok',self.OnCfgOk)
        self.pnCfg.BindEvent('cancel',self.OnCfgCancel)
        self.cbRes.SetPopupWid(self.pnRes)
        self.cbLog.SetPopupWid(self.pnLog)
        self.cbCfg.SetPopupWid(self.pnCfg)
        
        #self.pnLog=self.popLog.pn
        self.bBusy=False
        #self.widLogging=vtLog.GetPrintMsgWid(self)
        self.dlgFile=None
        self.cbClrSrc1.Enable(False)
        self.cbClrSrc2.Enable(False)
        self.cbClrCmp.Enable(False)
        self.enableResWid(self.trlstRes,None)
        self.funcOpen=funcOpen or self.__openFile__
        #self.spRec.SetValue(10)
    def SetValue(self,sDN1='',sDN2='',iRec=-1,bMD5=True,bMD5Always=False,
                    bAccess=False,bMod=True,bCreate=True,bAutoExpand=True,
                    bShowEqual=False,bFlat=False):
        try:
            self.__logDebug__(''%())
            self.txtSrc1.SetValue(sDN1)
            self.txtSrc2.SetValue(sDN2)
            self.iRec=iRec
            self.bMD5=bMD5
            self.bMD5Always=bMD5Always
            self.bAccess=bAccess
            self.bMod=bMod
            self.bCreate=bCreate
            self.bAutoExpand=bAutoExpand
            self.bShowEqual=bShowEqual
            self.bFlat=bFlat
            self.bSelProc=True
            self.pnCfg.SetValue(sDN1=sDN1,sDN2=sDN2,iRec=iRec,bMD5=bMD5,
                    bMD5Always=bMD5Always,bAccess=bAccess,bMod=bMod,
                    bCreate=bCreate,bAutoExpand=bAutoExpand,
                    bShowEqual=bShowEqual,bFlat=bFlat)
        except:
            self.__logTB__()
    def OnCfgOk(self,evt):
        try:
            d=self.pnCfg.GetValue()
            if VERBOSE>0:
                self.__logDebug__(['d',d])
            else:
                self.__logDebug__(''%())
            self.txtSrc1.SetValue(d['sDN1'])
            self.txtSrc2.SetValue(d['sDN2'])
            self.iRec=d['iRec']
            self.bMD5=d['bMD5']
            self.bMD5Always=d['bMD5Always']
            self.bAccess=d['bAccess']
            self.bMod=d['bMod']
            self.bCreate=d['bCreate']
            self.bAutoExpand=d['bAutoExpand']
            self.bShowEqual=d['bShowEqual']
            self.bFlat=d['bFlat']
        except:
            self.__logTB__()
    def OnCfgCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.pnCfg.SetValue(sDN1=self.txtSrc1.GetValue(),
                    sDN2=self.txtSrc2.GetValue(),
                    iRec=self.iRec,
                    bMD5=self.bMD5,
                    bMD5Always=self.bMD5Always,
                    bAccess=self.bAccess,
                    bMod=self.bMod,
                    bCreate=self.bCreate,
                    bAutoExpand=self.bAutoExpand,
                    bShowEqual=self.bShowEqual,
                    bFlat=self.bFlat)
        except:
            self.__logTB__()
    def OnInOutReadProc(self,evt):
        self.thdCtrl.OnThreadProc(evt)
    def OnInOutReadFin(self,evt):
        #self.thdCtrl.OnThreadFinished(evt)
        pass
    def OnInOutReadAbort(self,evt):
        #self.thdCtrl.OnThreadAborted(evt)
        pass
    def OnCbReadSrc1Button(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.tCount1=(0,0,0)
            self.dirComp=None
            self.DoRead1()
        except:
            self.__logTB__()
    def OnCbReadSrc2Button(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc2=None
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.DoRead2()
        except:
            self.__logTB__()
    def getTreeSel(self,tr):
        self.tiSel=tr.GetSelection()
    def procRead1(self,sDNSel,iRecLv,iStyleRd):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
        if os.path.exists(sDNSel):
            thd.Post('proc',0,100)
            thd.Post('proc',_(u'reading source 1 ...'),None)
            self.lDN[0]=sDNSel
            self.thdRead.procReadWalk(sDNSel,None,iRecLv,0,iStyleRd,True,self.refreshSel,1)
            self.tCount1=self.thdRead.GetCountTup()
        else:
            thd.Post('proc',_(u'    reading aborted, directory does not exist'),None)
    def procRead2(self,sDNSel,iRecLv,iStyleRd):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
        if os.path.exists(sDNSel):
            thd.Post('proc',0,100)
            thd.Post('proc',_(u'reading source 2 ...'),None)
            self.lDN[1]=sDNSel
            self.thdRead.procReadWalk(sDNSel,None,iRecLv,0,iStyleRd,True,self.refreshSel,2)
            self.tCount2=self.thdRead.GetCountTup()
        else:
            thd.Post('proc',_(u'    reading aborted, directory does not exist'),None)
    def procDummy(self):
        pass
    def DoRead1(self):
        try:
            thd=self.thdCtrl.GetThread()
            iStyleRd=0
            if self.bMD5:
                if self.bMD5Always:
                    iStyleRd|=0x01
                else:
                    iStyleRd|=0x20
            iStyleRd|=0x10
            iRecLv=self.iRec
            if iRecLv<0:
                iRecLv=sys.maxint
            if self.bFlat:
                bFlat=True
            else:
                bFlat=False
            if self.dirEntrySrc1 is None:
                sDNSel=self.txtSrc1.GetValue()
                if len(sDNSel)>0:
                    if os.path.exists(sDNSel):
                        self.thdRead.Start()
                        self.thdCtrl.Do(self.procRead1,sDNSel,iRecLv,iStyleRd)
                        return True
                    else:
                        thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
                        thd.Post('proc',_(u'   does not exist'),None)
                else:
                    thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
                    thd.Post('proc',_(u'   not defined'),None)
            else:
                thd.Post('proc',_(u'source 1 already calculated'),None)
        except:
            self.__logTB__()
        return False
    def DoRead2(self):
        try:
            thd=self.thdCtrl.GetThread()
            iStyleRd=0
            if self.bMD5:
                if self.bMD5Always:
                    iStyleRd|=0x01
                else:
                    iStyleRd|=0x20
            iStyleRd|=0x10
            iRecLv=self.iRec
            if iRecLv<0:
                iRecLv=sys.maxint
            if self.bFlat:
                bFlat=True
            else:
                bFlat=False
            if self.dirEntrySrc2 is None:
                sDNSel=self.txtSrc2.GetValue()
                if len(sDNSel)>0:
                    if os.path.exists(sDNSel):
                        self.thdRead.Start()
                        self.thdCtrl.Do(self.procRead2,sDNSel,iRecLv,iStyleRd)
                        return True
                    else:
                        thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
                        thd.Post('proc',_(u'   does not exist'),None)
                else:
                    thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
                    thd.Post('proc',_(u'   not defined'),None)
            else:
                thd.Post('proc',_(u'source 2 already calculated'),None)
        except:
            self.__logTB__()
        return False
    def procCmp(self,iStyle,bFlat=False):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',0,100)
        thd.Post('proc',_(u'comparing ...'),None)
        self.thdRead.Start()
        iCount=self.tCount1[0]+self.tCount2[0]+self.tCount1[3]+self.tCount2[3]
        if bFlat==True:
            self.dirComp.compareFlat(self.dirEntrySrc1,self.dirEntrySrc2,
                            iStyle,iCount,self.thdRead)
        else:
            self.dirComp.compareHier(self.dirEntrySrc1,self.dirEntrySrc2,
                            iStyle,iCount,self.thdRead)
        thd.Post('proc',_(u'compare done'),None)
        if thd.Is2Stop()==False:
            thd.CallBack(self.cbClrCmp.Enable,True)
            thd.CallBack(self.refreshComp,self.dirEntrySrc1.base,self.dirComp)
        else:
            self.dirComp=None
    def DoCmp(self):
        try:
            thd=self.thdCtrl.GetThread()
            self.dirComp=vtInOutCompDirEntry()
            iStyle=0x08
            if self.bCreate:
                iStyle|=0x04
            if self.bMod:
                iStyle|=0x02
            if self.bMD5:
                if self.bMD5Always:
                    pass
                else:
                    iStyle|=0x10
            if self.bAccess:
                iStyle|=0x01
            if self.bFlat:
                bFlat=True
            else:
                bFlat=False
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.thdCtrl.Do(self.procCmp,iStyle,bFlat)
            #self.nbData.SetSelection(1)
        except:
            self.__logTB__()
    def Start(self):
        try:
            thd=self.thdCtrl.GetThread()
            thd.Start()
            bProc=False
            bProc|=self.DoRead1()
            bProc|=self.DoRead2()
            if bProc:
                self.thdCtrl.Do(self.procDummy)
                self.trlstRes.DeleteAllItems()
                self.enableResWid(self.trlstRes,None)
                self.dirComp=None
            elif self.dirComp is None:
                self.thdCtrl.Do(self.procDummy)
                self.DoCmp()
            else:
                if self.DoBuildApplyCmd():
                    self.thdCtrl.Do(self.procDummy)
                else:
                    self.thdCtrl.Do(self.procDummy)
        except:
            self.__logTB__()
    def Stop(self):
        try:
            self.thdRead.Stop()
        except:
            self.__logTB__()
    def OnThreadProc(self,evt):
        evt.Skip()
        if evt.GetCount() is None:
            #self.pnLog.AddLog(evt.GetAct())
            pass
    def OnThreadAborted(self,evt):
        evt.Skip()
    def OnThreadFin(self,evt):
        evt.Skip()
    def getTarget(self):
        self.iTarget=0
        if self.tgTargetSrc1.GetValue():
            self.iTarget=1
        elif self.tgTargetSrc2.GetValue():
            self.iTarget=2
    def refreshSel(self,dn,dirEntry,iSrc):
        self.__logDebug__('dn:%s'%(dn))
        self.thdProc.Start()
        self.thdProc.DoCallBack(self.showRes,dn,dirEntry,iSrc)
    def showRes(self,dn,dirEntry,iSrc):
        try:
            thd=self.thdCtrl.GetThread()
            if iSrc==1:
                self.dirEntrySrc1=dirEntry
                if dirEntry is None:
                    thd.Post('proc',_(u'source 1 aborted'),None)
                    return
                self.cbClrSrc1.Enable(True)
                imgSrcIdx=self.dImg['Src1']
                iTyp=1
                thd.Post('proc',_(u'source 1 done'),None)
            elif iSrc==2:
                self.dirEntrySrc2=dirEntry
                if dirEntry is None:
                    thd.Post('proc',_(u'source 2 aborted'),None)
                    return
                self.cbClrSrc2.Enable(True)
                imgSrcIdx=self.dImg['Src2']
                iTyp=2
                thd.Post('proc',_(u'source 2 done'),None)
            if self.__isLogDebug__():
                self.__logInfo__('dn:%s;src1 is None=%d;src2 is None=%d'%(dn,
                        self.dirEntrySrc1 is None,self.dirEntrySrc2 is None))
            if thd.IsEmpty()==False:
                return
            if self.dirEntrySrc1 is None or self.dirEntrySrc2 is None:
                if thd.IsEmpty():
                    imgIdx=self.dImg['Cal']
                    if iSrc==1:
                        thd.Post('proc',_(u'source 1 show'),None)
                        tiRoot=self.trlstRes.AddRoot(_(u'source 1'),imgIdx,imgIdx)
                    elif iSrc==2:
                        thd.Post('proc',_(u'source 2 show'),None)
                        tiRoot=self.trlstRes.AddRoot(_(u'source 2'),imgIdx,imgIdx)
                    else:
                        self.__logCritical__('you are not supposed to be here;iSrc:%d'%(iSrc))
                        return
                    self.trlstRes.SetPyData(tiRoot,[dirEntry,iTyp,False])
                    sz=listFiles.getSizeStr(dirEntry.size,2)
                    self.trlstRes.SetItemText(tiRoot,sz,1)
                    self.trlstRes.SetItemImage(tiRoot,imgSrcIdx,column=2,
                                        which=wx.TreeItemIcon_Normal)
                    self.showResSub(self.trlstRes,tiRoot)
                    self.trlstRes.Expand(tiRoot)
                    self.enableResWid(self.trlstRes,tiRoot)
            else:
                thd.CallBack(self.DoCmp)
        except:
            self.__logTB__()
    def showResSub(self,trlst,ti):
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iTyp=l[1]
            imgSrcIdx=trlst.GetItemImage(ti,column=2,
                                    which=wx.TreeItemIcon_Normal)
            iSkip=len(dE.base)
            if dE.base[-1]!='/':
                iSkip+=1
            t=self.dImg['Dir']
            for d in dE.dirs:
                tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetPyData(tc,[d,iTyp,False])
                sz=listFiles.getSizeStr(d.size,2)
                trlst.SetItemText(tc,sz,iTyp)
                trlst.SetItemHasChildren(tc,True)
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
                trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
            tf=trlst.AppendItem(ti,'.')
            trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
            trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
            trlst.SetItemImage(tf,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
            t=self.dImg['file']
            iTyp+=4
            for f in dE.files:
                tc=trlst.AppendItem(tf,f.name)
                trlst.SetPyData(tc,[f,iTyp,True])
                sz=listFiles.getSizeStr(f.size,2)
                md5=f.md5 or ''
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
                trlst.SetItemText(tc,sz,1)
                trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
        except:
            self.__logTB__()
    def showCmp(self,dn,dirCmp,bEqual):
        self.__logInfo__('start')
        if self.IsMainThread()==False:
            return
        imgIdx=self.dImg['Cmp']
        imgSrcIdx=self.dImg['Srcs']
        tiRoot=self.trlstRes.AddRoot(_(u'compare results'),imgIdx,imgIdx)
        self.trlstRes.SetPyData(tiRoot,[dirCmp,3,False,self.ACT_UNDEF])
        self.trlstRes.SetItemImage(tiRoot,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
        self.showCmpSub(self.trlstRes,tiRoot,bEqual)
        self.trlstRes.SelectItem(tiRoot)
        self.trlstRes.Expand(tiRoot)
        self.enableResWid(self.trlstRes,tiRoot)
        if self.iTarget!=0:
            thd=self.thdCtrl.GetThread()
            if self.iTarget==1:
                thd.DoCallBackWX(self.SetCmd,self.ACT_TARGET1)
            elif self.iTarget==2:
                thd.DoCallBackWX(self.SetCmd,self.ACT_TARGET2)
        self.__logInfo__('done')
    def showCmpSub(self,trlst,ti,bEqual):
        if self.IsMainThread()==False:
            return
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iSkip=len(dE.base)#+1
            t=self.dImg['Dir']
            for d in dE.dir_results:
                bAdd=False
                for f in d.file_results:
                    if f.isEqual():
                        if bEqual==False:
                            continue
                    bAdd=True
                    break
                if len(d.dir_results)==0 and bAdd==False:
                    continue
                if bEqual==False:
                    if d.getDiffCount()==0:
                        continue
                if d.base[iSkip]=='/':
                    tc=trlst.AppendItem(ti,d.base[iSkip+1:])
                else:
                    tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
                trlst.SetPyData(tc,[d,3,False,self.ACT_UNDEF])
                trlst.SetItemHasChildren(tc,True)
            bAdd=False
            for f in dE.file_results:
                if f.isEqual():
                    if bEqual==False:
                        continue
                bAdd=True
                break
            if bAdd==True:
                tf=trlst.AppendItem(ti,'.')
                trlst.SetPyData(tf,[l[0],3,True,self.ACT_UNDEF])
                trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
                bAdd=False
                for f in dE.file_results:
                    t=self.dImg['wrn']
                    if f.isEqual():
                        if bEqual==False:
                            continue
                        t=self.dImg['ok']
                    bAdd=True
                    name=f.getName()
                    iSz=f.getSize()
                    if iSz<0:
                        sz=''
                    else:
                        sz=listFiles.getSizeStr(iSz,2)
                    #md5=f.getMD5()
                    tc=trlst.AppendItem(tf,name)
                    trlst.SetPyData(tc,[f,4,True,self.ACT_UNDEF])
                    trlst.SetItemText(tc,sz,1)
                    #trlst.SetItemText(tc,md5,2)
                    if f.hasSrc1():
                        if f.hasSrc2():
                            imgSrcIdx=self.dImg['Srcs']
                        else:
                            imgSrcIdx=self.dImg['Src1']
                    else:
                        if f.hasSrc2():
                            imgSrcIdx=self.dImg['Src2']
                        else:
                            imgSrcIdx=-1
                    if imgSrcIdx>=0:
                        trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                        which=wx.TreeItemIcon_Normal)
                    if f.isDiffSize():
                        trlst.SetItemText(tc,'X',4)
                        trlst.SetItemText(tc,'X',5)
                    if f.isDiffContent():
                        trlst.SetItemText(tc,'X',5)
                    if f.isDiffAccess():
                        trlst.SetItemText(tc,'X',6)
                    if f.isDiffModify():
                        trlst.SetItemText(tc,'X',7)
                    if f.isDiffCreate():
                        trlst.SetItemText(tc,'X',8)
                    trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                    trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
        except:
            self.__logTB__()
    def getTreeLabel(self,trlst,ti):
        tp=ti
        tr=trlst.GetRootItem()
        l=[]
        while tp is not None and tp!=tr:
            l.append(trlst.GetItemText(tp))
            tp=trlst.GetItemParent(tp)
        l.reverse()
        return l
    def findTreeItem(self,trlst,ti,sLbl,bRec=False):
        tp=ti
        t=trlst.GetFirstChild(ti)
        while t[0].IsOk():
            if trlst.GetItemText(t[0])==sLbl:
                return t[0]
            t=trlst.GetNextChild(ti,t[1])
        return None
    def enableResWid(self,trlst,ti):
        self.__logInfo__(''%())
        if self.IsMainThread()==False:
            return
        try:
            if ti is None:
                self.cbCpTo1.Enable(False)
                self.cbCpTo2.Enable(False)
                self.cbMvTo1.Enable(False)
                self.cbMvTo2.Enable(False)
                self.cbDel1.Enable(False)
                self.cbDel2.Enable(False)
            else:
                l=trlst.GetPyData(ti)
                if l is None:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(False)
                elif l[1]==1:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(False)
                elif l[1]==2:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(True)
                elif l[1]==3:
                    self.cbCpTo1.Enable(True)
                    self.cbCpTo2.Enable(True)
                    self.cbMvTo1.Enable(True)
                    self.cbMvTo2.Enable(True)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(True)
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc1():
                        self.cbOpenSrc1.Enable(True)
                        self.pnRes.SetSrc1(f.file1,self.dirEntrySrc1.base)
                        self.cbCpTo2.Enable(True)
                        self.cbMvTo2.Enable(True)
                        self.cbDel1.Enable(True)
                    else:
                        self.cbOpenSrc1.Enable(False)
                        self.pnRes.SetSrc1(None,'')
                        self.cbCpTo2.Enable(False)
                        self.cbMvTo2.Enable(False)
                        self.cbDel1.Enable(False)
                    if f.hasSrc2():
                        if self.dirEntrySrc1.base==self.dirEntrySrc2.base:
                            self.cbOpenSrc2.Enable(False)
                            self.pnRes.SetSrc2(None,'')
                            self.cbCpTo1.Enable(False)
                            self.cbMvTo1.Enable(False)
                            self.cbDel2.Enable(False)
                        else:
                            self.cbOpenSrc2.Enable(True)
                            self.cbCpTo1.Enable(True)
                            self.cbMvTo1.Enable(True)
                            self.cbDel2.Enable(True)
                        self.pnRes.SetSrc2(f.file2,self.dirEntrySrc2.base)
                    else:
                        self.cbOpenSrc2.Enable(False)
                        self.pnRes.SetSrc2(None,'')
                        self.cbCpTo1.Enable(False)
                        self.cbMvTo1.Enable(False)
                        self.cbDel2.Enable(False)
                    return
                elif l[1]==5:
                    f=l[0]
                    self.cbOpenSrc1.Enable(True)
                    self.pnRes.SetSrc1(f,self.dirEntrySrc1.base)
                    self.cbOpenSrc2.Enable(False)
                    self.pnRes.SetSrc2(None,'')
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(False)
                    return
                elif l[1]==6:
                    f=l[0]
                    self.cbOpenSrc2.Enable(True)
                    self.pnRes.SetSrc2(f,self.dirEntrySrc2.base)
                    self.cbOpenSrc1.Enable(False)
                    self.pnRes.SetSrc1(None,'')
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(True)
                    return
            self.cbOpenSrc1.Enable(False)
            self.cbOpenSrc2.Enable(False)
            self.pnRes.SetSrc1(None,'')
            self.pnRes.SetSrc2(None,'')
        except:
            self.__logTB__()
    def OnTrlstResTreeItemExpanding(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            l=self.trlstRes.GetPyData(ti)
            if l is None:
                return
            if l[1]==1:
                self.showResSub(self.trlstRes,ti)
            if l[1]==2:
                self.showResSub(self.trlstRes,ti)
            elif l[1]==3:
                bEqual=self.bShowEqual
                self.showCmpSub(self.trlstRes,ti,bEqual)
        except:
            self.__logTB__()
    def refreshComp(self,dn,dirCmp):
        self.__logDebug__('dn:%s'%(dn))
        bEqual=self.bShowEqual
        self.thdRead.DoCallBack(self.showCmp,dn,dirCmp,bEqual)
    def __openFile__(self,sDN,sFN):
        sExts=sFN.split('.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        filename=os.path.join(sDN,sFN)
        try:
            mime = fileType.GetMimeType()
            if sys.platform=='win32':
                filename=filename.replace('/','\\')
            cmd = fileType.GetOpenCommand(filename, mime)
            wx.Execute(cmd)
            self.__logPrintMsg__(_(u'opening %s ...')%filename)
            try:
                self.__logDebug__('cmd:%s'%(cmd))
            except:
                pass
        except:
            sMsg=u"Cann't open %s!"%(filename)
            dlg=vtmMsgDialog(self,sMsg ,
                                u'vCheckDirPanel',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
    def OnCbOpenSrc1Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc1()==False:
                        return
                    sFN=f.file1.fullname
                elif l[1]==5:
                    f=l[0]
                    sFN=f.fullname
                else:
                    return
                sDN=self.dirEntrySrc1.base
                self.funcOpen(sDN,sFN)
        except:
            self.__logTB__()
    def OnCbOpenSrc2Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc2()==False:
                        return
                    sFN=f.file2.fullname
                elif l[1]==6:
                    f=l[0]
                    sFN=f.fullname
                else:
                    return
                sDN=self.dirEntrySrc2.base
                self.funcOpen(sDN,sFN)
        except:
            self.__logTB__()
    def OnTrlstResTreeSelChanged(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            if self.bAutoExpand:
                if self.trlstRes.ItemHasChildren(ti):
                    if self.trlstRes.IsExpanded(ti)==False:
                        self.trlstRes.Expand(ti)
            self.enableResWid(self.trlstRes,ti)
        except:
            self.__logTB__()
    def __getTrData__(self):
        try:
            lTi=self.trlstRes.GetSelections()
            l=[]
            for ti in lTi:
                tup=self.trlstRes.GetPyData(ti)
                if tup[1] in [3,4]:
                    if len(tup)>=4:
                        l.append((ti,tup))
            return l
        except:
            self.__logTB__()
        return l
    def __setCmd__(self,thd,tr,ti,iCmd,bEqual):
        try:
            if ti is None:
                return
            tup=tr.GetPyData(ti)
            self.iAct+=1
            thd.Post('proc',self.iAct,self.iCount)
            try:
                if tup[1]==4:
                    if iCmd in [self.ACT_CP2TO1,self.ACT_MV2TO1,self.ACT_DEL2]:
                        if tup[0].hasSrc2():
                            tup[3]=iCmd
                    elif iCmd in [self.ACT_CP1TO2,self.ACT_MV1TO2,self.ACT_DEL1]:
                        if tup[0].hasSrc1():
                            tup[3]=iCmd
                    elif iCmd==self.ACT_TARGET1:
                        if tup[0].hasSrc2():
                            if tup[0].hasSrc1():
                                if tup[0].isEqual()==False:
                                    if tup[0].getDiffModify()>0:
                                        tup[3]=self.ACT_CP2TO1
                            else:
                                tup[3]=self.ACT_CP2TO1
                    elif iCmd==self.ACT_TARGET2:
                        if tup[0].hasSrc1():
                            if tup[0].hasSrc2():
                                if tup[0].isEqual()==False:
                                    if tup[0].getDiffModify()<0:
                                        tup[3]=self.ACT_CP1TO2
                            else:
                                tup[3]=self.ACT_CP1TO2
                    if self.__isLogDebug__():
                        self.__logDebug__('fn:%s'%(tup[0].getName()))
                    
                elif tup[1]==3:
                    tup[3]=iCmd
                    if self.__isLogDebug__():
                        self.__logDebug__('dn:%s'%(tup[0].base))
            except:
                self.__logTB__()
            imgIdx=self.dImgAct.get(tup[3],-1)
            
            tr.SetItemImage(ti,imgIdx,column=3,
                            which=wx.TreeItemIcon_Normal)
            if tup[2]==False:
                thd.DoCallBackWX(self.showCmpSub,tr,ti,bEqual)
                thd.DoCallBackWX(self.__setCmd__,thd,tr,ti,iCmd,bEqual)
            else:
                cid=tr.GetFirstChild(ti)
                while cid[0].IsOk():
                    tid=tr.GetPyData(ti)
                    if tid is not None:
                        if tid[1] in [3]:
                            if tid[2]==False:
                                thd.DoCallBackWX(self.showCmpSub,tr,cid[0],bEqual)
                                thd.DoCallBackWX(self.__setCmd__,thd,tr,cid[0],iCmd,bEqual)
                            else:
                                self.__setCmd__(thd,tr,cid[0],iCmd,bEqual)
                    cid=tr.GetNextChild(ti,cid[1])
        except:
            self.__logTB__()
    def getCmdTrans(self,iCmd):
        if iCmd==self.ACT_CP2TO1:
            return _(u'copy source 2 to 1')
        elif iCmd==self.ACT_CP1TO2:
            return _(u'copy source 1 to 2')
        elif iCmd==self.ACT_MV2TO1:
            return _(u'move source 2 to 1')
        elif iCmd==self.ACT_MV1TO2:
            return _(u'move source 1 to 2')
        elif iCmd==self.ACT_DEL1:
            return _(u'delete source 1')
        elif iCmd==self.ACT_DEL2:
            return _(u'delete source 2')
        elif iCmd==self.ACT_TARGET1:
            return _(u'target 1')
        elif iCmd==self.ACT_TARGET2:
            return _(u'target 2')
        else:
            return u''
    def SetCmd(self,iCmd):
        try:
            if self.dirComp is None:
                return
            l=self.__getTrData__()
            if len(l)==0:
                return
            self.iAct=0
            self.iCount=max(self.tCount1[0],self.tCount2[0])
            bEqual=self.bShowEqual
            thd=self.thdCtrl.GetThread()
            thd.Start()
            self.thdCtrl.Start()
            sCmd=self.getCmdTrans(iCmd)
            thd.Post('proc',_(u'set command:')+sCmd+u' ...',None)
            for ti,tup in l:
                try:
                    if tup[1]==4:
                        if self.__isLogDebug__():
                            self.__logDebug__('fn:%s'%(tup[0].getName()))
                    elif tup[1]==3:
                        if self.__isLogDebug__():
                            self.__logDebug__('dn:%s'%(tup[0].base))
                        pass
                    else:
                        continue
                except:
                    self.__logTB__()
                thd.DoCallBackWX(self.__setCmd__,thd,
                            self.trlstRes,ti,iCmd,bEqual)
            thd.DoCallBackWX(thd.Post,'proc',_(u'set command:')+sCmd+u' '+_(u'done'),None)
        except:
            self.__logTB__()
    def __buildApplyCmd__(self,tr,ti,lHier,l):
        try:
            tup=tr.GetPyData(ti)
            if tup[1]==4:
                if tup[3]!=self.ACT_UNDEF:
                    if tup[0].hasSrc1():
                        sFN1=tup[0].file1.fullname
                    else:
                        sFN1=None
                    if tup[0].hasSrc2():
                        sFN2=tup[0].file2.fullname
                    else:
                        sFN2=None
                    l.append((tup[3],sFN1,sFN2,ti))
                    if tup[3] in [self.ACT_CP1TO2,self.ACT_MV1TO2]:
                        self.iCount+=tup[0].getSize()
                    elif tup[3] in [self.ACT_CP2TO1,self.ACT_MV2TO1]:
                        self.iCount+=tup[0].getSize()
                    elif tup[3] in [self.ACT_DEL1]:
                        self.iCount+=1
                    elif tup[3] in [self.ACT_DEL2]:
                        self.iCount+=1
            elif tup[1]==3:
                if lHier is None:
                    lHier=[]
            cid=tr.GetFirstChild(ti)
            while cid[0].IsOk():
                tid=tr.GetPyData(ti)
                if tid is not None:
                    if tid[1] in [3]:
                        if tid[2]==True:
                            sLbl=tr.GetItemText(cid[0])
                            if sLbl=='.':
                                
                                self.__buildApplyCmd__(tr,cid[0],lHier,l)
                            else:
                                self.__buildApplyCmd__(tr,cid[0],lHier+[sLbl],l)
                cid=tr.GetNextChild(ti,cid[1])
        except:
            self.__logTB__()
    def DoBuildApplyCmd(self):
        try:
            self.__logInfo__(''%())
            ti=self.trlstRes.GetRootItem()
            self.iCount=0
            if ti is None:
                return False
            l=[]
            self.__buildApplyCmd__(self.trlstRes,ti,None,l)
            if self.iCount>0:
                sDN1=self.dirEntrySrc1.base
                sDN2=self.dirEntrySrc2.base
                thd=self.thdCtrl.GetThread()
                thd.Post('proc',_('apply actions ...'),None)
                thd.Do(self.procApplyCmd,sDN1,sDN2,l)
                return True
        except:
            self.__logTB__()
        return False
    def procCopy(self,sSrcFN,sDstFN,thd):
        thd.__logInfo__('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
        sTmpFN=getCurDateTimeFN(sDstFN)
        dn,fn=os.path.split(sTmpFN)
        try:
            os.makedirs(dn)
        except:
            pass
        try:
            if os.path.exists(sDstFN):
                os.rename(sDstFN,sTmpFN)
        except:
            thd.Post('proc','destination file could not be removed',None)
            thd.__logTB__()
            return -3
        try:
            st=os.stat(sSrcFN)
            fOut=open(sDstFN,'wb')
            fIn=open(sSrcFN,'rb')
            s=os.stat(sSrcFN)
            iSize=s[stat.ST_SIZE]
            iAct=0
            while iAct!=iSize:
                blk=fIn.read(4096)
                i=fIn.tell()
                self.iAct+=i-iAct
                iAct=i
                fOut.write(blk)
                thd.Post('proc',self.iAct,self.iCount)
                self.__logSetProcBarVal__(iAct,iSize)
            fIn.close()
            fOut.close()
        except:
            thd.__logTB__()
            try:
                fIn.close()
            except:
                pass
            try:
                fOut.close()
            except:
                pass
            try:
                if os.path.exists(sDstFN):
                    os.remove(sDstFN)
                if os.path.exists(sTmpFN):
                    os.rename(sTmpFN,sDstFN)
            except:
                thd.__logTB__()
            return -1
        try:
            if os.path.exists(sTmpFN):
                os.remove(sTmpFN)
        except:
            thd.Post('proc','destination file could not be removed',None)
            thd.__logTB__()
            return -2
        try:
            #st=os.stat(sSrcFN)
            setFileTimes(sDstFN,st[stat.ST_CTIME],st[stat.ST_ATIME],st[stat.ST_MTIME])
        except:
            thd.__logTB__()
        return 0
    def procMove(self,sSrcFN,sDstFN,thd):
        thd.__logInfo__('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
        iRet=self.procCopy(sSrcFN,sDstFN,thd)
        if iRet==0:
            try:
                if os.path.exists(sSrcFN):
                    os.remove(sSrcFN)
                    return 0
            except:
                thd.Post('proc','source file could not be removed',None)
                thd.__logTB__()
                return -4
            return -5
        return iRet
    def procDel(self,sSrcFN,thd):
        thd.__logInfo__('sSrcFN:%s'%(sSrcFN))
        self.iAct+=1
        try:
            if os.path.exists(sSrcFN):
                os.remove(sSrcFN)
                return 0
        except:
            thd.Post('proc','source file could not be removed',None)
            thd.__logTB__()
            return -4
        return -5
    def procApplyCmd(self,sDN1,sDN2,l):
        try:
            thd=self.thdCtrl.GetThread()
            for iCmd,sFN1,sFN2,ti in l:
                if thd.Is2Stop():
                    self.__logSetProcBarVal__(0,100)
                    return
                imgIdx=self.dImg['proc']
                self.__logSetProcBarVal__(0,100)
                thd.CallBack(self.updateTreeItemCmdResult,ti,imgIdx,True)
                if sFN1 is not None:
                    sCmd=':'.join([self.getCmdTrans(iCmd),sFN1])
                elif sFN2 is not None:
                    sCmd=':'.join([self.getCmdTrans(iCmd),sFN2])
                else:
                    sCmd=':'.join([self.getCmdTrans(iCmd),''])
                if iCmd==self.ACT_CP1TO2:
                    if sFN2 is None:
                        sFN2=sFN1
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1,' > ',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procCopy('/'.join([sDN1,sFN1]),'/'.join([sDN2,sFN2]),thd)
                elif iCmd==self.ACT_MV1TO2:
                    if sFN2 is None:
                        sFN2=sFN1
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1,' > ',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procMove('/'.join([sDN1,sFN2]),'/'.join([sDN2,sFN2]),thd)
                elif iCmd==self.ACT_CP2TO1:
                    if sFN1 is None:
                        sFN1=sFN2
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2,' > ',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procCopy('/'.join([sDN2,sFN2]),'/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_MV2TO1:
                    if sFN1 is None:
                        sFN1=sFN2
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2,' > ',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procMove('/'.join([sDN2,sFN2]),'/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_DEL1:
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procDel('/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_DEL2:
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    self.__logPrintMsg__(sCmd)
                    iRet=self.procDel('/'.join([sDN2,sFN2]),thd)
                else:
                    iRet=-1
                if iRet<0:
                    thd.CallBack(self.pnLog.AddLog,sCmd,'error')
                    imgIdx=self.dImg['err']
                else:
                    thd.CallBack(self.pnLog.AddLog,sCmd,'def')
                    imgIdx=self.dImg['ok']
                thd.CallBack(self.updateTreeItemCmdResult,ti,imgIdx)
        except:
            thd.__logTB__()
        thd.Post('proc',_('apply actions done.'),None)
        self.__logSetProcBarVal__(0,100)
    def updateTreeItemCmdResult(self,ti,imgIdx,bVisible=False):
        self.trlstRes.SetItemImage(ti,imgIdx,column=0,
                            which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemImage(ti,-1,column=2,
                            which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemImage(ti,-1,column=3,
                            which=wx.TreeItemIcon_Normal)
        tup=self.trlstRes.GetPyData(ti)
        if tup is not None:
            tup[3]=self.ACT_UNDEF
        if bVisible and self.bSelProc:
            self.trlstRes.EnsureVisible(ti)
            self.trlstRes.SelectItem(ti)
    def OnCbCpTo1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_CP2TO1)
    def OnCbCpTo2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_CP1TO2)
    def OnCbMvTo2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_MV1TO2)
    def OnCbMvTo1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_MV2TO1)
    def OnCbDel1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_DEL1)
    def OnCbDel2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_DEL2)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        return _(u'VIDARC Tools BrowseDir'),getAboutDescription(),version

    def OnCbClrSrc1Button(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.tCount1=(0,0,0)
            self.dirComp=None
            self.cbClrSrc1.Enable(False)
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            self.__logTB__()
    def OnCbClrSrc2Button(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc2=None
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.cbClrSrc2.Enable(False)
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            self.__logTB__()
    def OnCbClrCmpButton(self,event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirComp=None
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            self.__logTB__()
    def OnTgTarget1Button(self, event):
        event.Skip()
        try:
            self.tgTargetSrc2.SetValue(False)
        except:
            self.__logTB__()
        if self.tgTargetSrc1.GetValue():
            self.iTarget=1
            self.SetCmd(self.ACT_TARGET1)
        else:
            self.iTarget=0
    def OnTgTargetSrc2Button(self, event):
        event.Skip()
        try:
            self.tgTargetSrc1.SetValue(False)
        except:
            self.__logTB__()
        if self.tgTargetSrc2.GetValue():
            self.iTarget=2
            self.SetCmd(self.ACT_TARGET2)
        else:
            self.iTarget=0

    def OnCbRemoveButton(self, event):
        event.Skip()

    def OnCbClrAllButton(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.dirEntrySrc2=None
            self.tCount1=(0,0,0)
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.cbClrSrc2.Enable(False)
            self.cbClrSrc1.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            self.__logTB__()
    def Show(self,bFlag=True):
        try:
            self.__logDebug__(''%())
            if bFlag==False:
                self.cbRes.Close()
                self.cbLog.Close()
                self.cbCfg.Close()
            vtgFrame.Show(self,bFlag)
        except:
            self.__logTB__()
    def IsBusy(self):
        if VERBOSE>0:
            self.__logDebug__('thdRead:%d;thdProc:%d'%(self.thdRead.IsBusy(),
                    self.thdProc.IsBusy()))
        if self.thdRead.IsBusy():
            return True
        if self.thdProc.IsBusy():
            return True
        return False
    def OnCbCloseButton(self,evt):
        try:
            if self.IsBusy():
                self.__logInfo__('thd busy, close prevented'%())
                sMsg=_(u"synchronisation busy, cann't close frame yet.")
                self.__logPrintMsg__(sMsg)
                sMsg2=_(u"stop processing first.")
                self.pn.ShowMsgDlg(self.EXCLAMATION|self.OK,
                            u'\n'.join([sMsg,sMsg2]),None)
                return
            vtgFrame.OnCbCloseButton(self,evt)
            self.Destroy()
        except:
            self.__logTB__()
    def OnFrameClose(self,evt):
        try:
            if self.IsBusy():
                self.__logInfo__('thd busy, close prevented'%())
                sMsg=_(u"synchronisation busy, cann't close frame yet.")
                self.__logPrintMsg__(sMsg)
                sMsg2=_(u"stop processing first.")
                self.pn.ShowMsgDlg(self.EXCLAMATION|self.OK,
                            u'\n'.join([sMsg,sMsg2]),None)
                return
            vtgFrame.OnFrameClose(self,evt)
            self.Destroy()
        except:
            self.__logTB__()
