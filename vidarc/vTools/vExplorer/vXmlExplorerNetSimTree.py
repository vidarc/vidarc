#----------------------------------------------------------------------------
# Name:         vXmlExplorerNetSimTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vXmlExplorerNetSimTree.py,v 1.16 2012/01/12 10:08:03 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import array
    import types
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
    #from vidarc.tool.xml.vtXmlGrpTreeList import vtXmlGrpTreeList
    from vidarc.vTools.vExplorer.vXmlExplorerNetSim import vXmlExplorerNetSim
    from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemSelected
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState

    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    import vidarc.tool.InOut.vtInOutFileInfo as vtiLF
    #import vidarc.tool.InOut.vtInOutNetTools as vtiNT
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

def cmpTreeItems(l1,l2):
    return cmp(l1.lower(),l2.lower())

class vXmlExplorerNetSimTree(vtXmlGrpTree):
#class vXmlExplorerNetSimTree(vtXmlGrpTreeList):
    DSK_FIXED='HDD'
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,
        #vtXmlGrpTreeList.__init__(self,id=id, name=name,
        #        cols=[_(u'name'),(_(u'size'),60),(_(u'%'),60)],
                master=master,controller=controller,verbose=verbose,
                parent=parent, pos=pos, size=size,style=style)#|wx.TR_HIDE_ROOT)
        wx.EVT_TREE_ITEM_EXPANDING(self,self.GetId(),self.OnTcNodeExpanding)
        wx.EVT_RIGHT_DOWN(self, self.OnTcNodeRightDown)
        #self.GetMainWindow().Bind(wx.EVT_RIGHT_DOWN,self.OnTcNodeRightDown)
        #self.SetColumnAlign(1,2)
        #self.SetColumnAlign(2,2)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            [('tag','',)]),
                            #[[('tag','',)],[('size','')],[('percent','')]]),
                ])
        self.SetLabeling({
                None:[('tag','')],
                'ExplorerTrueCrypt':[('tag',''),u' (',('src',''),u')'],
                'ExplorerVMwareDisk':[('tag',''),u' (',('src',''),u')'],
                'ExplorerNetProtocolSmb':[('tag',''),u' (',('src',''),u')'],
                #None:[[('tag','')]],
                #'ExplorerTrueCrypt':[[('tag',''),u' (',('src',''),u')']],
                #'ExplorerVMwareDisk':[[('tag',''),u' (',('src',''),u')']],
                })
        self.MAP_DSK_TYPE={
            'hdd':          _('hard disk'),
            'floppy':       _('floppy disk'),
            'net':          _('network disk'),
            'removable':    _('removable disk'),
            'cdrom':        _('cdrom/dvd disk'),
            'unknown':      _('unknown')
            }
        self.SetDftNodeInfos()
        self.dRoot={}
        self.tiLocal=None
        self.dNet={}
        self.tiNet=None
    def __init_properties__(self):
        vtXmlGrpTree.__init_properties__(self)
        #vtXmlGrpTreeList.__init_properties__(self)
        self.dRoot={}
        self.iLimitWrn=75
        self.iLimitAlm=90
        self.tColorOk=(0,128,0)
        self.tColorWrn=(255,216,40)
        self.tColorAlm=(128,0,0)
        self.bSortTagName=False
        #self.BindEvent('expanding',self.OnTcNodeExpanding)
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','size','percent','src','|id','img'])
    def SetupImageList(self):
        self.__logDebug__(''%())
        if vtXmlGrpTree.SetupImageList(self):
        #if vtXmlGrpTreeList.SetupImageList(self):
            self.imgDict['vtArt']={}
            self.imgDict['vtArtState']={}
            
            self.__addElemImage2ImageList__('root',
                            imgExplorer.getDirImage(),
                            imgExplorer.getDirImage(),True)
            self.__addElemImage2ImageList__('ExplorerRoot',
                            imgExplorer.getDirImage(),
                            imgExplorer.getDirImage(),True)
            self.__addElemImage2ImageList__('computer',
                            imgExplorer.getComputerImage(),
                            imgExplorer.getComputerImage(),True)
            self.__addElemImage2ImageList__('hdd',
                            imgExplorer.getDiskFixedImage(),
                            imgExplorer.getDiskFixedImage(),True)
            self.__addElemImage2ImageList__('floppy',
                            imgExplorer.getDiskRemovableImage(),
                            imgExplorer.getDiskRemovableImage(),True)
            self.__addElemImage2ImageList__('removable',
                            imgExplorer.getDiskRemovableImage(),
                            imgExplorer.getDiskRemovableImage(),True)
            self.__addElemImage2ImageList__('net',
                            imgExplorer.getDiskNetImage(),
                            imgExplorer.getDiskNetImage(),True)
            self.__addElemImage2ImageList__('cdrom',
                            imgExplorer.getDriveCdromImage(),
                            imgExplorer.getDriveCdromImage(),True)
            self.__addElemImage2ImageList__('dir',
                            imgExplorer.getDirImage(),
                            imgExplorer.getDirImage(),True)
            self.__addElemImage2ImageList__('empty',
                            vtArt.getImage(vtArt.Invisible),
                            vtArt.getImage(vtArt.Invisible),True)
            self.__addElemImage2ImageList__('asc',
                            vtArt.getImage(vtArt.UpSmall),
                            vtArt.getImage(vtArt.UpSmall),True)
            self.__addElemImage2ImageList__('desc',
                            vtArt.getImage(vtArt.DownSmall),
                            vtArt.getImage(vtArt.DownSmall),True)
            self.__addElemImage2ImageList__('network',
                            vtArt.getImage(vtArt.Globe),
                            vtArt.getImage(vtArt.Globe),True)
            self.__addElemImage2ImageList__('host',
                            vtArt.getImage(vtArt.Server),
                            vtArt.getImage(vtArt.Server),True)
            self.__addElemImage2ImageList__('usr',
                            vtArt.getImage(vtArt.User),
                            vtArt.getImage(vtArt.User),True)
            self.__addElemImage2ImageList__('smb',
                            imgExplorer.getDiskNetImage(),
                            imgExplorer.getDiskNetImage(),True)
            self.__addElemImage2ImageList__('unDef',
                            vtArt.getImage(vtArt.UnDef),
                            vtArt.getImage(vtArt.UnDef),True)
            #self.__addElemImage2ImageList__('per03',
            #                imgExplorer.getPer03Image(),
            #                imgExplorer.getPer03Image(),True)
            self.SetImageList(self.imgLstTyp)
            self.imgDict['percent']={}
            self.SetupPercentList(bGen=True)
        else:
            self.SetupPercentList(bGen=False)
    def __cpyMaskedImg__(self,imgTmp,imgMsk,mergeMask=None):
        arTmp=array.array('B',imgTmp.GetData())
        arOvl=array.array('B',imgMsk.GetData())
        iLen=arTmp.buffer_info()[1]
        if arOvl.buffer_info()[1]!=iLen:
            self.__logCritical__('bitmap size mismatch',self)
        for i in xrange(0, iLen, 3):
            if arTmp[i]!=1 and arTmp[i+1]!=0 and arTmp[i+2]!=0:
                #if mergeMask is not None:
                #    if arOvl[i]==mergeMask[0] and arOvl[i+1]==mergeMask[1] and arOvl[i+2]==mergeMask[2]:
                #        arTmp[i]=1
                #        arTmp[i+1]=0
                #        arTmp[i+2]=0
                #        continue
                arTmp[i]=arOvl[i]
                arTmp[i+1]=arOvl[i+1] 
                arTmp[i+2]=arOvl[i+2]
        imgTmp.SetData(arTmp.tostring())
    def SetupPercentList(self,bGen=True):
        self.__logDebug__(''%())
        #self.imgLstPer=wx.ImageList(100,16)
        #imgTmp=imgExplorer.getPer03Image()
        iW=16
        iH=16
        iL=iW*iH
        fD=100.0/float(iL)
        self.fPerDelta=fD
        self.fIdxDelta=1/fD
        self.imgDict['percent']['fPerDelta']=self.fPerDelta
        self.imgDict['percent']['fIdxDelta']=self.fIdxDelta
        if bGen==False:
            return
        for iO in xrange(iL):
            fPer=iO*fD
            iR,iG,iB=self.tColorOk
            if fPer>self.iLimitWrn:
                iR,iG,iB=self.tColorWrn
            if fPer>self.iLimitAlm:
                iR,iG,iB=self.tColorAlm
            x=iO/iW
            y=iO-(x*iW)
            #print iO,fPer,x,y
            #imgTmp=wx.EmptyImage(16,16)
            imgTmp=imgExplorer.getPer00000Image()
            arTmp=array.array('B',imgTmp.GetData())
            for iX in xrange(x):
                if iX<x:
                    for iY in xrange(iH):
                        i=(((iH-1)-iY)*iW+iX)*3
                        #print '  ',iX,iY,i
                        arTmp[i]=iR#arOvl[i]
                        arTmp[i+1]=iG#255#arOvl[i+1] 
                        arTmp[i+2]=iB#arOvl[i+2]
                elif iX==x:
                    for iY in xrange(y):
                        #if iY<x:
                            i=(((iH-1)-iY)*iW+iX)*3
                            #print '  ',iX,iY,i
                            arTmp[i]=iR#arOvl[i]
                            arTmp[i+1]=iG#255#arOvl[i+1] 
                            arTmp[i+2]=iB#arOvl[i+2]
                       
#                    else:
#                        arTmp[i]=1
#                        arTmp[i+1]=0
#                        arTmp[i+2]=0
                        
            #for i in xrange(0, iL, 3):
            #    print ' ',i,iO
            #    if i<iO:
            #        arTmp[i]=255#arOvl[i]
            #        arTmp[i+1]=0#255#arOvl[i+1] 
            #        arTmp[i+2]=0#arOvl[i+2]
                    
            #imgTmp=imgExplorer.getPer01250Image()
            #ar=array.array('B',imgTmp.GetData())
            imgTmp.SetData(arTmp.tostring())
            #imgOvl=imgExplorer.getPerGreenImage()
            #self.__cpyMaskedImg__(imgTmp,imgOvl)
            imgIdx=self.imgLstTyp.Add(self.__convImg2Bmp__(imgTmp))
            self.imgDict['percent'][iO]=imgIdx
            #self.__addElemImage2ImageList__('per03',
            #                imgExplorer.getPer03Image(),
            #                imgExplorer.getPer03Image(),True)
        #for x in xrange(3,4):#16):
        #    for y in xrange(16):
        #        imgTmp=wx.EmptyImage(16,16)
        #        
        #        lB=[0]*255
        #        iL=y*16+x
        #        for iO in xrange(iL):
        #            iOfs=iO*3
        #            lB[iOfs*0]=255
        #            lB[iOfs*1]=255
        #            lB[iOfs*2]=255
        #array.array('B',lB)
        #imgTmp.SetData(ar.tostring())
        #img=self.__convImg2Bmp__(imgTmp)
        #self.imgLstPer.Add(img)
        #self.AssignImageList(self.imgLstPer,wx.IMAGE_LIST_NORMAL)
    def GetItemDN(self,ti):
        try:
            lHier=[]
            tp=ti
            while tp.IsOk()==True:
                td=self.GetPyData(tp)
                s=self.GetItemText(tp)
                tp=self.GetItemParent(tp)
                if td is not None:
                    t=type(td)
                    if t==types.DictType:
                        if 'root' in td:
                            oRoot=self.doc.GetReg('ExplorerRoot')
                            lHier.append(oRoot.GetRootShort(td['root']))
                            if len(lHier)>1:
                                lHier.reverse()
                                return td,'/'.join(lHier)
                            else:
                                lHier.append('')
                                return td,'/'.join(lHier)
                lHier.append(s)
        except:
            self.__logTB__()
        return None,None
    def OnTcNodeExpanding(self,evt):
        try:
            self.__logDebug__(''%())
            ti=evt.GetItem()
            td=self.GetPyData(ti)
            if type(td)==types.LongType:
                vtXmlGrpTree.OnTcNodeExpanding(self,evt)
                #vtXmlGrpTreeList.OnTcNodeExpanding(self,evt)
            else:
                evt.Skip()
                wx.CallAfter(self._readDN,ti)
        except:
            self.__logTB__()
    def _readDN(self,ti):
        try:
            self.Freeze()
            bDbg=False
            if self.__isLogDebug__():
                bDbg=True
            dRoot,sDN=self.GetItemDN(ti)
            if sDN is None:
                self.Thaw()
                return
            if bDbg:
                self.__logDebug__('dRoot:%s;sDN:%s'%(self.__logFmt__(dRoot),
                        sDN))
            oRoot=self.doc.GetReg('ExplorerRoot')
            d=oRoot.ReadContense(sDN)
            if bDbg:
                self.__logDebug__('%s'%(self.__logFmt__(d)))
            dDN=d['dDN']
            dUpd={}
            lD=[]
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                s=self.GetItemText(triChild[0])
                if s in dDN:
                    dUpd[s]=triChild[0]
                else:
                    lD.append(triChild[0])
                triChild=self.GetNextChild(ti,triChild[1])
            for sDN,dVal in dDN.iteritems():
                if sDN in dUpd:
                    pass
                else:
                    tid=wx.TreeItemData()
                    tid.SetData(dVal)
                    imgTup=self.__getImagesByInfos__(dVal.get('type','unknown'),{})
                    tc=self.AppendItem(ti,sDN,imgTup[0],imgTup[1],tid)
                    self.SetItemHasChildren(tc,True)
            for tc in lD:
                self.Delete(tc)
            self.SortChildren(ti)
        except:
            self.__logTB__()
        self.Thaw()
    def OnCompareItems(self,ti1,ti2):
        try:
            td1=self.GetPyData(ti1)
            td2=self.GetPyData(ti2)
            t1=type(td1)
            t2=type(td2)
            if t1==types.LongType:
                if t2==t1:
                    return vtXmlGrpTree.OnCompareItems(self,ti1,ti2)
                    #return vtXmlGrpTreeList.OnCompareItems(self,ti1,ti2)
                else:
                    return 1
            elif t2==types.LongType:
                return -1
            elif t1==types.DictType:
                if t2==t1:
                    iRet=cmp(td1.get('__sort'),td2.get('__sort'))
                    if iRet!=0:
                        return iRet
            s1=self.GetItemText(ti1)
            s2=self.GetItemText(ti2)
            return cmp(s1.lower(),s2.lower())
        except:
            self.__logTB__()
        return 0
    def OnTreeSort(self,event):
        try:
            self.__logDebug__(''%())
            ti=self.GetItemParent(self.triSelected)
            if ti.IsOk()==False:
                ti=self.triSelected
                if ti.IsOk()==False:
                    ti=self.tiRoot
            s=self.GetItemText(ti)
            td=self.GetPyData(ti)
            t=type(td)
            if t==types.LongType:
                self.SortChildren(ti)
            else:
                self.SortChildren(self.tiLocal)
                self.SortChildren(self.tiRoot)
        except:
            self.__logTB__()
    def OnTcNodeExpanded(self,evt):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if self.__isLogDebug__():
                bDbg=True
            ti=evt.GetItem()
            dRoot,sDN=self.GetItemDN(ti)
            if bDbg:
                self.__logDebug__('dRoot:%s;sDN:%s'%(self.__logFmt__(dRoot),
                        sDN))
        except:
            self.__logTB__()
    def __selectItem__(self,tn):
        bChanged=True
        if tn is None:
            self.objTreeItemSel=None
            self.triSelected=None
        else:
            self.triSelected=tn
            td=self.GetPyData(tn)
            self.objTreeItemSel=td
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
        wx.PostEvent(self,vtXmlTreeItemSelected(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        return 0
    def GetRootLabel(self,dVal):
        sRoot=dVal['root']
        if dVal.has_key('label')==False:
            sLbl=''
        else:
            sLbl=dVal['label']
        if len(sLbl)==0:
            return  sRoot
        else:
            return ''.join([sRoot,' (',sLbl,')'])
    def GetSelectedDN(self):
        try:
            if self.triSelected is None:
                return None,None,None
            ti=self.triSelected
            dRoot,sDN=self.GetItemDN(ti)
            return ti,dRoot,sDN
        except:
            self.__logTB__()
        return None,None,None
    def IsItemDN(self,ti=None):
        try:
            if ti is None:
                ti=self.triSelected
            if ti is None:
                return False
            td=self.GetPyData(ti)
            if type(td)==types.DictionaryType:
                return True
            else:
                return False
        except:
            self.__logTB__()
        return False
    def Walk(self,iRec,ti,func,*args,**kwargs):
        try:
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                ret=func(self,triChild[0],*args,**kwargs)
                if ret>0:
                    return triChild[0]
                elif ret<0:
                    return None
                if iRec>0:
                    if self.ItemHasChildren(triChild[0]):
                        tc=self.Walk(iRec-1,triChild[0],func,*args,**kwargs)
                        if tc is not None:
                            return tc
                triChild=self.GetNextChild(ti,triChild[1])
        except:
            self.__logTB__()
        return None
    def SelectDN(self,sDN):
        try:
            lDN=sDN.split('/')
            bDbg=False
            if self.__isLogDebug__():
                if VERBOSE>10:
                    bDbg=True
                self.__logDebug__('dn:%s;%s'%(sDN,self.__logFmt__(lDN)))
            ti=self.tiLocal
            def findRoot(tr,ti,sK,bLog):
                s=tr.GetItemText(ti)
                if bLog:
                    self.__logDebug__('?root;%s;%s'%(s,sK))
                if s.startswith(sK):
                    return 1
                else:
                    return 0
            def findDN(tr,ti,sK,bLog):
                s=tr.GetItemText(ti)
                if bLog:
                    self.__logDebug__('?dn;%s;%s'%(s,sK))
                if cmp(s,sK)==0:
                    return 1
                else:
                    return 0
            tp=self.Walk(0,ti,findRoot,lDN[0],bDbg)
            if tp is None:
                return -1
            iL=len(lDN)
            for iOfs in xrange(1,iL):
            #for sK in lDN[1:]:
                sK=lDN[iOfs]
                tc=self.Walk(0,tp,findDN,sK,bDbg)
                if tc is None:
                    self._readDN(tp)
                    self.Expand(tp)
                    tc=self.Walk(0,tp,findDN,sK,bDbg)
                    if tc is None:
                        return -2
                tp=tc
            if tp is None:
                return -2
            self.SelectItem(tp)
            self.EnsureVisible(tp)
        except:
            self.__logTB__()
    def GetRootInfoLst(self):
        try:
            self.__logDebug__(''%())
            oReg=self.doc.GetReg('ExplorerRoot')
            lRes=[]
            lRoot=oReg.GetRootLst()
            iOfs=0
            dMnt=oReg.GetMntDict()
            for sRoot in lRoot:
                dVal=oReg.GetRootInfoDict(sRoot)
                if sRoot in dMnt:
                    dVal['label']=dMnt[sRoot][:]
                if 'used' in dVal:
                    iSz=dVal.get('used',0)
                    if iSz>=0:
                        sSz=vtiLF.getSizeStr(iSz,2)
                    else:
                        sSz=''
                else:
                    sSz=''
                if 'percent' in dVal:
                    fPer=dVal.get('percent',0.0)
                    sPer='%5.1f'%fPer
                    iPer=self.imgDict['percent'][int(fPer*self.fIdxDelta)]
                else:
                    sPer=''
                    iPer=-1
                imgTup=self.__getImagesByInfos__(dVal.get('type','unknown'),{})
                l=[(self.GetRootLabel(dVal),imgTup[0]),
                    (sSz,-1),(sPer,iPer)]
                lRes.append(l)
            lHdr=[]
        except:
            self.__logTB__()
        return lRes
    def UpdateRootInfoDict(self):
        try:
            self.__logDebug__(''%())
            o=self.doc.getBaseNode()
            oReg=self.doc.GetRegByNode(o)
            ti=self.tiLocal
            def findRoot(tr,ti,sK,bLog):
                s=tr.GetItemText(ti)
                if bLog:
                    self.__logDebug__('?root;%s;%s'%(s,sK))
                if s.startswith(sK):
                    return 1
                else:
                    return 0
            def findDN(tr,ti,sK,bLog):
                s=tr.GetItemText(ti)
                if bLog:
                    self.__logDebug__('?dn;%s;%s'%(s,sK))
                if cmp(s,sK)==0:
                    return 1
                else:
                    return 0
            if oReg is not None:
                lRootOld=self.dRoot.keys()
                lRootOld.sort()
                lRoot=oReg.GetRootLst()
                iOfs=0
                dMnt=oReg.GetMntDict()
                for sRoot in lRootOld:
                    if sRoot.startswith('__'):
                        continue
                    if sRoot not in lRoot:
                        tp=self.Walk(0,self.tiLocal,findRoot,sRoot,False)
                        if tp is not None:
                            self.Delete(tp)
                        del self.dRoot[sRoot]
                for sRoot in lRoot:
                    if sRoot not in self.dRoot:
                        try:
                            dVal=oReg.GetRootInfoDict(sRoot)
                            if sRoot in dMnt:
                                dVal['label']=dMnt[sRoot][:]
                            self.dRoot[sRoot]=dVal
                            
                            tid=wx.TreeItemData()
                            tid.SetData(dVal)
                            imgTup=self.__getImagesByInfos__(dVal.get('type','unknown'),{})
                            sLbl=self.GetRootLabel(dVal)
                            ti=self.AppendItem(self.tiLocal,sLbl,imgTup[0],imgTup[1],tid)
                            dVal['ti']=ti
                            
                            if 'used' in dVal:
                                iSz=dVal.get('used',0)
                                if iSz>=0:
                                    sSz=vtiLF.getSizeStr(iSz,1)
                                else:
                                    sSz=''
                            else:
                                sSz=''
                            #self.SetItemText(ti,sSz)
                            #self.SetItemText(ti,sSz,1)
                            
                            #if 'percent' in dVal:
                            #    fPer=dVal.get('percent',0.0)
                            #    sPer='%5.1f'%fPer
                            #    v=self.imgDict['percent'][int(fPer*self.fIdxDelta)]
                            #else:
                            #    sPer=''
                            #    v=-1
                            #self.SetItemText(ti,sPer,2)
                            #self.SetItemImage(ti,v,2,which=wx.TreeItemIcon_Normal)
                            self.SetItemHasChildren(ti,True)
                        except:
                            self.__logTB__()
                    else:
                        dVal=self.dRoot[sRoot]
                        oReg.UpdateRootInfoDict(dVal)
                    #try:
                    #    dInfo=oReg.GetRootInfoDict(sRoot)
                    #except:
                    #    self.__logError__('root:%s'%(sRoot))
                    iOfs+=1
                self.SortChildren(self.tiLocal)
        except:
            self.__logTB__()
    def __addRootByInfos__(self,node,tagName,infos):
        try:
            self.__logDebug__(''%())
            if VERBOSE>10:
                self.__logDebug__('node:%s'%(repr(node)))
            tid=wx.TreeItemData()
            if self.TREE_DATA_ID:
                tid.SetData(self.doc.getKeyNum(node))
            else:
                tid.SetData(node)
            tn=self.AddRoot(_(u'vExplorer'),-1,-1,tid)
            imgTup=self.__getImagesByInfos__(tagName,infos)
            self.SetItemImage(tn,imgTup[0],which=wx.TreeItemIcon_Normal)
            
            
            self.dRoot={'__reg':'ExplorerRoot','__sort':0}
            tid=wx.TreeItemData()
            tid.SetData(self.dRoot)
            imgTup=self.__getImagesByInfos__('computer',infos)
            ti=self.AppendItem(tn,_(u'computer'),imgTup[0],imgTup[1],tid)
            self.tiLocal=ti
            self.UpdateRootInfoDict()
            self.Expand(self.tiLocal)
            
            self.dNet={'__reg':'ExplorerNetWork','__sort':1}
            tid=wx.TreeItemData()
            tid.SetData(self.dNet)
            imgTup=self.__getImagesByInfos__('network',infos)
            ti=self.AppendItem(tn,_(u'network'),imgTup[0],imgTup[1],tid)
            self.tiNet=ti
            #self.UpdateNetInfoDict()
            #self.Expand(self.tiNet)
            
            self.rootNode=node
            self.idRootNode=self.doc.getKeyNum(node)
        except:
            self.__logTB__()
        self.tiRoot=tn
        return tn
        return vtXmlGrpTreeList.__addRootByInfos__(self,o,tagName,infos)
    def __calcColumnSizes__(self):
        sz=self.GetClientSize()
        try:
            iWidth=sz[0]
            iFree=iWidth
            iColumn=1
            self.Freeze()
            self.SetColumnWidth(0,iWidth)
            if self.lColumn is not None:
                for t in self.lColumn:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.SetColumnWidth(iColumn,j)
                    iColumn+=1
            if self.lColumnItem is not None:
                for t in self.lColumnItem:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.SetColumnWidth(iColumn,j)
                    iColumn+=1
            #iFree-=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)+5*iColumn
            #if self.iMainColumnSize is None:
                # apply remaining size
            #    j=max(iFree,int(iWidth*self.REL_SIZE_MAIN_COLUMN))
            #    self.SetColumnWidth(0,j)
            #else:
            #    j=self.iMainColumnSize
            #    if j<0:
            #        j=-int(iWidth*j)
            #    else:
            #        j=self.iMainColumnSize
            #    self.SetColumnWidth(0,j)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Thaw()
        #wx.CallAfter(self.Refresh)
        #wx.CallAfter(self.Update)
        #par=self.GetParent()
        #par.Update()
    def OnCmpSize(self,event):
        event.Skip()
        self.__calcColumnSizes__()
    def UpdateNetInfoDict(self):
        try:
            self.__logDebug__(''%())
            o=self.doc.getBaseNode()
            oReg=self.doc.GetRegByNode(o)
            ti=self.tiNet
            def findRoot(tr,ti,sK,bLog):
                s=tr.GetItemText(ti)
                if bLog:
                    self.__logDebug__('?root;%s;%s'%(s,sK))
                if s.startswith(sK):
                    return 1
                else:
                    return 0
            if '__funcData' not in self.dNet:
                self.dNet['__funcData']=(oReg.GetNetHost4List,(),{})
            if oReg is not None:
                lNetOld=self.dNet.keys()
                lNetOld.sort()
                dNet=oReg.GetNetHostDict()
                iOfs=0
                for sHost in lNetOld:
                    if sHost.startswith('__'):
                        continue
                    if sHost not in dNet:
                        tp=self.Walk(0,self.tiNet,findRoot,sHost,False)
                        if tp is not None:
                            self.Delete(tp)
                        del self.dNet[sHost]
                for sHost,dVal in dNet.iteritems():
                    if sHost not in self.dNet:
                        try:
                            #if sRoot in dMnt:
                            #    dVal['label']=dMnt[sRoot][:]
                            self.dNet[sHost]=dVal
                            
                            tid=wx.TreeItemData()
                            tid.SetData(dVal)
                            imgTup=self.__getImagesByInfos__(dVal.get('type','unknown'),{})
                            ti=self.AppendItem(self.tiNet,sHost,imgTup[0],imgTup[1],tid)
                            dVal['ti']=ti
                            if '__share' in dVal:
                                lK=dVal['__share'].keys()
                                lK.sort()
                                for sShare in lK:#dVal['__share'].iteritems():
                                    dShare=dVal['__share'][sShare]
                                    if sShare is not None:
                                        imgTup=self.__getImagesByInfos__(dShare.get('type','unknown'),{})
                                        tid=wx.TreeItemData()
                                        tid.SetData(dShare)
                                        tc=self.AppendItem(ti,sShare,imgTup[0],imgTup[1],tid)
                        except:
                            self.__logTB__()
                    iOfs+=1
                self.SortChildren(self.tiNet)
        except:
            self.__logTB__()
    def __getImagesByInfos__(self,tagName,infos):
        try:
            if tagName=='ExplorerLaunchCmp':
                sImg=infos['img']
                if sImg=='':
                    pass
                else:
                    l=None
                    try:
                        if sImg.startswith('vtArt.'):
                            if sImg not in self.imgDict['vtArt']:
                                imgTmp=vtArt.getImage(eval(sImg))
                                self.__addImage2ImageList__('vtArt',sImg,imgTmp,imgTmp,True)
                            l=self.imgDict['vtArt'][sImg]
                        elif sImg.startswith('vtArtState.'):
                            if sImg not in self.imgDict['vtArtState']:
                                imgTmp=vtArtState.getImage(eval(sImg))
                                self.__addImage2ImageList__('vtArtState',sImg,imgTmp,imgTmp,True)
                            l=self.imgDict['vtArtState'][sImg]
                    except:
                        self.__logTB__()
                        l=None
                    if l is not None:
                        iL=len(l)
                        try:
                            img=l[0]
                        except:
                            img=self.imgDict['elem']['dft'][0]
                        try:
                            imgSel=l[1]
                        except:
                            imgSel=self.imgDict['elem']['dft'][1]
                        try:
                            imgInst=l[2]
                        except:
                            imgInst=self.imgDict['elem']['dft'][2]
                        try:
                            imgInstSel=l[3]
                        except:
                            imgInstSel=self.imgDict['elem']['dft'][3]
                        try:
                            imgInvalid=l[4]
                        except:
                            imgInvalid=self.imgDict['elem']['dft'][4]
                        try:
                            imgInvalidSel=l[5]
                        except:
                            imgInvalidSel=self.imgDict['elem']['dft'][5]
                        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
        except:
            self.__logError__('tagName:%s;infos;%s'%(tagName,self.__logFmt__(infos)))
            self.__logTB__()
        return vtXmlGrpTree.__getImagesByInfos__(self,tagName,infos)

