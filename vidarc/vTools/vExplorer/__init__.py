#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: __init__.py,v 1.3 2010/02/17 19:52:54 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import imgExp as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vExplorerMDIFrame'
PLUGABLE_FRAME=True
DOMAINS=['tools','analysis','vBrowseDir']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

SSL=0
import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
