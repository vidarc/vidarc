#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchCmdArgDlg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100502
# CVS-ID:       $Id: vXmlNodeExplorerLaunchCmdArgDlg.py,v 1.2 2010/05/18 12:54:18 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time,os,types
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.tool.InOut.vtInOutFileInfo as vtioFI
    import vidarc.tool.InOut.vtInOutFileTools as vtioFT
    
    import vidarc.vTools.vExplorer.imgExp as img
    from vSystem import isPlatForm
    WIN_FILE=0
    if isPlatForm('win'):
        SYSTEM=1
        try:
            import win32file
            WIN_FILE=1
        except:
            pass
    else:
        SYSTEM=0
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerLaunchCmdArgDlg(vtgDlg):
    def __init__(self, parent,dPos=None,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('lblRg',   (0,0),(1,1),{'name':'lblOld','label':_(u'current')},None),
            ('txt',     (0,1),(1,3),{'name':'txtOld','value':u''},None),
            ('lblCt',   (1,1),(1,1),{'name':'lblPos','label':_(u'possible')},None),
            ('lblCt',   (1,3),(1,1),{'name':'lblCur','label':_(u'current')},None),
            ('lst',     (2,1),(2,1),{'name':'lstPos','size':(150,220),},None),
            ('lstMulti',(2,3),(2,1),{'name':'lstCur','size':(150,220),},None),
            ('szBoxVert',(2,2),(2,1),
                    {'name':'bxsBt',},[
                    ('cbBmp',   0,4,{'name':'cbRight','bitmap':vtArt.getBitmap(vtArt.Right),},
                        {'btn':self.OnAdd}),
                    ('cbBmp',   0,4,{'name':'cbLeft','bitmap':vtArt.getBitmap(vtArt.Left),},
                        {'btn':self.OnDel}),
                    ('cbBmp',   0,4,{'name':'cbErase','bitmap':vtArt.getBitmap(vtArt.Erase),},
                        {'btn':self.OnClr}),
                    ]),
            ('lblRg',   (4,0),(1,1),{'name':'lblNew','label':_(u'new')},None),
            ('txt',     (4,1),(1,3),{'name':'txtNew','value':u''},None),
            #('lblRg', (1,0),(1,1),{'name':'lblExec','label':_(u'executable')},None),
            #('txt',   (1,1),(1,2),{'name':'txtExec','value':''},None),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExpCmdArg',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='popup',
                bmp=None or img.getPluginBitmap(),
                title=None or _(u'VIDARC Explorer command argument'),
                pnPos=None,pnSz=(300,400),pnStyle=None,
                pnName='pnExpCmdArg',
                lGrowCol=[(1,1),(3,1)],lGrowRow=[(2,2)],
                bEnMark=False,bEnMod=False,lWid=lWid)
        self.dPos={}
        self.lPosArg=[]
        if dPos is not None:
            self.SetPossible(dPos)
    def SetPossible(self,dPos):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            pn.lstPos.Clear()
            self.dPos=dPos.copy()
            self.lPosArg=[]
            l=[]
            for k,v in self.dPos.iteritems():
                l.append((v,k))
            def cmpArg(a,b):
                iR=cmp(a[0],b[0])
                if iR==0:
                    return cmp(a[1],b[1])
                return iR
            l.sort(cmpArg)
            self.lPosArg=[t[1] for t in l]
            
            for t in l:
                pn.lstPos.Append(t[0])
        except:
            self.__logTB__()
    def OnCbCancelButton(self,evt):
        try:
            self.__logDebug__(''%())
            par=self.GetParent()
            par.__Close__()
            #pn=self.GetPanel()
            #sVal=pn.txtOld.GetValue()
            #pn.txtNew.SetValue(sVal)
        except:
            self.__logTB__()
    def OnCbApplyButton(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            par=self.GetParent()
            sOld=pn.txtOld.GetValue()
            sNew=pn.txtNew.GetValue()
            if cmp(sOld,sNew)!=0:
                mod=True
            else:
                mod=-1
            par.SetValue(sNew,mod=mod)
            par.__Close__()
        except:
            self.__logTB__()
    def OnAdd(self,evt):
        try:
            bDbg=False
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
            pn=self.GetPanel()
            iSel=pn.lstPos.GetSelection()
            sSel=pn.lstPos.GetStringSelection()
            pn.lstCur.Append(sSel,iSel)
            sNew=pn.txtNew.GetValue()
            if bDbg:
                self.__logDebug__('sCur:%s'%(sNew))
            if len(sNew)>0:
                sNew=sNew+ u' '+self.lPosArg[iSel]
            else:
                sNew=self.lPosArg[iSel]
            self.txtNew.SetValue(sNew)
            if bDbg:
                self.__logDebug__('sNew:%s'%(sNew))
        except:
            self.__logTB__()
    def OnDel(self,evt):
        try:
            bDbg=False
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
            pn=self.GetPanel()
            lSel=list(pn.lstCur.GetSelections())
            sVal=pn.txtNew.GetValue()
            if bDbg:
                self.__logDebug__('sVal:%s;lSel;%s'%(sVal,self.__logFmt__(lSel)))
            for iIdx in lSel:
                sArg=self.lPosArg[pn.lstCur.GetClientData(iIdx)]
                iS=sVal.find(sArg)
                if iS>=0:
                    iE=iS+len(sArg)
                    if iS>0:
                        if sVal[iS-1]==u' ':
                            sVal=sVal[:iS-1]+sVal[iE:]
                        else:
                            sVal=sVal[:iS]+sVal[iE:]
                    else:
                        if iE>=len(sVal):
                            sVal=sVal[iE:]
                        else:
                            if sVal[iE]==u' ':
                                sVal=sVal[iE+1:]
                            else:
                                sVal=sVal[iE:]
            self.txtNew.SetValue(sVal)
            lSel.reverse()
            for iIdx in lSel:
                pn.lstCur.Delete(iIdx)
            if bDbg:
                self.__logDebug__('sVal:%s'%(sVal))
        except:
            self.__logTB__()
    def OnClr(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            pn.txtNew.SetValue(u'')
            pn.lstCur.Clear()
        except:
            self.__logTB__()
    def __getReplaceDict__(self):
        try:
            d={}
            pn=self.GetPanel()
            s=pn.txtNew.GetValue()
            if len(s)>0:
                d['new']=s
            s=pn.txtMatch.GetValue()
            if len(s)>0:
                d['match']=s
            s=pn.txtReplace.GetValue()
            if len(s)>0:
                d['repl']=s
            s=pn.txtInsLeft.GetValue()
            if len(s)>0:
                d['insLeft']=s
            s=pn.txtInsRight.GetValue()
            if len(s)>0:
                d['insRight']=s
            try:
                d['cropLeft']=int(pn.txtCropLeft.GetValue())
            except:
                pass
            try:
                d['cropRight']=int(pn.txtCropRight.GetValue())
            except:
                pass
            zNow=time.time()
            tNow=time.gmtime(zNow)
            bFlag=pn.chkDate.GetValue()
            if bFlag==True:
                d['date']=time.strftime("_%Y%m%d",tNow)
            bFlag=pn.chkTime.GetValue()
            if bFlag==True:
                d['time']=time.strftime("_%H%M%S",tNow)
            bFlag=pn.chkRestict.GetValue()
            if bFlag==True:
                d['rest']=bFlag
        except:
            self.__logTB__()
        return d
    def __getRenameNew__(self,sOld,d,bDbg=False):
        try:
            sN=sOld[:]
            def insAtExt(sN,sA):
                iExt=sN.rfind('.')
                if iExt>0:
                    sN=sN[:iExt]+sA+sN[iExt:]
                else:
                    sN=sN+sA
                return sN
            if 'new' in d:
                sN=d['new'][:]
            if 'match' in d:
                sN=sN.replace(d['match'],d.get('repl',u''))
            if 'insLeft' in d:
                sN=d['insLeft']+sN
            if 'insRight' in d:
                sN=insAtExt(sN,d['insRight'])
            if 'date' in d:
                sN=insAtExt(sN,d['date'])
            if 'time' in d:
                sN=insAtExt(sN,d['time'])
            iFound=vtioFI.exists(sN)
            if bDbg==True:
                self.__logDebug__('old:%s;new:%s;found:%d'%(sOld,sN,iFound))
            return sN
        except:
            self.__logTB__()
        return None
    def __getReplaceLst__(self):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            l=[]
            d=self.__getReplaceDict__()
            pn=self.GetPanel()
            lSel=pn.lstCur.GetSelections()
            if bDbg==True:
                self.__logDebug__('lSel;%s'%(self.__logFmt__(lSel)))
            if len(lSel)==0:
                lSel=range(pn.lstCur.GetCount())
            for iIdx in lSel:
                sOld=pn.lstCur.GetString(iIdx)
                sNew=self.__getRenameNew__(sOld,d,bDbg)
                if sNew is not None:
                    l.append((sOld,sNew))
        except:
            self.__logTB__()
        return l
    def OnPreView(self,evt):
        try:
            self.__logDebug__(''%())
            l=self.__getReplaceLst__()
            pn=self.GetPanel()
            pn.lstPreView.Clear()
            for t in l:
                sNew=t[1]
                pn.lstPreView.Append(sNew)
        except:
            self.__logTB__()
    def OnCropLeftText(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCropRightText(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def Show(self,bFlag):
        try:
            if bFlag==True:
                par=self.GetParent()
                sVal=par.GetValue()
                pn=self.GetPanel()
                pn.txtOld.SetValue(sVal)
                pn.txtNew.SetValue(sVal)
                lVal=sVal.split(' ')
                #pn.lstPos.Clear()
                pn.lstCur.Clear()
                for sN in lVal:
                    if sN in self.lPosArg:
                        iIdx=self.lPosArg.index(sN)
                        pn.lstCur.Append(self.dPos[sN],iIdx)
            vtgDlg.Show(self,bFlag)
        except:
            self.__logTB__()
    def DoModal(self,sBase,lDN,lFN):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__('sBase:%r;lDN;%s;lFN:%s'%(sBase,
                        self.__logFmt__(lDN),self.__logFmt__(lFN)))
            else:
                self.__logDebug__('sBase:%r'%(sBase))
            bVol=False
            if type(sBase)==types.DictType:
                bVol=True
            else:
                sBase=vtioFI.fn2posix(sBase)
            pn=self.GetPanel()
            self.lName=lDN+lFN
            self.lName.sort()
            pn.lstPos.Clear()
            pn.lstCur.Clear()
            for sN in self.lName:
                pn.lstCur.Append(sN)
            iRet=self.ShowModal()
            if iRet>0:
                l=self.__getReplaceLst__()
                if bDbg==True:
                    self.__logDebug__('l;%s'%(self.__logFmt__(l)))
                for t in l:
                    if bVol==True:
                        if SYSTEM==1:
                            if WIN_FILE==1:
                                try:
                                    iType=win32file.GetDriveType(t[0])
                                    if iType in [win32file.DRIVE_FIXED,win32file.DRIVE_REMOVABLE]:
                                        win32file.SetVolumeLabel(t[0],t[1])
                                except:
                                    self.__logError__('root:%s;label:%s'%(t[0],t[1]))
                                    self.__logTB__()
                            else:
                                self.__logError__('volume rename not possible, win32file missing'%())
                        else:
                            self.__logError__('volume rename not available on this platform'%())
                    else:
                        if sBase[-1]=='/':
                            sOld=vtioFI.join([sBase,t[0]])
                            sNew=vtioFI.join([sBase,t[1]])
                        else:
                            sOld=u''.join([sBase,t[0]])
                            sNew=u''.join([sBase,t[1]])
                        iOld=vtioFI.exists(sOld)
                        iNew=vtioFI.exists(sNew)
                        if bDbg==True:
                            self.__logDebug__('old(%d):%s;new(%d):%s'%(iOld,
                                    sOld,iNew,sNew))
                        try:
                            if iOld==1:
                                if iNew==0:
                                    os.rename(sOld,sNew)
                                    time.sleep(0.1)     # hack to allow vtInOutNotify to catch up
                        except:
                            self.__logError__('old(%d):%s;new(%d):%s'%(iOld,
                                    sOld,iNew,sNew))
                            self.__logTB__()
            return iRet
        except:
            self.__logTB__()
