#Boa:FramePanel:vExplorerWinDrivePanel
#----------------------------------------------------------------------------
# Name:         vExplorerWinDrivePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vExplorerWinDrivePanel.py,v 1.1 2010/02/13 22:42:26 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

[wxID_VEXPLORERWINDRIVEPANEL, wxID_VEXPLORERWINDRIVEPANELLSTROOT, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vExplorerWinDrivePanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRoot, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VEXPLORERWINDRIVEPANEL,
              name=u'vExplorerWinDrivePanel', parent=prnt, pos=wx.Point(22, 22),
              size=wx.Size(112, 364), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(104, 337))

        self.lstRoot = wx.ListBox(choices=[],
              id=wxID_VEXPLORERWINDRIVEPANELLSTROOT, name=u'lstRoot',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(104, 337), style=0)
        self.lstRoot.Bind(wx.EVT_LISTBOX, self.OnLstRootListbox,
              id=wxID_VEXPLORERWINDRIVEPANELLSTROOT)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
    def SetRootLst(self,l):
        self.lstRoot.Clear()
        for s in l:
            if s in ['A:\\','B:\\']:
                continue
            self.lstRoot.Append(s)
        self.lstRoot.SetSelection(0)
    def GetRootSel(self):
        return self.lstRoot.GetStringSelection()
    def OnLstRootListbox(self, event):
        event.Skip()
