#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerSrc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vXmlNodeExplorerSrc.py,v 1.4 2010/02/21 15:55:42 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    if vcCust.is2Import(__name__):
    #    from vXmlNodeExplorerSrcPanel import vXmlNodeExplorerSrcPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
    #    vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerSrc(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerSrc'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer source')
    # ---------------------------------------------------------
    # specific
    def GetSrc(self,node):
        return self.Get(node,'src')
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    # ---------------------------------------------------------
    # inheritance
    #def GetPossibleAcl(self):
    #    return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsRequired(self):
        "node has to be present"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01sIDAT8\x8d\xa5\x93\xcb+\x84Q\x18\x87\x9f\xf3}\xa3a4\xca\xa0i\\\xa2Q\
\xa2\\\x16\x16\x16S\x9aR\xd8\x08\xc5\xcer6\xcaZ\x16\x16\x83\x95"\x16\xfe\x02\
\x0b\xac\xdd\x12a\xc4\x90\t#\xd4\x18\x85\x84\xc8\xadq\x9fq\x99\xcf\x02\xe5\
\xf2\xcd|\xcaS\xef\xe6\x9c~O\xef{N\xaf\x10\x92\x8c\x16\xb33\xd3J\xa4;\x9df\
\xfa\x03\xbb\xdd\xfe?\x01\x80\xd3\xe9\xd4\x16\x94\x97\xd5*\xa5yFZ\xfb\xfa\
\x85\x9a\xa4\xad\xbd\xe3\xdb\xb9\xf4KPa\xa32?HcuU\xc4\xb9\xa3\n\x02\x81S\x9e\
\x9e\xc2\x14\x9b\xf6hi(\xd3\x94\xe8\x00,f\xabb\xb5&\x93h2\xe1\xf3o\xb0<\xb9K\
a\x8aBQ\xe6%Mu5Q%R^n\xa1\xe2p\xd4#\xc7\xe8\xc9H\xcf\xc2\x98`\xc1\x1f\x90\x98\
\xf3_s\x17|\xa1\xc4|\xcc@wg\x94\x0e\x04\xb8\x97\xdc<\xdc?"\xcb\x12( \x10\x84\
_\xc3\x18\x0cq\x04C!\x8e\x8eN"w\xe0\xf3m\x88\xcd\xadCb\xf5:\xf6\x0f\xf6\xb8\
\xbd=!;\xe1\x99\xca\xe2T^\x9f\x83\x0c\xaf\xc7\xd0\xdc\xd3\x1bY\x00p~v \x16\
\x16\x97\xc5\xd8\xd8\x84\xc8\xcd)\xa0\xab\xd1FF\xfc\x0bS\xdb!FVVU\xbf\xf3\
\x9b\xe0+\xc9Ii\\]^0\xb2e`h~\'j\xf8\xfd\r~080\xca\x92\xf1\x86q\xafW3\xac*\
\xf0\xac\xcd\x08\xcf_\x92\x9f\x08I\xd6,\x97\xcb\xa5\x00\xaa\xf5\xe7eR[$\x807\
\xea\xe0|3mD/4\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {}
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    #def GetPanelClass(self):
    #    if GUI:
    #        return vXmlNodeExplorerSrcPanel
    #    else:
    #        return None
