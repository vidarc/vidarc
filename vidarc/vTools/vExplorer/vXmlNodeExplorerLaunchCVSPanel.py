#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerLaunchCmdPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vXmlNodeExplorerLaunchCVSPanel.py,v 1.1 2010/06/05 21:33:58 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.input.vtInputFloat import vtInputFloat
    from vidarc.tool.input.vtInputTextPopup import vtInputTextPopup
    from vidarc.tool.misc.vtmFileBrowser import vtmFileBrowser
    from vidarc.tool.misc.vtmDirBrowser import vtmDirBrowser
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmdArgDlg import vXmlNodeExplorerLaunchCmdArgDlg

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerLaunchCVSPanel(vtgPanel,vtXmlNodePanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append(('lblRg',           (0,0),(1,1),    {'name':'lblAppl','label':_(u'application')},None))
        lWid.append((vtmFileBrowser,    (0,1),(1,3),    {
                'name':'fbbApp','bMulti':False,
                'bPosixFN':True, 'bSave':False,
                'sDftFN':u'','sFN':u'','sMsg':_(u'choose file'),
                'sWildCard':u'executable files|*.exe|all files|*.*'},None))
        lWid.append(('lblRg',           (1,0),(1,1),    {'name':'lblExecDN','label':_(u'directory')},None))
        lWid.append((vtmDirBrowser,     (1,1),(1,3),    {
                'name':u'fbbExecDN','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose execute directory')},None))
        lWid.append(('lblRg',           (2,0),(1,1),    {'name':'lblArg','label':_(u'arguments')},None))
        lWid.append((vtInputTextPopup,  (2,1),(1,3),    {
                'name':u'txtArg',},None))
        lWid.append(('lblRg',           (3,0),(1,1),    {'name':'lblLaunch','label':_(u'launch')},None))
        lWid.append(('szBoxHor',        (3,1),(1,3),{'name':'bxsStart',},[
                (vtInputFloat,      1,4,    {
                    'name':u'txtDly','default':0.0,'fraction_width':1,
                    'integer_width':4,'maximum':3600,'minimum':0.0},None),
                ('tgBmpLbl',    1,4,{'name':'tgSched',
                    'label':_(u'batch'),'bitmap':vtArt.getBitmap(vtArt.Build),},None),
                ('tgBmpLbl',    1,4,{'name':'tgAuto',
                    'label':_(u'auto'),'bitmap':vtArt.getBitmap(vtArt.AirPlane)},None),
                ('lblRg',       1,0,{'name':'lbl43','label':''},None),
                ]))
        lWid.append(('lblRg',           (4,0),(1,1),    {'name':'lblOpt','label':_(u'options')},None))
        lWid.append(('szBoxHor',        (4,1),(1,3),{'name':'bxsOptions',},[
                ('tgBmpLbl',    1,4,{'name':'tgShell',
                    'label':_(u'shell'),'bitmap':vtArt.getBitmap(vtArt.Shell),},None),
                ('tgBmpLbl',    1,4,{'name':'tgDetach',
                    'label':_(u'detach'),'bitmap':vtArt.getBitmap(vtArt.NetDisConnected)},None),
                ('tgBmpLbl',    1,4,{'name':'tgWatch',
                    'label':_(u'watch'),'bitmap':vtArt.getBitmap(vtArt.Eye),},None),
                ('tgBmpLbl',    1,0,{'name':'tgSave',
                    'label':_(u'save'),'bitmap':vtArt.getBitmap(vtArt.Save),},None),
                ]))
        lWid.append(('lblRg',           (5,0),(1,1),    {'name':'lblSave','label':_(u'save')},None))
        lWid.append((vtmDirBrowser,     (5,1),(1,3),    {
                'name':u'fbbSaveDN','bMulti':False,
                'bPosixFN':True,'bNew':True,
                'sMsg':_(u'choose save directory')},None))
        lWid.append(('lblRg',           (6,0),(1,1),    {'name':'lblClose','label':_(u'close')},None))
        lWid.append(('szBoxHor',        (6,1),(1,3),{'name':'bxsClose',},[
                (vtInputFloat,      1,4,    {
                    'name':u'txtDlyCls','default':0.0,'fraction_width':1,
                    'integer_width':4,'maximum':3600,'minimum':-1.0},None),
                ('lblRg',       1,4,{'name':'lbl61','label':''},None),
                ('lblRg',       1,4,{'name':'lbl62','label':''},None),
                ('lblRg',       1,0,{'name':'lbl63','label':''},None),
                ]))
        
        vtgPanel.__initCtrl__(self,lWid=lWid)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        return None,[(1,1),(3,1)]

    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        dlg=vXmlNodeExplorerLaunchCmdArgDlg(self.txtArg)
        try:
            dlg.SetPossible(self.objRegNode.GetArgDict())
        except:
            self.__logTB__()
        self.txtArg.SetPopupWin(dlg)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.fbbApp.SetValue('')
            self.fbbExecDN.SetValue('')
            self.txtArg.SetValue('')
            self.txtDly.SetValue(0.0)
            self.txtDlyCls.SetValue(0.0)
            
            self.tgShell.SetValue(False)
            self.tgAuto.SetValue(False)
            self.tgSched.SetValue(False)
            self.tgWatch.SetValue(True)
            #self.tgDetach.SetValue(True)
            self.tgSave.SetValue(False)
            self.fbbSaveDN.SetValue('')
        except:
            self.__logTB__()
    def SetNetDocs(self,d):
        #if d.has_key('vHum'):
        #    dd=d['vHum']
        # add code here
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            # add code here
            self.fbbApp.SetValue(self.objRegNode.GetApp(node))
            self.fbbExecDN.SetValue(self.objRegNode.GetDN(node))
            self.fbbSaveDN.SetValue(self.objRegNode.GetSaveDN(node))
            self.txtArg.SetValue(self.objRegNode.GetArg(node))
            self.txtDly.SetValue(self.objRegNode.GetDlyVal(node))
            self.txtDlyCls.SetValue(self.objRegNode.GetDlyClsVal(node))
            
            iVal=self.objRegNode.GetShellVal(node)
            if iVal>0:
                self.tgShell.SetValue(True)
            else:
                self.tgShell.SetValue(False)
            iVal=self.objRegNode.GetSaveVal(node)
            if iVal>0:
                self.tgSave.SetValue(True)
            else:
                self.tgSave.SetValue(False)
            iVal=self.objRegNode.GetWatchVal(node)
            if iVal>0:
                self.tgWatch.SetValue(True)
            else:
                self.tgWatch.SetValue(False)
            iVal=self.objRegNode.GetDetachVal(node)
            if iVal>0:
                self.tgDetach.SetValue(True)
            else:
                self.tgDetach.SetValue(False)
            iVal=self.objRegNode.GetAutoVal(node)
            if iVal>0:
                self.tgAuto.SetValue(True)
            else:
                self.tgAuto.SetValue(False)
            iVal=self.objRegNode.GetSchedVal(node)
            if iVal>0:
                self.tgSched.SetValue(True)
            else:
                self.tgSched.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def __GetNode__(self,node):
        try:
            self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetDN(node,self.fbbExecDN.GetValue())
            self.objRegNode.SetSaveDN(node,self.fbbSaveDN.GetValue())
            self.objRegNode.SetApp(node,self.fbbApp.GetValue())
            self.objRegNode.SetArg(node,self.txtArg.GetValue())
            if self.tgShell.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetShell(node,sVal)
            if self.tgWatch.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetWatch(node,sVal)
            if self.tgDetach.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetDetach(node,sVal)
            if self.tgSave.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetSave(node,sVal)
            if self.tgAuto.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetAuto(node,sVal)
            if self.tgSched.GetValue()==True:
                sVal=u'1'
            else:
                sVal=u'0'
            self.objRegNode.SetSched(node,sVal)
            self.objRegNode.SetDly(node,unicode(self.txtDly.GetValue()))
            self.objRegNode.SetDlyCls(node,unicode(self.txtDlyCls.GetValue()))
        except:
            self.__logTB__()
        #vtXmlNodePanel.__GetNode__(self,node)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbApp.Close()
        self.fbbExecDN.Close()
        self.fbbSaveDN.Close()
        self.txtArg.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.fbbApp.Cancel()
        self.fbbExecDN.Cancel()
        self.fbbSaveDN.Cancel()
        self.txtArg.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
