#Boa:FramePanel:vXmlNodeExplorerTrueCryptPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerTrueCryptPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vXmlNodeExplorerTrueCryptPanel.py,v 1.5 2010/04/03 21:40:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmFileBrowser
import wx.lib.buttons
import wx.lib.filebrowsebutton

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

[wxID_VXMLNODEEXPLORERTRUECRYPTPANEL, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELCHCMNT, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELFBBAPP, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELFBBSRC, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLAPPL, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLMNT, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLPWD, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLSRC, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGCLRPWD, 
 wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGFORGET, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vXmlNodeExplorerTrueCryptPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSrc, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.fbbSrc, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              3))
        parent.AddWindow(self.lblMnt, (1, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.chcMnt, (1, 1), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblPwd, (2, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddSizer(self.bxsBtPwd, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 3))
        parent.AddWindow(self.lblAppl, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.fbbApp, (3, 1), border=0, flag=wx.EXPAND, span=(1,
              3))

    def _init_coll_bxsBtPwd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgClrPwd, 0, border=0, flag=0)
        parent.AddWindow(self.tgForget, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsBtPwd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBtPwd_Items(self.bxsBtPwd)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEEXPLORERTRUECRYPTPANEL,
              name=u'vXmlNodeExplorerTrueCryptPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblSrc = wx.StaticText(id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLSRC,
              label=_(u'file:'), name=u'lblSrc', parent=self, pos=wx.Point(0,
              0), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.fbbSrc = vidarc.tool.misc.vtmFileBrowser.vtmFileBrowser(bMulti=False,
              bPosixFN=True, bSave=False,
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELFBBSRC, name=u'fbbSrc',
              parent=self, pos=wx.Point(60, 0), sDftFN=u'tmpl.TC',
              sFN=u'tmpl.TC', sMsg=u'choose file',
              sWildCard=_(u'true crypt files|*.TC|all files|*.*'),
              size=wx.Size(211, 30), style=0)

        self.chcMnt = wx.Choice(choices=[],
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELCHCMNT, name=u'chcMnt',
              parent=self, pos=wx.Point(60, 34), size=wx.Size(79, 21), style=0)

        self.tgClrPwd = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGCLRPWD,
              label=_(u'clear stored'), name=u'tgClrPwd', parent=self,
              pos=wx.Point(60, 62), size=wx.Size(100, 30), style=0)
        self.tgClrPwd.SetToggle(False)
        self.tgClrPwd.Bind(wx.EVT_BUTTON, self.OnCbClrPwdButton,
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGCLRPWD)

        self.tgForget = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Shield),
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGFORGET,
              label=_(u'forget'), name=u'tgForget', parent=self,
              pos=wx.Point(160, 62), size=wx.Size(100, 30), style=0)
        self.tgForget.SetToggle(False)
        self.tgForget.Bind(wx.EVT_BUTTON, self.OnTgForgetButton,
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELTGFORGET)

        self.lblMnt = wx.StaticText(id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLMNT,
              label=_(u'mount point:'), name=u'lblMnt', parent=self,
              pos=wx.Point(0, 34), size=wx.Size(56, 24), style=wx.ALIGN_RIGHT)

        self.lblPwd = wx.StaticText(id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLPWD,
              label=_(u'password:'), name=u'lblPwd', parent=self,
              pos=wx.Point(0, 62), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.lblAppl = wx.StaticText(id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELLBLAPPL,
              label=_(u'application:'), name=u'lblAppl', parent=self,
              pos=wx.Point(0, 96), size=wx.Size(56, 30), style=0)

        self.fbbApp = vidarc.tool.misc.vtmFileBrowser.vtmFileBrowser(bMulti=False,
              bPosixFN=True, bSave=False,
              id=wxID_VXMLNODEEXPLORERTRUECRYPTPANELFBBAPP, name=u'fbbApp',
              parent=self, pos=wx.Point(60, 96), sDftFN=u'', sFN=u'',
              sMsg=u'choose file',
              sWildCard=u'executable files|*.exe|all files|*.*',
              size=wx.Size(211, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        for i in xrange(65,65+26):
            self.chcMnt.Append('%s:\\'%(chr(i)))
        self.chcMnt.SetStringSelection('D:\\')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        #self.gbsData.AddGrowableCol(0,2)
        self.gbsData.AddGrowableCol(1,1)
        #self.gbsData.AddGrowableCol(2,1)
        self.gbsData.AddGrowableCol(3,1)
        self.gbsData.AddGrowableCol(4,1)
        self.gbsData.AddGrowableRow(1,1)
        self.gbsData.Layout()
        #self.gbsData.FitInside(self)
        self.gbsData.Fit(self)
        #self.fgsData.FitInside(self)

    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.tgClrPwd.SetValue(True)
            self.tgForget.SetValue(True)
            self.fbbSrc.SetValue('')
            self.chcMnt.SetStringSelection('D:\\')
            self.fbbApp.SetValue('')
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.fbbSrc.SetValue(self.objRegNode.GetSrc(node))
            self.fbbApp.SetValue(self.objRegNode.GetApp(node))
            try:
                sMnt=self.objRegNode.GetMnt(node)
                self.chcMnt.SetStringSelection(sMnt)
            except:
                self.chcMnt.SetStringSelection('D:\\')
                self.__logTB__()
            iVal=self.objRegNode.GetClrPwdVal(node)
            if iVal>0:
                self.tgClrPwd.SetValue(True)
            else:
                self.tgClrPwd.SetValue(False)
            iVal=self.objRegNode.GetReCallVal(node)
            if iVal>0:
                self.tgForget.SetValue(False)
            else:
                self.tgForget.SetValue(True)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetSrc(node,self.fbbSrc.GetValue())
            self.objRegNode.SetApp(node,self.fbbApp.GetValue())
            sMnt=self.chcMnt.GetStringSelection()
            self.objRegNode.SetMnt(node,sMnt)
            if self.tgClrPwd.GetValue()==True:
                val=u'1'
            else:
                val=u'0'
            self.objRegNode.SetClrPwd(node,val)
            if self.tgForget.GetValue()==True:
                val=u'0'
            else:
                val=u'1'
            self.objRegNode.SetReCall(node,val)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnCbClrPwdButton(self, event):
        event.Skip()

    def OnTgForgetButton(self, event):
        event.Skip()
