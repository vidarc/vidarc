#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: __register__.py,v 1.9 2010/06/05 21:33:10 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import vidarc.tool.log.vtLog as vtLog

from vidarc.vTools.vExplorer.vXmlNodeExplorerSrc import vXmlNodeExplorerSrc
from vidarc.vTools.vExplorer.vXmlNodeExplorerTrueCrypt import vXmlNodeExplorerTrueCrypt
from vidarc.vTools.vExplorer.vXmlNodeExplorerVMwareDisk import vXmlNodeExplorerVMwareDisk

#from vidarc.vTools.vExplorer.vXmlNodeExplorerTag import vXmlNodeExplorerTag
#from vidarc.vTools.vExplorer.vXmlNodeExplorerTagRd import vXmlNodeExplorerTagRd
from vidarc.vTools.vExplorer.vXmlNodeExplorerLauncher import vXmlNodeExplorerLauncher
from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchGrp import vXmlNodeExplorerLaunchGrp
from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmd import vXmlNodeExplorerLaunchCmd
from vidarc.vTools.vExplorer.vXmlNodeExplorerSynch import vXmlNodeExplorerSynch

from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCVS import vXmlNodeExplorerLaunchCVS

from vidarc.vTools.vExplorer.vXmlNodeExplorerNetWork import vXmlNodeExplorerNetWork
from vidarc.vTools.vExplorer.vXmlNodeExplorerNetHost import vXmlNodeExplorerNetHost
from vidarc.vTools.vExplorer.vXmlNodeExplorerNetProtocolSmb import vXmlNodeExplorerNetProtocolSmb

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

def RegisterNodes(self,bRegAsRoot=True,oRoot=None):
    try:
        oSrc=vXmlNodeExplorerSrc()
        oTrueCrypt=vXmlNodeExplorerTrueCrypt()
        oVMwareDisk=vXmlNodeExplorerVMwareDisk()
        
        oLauncher=vXmlNodeExplorerLauncher()
        oLaunchGrp=vXmlNodeExplorerLaunchGrp()
        oLaunchCmd=vXmlNodeExplorerLaunchCmd()
        oSynch=vXmlNodeExplorerSynch()
        oCVS=vXmlNodeExplorerLaunchCVS()
        
        oNetWork=vXmlNodeExplorerNetWork()
        oNetHost=vXmlNodeExplorerNetHost()
        oNetSmb=vXmlNodeExplorerNetProtocolSmb()
        
        #self.RegisterNode(oSrc,bRegAsRoot)
        self.RegisterNode(oSrc,False)
        self.RegisterNode(oTrueCrypt,False)
        self.RegisterNode(oVMwareDisk,False)
        self.RegisterNode(oNetWork,False)
        self.RegisterNode(oNetHost,False)
        self.RegisterNode(oNetSmb,False)
        
        self.RegisterNode(oLauncher,False)
        self.RegisterNode(oLaunchGrp,False)
        self.RegisterNode(oLaunchCmd,False)
        self.RegisterNode(oSynch,False)
        
        self.RegisterNode(oCVS,False)
        
        oLog=self.GetReg('log',bLogFlt=False)
        if oLog is None:
            oLog=vtXmlNodeLog()
            self.RegisterNode(oLog,False)
        oAttrML=self.GetReg('attrML',bLogFlt=False)
        if oAttrML is None:
            oAttrML=vtXmlNodeAttrCfgML()
            self.RegisterNode(oAttrML,False)
        #self.LinkRegisteredNode(oRoot,oRd)
        #self.LinkRegisteredNode(oRd,oTagRd)
        if oRoot is not None:
            self.LinkRegisteredNode(oRoot,oSrc)
            self.LinkRegisteredNode(oRoot,oNetWork)
            self.LinkRegisteredNode(oRoot,oLauncher)
            
        self.LinkRegisteredNode(oSrc,oSrc)
        self.LinkRegisteredNode(oSrc,oLog)
        
        #self.LinkRegisteredNode(oSrc,oNetSmb)
        #self.LinkRegisteredNode(oSrc,oNetSmb)
        
        self.LinkRegisteredNode(oSrc,oTrueCrypt)
        self.LinkRegisteredNode(oSrc,oVMwareDisk)
        self.LinkRegisteredNode(oSrc,oNetSmb)
        self.LinkRegisteredNode(oSrc,oNetWork)
        self.LinkRegisteredNode(oNetWork,oNetSmb)
        
        self.LinkRegisteredNode(oLauncher,oLaunchGrp)
        self.LinkRegisteredNode(oLaunchGrp,oLaunchGrp)
        self.LinkRegisteredNode(oLauncher,oLaunchCmd)
        self.LinkRegisteredNode(oLaunchGrp,oLaunchCmd)
        
        self.LinkRegisteredNode(oLauncher,oSynch)
        self.LinkRegisteredNode(oLaunchGrp,oSynch)
        
        self.LinkRegisteredNode(oLauncher,oCVS)
        self.LinkRegisteredNode(oLaunchGrp,oCVS)
        #self.LinkRegisteredNode(oStep,oWr)
        #self.LinkRegisteredNode(oStep,oChk)
        #self.LinkRegisteredNode(oStep,oLog)
        #self.LinkRegisteredNode(oStep,oBreak)
    except:
        vtLog.vtLngTB(__name__)
