#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerVMwareDisk.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100216
# CVS-ID:       $Id: vXmlNodeExplorerVMwareDisk.py,v 1.3 2010/02/21 15:55:42 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerVMwareDiskPanel import vXmlNodeExplorerVMwareDiskPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerVMwareDisk(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerVMwareDisk'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer VMware disk')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=1):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    def GetExec(self,node,sFN=None):
        #sTag=self.GetTag(node)
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" %s "'+oLocalRoot.ConvToOS(sFN)+'"',\
                    ['appl_vmwaremount','drive']
        else:
            return sTag,sFN,'"'+sApp+'" %s "'+oLocalRoot.ConvToOS(sFN)+'"',\
                    ['drive']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if bForce==True:
            sKind='/f'
        else:
            sKind='/d'
        if len(sApp)==0:
            return sTag,sFN,'"%s" %s '+sKind,\
                    ['appl_vmwaremount','drive']
        else:
            return sTag,sFN,'"'+sApp+'" %s '+sKind,\
                    ['drive']
    def GetApplName(self):
        return 'appl_vmwaremount'
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xc6IDAT8\x8dm\x93]HTi\x18\xc7\x7f\xef{\xcex\xce\xcch\xe3h\xe3\x96\
\xc3\xccfhd!A\x17\xddFP\xe4Rd\xb0k\x0b[\x92\x12xS$VD\x14v\x15\x14\xa5\x15\
\xdd\x14\xb2QK\x1fX\xc4\xc2Fl",K\x97A\x10\xee\x96}xQ\xa9\xe3W\x1f3\x1a3g\xce\
t\xce\xfbva\x19\x83=W\x0f<\x7f~<_\x7f!\xa4\xc1\xd7\xd8z\xb4O\xc7\x16\x95 X\
\x18J\xc3\xf4\x8c\xc3\xdf\xa7v\x16\x95\xcd\xaf\xc9O\x87\xaf\xeb\xd6_\x1a\x89\
[\xb3\x14\x9c,Z\xeby\x91\x90\x92\x12;\xc4\xa8S\x86\xf7\xe9\xaa\x1e\xe8i\x15\
\x0b\x00\xd1\x90$a\xcd\xb0\xaf\xe7O^\xbdI\xe1+\x7f\x1e`\x18\x06u5I\xcew6\x11\
\r}\xeb\xb8\x08\x00Pp\xf3\xa4\xc6\'\xa8N$\x89-\xae \x1a\x94H\x01\xbe\xef\x93\
\x9bMS"\xa1&\x1ecG\xd75\x9d\xc9y\x0c\xf4\xb4\x89y\x80\x10\x02\xadA)M\xb4<\
\xc2oM\x9b\xa8\xaf(\xa0\n\x0eJk\x0c!\xf8qI\x05;\xd6\xaf\xa4\xa0\xea\x19q\xca\
\x08\x04nh\x13`\xc3\xfe^]\x1d+\'d\x97P\x97\xacbU"\xc2\xda\xa5\x9a+\xf7\x1e\
\xf3\xff\xd0K\x9c\xbcK\xd6q\xb9tl\x17\'.\xf7\x93q\xe1l\xc7v\xaa"A\xcc\xc6C\
\x7f\xe8\x96\xe6-\xac\x89y\xc4+\xc3\x9c\xdc\xbb\x8d\x90e\xb1,VJ\xcb\xa6\x06\
\xf4\xc6\x06\xd0\x8a\x8e\xee>\x94\xd2\x84\x16Ey\xf4\xf0?\n\xf9\x1cR\x80\\RY\
\xc6\xf2R\x87\xde\xbb\x0fy\xf1f\x8a\xf6\x13\xd78p\xee6\xcf_\x8f\xd3\xd1\xdd\
\xc7\xc9\x1b\x0f@\x08l\xdbFH\x89i\x18\xf8\xbe?\x7f%iH\xf8\x94\xcf2\xf8t\x98\
\xac\xab\x8a6\xecy\x1eS\x93\x13\x08\x04\r\xf5u\xc8@\x10\xa5\x8b$sWPZ35\xe3\
\xf2*W\xca\xc5c\xad\x04\x03P\x9b\xa8\xe2\xfc\xc1_\x01\xa8M,\xa6m\xeb:\x1eO\
\x08\xd2\x8eZ\x08\x00x\x9f\x9e\xe1\xe6_\xff`\x1b\x1e\x96r8\xda\xb6\x99\xae\
\x8bwQJ\xd1\xdd\xd9\xcc\xef\xf7\x07\x19\x1a\xcd\x90\xc9\xcc"\r\x03!D1\xc0\
\xf7<>\xa43 \x04\xd5\x95a<mb\x04\xcbx79I>_\xe0\xc9\xb3a\x86G\xa6\x11RR\x93\
\x8cc\xd9s\xe3\x98\x00\xe2\xcb\x1f\xf8J!\xa5$\xef\x9b\x0c\xa5-\x8e\xefi$\x80\
\xc7\x8ad\x15\xe7:\x7f&\x97/ \x84 `\xd9\xa4\xdc\x08og]L\xad\xc1\xb2\x83\xc4\
\xe3KI\x8d\x8d\xa0\x94b|\xcc\xe4\xcc\xd8\x08?D,\xd6\xac^A\xfb\xb60\xb7\x1e\
\xbc \xf56\rZ\xa3\x81LNq\xfft\x8b0\xdfg\x15S\xaa\x82\xde#\xcd\xb8N\xae\xc8DR\
J\x02v\x98\xc1i\x93G\xcfG\xf9\xf7B\xfb\x02\xa3\n!\r\x9a\x8e\xdf\xd1\x95a\xf9\
]\x1b\xfb\x1a&\xdf}\xa4\xbf{\xf7\xf7\xca|\x06\x9c\xd1!\xcd1Lcj\x00\x00\x00\
\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerVMwareDiskPanel
        else:
            return None
    def Build(self):
        self.__logDebug__(''%())
        self.doc.RegisterExtension('vmdk',self)
