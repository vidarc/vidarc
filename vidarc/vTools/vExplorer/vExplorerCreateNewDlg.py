#----------------------------------------------------------------------------
# Name:         vExplorerCreateNewDlg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100402
# CVS-ID:       $Id: vExplorerCreateNewDlg.py,v 1.2 2010/04/02 23:26:27 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.tool.InOut.vtInOutFileInfo as vtioFI
    import vidarc.tool.InOut.vtInOutFileTools as vtioFT
    
    import vidarc.vTools.vExplorer.imgExp as img
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerCreateNewDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('lblRg', (0,0),(1,1),{'name':'lblDN','label':_(u'name')},None),
            ('txt',   (0,1),(1,3),{'name':'txtDN','value':u''},None),
            ('lblRg', (1,0),(1,1),{'name':'lblName','label':_(u'name')},None),
            ('txt',   (1,1),(1,3),{'name':'txtName','value':''},None),
            ('chk',   (2,1),(1,1),{'name':'chkFile','label':_(u'file'),},None),
            ('lblRg', (3,0),(1,1),{'name':'lblSuff','label':_(u'suffix')},None),
            ('chk',   (3,1),(1,1),{'name':'chkDate','label':_(u'date'),},None),
            ('chk',   (3,2),(1,1),{'name':'chkTime','label':_(u'time'),},None),
            #('lblRg', (1,0),(1,1),{'name':'lblExec','label':_(u'executable')},None),
            #('txt',   (1,1),(1,2),{'name':'txtExec','value':''},None),
            #('cbBmp', (1,3),(1,1),{'name':'cbChoose','bitmap':vtArt.getBitmap(vtArt.Browse),},
            #    {'btn':self.OnConfigApplChoose}),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExplorerCreateNew',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='frame',
                bmp=bmp or img.getPluginBitmap(),
                title=title or _(u'VIDARC Create New'),
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pnExplorerCreateNew',
                lGrowCol=[(1,1)],lGrowRow=None,
                bEnMark=False,bEnMod=False,lWid=lWid)
    def DoModal(self,sDN,iType=-1):
        try:
            pn=self.GetPanel()
            if iType>0:
                pn.chkFile.SetValue(True)
            elif iType==0:
                pn.chkFile.SetValue(False)
            pn.txtDN.SetValue(sDN)
            pn.txtName.SetValue(u'')
            pn.txtName.SetFocus()
            iRet=self.ShowModal()
            if iRet>0:
                bFile=pn.chkFile.GetValue()
                sDN=pn.txtDN.GetValue()
                sFN=pn.txtName.GetValue()
                bDate=pn.chkDate.GetValue()
                bTime=pn.chkTime.GetValue()
                if (bDate==True) or (bTime==True):
                    zNow=time.time()
                    tNow=time.gmtime(zNow)
                    l=[]
                    if bDate==True:
                        s=time.strftime("%Y%m%d",tNow)
                        l.append(s)
                    if bTime==True:
                        s=time.strftime("%H%M%S",tNow)
                        l.append(s)
                    iP=sFN.find('.')
                    if iP>=0:
                        sFN=u''.join([sFN[:iP],u'_',u'_'.join(l),sFN[iP:]])
                    else:
                        sFN=u''.join([sFN,u'_',u'_'.join(l)])
                sFull=u'/'.join([sDN,sFN])
                if bFile==True:
                    if vtioFI.exists(sFull)==0:
                        iR=vtioFT.makedirs(sDN)
                        if iR>0:
                            fOt=open(sFull,'w')
                            fOt.close()
                else:
                    iR=vtioFT.makedirs(sFull)
            return iRet
        except:
            self.__logTB__()
