#----------------------------------------------------------------------------
# Name:         vExplorerActionPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100215
# CVS-ID:       $Id: vExplorerRunPanel.py,v 1.2 2012/01/12 10:05:49 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import types
    
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmTreeListSel import vtmTreeListSel
    from vidarc.tool.misc.vtmThreadCtrl import vtmThreadCtrl
    from vidarc.tool.input.vtInputFloat import vtInputFloat
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.vtThread import vtThreadWX
    from vidarc.tool.time.vtTime import vtDateTime,vtTimeDiff
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerRunPanel(vtgPanel):
    ACT_CMD=100
    ACT_MCO=200
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append((vtmThreadCtrl,(0,0),(1,4),
                    {'name':'thdCtrl',},None))
        lWid.append(('szBoxHor',(1,0),(1,4),
                    {'name':'bxsProc',},[
                    #('lblRg',0,4,
                    #    {'name':'lblUpd','label':_('update time')},None),
                    (vtInputFloat,      1,4,    {
                        'name':u'txtDlyCls','default':0.0,'fraction_width':1,
                        'integer_width':4,'maximum':3600,'minimum':0.0,
                        'tip':_(u'update time [s], ')},None),
                    #('lblRg',0,4,
                    #    {'name':'lblUpdUnit','label':_('[s]')},None),
                    #('lblRg',1,4,
                    #    {'name':'lblCnt','label':_('count')},None),
                    ('txtRdRg',1,4,
                        {'name':'txtCnt','value':'','tip':_(u'process count')},None),
                    #('lblRg',1,4,
                    #    {'name':'lblActive','label':_('count')},None),
                    ('txtRdRg',1,4,
                        {'name':'txtAlm','value':'','tip':_(u'alarm count')},None),
                    ('txtRdRg',1,0,
                        {'name':'txtActive','value':'','tip':_(u'active process count')},None),
                    #('txtRdRg',2,4,
                    #{'name':'txtCur','value':''},None),
                    ]))
        lWidBt=[]
        lWidBt.append(('cbBmp',0,4,
                    {'name':'cbDel','bitmap':vtArt.getBitmap(vtArt.Del),},
                    {'btn':self.OnCbDelButton}))
        lWidBt.append(('cbBmp',0,4,
                    {'name':'cbView','bitmap':vtArt.getBitmap(vtArt.Glass),},
                    {'btn':self.OnCbViewButton}))
        lWid.append((vtmTreeListSel,(2,0),(1,4),
                    {'name':'trlstPrc','bButtons':True,'lWidBt':lWidBt},None))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        
        self.imgLst,self.dImg=self.trlstPrc.CreateImageList([
            ('empty'        ,vtArt.getBitmap(vtArt.Invisible)),
            ('busy'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('ok'           ,vtArt.getBitmap(vtArt.Apply)),
            ('err'          ,vtArt.getBitmap(vtArt.Cancel)),
            ('test'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('pkg'          ,vtArt.getBitmap(vtArt.Element)),
            ('cp'           ,imgExplorer.getCp1To2Bitmap()),
            ('mv'           ,imgExplorer.getMv1To2Bitmap()),
            ('del'          ,vtArt.getBitmap(vtArt.Del)),
            ('bth'          ,vtArt.getBitmap(vtArt.Build)),
            ('cmd'          ,vtArt.getBitmap(vtArt.Missile)),
            ('mco'          ,imgExplorer.getRecordBitmap()),
            ('stp'          ,vtArt.getBitmap(vtArt.Stop)),
            ('aborted'      ,vtArtState.getBitmap(vtArtState.Aborted)),
            ('running'      ,vtArtState.getBitmap(vtArtState.Running)),
            ('stopped'      ,vtArtState.getBitmap(vtArtState.Stopped)),
            ('error'        ,vtArtState.getBitmap(vtArtState.Exception)),
            ])
        font=self.GetFontByName('fixed')
        self.trlstPrc.SetFont(font)
        
        self.trlstPrc.AddColumn(text=_(u'name'))
        self.trlstPrc.AddColumn(text=_('PID'))
        self.trlstPrc.AddColumn(text=_('comment'))
        self.trlstPrc.AddColumn(text=_(''))
        self.trlstPrc.SetImageList(self.imgLst)
        self.trlstPrc.SetColumnWidth(1,60)
        self.trlstPrc.SetColumnWidth(3,40)
        self.trlstPrc.SetColumnAlignment(1,-2)
        self.trlstPrc.SetStretchLst([(0,-2),(2,-1)])
        self.trlstPrc.SetSelColumn(3,('X',''))
        self.trlstPrc.SetMap([('name',0),('pid',1),('comm',2)])
        self.trlstPrc.SetForceChildren(True)
        self.trlstPrc.SetValue([None,[(_(u'process'),self.dImg['cmd'])]],None)
        
        self.dProc={}
        self.applName=kwargs.get('applName','vExplorerRunPanel')
        return [(2,1)],[(1,1),(3,1)]
    def doAnswerKillProc(self,iAnswer,iPID):
        try:
            self.__logDebug__('iAnswer:%d;iPID:%d'%(iAnswer,iPID))
            if iAnswer==1:
                self.__logInfo__('kill process iPID:%d'%(iPID))
                if iPID in self.dProc:
                    oProc=self.dProc[iPID]
                    oProc.Kill()
                    #oProc.Close()
        except:
            self.__logTB__()
    def doAnswerKillProcAll(self,iAnswer):
        try:
            if iAnswer==1:
                self.bKillProcAll=True
        except:
            self.__logTB__()
    def OnCbDelButton(self,evt):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            def handleDelPrc(tup):
                dPrc=None
                tiPrc=None
                ti,td=tup
                if bDbg==True:
                    self.__logDebug__('td:%s'%(self.__logFmt__(td)))
                if td is None:
                    return
                t=type(td)
                if t==types.DictType:
                    iStatus=td.get('state',11)
                    if iStatus<=10:
                        iPID=td.get('pid','')
                        if iPID in self.dProc:
                            self.__logDebug__('proc:%d deleted'%(iPID))
                            oProc=self.dProc[iPID]
                            if oProc.IsRunning():
                                if self.bKillProcAll==False:
                                    iRet=self.ShowMsgDlg(self.YESNO,td.get('namefull','???')+u'\n\n'+_(u'Process still active!\nKill process?'),
                                        self.doAnswerKillProc,iPID)
                                else:
                                    self.doAnswerKillProc(1,iPID)
                            return
                        self.__logError__('%s iStatus:%d can not be deleted.'%(td.get('namefull','???'),
                                iStatus))
                        return
                    iPID=td.get('pid',None)
                    if iPID is not None:
                        if iPID in self.dProc:
                            self.dProc[iPID].Close()
                            del self.dProc[iPID]
                elif t==types.IntType:
                    # process tree item
                    if bDbg==True:
                        self.__logDebug__('dProc;%s'%(self.__logFmt__(self.dProc)))
                    if td in self.dProc:
                        oProc=self.dProc[td]
                        if oProc.IsRunning():
                            iRet=self.ShowMsgDlg(self.YESNO,_(u'Process still active!\nKill process?'),
                                    self.doAnswerKillProc,td)
                        return
                self.trlstPrc.Delete(ti)
                    
            tup=self.trlstPrc.GetSelected([-2,-1])
            bRoot=False
            if tup is None:
                bRoot=True
            else:
                if tup[1] is None:
                    bRoot=True
            self.bKillProcAll=False
            if bRoot:
                def getPrc(tr,ti,lPrc,lPrcAct):
                    tSel=tr.GetItem(ti,3)
                    if len(tSel[0])==0:
                        return 0
                    d=tr.GetItem(ti,-1)
                    t=type(d)
                    if t==types.DictType:
                        if d.get('state',0)<18:
                            lPrcAct.append(d.get('namefull','???'))
                        lPrc.append((ti,d))
                    return 0
                lPrc=[]
                lPrcAct=[]
                ti=self.trlstPrc.WalkBreathFirst(None,1,getPrc,lPrc,lPrcAct)
                if len(lPrcAct)>0:
                    iRet=self.ShowMsgDlg(self.YESNO,_(u'Processes (%d) still active!\nKill all processes?')%(len(lPrcAct)),
                            self.doAnswerKillProcAll)
                for tup in lPrc:
                    handleDelPrc(tup)
            else:
                handleDelPrc(tup)
        except:
            self.__logTB__()
    def OnCbViewButton(self,evt):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            tup=self.trlstPrc.GetSelected([-2,-1])
            if tup is not None:
                ti,td=tup
                if bDbg==True:
                    self.__logDebug__('td:%s'%(self.__logFmt__(td)))
                if td is None:
                    return
                t=type(td)
                iPID=None
                if t==types.DictType:
                    iPID=td.get('pid',None)
                elif t==types.IntType:
                    iPID=td
                if iPID is not None:
                    if iPID in self.dProc:
                        oProc=self.dProc[iPID]
                        oProc.Show(True)
        except:
            self.__logTB__()
    def FindProcTreeItem(self,iPID):
        try:
            if iPID is None:
                return None
            self.__logDebug__('iPID:%d'%(iPID))
            def cmpBth(tr,ti,iPID):
                d=tr.GetItem(ti,-1)
                if d.get('pid',None)==iPID:
                    return 1
                return 0
            ti=self.trlstPrc.WalkBreathFirst(None,1,cmpBth,iPID)
            return ti
        except:
            self.__logTB__()
        return None
    def OnProcCmd(self,evt):
        evt.Skip()
        try:
            sCmd=evt.GetCmd()
            dDat=evt.GetData()
            o=evt.GetEventObject()
            self.__logDebug__('cmd:%s;dDat:%s'%(repr(sCmd),
                        self.__logFmt__(dDat)))
            if sCmd=='state':
                if 'state' in dDat:
                    state=dDat['state']
                    pid=dDat.get('pid',None)
                    ti=self.FindProcTreeItem(pid)
                    if ti is None:
                        self.__logError__('tree item not found for pid:%r'%(pid))
                        return
                    iImg=self.dImg.get(state,-1)
                    if iImg<0:
                        self.dImg.get('err',0)
                    d=self.trlstPrc.GetItem(ti,-1)
                    self.trlstPrc.SetItem(ti,0,iImg)
                    self.trlstPrc.SetItem(ti,2,(dDat.get('msg',_(u'???')),self.dImg['empty']))
                    if state=='running':
                        d['state']=10
                    elif state=='stopped':
                        d['state']=19
                    elif state=='aborted':
                        d['state']=18
                    else:
                        self.__logError__('state unknown;%s'%(self.__logFmt__(dDat)))
                        return
                    iStatus=d.get('state',0)
                    self.__logDebug__('state:%d'%(iStatus))
                    #if iStatus>10:
                    #    iPID=d.get('pid','')
                    #    if iPID in self.dProc:
                    #        del self.dProc[iPID]
                    #        self.__logDebug__('proc:%d deleted'%(iPID))
                    #else:
                    #    if VERBOSE>10:
                    #        self.__logDebug__('dProc;%s'%(self.__logFmt__(self.dProc)))
        except:
            self.__logTB__()
    def AddProc(self,oProc,lHier=[],sName=None):
        try:
            self.__logDebug__(''%())
            sIdent=oProc.GetIdent()
            iPID=oProc.GetPID()
            if iPID in self.dProc:
                self.__logCritical__('pid:%d already in dProc'%(iPID))
            self.dProc[iPID]=oProc
            oProc.BindEvent('cmd',self.OnProcCmd)
            self.AddCmd(100,lHier,sName or _(u'???'),sIdent,iPID)
        except:
            self.__logTB__()
    def AddCmd(self,iAction,lHier,sName,sIdent,iPID):
        try:
            self.__logDebug__('iAction:%d;sName:%s;sIdent:%s'%(iAction,
                        sName,sIdent))
            sNameFull=_(u'%s:%8d')%(u'.'.join(lHier),iPID)
            d={
                'iAction'       :iAction,
                'name'          :sName,
                'namefull'      :sNameFull,
                'ident'         :sIdent,
                'pid'           :iPID,
                'state'         :0,
            }
            if iAction==self.ACT_CMD:
                sLbl=u'.'.join(lHier)
                iImg=self.dImg['cmd']
                lProc=[d,[(sLbl,iImg),unicode(iPID),(_(u'running'),self.dImg['busy'])],True,
                            [[iPID,[(sIdent,iImg)]]]
                        ]
                #iImg=self.dImg['pkg']
                #lH=[{},[(u'.'.join(lHier),iImg),'',''],True,[lProc]]
                
                self.trlstPrc.SetValue(lProc,[_(u'process')])
                ti=self.FindProcTreeItem(iPID)
                if ti is not None:
                    self.trlstPrc.EnsureVisible(ti)
                    self.trlstPrc.SelectItem(ti)
                #self.trlstPrc.SetValue(lH,[_(u'process')])
        except:
            self.__logTB__()
