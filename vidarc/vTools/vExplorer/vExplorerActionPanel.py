#----------------------------------------------------------------------------
# Name:         vExplorerActionPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100215
# CVS-ID:       $Id: vExplorerActionPanel.py,v 1.15 2010/07/19 09:48:35 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import sys,types,time

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmTreeListSel import vtmTreeListSel
    from vidarc.tool.misc.vtmThreadCtrl import vtmThreadCtrl
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.vtThread import vtThreadWX
    from vidarc.tool.time.vtTime import vtDateTime,vtTimeDiff
    #from vidarc.tool.InOut.fnUtil import *
    import vidarc.tool.InOut.vtInOutFileInfo as vtiLF
    import vidarc.tool.InOut.vtInOutFileTools as vtiFT
    from vidarc.tool.InOut.genFN import getCurDateTimeFN
    #from vidarc.tool.InOut.listFilesXml import *
    from vidarc.tool.InOut.vtInOutComp import vtInOutCompDirEntry
    #from vidarc.tool.InOut.vtInOutReadThread import *
    from vidarc.tool.InOut.vtInOutFileTimes import setFileTimes
    
    import vidarc.vTools.vExplorer.imgExp as imgExplorer
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VEXPLORERACTIONPANEL, wxID_VEXPLORERACTIONPANELTHDCTRL, 
 wxID_VEXPLORERACTIONPANELTRLSTBTH, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vExplorerActionPanel(vtgPanel):
    ACT_UNDEF=-1
    ACT_CP=5
    ACT_MV=6
    ACT_DEL=0
    ACT_CMD=100
    ACT_MCO=200
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=kwargs.get('lWid',[])
        lWid.append((vtmThreadCtrl,(0,0),(1,4),
                    {'name':'thdCtrl',},None))
        lWidBt=[]
        lWidBt.append(('cbBmp',0,4,
                    {'name':'cbDelBth','tip':_(u'delete batch or entry'),
                        'bitmap':vtArt.getBitmap(vtArt.Del),},
                    {'btn':self.OnCbDelBthButton}))
        lWidBt.append(('tgBmp',0,16,
                    {'name':'tgAutoStart','tip':_(u'auto start batch'),
                        'bitmap':vtArtState.getBitmap(vtArtState.Start),},
                    {'btn':self.OnTgAutoStartButton}))
        lWidBt.append(('tgBmp',0,4,
                    {'name':'tgAsk','tip':_(u'ask overwrite'),
                        'bitmap':vtArt.getBitmap(vtArt.Question),},
                    {'btn':self.OnTgAskButton}))
        lWid.append(('szBoxHor',(1,0),(1,4),
                    {'name':'bxsProc',},[
                    ('lblRg',0,4,
                    {'name':'lblCur','label':_('processing')},None),
                    ('txtRdRg',2,4,
                    {'name':'txtCur','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtElapsed','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtSpeed','value':''},None),
                    ('txtRdRg',1,4,
                    {'name':'txtEstimate','value':''},None),
                    ]))
        lWid.append((vtmTreeListSel,(2,0),(1,4),
                    {'name':'trlstBth','style':wx.TR_HAS_BUTTONS,'lWidBt':lWidBt},None))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        
        self.trlstBth.AddColumn(text=_(u'name'))
        self.trlstBth.AddColumn(text=_('size'))
        self.trlstBth.AddColumn(text=_('result'))
        self.trlstBth.AddColumn(text=_(''))
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdProc.SetValue('iSize',0)
        self.thdProc.SetValue('iCur',0)
        self.thdProc.SetValue('iCurStart',0)
        self.thdBth=vtThreadWX(self.trlstBth.GetMainWidget(),bPost=False)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        font=self.GetFontByName('fixed')
        self.trlstBth.SetFont(font)
        #self.trlstBth.SetAutoExpand(True)
        self.trlstBth.SetColumnWidth(1,60)
        self.trlstBth.SetColumnWidth(3,40)
        self.trlstBth.SetColumnAlignment(1,-2)
        self.trlstBth.SetStretchLst([(0,-2),(2,-1)])
        self.trlstBth.SetSelColumn(3,('X',''))
        self.trlstBth.SetMap([('name',0),('size',1),('res',2)])
        self.trlstBth.BindEvent('cmd',self.OnBthSelChanged)
        
        self.imgLst,self.dImg=self.trlstBth.CreateImageList([
            ('empty'        ,vtArt.getBitmap(vtArt.Invisible)),
            ('busy'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('ok'           ,vtArt.getBitmap(vtArt.Apply)),
            ('err'          ,vtArt.getBitmap(vtArt.Cancel)),
            ('test'         ,vtArt.getBitmap(vtArt.Diagnose)),
            ('pkg'          ,vtArt.getBitmap(vtArt.Element)),
            ('cp'           ,imgExplorer.getCp1To2Bitmap()),
            ('mv'           ,imgExplorer.getMv1To2Bitmap()),
            ('del'          ,vtArt.getBitmap(vtArt.Del)),
            ('bth'          ,vtArt.getBitmap(vtArt.Build)),
            ('cmd'          ,vtArt.getBitmap(vtArt.Missile)),
            ('mco'          ,imgExplorer.getRecordBitmap()),
            ('stp'          ,vtArt.getBitmap(vtArt.Stop)),
            ])
        self.dImgAct={
            self.ACT_CP:    (self.dImg['cp'],   _('copy')),
            self.ACT_MV:    (self.dImg['mv'],   _(u'move')),
            self.ACT_DEL:   (self.dImg['del'],  _(u'delete')),
            self.ACT_CMD:   (self.dImg['cmd'],  _(u'command')),
            self.ACT_MCO:   (self.dImg['mco'],  _(u'macro')),
            }
        
        self.trlstBth.SetImageList(self.imgLst)
        self.trlstBth.SetForceChildren(True)
        self.trlstBth.SetValue([None,[(_(u'batches'),self.dImg['bth'])]],None)
        
        #self.SetName(name)
        self.iBth=0
        self.dBth={}
        self.thdProc.SetValue('bAutoStart',kwargs.get('auto_start',True))
        self.thdProc.SetValue('bAsk',kwargs.get('ask',True))
        self.trlstBth.tgAutoStart.SetValue(self.thdProc.GetValue('bAutoStart'))
        self.zStart=vtDateTime()
        self.zNow=vtDateTime()
        self.zLastUpd=vtDateTime()
        self.zDiff=vtTimeDiff()
        self.__logSetProcBarVal__(0,100)
        #self.zLastUpt.Now()
        return [(2,1)],[(1,1),(3,1)]
    def OnCbDelBthButton(self,evt):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            def handleDelBth(tup):
                dBth=None
                tiBth=None
                ti,td=tup
                if bDbg==True:
                    self.__logDebug__('td:%s'%(self.__logFmt__(td)))
                t=type(td)
                if t==types.DictType:
                    iStatus=td.get('iStatus',10)
                    if iStatus<10:
                        return
                    iBth=td.get('bth',0)
                    if iStatus==10:
                        iSum=td.get('size',0)
                        self.thdProc.AddValue('iSize',-iSum)
                        #self.thdProc.AddValue('iCur',-iSum)
                    if iBth in self.dBth:
                        del self.dBth[iBth]
                elif t==types.LongType:
                    if dBth is None:
                        tp=ti
                        while tp is not None:
                            tp=self.trlstBth.GetItemParent(tp)
                            l=self.trlstBth.GetValue(None,tp)
                            if l is None:
                                continue
                            if len(l)!=1:
                                continue
                            tpd=l[0]
                            if dBth is None:
                                if type(tpd)==types.DictType:
                                    if bDbg==True:
                                        self.__logDebug__('tpd:%s'%(self.__logFmt__(tpd)))
                                    dBth=tpd
                                    tiBth=tp
                                    break
                    if dBth is not None:
                        if td<0:
                            td=-(td+1)
                        tpd['size']=max(0,dBth.get('size',0)-td)
                        self.thdProc.AddValue('iSize',-td)
                self.trlstBth.Delete(ti)
                if tiBth is not None:
                    if dBth is not None:
                        self.trlstBth.SetItem(tiBth,1,vtiLF.getSizeStr(dBth.get('size',0),2))
            tup=self.trlstBth.GetSelected([-2,-1])
            if tup is None:
                def getBth(tr,ti,lBth):
                    d=tr.GetItem(ti,-1)
                    lBth.append((ti,d))
                    return 0
                lBth=[]
                ti=self.trlstBth.WalkBreathFirst(None,0,getBth,lBth)
                for tup in lBth:
                    handleDelBth(tup)
            else:
                handleDelBth(tup)
            if self.__isLogDebug__():
                self.__logDebug__('dBth;%s'%(self.__logFmt__(self.dBth)))
        except:
            self.__logTB__()
    def OnTgAutoStartButton(self,evt):
        try:
            self.__logDebug__(''%())
            self.thdProc.SetValue('bAutoStart',self.trlstBth.tgAutoStart.GetValue())
        except:
            self.__logTB__()
    def GetAutoStart(self):
        try:
            return self.trlstBth.tgAutoStart.GetValue()
        except:
            self.__logTB__()
        return False
    def SetAutoStart(self,bFlag):
        try:
            self.trlstBth.tgAutoStart.SetValue(bFlag)
        except:
            self.__logTB__()
    def OnTgAskButton(self,evt):
        try:
            self.__logDebug__(''%())
            self.thdProc.SetValue('bAsk',self.trlstBth.tgAsk.GetValue())
        except:
            self.__logTB__()
    def GetAsk(self):
        try:
            return self.trlstBth.tgAsk.GetValue()
        except:
            self.__logTB__()
        return False
    def SetAsk(self,bFlag):
        try:
            self.trlstBth.tgAsk.SetValue(bFlag)
        except:
            self.__logTB__()
    def Start(self):
        try:
            self.__logDebug__(''%())
            self.__logProcDetails__()
            thd=self.thdCtrl.GetThread()
            thd.Start()
            self.zStart.Now()
            self.thdProc.SetValue('iCurStart',self.thdProc.GetValue('iCur'))
            def findBth2Start(tr,ti,trlst,lBth):
                d=tr.GetItem(ti,-1)
                if trlst.__isSel__(trlst,ti):
                    self.__logDebug__('dBth:%s'%(self.__logFmt__(d)))
                    iAction=d.get('iAction',99)
                    if (iAction>=0) and (iAction<100):
                        if 'size' in d:
                            if d.get('size')!=d.get('act',0):
                                lBth.append((d['bth'],ti,0))
                    elif iAction==100:
                        if d.get('iStatus',1)==10:
                            lBth.append((d['bth'],ti,1))
                            d['iStatus']=99
                return 0
            lBth=[]
            ti=self.trlstBth.WalkBreathFirst(None,0,findBth2Start,
                    self.trlstBth,lBth)
            self.thdCtrl.Do(self.procDummy)
            for iBth,ti,iType in lBth:
                if iType==0:
                    self.thdCtrl.Do(self.doProcBatch,ti,iBth,thd)
                elif iType==1:
                    self.thdCtrl.Do(self.doProcCmd,ti,iBth,thd)
                else:
                    self.thdCtrl.Do(self.doProcDummy,ti,iBth,thd)
        except:
            self.__logTB__()
    def procDummy(self):
        pass
    def Stop(self):
        try:
            self.__logDebug__(''%())
            self.thdProc.AddValue('iSize',-self.thdProc.GetValue('iCur'))
            self.thdProc.SetValue('iCur',0)
            self.__logProcDetails__()
        except:
            self.__logTB__()
    def __logProcDetails__(self):
        try:
            if self.__isLogDebug__():
                self.__logDebug__('dBth:%s;start:%s;  now:%s; last:%s; diff:%s;cur:%d;sta:%d; sz:%d'%(self.__logFmt__(self.dBth),
                    self.zStart,self.zNow,self.zLastUpd,self.zDiff.GetStr(),
                    self.thdProc.GetValue('iCur'),self.thdProc.GetValue('iCurStart'),
                    self.thdProc.GetValue('iSize')))
        except:
            self.__logTB__()
    def __showProcInfo__(self,iAct,iCnt):
        try:
            iAct=max(0,iAct)
            iCnt=max(0,iCnt)
            self.txtCur.SetValue(' / '.join([vtiLF.getSizeStr(iAct,2),
                            vtiLF.getSizeStr(iCnt,2)]))
            #self.txtElapsed.SetValue(self.zStart.CalcDiffStr(self.zNow))
            fSec=max(0.0,self.zStart.CalcDiffSecFloat(self.zNow))
            self.zDiff.SetDiffSec(fSec)
            self.txtElapsed.SetValue(self.zDiff.GetStr())
            iSz=iAct-self.thdProc.GetValue('iCurStart')
            if fSec>0:
                fSpeed=max(0.0,iSz/fSec)
            else:
                fSpeed=0
            self.txtSpeed.SetValue(vtiLF.getSizeStr(int(fSpeed),2)+ '/s')
            if VERBOSE>10:
                self.__logDebug__('act:%d;cnt:%d; sz:%d;sec:%f;spd:%f'%(iAct,iCnt,iSz,fSec,fSpeed))
            iSz=iCnt-iAct
            if fSpeed>0:
                fSec=max(0.0,iSz/fSpeed)
            else:
                fSec=0
            self.zDiff.SetDiffSec(fSec)
            self.txtEstimate.SetValue(self.zDiff.GetStr())
            if VERBOSE>10:
                self.__logDebug__('act:%d;cnt:%d; sz:%d;sec:%f;spd:%f'%(iAct,iCnt,iSz,fSec,fSpeed))
                self.__logProcDetails__()
        except:
            self.__logTB__()
    def OnThreadProc(self,evt):
        evt.Skip()
        try:
            if evt.GetCount() is None:
                t=evt.GetAct()
            #    print t
            else:
                self.zNow.Now()
                if self.zLastUpd.CalcDiffSecFloat(self.zNow)>1.0:
                    self.__showProcInfo__(evt.GetAct(),evt.GetCount())
                    self.zLastUpd.Now()
            #    print 'normalized',evt.GetNormalized(),evt.GetAct(),evt.GetCount()
        except:
            self.__logTB__()
    def OnThreadAborted(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.zNow.Now()
            self.__showProcInfo__(self.thdProc.GetValue('iCur'),self.thdProc.GetValue('iSize'))
            self.thdProc.AddValue('iSize',-self.thdProc.GetValue('iCur'))
            self.thdProc.SetValue('iCur',0)
            self.__logProcDetails__()
        except:
            self.__logTB__()
    def OnThreadFin(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.zNow.Now()
            self.__showProcInfo__(self.thdProc.GetValue('iCur'),self.thdProc.GetValue('iSize'))
            self.thdProc.AddValue('iSize',-self.thdProc.GetValue('iCur'))
            self.thdProc.SetValue('iCur',0)
            self.__logProcDetails__()
        except:
            self.__logTB__()
    def FindBatchTreeItem(self,iBth):
        try:
            self.__logDebug__('iBth:%d'%(iBth))
            def cmpBth(tr,ti,iBth):
                d=tr.GetItem(ti,-1)
                if d['bth']==iBth:
                    return 1
                return 0
            ti=self.trlstBth.WalkBreathFirst(None,0,cmpBth,iBth)
            return ti
        except:
            self.__logTB__()
        return None
    def buildAction(self,iAction,iImg,sSrcDN,sDstDN,lDN,lFN,
                thd,bDbg,l=None):
        try:
            if bDbg==True:
                self.__logDebug__('sSrcDN:%s;sDstDN:%s;lDN;%s;lFN:%s'%(sSrcDN,
                        sDstDN,self.__logFmt__(lDN),self.__logFmt__(lFN)))
            if l is None:
                l=[]
            iSzSum=0
            for sFN in lFN:
                dInfo=vtiLF.getFileInfoShort('/'.join([sSrcDN,sFN]))
                iSz=dInfo.get('size',-1)
                if iSz>0:
                    iSzSum+=iSz
                    ll=[iSz,[(sFN,iImg),vtiLF.getSizeStr(iSz,2)],True]
                else:
                    ll=[None,[(sFN,iImg),],True]
                l.append(ll)
            iSkip=len(sSrcDN)
            for sDN in lDN:
                d,iSzFN=vtiLF.readContense('/'.join([sSrcDN,sDN]))
                iSzSum+=iSzFN
                if bDbg:
                    self.__logDebug__('dn:%s;d:%s'%(sDN,self.__logFmt__(d)))
                lF=[]
                dFN=d.get('dFN')
                lTmp=dFN.keys()
                lTmp.sort()
                for sFN in lTmp:
                    iSz=dFN[sFN].get('size',-1)
                    if iSz>=0:
                        ll=[iSz,[(sFN,iImg),vtiLF.getSizeStr(iSz,2)],True]
                    else:
                        if dFN[sFN].get('type','dir')=='dir':
                            ll=[None,[(sFN,iImg),],True]
                    lF.append(ll)
                dDN=d.get('dDN')
                lTmp=dDN.keys()
                lTmp.sort()
                #lD=[]
                lT,iSzDN=self.buildAction(iAction,iImg,'/'.join([d['root']]),sDstDN,lTmp,[],thd,bDbg,lF)
                #for sTmpDN in lTmp:
                #    lD=[]
                #    self.buildAction(iAction,iImg,'/'.join([d['root'],sDN]),sDstDN,sTmp,[],thd,bDbg,lD)
                #    ll=[None,[(sTmpDN,iImg),],True,lD]
                #    lF.append(ll)
                iSzSum+=iSzDN
                ll=[-(iSzDN+iSzFN+1),[(sDN,iImg),vtiLF.getSizeStr(iSzDN+iSzFN,2)],True,lF]
                #ll=[None,[(sDN,iImg),vtiLF.getSizeStr(iSzDN+iSzFN,2)],True,lF]
                l.append(ll)
            if bDbg:
                self.__logDebug__('l;%s'%(self.__logFmt__(l)))
            return l,iSzSum
        except:
            self.__logTB__()
        return None,0
    def doBuildAction(self,ti,iBth,iAction,iImg,sSrcDN,sDstDN,lDN,lFN,
                    thd=None,bDbg=False):
        try:
            if thd is None:
                thd=self.thdCtrl.GetThread()
            self.__logProcDetails__()
            l,iSzSum=self.buildAction(iAction,iImg,sSrcDN,sDstDN,lDN,lFN,thd,bDbg)
            self.dBth[iBth]['size']=iSzSum
            self.dBth[iBth]['act']=0
            self.dBth[iBth]['iStatus']=10
            
            self.thdProc.AddValue('iSize',iSzSum)
            
            thd.CallBackWX(self.trlstBth.SetItem,ti,1,vtiLF.getSizeStr(iSzSum,2))
            #thd.CallBack(self.trlstBth.DeleteChildren,ti)
            thd.CallBackWX(self.trlstBth.SetValue,l,ti,bChildren=True)
            #thd.CallBack(self.trlstBth.SetItem,ti,-1,iSzSum)
            thd.CallBackWX(self.trlstBth.SetItem,ti,2,_(u'ready to start'))
            if self.thdProc.GetValue('bAutoStart')==True:
                thd.CallBack(self.thdCtrl.Start)
                thd.CallBack(self.thdCtrl.Do,self.doProcBatch,ti,iBth,
                        self.thdCtrl.GetThread())
            thd.CallBack(self.__logProcDetails__)
            ##self.thdCtrl.Do(self.doProcBatch,ti,iBth,
            ##            self.thdCtrl.GetThread())
        except:
            self.__logTB__()
    def procBatch(self,dBth,lTi,sSrcDN,sDstDN,thd,bDbg):
        iRet=0
        try:
            self.__logDebug__('iBth:%d'%(dBth['bth']))
            if bDbg:
                self.__logDebug__('sSrcDN:%r;sDstFN:%r'%(sSrcDN,sDstDN))
            iAction=dBth['iAction']
            iAct=dBth['act']
            iSize=dBth['size']
            #vtiFT.makedirs(sSrcDN,thd=thd)
            vtiFT.makedirs(sDstDN,thd=thd)
            for ti in lTi:
                if thd.Is2Stop():
                    return -99
                #print ti
                sFN,iSz,bExec=thd.CallBackWX(self.getBatchItemInfo,ti)
                if sFN is None:
                    continue
                if bDbg:
                    self.__logDebug__('sFN:%s;iSz:%r'%(sFN,iSz))
                sSrcFN='/'.join([sSrcDN,sFN])
                if sDstDN is not None:
                    sDstFN='/'.join([sDstDN,sFN])
                else:
                    sDstFN=None
                #print iAction,iSz,thd.GetValue('iCur'),thd.GetValue('iSize')
                #print '  ',sSrcFN
                #print '  ',sDstFN
                if (iSz is None) or (iSz<0):
                    lTc=thd.CallBackWX(self.getBatchItems,ti)
                    iR=self.procBatch(dBth,lTc,sSrcFN,sDstFN,thd,bDbg)
                    
                    thd.CallBackWX(self.setBatchItemResult,ti,iR)
                    if iR>=0:
                        if iAction==self.ACT_MV:
                            if vtiFT.removedirs(sSrcFN,thd=thd)<0:
                                iR=-10
                        elif iAction==self.ACT_DEL:
                            if vtiFT.removedirs(sSrcFN,thd=thd)<0:
                                iR=-10
                    if iR<0:
                        iRet=iR
                else:
                    if bExec==True:
                        if iAction==self.ACT_CP:
                            if thd.GetValue('bAsk'):
                                if vtiLF.exists(sDstFN):
                                    self.__logInfo__('overwrite:%s'%(sDstFN))
                            iR=vtiFT.copy(sSrcFN,sDstFN,thd,
                                    iCur=thd.GetValue('iCur'),
                                    iSz=thd.GetValue('iSize'))
                        elif iAction==self.ACT_MV:
                            iR=vtiFT.move(sSrcFN,sDstFN,thd,
                                    iCur=thd.GetValue('iCur'),
                                    iSz=thd.GetValue('iSize'))
                        #elif iAction==self.ACT_MV:
                        #    iR=vtiFT.move(sSrcFN,sDstFN,thd)
                        elif iAction==self.ACT_DEL:
                            iR=vtiFT.remove(sSrcFN,thd)
                        else:
                            iR=-1
                        if iR<0:
                            iRet=iR
                        else:
                            thd.AddValue('iCur',iSz)
                            dBth['act']=dBth['act']+iSz
                        thd.CallBackWX(self.setBatchItemResult,ti,iR)
        except:
            self.__logTB__()
        return iRet
    def setBatchItemResult(self,ti,iRes):
        try:
            if iRes>=0:
                iImg=self.dImg['ok']
                iSz=self.trlstBth.GetItem(ti,-1)
                if type(iSz)==types.DictType:
                    pass
                else:
                    if iSz is not None:
                        self.trlstBth.SetItem(ti,-1,-iSz)
                s=_(u'okay')
            elif iRes==-99:
                iImg=self.dImg['stp']
                s=_(u'manually stopped')
            else:
                iImg=self.dImg['err']
                s=_(u'unspecified error')
            self.trlstBth.SetItem(ti,0,iImg)
            self.trlstBth.SetItem(ti,2,(s,self.dImg['empty']))
            self.trlstBth.SetItem(ti,3,'')
            #self.trlstBth.SelectItem(ti)
        except:
            self.__logTB__()
    def getBatchItemInfo(self,ti):
        try:
            iSz=self.trlstBth.GetItem(ti,-1)
            t=self.trlstBth.GetItem(ti,3)
            if t[0]!='X':
                bExec=False
            else:
                if iSz>=0:
                    bExec=True
                else:
                    bExec=False
                #return None,iSz,False
            t=self.trlstBth.GetItem(ti,0)
            sN=t[0]
            return sN,iSz,bExec
        except:
            self.__logTB__()
        return None,None,False
    def getBatchItems(self,ti):
        try:
            lTi=[]
            def getItem(tr,ti,l):
                #t=tr.GetItem(ti,0)
                l.append(ti)
                return 0
            self.trlstBth.WalkBreathFirst(ti,0,getItem,lTi)
            return lTi
        except:
            return None
    def setBatchStart(self,ti):
        try:
            td=self.trlstBth.GetItem(ti,-1)
            self.trlstBth.SetItem(ti,2,(_(u'processing ...'),self.dImg['busy']))
            lTi=self.getBatchItems(ti)
            return td,lTi
        except:
            return None,None
    def doProcDummy(self,ti,iBth,thd):
        try:
            sBthName=None
            self.__logDebug__('iBth:%d'%(iBth))
            bDbg=False
            if iBth in self.dBth:
                dBth=self.dBth[iBth]
                dBth['iStatus']=1
            else:
                return 0
            td,lTi=thd.CallBackWX(self.setBatchStart,ti)
            sBthName,iTmp=thd.CallBackWX(self.trlstBth.GetItem,ti,0)
            self.__logPrintMsg__(_(u'processing, %s')%(sBthName))
            # +++++ do stuff here
            iRet=1
            # ----- do stuff here
            thd.CallBackWX(self.setBatchItemResult,ti,iRet)
            self.__logPrintMsg__(_(u'finished, %s')%(sBthName))
        except:
            self.__logTB__()
            self.__logPrintMsg__(_(u'batch %d execution faulty.')%(iBth))
        return 0
    def doProcBatch(self,ti,iBth,thd):
        try:
            sBthName=None
            self.__logDebug__('iBth:%d'%(iBth))
            bDbg=False
            if iBth in self.dBth:
                dBth=self.dBth[iBth]
                dBth['iStatus']=1
            else:
                return 0
            
            td,lTi=thd.CallBackWX(self.setBatchStart,ti)#self.trlstBth.GetItem,ti,-1)
            #print td
            self.__logProcDetails__()
            sBthName,iTmp=thd.CallBackWX(self.trlstBth.GetItem,ti,0)
            self.__logPrintMsg__(_(u'processing, %s')%(sBthName))
            sSrcDN=td['sSrcDN']
            sDstDN=td['sDstDN']
            if sSrcDN[-1]!='/':
                sSrcTmpDN=sSrcDN[:]+'/'
            else:
                sSrcTmpDN=sSrcDN[:]
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('dBth:%s'%(self.__logFmt__(td)))
                    bDbg=True
            iRet=0
            if sSrcDN is None:
                self.__logError__('sSrcDN is None'%())
                iRet=-10
            elif vtiLF.exists(sSrcTmpDN)<1:
                self.__logError__('sSrcDN:%s does not exist'%(sSrcDN))
                iRet=-11
            if (dBth['iAction'] not in [self.ACT_DEL]) and (sDstDN is None):
                self.__logError__('sDstDN is None'%())
                iRet=-20
            sSrcTmpDN=None
            if iRet==0:
                iRet=self.procBatch(td,lTi,sSrcDN,sDstDN,thd,bDbg)
            bFin=False
            try:
                d=self.dBth[iBth]
                if d.get('size')==d.get('act',0):
                    bFin=True
                    d['iStatus']=19
                if iRet<0:
                    d['iStatus']=18
            except:
                bFin=True
            if (bFin==True) or (iRet<0):
                thd.CallBackWX(self.setBatchItemResult,ti,iRet)
            else:
                thd.CallBackWX(self.trlstBth.SetItem,ti,2,_(u'ready to restart'))
            self.__logPrintMsg__(_(u'finished, %s')%(sBthName))
            self.__logSetProcBarVal__(0,100)
            self.__logProcDetails__()
        except:
            self.__logTB__()
            self.__logPrintMsg__(_(u'batch %d execution faulty.')%(iBth))
        return 0
    def doProcCmd(self,ti,iBth,thd):
        try:
            sBthName=None
            self.__logDebug__('iBth:%d'%(iBth))
            bDbg=False
            if iBth in self.dBth:
                dBth=self.dBth[iBth]
                dBth['iStatus']=1
            else:
                return 0
            td,lTi=thd.CallBackWX(self.setBatchStart,ti)
            sBthName,iTmp=thd.CallBackWX(self.trlstBth.GetItem,ti,0)
            self.__logPrintMsg__(_(u'processing, %s')%(sBthName))
            # +++++ do stuff here
            func=dBth['func']
            args=dBth['args']
            kwargs=dBth['kwargs']
            zDly=dBth['zDly']
            while zDly>0:
                time.sleep(0.1)
                zDly-=0.1
                if thd.Is2Stop()==True:
                    thd.CallBackWX(self.setBatchItemResult,ti,-99)
                    self.__logPrintMsg__(_(u'finished, %s')%(sBthName))
                    return -99
            oProc=thd.CallBackWX(func,*args,**kwargs)
            iRet=1
            if oProc is None:
                iRet=-1
            else:
                iRet=1
                while oProc.IsRunning():
                    time.sleep(0.1)
                dBth['iStatus']=19
            # ----- do stuff here
            thd.CallBackWX(self.setBatchItemResult,ti,iRet)
            self.__logPrintMsg__(_(u'finished, %s')%(sBthName))
        except:
            self.__logTB__()
            self.__logPrintMsg__(_(u'batch %d execution faulty.')%(iBth))
        return 0
    def AddCmd(self,iAction,sName,zDly,func,*args,**kwargs):
        try:
            self.__logDebug__('iAction:%d;sName:%s'%(iAction,sName))
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('func:%r;args:%s;kwargs:%s'%(func,
                            self.__logFmt__(args),self.__logFmt__(kwargs)))
                    bDbg=True
            iImg,sAct=self.dImgAct.get(iAction,(-1,_(u'unknown action')))
            if iImg==-1:
                self.__logCritical__('iAction:%d;dAct:%s'%(iAction,
                        self.__logFmt__(self.dImgAct)))
                return -1
            
            self.__logProcDetails__()
            d={
                'bth':self.iBth,
                'sName':sName,
                'func':func,
                'args':args,
                'kwargs':kwargs,
                'iAction':iAction,
                'iStatus':10,
                'zDly':zDly,
                }
            
            sLbl=u''.join([_(u'batch:%d %s')%(self.iBth,sName)])
            if iAction==self.ACT_CMD:
                self.trlstBth.SetValue([d,[(sLbl,iImg),'',(_(u'scheduled'),self.dImg['busy'])],True,
                            [[None,[(sName,self.dImg['cmd'])]]]
                        ],
                        [_(u'batches')])
            else:
                return -1
            self.dBth[self.iBth]=d
            ti=self.FindBatchTreeItem(self.iBth)
            if self.thdProc.IsBusy()==False:
                self.__logDebug__('set zStart to now')
                self.zStart.Now()
            if ti is not None:
                self.trlstBth.EnsureVisible(ti)
                self.trlstBth.SelectItem(ti)
            self.iBth+=1
            #thd.CallBackWX(self.trlstBth.SetItem,ti,2,_(u'ready to start'))
            if self.thdProc.GetValue('bAutoStart')==True:
                self.thdBth.CallBack(self.Start)
            self.__logProcDetails__()
        except:
            self.__logTB__()
        return 0
    def AddAction(self,iAction,sSrcDN,sDstDN,lDN,lFN):
        try:
            self.__logDebug__('iAction:%d'%(iAction))
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('sSrcDN:%r;sDstDN:%r;lDN:%s;lFN:%s'%(sSrcDN,sDstDN,
                            self.__logFmt__(lDN),self.__logFmt__(lFN)))
                    bDbg=True
            if (lDN is None) and (lFN is None):
                return
            iImg,sAct=self.dImgAct.get(iAction,(-1,_(u'unknown action')))
            if iImg==-1:
                self.__logCritical__('iAction:%d;dAct:%s'%(iAction,
                        self.__logFmt__(self.dImgAct)))
                return -1
            if sDstDN is None:
                sLbl=u''.join([_(u'batch:%d %s (%s)')%(self.iBth,sAct,sSrcDN)])
            else:
                sLbl=u''.join([_(u'batch:%d %s (%s->%s)')%(self.iBth,sAct,sSrcDN,sDstDN)])
            
            if sSrcDN[-1]=='/':
                sSrcDN=sSrcDN[:-1]
            if sDstDN is not None:
                if sDstDN[-1]=='/':
                    sDstDN=sDstDN[:-1]
            
            self.__logProcDetails__()
            d={
                'bth':self.iBth,
                'sSrcDN':sSrcDN,
                'sDstDN':sDstDN,
                'iAction':iAction,
                'iStatus':0,
                }
            self.trlstBth.SetValue([d,[(sLbl,iImg),'',(_(u'calculate ...'),self.dImg['busy'])],True,
                        #[[None,[(_(u'calculate ...'),self.dImg['busy'])]]]
                    ],
                    [_(u'batches')])
            self.dBth[self.iBth]=d
            ti=self.FindBatchTreeItem(self.iBth)
            if self.thdProc.IsBusy()==False:
                self.__logDebug__('set zStart to now')
                self.zStart.Now()
            self.thdBth.Do(self.doBuildAction,ti,self.iBth,iAction,iImg,
                        sSrcDN,sDstDN,lDN,lFN,self.thdBth,bDbg)
            if ti is not None:
                self.trlstBth.EnsureVisible(ti)
                self.trlstBth.SelectItem(ti)
            self.iBth+=1
            self.__logProcDetails__()
        except:
            self.__logTB__()
        return 0
    def OnBthSelChanged(self,evt):
        try:
            sCmd=evt.GetCmd()
            ti=evt.GetData()
            td=self.trlstBth.GetItem(ti,-1)
        except:
            self.__logTB__()
