#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerSynch.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vXmlNodeExplorerSynch.py,v 1.1 2010/05/21 22:58:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerSynchPanel import vXmlNodeExplorerSynchPanel
        from vExplorerSynchFrame import vExplorerSynchFrame
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerSynch(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerSynch'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'explorer synchronise')
    # ---------------------------------------------------------
    # specific
    #def GetApp(self,node):
    #    return self.Get(node,'app')
    def GetSrc1(self,node):
        return self.Get(node,'src1')
    def GetSrc2(self,node):
        return self.Get(node,'src2')
    def GetRec(self,node):
        return self.Get(node,'recursion')
    def GetRecVal(self,node,fallback=-1):
        return self.GetVal(node,'recursion',float,fallback=fallback)
    def GetCalcMD5(self,node):
        return self.Get(node,'calcMD5')
    def GetCalcMD5Val(self,node,fallback=1):
        return self.GetVal(node,'calcMD5',float,fallback=fallback)
    def GetCalcAlwaysMD5(self,node):
        return self.Get(node,'calcAlwaysMD5')
    def GetCalcAlwaysMD5Val(self,node,fallback=0):
        return self.GetVal(node,'calcAlwaysMD5',float,fallback=fallback)
    def GetAccess(self,node):
        return self.Get(node,'access')
    def GetAccessVal(self,node,fallback=0):
        return self.GetVal(node,'access',float,fallback=fallback)
    def GetMod(self,node):
        return self.Get(node,'mod')
    def GetModVal(self,node,fallback=1):
        return self.GetVal(node,'mod',float,fallback=fallback)
    def GetCreate(self,node):
        return self.Get(node,'create')
    def GetCreateVal(self,node,fallback=0):
        return self.GetVal(node,'create',float,fallback=fallback)
    def GetAutoExpand(self,node):
        return self.Get(node,'autoExpand')
    def GetAutoExpandVal(self,node,fallback=1):
        return self.GetVal(node,'autoExpand',float,fallback=fallback)
    def GetShowEqual(self,node):
        return self.Get(node,'showEqual')
    def GetShowEqualVal(self,node,fallback=0):
        return self.GetVal(node,'showEqual',float,fallback=fallback)
    def GetFlat(self,node):
        return self.Get(node,'flat')
    def GetFlatVal(self,node,fallback=0):
        return self.GetVal(node,'flat',float,fallback=fallback)
    
    #def SetApp(self,node,val):
    #    self.Set(node,'app',val)
    def SetSrc1(self,node,val):
        self.Set(node,'src1',val)
    def SetSrc2(self,node,val):
        self.Set(node,'src2',val)
    def SetRec(self,node,val):
        self.Set(node,'recursion',val)
    def SetRecVal(self,node,val):
        self.SetRec(node,unicode(val))
    def SetCalcMD5(self,node,val):
        self.Set(node,'calcMD5',val)
    def SetCalcAlwaysMD5(self,node,val):
        self.Set(node,'calcAlwaysMD5',val)
    def SetAccess(self,node,val):
        self.Set(node,'access',val)
    def SetMod(self,node,val):
        self.Set(node,'mod',val)
    def SetCreate(self,node,val):
        self.Set(node,'create',val)
    def SetAutoExpand(self,node,val):
        self.Set(node,'autoExpand',val)
    def SetShowEqual(self,node,val):
        self.Set(node,'showEqual',val)
    def SetFlat(self,node,val):
        self.Set(node,'flat',val)
    
    def GetExec(self,node,sFN=None):
        #sTag=self.GetTag(node)
        #if node is not None:
        #    sApp=self.GetApp(node)
        return '','',None,[]
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['appl_truecrypt','root','pwd']
        else:
            return sTag,sFN,'"'+sApp+'" /v "'+oLocalRoot.ConvToOS(sFN)+'" /l %s /p "%s" /s /q ',\
                    ['root','pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        if node is not None:
            sApp=self.GetApp(node)
        else:
            sApp=''
        oLocalRoot=self.doc.GetRegisteredNode('ExplorerRoot')
        sDN,sTag=oLocalRoot.SplitFN(sFN)
        if len(sApp)==0:
            return sTag,sFN,'"%s" /d %s /s /q',\
                    ['appl_truecrypt','root']
        else:
            return sTag,sFN,'"'+sApp+' /d %s /s /q',\
                    ['root']
    def GetApplName(self):
        return 'appl_truecrypt'
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
            frm=vExplorerSynchFrame(parent)
            frm.Centre()
            sDN1=self.GetSrc1(node)
            sDN2=self.GetSrc2(node)
            iRec=self.GetRecVal(node)
            bMD5=self.GetCalcMD5Val(node)
            bMD5Always=self.GetCalcAlwaysMD5Val(node)
            bAccess=self.GetAccessVal(node)
            bMod=self.GetModVal(node)
            bCreate=self.GetCreateVal(node)
            bAutoExpand=self.GetAutoExpandVal(node)
            bShowEqual=self.GetShowEqualVal(node)
            bFlat=self.GetFlatVal(node)
            frm.SetValue(sDN1,sDN2,iRec,bMD5,bMD5Always,
                    bAccess,bMod,bCreate,bAutoExpand,bShowEqual,bFlat)
            frm.Show()
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x0fIDAT8\x8d\x95\x93MN\x02A\x10\x85\xbf\xd7\xd3\x84\x10p\xe5\xc4\
\xe0\xcaHH\xbc\x91\xb7p)aeX\xa81s\x02\xe3Y\xbc\x827\x181D]\x1a\x17\n\xa8\x88\
\xe5b\xc4\xcc/0/\xe9MU\xbd\xee\xf7\xaa\xaa\x91\x0b\xd8\xe60<\xb2\xb2\xb8\xa3\
\x06l\xd0\xb7|\xcco[X\x05\xc9\x05\xb5\x89\x8ab\x15\x14\xd8Y\xcfp\xcb\xf5L\
\x13\xcc\xb3\xae=\x80]\x1e\x18\xe1,\x89,\x1c\xcc\x1a\xe8t"\x00\xbb\xe9fU\xc5\
\x9d\xe2\x05\x00\x84\x1f\xf0\xdcA\'O\xff\xf2\xec\xb8k\xdc\xe6e\xbc\'\xb9A\
\xdf\x14\xc5\x12W\xbd\xe4\x85/\x87F\x0f\xca\x97\x97:\xf9\xeb\x95\xa2X\x9e\
\x9doxm\xa2\xd1X\xe9d\x1a\xe9\xa6\xe5\xf3\x9e\xc7\x16\x8abUM\xa0\x8a\xbc\x8a\
\xfbt\xc1:\xb9U\xc8\xee\xc1\xf9\xa1\xe16\xac\xc24@\x17\xf7\xc5=\x00\xa0\xb5\
\x84\xbdy9\xf1\xad\x01/\xcd\x0c\xb9x\xc1\xd4\xc3\xee\'\xdc\x85\xd0^\xc0\x8f\
\xc0Y2\xa1\xe1\xa4\xd4j\xc6\x02\x80]\xef\x1b\xe36\x9bz\xb3B\xf17\xd6 \x03\
\xfc\x02\xc6\x11bF\xfe\xeaL9\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerSynchPanel
        else:
            return None
