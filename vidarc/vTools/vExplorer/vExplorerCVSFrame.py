#----------------------------------------------------------------------------
# Name:         vExplorerCVSFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100531
# CVS-ID:       $Id: vExplorerCVSFrame.py,v 1.3 2010/06/14 10:07:17 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.gui.vtgFrame import vtgFrame
    import vidarc.vTools.vExplorer.imgExp as img
    
    from vidarc.vTools.vExplorer.vExplorerCVSPanel import vExplorerCVSPanel
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerCVSFrame(vtgFrame):
    def __init__(self, parent,dPos=None,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None,zClose=None,zClsSleep=0.5):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtgFrame.__init__(self,parent,_(u'VIDARC Explorer CVS'),
                vExplorerCVSPanel,name=name or 'dlgExpCVS',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                bmp=None or img.getPluginBitmap(),
                pnPos=None,pnSz=(300,400),pnStyle=None,
                pnName='pnExpCVS')
        self.pn.applName=_(u'VIDARC Explorer CVS')
    def SetValue(self,oReg,sAppl,sDN):
        self.pn.SetValue(oReg,sAppl,sDN)
    def OnCbCloseButton(self,evt):
        try:
            if self.IsBusy():
                self.__logInfo__('thd busy, close prevented'%())
                sMsg=_(u"cvs busy, cann't close frame yet.")
                self.__logPrintMsg__(sMsg)
                sMsg2=_(u"stop processing first.")
                self.pn.ShowMsgDlg(self.EXCLAMATION|self.OK,
                            u'\n'.join([sMsg,sMsg2]),None)
                return
            vtgFrame.OnCbCloseButton(self,evt)
            self.Destroy()
        except:
            self.__logTB__()
    def OnFrameClose(self,evt):
        try:
            if self.IsBusy():
                self.__logInfo__('thd busy, close prevented'%())
                sMsg=_(u"cvs busy, cann't close frame yet.")
                self.__logPrintMsg__(sMsg)
                sMsg2=_(u"stop processing first.")
                self.pn.ShowMsgDlg(self.EXCLAMATION|self.OK,
                            u'\n'.join([sMsg,sMsg2]),None)
                return
            vtgFrame.OnFrameClose(self,evt)
            self.Destroy()
        except:
            self.__logTB__()
