#----------------------------------------------------------------------------
# Name:         vXmlNodeExplorerNetWork.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100227
# CVS-ID:       $Id: vXmlNodeExplorerNetWork.py,v 1.3 2010/04/03 21:40:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.InOut.vtInOutNetTools as vtiNT

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    #GUI=0
    if vcCust.is2Import(__name__):
        from vXmlNodeExplorerNetWorkPanel import vXmlNodeExplorerNetWorkPanel
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeExplorerNetWork(vtXmlNodeTag):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='ExplorerNetWork'):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'network')
    # ---------------------------------------------------------
    # specific
    def GetApp(self,node):
        return self.Get(node,'app')
    def GetClrPwd(self,node):
        return self.Get(node,'clrpwd')
    def GetClrPwdVal(self,node,fallback=1):
        return self.GetVal(node,'clrpwd',int,fallback=fallback)
    def GetReCall(self,node):
        return self.Get(node,'recall')
    def GetReCallVal(self,node,fallback=1):
        return self.GetVal(node,'recall',int,fallback=fallback)
    def GetMnt(self,node):
        return self.Get(node,'mnt')
    def GetSrc(self,node):
        return self.Get(node,'src')
    def GetCfg(self,node):
        return self.Get(node,'cfg')
    
    def SetApp(self,node,val):
        self.Set(node,'app',val)
    def SetClrPwd(self,node,val):
        self.Set(node,'clrpwd',val)
    def SetReCall(self,node,val):
        self.Set(node,'recall',val)
    def SetMnt(self,node,val):
        self.Set(node,'mnt',val)
    def SetSrc(self,node,val):
        self.Set(node,'src',val)
    
    def procMnt(self,sSrc,sMnt,t,thd=None):
        try:
            self.__logDebug__('lSrc:%s;sMnt:%s;thd:%r'%(sSrc,sMnt,thd))
            sUsr,sPwd=t
            return vtiNT.SambaMnt(sSrc,sMnt,sUsr,sPwd,thd=thd)
        except:
            self.__logTB__()
            return -1
    def procUnMnt(self,sMnt,thd=None):
        try:
            self.__logDebug__('sMnt:%s'%(sMnt))
            # perform several times due notify hook
            for iTry in xrange(120):
                ret=vtiNT.SambaUnMnt(sMnt,iForce=0,thd=thd)
                if ret>=0:
                    return ret
            return ret
        except:
            self.__logTB__()
            return -1
    def GetExec(self,node,sFN=None):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return sHost,sFN,(self.procMnt,(sFN,),{'thd':'self'}),['drive','usr_pwd']
    def GetKill(self,node,sFN=None,bForce=False):
        if sFN is None:
            sFN=self.GetSrc(node)
        i=sFN.find('\\',2)
        if i>0:
            sHost=sFN[2:i]
        else:
            sHost=sFN
        return sHost,sFN,(self.procUnMnt,(),{'thd':'self'}),['drive']
    def GetApplName(self):
        return '__method'
    # ---------------------------------------------------------
    # inheritance
    def GetData(self,node,dImg={},**kwargs):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        try:
            self.__logDebug__(''%())
            lHdr=[(_(u'Name'),-1,None,1)]
            dSortDef={-1:{'iSort':2}}
            lData=[]
            lNet=vtiNT.getHostLst(None,None)
            sTagName=self.GetTagName()
            for dVal in lNet:
                sLbl=dVal.get('name','???')
                l=[{0:sLbl,'__reg':'ExplorerNetHost','host':sLbl},[
                    (sLbl,self.__getImgFromDict__(dImg,'elem','ExplorerNetHost')),
                    ]]
                lData.append(l)
            def cmpDat(a,b):
                return cmp(a[0][0],b[0][0])
            lData.sort(cmpDat)
            return (lData,lHdr,dSortDef,True)
        except:
            self.__logTB__()
        return (None,None,None,True)
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02:IDAT8\x8d\x95\x91OH\xd3q\x18\x87\x9f\xefo\xbf\xad\\:u\xcda\xb9\r\
\xcc.\xced)y\xd0P*\xed\x90\x88d\xd1\xb9? Tt\xf4\x9a.;xQ7\xe7L\xd4\xe8bx(Z\
\xd1ILo\xddD\xb0H0j\x889\x9dn\xed\x97\x7f\xa6h\xba};\x15.-\xf4\x85\xf7\xf2\
\xf2>\xcf\xfb\x81W\x08E\xc7a\xca\xeb\xe9\x94\xaa\xaa\xa2\xaa*w\xef\xdd\x17\
\xe20\x02_\x97W&\x93I\x1cv;\xab\xf18K\x91\x08\xeaAa\xbf\xd7\'eR!?\xdf\x86\
\xa6\xc5\x88,\xaf\x11\x0cG\xff/\x18y7,\xb5\r\x8d\x99\x95\x19|Z/\r\'+\xb0\xac\
d\xb1\xf4c\x95\xcf\xb3s\xd8\xcd\xa6\xfd\x05\x03C}r81L\xe3z#QC\x94D\x86\xc0\
\xa6\xdf\xc2e\xfdD\xe8}\x84\xe9\xb0\x13\x87\xd5D\xab\xdb-\xf6\x08\x9e\x0e\
\xf6\xcb&[\x13\xc6i#\x95\xb3\x95\x14\x18\x0b\xb0\x18\xb2\xc90z92\x15b\xf1\
\xe5\x04\xf6\xab\xa5\xb4\xba\xdd\x02\x00\xa1\xe8R\xba\xbe\xbf^\x9a\x07\xcd2\
\xf0& \x7f\xcf<\x1d\rr\xe8\x05\xf2\xd9i!{\xda\xda\xe4\xee}e\xf7\xf5\xd1\xd1\
\x119~|\x9c\x9ap\r\xd7\xaf\xdd\x10\x00~\xdf\x03\xe9,\x7fM\xe8\x95\x91\xa4\
\x96\xcd)\x97+%q\x8a@[\xd3\x88\x98"\x14\xa6\x17\x02\xf0\xc4\x7fGVTv31YBr\xf3\
"\x9b\x16\x0bG\xd3\xd2\xfe-\xf8\x1a\xfb\x8a"\rd`\xa0\xd7\x7fA\x9ar\x07\x08\
\x0c\x97S\xecx\x84yn\x91\x9f6\x1b\x97\xaak\xc4\xbe\x82\xaeN\x8f\x0c\x88\xb7\
\x14\'\x12\xe4dz\xd8\xda\x1ec2p\x8e\\\xed<\x91\xaen\x8a>~@\xad\xaa\xda\xf31!\
\x14\x1d^O\xa7\xd4\xabi\xec\x9c\tcW\x1f\x13\x1f\xdbA\xed\x01]\xfc\x18\xeb\
\x8aB</\x8f\x9d\xdaZ\\uuT\xd7\\NI \xbc^\xaf4\x18\xf48\x9dE\xc4W6\xf82\xf5\
\x9c\xed\xbe\x11D\xd5\x15\xcceeX\x1d\x0e\xd233\xf7D\xff#h\xefh\x97\xa5gK\xf8\
\x1e\x8b\x11\x9c[ \xf8-\x8a\xc3b\xa4\xa5\xa5y_\xe0\xefR\xad9V\xe6\xe6\xe7Y\
\xd2\x96\t\x86\x16\xc8\xcb\xc9\xa2\xa5\xf9\xe1\x81`\x00\xe5\xe6\xad\xdb\xc2\
\xa0\xd7\x13\n/q"\xdbD\xeb!`\x80_\x82\x9b\xd3A\xfa\xb8\xc4\x82\x00\x00\x00\
\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    #'pnClsAdd':vtXmlNodeTag.GetPanelClass(self),
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeExplorerNetWorkPanel
        else:
            return None
    #def Build(self):
    #    self.__logDebug__(''%())
    #    self.doc.RegisterNetProtocol('smb',self)
