#----------------------------------------------------------------------------
# Name:         vExplorerSynchLogDlg
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vExplorerSynchLogDlg.py,v 1.1 2010/05/21 22:58:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.misc.vtmListCtrl import vtmListCtrl
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vExplorerSynchLogDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            (vtmListCtrl,      (0,0),(1,1),{'name':'lstLog',
                'cols':[
                    (_(u''),        -2,20,None,None),
                    (_(u'time'),    -1,80,None,None),
                    (_(u'log'),     -1,400,None,None),
                    ]},None),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgExplorerSynchResult',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='dlg',
                bmp=bmp,
                title=title or _(u'VIDARC Explorer Synch Log'),
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pnExplorerSynchLog',
                lGrowCol=[(0,1),],lGrowRow=[(0,1),],
                bEnMark=False,bEnMod=False,lWid=lWid)
        self.imgLst,self.dImg=self.lstLog.CreateImageList([
            ('def',     vtArt.getBitmap(vtArt.Apply)),
            ('warn',    vtArt.getBitmap(vtArt.Warning)),
            ('error',   vtArt.getBitmap(vtArt.Error)),
            ('quest',   vtArt.getBitmap(vtArt.Question)),
            ('empty',   vtArt.getBitmap(vtArt.Empty)),
            ('in',      vtArt.getBitmap(vtArt.Right)),
            ('out',     vtArt.getBitmap(vtArt.Left)),
            ('proc',    vtArt.getBitmap(vtArt.Build)),
            ])
        self.lstLog.SetImageList(self.imgLst)
        self.lstLog.SetStretchLst([(2,-1)])
    def Clear(self):
        self.lstRes.DeleteAllItems()
    def AddLog(self,sLog,sKind='def'):
        try:
            if sLog is None:
                self.lstLog.DeleteAllItems()
                return
            if sKind in self.dImg:
                img=self.dImg[sKind]
            else:
                img=-1
            idx=self.lstLog.InsertImageStringItem(0,'',img)
            zNow=time.time()
            #sTM=time.strftime('%Y-%m-%d %H:%M:%s',time.localtime(zNow))
            sTM=time.strftime('%H:%M:%S',time.localtime(zNow))
            self.lstLog.SetStringItem(idx,1,sTM)
            self.lstLog.SetStringItem(idx,2,sLog)
        except:
            self.__logTB__()
