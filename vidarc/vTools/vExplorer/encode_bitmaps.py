#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: encode_bitmaps.py,v 1.8 2010/07/19 09:48:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

#import sys
#from wxPython.tools import img2py


command_lines = [
    #"-u -i -n Application   img/BrowseFile01_16.ico         imgExp.py",
    #"-a -u -n Plugin        img/BrowseFile01_16.png         imgExp.py",
    "-u -i -n Application   img/vExplorer01_32.ico          imgExp.py",
    "-a -u -n Plugin        img/vExplorer01_32.png          imgExp.py",
    "-a -u -n Properties    img/PropertiesAS_32.ico         imgExp.py",
    
    #"-a -u -n ToDay         img/ToDay01_16.png              images.py",
    #"-a -u -n Store         img/Input01_16.png              images.py",
    #"-a -u -n Shell         img/Shell01_16.png              images.py",
    #"-a -u -n ShellPipes    img/ShellOutput01_16.png        images.py",
    "-a -u -n Enter         img/Enter01_16.png              imgExp.py",
    "-a -u -n EnterAdd      img/EnterAdd01_16.png           imgExp.py",
    
    #"-a -u -n Cancel        img/abort.png                   imgExp.py",
    #"-a -u -n Del           img/waste.png                   imgExp.py",
    #"-a -u -n Edit          img/Edit02_16.png               imgExp.py",
    #"-a -u -n Apply         img/ok.png                      imgExp.py",
    #"-a -u -n Exit          img/Exit01_16.png               imgExp.py",
    
    "-a -u -n DirClose      img/DirClosed01_16.png          imgExp.py",
    "-a -u -n DirOpen       img/DirOpened01_16.png          imgExp.py",
    "-a -u -n Dir           img/vExplorer01_16.png          imgExp.py",
    
    "-a -u -n DirStop       img/DirStop01_16.png            imgExp.py",
    "-a -u -n DirBlock      img/DirBlocked01_16.png         imgExp.py",
    "-a -u -n DirPie        img/DirPie01_16.png             imgExp.py",
    "-a -u -n DirShutDown   img/DirShutDown01_16.png        imgExp.py",
    "-a -u -n DirStart      img/DirStart01_16.png           imgExp.py",
    
    "-a -u -n Src1          img/Src1_02_16.png              imgExp.py",
    "-a -u -n Src2          img/Src2_02_16.png              imgExp.py",
    "-a -u -n Srcs          img/Src12_02_16.png             imgExp.py",
    "-a -u -n OpenSrc1      img/OpenSrc1_01_16.png          imgExp.py",
    "-a -u -n OpenSrc2      img/OpenSrc2_01_16.png          imgExp.py",
    "-a -u -n Cp2To1        img/Cp2To1_04_16.png            imgExp.py",
    "-a -u -n Cp1To2        img/Cp1To2_04_16.png            imgExp.py",
    "-a -u -n Mv2To1        img/Mv2To1_04_16.png            imgExp.py",
    "-a -u -n Mv1To2        img/Mv1To2_04_16.png            imgExp.py",
    "-a -u -n Del1          img/Del1_02_16.png              imgExp.py",
    "-a -u -n Del2          img/Del2_02_16.png              imgExp.py",
    
    "-a -u -n Play          img/StateRunning01_16.png       imgExp.py",
    "-a -u -n Record        img/StateStopped01_16.png       imgExp.py",
    
    "-a -u -n Computer      img/Comp03_16.png               imgExp.py",
    "-a -u -n DiskFixed     img/DiskFixed02_16.png          imgExp.py",
    "-a -u -n DiskNet       img/DiskNetwork02_16.png        imgExp.py",
    "-a -u -n DiskNetFlt    img/DiskNetwork03_16.png        imgExp.py",
    "-a -u -n DriveCdrom    img/DriveCdrom02_16.png         imgExp.py",
    "-a -u -n DiskRemovable img/DriveRemovable01_16.png     imgExp.py",
    
    "-a -u -n TrueCryptOk   img/TrueCrypt01_16.png          imgExp.py",
    "-a -u -n TrueCryptFlt  img/TrueCrypt02_16.png          imgExp.py",
    "-a -u -n VMware        img/VMwareTray01_16.png         imgExp.py",
    
    "-a -u -n Per03         img/Per03_16.png                imgExp.py",
    "-a -u -n Per00000      img/Per00000_16.png             imgExp.py",
    "-a -u -n Per01250      img/Per01250_16.png             imgExp.py",
    
    "-a -u -n PerGreen      img/PerGreen01_16.png           imgExp.py",
    
    "-a -u -n AlmNone       img/AlarmLampNone01_16.png      imgExp.py",
    "-a -u -n AlmHigh       img/AlarmLampHigh01_16.png      imgExp.py",
    "-a -u -n AlmHigh       img/AlarmLampHigh01_16.png      imgExp.py",
    "-a -u -n AlmMed        img/AlarmLampMed01_16.png       imgExp.py",
    "-a -u -n AlmLow        img/AlarmLampLow01_16.png       imgExp.py",
    
    "-a -u -n Aborted       img/StateAborted01_16.ico       imgExp.py",
    "-a -u -n Running       img/StateRunning01_16.ico       imgExp.py",
    "-a -u -n Stopped       img/StateStopped02_16.ico       imgExp.py",
    "-a -u -n Error         img/CancelAS_16.ico             imgExp.py",
    ]


#for line in command_lines:
#    args = line.split()
#    img2py.main(args)

