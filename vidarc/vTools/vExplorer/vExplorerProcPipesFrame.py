#----------------------------------------------------------------------------
# Name:         vExplorerProcPipesFrame
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100517
# CVS-ID:       $Id: vExplorerProcPipesFrame.py,v 1.4 2012/01/12 10:06:43 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time,os,types,sys
    import wx
    
    #from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgFrame import vtgFrame
    from vidarc.tool.gui.vtgPanel import vtgPanel
    from vidarc.tool.misc.vtmListCtrl import vtmListCtrl
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.vTools.vExplorer.imgExp as img
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

ICON_ABORTED=None
ICON_RUNNING=None
ICON_STOPPED=None
ICON_ERROR=None

def getIconByName(s):
    global ICON_ABORTED
    global ICON_RUNNING
    global ICON_STOPPED
    global ICON_ERROR
    if ICON_ABORTED is None:
        ICON_ABORTED=vtArt.getIcon(img.getAbortedBitmap())
        ICON_RUNNING=vtArt.getIcon(img.getRunningBitmap())
        ICON_STOPPED=vtArt.getIcon(img.getStoppedBitmap())
        ICON_ERROR=vtArt.getIcon(img.getErrorBitmap())
        
    if s=='ICON_ABORTED':
        return ICON_ABORTED
    if s=='ICON_RUNNING':
        return ICON_RUNNING
    if s=='ICON_STOPPED':
        return ICON_STOPPED
    if s=='ICON_ERROR':
        return ICON_ERROR
    return ICON_ERROR

class vExplorerProcPipesFrame(vtgFrame):
    def __init__(self, parent,dPos=None,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None,zClose=None,zClsSleep=0.5):
        global _
        _=vtLgBase.assignPluginLang('vExplorer')
        lWid=[
            ('szBoxHor',(0,0),(1,2),
                    {'name':'bxsIn',},[
                    ('txt',     1,4,{'name':'txtIn','value':u''},
                        {'ent':self.OnTxtInTextEnter}),
                    ('cbBmp',   0,4,{'name':'cbKeySpecial','bitmap':vtArt.getBitmap(vtArt.LifeBelt),},None),
                    ('cbBmp',   0,4,{'name':'cbEnter','bitmap':img.getEnterBitmap(),},
                        {'btn':self.OnCbEnter}),
                    ('cbBmp',   0,4,{'name':'cbEnterAdd','bitmap':img.getEnterAddBitmap(),},None),
                    ('cbBmp',   0,0,{'name':'cbPopup','bitmap':vtArt.getBitmap(vtArt.Down),},None),
                    ]),
            ('szBoxHor',(1,0),(1,2),
                    {'name':'bxsOt',},[
                    #('lstExt',  1,4,
                    #        {'name':'lstPipes',},None),
                    (vtmListCtrl,1,4,{
                            'name':'lstPipes',
                            'cols':[
                                [u'',None,20,None],
                                [_(u'message'),None,-1,None],
                                #[_(u'time'),None,100,None],
                                ],
                            },None),
                    ('szBoxVert',0,0,
                            {'name':'bxsBt',},[
                            ('cbBmp',   0,4,{'name':'cbClr','bitmap':vtArt.getBitmap(vtArt.Erase),'size':(32,32),},
                                {'btn':self.OnCbClr}),
                            ('cbBmp',   0,4,{'name':'cbSave','bitmap':vtArt.getBitmap(vtArt.Save),'size':(32,32),},
                                {'btn':self.OnCbSave}),
                            ]),
                    ]),
            ('szBoxHor',(2,0),(1,1),
                    {'name':'bxsCfg',},[
                    ('chk',     0,4,{'name':'chcWarp','label':_(u'warp'),},None),
                    ('chk',     0,4,{'name':'chcScroll','label':_(u'scroll'),},None),
                    ('spInt',   0,4,{'name':'spSz','initial':100,'size':(80,-1)},None),
                    ]),
            ]
        vtgFrame.__init__(self,parent,_(u'VIDARC Explorer process pipes'),
                vtgPanel,name=name or 'dlgExpPipes',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                bmp=None or img.getPluginBitmap(),
                pnPos=None,pnSz=(300,400),pnStyle=None,
                pnName='pnExpPipes',
                lGrowCol=[(0,1)],lGrowRow=[(1,1)],
                bEnMark=False,bEnMod=False,
                lWid=lWid)
        self.pn.applName=_(u'VIDARC Explorer process pipes')
        self.imgLst,self.dImg=self.lstPipes.CreateImageList([
                ('empty'    ,vtArt.getBitmap(vtArt.Invisible)),
                ('asc'      ,vtArt.getBitmap(vtArt.UpSmall)),
                ('desc'     ,vtArt.getBitmap(vtArt.DownSmall)),
                ('stdin'    ,vtArt.getBitmap(vtArt.ExportFree)),
                ('stdout'   ,vtArt.getBitmap(vtArt.ImportFree)),
                ('stderr'   ,vtArt.getBitmap(vtArt.Error)),
                ])
        self.lstPipes.SetImageList(self.imgLst,self.dImg['empty'],self.dImg['asc'],
                self.dImg['desc'])
        self.spSz.SetValue(100)
        self._zClose=zClose
        self._zClsSleep=zClsSleep
        #self.lstPipes.SetColumnWidth(2,30)
        #self.lstPipes.SetStretchLst([(1,-1.0)])
        
        self.popWin=None
        self.popComplete=None
        self.bBusy=False
        self.cmd=None
        self.proc=None
        self._pid=None
        self.lIdx=[sys.maxint,sys.maxint,sys.maxint]
        self.iOfsLast=-1
    def GetIdent(self):
        try:
            if self.proc is None:
                return _(u'???')
            else:
                return self.proc.GetIdent()
        except:
            self.__logTB__()
    def GetPID(self):
        try:
            return self._pid
        except:
            self.__logTB__()
    def SetCmd(self,cmd):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def SetProc(self,proc):
        try:
            self.__logDebug__(''%())
            self.proc=proc
            self.pn.applName=self.proc.GetIdent()
            self._pid=proc.GetPID()
        except:
            self.__logTB__()
    def __autoClose__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(['_zClose',self._zClose,'_zClsSleep',self._zClsSleep])
            if self._zClose is None:
                return
            if self._zClose<=0.0:
                self.Close()
                return
            self._zClose-=self._zClsSleep
            self.CallTopWid('CallBackDelayed',self._zClsSleep,self.__autoClose__)
        except:
            self.__logTB__()
    def SetState(self,state):
        try:
            self.__logDebug__('state:%s'%(state))
            if state=='running':
                o=vtArtState.Running
                icon=getIconByName('ICON_RUNNING')
                sMsg=_(u'process %s is running')%(self.GetIdent())
            elif state=='stopped':
                o=vtArtState.Stopped
                icon=getIconByName('ICON_STOPPED')
                sMsg=_(u'process %s has stopped')%(self.GetIdent())
                self.__autoClose__()
            elif state=='aborted':
                o=vtArtState.Aborted
                icon=getIconByName('ICON_ABORTED')
                sMsg=_(u'process %s has been aborted')%(self.GetIdent())
                self.__autoClose__()
            #self.lbbState.SetBitmap(vtArtState.getBitmap(o))
            #self.lbbState.Refresh()
            
            self.SetIcon(icon)
            #if self.proc is not None:
            #    par=self.GetParent()
            #    par.SetState(state,self.proc.tiSel)
            self.Post('state',{
                    'ident':self.GetIdent(),
                    'pid':self.GetPID(),
                    'msg':sMsg,
                    'state':state})
        except:
            self.__logTB__()
    def __add2Lst__(self,iOfs,s):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__([iOfs,s])
                    bDbg=True
            if len(s)==0:
                return
            if iOfs==0:
                img=self.dImg['stdin']
            elif iOfs==1:
                img=self.dImg['stdout']
            elif iOfs==2:
                img=self.dImg['stderr']
            iIdx=self.lIdx[iOfs]
            iMax=sys.maxint
            if bDbg==True:
                self.__logDebug__(['lIdx',self.lIdx,'iIdx',iIdx,
                        'iMax',iMax,'iOfs',iOfs,'iOfsLast',self.iOfsLast])
            if self.iOfsLast!=iOfs:
                iIdx=iMax
            self.iOfsLast=iOfs
            l=s.split('\r')
            sCur=l[0]
            if iIdx==iMax:
                iIdx=self.lstPipes.InsertImageItem(iMax,img)
            else:
                sCur=self.lstPipes.GetItem(iIdx,1).m_text+sCur
            if bDbg==True:
                self.__logDebug__(['lIdx',self.lIdx,'iIdx',iIdx,
                        'iMax',iMax,'img',img])
            bLF=False
            for s in l[1:]:
                iLC=len(sCur)
                iLA=len(s)
                if iLA==1:
                    if s=='\n':
                        bLF=True
                if bLF==False:
                    if iLA>=iLC:
                        sCur=s
                    else:
                        sCur=s+sCur[iLA:]
            if self.chcScroll.GetValue():
                self.lstPipes.EnsureVisible(iIdx)
            s=[]
            for c in sCur:
                iC=ord(c)
                if iC<27 and iC!=10:
                    s.append('^%c'%(64+ord(c)))
                else:
                    s.append(c)
            s=''.join(s)
            if len(s)!=len(sCur):
                sCur=s
            
            if len(sCur)>0 and sCur[-1]=='\n':
                self.lstPipes.SetStringItem(iIdx,1,sCur[:-1],-1)
                iIdx=iMax
            else:
                self.lstPipes.SetStringItem(iIdx,1,sCur,-1)
                if bLF:
                    iIdx=iMax
            self.lIdx[iOfs]=iIdx
            if bDbg==True:
                self.__logDebug__(['lIdx',self.lIdx,'iIdx',iIdx,
                        'bLF',bLF])
        except:
            self.__logTB__()
    def WriteStdIn(self,s,bLineSep=False,bEnter=True):
        try:
            self.__logDebug__(''%())
            self.proc.WriteStdIn(s,bLineSep=bLineSep,bEnter=bEnter)
            self.__add2Lst__(0,s)
        except:
            self.__logTB__()
    def AddStdOut(self,s):
        try:
            if VERBOSE>0:
                self.__logDebug__(s)
            self.__add2Lst__(1,s)
        except:
            self.__logTB__()
    def AddStdErr(self,s):
        try:
            if VERBOSE>0:
                self.__logDebug__(s)
            self.__add2Lst__(2,s)
        except:
            self.__logTB__()
    def __setPopupState__(self,flag):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def __createPopup__(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCbPopup(self, event):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCbEnter(self, event):
        try:
            self.__logDebug__(''%())
            s=self.txtIn.GetValue()
            self.WriteStdIn(s)
            self.txtIn.SetValue(u'')
        except:
            self.__logTB__()
    def OnCbClr(self, event):
        try:
            self.__logDebug__(''%())
            self.lstPipes.DeleteAllItems()
            if self.proc is None:
                self.SetIcon(getIconByName('ICON_STOPPED'))
            else:
                if self.proc.IsRunning():
                    self.SetIcon(getIconByName('ICON_RUNNING'))
        except:
            self.__logTB__()
    def OnCbSave(self, evt):
        if evt is not None:
            evt.Skip()
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnTxtInTextEnter(self, evt):
        if evt is not None:
            evt.Skip()
            s=self.txtIn.GetValue()
            self.WriteStdIn(s)
            self.txtIn.SetValue(u'')
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnCbCloseButton(self, evt):
        try:
            evt.Skip()
            if self.proc is not None:
                if self.proc.IsRunning():
                    self.Iconize(True)
                    return
            self.Close()
            if self.proc is not None:
                self.proc.ClrFrm()
        except:
            self.__logTB__()
    def OnFrameClose(self,evt):
        try:
            if self.proc is not None:
                if self.proc.IsRunning():
                    self.ShowMsgDlg(self.OK,_(u'Process still active!'),None)
                    evt.Veto()
                    return
                self.proc.ClrFrm()
            #self.Close()
            evt.Skip()
        except:
            self.__logTB__()
