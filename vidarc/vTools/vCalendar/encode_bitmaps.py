#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060209
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2007/11/06 16:13:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/Calendar01_16.ico       images.py",
    "-a -u -n Plugin        img/Calendar01_16.png           images.py",
    "-a -u -n Calendar      img/Calendar01_16.png           images.py",
    "-a -u -n ToDay         img/ToDay01_16.png              images.py",
    "-a -u -n Store         img/Input01_16.png              images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

