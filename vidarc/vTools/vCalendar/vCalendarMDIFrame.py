#Boa:MDIChild:vCalendarMDIFrame
#----------------------------------------------------------------------------
# Name:         vCalendarMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071106
# CVS-ID:       $Id: vCalendarMDIFrame.py,v 1.2 2007/11/17 10:31:04 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.tool.log.vtLog as vtLog

try:
    import vidarc.vTools.vCalendar.__init__ as plgInit
    from vidarc.vTools.vCalendar.vCalendarPanel import *
    import vidarc.vTools.vCalendar.images as images
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

DOMAINS=plgInit.DOMAINS
def getDomainTransLst(lang):
    vtLog.vtLngCur(vtLog.WARN,'use new plugin resolve style','import')
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    vtLog.vtLngCur(vtLog.WARN,'use new plugin resolve style','import')
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vContactMDIFrame(parent)

[wxID_VCALENDARMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vCalendarMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=-1,
              name=u'vCalendarMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 350), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vCalendar')
        self.SetClientSize(wx.Size(492, 347))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Calendar',bToolBar=True)
        self.dCfg.update({'x':10,'y':10,'width':500,'height':350})
        
        try:
            self.pn=vCalendarPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vContactPlugIn')
            #self.pn.OpenCfgFile('vCalendarCfg.xml')
        except:
            vtLog.vtLngTB(self.GetName())
    def __getPluginImage__(self):
        return images.getPluginImage()
    def __getPluginBitmap__(self):
        return images.getPluginBitmap()
    def __makeTitle__(self):
        pass
    def OpenFile(self,fn):
        pass
    def OpenCfgFile(self,fn=None):
        pass
    def OnMasterShutDown(self,evt):
        pass
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        if self.IsShown():
            self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
    def OnMainClose(self,event):
        #vMDIFrame.OnMainClose(self,event)
        event.Skip()
        self.GetCfgData()
