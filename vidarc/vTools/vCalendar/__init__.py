#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      200060209
# CVS-ID:       $Id: __init__.py,v 1.4 2007/11/19 18:01:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import images as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vCalendarMDIFrame'
PLUGABLE_FRAME=True
DOMAINS=['tools','date_time','vCalendar']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
