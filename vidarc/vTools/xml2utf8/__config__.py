#----------------------------------------------------------------------------
# Name:         __config__.py
# Purpose:      package configuration.
#
# Author:       Walter Obweger
#
# Created:      20110411
# CVS-ID:       $Id: __config__.py,v 1.1 2011/04/11 16:46:33 wal Exp $
# Copyright:    (c) 2011 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------

VER_MAJOR        = 0      # The first three must match wxWidgets
VER_MINOR        = 0
VER_RELEASE      = 1
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.
VERSION          = u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE)

DESCRIPTION      = "VIDARC Tool xml2utf8"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
MAINTAINER       = "Walter Obweger"
MAINTAINER_EMAIL = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "VIDARC license"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "library,tool,log"

LONG_DESCRIPTION = """\
vidarc tool xml-file encoding.
"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000/XP
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""
INST_PATH="vidarc/tools"
GROUP_BASE="VIDARC\Tools"

APP='xml2utf8'
ICONS=[]
APP_PY='xml2utf8.py'
APP_INCL=[]
APP_EXCL=[]
LANG=[]

def getAppl():
    return [
        [0,'xml2utf8',
            u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE),
            'xml2utf8.py',
            ICONS,
            None
        ],
        ]
