#----------------------------------------------------------------------------
# Name:         xml2utf8.py
# Purpose:      convert xml-files to utf8 encoding using libxml2
#
# Author:       Walter Obweger
#
# Created:      20110411
# CVS-ID:       $Id: xml2utf8.py,v 1.1 2011/04/11 16:46:33 wal Exp $
# Copyright:    (c) 2011 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,os,fnmatch
from optparse import OptionParser
import platform
import libxml2
import traceback

def xml2utf8(sSrcFN,sSrcDN,sDstDN,iSkip,bVerbose):
    try:
        if bVerbose:
            print '...',
        doc=libxml2.parseFile(sSrcFN)
        sDstFN=os.path.join(sDstDN,sSrcFN[iSkip+1:])
        sDN,sFN=os.path.split(sDstFN)
        if os.path.exists(sDN)==False:
            try:
                os.makedirs(sDN)
            except:
                pass
        if os.path.exists(sDstFN)==False:
            doc.saveFileEnc(sDstFN,'utf8')
            if bVerbose:
                print 'done',
        else:
            if bVerbose:
                print 'skip',
            
    except:
        if bVerbose:
            print 'failed',
            traceback.print_exc()
    if bVerbose:
        print

if __name__=='__main__':
    usage = "usage: %prog [options]"
    oParser=OptionParser(usage,version="%prog 0.1")
    oParser.add_option('-s','--source',dest='sSrcDN',
            help='source directory',metavar='sSrcDN',default='.')
    oParser.add_option('-d','--destination',dest='sDstDN',
            help='destination directory',metavar='sDstDN',default='.')
    oParser.add_option("-v", action="store_true", dest="verbose", default=True)
    oParser.add_option("-q", action="store_false", dest="verbose", default=True)
    (opt,args)=oParser.parse_args()
    lFN=[]
    for sRoot,lDirs,lFiles in os.walk(opt.sSrcDN):
        for sFN in lFiles:
            
            if fnmatch.fnmatch(sFN,'*.xml'):
                lFN.append(os.path.join(sRoot,sFN))
    iSkip=len(opt.sSrcDN)
    iLen=len(lFN)
    iOfs=1
    bVerbose=opt.verbose
    if bVerbose:
        print '+'*78
        print 'convert xml-file to utf-8'
        print '          source: %s'%opt.sSrcDN
        print '    destionation: %s'%opt.sDstDN
        print '-'*78
    for sFN in lFN:
        if bVerbose:
            print '%3d-%3d ...%s'%(iOfs,iLen,sFN[iSkip:]),
        xml2utf8(sFN,opt.sSrcDN,opt.sDstDN,iSkip,bVerbose)
        iOfs+=1
    if bVerbose:
        print '-'*78

