#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20110411
# CVS-ID:       $Id: __init__.py,v 1.1 2011/04/11 16:46:33 wal Exp $
# Copyright:    (c) 2011 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__

VERSION=__config__.VERSION
try:
    getPluginData=imgPlg.getPluginData
except:
    getPluginData=None

PLUGABLE=None
PLUGABLE_FRAME=False
DOMAINS=[]

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

SSL=0
import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
