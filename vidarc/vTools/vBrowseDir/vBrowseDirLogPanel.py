#Boa:FramePanel:vBrowseDirLogPanel
#----------------------------------------------------------------------------
# Name:         vBrowseDirLogPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090109
# CVS-ID:       $Id: vBrowseDirLogPanel.py,v 1.3 2009/02/22 17:53:55 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.misc.vtmListCtrl

import sys,time

[wxID_VBROWSEDIRLOGPANEL, wxID_VBROWSEDIRLOGPANELLSTLOG, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vBrowseDirLogPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_fgsRes_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsRes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstLog, 0, border=0, flag=wx.EXPAND)

    def _init_coll_lstLog_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=_(u'time'),
              width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=_(u'log'),
              width=400)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsRes = wx.FlexGridSizer(cols=1, hgap=4, rows=1, vgap=4)

        self._init_coll_fgsRes_Items(self.fgsRes)
        self._init_coll_fgsRes_Growables(self.fgsRes)

        self.SetSizer(self.fgsRes)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VBROWSEDIRLOGPANEL,
              name=u'vBrowseDirLogPanel', parent=prnt, pos=wx.Point(176, 176),
              size=wx.Size(339, 217), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(331, 190))

        self.lstLog = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VBROWSEDIRLOGPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 0), size=wx.Size(331,
              190), style=wx.LC_REPORT)
        self._init_coll_lstLog_Columns(self.lstLog)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vToolsBrowseDir')
        self._init_ctrls(parent)
        
        self.imgLst=wx.ImageList(16,16)
        self.imgDict={}
        img=vtArt.getBitmap(vtArt.Apply)
        self.imgDict['def']=self.imgLst.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDict['warn']=self.imgLst.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDict['error']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Question)
        self.imgDict['quest']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDict['empty']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Right)
        self.imgDict['in']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Left)
        self.imgDict['out']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Build)
        self.imgDict['proc']=self.imgLst.Add(img)
        self.lstLog.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.lstLog.SetStretchLst([(2,-1)])
        
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        self.lstRes.DeleteAllItems()
    def AddLog(self,sLog,sKind='def'):
        try:
            if sLog is None:
                self.lstLog.DeleteAllItems()
                return
            if sKind in self.imgDict:
                img=self.imgDict[sKind]
            else:
                img=-1
            idx=self.lstLog.InsertImageStringItem(0,'',img)
            zNow=time.time()
            #sTM=time.strftime('%Y-%m-%d %H:%M:%s',time.localtime(zNow))
            sTM=time.strftime('%H:%M:%S',time.localtime(zNow))
            self.lstLog.SetStringItem(idx,1,sTM)
            self.lstLog.SetStringItem(idx,2,sLog)
        except:
            vtLog.vtLngTB(self.GetName())
