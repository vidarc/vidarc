#Boa:Frame:vBrowseDirMainFrame
#----------------------------------------------------------------------------
# Name:         vBrowseDirMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090108
# CVS-ID:       $Id: vBrowseDirMainFrame.py,v 1.3 2009/02/01 10:00:30 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    
    import vidarc.vTools.vBrowseDir.images as imgBrowseDir
    from vidarc.vTools.vBrowseDir.vBrowseDirPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgBrowseDir.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgBrowseDir.getApplicationBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vContactMain'):
    return vBrowseDirMainFrame(parent,id,pos,size,style,name)

[wxID_VBROWSEDIRMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vBrowseDirMainFrame(wx.Frame,vMDIFrame,vCfg):
    STATUS_CLK_POS=4
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VBROWSEDIRMAINFRAME,
              name=u'vBrowseDirMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 320), style=wx.DEFAULT_FRAME_STYLE|wx.THICK_FRAME,
              title=u'vBrowseDir')
        self.SetClientSize(wx.Size(492, 293))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'BrowseDir',iNotifyTime=250)
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':500,'height':350})
        
        try:
            self.pn=vBrowseDirPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vBrowseDirPanel')
            self.pn.OpenCfgFile('vBrowseDirCfg.xml')
        except:
            vtLog.vtLngTB(self.GetName())
        self.dCfg.update(self._getCfgData(['vBrowseDirGui']))
        self.pn.SetCfgData(self._setCfgData,['vBrowseDirGui'],self.dCfg)
        self.__setCfg__()
    def __makeTitle__(self):
        pass
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        return
        self.pn.OpenFile(fn)
    def OpenCfgFile(self,fn=None):
        return
        self.pn.OpenCfgFile(fn)
    def __setCfg__(self):
        try:
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def OnMasterShutDown(self,evt):
        pass
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        if self.IsShown():
            self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
    def OnMainClose(self,event):
        self.GetCfgData()
        if self.pn.xdCfg is not None:
            self._setCfgData(['vBrowseDirGui'],self.dCfg)
        #vMDIFrame.OnMainClose(self,event)
        event.Skip()
