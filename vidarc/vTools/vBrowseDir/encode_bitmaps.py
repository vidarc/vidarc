#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/BrowseFile01_16.ico         images.py",
    "-a -u -n Plugin        img/BrowseFile01_16.png         images.py",
    "-a -u -n DirClose      img/DirClosed01_16.png          images.py",
    "-a -u -n DirOpen       img/DirOpened01_16.png          images.py",
    
    "-a -u -n Src1          img/Src1_02_16.png              images.py",
    "-a -u -n Src2          img/Src2_02_16.png              images.py",
    "-a -u -n Srcs          img/Src12_02_16.png             images.py",
    "-a -u -n OpenSrc1      img/OpenSrc1_01_16.png          images.py",
    "-a -u -n OpenSrc2      img/OpenSrc2_01_16.png          images.py",
    "-a -u -n Cp2To1        img/Cp2To1_04_16.png            images.py",
    "-a -u -n Cp1To2        img/Cp1To2_04_16.png            images.py",
    "-a -u -n Mv2To1        img/Mv2To1_04_16.png            images.py",
    "-a -u -n Mv1To2        img/Mv1To2_04_16.png            images.py",
    "-a -u -n Del1          img/Del1_02_16.png              images.py",
    "-a -u -n Del2          img/Del2_02_16.png              images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

