#Boa:FramePanel:vBrowseDirPanel
#----------------------------------------------------------------------------
# Name:         vBrowseDirPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090108
# CVS-ID:       $Id: vBrowseDirPanel.py,v 1.7 2009/05/11 16:00:53 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmThreadCtrl
import wx.gizmos
import wx.lib.buttons
import wx.lib.filebrowsebutton
import time,os,os.path,stat,threading
import Queue,sys

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

import images as vBrowseDirImages

from vidarc.tool.InOut.fnUtil import *
import vidarc.tool.InOut.listFiles as listFiles
from vidarc.tool.InOut.genFN import getCurDateTimeFN
from vidarc.tool.InOut.listFilesXml import *
from vidarc.tool.InOut.vtInOutComp import vtInOutCompDirEntry
from vidarc.tool.InOut.vtInOutReadThread import *
from test.tool.InOut.vtInOutFileTimes import setFileTimes
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
import vidarc.tool.misc.vtmTreeListCtrl
from vidarc.tool.vtThread import vtThreadWX
from vBrowseDirResultDialog import vBrowseDirResultDialog

from vBrowseDirLogPanel import vBrowseDirLogPanel

VERBOSE=0

def getAboutDescription():
    return _(u"""vBrowseDir module.

This module gather file information from configurable directories,
and compares them. 
Files can be copied, moved or deleted from both directories.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")

def create(parent):
    return vBrowseDirPanel(parent)

[wxID_VBROWSEDIRPANEL, wxID_VBROWSEDIRPANELCBCLRALL, 
 wxID_VBROWSEDIRPANELCBCLRCMP, wxID_VBROWSEDIRPANELCBCLRSRC1, 
 wxID_VBROWSEDIRPANELCBCLRSRC2, wxID_VBROWSEDIRPANELCBCPTO1, 
 wxID_VBROWSEDIRPANELCBCPTO2, wxID_VBROWSEDIRPANELCBDEL1, 
 wxID_VBROWSEDIRPANELCBDEL2, wxID_VBROWSEDIRPANELCBMVTO1, 
 wxID_VBROWSEDIRPANELCBMVTO2, wxID_VBROWSEDIRPANELCBOPENSRC1, 
 wxID_VBROWSEDIRPANELCBOPENSRC2, wxID_VBROWSEDIRPANELCBPOPUP, 
 wxID_VBROWSEDIRPANELCBREADSRC1, wxID_VBROWSEDIRPANELCBREADSRC2, 
 wxID_VBROWSEDIRPANELCBREMOVE, wxID_VBROWSEDIRPANELCHKACCESS, 
 wxID_VBROWSEDIRPANELCHKAUTOEXPAND, wxID_VBROWSEDIRPANELCHKCALCALWAYSMD5, 
 wxID_VBROWSEDIRPANELCHKCALCMD5, wxID_VBROWSEDIRPANELCHKCREATE, 
 wxID_VBROWSEDIRPANELCHKFLAT, wxID_VBROWSEDIRPANELCHKSELPROC,
 wxID_VBROWSEDIRPANELCHKMOD, 
 wxID_VBROWSEDIRPANELCHKSHOWEQUAL, wxID_VBROWSEDIRPANELDBBSRC1, 
 wxID_VBROWSEDIRPANELDBBSRC2, wxID_VBROWSEDIRPANELNBDATA, 
 wxID_VBROWSEDIRPANELPNCFG, wxID_VBROWSEDIRPANELPNDATA, 
 wxID_VBROWSEDIRPANELSBCALC, wxID_VBROWSEDIRPANELSBCHECK, 
 wxID_VBROWSEDIRPANELSBSHOW, wxID_VBROWSEDIRPANELSPREC, 
 wxID_VBROWSEDIRPANELTGTARGETSRC1, wxID_VBROWSEDIRPANELTGTARGETSRC2, 
 wxID_VBROWSEDIRPANELTHDCTRL, wxID_VBROWSEDIRPANELTRLSTRES, 
] = [wx.NewId() for _init_ctrls in range(39)]

def getPluginImage():
    return vCheckDirImages.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(vBrowseDirImages.getPluginBitmap())
    return icon

class vBrowseDirPanel(wx.Panel):
    LST_CFGOBJ_MAP=[('chkCalcMD5','calc_md5'),
            ('chkCalcAlwaysMD5','calc_always_md5'),
            ('chkAccess','check_time_access'),
            ('chkMod','check_time_mod'),
            ('chkCreate','check_time_create'),
            ('chkAutoExpand','show_auto_expand'),
            ('chkFlat','show_flat'),
            ('chkSelProc','sel_processing'),
            ('chkShowEqual','show_equal'),
            ('tgTargetSrc1','target_src1'),
            ('tgTargetSrc2','target_src2'),
            ]
    ACT_UNDEF=0
    ACT_CP1TO2=10
    ACT_MV1TO2=11
    ACT_DEL1=12
    ACT_CP2TO1=20
    ACT_MV2TO1=21
    ACT_DEL2=22
    ACT_TARGET1=100
    ACT_TARGET2=200
    def _init_coll_fgsBrowse_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsBrowse_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trlstRes, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.fgsBrowseBt, 0, border=4, flag=wx.LEFT)

    def _init_coll_sbsShow_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chkAutoExpand, 0, border=0, flag=wx.GROW)
        parent.AddWindow(self.chkShowEqual, 0, border=0, flag=wx.GROW)
        parent.AddWindow(self.chkFlat, 0, border=0, flag=wx.GROW)
        parent.AddWindow(self.chkSelProc, 0, border=0, flag=wx.GROW)

    def _init_coll_fgsCfg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsBtMisc1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbClrCmp, 0, border=0, flag=0)

    def _init_coll_fgsBrowseBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgTargetSrc1, 0, border=0, flag=0)
        parent.AddWindow(self.tgTargetSrc2, 0, border=0, flag=0)
        parent.AddWindow(self.cbCpTo1, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbCpTo2, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbMvTo1, 0, border=0, flag=0)
        parent.AddWindow(self.cbMvTo2, 0, border=0, flag=0)
        parent.AddWindow(self.cbDel1, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbDel2, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbOpenSrc1, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbOpenSrc2, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbPopup, 0, border=0, flag=0)
        parent.AddWindow(self.cbRemove, 0, border=0, flag=0)
        parent.AddWindow(self.cbClrAll, 0, border=16, flag=wx.TOP)

    def _init_coll_sbsCalc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chkCalcMD5, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.spRec, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chkCalcAlwaysMD5, 0, border=0, flag=wx.EXPAND)

    def _init_coll_sbsCheck_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chkAccess, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chkMod, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chkCreate, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbData, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.thdCtrl, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsCfgData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.sbsCalc, 0, border=8, flag=wx.EXPAND | wx.RIGHT)
        parent.AddSizer(self.sbsCheck, 0, border=8, flag=wx.EXPAND | wx.RIGHT)
        parent.AddSizer(self.sbsShow, 1, border=8, flag=wx.GROW | wx.RIGHT)

    def _init_coll_fgsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbSrc1, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbReadSrc1, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbClrSrc1, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.dbbSrc2, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbReadSrc2, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbClrSrc2, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.fgsCfgData, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBtMisc1, 0, border=0, flag=0)

    def _init_coll_fgsCfgData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(2)

    def _init_coll_trlstRes_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(_(u'name'))
        parent.AddColumn(_(u'size'))
        parent.AddColumn(u'')
        parent.AddColumn(u'')
        parent.AddColumn(_(u'S'))
        parent.AddColumn(_(u'M'))
        parent.AddColumn(_(u'tA'))
        parent.AddColumn(_(u'tM'))
        parent.AddColumn(_(u'tC'))

    def _init_coll_nbData_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnCfg, select=True,
              text=_(u'config'))
        parent.AddPage(imageId=-1, page=self.pnData, select=False,
              text=_(u'data'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsCfg = wx.FlexGridSizer(cols=3, hgap=4, rows=4, vgap=4)

        self.fgsBrowse = wx.FlexGridSizer(cols=2, hgap=4, rows=3, vgap=4)

        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=4, rows=2, vgap=4)

        self.sbsCalc = wx.StaticBoxSizer(box=self.sbCalc, orient=wx.VERTICAL)

        self.sbsCheck = wx.StaticBoxSizer(box=self.sbCheck, orient=wx.VERTICAL)

        self.sbsShow = wx.StaticBoxSizer(box=self.sbShow, orient=wx.VERTICAL)

        self.fgsCfgData = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.fgsBrowseBt = wx.FlexGridSizer(cols=2, hgap=4, rows=8, vgap=4)

        self.bxsBtMisc1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsCfg_Items(self.fgsCfg)
        self._init_coll_fgsCfg_Growables(self.fgsCfg)
        self._init_coll_fgsBrowse_Items(self.fgsBrowse)
        self._init_coll_fgsBrowse_Growables(self.fgsBrowse)
        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_sbsCalc_Items(self.sbsCalc)
        self._init_coll_sbsCheck_Items(self.sbsCheck)
        self._init_coll_sbsShow_Items(self.sbsShow)
        self._init_coll_fgsCfgData_Items(self.fgsCfgData)
        self._init_coll_fgsCfgData_Growables(self.fgsCfgData)
        self._init_coll_fgsBrowseBt_Items(self.fgsBrowseBt)
        self._init_coll_bxsBtMisc1_Items(self.bxsBtMisc1)

        self.SetSizer(self.fgsMain)
        self.pnData.SetSizer(self.fgsBrowse)
        self.pnCfg.SetSizer(self.fgsCfg)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VBROWSEDIRPANEL,
              name=u'vBrowseDirPanel', parent=prnt, pos=wx.Point(1, 2),
              size=wx.Size(526, 384), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(518, 357))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.nbData = wx.Notebook(id=wxID_VBROWSEDIRPANELNBDATA, name=u'nbData',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(518, 357),
              style=wx.SUNKEN_BORDER)

        self.pnCfg = wx.Panel(id=wxID_VBROWSEDIRPANELPNCFG, name=u'pnCfg',
              parent=self.nbData, pos=wx.Point(0, 0), size=wx.Size(510, 331),
              style=wx.TAB_TRAVERSAL)
        self.pnCfg.SetAutoLayout(True)

        self.dbbSrc1 = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VBROWSEDIRPANELDBBSRC1, labelText=_(u'source 1:'),
              parent=self.pnCfg, pos=wx.Point(0, 0), size=wx.Size(350, 32),
              startDirectory='.', newDirectory=True, style=0)

        self.cbReadSrc1 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Measure),
              id=wxID_VBROWSEDIRPANELCBREADSRC1, label=u'Read',
              name=u'cbReadSrc1', parent=self.pnCfg, pos=wx.Point(354, 0),
              size=wx.Size(76, 32), style=0)
        self.cbReadSrc1.SetMinSize(wx.Size(-1, -1))
        self.cbReadSrc1.SetToolTipString(_(u'read source directory 1'))
        self.cbReadSrc1.Bind(wx.EVT_BUTTON, self.OnCbReadSrc1Button,
              id=wxID_VBROWSEDIRPANELCBREADSRC1)

        self.cbClrSrc1 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VBROWSEDIRPANELCBCLRSRC1, label=_(u'Clear'),
              name=u'cbClrSrc1', parent=self.pnCfg, pos=wx.Point(434, 0),
              size=wx.Size(76, 32), style=0)
        self.cbClrSrc1.Bind(wx.EVT_BUTTON, self.OnCbClrSrc1Button,
              id=wxID_VBROWSEDIRPANELCBCLRSRC1)

        self.dbbSrc2 = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose a directory', id=wxID_VBROWSEDIRPANELDBBSRC2,
              labelText=_(u'source 2:'), parent=self.pnCfg, pos=wx.Point(0, 36),
              size=wx.Size(350, 36), startDirectory='.', newDirectory=True, style=0)

        self.cbReadSrc2 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Measure),
              id=wxID_VBROWSEDIRPANELCBREADSRC2, label=_(u'Read'),
              name=u'cbReadSrc2', parent=self.pnCfg, pos=wx.Point(354, 36),
              size=wx.Size(76, 36), style=0)
        self.cbReadSrc2.SetMinSize((-1 , -1))
        self.cbReadSrc2.SetToolTipString(_(u'read source directory 2'))
        self.cbReadSrc2.Bind(wx.EVT_BUTTON, self.OnCbReadSrc2Button,
              id=wxID_VBROWSEDIRPANELCBREADSRC2)

        self.cbClrSrc2 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VBROWSEDIRPANELCBCLRSRC2, label=_(u'Clear'),
              name=u'cbClrSrc2', parent=self.pnCfg, pos=wx.Point(434, 36),
              size=wx.Size(76, 36), style=0)
        self.cbClrSrc2.Bind(wx.EVT_BUTTON, self.OnCbClrSrc2Button,
              id=wxID_VBROWSEDIRPANELCBCLRSRC2)

        self.sbCalc = wx.StaticBox(id=wxID_VBROWSEDIRPANELSBCALC,
              label=_(u'Calculation'), name=u'sbCalc', parent=self.pnCfg,
              pos=wx.Point(0, 76), size=wx.Size(98, 112), style=0)

        self.sbCheck = wx.StaticBox(id=wxID_VBROWSEDIRPANELSBCHECK,
              label=_(u'Check'), name=u'sbCheck', parent=self.pnCfg,
              pos=wx.Point(106, 76), size=wx.Size(123, 112), style=0)

        self.sbShow = wx.StaticBox(id=wxID_VBROWSEDIRPANELSBSHOW,
              label=_(u'Show'), name=u'sbShow', parent=self.pnCfg,
              pos=wx.Point(237, 76), size=wx.Size(105, 112), style=0)

        self.chkCalcMD5 = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKCALCMD5,
              label=u'MD5', name=u'chkCalcMD5', parent=self.pnCfg,
              pos=wx.Point(5, 93), size=wx.Size(88, 30), style=0)
        self.chkCalcMD5.SetValue(True)

        self.spRec = wx.SpinCtrl(id=wxID_VBROWSEDIRPANELSPREC, initial=0,
              max=100, min=-1, name=u'spRec', parent=self.pnCfg, pos=wx.Point(5,
              123), size=wx.Size(88, 21), style=wx.SP_ARROW_KEYS)
        self.spRec.SetToolTipString(_(u'recursion levels'))
        self.spRec.SetMinSize((60 , -1))
        self.spRec.SetMaxSize((60 , -1))

        self.chkCalcAlwaysMD5 = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKCALCALWAYSMD5,
              label=_(u'always'), name=u'chkCalcAlwaysMD5', parent=self.pnCfg,
              pos=wx.Point(5, 144), size=wx.Size(88, 30), style=0)
        self.chkCalcAlwaysMD5.SetValue(True)

        self.chkAccess = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKACCESS,
              label=_(u'access time'), name=u'chkAccess', parent=self.pnCfg,
              pos=wx.Point(111, 93), size=wx.Size(113, 30), style=0)
        self.chkAccess.SetValue(False)

        self.chkMod = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKMOD,
              label=_(u'modification time'), name=u'chkMod', parent=self.pnCfg,
              pos=wx.Point(111, 123), size=wx.Size(113, 30), style=0)
        self.chkMod.SetValue(False)

        self.chkCreate = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKCREATE,
              label=_(u'create time'), name=u'chkCreate', parent=self.pnCfg,
              pos=wx.Point(111, 153), size=wx.Size(113, 30), style=0)
        self.chkCreate.SetValue(False)

        self.chkAutoExpand = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKAUTOEXPAND,
              label=_(u'auto expand'), name=u'chkAutoExpand', parent=self.pnCfg,
              pos=wx.Point(242, 93), size=wx.Size(95, 30), style=0)
        self.chkAutoExpand.SetValue(False)

        self.chkShowEqual = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKSHOWEQUAL,
              label=_(u'show equal'), name=u'chkShowEqual', parent=self.pnCfg,
              pos=wx.Point(242, 123), size=wx.Size(95, 30), style=0)
        self.chkShowEqual.SetValue(False)

        self.chkFlat = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKFLAT,
              label=_(u'flat'), name=u'chkFlat', parent=self.pnCfg,
              pos=wx.Point(242, 153), size=wx.Size(95, 30), style=0)
        self.chkFlat.SetValue(False)

        self.chkSelProc = wx.CheckBox(id=wxID_VBROWSEDIRPANELCHKSELPROC,
              label=_(u'select processing'), name=u'chkSelProc', parent=self.pnCfg,
              pos=wx.Point(242, 153), size=wx.Size(95, 30), style=0)
        self.chkSelProc.SetValue(False)

        self.cbClrCmp = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VBROWSEDIRPANELCBCLRCMP, label=_(u'Clear'),
              name=u'cbClrCmp', parent=self.pnCfg, pos=wx.Point(434, 76),
              size=wx.Size(76, 36), style=0)
        self.cbClrCmp.Bind(wx.EVT_BUTTON, self.OnCbClrCmpButton,
              id=wxID_VBROWSEDIRPANELCBCLRCMP)

        self.pnData = wx.Panel(id=wxID_VBROWSEDIRPANELPNDATA, name=u'pnData',
              parent=self.nbData, pos=wx.Point(0, 0), size=wx.Size(510, 331),
              style=wx.TAB_TRAVERSAL)

        self.trlstRes = vidarc.tool.misc.vtmTreeListCtrl.vtmTreeListCtrl(id=wxID_VBROWSEDIRPANELTRLSTRES,
              name=u'trlstRes', parent=self.pnData, pos=wx.Point(0, 0),
              size=wx.Size(434, 331), style=wx.TR_HAS_BUTTONS|wx.TR_MULTIPLE)
        self.trlstRes.SetMinSize((-1 , -1))
        self._init_coll_trlstRes_Columns(self.trlstRes)
        self.trlstRes.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrlstResTreeSelChanged, id=wxID_VBROWSEDIRPANELTRLSTRES)
        self.trlstRes.Bind(wx.EVT_TREE_ITEM_EXPANDING,
              self.OnTrlstResTreeItemExpanding,
              id=wxID_VBROWSEDIRPANELTRLSTRES)

        self.tgTargetSrc1 = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Pin),
              id=wxID_VBROWSEDIRPANELTGTARGETSRC1, name=u'tgTargetSrc1',
              parent=self.pnData, pos=wx.Point(442, 0), size=wx.Size(32, 30),
              style=0)
        self.tgTargetSrc1.SetToolTipString(_(u'target source 1 active'))
        self.tgTargetSrc1.SetToggle(False)
        self.tgTargetSrc1.Bind(wx.EVT_BUTTON, self.OnTgTarget1Button,
              id=wxID_VBROWSEDIRPANELTGTARGETSRC1)

        self.tgTargetSrc2 = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Pin),
              id=wxID_VBROWSEDIRPANELTGTARGETSRC2, name=u'tgTargetSrc2',
              parent=self.pnData, pos=wx.Point(478, 0), size=wx.Size(32, 30),
              style=0)
        self.tgTargetSrc2.SetToggle(False)
        self.tgTargetSrc2.SetToolTipString(_(u'target source 2 active'))
        self.tgTargetSrc2.Bind(wx.EVT_BUTTON, self.OnTgTargetSrc2Button,
              id=wxID_VBROWSEDIRPANELTGTARGETSRC2)

        self.cbCpTo1 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Copy),
              id=wxID_VBROWSEDIRPANELCBCPTO1, name=u'cbCpTo1',
              parent=self.pnData, pos=wx.Point(442, 42), size=wx.Size(32, 30),
              style=0)
        self.cbCpTo1.SetToolTipString(_(u'copy source 2 to 1'))
        self.cbCpTo1.Bind(wx.EVT_BUTTON, self.OnCbCpTo1Button,
              id=wxID_VBROWSEDIRPANELCBCPTO1)

        self.cbCpTo2 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Copy),
              id=wxID_VBROWSEDIRPANELCBCPTO2, name=u'cbCpTo2',
              parent=self.pnData, pos=wx.Point(478, 42), size=wx.Size(32, 30),
              style=0)
        self.cbCpTo2.SetToolTipString(_(u'copy source 1 to 2'))
        self.cbCpTo2.Bind(wx.EVT_BUTTON, self.OnCbCpTo2Button,
              id=wxID_VBROWSEDIRPANELCBCPTO2)

        self.cbMvTo2 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Move),
              id=wxID_VBROWSEDIRPANELCBMVTO2, name=u'cbMvTo2',
              parent=self.pnData, pos=wx.Point(478, 76), size=wx.Size(32, 30),
              style=0)
        self.cbMvTo2.SetToolTipString(_(u'move source 1 to 2'))
        self.cbMvTo2.Bind(wx.EVT_BUTTON, self.OnCbMvTo2Button,
              id=wxID_VBROWSEDIRPANELCBMVTO2)

        self.cbMvTo1 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Move),
              id=wxID_VBROWSEDIRPANELCBMVTO1, name=u'cbMvTo1',
              parent=self.pnData, pos=wx.Point(442, 76), size=wx.Size(32, 30),
              style=0)
        self.cbMvTo1.SetToolTipString(_(u'move source 2 to 1'))
        self.cbMvTo1.Bind(wx.EVT_BUTTON, self.OnCbMvTo1Button,
              id=wxID_VBROWSEDIRPANELCBMVTO1)

        self.cbDel1 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VBROWSEDIRPANELCBDEL1, name=u'cbDel1', parent=self.pnData,
              pos=wx.Point(442, 118), size=wx.Size(32, 30), style=0)
        self.cbDel1.SetToolTipString(_(u'delete source 1'))
        self.cbDel1.Bind(wx.EVT_BUTTON, self.OnCbDel1Button,
              id=wxID_VBROWSEDIRPANELCBDEL1)

        self.cbDel2 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VBROWSEDIRPANELCBDEL2, name=u'cbDel2', parent=self.pnData,
              pos=wx.Point(478, 118), size=wx.Size(32, 30), style=0)
        self.cbDel2.SetToolTipString(_(u'delete source 2'))
        self.cbDel2.Bind(wx.EVT_BUTTON, self.OnCbDel2Button,
              id=wxID_VBROWSEDIRPANELCBDEL2)

        self.cbOpenSrc1 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Browse),
              id=wxID_VBROWSEDIRPANELCBOPENSRC1, name=u'cbOpenSrc1',
              parent=self.pnData, pos=wx.Point(442, 160), size=wx.Size(32, 30),
              style=0)
        self.cbOpenSrc1.SetToolTipString(_(u'open source file 1'))
        self.cbOpenSrc1.Bind(wx.EVT_BUTTON, self.OnCbOpenSrc1Button,
              id=wxID_VBROWSEDIRPANELCBOPENSRC1)

        self.cbOpenSrc2 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Browse),
              id=wxID_VBROWSEDIRPANELCBOPENSRC2, name=u'cbOpenSrc2',
              parent=self.pnData, pos=wx.Point(478, 160), size=wx.Size(32, 30),
              style=0)
        self.cbOpenSrc2.SetToolTipString(_(u'open source file 2'))
        self.cbOpenSrc2.Bind(wx.EVT_BUTTON, self.OnCbOpenSrc2Button,
              id=wxID_VBROWSEDIRPANELCBOPENSRC2)

        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Down),
              id=wxID_VBROWSEDIRPANELCBPOPUP, name=u'cbPopup',
              parent=self.pnData, pos=wx.Point(442, 194), size=wx.Size(32, 30),
              style=0)
        self.cbPopup.SetToolTipString(_(u'show detail file informations'))
        self.cbPopup.Bind(wx.EVT_BUTTON, self.OnPopupButton,
              id=wxID_VBROWSEDIRPANELCBPOPUP)

        self.cbRemove = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VBROWSEDIRPANELCBREMOVE, name=u'cbRemove',
              parent=self.pnData, pos=wx.Point(478, 194), size=wx.Size(31, 30),
              style=0)
        self.cbRemove.SetToolTipString(_(u'remove selected tree item'))
        self.cbRemove.Bind(wx.EVT_BUTTON, self.OnCbRemoveButton,
              id=wxID_VBROWSEDIRPANELCBREMOVE)

        self.cbClrAll = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VBROWSEDIRPANELCBCLRALL, name=u'cbClrAll',
              parent=self.pnData, pos=wx.Point(442, 244), size=wx.Size(31, 30),
              style=0)
        self.cbClrAll.SetToolTipString(_(u'clear all'))
        self.cbClrAll.Bind(wx.EVT_BUTTON, self.OnCbClrAllButton,
              id=wxID_VBROWSEDIRPANELCBCLRALL)

        self.thdCtrl = vidarc.tool.misc.vtmThreadCtrl.vtmThreadCtrl(id=wxID_VBROWSEDIRPANELTHDCTRL,
              name=u'thdCtrl', parent=self, pos=wx.Point(0, 375),
              size=wx.Size(518, 0), style=0)

        self._init_coll_nbData_Pages(self.nbData)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vToolsBrowseDir')
        self._init_ctrls(parent)
        self.SetName(name)
        self.cfgFunc=None
        self.dCfg={}
        self.xdCfg=vtXmlDom(appl='vBrowseDirCfg',audit_trail=False)
        
        self.pnLog=vBrowseDirLogPanel(self.nbData,wx.NewId(),pos=wx.DefaultPosition,
                        size=wx.DefaultSize,style=0,name='pnLog')
        self.nbData.AddPage(self.pnLog,_(u'log'))
        imgLst=wx.ImageList(16,16)
        l=[]
        l.append(imgLst.Add(vtArt.getBitmap(vtArt.Settings)))
        l.append(imgLst.Add(vtArt.getBitmap(vtArt.Report)))
        l.append(imgLst.Add(vtArt.getBitmap(vtArt.Log)))
        self.nbData.AssignImageList(imgLst)
        for i in xrange(3):
            self.nbData.SetPageImage(i,l[i])
        
        self.lDN=['','']
        self.tiSel=None
        self.tiResSel=None
        self.dirEntrySrc1=None
        self.dirEntrySrc2=None
        self.dirComp=None
        self.tCount1=(0,0,0)
        self.tCount2=(0,0,0)
        self.iTarget=0
        self.iAct=0
        self.iCount=0
        self.thdRead=vtInOutReadThread(self.nbData,True)
        self.thdRead.BindEvents(self.OnInOutReadProc,
                            self.OnInOutReadFin,
                            self.OnInOutReadAbort)
        #self.thdRead.BindEvents(self.OnInOutReadProc)
        #EVT_VTINOUT_READ_THREAD_ELEMENTS(self,self.OnInOutReadProc)
        #EVT_VTINOUT_READ_THREAD_FINISHED(self,self.OnInOutReadFin)
        #EVT_VTINOUT_READ_THREAD_ABORTED(self,self.OnInOutReadAbort)
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        idxNrm=self.imgLst.Add(vBrowseDirImages.getDirCloseBitmap())
        idxSel=self.imgLst.Add(vBrowseDirImages.getDirOpenBitmap())
        self.dImg['Dir']=[idxNrm,idxSel]
        #idxNrm=self.imgLst.Add(vBrowseDirImages.getSnapBitmap())
        #self.dImg['Snap']=[idxNrm,idxNrm]
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Cancel))
        self.dImg['qst']=self.imgLst.Add(vtArt.getBitmap(vtArt.Question))
        self.dImg['wrn']=self.imgLst.Add(vtArt.getBitmap(vtArt.Warning))
        self.dImg['file']=self.imgLst.Add(vtArt.getBitmap(vtArt.Element))
        self.dImg['Cal']=self.imgLst.Add(vtArt.getBitmap(vtArt.Measure))
        self.dImg['Cmp']=self.imgLst.Add(vtArt.getBitmap(vtArt.Report))
        #self.trDirSnap.SetImageList(self.imgLst)
        t=self.dImg['Dir']
        #self.trDirSnap.AddRoot('Root',t[0],t[1])
        
        bmp=vBrowseDirImages.getCp2To1Bitmap()
        self.cbCpTo1.SetBitmapLabel(bmp)
        self.dImg['Cp2To1']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getCp1To2Bitmap()
        self.cbCpTo2.SetBitmapLabel(bmp)
        self.dImg['Cp1To2']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getMv2To1Bitmap()
        self.cbMvTo1.SetBitmapLabel(bmp)
        self.dImg['Mv2To1']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getMv1To2Bitmap()
        self.cbMvTo2.SetBitmapLabel(bmp)
        self.dImg['Mv1To2']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getSrc1Bitmap()
        self.tgTargetSrc1.SetBitmapLabel(bmp)
        self.dImg['Src1']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getSrc2Bitmap()
        self.tgTargetSrc2.SetBitmapLabel(bmp)
        self.dImg['Src2']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getSrcsBitmap()
        self.dImg['Srcs']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getDel1Bitmap()
        self.cbDel1.SetBitmapLabel(bmp)
        self.dImg['Del1']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getDel2Bitmap()
        self.cbDel2.SetBitmapLabel(bmp)
        self.dImg['Del2']=self.imgLst.Add(bmp)
        bmp=vBrowseDirImages.getOpenSrc1Bitmap()
        self.cbOpenSrc1.SetBitmapLabel(bmp)
        bmp=vBrowseDirImages.getOpenSrc2Bitmap()
        self.cbOpenSrc2.SetBitmapLabel(bmp)
        
        self.dImgAct={
            self.ACT_CP2TO1:    self.dImg['Cp2To1'],
            self.ACT_CP1TO2:    self.dImg['Cp1To2'],
            self.ACT_MV2TO1:    self.dImg['Mv2To1'],
            self.ACT_MV1TO2:    self.dImg['Mv1To2'],
            self.ACT_DEL1:      self.dImg['Del1'],
            self.ACT_DEL2:      self.dImg['Del2'],
            }
        
        bmp=vtArt.getBitmap(vtArt.Build)
        self.dImg['proc']=self.imgLst.Add(bmp)
        
        iAlignFlag=wx.ALIGN_RIGHT
        self.trlstRes.SetColumnAlignment(1,iAlignFlag)
        self.trlstRes.SetColumnAlignment(3,iAlignFlag)
        self.trlstRes.SetColumnAlignment(4,iAlignFlag)
        self.trlstRes.SetColumnAlignment(5,iAlignFlag)
        self.trlstRes.SetColumnAlignment(6,iAlignFlag)
        self.trlstRes.SetColumnAlignment(7,iAlignFlag)
        self.trlstRes.SetColumnWidth(1,80)
        self.trlstRes.SetColumnWidth(2,24)
        self.trlstRes.SetColumnWidth(3,30)
        self.trlstRes.SetColumnWidth(4,30)
        self.trlstRes.SetColumnWidth(5,30)
        self.trlstRes.SetColumnWidth(6,30)
        self.trlstRes.SetColumnWidth(7,30)
        self.trlstRes.SetColumnWidth(8,30)
        #self.trlstRes.SetColumnWidth(9,30)
        self.trlstRes.SetStretchLst([(0,-1)])
        self.trlstRes.SetImageList(self.imgLst)
        
        self.popWin=vBrowseDirResultDialog(self)
        self.bBusy=False
        self.widLogging=vtLog.GetPrintMsgWid(self)
        self.dlgFile=None
        self.cbClrSrc1.Enable(False)
        self.cbClrSrc2.Enable(False)
        self.cbClrCmp.Enable(False)
        self.enableResWid(self.trlstRes,None)
        self.spRec.SetValue(10)
        #self.Move(pos)
        #self.SetSize(size)
        #parent.Bind(wx.EVT_CLOSE, self.OnFrmClose)
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def OnInOutReadProc(self,evt):
        self.thdCtrl.OnThreadProc(evt)
    def OnInOutReadFin(self,evt):
        #self.thdCtrl.OnThreadFinished(evt)
        pass
    def OnInOutReadAbort(self,evt):
        #self.thdCtrl.OnThreadAborted(evt)
        pass
    def OnFrmClose(self,evt):
        self.GetCfgData()
        evt.Skip()
    def OnSize(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            bMod=False
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
            wx.LayoutAlgorithm().LayoutWindow(self, self.nbData)
            self.nbData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMove(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            #pos=self.GetParent().GetPosition()
            #self.dCfg['pos']='%d,%d'%(pos[0],pos[1])
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
        self.xdCfg.Save(fn)
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
        
        for sObjName,sCfgKey in self.LST_CFGOBJ_MAP:
            try:
                if sCfgKey in self.dCfg:
                    if self.dCfg[sCfgKey].strip()!='0':
                        self.__dict__[sObjName].SetValue(True)
                    else:
                        self.__dict__[sObjName].SetValue(False)
            except:
                vtLog.vtLngTB(self.GetName())
        try:
            self.spRec.SetValue(int(self.dCfg.get('calc_reclimit',10)))
        except:
            vtLog.vtLngTB(self.GetName())
        
        self.lDN=[]
        dDN={}
        try:
            keys=self.dCfg.keys()
            for k in keys:
                if k[:2].find('DN')==0:
                    if len(k)==2:
                        pass
                    else:
                        dDN[k[2:]]=self.dCfg[k]
                        #self.lDN.append(self.dCfg[k])
        except:
            pass
        lKeys=dDN.keys()
        lKeys.sort()
        if len(lKeys)>2:
            lKeys=lKeys[:2]
        for k in lKeys:
            self.lDN.append(dDN[k])
        iLen=len(self.lDN)
        if iLen>=1:
            self.dbbSrc1.SetValue(self.lDN[0])
        else:
            self.lDN.append('')
        if iLen>=2:
            self.dbbSrc2.SetValue(self.lDN[1])
        else:
            self.lDN.append('')
        self.thdProc.CallBack(self.getTarget)
    def GetCfgData(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
        keys=self.dCfg.keys()
        for k in keys:
            if k[:2].find('DN')==0:
                del self.dCfg[k]
        #self.lDN.sort()
        i=0
        for sDN in self.lDN:
            self.dCfg['DN_%03d'%i]=sDN
            i+=1
        
        for sObjName,sCfgKey in self.LST_CFGOBJ_MAP:
            try:
                if self.__dict__[sObjName].GetValue():
                    self.dCfg[sCfgKey]=1
                else:
                    self.dCfg[sCfgKey]=0
            except:
                vtLog.vtLngTB(self.GetName())
        #if self.chkCalcMD5.GetValue():
        #    self.dCfg['calc_md5']=1
        #else:
        #    self.dCfg['calc_md5']=0
        self.dCfg['calc_reclimit']=self.spRec.GetValue()
        
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def OnCbReadSrc1Button(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.tCount1=(0,0,0)
            self.dirComp=None
            self.DoRead1()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbReadSrc2Button(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc2=None
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.DoRead2()
        except:
            vtLog.vtLngTB(self.GetName())
    def getTreeSel(self,tr):
        self.tiSel=tr.GetSelection()
    def procRead1(self,sDNSel,iRecLv,iStyleRd):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
        if os.path.exists(sDNSel):
            thd.Post('proc',0,100)
            thd.Post('proc',_(u'reading source 1 ...'),None)
            self.lDN[0]=sDNSel
            self.thdRead.procReadWalk(sDNSel,None,iRecLv,0,iStyleRd,True,self.refreshSel,1)
            self.tCount1=self.thdRead.GetCountTup()
        else:
            thd.Post('proc',_(u'    reading aborted, directory does not exist'),None)
    def procRead2(self,sDNSel,iRecLv,iStyleRd):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
        if os.path.exists(sDNSel):
            thd.Post('proc',0,100)
            thd.Post('proc',_(u'reading source 2 ...'),None)
            self.lDN[1]=sDNSel
            self.thdRead.procReadWalk(sDNSel,None,iRecLv,0,iStyleRd,True,self.refreshSel,2)
            self.tCount2=self.thdRead.GetCountTup()
        else:
            thd.Post('proc',_(u'    reading aborted, directory does not exist'),None)
    def procDummy(self):
        pass
    def DoRead1(self):
        try:
            thd=self.thdCtrl.GetThread()
            iStyleRd=0
            if self.chkCalcMD5.GetValue():
                if self.chkCalcAlwaysMD5.GetValue():
                    iStyleRd|=0x01
                else:
                    iStyleRd|=0x20
            iStyleRd|=0x10
            iRecLv=self.spRec.GetValue()
            if iRecLv<0:
                iRecLv=sys.maxint
            if self.chkFlat.GetValue():
                bFlat=True
            else:
                bFlat=False
            if self.dirEntrySrc1 is None:
                sDNSel=self.dbbSrc1.GetValue()
                if len(sDNSel)>0:
                    if os.path.exists(sDNSel):
                        self.thdRead.Start()
                        self.thdCtrl.Do(self.procRead1,sDNSel,iRecLv,iStyleRd)
                        return True
                    else:
                        thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
                        thd.Post('proc',_(u'   does not exist'),None)
                else:
                    thd.Post('proc',_(u'source 1:%s')%sDNSel,None)
                    thd.Post('proc',_(u'   not defined'),None)
            else:
                thd.Post('proc',_(u'source 1 already calculated'),None)
        except:
            vtLog.vtLngTB(self.GetName())
        return False
    def DoRead2(self):
        try:
            thd=self.thdCtrl.GetThread()
            iStyleRd=0
            if self.chkCalcMD5.GetValue():
                if self.chkCalcAlwaysMD5.GetValue():
                    iStyleRd|=0x01
                else:
                    iStyleRd|=0x20
            iStyleRd|=0x10
            iRecLv=self.spRec.GetValue()
            if iRecLv<0:
                iRecLv=sys.maxint
            if self.chkFlat.GetValue():
                bFlat=True
            else:
                bFlat=False
            if self.dirEntrySrc2 is None:
                sDNSel=self.dbbSrc2.GetValue()
                if len(sDNSel)>0:
                    if os.path.exists(sDNSel):
                        self.thdRead.Start()
                        self.thdCtrl.Do(self.procRead2,sDNSel,iRecLv,iStyleRd)
                        return True
                    else:
                        thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
                        thd.Post('proc',_(u'   does not exist'),None)
                else:
                    thd.Post('proc',_(u'source 2:%s')%sDNSel,None)
                    thd.Post('proc',_(u'   not defined'),None)
            else:
                thd.Post('proc',_(u'source 2 already calculated'),None)
        except:
            vtLog.vtLngTB(self.GetName())
        return False
    def procCmp(self,iStyle,bFlat=False):
        thd=self.thdCtrl.GetThread()
        thd.Post('proc',0,100)
        thd.Post('proc',_(u'comparing ...'),None)
        self.thdRead.Start()
        iCount=self.tCount1[0]+self.tCount2[0]+self.tCount1[3]+self.tCount2[3]
        if bFlat==True:
            self.dirComp.compareFlat(self.dirEntrySrc1,self.dirEntrySrc2,
                            iStyle,iCount,self.thdRead)
        else:
            self.dirComp.compareHier(self.dirEntrySrc1,self.dirEntrySrc2,
                            iStyle,iCount,self.thdRead)
        thd.Post('proc',_(u'compare done'),None)
        if thd.Is2Stop()==False:
            thd.CallBack(self.cbClrCmp.Enable,True)
            thd.CallBack(self.refreshComp,self.dirEntrySrc1.base,self.dirComp)
        else:
            self.dirComp=None
    def DoCmp(self):
        try:
            thd=self.thdCtrl.GetThread()
            self.dirComp=vtInOutCompDirEntry()
            iStyle=0x08
            if self.chkCreate.GetValue():
                iStyle|=0x04
            if self.chkMod.GetValue():
                iStyle|=0x02
            if self.chkCalcMD5.GetValue():
                if self.chkCalcAlwaysMD5.GetValue():
                    pass
                else:
                    iStyle|=0x10
            if self.chkAccess.GetValue():
                iStyle|=0x01
            if self.chkFlat.GetValue():
                bFlat=True
            else:
                bFlat=False
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.thdCtrl.Do(self.procCmp,iStyle,bFlat)
            self.nbData.SetSelection(1)
        except:
            vtLog.vtLngTB(self.GetName())
    def Start(self):
        try:
            thd=self.thdCtrl.GetThread()
            thd.Start()
            bProc=False
            bProc|=self.DoRead1()
            bProc|=self.DoRead2()
            if bProc:
                self.thdCtrl.Do(self.procDummy)
                self.trlstRes.DeleteAllItems()
                self.enableResWid(self.trlstRes,None)
                self.dirComp=None
            elif self.dirComp is None:
                self.thdCtrl.Do(self.procDummy)
                self.DoCmp()
            else:
                if self.DoBuildApplyCmd():
                    self.thdCtrl.Do(self.procDummy)
                else:
                    self.thdCtrl.Do(self.procDummy)
        except:
            vtLog.vtLngTB(self.GetName())
    def Stop(self):
        try:
            self.thdRead.Stop()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnThreadProc(self,evt):
        evt.Skip()
        if evt.GetCount() is None:
            self.pnLog.AddLog(evt.GetAct())
    def OnThreadAborted(self,evt):
        evt.Skip()
    def OnThreadFin(self,evt):
        evt.Skip()
    def getTarget(self):
        self.iTarget=0
        if self.tgTargetSrc1.GetValue():
            self.iTarget=1
        elif self.tgTargetSrc2.GetValue():
            self.iTarget=2
    def refreshSel(self,dn,dirEntry,iSrc):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        self.thdProc.Start()
        self.thdProc.DoCallBack(self.showRes,dn,dirEntry,iSrc)
    def showRes(self,dn,dirEntry,iSrc):
        try:
            thd=self.thdCtrl.GetThread()
            if iSrc==1:
                self.dirEntrySrc1=dirEntry
                if dirEntry is None:
                    thd.Post('proc',_(u'source 1 aborted'),None)
                    return
                self.cbClrSrc1.Enable(True)
                imgSrcIdx=self.dImg['Src1']
                iTyp=1
                thd.Post('proc',_(u'source 1 done'),None)
            elif iSrc==2:
                self.dirEntrySrc2=dirEntry
                if dirEntry is None:
                    thd.Post('proc',_(u'source 2 aborted'),None)
                    return
                self.cbClrSrc2.Enable(True)
                imgSrcIdx=self.dImg['Src2']
                iTyp=2
                thd.Post('proc',_(u'source 2 done'),None)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,
                        'dn:%s;src1 is None=%d;src2 is None=%d'%(dn,
                        self.dirEntrySrc1 is None,self.dirEntrySrc2 is None),self)
            if thd.IsEmpty()==False:
                return
            if self.dirEntrySrc1 is None or self.dirEntrySrc2 is None:
                if thd.IsEmpty():
                    imgIdx=self.dImg['Cal']
                    if iSrc==1:
                        thd.Post('proc',_(u'source 1 show'),None)
                        tiRoot=self.trlstRes.AddRoot(_(u'source 1'),imgIdx,imgIdx)
                    elif iSrc==2:
                        thd.Post('proc',_(u'source 2 show'),None)
                        tiRoot=self.trlstRes.AddRoot(_(u'source 2'),imgIdx,imgIdx)
                    else:
                        vtLog.vtLngCurWX(vtLog.CRITICAL,'you are not supposed to be here;iSrc:%d'%(iSrc),self)
                        return
                    self.trlstRes.SetPyData(tiRoot,[dirEntry,iTyp,False])
                    sz=listFiles.getSizeStr(dirEntry.size,2)
                    self.trlstRes.SetItemText(tiRoot,sz,1)
                    self.trlstRes.SetItemImage(tiRoot,imgSrcIdx,column=2,
                                        which=wx.TreeItemIcon_Normal)
                    self.showResSub(self.trlstRes,tiRoot)
                    self.trlstRes.Expand(tiRoot)
                    self.enableResWid(self.trlstRes,tiRoot)
            else:
                thd.CallBack(self.DoCmp)
        except:
            vtLog.vtLngTB(self.GetName())
    def showResSub(self,trlst,ti):
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iTyp=l[1]
            imgSrcIdx=trlst.GetItemImage(ti,column=2,
                                    which=wx.TreeItemIcon_Normal)
            iSkip=len(dE.base)
            if dE.base[-1]!='/':
                iSkip+=1
            t=self.dImg['Dir']
            for d in dE.dirs:
                tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetPyData(tc,[d,iTyp,False])
                sz=listFiles.getSizeStr(d.size,2)
                trlst.SetItemText(tc,sz,iTyp)
                trlst.SetItemHasChildren(tc,True)
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
                trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
            tf=trlst.AppendItem(ti,'.')
            trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
            trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
            trlst.SetItemImage(tf,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
            t=self.dImg['file']
            iTyp+=4
            for f in dE.files:
                tc=trlst.AppendItem(tf,f.name)
                trlst.SetPyData(tc,[f,iTyp,True])
                sz=listFiles.getSizeStr(f.size,2)
                md5=f.md5 or ''
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
                trlst.SetItemText(tc,sz,1)
                trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
        except:
            vtLog.vtLngTB(self.GetName())
    def showCmp(self,dn,dirCmp,bEqual):
        vtLog.vtLngCurWX(vtLog.INFO,'start',self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        imgIdx=self.dImg['Cmp']
        imgSrcIdx=self.dImg['Srcs']
        tiRoot=self.trlstRes.AddRoot(_(u'compare results'),imgIdx,imgIdx)
        self.trlstRes.SetPyData(tiRoot,[dirCmp,3,False,self.ACT_UNDEF])
        self.trlstRes.SetItemImage(tiRoot,imgSrcIdx,column=2,
                                    which=wx.TreeItemIcon_Normal)
        self.showCmpSub(self.trlstRes,tiRoot,bEqual)
        self.trlstRes.SelectItem(tiRoot)
        self.trlstRes.Expand(tiRoot)
        self.enableResWid(self.trlstRes,tiRoot)
        if self.iTarget!=0:
            thd=self.thdCtrl.GetThread()
            if self.iTarget==1:
                thd.DoCallBackWX(self.SetCmd,self.ACT_TARGET1)
            elif self.iTarget==2:
                thd.DoCallBackWX(self.SetCmd,self.ACT_TARGET2)
        vtLog.vtLngCurWX(vtLog.INFO,'done',self)
    def showCmpSub(self,trlst,ti,bEqual):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iSkip=len(dE.base)#+1
            t=self.dImg['Dir']
            for d in dE.dir_results:
                bAdd=False
                for f in d.file_results:
                    if f.isEqual():
                        if bEqual==False:
                            continue
                    bAdd=True
                    break
                if len(d.dir_results)==0 and bAdd==False:
                    continue
                if bEqual==False:
                    if d.getDiffCount()==0:
                        continue
                if d.base[iSkip]=='/':
                    tc=trlst.AppendItem(ti,d.base[iSkip+1:])
                else:
                    tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
                trlst.SetPyData(tc,[d,3,False,self.ACT_UNDEF])
                trlst.SetItemHasChildren(tc,True)
            bAdd=False
            for f in dE.file_results:
                if f.isEqual():
                    if bEqual==False:
                        continue
                bAdd=True
                break
            if bAdd==True:
                tf=trlst.AppendItem(ti,'.')
                trlst.SetPyData(tf,[l[0],3,True,self.ACT_UNDEF])
                trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
                bAdd=False
                for f in dE.file_results:
                    t=self.dImg['wrn']
                    if f.isEqual():
                        if bEqual==False:
                            continue
                        t=self.dImg['ok']
                    bAdd=True
                    name=f.getName()
                    iSz=f.getSize()
                    if iSz<0:
                        sz=''
                    else:
                        sz=listFiles.getSizeStr(iSz,2)
                    #md5=f.getMD5()
                    tc=trlst.AppendItem(tf,name)
                    trlst.SetPyData(tc,[f,4,True,self.ACT_UNDEF])
                    trlst.SetItemText(tc,sz,1)
                    #trlst.SetItemText(tc,md5,2)
                    if f.hasSrc1():
                        if f.hasSrc2():
                            imgSrcIdx=self.dImg['Srcs']
                        else:
                            imgSrcIdx=self.dImg['Src1']
                    else:
                        if f.hasSrc2():
                            imgSrcIdx=self.dImg['Src2']
                        else:
                            imgSrcIdx=-1
                    if imgSrcIdx>=0:
                        trlst.SetItemImage(tc,imgSrcIdx,column=2,
                                        which=wx.TreeItemIcon_Normal)
                    if f.isDiffSize():
                        trlst.SetItemText(tc,'X',4)
                        trlst.SetItemText(tc,'X',5)
                    if f.isDiffContent():
                        trlst.SetItemText(tc,'X',5)
                    if f.isDiffAccess():
                        trlst.SetItemText(tc,'X',6)
                    if f.isDiffModify():
                        trlst.SetItemText(tc,'X',7)
                    if f.isDiffCreate():
                        trlst.SetItemText(tc,'X',8)
                    trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                    trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
        except:
            vtLog.vtLngTB(self.GetName())
    def getTreeLabel(self,trlst,ti):
        tp=ti
        tr=trlst.GetRootItem()
        l=[]
        while tp is not None and tp!=tr:
            l.append(trlst.GetItemText(tp))
            tp=trlst.GetItemParent(tp)
        l.reverse()
        return l
    def findTreeItem(self,trlst,ti,sLbl,bRec=False):
        tp=ti
        t=trlst.GetFirstChild(ti)
        while t[0].IsOk():
            if trlst.GetItemText(t[0])==sLbl:
                return t[0]
            t=trlst.GetNextChild(ti,t[1])
        return None
    def enableResWid(self,trlst,ti):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if ti is None:
                self.cbCpTo1.Enable(False)
                self.cbCpTo2.Enable(False)
                self.cbMvTo1.Enable(False)
                self.cbMvTo2.Enable(False)
                self.cbDel1.Enable(False)
                self.cbDel2.Enable(False)
            else:
                l=trlst.GetPyData(ti)
                if l is None:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(False)
                elif l[1]==1:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(False)
                elif l[1]==2:
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(True)
                elif l[1]==3:
                    self.cbCpTo1.Enable(True)
                    self.cbCpTo2.Enable(True)
                    self.cbMvTo1.Enable(True)
                    self.cbMvTo2.Enable(True)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(True)
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc1():
                        self.cbOpenSrc1.Enable(True)
                        self.popWin.SetSrc1(f.file1,self.dirEntrySrc1.base)
                        self.cbCpTo2.Enable(True)
                        self.cbMvTo2.Enable(True)
                        self.cbDel1.Enable(True)
                    else:
                        self.cbOpenSrc1.Enable(False)
                        self.popWin.SetSrc1(None,'')
                        self.cbCpTo2.Enable(False)
                        self.cbMvTo2.Enable(False)
                        self.cbDel1.Enable(False)
                    if f.hasSrc2():
                        if self.dirEntrySrc1.base==self.dirEntrySrc2.base:
                            self.cbOpenSrc2.Enable(False)
                            self.popWin.SetSrc2(None,'')
                            self.cbCpTo1.Enable(False)
                            self.cbMvTo1.Enable(False)
                            self.cbDel2.Enable(False)
                        else:
                            self.cbOpenSrc2.Enable(True)
                            self.cbCpTo1.Enable(True)
                            self.cbMvTo1.Enable(True)
                            self.cbDel2.Enable(True)
                        self.popWin.SetSrc2(f.file2,self.dirEntrySrc2.base)
                    else:
                        self.cbOpenSrc2.Enable(False)
                        self.popWin.SetSrc2(None,'')
                        self.cbCpTo1.Enable(False)
                        self.cbMvTo1.Enable(False)
                        self.cbDel2.Enable(False)
                    return
                elif l[1]==5:
                    f=l[0]
                    self.cbOpenSrc1.Enable(True)
                    self.popWin.SetSrc1(f,self.dirEntrySrc1.base)
                    self.cbOpenSrc2.Enable(False)
                    self.popWin.SetSrc2(None,'')
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(True)
                    self.cbDel2.Enable(False)
                    return
                elif l[1]==6:
                    f=l[0]
                    self.cbOpenSrc2.Enable(True)
                    self.popWin.SetSrc2(f,self.dirEntrySrc2.base)
                    self.cbOpenSrc1.Enable(False)
                    self.popWin.SetSrc1(None,'')
                    self.cbCpTo1.Enable(False)
                    self.cbCpTo2.Enable(False)
                    self.cbMvTo1.Enable(False)
                    self.cbMvTo2.Enable(False)
                    self.cbDel1.Enable(False)
                    self.cbDel2.Enable(True)
                    return
            self.cbOpenSrc1.Enable(False)
            self.cbOpenSrc2.Enable(False)
            self.popWin.SetSrc1(None,'')
            self.popWin.SetSrc2(None,'')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTrlstResTreeItemExpanding(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            l=self.trlstRes.GetPyData(ti)
            if l is None:
                return
            if l[1]==1:
                self.showResSub(self.trlstRes,ti)
            if l[1]==2:
                self.showResSub(self.trlstRes,ti)
            elif l[1]==3:
                bEqual=self.chkShowEqual.GetValue()
                self.showCmpSub(self.trlstRes,ti,bEqual)
        except:
            vtLog.vtLngTB(self.GetName())
    def refreshComp(self,dn,dirCmp):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        bEqual=self.chkShowEqual.GetValue()
        self.thdRead.DoCallBack(self.showCmp,dn,dirCmp,bEqual)
    def __openFile__(self,sDN,sFN):
        sExts=sFN.split('.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        filename=os.path.join(sDN,sFN)
        try:
            mime = fileType.GetMimeType()
            if sys.platform=='win32':
                filename=filename.replace('/','\\')
            cmd = fileType.GetOpenCommand(filename, mime)
            wx.Execute(cmd)
            vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
            try:
                vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
            except:
                pass
        except:
            sMsg=u"Cann't open %s!"%(filename)
            dlg=vtmMsgDialog(self,sMsg ,
                                u'vCheckDirPanel',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
    def OnCbOpenSrc1Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc1()==False:
                        return
                    sFN=f.file1.fullname
                elif l[1]==5:
                    f=l[0]
                    sFN=f.fullname
                else:
                    return
                sDN=self.dirEntrySrc1.base
                self.__openFile__(sDN,sFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbOpenSrc2Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==4:
                    f=l[0]
                    if f.hasSrc2()==False:
                        return
                    sFN=f.file2.fullname
                elif l[1]==6:
                    f=l[0]
                    sFN=f.fullname
                else:
                    return
                sDN=self.dirEntrySrc2.base
                self.__openFile__(sDN,sFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnPopupButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
    def OnTrlstResTreeSelChanged(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            if self.chkAutoExpand.GetValue():
                if self.trlstRes.ItemHasChildren(ti):
                    if self.trlstRes.IsExpanded(ti)==False:
                        self.trlstRes.Expand(ti)
            self.enableResWid(self.trlstRes,ti)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getTrData__(self):
        try:
            lTi=self.trlstRes.GetSelections()
            l=[]
            for ti in lTi:
                tup=self.trlstRes.GetPyData(ti)
                if tup[1] in [3,4]:
                    if len(tup)>=4:
                        l.append((ti,tup))
            return l
        except:
            vtLog.vtLngTB(self.GetName())
        return l
    def __setCmd__(self,thd,tr,ti,iCmd,bEqual):
        try:
            if ti is None:
                return
            tup=tr.GetPyData(ti)
            self.iAct+=1
            thd.Post('proc',self.iAct,self.iCount)
            try:
                if tup[1]==4:
                    if iCmd in [self.ACT_CP2TO1,self.ACT_MV2TO1,self.ACT_DEL2]:
                        if tup[0].hasSrc2():
                            tup[3]=iCmd
                    elif iCmd in [self.ACT_CP1TO2,self.ACT_MV1TO2,self.ACT_DEL1]:
                        if tup[0].hasSrc1():
                            tup[3]=iCmd
                    elif iCmd==self.ACT_TARGET1:
                        if tup[0].hasSrc2():
                            if tup[0].hasSrc1():
                                if tup[0].isEqual()==False:
                                    if tup[0].getDiffModify()>0:
                                        tup[3]=self.ACT_CP2TO1
                            else:
                                tup[3]=self.ACT_CP2TO1
                    elif iCmd==self.ACT_TARGET2:
                        if tup[0].hasSrc1():
                            if tup[0].hasSrc2():
                                if tup[0].isEqual()==False:
                                    if tup[0].getDiffModify()<0:
                                        tup[3]=self.ACT_CP1TO2
                            else:
                                tup[3]=self.ACT_CP1TO2
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s'%(tup[0].getName()),self)
                    
                elif tup[1]==3:
                    tup[3]=iCmd
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s'%(tup[0].base),self)
            except:
                vtLog.vtLngTB(self.GetName())
            imgIdx=self.dImgAct.get(tup[3],-1)
            
            tr.SetItemImage(ti,imgIdx,column=3,
                            which=wx.TreeItemIcon_Normal)
            if tup[2]==False:
                thd.DoCallBackWX(self.showCmpSub,tr,ti,bEqual)
                thd.DoCallBackWX(self.__setCmd__,thd,tr,ti,iCmd,bEqual)
            else:
                cid=tr.GetFirstChild(ti)
                while cid[0].IsOk():
                    tid=tr.GetPyData(ti)
                    if tid is not None:
                        if tid[1] in [3]:
                            if tid[2]==False:
                                thd.DoCallBackWX(self.showCmpSub,tr,cid[0],bEqual)
                                thd.DoCallBackWX(self.__setCmd__,thd,tr,cid[0],iCmd,bEqual)
                            else:
                                self.__setCmd__(thd,tr,cid[0],iCmd,bEqual)
                    cid=tr.GetNextChild(ti,cid[1])
        except:
            vtLog.vtLngTB(self.GetName())
    def getCmdTrans(self,iCmd):
        if iCmd==self.ACT_CP2TO1:
            return _(u'copy source 2 to 1')
        elif iCmd==self.ACT_CP1TO2:
            return _(u'copy source 1 to 2')
        elif iCmd==self.ACT_MV2TO1:
            return _(u'move source 2 to 1')
        elif iCmd==self.ACT_MV1TO2:
            return _(u'move source 1 to 2')
        elif iCmd==self.ACT_DEL1:
            return _(u'delete source 1')
        elif iCmd==self.ACT_DEL2:
            return _(u'delete source 2')
        elif iCmd==self.ACT_TARGET1:
            return _(u'target 1')
        elif iCmd==self.ACT_TARGET2:
            return _(u'target 2')
        else:
            return u''
    def SetCmd(self,iCmd):
        try:
            if self.dirComp is None:
                return
            l=self.__getTrData__()
            if len(l)==0:
                return
            self.iAct=0
            self.iCount=max(self.tCount1[0],self.tCount2[0])
            bEqual=self.chkShowEqual.GetValue()
            thd=self.thdCtrl.GetThread()
            thd.Start()
            self.thdCtrl.Start()
            sCmd=self.getCmdTrans(iCmd)
            thd.Post('proc',_(u'set command:')+sCmd+u' ...',None)
            for ti,tup in l:
                try:
                    if tup[1]==4:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s'%(tup[0].getName()),self)
                    elif tup[1]==3:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s'%(tup[0].base),self)
                        pass
                    else:
                        continue
                except:
                    vtLog.vtLngTB(self.GetName())
                thd.DoCallBackWX(self.__setCmd__,thd,
                            self.trlstRes,ti,iCmd,bEqual)
            thd.DoCallBackWX(thd.Post,'proc',_(u'set command:')+sCmd+u' '+_(u'done'),None)
        except:
            vtLog.vtLngTB(self.GetName())
    def __buildApplyCmd__(self,tr,ti,lHier,l):
        try:
            tup=tr.GetPyData(ti)
            if tup[1]==4:
                if tup[3]!=self.ACT_UNDEF:
                    if tup[0].hasSrc1():
                        sFN1=tup[0].file1.fullname
                    else:
                        sFN1=None
                    if tup[0].hasSrc2():
                        sFN2=tup[0].file2.fullname
                    else:
                        sFN2=None
                    l.append((tup[3],sFN1,sFN2,ti))
                    if tup[3] in [self.ACT_CP1TO2,self.ACT_MV1TO2]:
                        self.iCount+=tup[0].getSize()
                    elif tup[3] in [self.ACT_CP2TO1,self.ACT_MV2TO1]:
                        self.iCount+=tup[0].getSize()
                    elif tup[3] in [self.ACT_DEL1]:
                        self.iCount+=1
                    elif tup[3] in [self.ACT_DEL2]:
                        self.iCount+=1
            elif tup[1]==3:
                if lHier is None:
                    lHier=[]
            cid=tr.GetFirstChild(ti)
            while cid[0].IsOk():
                tid=tr.GetPyData(ti)
                if tid is not None:
                    if tid[1] in [3]:
                        if tid[2]==True:
                            sLbl=tr.GetItemText(cid[0])
                            if sLbl=='.':
                                
                                self.__buildApplyCmd__(tr,cid[0],lHier,l)
                            else:
                                self.__buildApplyCmd__(tr,cid[0],lHier+[sLbl],l)
                cid=tr.GetNextChild(ti,cid[1])
        except:
            vtLog.vtLngTB(self.GetName())
    def DoBuildApplyCmd(self):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            ti=self.trlstRes.GetRootItem()
            self.iCount=0
            if ti is None:
                return False
            l=[]
            self.__buildApplyCmd__(self.trlstRes,ti,None,l)
            if self.iCount>0:
                sDN1=self.dirEntrySrc1.base
                sDN2=self.dirEntrySrc2.base
                thd=self.thdCtrl.GetThread()
                thd.Post('proc',_('apply actions ...'),None)
                thd.Do(self.procApplyCmd,sDN1,sDN2,l)
                return True
        except:
            vtLog.vtLngTB(self.GetName())
        return False
    def procCopy(self,sSrcFN,sDstFN,thd):
        thd.__logInfo__('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
        sTmpFN=getCurDateTimeFN(sDstFN)
        dn,fn=os.path.split(sTmpFN)
        try:
            os.makedirs(dn)
        except:
            pass
        try:
            if os.path.exists(sDstFN):
                os.rename(sDstFN,sTmpFN)
        except:
            thd.Post('proc','destination file could not be removed',None)
            thd.__logTB__()
            return -3
        try:
            st=os.stat(sSrcFN)
            fOut=open(sDstFN,'wb')
            fIn=open(sSrcFN,'rb')
            s=os.stat(sSrcFN)
            iSize=s[stat.ST_SIZE]
            iAct=0
            while iAct!=iSize:
                blk=fIn.read(4096)
                i=fIn.tell()
                self.iAct+=i-iAct
                iAct=i
                fOut.write(blk)
                thd.Post('proc',self.iAct,self.iCount)
                vtLog.SetProcBarVal(iAct,iSize)
            fIn.close()
            fOut.close()
        except:
            thd.__logTB__()
            try:
                fIn.close()
            except:
                pass
            try:
                fOut.close()
            except:
                pass
            try:
                if os.path.exists(sDstFN):
                    os.remove(sDstFN)
                if os.path.exists(sTmpFN):
                    os.rename(sTmpFN,sDstFN)
            except:
                thd.__logTB__()
            return -1
        try:
            if os.path.exists(sTmpFN):
                os.remove(sTmpFN)
        except:
            thd.Post('proc','destination file could not be removed',None)
            thd.__logTB__()
            return -2
        try:
            #st=os.stat(sSrcFN)
            setFileTimes(sDstFN,st[stat.ST_CTIME],st[stat.ST_ATIME],st[stat.ST_MTIME])
        except:
            thd.__logTB__()
        return 0
    def procMove(self,sSrcFN,sDstFN,thd):
        thd.__logInfo__('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
        iRet=self.procCopy(sSrcFN,sDstFN,thd)
        if iRet==0:
            try:
                if os.path.exists(sSrcFN):
                    os.remove(sSrcFN)
                    return 0
            except:
                thd.Post('proc','source file could not be removed',None)
                thd.__logTB__()
                return -4
            return -5
        return iRet
    def procDel(self,sSrcFN,thd):
        thd.__logInfo__('sSrcFN:%s'%(sSrcFN))
        self.iAct+=1
        try:
            if os.path.exists(sSrcFN):
                os.remove(sSrcFN)
                return 0
        except:
            thd.Post('proc','source file could not be removed',None)
            thd.__logTB__()
            return -4
        return -5
    def procApplyCmd(self,sDN1,sDN2,l):
        try:
            thd=self.thdCtrl.GetThread()
            for iCmd,sFN1,sFN2,ti in l:
                if thd.Is2Stop():
                    vtLog.SetProcBarVal(0,100)
                    return
                imgIdx=self.dImg['proc']
                vtLog.SetProcBarVal(0,100)
                thd.CallBack(self.updateTreeItemCmdResult,ti,imgIdx,True)
                if sFN1 is not None:
                    sCmd=':'.join([self.getCmdTrans(iCmd),sFN1])
                elif sFN2 is not None:
                    sCmd=':'.join([self.getCmdTrans(iCmd),sFN2])
                else:
                    sCmd=':'.join([self.getCmdTrans(iCmd),''])
                if iCmd==self.ACT_CP1TO2:
                    if sFN2 is None:
                        sFN2=sFN1
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1,' > ',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procCopy('/'.join([sDN1,sFN1]),'/'.join([sDN2,sFN2]),thd)
                elif iCmd==self.ACT_MV1TO2:
                    if sFN2 is None:
                        sFN2=sFN1
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1,' > ',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procMove('/'.join([sDN1,sFN2]),'/'.join([sDN2,sFN2]),thd)
                elif iCmd==self.ACT_CP2TO1:
                    if sFN1 is None:
                        sFN1=sFN2
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2,' > ',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procCopy('/'.join([sDN2,sFN2]),'/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_MV2TO1:
                    if sFN1 is None:
                        sFN1=sFN2
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2,' > ',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procMove('/'.join([sDN2,sFN2]),'/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_DEL1:
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN1])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procDel('/'.join([sDN1,sFN1]),thd)
                elif iCmd==self.ACT_DEL2:
                    sCmd=''.join([self.getCmdTrans(iCmd),':',sFN2])
                    #thd.CallBack(self.pnLog.AddLog,sCmd,'proc')
                    vtLog.PrintMsg(sCmd)
                    iRet=self.procDel('/'.join([sDN2,sFN2]),thd)
                else:
                    iRet=-1
                if iRet<0:
                    thd.CallBack(self.pnLog.AddLog,sCmd,'error')
                    imgIdx=self.dImg['err']
                else:
                    thd.CallBack(self.pnLog.AddLog,sCmd,'def')
                    imgIdx=self.dImg['ok']
                thd.CallBack(self.updateTreeItemCmdResult,ti,imgIdx)
        except:
            thd.__logTB__()
        thd.Post('proc',_('apply actions done.'),None)
        vtLog.SetProcBarVal(0,100)
    def updateTreeItemCmdResult(self,ti,imgIdx,bVisible=False):
        self.trlstRes.SetItemImage(ti,imgIdx,column=0,
                            which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemImage(ti,-1,column=2,
                            which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemImage(ti,-1,column=3,
                            which=wx.TreeItemIcon_Normal)
        tup=self.trlstRes.GetPyData(ti)
        if tup is not None:
            tup[3]=self.ACT_UNDEF
        if bVisible and self.chkSelProc.GetValue():
            self.trlstRes.EnsureVisible(ti)
            self.trlstRes.SelectItem(ti)
    def OnCbCpTo1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_CP2TO1)
    def OnCbCpTo2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_CP1TO2)
    def OnCbMvTo2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_MV1TO2)
    def OnCbMvTo1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_MV2TO1)
    def OnCbDel1Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_DEL1)
    def OnCbDel2Button(self, event):
        event.Skip()
        self.SetCmd(self.ACT_DEL2)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        return _(u'VIDARC Tools BrowseDir'),getAboutDescription(),version

    def OnCbClrSrc1Button(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.tCount1=(0,0,0)
            self.dirComp=None
            self.cbClrSrc1.Enable(False)
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbClrSrc2Button(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc2=None
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.cbClrSrc2.Enable(False)
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbClrCmpButton(self,event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirComp=None
            self.cbClrCmp.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTgTarget1Button(self, event):
        event.Skip()
        try:
            self.tgTargetSrc2.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
        if self.tgTargetSrc1.GetValue():
            self.iTarget=1
            self.SetCmd(self.ACT_TARGET1)
        else:
            self.iTarget=0
    def OnTgTargetSrc2Button(self, event):
        event.Skip()
        try:
            self.tgTargetSrc1.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
        if self.tgTargetSrc2.GetValue():
            self.iTarget=2
            self.SetCmd(self.ACT_TARGET2)
        else:
            self.iTarget=0

    def OnCbRemoveButton(self, event):
        event.Skip()

    def OnCbClrAllButton(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            self.dirEntrySrc1=None
            self.dirEntrySrc2=None
            self.tCount1=(0,0,0)
            self.tCount2=(0,0,0)
            self.dirComp=None
            self.cbClrSrc2.Enable(False)
            self.cbClrSrc1.Enable(False)
            self.pnLog.AddLog(None,'proc')
        except:
            vtLog.vtLngTB(self.GetName())
