#Boa:FramePanel:vNetCfgMainPanel
#----------------------------------------------------------------------------
# Name:         vS7Wcf07MainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071116
# CVS-ID:       $Id: vNetCfgMainPanel.py,v 1.7 2007/11/21 22:43:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import sys,os,os.path,platform
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.InOut.fnUtil import shiftFile
    import vidarc.vTools.vNetCfg.images as imgNetCfg
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.vtThread import vtThread
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgNetCfg.getPluginImage()

[wxID_VNETCFGMAINPANEL, wxID_VNETCFGMAINPANELCBCFGADD, 
 wxID_VNETCFGMAINPANELCBCFGDEL, wxID_VNETCFGMAINPANELCBGEN, 
 wxID_VNETCFGMAINPANELCBSAVE, wxID_VNETCFGMAINPANELCHCCFG, 
 wxID_VNETCFGMAINPANELDBBOUT, wxID_VNETCFGMAINPANELLBLCFG, 
 wxID_VNETCFGMAINPANELLSTINTERFACE, wxID_VNETCFGMAINPANELTXTCFG, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vNetCfgMainPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_bxsFiles_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstInterface, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilesBt, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCfgs, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.EXPAND)
        parent.AddWindow(self.dbbOut, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFiles, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)

    def _init_coll_bxsFilesBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSave, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbGen, 0, border=0, flag=0)

    def _init_coll_bxsCfgs_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCfg, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.chcCfg, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCfgDel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCfgAdd, 0, border=0, flag=0)
        parent.AddWindow(self.txtCfg, 1, border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsCfgs = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFiles = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilesBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsCfgs_Items(self.bxsCfgs)
        self._init_coll_bxsFiles_Items(self.bxsFiles)
        self._init_coll_bxsFilesBt_Items(self.bxsFilesBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VNETCFGMAINPANEL,
              name=u'vNetCfgMainPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(342, 259), style=wx.TAB_TRAVERSAL)
        self.SetAutoLayout(True)

        self.lblCfg = wx.StaticText(id=wxID_VNETCFGMAINPANELLBLCFG,
              label=_(u'configuration'), name=u'lblCfg', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(63, 13), style=0)

        self.chcCfg = wx.Choice(choices=[], id=wxID_VNETCFGMAINPANELCHCCFG,
              name=u'chcCfg', parent=self, pos=wx.Point(67, 0), size=wx.Size(94,
              21), style=0)
        self.chcCfg.Bind(wx.EVT_CHOICE, self.OnChcCfgChoice,
              id=wxID_VNETCFGMAINPANELCHCCFG)

        self.cbCfgDel = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VNETCFGMAINPANELCBCFGDEL, name=u'cbCfgDel', parent=self,
              pos=wx.Point(169, 0), size=wx.Size(31, 30), style=0)
        self.cbCfgDel.Bind(wx.EVT_BUTTON, self.OnCbCfgDelButton,
              id=wxID_VNETCFGMAINPANELCBCFGDEL)

        self.txtCfg = wx.TextCtrl(id=wxID_VNETCFGMAINPANELTXTCFG,
              name=u'txtCfg', parent=self, pos=wx.Point(243, 0),
              size=wx.Size(91, 32), style=0, value=u'')

        self.cbCfgAdd = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VNETCFGMAINPANELCBCFGADD, name=u'cbCfgAdd', parent=self,
              pos=wx.Point(208, 0), size=wx.Size(31, 30), style=0)
        self.cbCfgAdd.Bind(wx.EVT_BUTTON, self.OnCbCfgAddButton,
              id=wxID_VNETCFGMAINPANELCBCFGADD)

        self.dbbOut = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose a directory', id=wxID_VNETCFGMAINPANELDBBOUT,
              labelText=u'Select a directory:', parent=self, pos=wx.Point(0,
              40), size=wx.Size(334, 29), startDirectory='.', style=0)

        self.lstInterface = wx.ListCtrl(id=wxID_VNETCFGMAINPANELLSTINTERFACE,
              name=u'lstInterface', parent=self, pos=wx.Point(0, 69),
              size=wx.Size(299, 159), style=wx.LC_REPORT)

        self.cbSave = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VNETCFGMAINPANELCBSAVE, name=u'cbSave', parent=self,
              pos=wx.Point(303, 69), size=wx.Size(31, 30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VNETCFGMAINPANELCBSAVE)

        self.cbGen = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VNETCFGMAINPANELCBGEN, name=u'cbGen', parent=self,
              pos=wx.Point(303, 107), size=wx.Size(31, 30), style=0)
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VNETCFGMAINPANELCBGEN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vNetCfg')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.lstInterface.InsertColumn(0,_(u'idx'),format=wx.LIST_FORMAT_RIGHT,width=60)
        self.lstInterface.InsertColumn(1,_(u'kind'),format=wx.LIST_FORMAT_LEFT,width=80)
        self.lstInterface.InsertColumn(2,_(u'name'),format=wx.LIST_FORMAT_LEFT,width=140)
        self.lstInterface.InsertColumn(3,_(u'attr'),format=wx.LIST_FORMAT_LEFT,width=280)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        #self.dImg['qst']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        self.lstInterface.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.widLog=vtLog.GetPrintMsgWid(self)
        
        self.thdProc=vtThread(self,bPost=True)
        
        self.xdCfg=vtXmlDom(appl='vNetCfgCfg',audit_trail=False)
        self.bBlock=True
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dCfg:%s'%(vtLog.pformat(self.dCfg)),self)
            self.chcCfg.Clear()
            for i in xrange(20):
                sCfg='cfg%02d'%i
                if sCfg in self.dCfg:
                    dd=self.dCfg[sCfg]
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;dd:%s'%(sCfg,vtLog.pformat(dd)),self)
                    self.chcCfg.Append(dd['name'])
            self.chcCfg.SetSelection(0)
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)

    def OnChcCfgChoice(self, event):
        event.Skip()
        wx.CallAfter(self.__showCfg__)
    def OnCbCfgDelButton(self, event):
        event.Skip()
        try:
            iAct=self.chcCfg.GetSelection()
            if iAct<0:
                return
            iCount=self.chcCfg.GetCount()
            iCount=min(iCount,20)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            for i in xrange(iAct,iCount-1):
                self.dCfg['cfg%02d'%(i)]=self.dCfg['cfg%02d'%(i+1)]
            del self.dCfg['cfg%02d'%(iCount-1)]
            self.chcCfg.Delete(iAct)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            self.chcCfg.SetSelection(0)
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCfgAddButton(self, event):
        event.Skip()
        try:
            iCount=self.chcCfg.GetCount()
            iCount=min(iCount,20)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            for i in xrange(iCount,0,-1):
                self.dCfg['cfg%02d'%(i)]=self.dCfg['cfg%02d'%(i-1)]
            sName=self.txtCfg.GetValue()
            self.chcCfg.Insert(sName,0)
            self.dCfg['cfg00']={'name':sName}
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            self.chcCfg.SetSelection(0)
            self.txtCfg.SetValue('')
            
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def __selCfg__(self):
        try:
            iAct=self.chcCfg.GetSelection()
            if iAct>=0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'iAct:%d,%s'%(iAct,vtLog.pformat(self.dCfg)),self)
                d=self.dCfg['cfg%02d'%(iAct)]
                return iAct,d
        except:
            vtLog.vtLngTB(self.GetName())
        return -1,None
    def __showCfg__(self):
        try:
            iAct,d=self.__selCfg__()
            if iAct<0:
                self.__showInterface__()
                return
            if 'outDN' in d:
                self.dbbOut.SetValue(d['outDN'])
            else:
                sDN=os.getcwd()
                self.dbbOut.SetValue(sDN)
                d['outDN']=sDN
        except:
            vtLog.vtLngTB(self.GetName())
        self.__showInterface__()
    def __showInterface__(self):
        try:
            self.lstInterface.DeleteAllItems()
            
            sFullFN=self.GetFN()
            sDN,sFN=os.path.split(sFullFN)
            if len(sFullFN)<=0:
                return
            if os.path.exists(sFullFN)==False:
                imgId=self.dImg['err']
                idx=self.lstInterface.InsertStringItem(sys.maxint,'',imgId)
                return
            imgId=self.dImg['ok']
            idx=self.lstInterface.InsertStringItem(sys.maxint,'',imgId)
            self.lstInterface.SetStringItem(idx,2,sFN)
            fIn=open(sFullFN)
            bInterfaceSec=False
            iIdx=1
            for s in fIn:
                if s.startswith('# Interface IP Configuration'):
                    bInterfaceSec=True
                elif s.startswith('# End of interface IP configuration'):
                    bInterfaceSec=False
                if bInterfaceSec==True:
                    if s.startswith('set '):
                        iS=s.find('name=')
                        if iS>0:
                            iE=s.find('"',iS+6)
                            if iE>0:
                                #l=s[iE+2:].split(' ')
                                sKind=s[4:iS]
                                sName=s[iS+6:iE]
                                #print sKind,sName,l
                                idx=self.lstInterface.InsertStringItem(sys.maxint,'%03d'%iIdx,-1)
                                self.lstInterface.SetStringItem(idx,1,sKind)
                                self.lstInterface.SetStringItem(idx,2,sName)
                                self.lstInterface.SetStringItem(idx,3,s[iE+2:-1])
                                iIdx+=1
            fIn.close()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbGenButton(self, event):
        sFullFN=self.GetFN()
        self.thdProc.Do(self.__set__,sFullFN)
    def __set__(self, sFullFN):
        try:
            vtLog.PrintMsg(_(u'setting configuration ...'),self.widLog)
            
            sDN,sFN=os.path.split(sFullFN)
            if len(sFullFN)<=0:
                return
            if os.path.exists(sFullFN)==False:
                vtLog.PrintMsg(_(u'configuration %s faulty...')%(sFullFN),self.widLog)
                return
            iProcCount=2
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            sSys=platform.system()
            sNode=platform.node()
            if sSys=='Windows':
                vtLog.SetProcBarVal(1,iProcCount,self.widLog)
                sCmd='netsh -f %s'%sFullFN
                tPipes=os.popen3(sCmd)
                lOut=tPipes[1].readlines()
                lErr=tPipes[2].readlines()
                vtLog.SetProcBarVal(2,iProcCount,self.widLog)
            vtLog.PrintMsg(_(u'configuration set.'),self.widLog)
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetFN(self):
        try:
            iAct,d=self.__selCfg__()
            if iAct<0:
                return ''
            sSys=platform.system()
            sNode=platform.node()
            sDN=self.dbbOut.GetValue()
            d['outDN']=sDN
            sFN=u'_'.join([sNode,sSys,'.'.join([d['name'],'cfg'])])
            sFullFN=os.path.join(sDN,sFN)
            return sFullFN
        except:
            vtLog.vtLngTB(self.GetName())
            return sFullFN
    def OnCbSaveButton(self, event):
        event.Skip()
        sFullFN=self.GetFN()
        self.thdProc.Do(self.__get__,sFullFN)
    def __get__(self, sFullFN):
        try:
            self.lstInterface.DeleteAllItems()
            vtLog.PrintMsg(_(u'generate ...'),self.widLog)
            if len(sFullFN)==0:
                vtLog.vtLngCur(vtLog.ERROR,'no output file set',self)
                vtLog.PrintMsg(_(u'no output filename set.'),self.widLog)
                return
            if sFullFN.lower().endswith('.cfg')==False:
                vtLog.vtLngCur(vtLog.ERROR,'output file %s must be csv'%(sFullFN),self)
                vtLog.PrintMsg(_(u'output filename:%s must be cfg.')%(sFullFN),self.widLog)
                return
            iProcCount=2
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            vtLog.PrintMsg(_(u'get network interface configuration ...'),self.widLog)
            sSys=platform.system()
            if sSys=='Windows':
                vtLog.SetProcBarVal(1,iProcCount,self.widLog)
                sDN,sFN=os.path.split(sFullFN)
                #sFN=u'_'.join([sNode,sSys,sFN])
                if os.path.exists(sDN)==False:
                    os.makedirs(sDN)
                #sFullFN=os.path.join(sDN,sFN)
                shiftFile(sFullFN)
                fOut=open(sFullFN,'w')
                sCmd='netsh -c interface dump'
                tPipes=os.popen3(sCmd)
                lOut=tPipes[1].readlines()
                lErr=tPipes[2].readlines()
                vtLog.PrintMsg(_(u'save file:%s ...')%(sFullFN),self.widLog)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'out:%s'%(';'.join([s.replace('\n','') for s in lOut])),self)
                for s in lOut:
                    fOut.write(s)
                fOut.close()
                vtLog.SetProcBarVal(2,iProcCount,self.widLog)
                vtLog.PrintMsg(_(u'saved file:%s.')%(sFullFN),self.widLog)
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            self.__showInterface__()
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.PrintMsg(_(u'save file:%s faulty.')%(sFullFN),self.widLog)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Network configuration store and restore module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Tools NetCfg'),desc,version
