#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071116
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2007/11/16 19:23:07 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/NetCfg01_16.ico      images.py",
    "-a -u -n Plugin        img/NetCfg01_16.png      images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

