#Boa:FramePanel:vCsvMergerPanel

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton

import os,sys,codecs

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt

    import images as vCsvMergerPanelImages

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

def getPluginImage():
    return vCsvMergerPanelImages.getCsvMergerImage()

[wxID_VCSVMERGERPANEL, wxID_VCSVMERGERPANELCBADD1, wxID_VCSVMERGERPANELCBADD2, 
 wxID_VCSVMERGERPANELCBADDOUT1, wxID_VCSVMERGERPANELCBBUILD1, 
 wxID_VCSVMERGERPANELCBBUILD2, wxID_VCSVMERGERPANELCBCALC1, 
 wxID_VCSVMERGERPANELCBCALC2, wxID_VCSVMERGERPANELCBDEL1, 
 wxID_VCSVMERGERPANELCBDEL2, wxID_VCSVMERGERPANELCBDELOUT, 
 wxID_VCSVMERGERPANELCBDELOUT1, wxID_VCSVMERGERPANELCBDN, 
 wxID_VCSVMERGERPANELCBEVAL1, wxID_VCSVMERGERPANELCBEVAL2, 
 wxID_VCSVMERGERPANELCBREAD, wxID_VCSVMERGERPANELCBRESOUT, 
 wxID_VCSVMERGERPANELCBSAVE, wxID_VCSVMERGERPANELCBSTOP, 
 wxID_VCSVMERGERPANELCBUP, wxID_VCSVMERGERPANELCHCHDR1, 
 wxID_VCSVMERGERPANELCHCHDR2, wxID_VCSVMERGERPANELCHCHDROUT, 
 wxID_VCSVMERGERPANELCLSTKEY1, wxID_VCSVMERGERPANELCLSTKEY2, 
 wxID_VCSVMERGERPANELFBBIN1, wxID_VCSVMERGERPANELFBBIN2, 
 wxID_VCSVMERGERPANELFBBOUTFN, wxID_VCSVMERGERPANELLBLCONV1, 
 wxID_VCSVMERGERPANELLBLCONV2, wxID_VCSVMERGERPANELLBLSEP, 
 wxID_VCSVMERGERPANELLBLSEP2, wxID_VCSVMERGERPANELLBLSEPOUT, 
 wxID_VCSVMERGERPANELLSTDATA1, wxID_VCSVMERGERPANELLSTDATA2, 
 wxID_VCSVMERGERPANELLSTIN1, wxID_VCSVMERGERPANELLSTIN2, 
 wxID_VCSVMERGERPANELLSTOUT, wxID_VCSVMERGERPANELLSTRES1, 
 wxID_VCSVMERGERPANELLSTRES2, wxID_VCSVMERGERPANELLSTRESOUT, 
 wxID_VCSVMERGERPANELNBMERGER, wxID_VCSVMERGERPANELPNINPUT1, 
 wxID_VCSVMERGERPANELPNINPUT2, wxID_VCSVMERGERPANELPNOUT, 
 wxID_VCSVMERGERPANELPNRESOUT, wxID_VCSVMERGERPANELTXTCONV1, 
 wxID_VCSVMERGERPANELTXTCONV2, wxID_VCSVMERGERPANELTXTRES1, 
 wxID_VCSVMERGERPANELTXTRES2, wxID_VCSVMERGERPANELTXTSEP1, 
 wxID_VCSVMERGERPANELTXTSEP2, wxID_VCSVMERGERPANELTXTSEPOUT, 
] = [wx.NewId() for _init_ctrls in range(53)]

class vCsvMergerPanel(wx.Panel):
    VERBOSE=1
    def _init_coll_fgsResOut_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(1)
        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsOut_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(2)

    def _init_coll_fgsInput1_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsResOut_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRes1, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbBuild1, 0, border=4, flag=wx.ALL)
        parent.AddWindow(self.lstRes2, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbBuild2, 0, border=4, flag=wx.ALL)
        parent.AddWindow(self.lstResOut, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbResOut, 0, border=4, flag=wx.ALL)
        parent.AddWindow(self.fbbOutFN, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbSave, 0, border=4, flag=wx.ALL)

    def _init_coll_fgsOut_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsIn, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsOut1Bt, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddSizer(self.fgsOutCfg, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsOut, 0, border=4,
              flag=wx.LEFT | wx.EXPAND | wx.TOP)

    def _init_coll_fgsInput1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbIn1, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsSep1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.clstKey1, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsConv1, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lstData1, 1, border=4, flag=wx.EXPAND | wx.TOP)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsConv2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblConv2, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtConv2, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbCalc2, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.txtRes2, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbEval2, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbAdd2, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDel2, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsIn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstIn1, 1, border=2, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddWindow(self.lstIn2, 1, border=2, flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbMerger, 1, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)

    def _init_coll_bxsOutCfgSep_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSepOut, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtSepOut, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcHdrOut, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSep2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSep2, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtSep2, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcHdr2, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsOutCfg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbRead, 0, border=4, flag=0)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsOut_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbDelOut, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsOutCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstOut, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsOutCfgSep, 0, border=4, flag=wx.EXPAND)

    def _init_coll_bxsConv1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblConv1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtConv1, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbCalc1, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.txtRes1, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbEval1, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbAdd1, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDel1, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsSep1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSep, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtSep1, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcHdr1, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsOut1Bt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDelOut1, 0, border=0, flag=0)
        parent.AddWindow(self.cbAddOut1, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsInput2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbIn2, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsSep2, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.clstKey2, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsConv2, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lstData2, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsInput2_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_nbMerger_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnInput1, select=True,
              text=u'Input 1')
        parent.AddPage(imageId=-1, page=self.pnInput2, select=False,
              text=u'Input 2')
        parent.AddPage(imageId=-1, page=self.pnOut, select=False,
              text=u'Output')
        parent.AddPage(imageId=-1, page=self.pnResOut, select=False,
              text=u'Result')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsInput1 = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsSep1 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsInput2 = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsSep2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConv1 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConv2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsResOut = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.fgsOut = wx.FlexGridSizer(cols=4, hgap=0, rows=1, vgap=0)

        self.bxsIn = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsOut1Bt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsOut = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsOutCfgSep = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsOutCfg = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self._init_coll_fgsInput1_Items(self.fgsInput1)
        self._init_coll_fgsInput1_Growables(self.fgsInput1)
        self._init_coll_bxsSep1_Items(self.bxsSep1)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsInput2_Items(self.fgsInput2)
        self._init_coll_fgsInput2_Growables(self.fgsInput2)
        self._init_coll_bxsSep2_Items(self.bxsSep2)
        self._init_coll_bxsConv1_Items(self.bxsConv1)
        self._init_coll_bxsConv2_Items(self.bxsConv2)
        self._init_coll_fgsResOut_Items(self.fgsResOut)
        self._init_coll_fgsResOut_Growables(self.fgsResOut)
        self._init_coll_fgsOut_Items(self.fgsOut)
        self._init_coll_fgsOut_Growables(self.fgsOut)
        self._init_coll_bxsIn_Items(self.bxsIn)
        self._init_coll_bxsOut1Bt_Items(self.bxsOut1Bt)
        self._init_coll_bxsOut_Items(self.bxsOut)
        self._init_coll_bxsOutCfgSep_Items(self.bxsOutCfgSep)
        self._init_coll_fgsOutCfg_Items(self.fgsOutCfg)
        self._init_coll_fgsOutCfg_Growables(self.fgsOutCfg)

        self.SetSizer(self.fgsData)
        self.pnResOut.SetSizer(self.fgsResOut)
        self.pnInput1.SetSizer(self.fgsInput1)
        self.pnInput2.SetSizer(self.fgsInput2)
        self.pnOut.SetSizer(self.fgsOut)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCSVMERGERPANEL,
              name=u'vCsvMergerPanel', parent=prnt, pos=wx.Point(132, 132),
              size=wx.Size(332, 251), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(324, 224))

        self.nbMerger = wx.Notebook(id=wxID_VCSVMERGERPANELNBMERGER,
              name=u'nbMerger', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(277, 216), style=0)

        self.pnInput1 = wx.Panel(id=wxID_VCSVMERGERPANELPNINPUT1,
              name=u'pnInput1', parent=self.nbMerger, pos=wx.Point(0, 0),
              size=wx.Size(269, 190), style=wx.TAB_TRAVERSAL)

        self.fbbIn1 = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=u'Choose a file', fileMask=u'*.csv',
              id=wxID_VCSVMERGERPANELFBBIN1, labelText=u'Input 1:',
              parent=self.pnInput1, pos=wx.Point(0, 0), size=wx.Size(269, 29),
              startDirectory='.', style=0)
        self.fbbIn1.SetMinSize(wx.Size(-1, -1))

        self.lblSep = wx.StaticText(id=wxID_VCSVMERGERPANELLBLSEP,
              label=u'Separator', name=u'lblSep', parent=self.pnInput1,
              pos=wx.Point(0, 29), size=wx.Size(89, 21), style=0)
        self.lblSep.SetMinSize(wx.Size(-1, -1))

        self.txtSep1 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTSEP1,
              name=u'txtSep1', parent=self.pnInput1, pos=wx.Point(93, 29),
              size=wx.Size(85, 21), style=0, value=u',')
        self.txtSep1.SetMinSize(wx.Size(-1, -1))

        self.cbRead = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBREAD,
              bitmap=vtArt.getBitmap(vtArt.Open), name=u'cbRead', parent=self,
              pos=wx.Point(289, 4), size=wx.Size(31, 30), style=0)
        self.cbRead.Bind(wx.EVT_BUTTON, self.OnCbReadButton,
              id=wxID_VCSVMERGERPANELCBREAD)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBSTOP,
              bitmap=vtArt.getBitmap(vtArt.Stop), name=u'cbStop', parent=self,
              pos=wx.Point(289, 38), size=wx.Size(31, 30), style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VCSVMERGERPANELCBSTOP)

        self.pnInput2 = wx.Panel(id=wxID_VCSVMERGERPANELPNINPUT2,
              name=u'pnInput2', parent=self.nbMerger, pos=wx.Point(0, 0),
              size=wx.Size(269, 190), style=wx.TAB_TRAVERSAL)

        self.pnResOut = wx.Panel(id=wxID_VCSVMERGERPANELPNRESOUT,
              name=u'pnResOut', parent=self.nbMerger, pos=wx.Point(0, 0),
              size=wx.Size(269, 190), style=wx.TAB_TRAVERSAL)

        self.chcHdr1 = wx.CheckBox(id=wxID_VCSVMERGERPANELCHCHDR1,
              label=u'header column', name=u'chcHdr1', parent=self.pnInput1,
              pos=wx.Point(182, 29), size=wx.Size(85, 21), style=0)
        self.chcHdr1.SetValue(False)
        self.chcHdr1.SetMinSize(wx.Size(-1, -1))

        self.clstKey1 = wx.CheckListBox(choices=[],
              id=wxID_VCSVMERGERPANELCLSTKEY1, name=u'clstKey1',
              parent=self.pnInput1, pos=wx.Point(0, 54), size=wx.Size(269, 63),
              style=0)
        self.clstKey1.SetMinSize(wx.Size(-1, -1))
        self.clstKey1.Bind(wx.EVT_LISTBOX, self.OnClstKey1Listbox,
              id=wxID_VCSVMERGERPANELCLSTKEY1)
        self.clstKey1.Bind(wx.EVT_CHECKLISTBOX, self.OnClstKey1Checklistbox,
              id=wxID_VCSVMERGERPANELCLSTKEY1)

        self.lstData1 = wx.ListCtrl(id=wxID_VCSVMERGERPANELLSTDATA1,
              name=u'lstData1', parent=self.pnInput1, pos=wx.Point(0, 159),
              size=wx.Size(269, 31), style=wx.LC_REPORT)
        self.lstData1.SetMinSize(wx.Size(-1, -1))
        self.lstData1.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstData1ListColClick,
              id=wxID_VCSVMERGERPANELLSTDATA1)
        self.lstData1.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstData1ListItemDeselected,
              id=wxID_VCSVMERGERPANELLSTDATA1)
        self.lstData1.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstData1ListItemSelected, id=wxID_VCSVMERGERPANELLSTDATA1)

        self.fbbIn2 = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=u'Choose a file', fileMask=u'*.csv',
              id=wxID_VCSVMERGERPANELFBBIN2, labelText=u'Input 2:',
              parent=self.pnInput2, pos=wx.Point(0, 0), size=wx.Size(269, 29),
              startDirectory='.', style=0)
        self.fbbIn2.SetMinSize(wx.Size(-1, -1))

        self.lblSep2 = wx.StaticText(id=wxID_VCSVMERGERPANELLBLSEP2,
              label=u'Separator', name=u'lblSep2', parent=self.pnInput2,
              pos=wx.Point(0, 29), size=wx.Size(89, 21), style=0)
        self.lblSep2.SetMinSize(wx.Size(-1, -1))

        self.txtSep2 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTSEP2,
              name=u'txtSep2', parent=self.pnInput2, pos=wx.Point(93, 29),
              size=wx.Size(85, 21), style=0, value=u',')
        self.txtSep2.SetMinSize(wx.Size(-1, -1))

        self.chcHdr2 = wx.CheckBox(id=wxID_VCSVMERGERPANELCHCHDR2,
              label=u'header column', name=u'chcHdr2', parent=self.pnInput2,
              pos=wx.Point(182, 29), size=wx.Size(85, 21), style=0)
        self.chcHdr2.SetValue(False)
        self.chcHdr2.SetMinSize(wx.Size(-1, -1))

        self.clstKey2 = wx.CheckListBox(choices=[],
              id=wxID_VCSVMERGERPANELCLSTKEY2, name=u'clstKey2',
              parent=self.pnInput2, pos=wx.Point(0, 54), size=wx.Size(269, 63),
              style=0)
        self.clstKey2.SetMinSize(wx.Size(-1, -1))
        self.clstKey2.Bind(wx.EVT_CHECKLISTBOX, self.OnClstKey2Checklistbox,
              id=wxID_VCSVMERGERPANELCLSTKEY2)
        self.clstKey2.Bind(wx.EVT_LISTBOX, self.OnClstKey2Listbox,
              id=wxID_VCSVMERGERPANELCLSTKEY2)

        self.lstData2 = wx.ListCtrl(id=wxID_VCSVMERGERPANELLSTDATA2,
              name=u'lstData2', parent=self.pnInput2, pos=wx.Point(0, 159),
              size=wx.Size(269, 31), style=wx.LC_REPORT)
        self.lstData2.SetMinSize(wx.Size(-1, -1))
        self.lstData2.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstData2ListColClick,
              id=wxID_VCSVMERGERPANELLSTDATA2)
        self.lstData2.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstData2ListItemDeselected,
              id=wxID_VCSVMERGERPANELLSTDATA2)
        self.lstData2.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstData2ListItemSelected, id=wxID_VCSVMERGERPANELLSTDATA2)

        self.lblConv1 = wx.StaticText(id=wxID_VCSVMERGERPANELLBLCONV1,
              label=u'Convert', name=u'lblConv1', parent=self.pnInput1,
              pos=wx.Point(0, 121), size=wx.Size(43, 30), style=0)
        self.lblConv1.SetMinSize(wx.Size(-1, -1))

        self.txtConv1 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTCONV1,
              name=u'txtConv1', parent=self.pnInput1, pos=wx.Point(47, 121),
              size=wx.Size(35, 30), style=0, value=u'')
        self.txtConv1.SetMinSize(wx.Size(-1, -1))

        self.cbCalc1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBCALC1,
              bitmap=vtArt.getBitmap(vtArt.Calc), name=u'cbCalc1',
              parent=self.pnInput1, pos=wx.Point(90, 121), size=wx.Size(31, 30),
              style=0)
        self.cbCalc1.Bind(wx.EVT_BUTTON, self.OnCbCalc1Button,
              id=wxID_VCSVMERGERPANELCBCALC1)

        self.cbEval1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBEVAL1,
              bitmap=vtArt.getBitmap(vtArt.Browse), name=u'cbEval1',
              parent=self.pnInput1, pos=wx.Point(168, 121), size=wx.Size(31,
              30), style=0)
        self.cbEval1.Bind(wx.EVT_BUTTON, self.OnCbEval1Button,
              id=wxID_VCSVMERGERPANELCBEVAL1)

        self.cbAdd1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBADD1,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd1',
              parent=self.pnInput1, pos=wx.Point(203, 121), size=wx.Size(31,
              30), style=0)
        self.cbAdd1.Bind(wx.EVT_BUTTON, self.OnCbAdd1Button,
              id=wxID_VCSVMERGERPANELCBADD1)

        self.cbDel1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBDEL1,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel1',
              parent=self.pnInput1, pos=wx.Point(238, 121), size=wx.Size(31,
              30), style=0)
        self.cbDel1.Bind(wx.EVT_BUTTON, self.OnCbDel1Button,
              id=wxID_VCSVMERGERPANELCBDEL1)

        self.txtRes1 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTRES1,
              name=u'txtRes1', parent=self.pnInput1, pos=wx.Point(125, 121),
              size=wx.Size(39, 30), style=0, value=u'')
        self.txtRes1.SetMinSize(wx.Size(-1, -1))

        self.lblConv2 = wx.StaticText(id=wxID_VCSVMERGERPANELLBLCONV2,
              label=u'Convert', name=u'lblConv2', parent=self.pnInput2,
              pos=wx.Point(0, 121), size=wx.Size(43, 30), style=0)
        self.lblConv2.SetMinSize(wx.Size(-1, -1))

        self.txtConv2 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTCONV2,
              name=u'txtConv2', parent=self.pnInput2, pos=wx.Point(47, 121),
              size=wx.Size(35, 30), style=0, value=u'')
        self.txtConv2.SetMinSize(wx.Size(-1, -1))

        self.cbCalc2 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBCALC2,
              bitmap=vtArt.getBitmap(vtArt.Calc), name=u'cbCalc2',
              parent=self.pnInput2, pos=wx.Point(90, 121), size=wx.Size(31, 30),
              style=0)
        self.cbCalc2.Bind(wx.EVT_BUTTON, self.OnCbCalc2Button,
              id=wxID_VCSVMERGERPANELCBCALC2)

        self.cbEval2 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBEVAL2,
              bitmap=vtArt.getBitmap(vtArt.Browse), name=u'cbEval2',
              parent=self.pnInput2, pos=wx.Point(168, 121), size=wx.Size(31,
              30), style=0)
        self.cbEval2.Bind(wx.EVT_BUTTON, self.OnCbEval2Button,
              id=wxID_VCSVMERGERPANELCBEVAL2)

        self.cbAdd2 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBADD2,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd2',
              parent=self.pnInput2, pos=wx.Point(203, 121), size=wx.Size(31,
              30), style=0)
        self.cbAdd2.Bind(wx.EVT_BUTTON, self.OnCbAdd2Button,
              id=wxID_VCSVMERGERPANELCBADD2)

        self.cbDel2 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBDEL2,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel2',
              parent=self.pnInput2, pos=wx.Point(238, 121), size=wx.Size(31,
              30), style=0)
        self.cbDel2.Bind(wx.EVT_BUTTON, self.OnCbDel2Button,
              id=wxID_VCSVMERGERPANELCBDEL2)

        self.txtRes2 = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTRES2,
              name=u'txtRes2', parent=self.pnInput2, pos=wx.Point(125, 121),
              size=wx.Size(39, 30), style=0, value=u'')
        self.txtRes2.SetMinSize(wx.Size(-1, -1))

        self.lstRes1 = wx.ListCtrl(id=wxID_VCSVMERGERPANELLSTRES1,
              name=u'lstRes1', parent=self.pnResOut, pos=wx.Point(0, 0),
              size=wx.Size(230, 80), style=wx.LC_REPORT)
        self.lstRes1.SetMinSize(wx.Size(-1, -1))

        self.lstRes2 = wx.ListCtrl(id=wxID_VCSVMERGERPANELLSTRES2,
              name=u'lstRes2', parent=self.pnResOut, pos=wx.Point(0, 84),
              size=wx.Size(230, 80), style=wx.LC_REPORT)
        self.lstRes2.SetMinSize(wx.Size(-1, -1))

        self.lstResOut = wx.ListCtrl(id=wxID_VCSVMERGERPANELLSTRESOUT,
              name=u'lstResOut', parent=self.pnResOut, pos=wx.Point(0, 168),
              size=wx.Size(230, 22), style=wx.LC_REPORT)
        self.lstResOut.SetMinSize(wx.Size(-1, -1))

        self.cbBuild1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBBUILD1,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbBuild1',
              parent=self.pnResOut, pos=wx.Point(234, 4), size=wx.Size(31, 30),
              style=0)
        self.cbBuild1.Bind(wx.EVT_BUTTON, self.OnCbBuild1Button,
              id=wxID_VCSVMERGERPANELCBBUILD1)

        self.cbBuild2 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBBUILD2,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbBuild2',
              parent=self.pnResOut, pos=wx.Point(234, 84), size=wx.Size(31, 30),
              style=0)
        self.cbBuild2.Bind(wx.EVT_BUTTON, self.OnCbBuild2Button,
              id=wxID_VCSVMERGERPANELCBBUILD2)

        self.cbResOut = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBRESOUT,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbResOut',
              parent=self.pnResOut, pos=wx.Point(234, 168), size=wx.Size(31,
              30), style=0)
        self.cbResOut.Bind(wx.EVT_BUTTON, self.OnCbResOutButton,
              id=wxID_VCSVMERGERPANELCBRESOUT)

        self.pnOut = wx.Panel(id=wxID_VCSVMERGERPANELPNOUT, name=u'pnOut',
              parent=self.nbMerger, pos=wx.Point(0, 0), size=wx.Size(269, 190),
              style=wx.TAB_TRAVERSAL)

        self.lstIn1 = wx.ListBox(choices=[], id=wxID_VCSVMERGERPANELLSTIN1,
              name=u'lstIn1', parent=self.pnOut, pos=wx.Point(4, 4),
              size=wx.Size(131, 89), style=wx.LB_MULTIPLE)
        self.lstIn1.SetMinSize(wx.Size(-1, -1))

        self.lstIn2 = wx.ListBox(choices=[], id=wxID_VCSVMERGERPANELLSTIN2,
              name=u'lstIn2', parent=self.pnOut, pos=wx.Point(4, 97),
              size=wx.Size(131, 89), style=wx.LB_MULTIPLE)
        self.lstIn2.SetMinSize(wx.Size(-1, -1))

        self.cbDelOut1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBDELOUT1,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbDelOut1',
              parent=self.pnOut, pos=wx.Point(139, 4), size=wx.Size(31, 30),
              style=0)
        self.cbDelOut1.Bind(wx.EVT_BUTTON, self.OnCbDelOut1Button,
              id=wxID_VCSVMERGERPANELCBDELOUT1)

        self.cbAddOut1 = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBADDOUT1,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbAddOut1',
              parent=self.pnOut, pos=wx.Point(139, 38), size=wx.Size(31, 30),
              style=0)
        self.cbAddOut1.Bind(wx.EVT_BUTTON, self.OnCbAddOut1Button,
              id=wxID_VCSVMERGERPANELCBADDOUT1)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self.pnOut,
              pos=wx.Point(309, 4), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VCSVMERGERPANELCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBDN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDn',
              parent=self.pnOut, pos=wx.Point(309, 38), size=wx.Size(31, 30),
              style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VCSVMERGERPANELCBDN)

        self.lstOut = wx.ListBox(choices=[], id=wxID_VCSVMERGERPANELLSTOUT,
              name=u'lstOut', parent=self.pnOut, pos=wx.Point(174, 0),
              size=wx.Size(95, 127), style=wx.LB_MULTIPLE)
        self.lstOut.SetMinSize(wx.Size(-1, -1))

        self.cbDelOut = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBDELOUT,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelOut',
              parent=self.pnOut, pos=wx.Point(309, 72), size=wx.Size(31, 30),
              style=0)
        self.cbDelOut.Bind(wx.EVT_BUTTON, self.OnCbDelOutButton,
              id=wxID_VCSVMERGERPANELCBDELOUT)

        self.fbbOutFN = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=u'Choose a file', fileMask='*.*',
              id=wxID_VCSVMERGERPANELFBBOUTFN, labelText=u'Output:',
              parent=self.pnResOut, pos=wx.Point(0, 252), size=wx.Size(230, 0),
              startDirectory='.', style=0)
        self.fbbOutFN.SetMinSize(wx.Size(-1, -1))

        self.cbSave = wx.lib.buttons.GenBitmapButton(ID=wxID_VCSVMERGERPANELCBSAVE,
              bitmap=vtArt.getBitmap(vtArt.Save), name=u'cbSave',
              parent=self.pnResOut, pos=wx.Point(234, 252), size=wx.Size(31,
              30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VCSVMERGERPANELCBSAVE)

        self.lblSepOut = wx.StaticText(id=wxID_VCSVMERGERPANELLBLSEPOUT,
              label=u'Separator', name=u'lblSepOut', parent=self.pnOut,
              pos=wx.Point(174, 127), size=wx.Size(95, 21), style=0)
        self.lblSepOut.SetMinSize(wx.Size(-1, -1))

        self.txtSepOut = wx.TextCtrl(id=wxID_VCSVMERGERPANELTXTSEPOUT,
              name=u'txtSepOut', parent=self.pnOut, pos=wx.Point(178, 148),
              size=wx.Size(91, 21), style=0, value=u',')
        self.txtSepOut.SetMinSize(wx.Size(-1, -1))

        self.chcHdrOut = wx.CheckBox(id=wxID_VCSVMERGERPANELCHCHDROUT,
              label=u'header', name=u'chcHdrOut', parent=self.pnOut,
              pos=wx.Point(178, 169), size=wx.Size(91, 21), style=0)
        self.chcHdrOut.SetValue(False)
        self.chcHdrOut.SetMinSize(wx.Size(-1, -1))

        self._init_coll_nbMerger_Pages(self.nbMerger)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.cfgFunc=None
        self.cfgOrigin=None
        self.idxSel1CLst=-1
        self.idxSel1Lst=-1
        self.idxSel2CLst=-1
        self.idxSel2Lst=-1
        self.lKey1=[]
        self.lConv1=[]
        self.lKey2=[]
        self.lConv2=[]
        self.lOut=[]
        self.cbSave.Enable(False)
        
    def SetCfgData(self,func,l,d):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(d)
        self.cfgFunc=func
        self.cfgOrigin=l
        self.fbbIn1.SetValue(d['in01FN'])
        self.txtSep1.SetValue(d['in01Sep'])
        try:
            if int(d['in01Hdr'])>0:
                self.chcHdr1.SetValue(True)
        except:
            pass
        self.lKey1=d['in01keys'].split(',')
        self.lConv1=d['in01conv'].split('|')
        
        self.fbbIn2.SetValue(d['in02FN'])
        self.txtSep2.SetValue(d['in02Sep'])
        try:
            if int(d['in02Hdr'])>0:
                self.chcHdr2.SetValue(True)
        except:
            pass
        self.lKey2=d['in02keys'].split(',')
        self.lConv2=d['in02conv'].split('|')
        
        self.lOut=d['out'].split(',')
        self.fbbOutFN.SetValue(d['outFN'])
        self.txtSepOut.SetValue(d['outSep'])
        try:
            if int(d['outHdr'])>0:
                self.chcHdrOut.SetValue(True)
        except:
            pass
        try:
            self.lstOut.Clear()
            for s in self.lOut:
                self.lstOut.Append(s)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetCfgData(self):
        d={}
        d['in01FN']=self.fbbIn1.GetValue()
        d['in01Sep']=self.txtSep1.GetValue()
        if self.chcHdr1.GetValue():
            d['in01Hdr']='1'
        else:
            d['in01Hdr']='0'
        d['in01keys']=','.join(self.lKey1)
        d['in01conv']='|'.join(self.lConv1)
        
        d['in02FN']=self.fbbIn2.GetValue()
        d['in02Sep']=self.txtSep2.GetValue()
        if self.chcHdr2.GetValue():
            d['in02Hdr']='1'
        else:
            d['in02Hdr']='0'
        d['in02keys']=','.join(self.lKey2)
        d['in02conv']='|'.join(self.lConv2)
        
        d['out']=','.join(self.lOut)
        d['outFN']=self.fbbOutFN.GetValue()
        d['outSep']=self.txtSepOut.GetValue()
        if self.chcHdrOut.GetValue():
            d['outHdr']='1'
        else:
            d['outHdr']='0'
        
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)

    def __readCsv__(self,fn,sep=',',trunc=-2,lConv=None):
        lRes=[]
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;start'%(fn),self)
            bDbg=False
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    bDbg=True
            inf=codecs.open(fn,encoding='ISO-8859-1')
            lines=inf.readlines()
            inf.close()
            def convA(a):
                try:
                    if a[0]=='"' and a[-1]=='"':
                        return a[1:-1].strip()
                except:
                    pass
                return a
            for l in lines:
                def convB(a,b):
                    try:
                        r=[a]
                        for c in b:
                            r.append(eval(c))
                        return r[-1]
                    except:
                        return a
                lst=map(convA,l[:trunc].split(sep))
                if lConv is not None:
                    try:
                        lstConv=map(convB,lst,lConv)
                        lst=lstConv
                    except:
                        vtLog.vtLngTB(self.GetName())
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%r;%s'%(l,vtLog.pformat(lst)),self)
                lRes.append(lst)
        except:
            vtLog.vtLngTB(self.GetName())
        vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;done;count:%d'%(fn,len(lRes)),self)
        return lRes
    def __showCsv__(self,lRes,lst,clst,lKey,lResHdr,hdr=True):
        lst.ClearAll()
        clst.Clear()
        lst.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT, heading=u'Row',
                    width=60)
        if hdr:
            col=1
            for s in lRes[0]:
                clst.Append(s)
                try:
                    i=lKey.index(s)
                    clst.Check(col-1)
                except:
                    pass
                lResHdr.Append(s)
                lst.InsertColumn(col=col, format=wx.LIST_FORMAT_LEFT,
                        heading=s, width=-1)
                col+=1
            iAct=1
        else:
            for i in range(len(lRes[0])):
                s='col_%02d'%i
                clst.Append(s)
                lst.InsertColumn(col=i+1, format=wx.LIST_FORMAT_LEFT,
                        heading=s, width=-1)
            iAct=0
        iLen=len(lRes)
        while iAct<iLen:
            try:
                idx=lst.InsertStringItem(sys.maxint,'%04d'%iAct)
                iCol=1
                for s in lRes[iAct]:
                    lst.SetStringItem(idx,iCol,s)#str(s))
                    iCol+=1
            except:
                vtLog.vtLngTB(self.GetName())
            iAct+=1
    def __convConv__(self,lConv):
        l=[]
        for s in lConv:
            strs=s.split('$')
            l.append(strs)
            #for ss in strs:
            #    j=-1
            #    i=ss.find('$',j+1)
            #    while i>=0:
            #        j=ss.find('$',i+1)
        return l
    def OnCbReadButton(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            self.lstRes1.ClearAll()
            self.lstRes2.ClearAll()
            self.lstResOut.ClearAll()
            self.lstIn1.Clear()
            self.lstIn2.Clear()
            self.cbSave.Enable(False)
            self.dRes1=None
            self.dRes2=None
            #self.lstOut.Clear()
            self.lRes1=self.__readCsv__(self.fbbIn1.GetValue(),self.txtSep1.GetValue(),
                            lConv=self.__convConv__(self.lConv1))
            self.__showCsv__(self.lRes1,self.lstData1,self.clstKey1,self.lKey1,self.lstIn1,
                            self.chcHdr1.GetValue())
            
            self.lRes2=self.__readCsv__(self.fbbIn2.GetValue(),self.txtSep2.GetValue(),
                            lConv=self.__convConv__(self.lConv2))
            self.__showCsv__(self.lRes2,self.lstData2,self.clstKey2,self.lKey2,self.lstIn2,
                            self.chcHdr2.GetValue())
            #lRes02=self.__readCsv__(self.fbbIn02.GetValue(),self.txtSep2.GetValue())
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbStopButton(self, event):
        event.Skip()

    def OnCbSaveButton(self, event):
        try:
            fn=self.fbbOutFN.GetValue()
            vtLog.vtLngCurWX(vtLog.INFO,'fn:%s'%(fn),self)
            if len(fn)>0:
                self.GetCfgData()
                sSep=self.txtSepOut.GetValue()
                f=codecs.open(fn,'w',encoding='ISO-8859-1')
                if self.chcHdrOut.GetValue():
                    f.write(sSep.join(self.lResHdr)+os.linesep)
                keys=self.dResOut.keys()
                keys.sort()
                for k in keys:
                    f.write(sSep.join(self.dResOut[k])+os.linesep)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbCalc1Button(self, event):
        if self.idxSel1CLst>=0 and self.idxSel1Lst>=0:
            m=self.lstData1.GetItem(self.idxSel1Lst,self.idxSel1CLst+1)
            s=m.m_text
            def convB(a,b):
                try:
                    r=[a]
                    for c in b:
                        r.append(eval(c))
                    return r[-1]
                except:
                    return a
            try:
                i=convB(s,self.__convConv__(self.lConv1)[self.idxSel1CLst])
                self.txtRes1.SetValue(str(i))
            except:
                self.txtRes1.SetValue('error')
        event.Skip()

    def OnCbEval1Button(self, event):
        event.Skip()
    def __addConv__(self,l,i,s):
        iLen=len(l)
        j=iLen
        while j<=i:
            l.append('')
            j+=1
        l[i]=s
        return l
    def OnCbAdd1Button(self, event):
        if self.idxSel1CLst>=0:
            sConv=self.txtConv1.GetValue()
            self.lConv1=self.__addConv__(self.lConv1,self.idxSel1CLst,sConv)
            vtLog.CallStack('')
            vtLog.pprint(self.lConv1)
            self.GetCfgData()
        event.Skip()

    def OnCbDel1Button(self, event):
        if self.idxSel1CLst>=0:
            try:
                self.lConv1[self.idxSel1CLst]=''
            except:
                pass
            self.GetCfgData()
        event.Skip()

    def OnClstKey1Listbox(self, event):
        self.idxSel1CLst=event.GetSelection()
        if self.idxSel1CLst>=0:
            self.txtConv1.SetValue(self.lConv1[self.idxSel1CLst])
        else:
            self.txtConv1.SetValue('')
        event.Skip()

    def OnLstData1ListColClick(self, event):
        self.idxSel1Lst=-1
        event.Skip()

    def OnLstData1ListItemDeselected(self, event):
        self.idxSel1Lst=-1
        event.Skip()

    def OnLstData1ListItemSelected(self, event):
        self.idxSel1Lst=event.GetIndex()
        event.Skip()

    def OnClstKey1Checklistbox(self, event):
        i=event.GetSelection()
        j=self.clstKey1.IsChecked(i)
        try:
            s=self.clstKey1.GetString(i)
            if j:
                try:
                    self.lKey1.index(s)
                except:
                    self.lKey1.append(s)
                    self.lKey1.sort()
            else:
                try:
                    self.lKey1.remove(s)
                except:
                    pass
        except:
            pass
        event.Skip()

    def OnCbCalc2Button(self, event):
        if self.idxSel2CLst>=0 and self.idxSel2Lst>=0:
            m=self.lstData2.GetItem(self.idxSel2Lst,self.idxSel2CLst+1)
            s=m.m_text
            def convB(a,b):
                try:
                    r=[a]
                    for c in b:
                        r.append(eval(c))
                    return r[-1]
                except:
                    return a
            try:
                i=convB(s,self.__convConv__(self.lConv2)[self.idxSel2CLst])
                self.txtRes2.SetValue(str(i))
            except:
                self.txtRes2.SetValue('error')
        event.Skip()

    def OnCbEval2Button(self, event):
        event.Skip()

    def OnCbAdd2Button(self, event):
        if self.idxSel2CLst>=0:
            sConv=self.txtConv2.GetValue()
            self.lConv2=self.__addConv__(self.lConv2,self.idxSel2CLst,sConv)
            vtLog.CallStack('')
            vtLog.pprint(self.lConv2)
            self.GetCfgData()
        event.Skip()

    def OnCbDel2Button(self, event):
        if self.idxSel2CLst>=0:
            try:
                self.lConv2[self.idxSel2CLst]=''
            except:
                pass
            self.GetCfgData()
        event.Skip()
        
    def OnClstKey2Checklistbox(self, event):
        i=event.GetSelection()
        j=self.clstKey2.IsChecked(i)
        try:
            s=self.clstKey2.GetString(i)
            if j:
                try:
                    self.lKey2.index(s)
                except:
                    self.lKey2.append(s)
                    self.lKey2.sort()
            else:
                try:
                    self.lKey2.remove(s)
                except:
                    pass
        except:
            pass
        event.Skip()

    def OnClstKey2Listbox(self, event):
        self.idxSel2CLst=event.GetSelection()
        if self.idxSel2CLst>=0:
            self.txtConv2.SetValue(self.lConv2[self.idxSel2CLst])
        else:
            self.txtConv2.SetValue('')
        event.Skip()

    def OnLstData2ListColClick(self, event):
        self.idxSel2Lst=-1
        event.Skip()

    def OnLstData2ListItemDeselected(self, event):
        self.idxSel2Lst=-1
        event.Skip()

    def OnLstData2ListItemSelected(self, event):
        self.idxSel2Lst=event.GetIndex()
        event.Skip()
        
    def OnCbBuild1Button(self, event):
        try:
            bDbg=False
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    bDbg=True
            self.lstRes1.ClearAll()
            self.dRes1={}
            lOut=[]
            self.lstRes1.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT, heading=u'Key',
                        width=60)
            col=1
            for k in self.lOut:
                if k[:2]=='1:':
                    i=self.clstKey1.FindString(k[2:])
                    if i>=0:
                        lOut.append(i)
                        self.lstRes1.InsertColumn(col=col, format=wx.LIST_FORMAT_LEFT,
                                heading=k[2:], width=-1)
                        col+=1
            lK=[]
            for k in self.lKey1:
                i=self.clstKey1.FindString(k)
                if i>=0:
                    lK.append(i)
            if self.chcHdr1.GetValue():
                iStart=1
            else:
                iStart=0
            iAct=iStart
            for l in self.lRes1[iStart:]:
                k=''
                for idx in lK:
                    k=k+str(l[idx])+','
                lRes=[iAct]
                for idx in lOut:
                    val=l[idx]
                    try:
                        sVal=str(val)
                    except:
                        sVal=val
                    lRes.append(sVal)
                self.dRes1[k]=lRes
                iAct+=1
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'data:%r; key:%r; res:%r'%(l,k,lRes),self)
            keys=self.dRes1.keys()
            keys.sort()
            for k in keys:
                idx=self.lstRes1.InsertStringItem(sys.maxint,k)
                col=1
                for val in self.dRes1[k][1:]:
                    self.lstRes1.SetStringItem(idx,col,val)
                    col+=1
        except:
            vtLog.vtLngTB(self.GetName())
        if event is not None:
            event.Skip()

    def OnCbBuild2Button(self, event):
        try:
            bDbg=False
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    bDbg=True
            self.lstRes2.ClearAll()
            self.dRes2={}
            lOut=[]
            self.lstRes2.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT, heading=u'Key',
                        width=60)
            col=1
            for k in self.lOut:
                if k[:2]=='2:':
                    i=self.clstKey2.FindString(k[2:])
                    if i>=0:
                        lOut.append(i)
                        self.lstRes2.InsertColumn(col=col, format=wx.LIST_FORMAT_LEFT,
                                heading=k[2:], width=-1)
                        col+=1
            lK=[]
            for k in self.lKey2:
                i=self.clstKey2.FindString(k)
                if i>=0:
                    lK.append(i)
            if self.chcHdr2.GetValue():
                iStart=1
            else:
                iStart=0
            iAct=iStart
            for l in self.lRes2[iStart:]:
                k=''
                for idx in lK:
                    k=k+str(l[idx])+','
                lRes=[iAct]
                for idx in lOut:
                    val=l[idx]
                    try:
                        sVal=str(val)
                    except:
                        sVal=val
                    lRes.append(sVal)
                self.dRes2[k]=lRes
                iAct+=1
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'data:%r; key:%r; res:%r'%(l,k,lRes),self)
            keys=self.dRes2.keys()
            keys.sort()
            for k in keys:
                idx=self.lstRes2.InsertStringItem(sys.maxint,k)
                col=1
                for val in self.dRes2[k][1:]:
                    self.lstRes2.SetStringItem(idx,col,val)
                    col+=1
        except:
            vtLog.vtLngTB(self.GetName())
        if event is not None:
            event.Skip()

    def OnCbResOutButton(self, event):
        if self.dRes1 is None:
            self.OnCbBuild1Button(None)
        if self.dRes2 is None:
            self.OnCbBuild2Button(None)
        
        try:
            self.lstResOut.ClearAll()
            self.dResOut={}
            self.lResHdr=[]
            self.lstResOut.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT, heading=u'Key',
                        width=60)
            lOut=[]
            col=1
            for k in self.lOut:
                iSrc,sKey=k.split(':')
                if iSrc=='1':
                    i=self.clstKey1.FindString(sKey)
                    if i>=0:
                        lOut.append((iSrc,i))
                        self.lResHdr.append(sKey)
                        self.lstResOut.InsertColumn(col=col, format=wx.LIST_FORMAT_LEFT,
                                heading=sKey, width=-1)
                        col+=1
                elif iSrc=='2':
                    i=self.clstKey2.FindString(sKey)
                    if i>=0:
                        lOut.append((iSrc,i))
                        self.lResHdr.append(sKey)
                        self.lstResOut.InsertColumn(col=col, format=wx.LIST_FORMAT_LEFT,
                                heading=sKey, width=-1)
                        col+=1
            
            keys1=self.dRes1.keys()
            keys2=self.dRes2.keys()
            def buildRes(i,j):
                l=[]
                for iSrc,idx in lOut:
                    if iSrc=='1':
                        if i<0:
                            l.append('')
                        else:
                            val=self.lRes1[i][idx]
                            try:
                                sVal=str(val)
                            except:
                                sVal=val
                            l.append(sVal)
                    elif iSrc=='2':
                        if j<0:
                            l.append('')
                        else:
                            val=self.lRes2[j][idx]
                            try:
                                sVal=str(val)
                            except:
                                sVal=val
                            l.append(sVal)
                return l
            for k in keys1:
                try:
                    i=keys2.index(k)
                    l=buildRes(self.dRes1[k][0],self.dRes2[k][0])
                    keys2.remove(k)
                except:
                    l=buildRes(self.dRes1[k][0],-1)
                self.dResOut[k]=l
            for k in keys2:
                l=buildRes(-1,self.dRes2[k][0])
                self.dResOut[k]=l
            keys=self.dResOut.keys()
            keys.sort()
            for k in keys:
                idx=self.lstResOut.InsertStringItem(sys.maxint,k)
                col=1
                for val in self.dResOut[k]:
                    self.lstResOut.SetStringItem(idx,col,val)
                    col+=1
            self.cbSave.Enable(True)
        except:
            vtLog.vtLngTB(self.GetName())
        if event is not None:
            event.Skip()

    def OnCbDelOut1Button(self, event):
        event.Skip()
    def __calcOut__(self):
        self.lOut=[]
        iLen=self.lstOut.GetCount()
        for i in range(iLen):
            s=self.lstOut.GetString(i)
            self.lOut.append(s)
        
    def OnCbAddOut1Button(self, event):
        try:
            l=self.lstIn1.GetSelections()
            for i in l:
                s=self.lstIn1.GetString(i)
                self.lstOut.Append('%d:%s'%(1,s))
            l=self.lstIn2.GetSelections()
            for i in l:
                s=self.lstIn2.GetString(i)
                self.lstOut.Append('%d:%s'%(2,s))
            self.__calcOut__()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbUpButton(self, event):
        self.__calcOut__()
        l=self.lstOut.GetSelections()
        for i in l:
            if i>0:
                self.lOut=self.lOut[:i-1]+[self.lOut[i],self.lOut[i-1]]+self.lOut[i+1:]
        self.lstOut.Clear()
        for s in self.lOut:
            self.lstOut.Append(s)
        for i in l:
            self.lstOut.SetSelection(i-1)
        self.GetCfgData()
        event.Skip()

    def OnCbDnButton(self, event):
        self.__calcOut__()
        l=self.lstOut.GetSelections()
        iLen=len(self.lOut)
        for i in l:
            if i<iLen:
                self.lOut=self.lOut[:i]+[self.lOut[i+1],self.lOut[i]]+self.lOut[i+2:]
        self.lstOut.Clear()
        for s in self.lOut:
            self.lstOut.Append(s)
        for i in l:
            self.lstOut.SetSelection(i+1)
        self.GetCfgData()
        event.Skip()

    def OnCbDelOutButton(self, event):
        try:
            l=self.lstOut.GetSelections()
            for i in xrange(len(l)-1,-1,-1):
                self.lstOut.Delete(l[i])
            
            self.__calcOut__()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()


