#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n CsvMerger img/CSV_Merger01_16.png images.py",
    #"-a -u -n ToDay img/ToDay01_16.png images.py",
    #"-a -u -n Store img/Input01_16.png images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

