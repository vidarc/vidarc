#----------------------------------------------------------------------------
# Name:         vReplFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20131112
# CVS-ID:       $Id: vReplFile.py,v 1.1 2013/11/12 15:17:15 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os,sys,codecs
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)


class vReplFile(vpc.vcOrg):
    def __init__(self,sOrigin=None,sSuffix=None,iVerbose=-1):
        vpc.vcOrg.__init__(self,sOrigin or 'vReplFile',sSuffix or '')
        self.SetVerbose(iVerbose)
    def repl(self,sInFN,sOtFN,lRepl,encoding='UTF-8'):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            #inf=codecs.open(fn,encoding='ISO-8859-1')
            #s=fIn.readlines()
            fIn=codecs.open(sInFN,encoding=encoding)
            s=fIn.read()
            fIn.close()
            for sOld,sNew in lRepl:
                s=s.replace(sOld,sNew)
            if sOtFN is None:
                fOt=codecs.open(sInFN,'w',encoding=encoding)
            else:
                fOt=codecs.open(sOtFN,'w',encoding=encoding)
            s=fOt.write(s)
            fOt.close()
            return 1
        except:
            self.__logTB__()
        return 0
