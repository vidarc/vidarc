#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20131112
# CVS-ID:       $Id: __init__.py,v 1.1 2013/11/12 15:17:15 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#PLUGABLE='vCsvMergerPanel'
