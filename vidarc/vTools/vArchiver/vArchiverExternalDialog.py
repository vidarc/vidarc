#Boa:Dialog:vArchiverExternalDialog
#----------------------------------------------------------------------------
# Name:         vArchiverExternalDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20090223
# CVS-ID:       $Id: vArchiverExternalDialog.py,v 1.1 2009/03/15 18:51:18 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vTools.vArchiver.vArchiverExternalPanel import vArchiverExternalPanel

def create(parent):
    return vArchiverExternalDialog(parent)

[wxID_VARCHIVEREXTERNALDIALOG, wxID_VARCHIVEREXTERNALDIALOGCBAPPLY, 
 wxID_VARCHIVEREXTERNALDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vArchiverExternalDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VARCHIVEREXTERNALDIALOG,
              name=u'vArchiverExternalDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(400, 420),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vArchiverExternal Dialog')
        self.SetClientSize(wx.Size(392, 393))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VARCHIVEREXTERNALDIALOGCBAPPLY, label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VARCHIVEREXTERNALDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VARCHIVEREXTERNALDIALOGCBCANCEL, label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VARCHIVEREXTERNALDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vArchiver')
        self._init_ctrls(parent)
        try:
            self.doc=None
            self.node=None
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vArchiverExternalPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                            style=0,name=u'pnArchiverExternal')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetCmd(self):
        return self.pn.GetCmd()
    def GetArg(self):
        return self.pn.GetArg()
    def SetCmd(self,sVal):
        self.pn.SetCmd(sVal)
    def SetArg(self,sVal):
        self.pn.SetArg(sVal)
