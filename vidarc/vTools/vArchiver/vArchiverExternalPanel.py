#Boa:FramePanel:vArchiverExternalPanel
#----------------------------------------------------------------------------
# Name:         vArchiverExternalPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20090223
# CVS-ID:       $Id: vArchiverExternalPanel.py,v 1.1 2009/03/15 18:51:18 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VARCHIVEREXTERNALPANEL, wxID_VARCHIVEREXTERNALPANELFBBEXTERNAL, 
 wxID_VARCHIVEREXTERNALPANELLBLARCHIVER, wxID_VARCHIVEREXTERNALPANELLBLARGS, 
 wxID_VARCHIVEREXTERNALPANELTXTARGS, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vArchiverExternalPanel(wx.Panel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArchiver, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.fbbExternal, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblArgs, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtArgs, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=4, rows=4, vgap=4)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VARCHIVEREXTERNALPANEL,
              name=u'vArchiverExternalPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.fbbExternal = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=u'Choose a file', fileMask='*.*',
              id=wxID_VARCHIVEREXTERNALPANELFBBEXTERNAL,
              labelText='', parent=self,
              pos=wx.Point(61, 0), size=wx.Size(243, 29), startDirectory='.',
              style=0)

        self.lblArchiver = wx.StaticText(id=wxID_VARCHIVEREXTERNALPANELLBLARCHIVER,
              label=_(u'archiver:'), name=u'lblArchiver', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(57, 29), style=0)

        self.lblArgs = wx.StaticText(id=wxID_VARCHIVEREXTERNALPANELLBLARGS,
              label=_(u'arguments:'), name=u'lblArgs', parent=self,
              pos=wx.Point(0, 33), size=wx.Size(57, 21), style=0)

        self.txtArgs = wx.TextCtrl(id=wxID_VARCHIVEREXTERNALPANELTXTARGS,
              name=u'txtArgs', parent=self, pos=wx.Point(61, 33),
              size=wx.Size(243, 21), style=0, value=u'')
        self.txtArgs.SetToolTipString(_(u'%f ... filename\n%d .. directory'))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vArchiver')
        self._init_ctrls(parent, id)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        
        self.ClrBlockDelayed()
    def GetCmd(self):
        return self.fbbExternal.GetValue()
    def GetArg(self):
        return self.txtArgs.GetValue()
    def SetCmd(self,sVal):
        self.fbbExternal.SetValue(sVal)
    def SetArg(self,sVal):
        self.txtArgs.SetValue(sVal)
