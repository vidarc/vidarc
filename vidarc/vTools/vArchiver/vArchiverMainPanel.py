#Boa:FramePanel:vArchiverMainPanel
#----------------------------------------------------------------------------
# Name:         vArchiverMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090223
# CVS-ID:       $Id: vArchiverMainPanel.py,v 1.2 2009/03/30 19:50:36 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmThreadCtrl
import vidarc.tool.misc.vtmListCtrl
import wx.lib.filebrowsebutton
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog

try:
    import sys,os,os.path
    from os.path import join, getsize
    import types
    import platform
    import tarfile
    import zipfile
    
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.InOut.fnUtil import shiftFile
    import vidarc.vTools.vArchiver.images as imgArchiver
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.vtThread import vtThreadWX
    from vidarc.tool.vtProcess import vtProcess
    from vidarc.vTools.vArchiver.vArchiverExternalDialog import vArchiverExternalDialog
except:
    vtLog.vtLngTB('import')

def getAboutDescription():
    return _(u"""vArchiver module.

This module perform frequent archive jobs.
Filenames are automatically generated containing date, running number 
and hostname.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")

def getPluginImage():
    return imgArchiver.getPluginImage()

[wxID_VARCHIVERMAINPANEL, wxID_VARCHIVERMAINPANELCBADD, 
 wxID_VARCHIVERMAINPANELCBCFGADD, wxID_VARCHIVERMAINPANELCBCFGDEL, 
 wxID_VARCHIVERMAINPANELCBINPUTAPPLY, wxID_VARCHIVERMAINPANELCBINPUTDEL, 
 wxID_VARCHIVERMAINPANELCHCCFG, wxID_VARCHIVERMAINPANELDBBIN, 
 wxID_VARCHIVERMAINPANELDBBOUT, wxID_VARCHIVERMAINPANELLBLCFG, 
 wxID_VARCHIVERMAINPANELLSTLOG, wxID_VARCHIVERMAINPANELTGZIP, 
 wxID_VARCHIVERMAINPANELTHDCTRL, wxID_VARCHIVERMAINPANELTXTCFG, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vArchiverMainPanel(wx.Panel):
    def _init_coll_bxsFiles_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstLog, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilesBt, 0, border=0, flag=0)

    def _init_coll_bxsOutput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbOut, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.tgZip, 0, border=0, flag=0)

    def _init_coll_bxsFilesBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbInputApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbInputDel, 0, border=0, flag=0)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCfgs, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsOutput, 0, border=9, flag=wx.EXPAND | wx.TOP)
        parent.AddSizer(self.bxsInput, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsFiles, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddWindow(self.thdCtrl, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsInput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbIn, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)

    def _init_coll_bxsCfgs_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCfg, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.chcCfg, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCfgDel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCfgAdd, 0, border=0, flag=0)
        parent.AddWindow(self.txtCfg, 1, border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsCfgs = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFiles = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilesBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsOutput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsCfgs_Items(self.bxsCfgs)
        self._init_coll_bxsFiles_Items(self.bxsFiles)
        self._init_coll_bxsFilesBt_Items(self.bxsFilesBt)
        self._init_coll_bxsInput_Items(self.bxsInput)
        self._init_coll_bxsOutput_Items(self.bxsOutput)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VARCHIVERMAINPANEL,
              name=u'vArchiverMainPanel', parent=prnt, pos=wx.Point(4, 5),
              size=wx.Size(286, 231), style=wx.TAB_TRAVERSAL)
        self.SetAutoLayout(True)
        self.SetClientSize(wx.Size(278, 204))

        self.lblCfg = wx.StaticText(id=wxID_VARCHIVERMAINPANELLBLCFG,
              label=_(u'configuration'), name=u'lblCfg', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(63, 13), style=0)

        self.chcCfg = wx.Choice(choices=[], id=wxID_VARCHIVERMAINPANELCHCCFG,
              name=u'chcCfg', parent=self, pos=wx.Point(67, 0), size=wx.Size(66,
              21), style=0)
        self.chcCfg.Bind(wx.EVT_CHOICE, self.OnChcCfgChoice,
              id=wxID_VARCHIVERMAINPANELCHCCFG)

        self.cbCfgDel = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VARCHIVERMAINPANELCBCFGDEL, name=u'cbCfgDel', parent=self,
              pos=wx.Point(141, 0), size=wx.Size(31, 30), style=0)
        self.cbCfgDel.Bind(wx.EVT_BUTTON, self.OnCbCfgDelButton,
              id=wxID_VARCHIVERMAINPANELCBCFGDEL)

        self.txtCfg = wx.TextCtrl(id=wxID_VARCHIVERMAINPANELTXTCFG,
              name=u'txtCfg', parent=self, pos=wx.Point(215, 0),
              size=wx.Size(62, 32), style=0, value=u'')

        self.cbCfgAdd = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VARCHIVERMAINPANELCBCFGADD, name=u'cbCfgAdd', parent=self,
              pos=wx.Point(180, 0), size=wx.Size(31, 30), style=0)
        self.cbCfgAdd.Bind(wx.EVT_BUTTON, self.OnCbCfgAddButton,
              id=wxID_VARCHIVERMAINPANELCBCFGADD)

        self.dbbOut = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose a directory',
              id=wxID_VARCHIVERMAINPANELDBBOUT, labelText=_(u'ouput:'),
              parent=self, pos=wx.Point(0, 41), size=wx.Size(243, 30),
              startDirectory='.', style=0)

        self.tgZip = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Zip),
              id=wxID_VARCHIVERMAINPANELTGZIP, name=u'tgZip', parent=self,
              pos=wx.Point(247, 41), size=wx.Size(31, 30), style=0)
        self.tgZip.Bind(wx.EVT_BUTTON, self.OnTgZipButton,
              id=wxID_VARCHIVERMAINPANELTGZIP)

        self.dbbIn = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose a directory',
              id=wxID_VARCHIVERMAINPANELDBBIN, labelText=_(u'input:'),
              parent=self, pos=wx.Point(0, 71), size=wx.Size(243, 30),
              startDirectory='.', style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VARCHIVERMAINPANELCBADD, name=u'cbAdd', parent=self,
              pos=wx.Point(247, 71), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VARCHIVERMAINPANELCBADD)

        self.lstLog = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VARCHIVERMAINPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 105),
              size=wx.Size(243, 68), style=wx.LC_REPORT)

        self.cbInputApply = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VARCHIVERMAINPANELCBINPUTAPPLY, name=u'cbInputApply',
              parent=self, pos=wx.Point(247, 105), size=wx.Size(31, 30),
              style=0)
        self.cbInputApply.Bind(wx.EVT_BUTTON, self.OnCbInputApplyButton,
              id=wxID_VARCHIVERMAINPANELCBINPUTAPPLY)

        self.cbInputDel = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VARCHIVERMAINPANELCBINPUTDEL, name=u'cbInputDel',
              parent=self, pos=wx.Point(247, 143), size=wx.Size(31, 30),
              style=0)
        self.cbInputDel.Bind(wx.EVT_BUTTON, self.OnCbInputDelButton,
              id=wxID_VARCHIVERMAINPANELCBINPUTDEL)

        self.thdCtrl = vidarc.tool.misc.vtmThreadCtrl.vtmThreadCtrl(id=wxID_VARCHIVERMAINPANELTHDCTRL,
              name=u'thdCtrl', parent=self, pos=wx.Point(0, 177),
              size=wx.Size(278, 27), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vArchiver')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.lstLog.InsertColumn(0,'',format=wx.LIST_FORMAT_RIGHT,width=20)
        self.lstLog.InsertColumn(1,_(u'name'),format=wx.LIST_FORMAT_LEFT,width=80)
        self.lstLog.SetStretchLst([(1,-1)])
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        #self.dImg['qst']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        self.lstLog.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.widLog=vtLog.GetPrintMsgWid(self)
        
        self.thdProc=vtThreadWX(self,bPost=True)
        self.thdCtrl.SetThread(self.thdProc)
        self.thdCtrl.SetFuncStart(self.Start)
        self.thdCtrl.SetFuncStop(self.Stop)
        self.thdProc.BindEvents(self.OnThreadProc,self.OnThreadAborted,self.OnThreadFin)
        
        self.zNow=vtDateTime(True)
        self.zNow.Now()
        
        self.popupExternalDlg=None
        
        self.xdCfg=vtXmlDom(appl='vArchiverCfg',audit_trail=False)
        self.bBlock=True
        parent.AddMenuItems('mnTools',0,[
            (u'external',vtArt.getBitmap(vtArt.Config),self.popupExternal,(),{}),
            ])
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
        #r=self.xdCfg.getBaseNode()
        #self.xdCfg.addChild(r,'vArchiverGui')
        self.xdCfg.Save(fn)
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dCfg:%s'%(vtLog.pformat(self.dCfg)),self)
            self.chcCfg.Clear()
            for i in xrange(20):
                sCfg='cfg%02d'%i
                if sCfg in self.dCfg:
                    dd=self.dCfg[sCfg]
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;dd:%s'%(sCfg,vtLog.pformat(dd)),self)
                    self.chcCfg.Append(dd['name'])
            self.chcCfg.SetSelection(0)
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)

        #self.__showInterface__()
    def __showInterface__(self):
        vtLog.vtLngCurWX(vtLog.CRITICAL,''%(),self)
        return
        try:
            self.lstInterface.DeleteAllItems()
            
            sFullFN=self.GetFN()
            sDN,sFN=os.path.split(sFullFN)
            if len(sFullFN)<=0:
                return
            if os.path.exists(sFullFN)==False:
                imgId=self.dImg['err']
                idx=self.lstInterface.InsertStringItem(sys.maxint,'',imgId)
                return
            imgId=self.dImg['ok']
            idx=self.lstInterface.InsertStringItem(sys.maxint,'',imgId)
            self.lstInterface.SetStringItem(idx,2,sFN)
            fIn=open(sFullFN)
            bInterfaceSec=False
            iIdx=1
            for s in fIn:
                if s.startswith('# Interface IP Configuration'):
                    bInterfaceSec=True
                elif s.startswith('# End of interface IP configuration'):
                    bInterfaceSec=False
                if bInterfaceSec==True:
                    if s.startswith('set '):
                        iS=s.find('name=')
                        if iS>0:
                            iE=s.find('"',iS+6)
                            if iE>0:
                                #l=s[iE+2:].split(' ')
                                sKind=s[4:iS]
                                sName=s[iS+6:iE]
                                #print sKind,sName,l
                                idx=self.lstInterface.InsertStringItem(sys.maxint,'%03d'%iIdx,-1)
                                self.lstInterface.SetStringItem(idx,1,sKind)
                                self.lstInterface.SetStringItem(idx,2,sName)
                                self.lstInterface.SetStringItem(idx,3,s[iE+2:-1])
                                iIdx+=1
            fIn.close()
        except:
            vtLog.vtLngTB(self.GetName())
    def __set__(self, sFullFN):
        try:
            vtLog.PrintMsg(_(u'setting configuration ...'),self.widLog)
            
            sDN,sFN=os.path.split(sFullFN)
            if len(sFullFN)<=0:
                return
            if os.path.exists(sFullFN)==False:
                vtLog.PrintMsg(_(u'configuration %s faulty...')%(sFullFN),self.widLog)
                return
            iProcCount=2
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            sSys=platform.system()
            sNode=platform.node()
            if sSys=='Windows':
                vtLog.SetProcBarVal(1,iProcCount,self.widLog)
                sCmd='netsh -f %s'%sFullFN
                tPipes=os.popen3(sCmd)
                lOut=tPipes[1].readlines()
                lErr=tPipes[2].readlines()
                vtLog.SetProcBarVal(2,iProcCount,self.widLog)
            vtLog.PrintMsg(_(u'configuration set.'),self.widLog)
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetFN(self):
        try:
            iAct,d=self.__selCfg__()
            if iAct<0:
                return ''
            sSys=platform.system()
            sNode=platform.node()
            sDN=self.dbbOut.GetValue()
            d['outDN']=sDN
            sFN=u'_'.join([sNode,sSys,'.'.join([d['name'],'cfg'])])
            sFullFN=os.path.join(sDN,sFN)
            return sFullFN
        except:
            vtLog.vtLngTB(self.GetName())
            return sFullFN
    def __get__(self, sFullFN):
        try:
            self.lstInterface.DeleteAllItems()
            vtLog.PrintMsg(_(u'generate ...'),self.widLog)
            if len(sFullFN)==0:
                vtLog.vtLngCur(vtLog.ERROR,'no output file set',self)
                vtLog.PrintMsg(_(u'no output filename set.'),self.widLog)
                return
            if sFullFN.lower().endswith('.cfg')==False:
                vtLog.vtLngCur(vtLog.ERROR,'output file %s must be csv'%(sFullFN),self)
                vtLog.PrintMsg(_(u'output filename:%s must be cfg.')%(sFullFN),self.widLog)
                return
            iProcCount=2
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            vtLog.PrintMsg(_(u'get network interface configuration ...'),self.widLog)
            sSys=platform.system()
            if sSys=='Windows':
                vtLog.SetProcBarVal(1,iProcCount,self.widLog)
                sDN,sFN=os.path.split(sFullFN)
                #sFN=u'_'.join([sNode,sSys,sFN])
                if os.path.exists(sDN)==False:
                    os.makedirs(sDN)
                #sFullFN=os.path.join(sDN,sFN)
                shiftFile(sFullFN)
                fOut=open(sFullFN,'w')
                sCmd='netsh -c interface dump'
                tPipes=os.popen3(sCmd)
                lOut=tPipes[1].readlines()
                lErr=tPipes[2].readlines()
                vtLog.PrintMsg(_(u'save file:%s ...')%(sFullFN),self.widLog)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'out:%s'%(';'.join([s.replace('\n','') for s in lOut])),self)
                for s in lOut:
                    fOut.write(s)
                fOut.close()
                vtLog.SetProcBarVal(2,iProcCount,self.widLog)
                vtLog.PrintMsg(_(u'saved file:%s.')%(sFullFN),self.widLog)
            vtLog.SetProcBarVal(0,iProcCount,self.widLog)
            self.__showInterface__()
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.PrintMsg(_(u'save file:%s faulty.')%(sFullFN),self.widLog)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        return _(u'VIDARC Tools Archiver'),getAboutDescription(),version
    # ----------------------------------------
    # application
    # ----------------------------------------
    def OnChcCfgChoice(self, event):
        event.Skip()
        wx.CallAfter(self.__showCfg__)
    def OnCbCfgDelButton(self, event):
        event.Skip()
        try:
            iAct=self.chcCfg.GetSelection()
            if iAct<0:
                return
            iCount=self.chcCfg.GetCount()
            iCount=min(iCount,20)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            for i in xrange(iAct,iCount-1):
                self.dCfg['cfg%02d'%(i)]=self.dCfg['cfg%02d'%(i+1)]
            del self.dCfg['cfg%02d'%(iCount-1)]
            self.chcCfg.Delete(iAct)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            self.chcCfg.SetSelection(0)
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCfgAddButton(self, event):
        event.Skip()
        try:
            iCount=self.chcCfg.GetCount()
            iCount=min(iCount,20)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            for i in xrange(iCount,0,-1):
                self.dCfg['cfg%02d'%(i)]=self.dCfg['cfg%02d'%(i-1)]
            sName=self.txtCfg.GetValue()
            sStyle='bz2'
            if self.tgZip.GetValue():
                sStyle='zip'
            
            self.chcCfg.Insert(sName,0)
            self.dCfg['cfg00']={'name':sName,'style':sStyle}
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iCount:%d,%s'%(iCount,vtLog.pformat(self.dCfg)),self)
            self.chcCfg.SetSelection(0)
            self.txtCfg.SetValue('')
            
            wx.CallAfter(self.__showCfg__)
        except:
            vtLog.vtLngTB(self.GetName())
    def __selCfg__(self):
        try:
            iAct=self.chcCfg.GetSelection()
            if iAct>=0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'iAct:%d,%s'%(iAct,vtLog.pformat(self.dCfg)),self)
                d=self.dCfg['cfg%02d'%(iAct)]
                return iAct,d
        except:
            vtLog.vtLngTB(self.GetName())
        return -1,None
    def __showCfg__(self):
        try:
            self.lstLog.DeleteAllItems()
            iAct,d=self.__selCfg__()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iAct:%d;d:%s'%(iAct,vtLog.pformat(d)),self)
            if iAct<0:
                #self.__showInterface__()
                return
            if 'outDN' in d:
                self.dbbOut.SetValue(d['outDN'])
            else:
                sDN=os.getcwd()
                self.dbbOut.SetValue(sDN)
                d['outDN']=sDN
            bFlag=False
            if 'style' in d:
                if d['style'].lower()=='zip':
                    bFlag=True
            self.tgZip.SetValue(bFlag)
            imgId=-1
            for i in xrange(1000):
                k='inDN_%03d'%i
                if k in d:
                    idx=self.lstLog.InsertStringItem(sys.maxint,'',imgId)
                    self.lstLog.SetStringItem(idx,1,d[k])
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbAddButton(self, event):
        event.Skip()
        try:
            imgId=-1
            sDN=self.dbbIn.GetValue()
            idx=self.lstLog.InsertStringItem(sys.maxint,'',imgId)
            self.lstLog.SetStringItem(idx,1,sDN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbInputDelButton(self, event):
        try:
            iCount=self.lstLog.GetItemCount()
            for i in xrange(iCount-1,-1,-1):
                if self.lstLog.GetItemState(i,wx.LIST_STATE_SELECTED)==wx.LIST_STATE_SELECTED:
                    self.lstLog.DeleteItem(i)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbInputApplyButton(self, event):
        event.Skip()
        try:
            iAct,d=self.__selCfg__()
            for i in xrange(1000):
                k='inDN_%03d'%i
                if k in d:
                    del d[k]
            lDN=[]
            iCount=self.lstLog.GetItemCount()
            for i in xrange(iCount):
                sDN=self.lstLog.GetItem(i,1).m_text
                lDN.append(sDN)
            
            bFlag=self.tgZip.GetValue()
            if bFlag:
                d['style']='zip'
            i=0
            for sDN in lDN:
                k='inDN_%03d'%i
                d[k]=sDN
                i+=1
        except:
            vtLog.vtLngTB(self.GetName())
    def __add2CompressFile__(self,archFile,sDir,iAct,iSize,iSkip,thd):
        for root,dirs,files in os.walk(sDir):
            root=root.replace('\\','/')
            for sfn in files:
                if thd.Is2Stop():
                    return
                fn='/'.join([root,sfn])[iSkip:]
                if type(fn)==types.UnicodeType:
                    fn=fn.encode('ISO-8859-1')
                archFile.write(str(fn))
                mode=os.stat(fn)
                iAct=iAct+mode.st_size
                thd.Post('proc',iAct,iSize)
        return iAct
    def GenerateZip(self,sFN,sDN,iAct,iSize,iSkip,thd):
        cur=0
        zipFile=zipfile.ZipFile(sFN,'w',zipfile.ZIP_DEFLATED)
        iAct=self.__add2CompressFile__(zipFile,sDN,iAct,iSize,iSkip,thd)
        zipFile.close()
        if thd.Is2Stop():
            os.remove(self.sFN)
        return iAct
    def GenerateBz2(self,sFN,sDN,iAct,iSize,iSkip,thd):
        cur=0
        #tar=tarfile.TarFileCompat(sFN,'w:bz2')
        tar=tarfile.TarFileCompat(sFN,'w',zipfile.ZIP_DEFLATED)
        iAct=self.__add2CompressFile__(tar,sDN,iAct,iSize,iSkip,thd)
        #self.__add2CompressFile__(tar,sDir)
        tar.close()
        if thd.Is2Stop():
            os.remove(sFN)
        return iAct
    def procCalcSize(self,sDN):
        lSize=0
        for sDN,lDir,lFile in os.walk(sDN):
            lSize+=sum(getsize(join(sDN, name)) for name in lFile)
        return lSize
    def procArchives(self,thd,sName,sHost,sNow,sExt,sOutDN,lDN,f):
        iSize=0
        iAct=0
        #vtLog.PrintMsg(_(u'generate archive %s ...')%(sDN),self.widLog)
        thd.Post('proc',_(u'generate archive %s calculate size ...')%(sName),None)
        sCWD=os.getcwd()
        try:
            for sDN in lDN:
                if os.path.exists(sDN):
                    iSize+=self.procCalcSize(sDN)
            for sDN in lDN:
                thd.Post('proc',_(u'generate archive %s %s ...')%(sName,sDN),None)
                if os.path.exists(sDN):
                    l=sDN.split('/')
                    if len(l)>1:
                        sFN=u'_'.join([sName,l[-1],sNow,sHost,sExt])
                    else:
                        sFN=u'_'.join([sName,sNow,sHost,sExt])
                    sFullFN=u'/'.join([sOutDN,sFN])
                    vtLog.PrintMsg(_(u'generate archive %s ...')%(sFullFN),self.widLog)
                    sBaseDN='/'.join(l[:-1])
                    os.chdir(sBaseDN)
                    iSkip=len(sBaseDN)+1
                    iAct+=f(sFullFN,sDN,iAct,iSize,iSkip,thd)
        except:
            thd.__logTB__()
        os.chdir(sCWD)
        thd.Post('proc',_(u'generate archive %s done.')%(sName),None)
    def externalOut(self,s,l):
        l.append(s)
    def externalErr(self,s,l):
        l.append(s)
    def GenerateExternal(self,sCmd,sArg,sFN,sDN,iAct,iSize,iSkip,thd):
        s=sArg.replace('%f',sFN)
        s=s.replace('%d',sDN)
        iAct+=1
        thd.Post('proc',iAct,iSize)
        thd.__logInfo__('external cmd;'+' '.join(['%s'%sCmd,s]))
        p=vtProcess(' '.join(['"%s"'%sCmd,s]),True)
        lOut=[]
        lErr=[]
        p.SetCB(1,None,self.externalOut,lOut)
        p.SetCB(2,None,self.externalErr,lErr)
        p.communicateLoop()
        thd.__logInfo__(''.join(lOut))
        thd.__logError__(''.join(lErr))
        return iAct
    def procExternal(self,thd,sCmd,sArg,sName,sHost,sNow,sExt,sOutDN,lDN,f):
        iSize=0
        iAct=0
        #vtLog.PrintMsg(_(u'generate archive %s ...')%(sDN),self.widLog)
        thd.Post('proc',_(u'generate archive %s calculate size ...')%(sName),None)
        sCWD=os.getcwd()
        try:
            iSize=len(lDN)
            for sDN in lDN:
                thd.Post('proc',_(u'generate archive %s %s ...')%(sName,sDN),None)
                if os.path.exists(sDN):
                    l=sDN.split('/')
                    if len(l)>1:
                        sFN=u'_'.join([sName,l[-1],sNow,sHost,sExt])
                    else:
                        sFN=u'_'.join([sName,sNow,sHost,sExt])
                    sFullFN=u'/'.join([sOutDN,sFN])
                    vtLog.PrintMsg(_(u'generate archive %s ...')%(sFullFN),self.widLog)
                    sBaseDN='/'.join(l[:-1])
                    os.chdir(sBaseDN)
                    iSkip=len(sBaseDN)+1
                    iAct+=f(sCmd,sArg,sFullFN,sDN,iAct,iSize,iSkip,thd)
        except:
            thd.__logTB__()
        os.chdir(sCWD)
        thd.Post('proc',_(u'generate archive %s done.')%(sName),None)
    def Start(self):
        try:
            self.zNow.Now()
            sNow='_'.join([self.zNow.GetDateSmallStr(),self.zNow.GetTimeSmallStr()])
            iAct,d=self.__selCfg__()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iAct:%d;d:%s'%(iAct,vtLog.pformat(d)),self)
            #if 'outDN' in d:
            #    self.dbbOut.SetValue(d['outDN'])
            sOutDN=self.dbbOut.GetValue()
            lDN=[]
            for i in xrange(1000):
                k='inDN_%03d'%i
                if k in d:
                    sDN=d[k].replace('\\','/')
                    lDN.append(sDN)
            
            sHost=platform.node()
            sName=d['name']
            sOutDN=self.dbbOut.GetValue()
            sOutDN=sOutDN.replace('\\','/')
            
            #self.lstLog.DeleteAllItems()
            thd=self.thdCtrl.GetThread()
            thd.Start()
            vtLog.PrintMsg(_(u'generate archive %s ...')%(sOutDN),self.widLog)
            
            sCmd=d.get('external',None)
            sArg=d.get('argument',None)
            if sCmd is not None and sArg is not None:
                sExt=u''
                f=self.GenerateExternal
                
                self.thdCtrl.Do(self.procExternal,thd,sCmd,sArg,
                        sName,sHost,sNow,sExt,sOutDN,lDN,f)
            else:
                iStyle=0
                if 'style' in d:
                    if d['style'].lower()=='zip':
                        iStyle=1
                if iStyle==1:
                    sExt=u'.zip'
                    f=self.GenerateZip
                else:
                    sExt=u'.tar.bz2'
                    f=self.GenerateBz2
                self.thdCtrl.Do(self.procArchives,thd,sName,sHost,sNow,sExt,
                        sOutDN,lDN,f)
        except:
            vtLog.vtLngTB(self.GetName())
    def Stop(self):
        try:
            #self.thdRead.Stop()
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OnThreadProc(self,evt):
        evt.Skip()
        #if evt.GetCount() is None:
        #    self.pnLog.AddLog(evt.GetAct())
    def OnThreadAborted(self,evt):
        evt.Skip()
    def OnThreadFin(self,evt):
        evt.Skip()

    def OnTgZipButton(self, event):
        event.Skip()
    def popupExternal(self):
        try:
            if self.popupExternalDlg is None:
                self.popupExternalDlg=vArchiverExternalDialog(self)
            iAct,d=self.__selCfg__()
            if d is not None:
                sCmd=d.get('external','')
                sArg=d.get('argument','')
            else:
                sCmd=''
                sArg=''
            self.popupExternalDlg.Centre()
            self.popupExternalDlg.SetCmd(sCmd)
            self.popupExternalDlg.SetArg(sArg)
            iRet=self.popupExternalDlg.ShowModal()
            if iRet==1:
                sCmd=self.popupExternalDlg.GetCmd()
                sArg=self.popupExternalDlg.GetArg()
                d['external']=sCmd
                d['argument']=sArg
        except:
            vtLog.vtLngTB(self.GetName())

