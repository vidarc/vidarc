#----------------------------------------------------------------------------
# Name:         vXmlLauncher.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vXmlLauncher.py,v 1.1 2008/08/13 00:14:37 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vTools.vLauncher.vXmlNodeLauncherRoot import vXmlNodeLauncherRoot
from vidarc.vTools.vLauncher.vXmlNodeGrp import vXmlNodeGrp
from vidarc.vTools.vLauncher.vXmlNodeCmd import vXmlNodeCmp

#from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
#from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
#from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

class vXmlLauncher(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='LauncherRoot'
    APPL_REF=['vHum','vGlobals','vMsg']
    def __init__(self,*kw,**kwargs):
        if 'skip' in kwargs:
            skip=kwargs['skip']
            for s in ['config']:
                try:
                    idx=skip.index(s)
                except:
                    skip.append(s)
        try:
            vtXmlDomReg.__init__(self,*kw,**kwargs)
            oRoot=vXmlNodeLauncherRoot()
            self.RegisterNode(oRoot,False)
            #oLauncher=vXmlNodeLauncher()
            
            #oLog=vtXmlNodeLog()
            #oCfg=vtXmlNodeCfg()
            oGrp=vtXmlNodeGrp()
            oCmd=vtXmlNodeCmd()
            self.RegisterNode(oLauncher,True)
            #self.RegisterNode(oLog,False)
            #self.RegisterNode(oCfg,True)
            #self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            self.RegisterNode(oGrp,True)
            self.RegisterNode(oCmd,True)
            
            #self.LinkRegisteredNode(oRoot,oLauncher)
            #self.LinkRegisteredNode(oLauncher,oLog)
            self.LinkRegisteredNode(oRoot,oGrp)
            self.LinkRegisteredNode(oRoot,oCmd)
            self.LinkRegisteredNode(oGrp,oCmd)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'Launcher')
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'Launcher')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
