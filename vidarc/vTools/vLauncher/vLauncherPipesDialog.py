#Boa:Dialog:vLauncherPipesDialog
#----------------------------------------------------------------------------
# Name:         vLauncherPipesFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherPipesDialog.py,v 1.1 2008/08/13 00:14:37 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.vTools.vLauncher.images as imgLauncher

def create(parent):
    return vLauncherPipesDialog(parent)

[wxID_VLAUNCHERPIPESDIALOG, wxID_VLAUNCHERPIPESDIALOGCBCLR, 
 wxID_VLAUNCHERPIPESDIALOGCBENTER, wxID_VLAUNCHERPIPESDIALOGCBSAVE, 
 wxID_VLAUNCHERPIPESDIALOGCHCWRAP, wxID_VLAUNCHERPIPESDIALOGSPSZ, 
 wxID_VLAUNCHERPIPESDIALOGTXTIN, wxID_VLAUNCHERPIPESDIALOGTXTOUT, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vLauncherPipesDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbClr, 0, border=8, flag=0)
        parent.AddWindow(self.cbSave, 0, border=7, flag=wx.TOP)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtIn, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbEnter, 0, border=0, flag=0)
        parent.AddWindow(self.txtOut, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=0)
        parent.AddSizer(self.bxsOption, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsOption_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcWrap, 0, border=4, flag=0)
        parent.AddWindow(self.spSz, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=3, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsOption = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsOption_Items(self.bxsOption)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLAUNCHERPIPESDIALOG,
              name=u'vLauncherPipesDialog', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(290, 210), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLauncherPipesDialog')
        self.SetClientSize(wx.Size(282, 183))

        self.txtIn = wx.TextCtrl(id=wxID_VLAUNCHERPIPESDIALOGTXTIN,
              name=u'txtIn', parent=self, pos=wx.Point(0, 0), size=wx.Size(247,
              30), style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtIn.Bind(wx.EVT_TEXT_ENTER, self.OnTxtInTextEnter,
              id=wxID_VLAUNCHERPIPESDIALOGTXTIN)

        self.cbEnter = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERPIPESDIALOGCBENTER,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbEnter', parent=self,
              pos=wx.Point(251, 0), size=wx.Size(31, 30), style=0)
        self.cbEnter.Bind(wx.EVT_BUTTON, self.OnCbEnterButton,
              id=wxID_VLAUNCHERPIPESDIALOGCBENTER)

        self.txtOut = wx.TextCtrl(id=wxID_VLAUNCHERPIPESDIALOGTXTOUT,
              name=u'txtOut', parent=self, pos=wx.Point(0, 34),
              size=wx.Size(247, 123), style=wx.TE_READONLY | wx.TE_MULTILINE,
              value=u'')

        self.cbClr = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERPIPESDIALOGCBCLR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbClr', parent=self,
              pos=wx.Point(251, 34), size=wx.Size(31, 30), style=0)
        self.cbClr.Bind(wx.EVT_BUTTON, self.OnCbClrButton,
              id=wxID_VLAUNCHERPIPESDIALOGCBCLR)

        self.cbSave = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERPIPESDIALOGCBSAVE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbSave', parent=self,
              pos=wx.Point(251, 71), size=wx.Size(31, 30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VLAUNCHERPIPESDIALOGCBSAVE)

        self.spSz = wx.SpinCtrl(id=wxID_VLAUNCHERPIPESDIALOGSPSZ, initial=0,
              max=100, min=0, name=u'spSz', parent=self, pos=wx.Point(74, 161),
              size=wx.Size(86, 22), style=wx.SP_ARROW_KEYS)

        self.chcWrap = wx.CheckBox(id=wxID_VLAUNCHERPIPESDIALOGCHCWRAP,
              label=_(u'wrap'), name=u'chcWrap', parent=self, pos=wx.Point(0,
              161), size=wx.Size(70, 13), style=0)
        self.chcWrap.SetValue(True)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnCbEnterButton(self, event):
        event.Skip()

    def OnCbClrButton(self, event):
        event.Skip()

    def OnCbSaveButton(self, event):
        event.Skip()

    def OnTxtInTextEnter(self, event):
        event.Skip()
