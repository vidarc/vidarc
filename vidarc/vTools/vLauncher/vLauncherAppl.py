#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vLauncherAppl.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherAppl.py,v 1.3 2009/02/05 21:08:06 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import sys,getopt,os

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
vtLog.vtLngInit('vLauncher','vLauncher.log',vtLog.ERROR)
import vSystem
import vLauncherMainFrame

modules ={u'vLauncherMainFrame': [1, 'Main frame of Application', u'vLauncherMainFrame.py']}

class BoaApp(wx.App):
    def RedirectStdio(self, filename=None):
        """Redirect sys.stdout and sys.stderr to a file or a popup window."""
        if filename:
            sys.stderr = open(filename, 'w')
            sys.stdout = open(filename.replace('.err.','.out.'), 'w')
        else:
            self.stdioWin = self.outputWindowClass()
            sys.stdout = sys.stderr = self.stdioWin
    def OnInit(self):
        #self.RedirectStdio(vSystem.getErrFN('vLauncher'))
        wx.InitAllImageHandlers()
        self.main = vLauncherMainFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")

def main():
    optshort,optlong='l:b:c:h',['lang=','log=','config=','base=','help']
    vtLgBase.initAppl(optshort,optlong,'vLaunch')
    iLogLv=vtLog.ERROR
    
    cfgFN=os.path.join(os.getcwdu(),'vLauncher.xml')
    sBaseDir=None
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--base']:
                sBaseDir=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLog.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLog.INFO
                elif o[1]=='2':
                    iLogLv=vtLog.WARN
                elif o[1]=='3':
                    iLogLv=vtLog.ERROR
                elif o[1]=='4':
                    iLogLv=vtLog.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLog.FATAL
    except:
        showHelp()
    vtLog.vtLngInit('vLauncher','vLauncher.log',iLogLv)
    #application.main.SetBaseDir(sBaseDir)
    application = BoaApp(0,filename=vSystem.getErrFN('vLauncher'))
    application.main.OpenFile(cfgFN)
    application.MainLoop()
    
if __name__ == '__main__':
    main()
