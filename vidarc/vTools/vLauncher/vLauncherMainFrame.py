#Boa:Frame:vLauncherMainFrame
#----------------------------------------------------------------------------
# Name:         vLauncherMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherMainFrame.py,v 1.7 2009/10/18 18:53:42 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.log.vtArtLogThrobber
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import time,traceback

MDI_CHILD_FRAME=True

try:
    import getpass
    import codecs,string,os,os.path,stat
    import vidarc.vTools.vLauncher.images as images
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    #from vidarc.vApps.common.vCfg import vCfg
    import vidarc.tool.InOut.fnUtil as fnUtil
    from vXmlDomBasic import vXmlDomBasic
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    from vidarc.vTools.vLauncher.vLauncherMainPanel import vLauncherMainPanel
    from vidarc.vTools.vLauncher.vLauncherObj import vLauncherGrp,vLauncherCmd
    #from vidarc.vTools.vLauncher.vCalendarMainFrame import vCalendarMainFrame
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

def create(parent):
    return vLauncherMainFrame(parent)

[wxID_VLAUNCHERMAINFRAME, wxID_VLAUNCHERMAINFRAMECBCALENDAR, 
 wxID_VLAUNCHERMAINFRAMECBCLOSE, wxID_VLAUNCHERMAINFRAMECBLB, 
 wxID_VLAUNCHERMAINFRAMECBLT, wxID_VLAUNCHERMAINFRAMECBRB, 
 wxID_VLAUNCHERMAINFRAMECBRT, wxID_VLAUNCHERMAINFRAMECBSAVE, 
 wxID_VLAUNCHERMAINFRAMEPNMAIN, wxID_VLAUNCHERMAINFRAMETGBEXPAND, 
 wxID_VLAUNCHERMAINFRAMETHRALMHIGH, wxID_VLAUNCHERMAINFRAMETXTCLK, 
] = [wx.NewId() for _init_ctrls in range(12)]

[wxID_VGFMAINTBMAINTOOL_LEFT_BOTTOM, wxID_VGFMAINTBMAINTOOL_LEFT_TOP, 
 wxID_VGFMAINTBMAINTOOL_RIGHT_BOTTOM, wxID_VGFMAINTBMAINTOOL_RIGHT_TOP, 
] = [wx.NewId() for _init_coll_tbMain_Tools in range(4)]

dictLaunchApps={
    u'vPrjTimer':[u'vPrjTimer/vPrjTimerAppl.py',u'vPrjTimer/vProjectTimer.exe'],
    u'vPrjEng':[u'vPrjEng/vPrjEngAppl.py',u'vPrjEng/vPrjEng.exe'],
    u'vPrj':[u'vPrj/vPrjAppl.py',u'vProject/vProject.exe'],
    u'vPrjDoc':[u'vPrjDoc/vPrjDocAppl.py',u'vPrjDoc/vPrjDoc.exe'],
    u'vHum':[u'vHum/vHumAppl.py',u'vHum/vHuman.exe'],
    u'vDoc':[u'vDoc/vDocAppl.py',u'vDoc/vDoc.exe'],
    u'vSec':[],
    u'Net':[],
    u'Tools':[],
    u'vVSAS':[u'vVSAS/vVSASAppl.py',u'vVSAS/vVSAS.exe']
                }
class vLauncherMainFrame(wx.Frame,vtLog.vtLogOrigin):
    def _init_coll_fgsBottom_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbLB, 0, border=0, flag=0)
        parent.AddSizer(self.bxsBottom, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbRB, 0, border=0, flag=0)

    def _init_coll_fgsBottom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBottom_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrAlmHigh, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbCalendar, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.txtClk, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsTop_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsTop, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.fgsInfo, 1, border=0,
              flag=wx.ALIGN_CENTER | wx.EXPAND)
        parent.AddSizer(self.fgsBottom, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsTop_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbLT, 0, border=0, flag=0)
        parent.AddSizer(self.bxsTop, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbRT, 0, border=0, flag=0)

    def _init_coll_bxsTop_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgbExpand, 0, border=0, flag=0)
        parent.AddWindow(self.cbSave, 0, border=0, flag=0)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsTop = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBottom = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsTop = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.fgsBottom = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.fgsInfo = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsTop_Items(self.bxsTop)
        self._init_coll_bxsBottom_Items(self.bxsBottom)
        self._init_coll_fgsTop_Items(self.fgsTop)
        self._init_coll_fgsTop_Growables(self.fgsTop)
        self._init_coll_fgsBottom_Items(self.fgsBottom)
        self._init_coll_fgsBottom_Growables(self.fgsBottom)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)

        self.pnMain.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VLAUNCHERMAINFRAME,
              name=u'vLauncherMainFrame', parent=prnt, pos=wx.Point(210, 126),
              size=wx.Size(261, 394),
              style=wx.MINIMIZE_BOX|wx.CLOSE_BOX|wx.SYSTEM_MENU|wx.CAPTION|wx.RESIZE_BORDER|wx.FRAME_TOOL_WINDOW,
              title=_(u'vLauncher'))
        self.SetClientSize(wx.Size(253, 367))

        self.pnMain = wx.Panel(id=wxID_VLAUNCHERMAINFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(253, 367),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.cbRT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHERMAINFRAMECBRT, name=u'cbRT', parent=self.pnMain,
              pos=wx.Point(229, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbRT.SetConstraints(LayoutAnchors(self.cbRT, False, True, True,
              False))
        self.cbRT.Bind(wx.EVT_BUTTON, self.OnCbRTButton,
              id=wxID_VLAUNCHERMAINFRAMECBRT)

        self.cbRB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHERMAINFRAMECBRB, name=u'cbRB', parent=self.pnMain,
              pos=wx.Point(252, 337), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbRB.SetConstraints(LayoutAnchors(self.cbRB, False, False, True,
              True))
        self.cbRB.Bind(wx.EVT_BUTTON, self.OnCbRBButton,
              id=wxID_VLAUNCHERMAINFRAMECBRB)

        self.cbLB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHERMAINFRAMECBLB, name=u'cbLB', parent=self.pnMain,
              pos=wx.Point(0, 337), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLB.SetConstraints(LayoutAnchors(self.cbLB, True, False, False,
              True))
        self.cbLB.Bind(wx.EVT_BUTTON, self.OnCbLBButton,
              id=wxID_VLAUNCHERMAINFRAMECBLB)

        self.cbLT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHERMAINFRAMECBLT, name=u'cbLT', parent=self.pnMain,
              pos=wx.Point(0, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLT.SetConstraints(LayoutAnchors(self.cbLT, True, True, False,
              False))
        self.cbLT.Bind(wx.EVT_BUTTON, self.OnLeftTop,
              id=wxID_VLAUNCHERMAINFRAMECBLT)

        self.cbClose = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VLAUNCHERMAINFRAMECBCLOSE, name=u'cbClose',
              parent=self.pnMain, pos=wx.Point(336, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbClose.SetConstraints(LayoutAnchors(self.cbClose, False, True,
              True, False))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VLAUNCHERMAINFRAMECBCLOSE)

        self.tgbExpand = wx.lib.buttons.GenBitmapToggleButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERMAINFRAMETGBEXPAND, name=u'tgbExpand',
              parent=self.pnMain, pos=wx.Point(24, 0), size=wx.Size(24, 24),
              style=0)
        self.tgbExpand.Bind(wx.EVT_BUTTON, self.OnTgbExpandButton,
              id=wxID_VLAUNCHERMAINFRAMETGBEXPAND)

        self.txtClk = wx.TextCtrl(id=wxID_VLAUNCHERMAINFRAMETXTCLK,
              name=u'txtClk', parent=self.pnMain, pos=wx.Point(84, 337),
              size=wx.Size(168, 30), style=0, value='textCtrl1')

        self.cbSave = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VLAUNCHERMAINFRAMECBSAVE, name=u'cbSave',
              parent=self.pnMain, pos=wx.Point(48, 0), size=wx.Size(24, 24),
              style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VLAUNCHERMAINFRAMECBSAVE)

        self.cbCalendar = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERMAINFRAMECBCALENDAR, name=u'cbCalendar',
              parent=self.pnMain, pos=wx.Point(56, 337), size=wx.Size(24, 24),
              style=0)
        self.cbCalendar.Bind(wx.EVT_BUTTON, self.OnCbCalendarButton,
              id=wxID_VLAUNCHERMAINFRAMECBCALENDAR)

        self.thrAlmHigh = vidarc.tool.art.log.vtArtLogThrobber.vtArtLogThrobberHigh(id=wxID_VLAUNCHERMAINFRAMETHRALMHIGH,
              name=u'thrAlmHigh', parent=self.pnMain, pos=wx.Point(28, 337),
              size=wx.Size(24, 24), style=wx.RAISED_BORDER)
        self.thrAlmHigh.Bind(vidarc.tool.art.log.vtArtLogThrobber.vEVT_ART_LOG_CHANED,
              self.OnThrAlmHighArtLogChaned,
              id=wxID_VLAUNCHERMAINFRAMETHRALMHIGH)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.dom=vXmlDomBasic()
        vtLog.vtLogOrigin.__init__(self,origin=self.GetName())
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnVgfMainClose)
        self.Bind(wx.EVT_SIZE,self.OnVgfMainSize)
        
        self.sBaseDir=None
        self.baseNode=None
        self.lstPanel=[]
        
        i=0
        #add config
        self.iProtect=i
        
        img=images.getPosLTBitmap()
        self.cbLT.SetBitmapLabel(img)
        
        img=images.getPosRTBitmap()
        self.cbRT.SetBitmapLabel(img)
        
        img=images.getPosLBBitmap()
        self.cbLB.SetBitmapLabel(img)
        
        img=images.getPosRBBitmap()
        self.cbRB.SetBitmapLabel(img)
        
        img=images.getCloseBitmap()
        self.cbClose.SetBitmapLabel(img)
        
        self.tgbExpand.SetBitmapLabel(images.getCollapseBitmap())
        self.tgbExpand.SetBitmapSelected(images.getExpandBitmap())
        self.tgbExpand.SetValue(True)
        self.bExpand=True
        
        self.cbCalendar.SetBitmapLabel(images.getCalendarBitmap())
        
        id=wx.NewId()
        self.pnData=vLauncherMainPanel(self.pnMain,id,wx.DefaultPosition,
                    wx.DefaultSize,0,'pnLauncher')
        self.fgsInfo.AddWindow(self.pnData, 1, border=4, flag=wx.EXPAND|wx.TOP|wx.BOTTOM)
        self.pnData.SetFuncNotify(self.thrAlmHigh.SetDelta)
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        if wx.Platform == '__WXMSW__':
            self.TBMENU_RESTORE=wx.NewId()
            self.TBMENU_CLOSE=wx.NewId()
            # setup a taskbar icon, and catch some events from it
            self.tbicon = wx.TaskBarIcon()
            self.tbicon.SetIcon(icon, "VIDARC Launcher")
            wx.EVT_TASKBAR_LEFT_DCLICK(self.tbicon, self.OnTaskBarActivate)
            wx.EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
            wx.EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
            wx.EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)
        
        self.thrAlmHigh.Bind(wx.EVT_BUTTON, self.OnThrAlmHighAcknowledge,
              id=wxID_VLAUNCHERMAINFRAMETHRALMHIGH)
        
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        self.iPos=0
        self.fgsMain.Layout()
        self.fgsMain.Fit(self)
        wx.CallAfter(self.pnData.LaunchAuto)
    def __genPanel__(self,parent,name,isLauncher=False):
        panel = vInfoPanel(id=wx.NewId(), name=u'pn%s'%name,
              parent=parent, pos=wx.Point(0, 0), size=wx.Size(192, 253),
              style=wx.TAB_TRAVERSAL,isLauncher=isLauncher)
        EVT_INFO_CHANGED(panel,self.OnInfoChanged)
        EVT_INFO_LAUNCH(panel,self.OnInfoLaunch)
        panel.SetAutoLayout(True)
        return panel
    def Notify(self):
        t = time.localtime(time.time())
        st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
        self.txtClk.SetValue(st)
        pass
    def OnTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
        if not self.IsShown():
            self.Show(True)
        self.Raise()

    def OnTaskBarMenu(self, evt):
        menu = wx.Menu()
        #menu.Append(self.TBMENU_RESTORE, "Restore VIDARC Launcher")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_RESTORE,
                kind=wx.ITEM_NORMAL, text=_(u"Restore VIDARC Launcher"))
        it.SetBitmap(images.getOpenBitmap())
        menu.AppendItem(it)
        menu.AppendSeparator()
        #menu.Append(self.TBMENU_CLOSE,   "Close")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_CLOSE,
                kind=wx.ITEM_NORMAL, text=_(u"Close"))
        it.SetBitmap(images.getCancelBitmap())
        menu.AppendItem(it)
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()
    def OnVgfMainClose(self, event):
        vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            dlg = vtmMsgDialog(self,
                    _(u'Do you want to save configuration?'),
                    u'VIDARC Launcher '+ _(u'Close'),
                    wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                    )
            if dlg.ShowModal()==wx.ID_YES:
                self.SaveFile()
            self.pnData.ShutDown()
            if hasattr(self, "tbicon"):
                #del self.tbicon
                self.tbicon.Destroy()
                #del self.tbicon
            self.dom.Close()
            self.timer.Stop()
            #self.Destroy()
            event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
        vtLog.vtLngCurWX(vtLog.DEBUG,'fin'%(),self)
    def OnLeftTop(self, event):
        self.iPos=0
        self.__update__()
        event.Skip()
    def __update__(self):
        pos=self.GetPosition()
        if self.bExpand:
            self.size=self.GetSize()
            self.txtClk.Show(True)
            self.pnData.Show(True)
        else:
            self.txtClk.Show(False)
            self.pnData.Show(False)
            wsize=self.GetSize()
            size=self.GetClientSize()
            dw=wsize.GetWidth()-size.GetWidth()
            dy=wsize.GetHeight()-size.GetHeight()
            self.SetSize(wx.Size(dw+24+48+24,dy+24+24))
        size=self.GetSize()
        dispSize=wx.ClientDisplayRect()
        if self.iPos==0:
            self.SetPosition(wx.Point(dispSize[0],dispSize[1]))
        elif self.iPos==1:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],0))
        elif self.iPos==2:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0],
                        dispSize[1]+dispSize[3]-size[1]))
        elif self.iPos==3:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],
                    dispSize[1]+dispSize[3]-size[1]))
        
        self.fgsMain.Layout()
        
        pos=self.GetPosition()
        size=self.GetSize()
        if self.baseNode is not None:
            nSet=self.dom.getChild(self.baseNode,'settings')
            if nSet is not None:
                self.dom.setNodeText(nSet,'pos',str(self.iPos))
                self.dom.setNodeText(nSet,'geometry',
                        '%d,%d,%d,%d'%(pos.x,pos.y,size.GetWidth(),size.GetHeight()))
            #self.SaveFile()
    def OnCbRTButton(self, event):
        self.iPos=1
        self.__update__()
        event.Skip()

    def OnCbLBButton(self, event):
        self.iPos=2
        self.__update__()
        event.Skip()

    def OnCbRBButton(self, event):
        self.iPos=3
        self.__update__()
        event.Skip()
    def OnVgfMainSize(self, event):
        size=event.GetSize()
        w=size.GetWidth()
        h=size[1]
        bMod=False
        if self.bExpand:
            if w<100:
                w=100
                bMod=True
            if h<100:
                h=100
                bMod=True
        dispSize=wx.ClientDisplayRect()
        if w>dispSize[2]:
            w=dispSize[2]
            bMod=True
        if h>dispSize[3]:
            h=dispSize[3]
            bMod=True
        if  bMod==True:
            self.SetSize(wx.Size(w,h))
        self.__update__()
        event.Skip()
    def CreateNew(self):
        self.dom.New(root='launcherentries')
        self.baseNode=self.dom.getRoot()
        root=self.dom.getRoot()
        nSet=self.dom.createSubNode(root,'settings',False)
        nPos=self.dom.createSubNodeText(nSet,'pos','0',False)
        self.iPos=0
    def __procValues__(self,n,dom,keys,d):
        sTag=dom.getTagName(n)
        if sTag in keys:
            d[sTag]=dom.getText(n)
            keys.remove(sTag)
            if len(keys)==0:
                return -1
        return 0
    def __procHistValues__(self,n,dom,lHist):
        sTag=dom.getTagName(n)
        if sTag=='value':
            lHist.append(dom.getText(n))
        return 0
    def __procValuesWithHist__(self,n,dom,keys,d):
        sTag=dom.getTagName(n)
        #print sTag,keys
        if sTag in keys:
            if sTag=='hist':
                lHist=[]
                dom.procChildsExt(n,self.__procHistValues__,dom,lHist)
                d[sTag]=lHist
            elif sTag=='recent':
                lRecent=[]
                dom.procChildsExt(n,self.__procHistValues__,dom,lRecent)
                d[sTag]=lRecent
            else:
                d[sTag]=dom.getText(n)
                #print d
            keys.remove(sTag)
            if len(keys)==0:
                return -1
        return 0
    def __procLaunches__(self,n,dom,launches):
        sTag=dom.getTagName(n)
        if sTag=='grp':
            d={'name':''}
            dom.procChildsExt(n,self.__procValues__,dom,d.keys(),d)
            o=vLauncherGrp(**d)
            launches.append(o)
            dom.procChildsExt(n,self.__procLaunches__,dom,launches=o)
        elif sTag=='cmd':
            d={
                'name':'','appl':'','dn':'','args':'','usr':'',
                'img':'','recent_size':'','recent':None,
                'delay':'','hist':None,'geometry':'',
                'opt':''
                }
            dom.procChildsExt(n,self.__procValuesWithHist__,dom,d.keys(),d)
            o=vLauncherCmd(**d)
            launches.append(o)
        return 0
    def OpenFile(self,fn):
        global dictLaunchApps
        self.launches=vLauncherGrp()
        if self.dom.Open(fn)<0:
            self.CreateNew()
            self.baseNode=self.dom.getRoot()
            self.SaveFile(fn)
        self.baseNode=self.dom.getRoot()
        nLaunches=self.dom.getChildForced(self.baseNode,'launches')
        self.launches=vLauncherGrp()#[]
        self.dom.procChildsExt(nLaunches,self.__procLaunches__,self.dom,launches=self.launches)
        self.pnData.SetLaunches(self.launches)
        LauncherNodes={}
        for c in self.dom.getChilds(self.baseNode,'launcher'):
            sName=self.dom.getNodeText(c,'name')
            if len(sName)<=0:
                continue
            try:
                idx=self.launchers.index(sName)
                self.lstPanel[idx].SetNode(c,self.dom)
            except:
                panel=self.__genPanel__(self.lbkInfo,sName)
                sImage=self.dom.getNodeText(c,'image')
                idxImg=-1
                try:
                    if len(sImage)>0:
                        bmp=wx.Image(sImage).ConvertToBitmap()
                        imgLst=self.lbkInfo.GetImageList()
                        idxImg=imgLst.Add(bmp)
                except:
                    pass
                iPgCount=self.lbkInfo.GetPageCount()-1
                self.lbkInfo.RemovePage(iPgCount)
                self.lbkInfo.AddPage(panel,sName,False,idxImg)
                self.lbkInfo.AddPage(self.pnCfg,u'Settings',False,self.iCfgImg)
                self.iProtect=iPgCount
                
                self.lstPanel.insert(iPgCount,panel)
                panel.SetNode(c,self.dom)
                self.launchers.insert(iPgCount,sName)
                
                dictLaunchApps[sName]=[]
            LauncherNodes[sName]=c
            pass
        nSet=self.dom.getChild(self.baseNode,'settings')
        try:
            self.iPos=int(self.dom.getNodeText(nSet,'pos'))
        except:
            self.iPos=0
        try:
                s=self.dom.getNodeText(nSet,'geometry')
                l=s.split(',')
                if len(l)>=4:
                    lVal=map(int,l)
                    self.Move((lVal[0],lVal[1]))
                    self.SetSize((lVal[2],lVal[3]))
        except:
            vtLog.vtLngTB(self.GetName())
        self.__update__()
    def SaveFile(self,fn=None):
        nLaunches=self.dom.getChild(self.baseNode,'launches')
        if nLaunches is not None:
            self.dom.deleteNode(nLaunches,self.baseNode)
        nLaunches=self.dom.getChildForced(self.baseNode,'launches')
        self.launches.Save(nLaunches,self.dom)
        self.dom.AlignDoc()
        fnUtil.shiftFile(self.dom.sFN)
        self.dom.Save(fn)
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()
    def OnTgbExpandButton(self, event):
        self.bExpand=self.tgbExpand.GetValue()
        if self.bExpand:
            self.SetSize(self.size)
        self.__update__()    
            
        event.Skip()
    def OnCbLaunchButton(self, event):
        idx=self.lbkInfo.GetSelection()
        sFN=self.lstPanel[idx].GenLaunch()
        event.Skip()

    def OnCbSaveButton(self, event):
        event.Skip()
        self.SaveFile()

    def OnCbCalendarButton(self, event):
        event.Skip()
        #frm=vCalendarMainFrame(self,wx.NewId(),wx.Point(0,0),wx.DefaultSize,
        #        0,'vCalendar')
        #frm.Show(True)

    def OnThrAlmHighArtLogChaned(self, event):
        event.Skip()
    def OnThrAlmHighAcknowledge(self, event):
        event.Skip()
        try:
            self.pnData.AlmHighAcknowledge()
        except:
            vtLog.vtLngTB(self.GetName())
