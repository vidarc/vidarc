#Boa:Dialog:vLauncherTreeDialog
#----------------------------------------------------------------------------
# Name:         vLauncherTreeDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherTreeDialog.py,v 1.3 2008/08/10 17:35:45 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton
import wx.lib.buttons

import binascii,tempfile,os
import cStringIO
import wx.tools.img2img as img2img
import wx.tools.img2py as img2py

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.vTools.vLauncher.images as imgLauncher
from vidarc.vTools.vLauncher.vLauncherObj import vLauncherGrp,vLauncherCmd
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

def create(parent):
    return vLauncherTreeDialog(parent)

[wxID_VLAUNCHERTREEDIALOG, wxID_VLAUNCHERTREEDIALOGCBADD, 
 wxID_VLAUNCHERTREEDIALOGCBAPPLY, wxID_VLAUNCHERTREEDIALOGCBCANCEL, 
 wxID_VLAUNCHERTREEDIALOGCBDEL, wxID_VLAUNCHERTREEDIALOGCBDN, 
 wxID_VLAUNCHERTREEDIALOGCBIMAGEBROWSE, wxID_VLAUNCHERTREEDIALOGCBLAUNCH, 
 wxID_VLAUNCHERTREEDIALOGCBUP, wxID_VLAUNCHERTREEDIALOGCHCADD, 
 wxID_VLAUNCHERTREEDIALOGCHCIMAGE, wxID_VLAUNCHERTREEDIALOGCHKAUTOLAUNCH, 
 wxID_VLAUNCHERTREEDIALOGCHKDETACH, wxID_VLAUNCHERTREEDIALOGCHKSAVEPIPES, 
 wxID_VLAUNCHERTREEDIALOGCHKSHELL, wxID_VLAUNCHERTREEDIALOGDBBDN, 
 wxID_VLAUNCHERTREEDIALOGFBBAPPL, wxID_VLAUNCHERTREEDIALOGLBBIMAGE, 
 wxID_VLAUNCHERTREEDIALOGLBLAPPL, wxID_VLAUNCHERTREEDIALOGLBLARG, 
 wxID_VLAUNCHERTREEDIALOGLBLDELAY, wxID_VLAUNCHERTREEDIALOGLBLDN, 
 wxID_VLAUNCHERTREEDIALOGLBLIMG, wxID_VLAUNCHERTREEDIALOGLBLNAME, 
 wxID_VLAUNCHERTREEDIALOGSPNDELAY, wxID_VLAUNCHERTREEDIALOGTXTARG, 
 wxID_VLAUNCHERTREEDIALOGTXTNAME, 
] = [wx.NewId() for _init_ctrls in range(27)]

class vLauncherTreeDialog(wx.Dialog):
    def _init_coll_bxsBt1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsPipes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chkDetach, 0, border=0, flag=0)
        parent.AddWindow(self.chkSavePipes, 0, border=0, flag=0)

    def _init_coll_bxsOpt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chkShell, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chkAutoLaunch, 0, border=0, flag=0)

    def _init_coll_bxsImage_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcImage, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lbbImage, 0, border=8, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbImageBrowse, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsBt2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbUp, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbLaunch, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsAdd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcAdd, 1, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt1, 0, border=0, flag=0)
        parent.AddSizer(self.bxsBt2, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAdd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblName, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtName, 0, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblAppl, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.fbbAppl, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDN, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.dbbDN, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblArg, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtArg, 0, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblDelay, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.spnDelay, 0, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblImg, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsImage, 0, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsOpt, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsPipes, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=6, vgap=4)

        self.bxsBt1 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsOpt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPipes = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAdd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsImage = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt1_Items(self.bxsBt1)
        self._init_coll_bxsBt2_Items(self.bxsBt2)
        self._init_coll_bxsOpt_Items(self.bxsOpt)
        self._init_coll_bxsPipes_Items(self.bxsPipes)
        self._init_coll_bxsAdd_Items(self.bxsAdd)
        self._init_coll_bxsImage_Items(self.bxsImage)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLAUNCHERTREEDIALOG,
              name=u'vLauncherTreeDialog', parent=prnt, pos=wx.Point(154, 154),
              size=wx.Size(400, 297), style=wx.RESIZE_BORDER,
              title=u'vLauncherTreeDialog')
        self.SetClientSize(wx.Size(392, 270))

        self.lblName = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLNAME,
              label=_(u'Name:'), name=u'lblName', parent=self, pos=wx.Point(0,
              68), size=wx.Size(80, 21), style=wx.ALIGN_RIGHT)

        self.txtName = wx.TextCtrl(id=wxID_VLAUNCHERTREEDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(92, 68),
              size=wx.Size(300, 21), style=0, value=u'')

        self.fbbAppl = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a application'), fileMask='*.*',
              id=wxID_VLAUNCHERTREEDIALOGFBBAPPL, labelText=u'', parent=self,
              pos=wx.Point(84, 93), size=wx.Size(308, 29), startDirectory='.',
              style=0)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a directory'),
              id=wxID_VLAUNCHERTREEDIALOGDBBDN, labelText=u'', parent=self,
              pos=wx.Point(84, 126), size=wx.Size(308, 29), startDirectory='.',
              style=0)

        self.lblAppl = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLAPPL,
              label=_(u'Application:'), name=u'lblAppl', parent=self,
              pos=wx.Point(0, 93), size=wx.Size(80, 29), style=wx.ALIGN_RIGHT)

        self.lblDN = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLDN,
              label=_(u'Directory:'), name=u'lblDN', parent=self,
              pos=wx.Point(0, 126), size=wx.Size(80, 29), style=wx.ALIGN_RIGHT)

        self.lblArg = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLARG,
              label=_(u'Arguments:'), name=u'lblArg', parent=self,
              pos=wx.Point(0, 159), size=wx.Size(80, 21), style=wx.ALIGN_RIGHT)

        self.txtArg = wx.TextCtrl(id=wxID_VLAUNCHERTREEDIALOGTXTARG,
              name=u'txtArg', parent=self, pos=wx.Point(92, 159),
              size=wx.Size(300, 21), style=0, value=u'')

        self.lblImg = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLIMG,
              label=_(u'Image:'), name=u'lblImg', parent=self, pos=wx.Point(0,
              209), size=wx.Size(80, 30), style=wx.ALIGN_RIGHT)

        self.lbbImage = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Invisible),
              id=wxID_VLAUNCHERTREEDIALOGLBBIMAGE, name=u'lbbImage',
              parent=self, pos=wx.Point(340, 209), size=wx.Size(24, 24),
              style=wx.NO_3D | wx.NO_BORDER)

        self.chcImage = wx.Choice(choices=[u'---', u' -> '],
              id=wxID_VLAUNCHERTREEDIALOGCHCIMAGE, name=u'chcImage',
              parent=self, pos=wx.Point(92, 209), size=wx.Size(244, 21),
              style=0)
        self.chcImage.Bind(wx.EVT_CHOICE, self.OnChcImageChoice,
              id=wxID_VLAUNCHERTREEDIALOGCHCIMAGE)

        self.cbImageBrowse = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Drop),
              id=wxID_VLAUNCHERTREEDIALOGCBIMAGEBROWSE, name=u'cbImageBrowse',
              parent=self, pos=wx.Point(368, 209), size=wx.Size(24, 24),
              style=0)
        self.cbImageBrowse.Bind(wx.EVT_BUTTON, self.OnCbImageBrowseButton,
              id=wxID_VLAUNCHERTREEDIALOGCBIMAGEBROWSE)

        self.lblDelay = wx.StaticText(id=wxID_VLAUNCHERTREEDIALOGLBLDELAY,
              label=_(u'Delay:'), name=u'lblDelay', parent=self, pos=wx.Point(0,
              184), size=wx.Size(80, 21), style=wx.ALIGN_RIGHT)

        self.spnDelay = wx.SpinCtrl(id=wxID_VLAUNCHERTREEDIALOGSPNDELAY,
              initial=0, max=3600, min=0, name=u'spnDelay', parent=self,
              pos=wx.Point(92, 184), size=wx.Size(300, 21),
              style=wx.SP_ARROW_KEYS)

        self.cbApply = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBAPPLY, name=u'cbApply',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(31, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VLAUNCHERTREEDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBCANCEL, name=u'cbCancel',
              parent=self, pos=wx.Point(35, 0), size=wx.Size(31, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VLAUNCHERTREEDIALOGCBCANCEL)

        self.cbDel = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBDEL, name=u'cbDel', parent=self,
              pos=wx.Point(88, 0), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VLAUNCHERTREEDIALOGCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBUP, name=u'cbUp', parent=self,
              pos=wx.Point(123, 0), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VLAUNCHERTREEDIALOGCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBDN, name=u'cbDn', parent=self,
              pos=wx.Point(158, 0), size=wx.Size(31, 30), style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VLAUNCHERTREEDIALOGCBDN)

        self.chcAdd = wx.Choice(choices=[], id=wxID_VLAUNCHERTREEDIALOGCHCADD,
              name=u'chcAdd', parent=self, pos=wx.Point(92, 34),
              size=wx.Size(265, 21), style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBADD, name=u'cbAdd', parent=self,
              pos=wx.Point(361, 34), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VLAUNCHERTREEDIALOGCBADD)

        self.chkShell = wx.CheckBox(id=wxID_VLAUNCHERTREEDIALOGCHKSHELL,
              label=_(u'shell'), name=u'chkShell', parent=self, pos=wx.Point(84,
              243), size=wx.Size(70, 13), style=0)
        self.chkShell.SetValue(False)

        self.chkAutoLaunch = wx.CheckBox(id=wxID_VLAUNCHERTREEDIALOGCHKAUTOLAUNCH,
              label=_(u'auto launch'), name=u'chkAutoLaunch', parent=self,
              pos=wx.Point(154, 243), size=wx.Size(70, 13), style=0)
        self.chkAutoLaunch.SetValue(False)

        self.chkSavePipes = wx.CheckBox(id=wxID_VLAUNCHERTREEDIALOGCHKSAVEPIPES,
              label=_(u'save pipes'), name=u'chkSavePipes', parent=self,
              pos=wx.Point(154, 260), size=wx.Size(70, 13), style=0)
        self.chkSavePipes.SetValue(False)

        self.chkDetach = wx.CheckBox(id=wxID_VLAUNCHERTREEDIALOGCHKDETACH,
              label=_(u'detach'), name=u'chkDetach', parent=self,
              pos=wx.Point(84, 260), size=wx.Size(70, 13), style=0)
        self.chkDetach.SetValue(True)

        self.cbLaunch = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERTREEDIALOGCBLAUNCH, name=u'cbLaunch',
              parent=self, pos=wx.Point(193, 0), size=wx.Size(31, 30), style=0)
        self.cbLaunch.Bind(wx.EVT_BUTTON, self.OnCbLaunchButton,
              id=wxID_VLAUNCHERTREEDIALOGCBLAUNCH)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.cbApply.SetBitmapLabel(imgLauncher.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(imgLauncher.getCancelBitmap())
        self.cbAdd.SetBitmapLabel(imgLauncher.getAddBitmap())
        self.cbDel.SetBitmapLabel(imgLauncher.getDelBitmap())
        self.cbUp.SetBitmapLabel(imgLauncher.getUpBitmap())
        self.cbDn.SetBitmapLabel(imgLauncher.getDownBitmap())
        self.cbLaunch.SetBitmapLabel(imgLauncher.getLaunchBitmap())
        self.Clear()
        self.fgsMain.Layout()
        self.fgsMain.Fit(self)
        self.oGrp=vLauncherGrp()
        self.oCmd=vLauncherCmd()
        self.__setupImages__()
    def __setupImages__(self):
        ArtIDs = [ "wx.ART_ADD_BOOKMARK",
           "wx.ART_DEL_BOOKMARK",
           "wx.ART_HELP_SIDE_PANEL",
           "wx.ART_HELP_SETTINGS",
           "wx.ART_HELP_BOOK",
           "wx.ART_HELP_FOLDER",
           "wx.ART_HELP_PAGE",
           "wx.ART_GO_BACK",
           "wx.ART_GO_FORWARD",
           "wx.ART_GO_UP",
           "wx.ART_GO_DOWN",
           "wx.ART_GO_TO_PARENT",
           "wx.ART_GO_HOME",
           "wx.ART_FILE_OPEN",
           "wx.ART_FILE_SAVE",
           "wx.ART_FILE_SAVE_AS",
           "wx.ART_PRINT",
           "wx.ART_HELP",
           "wx.ART_TIP",
           "wx.ART_REPORT_VIEW",
           "wx.ART_LIST_VIEW",
           "wx.ART_NEW_DIR",
           "wx.ART_HARDDISK",
           "wx.ART_FLOPPY",
           "wx.ART_CDROM",
           "wx.ART_REMOVABLE",
           "wx.ART_FOLDER",
           "wx.ART_FOLDER_OPEN",
           "wx.ART_GO_DIR_UP",
           "wx.ART_EXECUTABLE_FILE",
           "wx.ART_NORMAL_FILE",
           "wx.ART_TICK_MARK",
           "wx.ART_CROSS_MARK",
           "wx.ART_ERROR",
           "wx.ART_QUESTION",
           "wx.ART_WARNING",
           "wx.ART_INFORMATION",
           "wx.ART_MISSING_IMAGE",
           "wx.ART_COPY",
           "wx.ART_CUT",
           "wx.ART_PASTE",
           "wx.ART_DELETE",
           "wx.ART_NEW",
           "wx.ART_UNDO",
           "wx.ART_REDO",
           "wx.ART_QUIT",
           "wx.ART_FIND",
           "wx.ART_FIND_AND_REPLACE",
           ]
        for s in vtArt.lNames:
            self.chcImage.Append('vtArt.'+s[3:-6])
        for s in ArtIDs:
            self.chcImage.Append(s)
        
    def Clear(self):
        self.oCmd=None
        self.txtName.SetValue(u'')
        self.fbbAppl.SetValue(u'')
        self.dbbDN.SetValue(u'')
        self.txtArg.SetValue(u'')
        #self.txtUsr.SetValue(u'')
        self.chcImage.SetSelection(0)
        self.spnDelay.SetValue(0)
        self.chkShell.SetValue(False)
        self.chkSavePipes.SetValue(False)
        self.chkDetach.SetValue(False)
        self.chkAutoLaunch.SetValue(False)
        self.chcAdd.Clear()
        self.chcAdd.Append(u'---',-1)
        self.chcAdd.Append(u'group',10)
        self.chcAdd.Append(u'command',20)
        wx.CallAfter(self.chcAdd.SetSelection,0)
    def SetData(self,o):
        self.Clear()
        if o is None:
            self.__setImgData__('')
            return
        if isinstance(o,vLauncherCmd):
            self.oCmd=o
            self.txtName.SetValue(o.name)
            self.fbbAppl.SetValue(o.appl)
            self.dbbDN.SetValue(o.dn)
            self.txtArg.SetValue(o.args)
            try:
                if len(o.img)>0:
                    if o.img.startswith('wx.') or o.img.startswith('vtArt.'):
                        self.chcImage.SetStringSelection(o.img)
                    else:
                        self.chcImage.SetSelection(1)
                else:
                    self.chcImage.SetSelection(0)
            except:
                #self.chcImage.SetSelection(0)
                pass
                vtLog.vtLngTB(self.GetName())
            #self.txtUsr.SetValue(o.usr)
            
            try:
                i=int(o.delay)
                self.spnDelay.SetValue(i)
            except:
                self.spnDelay.SetValue(0)
            s=o.opt
            l=s.split(',')
            for s in l:
                if s=='shell':
                    self.chkShell.SetValue(True)
                elif s=='save_pipes':
                    self.chkSavePipes.SetValue(True)
                elif s=='detached':
                    self.chkDetach.SetValue(True)
                elif s=='autolaunch':
                    self.chkAutoLaunch.SetValue(True)
            #self.chcAdd.Append(u'group',10)
            #self.chcAdd.Append(u'command',20)
        elif isinstance(o,vLauncherGrp):
            self.oCmd=o
            self.txtName.SetValue(o.name)
            #self.chcAdd.Append(u'group',10)
            self.chcAdd.Append(u'child group',11)
            #self.chcAdd.Append(u'command',20)
            self.chcAdd.Append(u'child command',21)
        self.__setImgData__(o.img)
    def GetData(self):
        try:
            if self.oCmd is not None:
                o=self.oCmd
                if isinstance(o,vLauncherCmd):
                    o.name=self.txtName.GetValue()
                    o.appl=self.fbbAppl.GetValue()
                    o.dn=self.dbbDN.GetValue()
                    o.args=self.txtArg.GetValue()
                    #o.usr=self.txtUsr.GetValue()
                    if self.chcImage.GetSelection()==0:
                        o.img=u''
                    elif self.chcImage.GetSelection()==1:
                        #o.img=u''
                        pass
                    else:
                        o.img=self.chcImage.GetStringSelection()
                    l=[]
                    if self.chkShell.GetValue():
                        l.append('shell')
                    elif self.chkSavePipes.GetValue():
                        l.append('save_pipes')
                    elif self.chkDetach.GetValue():
                        l.append('detached')
                    elif self.chkAutoLaunch.GetValue():
                        l.append('autolaunch')
                    o.opt=','.join(l)
                    o.CalcInternal()
                elif isinstance(o,vLauncherGrp):
                    o.name=self.txtName.GetValue()
        except:
            vtLog.vtLngTB(self.GetName())
        
    def OnCbAddButton(self, event):
        event.Skip()
        try:
            idxSel=self.chcAdd.GetSelection()
            if idxSel>0:
                oCmd=self.oCmd
                iNr=self.chcAdd.GetClientData(idxSel)
                oPar=None
                if iNr==10:
                    o=vLauncherGrp()
                elif iNr==11:
                    o=vLauncherGrp()
                    oPar=self.oCmd
                elif iNr==20:
                    o=vLauncherCmd()
                elif iNr==21:
                    o=vLauncherCmd()
                    oPar=self.oCmd
                self.oCmd=o
                self.GetData()
                self.SetData(oCmd)
                par=self.GetParent()
                par.AddLaunchEntry(oPar,o)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbLaunchButton(self, event):
        event.Skip()
        try:
            par=self.GetParent()
            par.OnCbLaunchButton(None)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDelButton(self, event):
        event.Skip()
        try:
            if self.oCmd is not None:
                iCount=self.oCmd.GetLaunchCount()
                if iCount==0:
                    o=self.oCmd
                    self.Clear()
                    par=self.GetParent()
                    par.DelLaunchEntry(o)
                else:
                    dlg=vtmMsgDialog(self,_(u'Process still active!'),
                            'vLauncher',wx.OK)
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def move(self,iOfs):
        try:
            if self.oCmd is not None:
                iCount=self.oCmd.GetLaunchCount()
                if iCount==0:
                    o=self.oCmd
                    #self.Clear()
                    par=self.GetParent()
                    par.MoveLaunchEntry(iOfs,o)
                else:
                    dlg=vtmMsgDialog(self,_(u'Process still active!'),
                            'vLauncher',wx.OK)
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbUpButton(self, event):
        event.Skip()
        self.move(-1)
    def OnCbDnButton(self, event):
        event.Skip()
        self.move(1)
    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            self.GetData()
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Show(False)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.SetData(self.oCmd)
        self.Show(False)
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                #doc=par.__getDoc__()
                #node=par.__getNode__()
                #fid=par.__getSelID__()
                #self.trInfo.SelectByID(fid)
                pass
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)

    def OnChcImageChoice(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        if self.chcImage.GetSelection()==0:
            self.lbbImage.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
            self.lbbImage.Refresh()
            return
        elif self.chcImage.GetSelection()==1:
            return
        s=self.chcImage.GetStringSelection()
        self.sImg=s
        if s[:3]=='wx.':
            bmp = wx.ArtProvider_GetBitmap(eval(s), eval('wx.ART_TOOLBAR'), (16,16))
        else:
            bmp=vtArt.getBitmap(eval(s))
        self.lbbImage.SetBitmapLabel(bmp)
        self.lbbImage.Refresh()
        #self.cbImageBrowse.SetBitmapLabel(bmp)
        #self.cbImageBrowse.Refresh()

    def OnCbImageBrowseButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        dlg = wx.FileDialog(self, _("Import Image"), ".", "", _(u"PNG files (*.png)|*.png|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                sFN = dlg.GetPath()
                self.AddFile(sFN)
            return
        finally:
            dlg.Destroy()
    def __setImgData__(self,sImg):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if len(sImg)!=0:
                if sImg[:3]=='wx.':
                    bmp = wx.ArtProvider_GetBitmap(eval(sImg), eval('wx.ART_TOOLBAR'), (16,16))
                elif sImg[:6]=='vtArt.':
                    bmp=vtArt.getBitmap(eval(sImg))
                else:
                    stream = cStringIO.StringIO(binascii.unhexlify(sImg))
                    bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
                self.lbbImage.SetBitmapLabel(bmp)
            else:
                self.lbbImage.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
        except:
            vtLog.vtLngTB(self.GetName())
            self.lbbImage.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
        self.lbbImage.Refresh()
    def AddFile(self,sFullFN):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if self.oCmd is not None:
                compressed = 0
                maskClr = None
                tfname = tempfile.mktemp()
                ok, msg = img2img.convert(sFullFN, maskClr, None, tfname, wx.BITMAP_TYPE_PNG, ".png")
                if not ok:
                    vtLog.vtLngCurWX(self.ERROR,msg,self)
                data = open(tfname, "rb").read()
                sImg=binascii.hexlify(data)
                self.oCmd.img=sImg
                os.unlink(tfname)
                self.__setImgData__(sImg)
                self.chcImage.SetSelection(1)
        except:
            vtLog.vtLngTB(self.GetName())
            self.sImg=''

    
