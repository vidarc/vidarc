#Boa:Dialog:vLauncherSpecialKeysDialog

import wx
import wx.lib.buttons
import vidarc.tool.misc.vtmListCtrl

import sys
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

def create(parent):
    return vLauncherSpecialKeysDialog(parent)

[wxID_VLAUNCHERSPECIALKEYSDIALOG, wxID_VLAUNCHERSPECIALKEYSDIALOGCBCLOSE, 
 wxID_VLAUNCHERSPECIALKEYSDIALOGCBSEND, 
 wxID_VLAUNCHERSPECIALKEYSDIALOGLSTKEYS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vLauncherSpecialKeysDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSend, 0, border=0, flag=0)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstKeys, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=0)
        parent.AddWindow(self.cbClose, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstKeys_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=u'Key Combination', width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'ASCII',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=2, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLAUNCHERSPECIALKEYSDIALOG,
              name=u'vLauncherSpecialKeysDialog', parent=prnt, pos=wx.Point(344,
              72), size=wx.Size(400, 250), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLauncherSpecialKeysDialog')
        self.SetClientSize(wx.Size(392, 223))

        self.lstKeys = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VLAUNCHERSPECIALKEYSDIALOGLSTKEYS,
              name=u'lstKeys', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(357, 189), style=wx.LC_REPORT)
        self._init_coll_lstKeys_Columns(self.lstKeys)
        self.lstKeys.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstKeysListItemSelected,
              id=wxID_VLAUNCHERSPECIALKEYSDIALOGLSTKEYS)
        self.lstKeys.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstKeysListItemDeselected,
              id=wxID_VLAUNCHERSPECIALKEYSDIALOGLSTKEYS)

        self.cbSend = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERSPECIALKEYSDIALOGCBSEND,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbSend', parent=self,
              pos=wx.Point(361, 0), size=wx.Size(31, 30), style=0)
        self.cbSend.Bind(wx.EVT_BUTTON, self.OnSendButton,
              id=wxID_VLAUNCHERSPECIALKEYSDIALOGCBSEND)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLAUNCHERSPECIALKEYSDIALOGCBCLOSE,
              bitmap=vtArt.getBitmap(vtArt.Close), label=_(u'Close'),
              name=u'cbClose', parent=self, pos=wx.Point(114, 193),
              size=wx.Size(128, 30), style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VLAUNCHERSPECIALKEYSDIALOGCBCLOSE)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.lstKeys.SetStretchLst([(0,-1.0),(1,-0.5)])
        for s0,s1 in [('Ctrl-A','SOH'),('Ctrl-B','STX'),('Ctrl-C','ETX'),
                ('Ctrl-D','EOT'),('Ctrl-E','ENQ'),('Ctrl-F','ACK'),
                ('Ctrl-G','BEL'),('Ctrl-H','BS'),('Ctrl-I','HT'),
                ('Ctrl-J','LF'),('Ctrl-K','VT'),('Ctrl-L','FF'),
                ('Ctrl-M','CR'),('Ctrl-N','SO'),
                ('Ctrl-O','SI'),('Ctrl-P','DLE'),('Ctrl-Q','DC1'),
                ('Ctrl-R','DC2'),('Ctrl-S','DC3'),('Ctrl-T','DC4'),
                ('Ctrl-U','NAK'),('Ctrl-V','SYN'),('Ctrl-W','ETB'),
                ('Ctrl-X','CAN'),('Ctrl-Y','EM'),('Ctrl-Z','SUB'),]:
            idx=self.lstKeys.InsertStringItem(sys.maxint,s0)
            self.lstKeys.SetStringItem(idx,1,s1,-1)
        self.idxSel=-1
    def OnSendButton(self, event):
        event.Skip()
        try:
            if self.idxSel>=0:
                par=self.GetParent()
                s=chr(self.idxSel+1)
                par.WriteStdIn(s,False,False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCloseButton(self, event):
        event.Skip()
        self.EndModal(0)

    def OnLstKeysListItemSelected(self, event):
        event.Skip()
        try:
            self.idxSel=event.GetIndex()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLstKeysListItemDeselected(self, event):
        event.Skip()
        self.idxSel=-1
