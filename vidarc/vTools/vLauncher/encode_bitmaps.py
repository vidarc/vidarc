#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: encode_bitmaps.py,v 1.3 2009/01/13 10:29:18 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/vLaunch01_16.ico            images.py",
    "-a -u -n Plugin        img/vLaunch01_16.png            images.py",
    "-a -u -n Launch        img/vLaunch01_16.png            images.py",
    "-a -u -n Calendar      img/Calendar01_16.ico           images.py",
    
    "-a -u -n ToDay         img/ToDay01_16.png              images.py",
    "-a -u -n Store         img/Input01_16.png              images.py",
    "-a -u -n Shell         img/Shell01_16.png              images.py",
    "-a -u -n ShellPipes    img/ShellOutput01_16.png        images.py",
    "-a -u -n Enter         img/Enter01_16.png              images.py",
    "-a -u -n EnterAdd      img/EnterAdd01_16.png           images.py",
    
    #"-a -u -n ToDay         img/ToDay01_16.png              images.py",
    #"-a -u -n Store         img/Input01_16.png              images.py",
    "-a -u -n Apply         img/ok.png                      images.py",
    "-a -u -n Add           img/add.png                     images.py",
    "-a -u -n Cancel        img/abort.png                   images.py",
    "-a -u -n Del           img/waste.png                   images.py",
    
    "-a -u -n Host          img/Client03_16.png             images.py",
    "-a -u -n Usr           img/Usr02_16.png                images.py",
    "-a -u -n Down          img/Down08_16.png               images.py",
    "-a -u -n DownDisable   img/Down07_16.png               images.py",
    "-a -u -n Up            img/Up08_16.png                 images.py",
    "-a -u -n UpDisable     img/Up07_16.png                 images.py",
    "-a -u -n Open          img/Open01_16.png               images.py",
    "-a -u -n Close         img/Close01_16.png              images.py",
    "-a -u -n Browse        img/BrowseFile01_16.png         images.py",
    "-a -u -n Synch         img/Recycle01_16.png            images.py",
    "-a -u -n Collapse      img/Collapse01_16.png           images.py",
    "-a -u -n Expand        img/Expand01_16.png             images.py",
    
    "-a -u -n CollapseBig   img/Collapse01_32.png           images.py",
    "-a -u -n ExpandBig     img/Expand01_32.png             images.py",
    "-a -u -n LaunchBig     img/vLaunch01_32.png            images.py",
    "-a -u -n DropTargetBig img/DropTarget01_32.png         images.py",
    "-a -u -n SettingsBig   img/Settings02_32.png           images.py",
    
    "-a -u -n PosLT         img/vLaunchPosLT_16.png         images.py",
    "-a -u -n PosRT         img/vLaunchPosRT_16.png         images.py",
    "-a -u -n PosLB         img/vLaunchPosLB_16.png         images.py",
    "-a -u -n PosRB         img/vLaunchPosRB_16.png         images.py",
    "-a -u -n Invisible     img/Invisible.png               images.py",
    "-a -u -n Grouping      img/TreeGrouping07b_16.png      images.py",
    
    "-a -u -n AlmNone       img/AlarmLampNone01_16.png      images.py",
    "-a -u -n AlmHigh       img/AlarmLampHigh01_16.png      images.py",
    "-a -u -n AlmHigh       img/AlarmLampHigh01_16.png      images.py",
    "-a -u -n AlmMed        img/AlarmLampMed01_16.png       images.py",
    "-a -u -n AlmLow        img/AlarmLampLow01_16.png       images.py",
    
    "-a -u -n Aborted       img/StateAborted01_16.ico       images.py",
    "-a -u -n Running       img/StateRunning01_16.ico       images.py",
    "-a -u -n Stopped       img/StateStopped01_16.ico       images.py",
    "-a -u -n Error         img/Error01_16.ico              images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

