#Boa:Frame:vLauncherPipesFrame
#----------------------------------------------------------------------------
# Name:         vLauncherPipesFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherPipesFrame.py,v 1.4 2009/01/13 11:40:51 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmListCtrl
import wx.lib.buttons

import sys
try:
    import vidarc.tool.log.vtLog as vtLog
    
    import vidarc.vTools.vLauncher.images as imgLauncher
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    from vidarc.tool.gui.vtgCore import vtGuiCoreLimitWindowToScreen
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    from vidarc.tool.misc.vtmListCtrlPopupCtrlAttached import vtmListCtrlPopupCtrlAttached
    
    from vidarc.vTools.vLauncher.vLauncherInputDialog import vLauncherInputDialog
    from vidarc.vTools.vLauncher.vLauncherSpecialKeysDialog import vLauncherSpecialKeysDialog
except:
    vtLog.vtLngTB('import')

def getIcon(bmp):
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon

ICON_ABORTED=None
ICON_RUNNING=None
ICON_STOPPED=None
ICON_ERROR=None

def getIconByName(s):
    global ICON_ABORTED
    global ICON_RUNNING
    global ICON_STOPPED
    global ICON_ERROR
    if ICON_ABORTED is None:
        ICON_ABORTED=getIcon(imgLauncher.getAbortedBitmap())
        ICON_RUNNING=getIcon(imgLauncher.getRunningBitmap())
        ICON_STOPPED=getIcon(imgLauncher.getStoppedBitmap())
        ICON_ERROR=getIcon(imgLauncher.getErrorBitmap())
        
    if s=='ICON_ABORTED':
        return ICON_ABORTED
    if s=='ICON_RUNNING':
        return ICON_RUNNING
    if s=='ICON_STOPPED':
        return ICON_STOPPED
    if s=='ICON_ERROR':
        return ICON_ERROR
    return ICON_ERROR


def create(parent):
    return vLauncherPipesFrame(parent)

[wxID_VLAUNCHERPIPESFRAME, wxID_VLAUNCHERPIPESFRAMECBCLR, 
 wxID_VLAUNCHERPIPESFRAMECBENTER, wxID_VLAUNCHERPIPESFRAMECBENTERADD, 
 wxID_VLAUNCHERPIPESFRAMECBKEYBOARD, wxID_VLAUNCHERPIPESFRAMECBPOPUP, 
 wxID_VLAUNCHERPIPESFRAMECBSAVE, wxID_VLAUNCHERPIPESFRAMECHCSCROLL, 
 wxID_VLAUNCHERPIPESFRAMECHCWRAP, wxID_VLAUNCHERPIPESFRAMELBBSTATE, 
 wxID_VLAUNCHERPIPESFRAMELSTPIPES, wxID_VLAUNCHERPIPESFRAMEPNMAIN, 
 wxID_VLAUNCHERPIPESFRAMESPSZ, wxID_VLAUNCHERPIPESFRAMETXTIN, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vLauncherPipesFrame(wx.Frame,vtLog.vtLogOrigin):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbClr, 0, border=8, flag=0)
        parent.AddWindow(self.cbSave, 0, border=7, flag=wx.TOP)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsIn, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbPopup, 0, border=0, flag=0)
        parent.AddWindow(self.lstPipes, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=0)
        parent.AddSizer(self.bxsOption, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lbbState, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_bxsOption_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcWrap, 0, border=4, flag=0)
        parent.AddWindow(self.chcScroll, 0, border=0, flag=0)
        parent.AddWindow(self.spSz, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsIn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtIn, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbKeyBoard, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbEnter, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbEnterAdd, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_lstPipes_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=3, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsOption = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsIn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsOption_Items(self.bxsOption)
        self._init_coll_bxsIn_Items(self.bxsIn)

        self.pnMain.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VLAUNCHERPIPESFRAME,
              name=u'vLauncherPipesFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(290, 210), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vLauncherPipesFrame')
        self.SetClientSize(wx.Size(282, 183))
        self.Bind(wx.EVT_MOVE, self.OnVLauncherPipesFrameMove)
        self.Bind(wx.EVT_SIZE, self.OnVLauncherPipesFrameSize)
        self.Bind(wx.EVT_CLOSE, self.OnVLauncherPipesFrameClose)

        self.pnMain = wx.Panel(id=wxID_VLAUNCHERPIPESFRAMEPNMAIN,
              name=u'pnMain', parent=self, pos=wx.Point(0, 0), size=wx.Size(282,
              183), style=wx.TAB_TRAVERSAL)

        self.txtIn = wx.TextCtrl(id=wxID_VLAUNCHERPIPESFRAMETXTIN,
              name=u'txtIn', parent=self.pnMain, pos=wx.Point(0, 0),
              size=wx.Size(142, 30), style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtIn.Bind(wx.EVT_TEXT_ENTER, self.OnTxtInTextEnter,
              id=wxID_VLAUNCHERPIPESFRAMETXTIN)
        self.txtIn.Bind(wx.EVT_CHAR, self.OnTxtInChar)

        self.cbEnter = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERPIPESFRAMECBENTER, name=u'cbEnter',
              parent=self.pnMain, pos=wx.Point(181, 0), size=wx.Size(31, 30),
              style=0)
        self.cbEnter.Bind(wx.EVT_BUTTON, self.OnCbEnterButton,
              id=wxID_VLAUNCHERPIPESFRAMECBENTER)

        self.cbEnterAdd = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERPIPESFRAMECBENTERADD, name=u'cbEnterAdd',
              parent=self.pnMain, pos=wx.Point(216, 0), size=wx.Size(31, 30),
              style=0)
        self.cbEnterAdd.Bind(wx.EVT_BUTTON, self.OnCbEnterAddButton,
              id=wxID_VLAUNCHERPIPESFRAMECBENTERADD)

        self.cbClr = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VLAUNCHERPIPESFRAMECBCLR, name=u'cbClr',
              parent=self.pnMain, pos=wx.Point(251, 34), size=wx.Size(31, 30),
              style=0)
        self.cbClr.Bind(wx.EVT_BUTTON, self.OnCbClrButton,
              id=wxID_VLAUNCHERPIPESFRAMECBCLR)

        self.cbSave = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VLAUNCHERPIPESFRAMECBSAVE, name=u'cbSave',
              parent=self.pnMain, pos=wx.Point(251, 71), size=wx.Size(31, 30),
              style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VLAUNCHERPIPESFRAMECBSAVE)

        self.spSz = wx.SpinCtrl(id=wxID_VLAUNCHERPIPESFRAMESPSZ, initial=0,
              max=100, min=0, name=u'spSz', parent=self.pnMain,
              pos=wx.Point(144, 161), size=wx.Size(86, 22),
              style=wx.SP_ARROW_KEYS)
        self.spSz.Bind(wx.EVT_TEXT_ENTER, self.OnSpSzTextEnter,
              id=wxID_VLAUNCHERPIPESFRAMESPSZ)
        self.spSz.Bind(wx.EVT_SPIN, self.OnSpSzSpin,
              id=wxID_VLAUNCHERPIPESFRAMESPSZ)

        self.chcWrap = wx.CheckBox(id=wxID_VLAUNCHERPIPESFRAMECHCWRAP,
              label=_(u'wrap'), name=u'chcWrap', parent=self.pnMain,
              pos=wx.Point(0, 161), size=wx.Size(70, 13), style=0)
        self.chcWrap.SetValue(True)

        self.lbbState = wx.StaticBitmap(bitmap=vtArtState.getBitmap(vtArtState.Stopped),
              id=wxID_VLAUNCHERPIPESFRAMELBBSTATE, name=u'lbbState',
              parent=self.pnMain, pos=wx.Point(258, 164), size=wx.Size(16, 16),
              style=0)

        self.lstPipes = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VLAUNCHERPIPESFRAMELSTPIPES,
              name=u'lstPipes', parent=self.pnMain, pos=wx.Point(0, 34),
              size=wx.Size(247, 123), style=wx.LC_REPORT)
        self.lstPipes.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL, False,
              u'Courier New'))
        self._init_coll_lstPipes_Columns(self.lstPipes)

        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Down),
              id=wxID_VLAUNCHERPIPESFRAMECBPOPUP, name=u'cbPopup',
              parent=self.pnMain, pos=wx.Point(251, 0), size=wx.Size(31, 30),
              style=0)
        self.cbPopup.Bind(wx.EVT_BUTTON, self.OnCbPopupButton,
              id=wxID_VLAUNCHERPIPESFRAMECBPOPUP)

        self.chcScroll = wx.CheckBox(id=wxID_VLAUNCHERPIPESFRAMECHCSCROLL,
              label=_(u'scroll'), name=u'chcScroll', parent=self.pnMain,
              pos=wx.Point(70, 161), size=wx.Size(70, 13), style=0)
        self.chcScroll.SetValue(True)

        self.cbKeyBoard = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.LifeBelt),
              id=wxID_VLAUNCHERPIPESFRAMECBKEYBOARD, name=u'cbKeyBoard',
              parent=self.pnMain, pos=wx.Point(146, 0), size=wx.Size(31, 30),
              style=0)
        self.cbKeyBoard.Bind(wx.EVT_BUTTON, self.OnCbKeyBoardButton,
              id=wxID_VLAUNCHERPIPESFRAMECBKEYBOARD)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        # ++++++++++++++
        # add stuff here
        vtLog.vtLogOrigin.__init__(self,origin=self.GetName())
        
        self.cbEnter.SetBitmapLabel(imgLauncher.getEnterBitmap())
        self.cbEnterAdd.SetBitmapLabel(imgLauncher.getEnterAddBitmap())
        
        self.imgDict={}
        self.imgLst=wx.ImageList(16,16)
        self.imgDict['stdin']=self.imgLst.Add(vtArt.getBitmap(vtArt.ExportFree))
        self.imgDict['stdout']=self.imgLst.Add(vtArt.getBitmap(vtArt.ImportFree))
        self.imgDict['stderr']=self.imgLst.Add(vtArt.getBitmap(vtArt.Error))
        self.lstPipes.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.lstPipes.SetColumnWidth(2,30)
        self.lstPipes.SetStretchLst([(1,-1.0)])
        
        self.spSz.SetValue(100)
        
        self.popWin=None
        self.popComplete=None
        self.bBusy=False
        self.cmd=None
        self.proc=None
        self.lIdx=[sys.maxint,sys.maxint,sys.maxint]
        self.iOfsLast=-1
        # add stuff here
        # --------------
    def SetCmd(self,cmd):
        l=cmd.GetGeometry()
        if l is not None:
            x,y,w,h=vtGuiCoreLimitWindowToScreen(l[0],l[1],l[2],l[3])
            self.Move((x,y))
            self.SetSize((w,h))
        self.cmd=cmd
        if self.cmd.lHist is not None:
            self.__setPopupState__(True)
            self.OnCbPopupButton(None)
        try:
            i=int(self.cmd.recent_size)
        except:
            self.cmd.recent_size='100'
            i=100
        self.spSz.SetValue(i)
    def SetProc(self,proc):
        self.proc=proc
    def SetState(self,state):
        if state=='running':
            o=vtArtState.Running
            icon=getIconByName('ICON_RUNNING')
        elif state=='stopped':
            o=vtArtState.Stopped
            icon=getIconByName('ICON_STOPPED')
            #self.lbbState.SetBitmap(vtArtState.getBitmap(vtArtState.Stopped))
        elif state=='aborted':
            o=vtArtState.Aborted
            icon=getIconByName('ICON_ABORTED')
            #self.lbbState.SetBitmap(vtArtState.getBitmap(vtArtState.Aborted))
        self.lbbState.SetBitmap(vtArtState.getBitmap(o))
        self.lbbState.Refresh()
        
        #icon = vtArtState.getIcon(vtArtState.getBitmap(o))
        self.SetIcon(icon)
        if self.proc is not None:
            par=self.GetParent()
            par.SetState(state,self.proc.tiSel)
    def __add2Lst__(self,iOfs,s):
        if len(s)==0:
            return
        if iOfs==0:
            img=self.imgDict['stdin']
        elif iOfs==1:
            img=self.imgDict['stdout']
        elif iOfs==2:
            img=self.imgDict['stderr']
        iIdx=self.lIdx[iOfs]
        iMax=sys.maxint
        if self.iOfsLast!=iOfs:
            iIdx=iMax
        self.iOfsLast=iOfs
        l=s.split('\r')
        sCur=l[0]
        if iIdx==iMax:
            iIdx=self.lstPipes.InsertImageItem(iMax,img)
        else:
            sCur=self.lstPipes.GetItem(iIdx,1).m_text+sCur
        bLF=False
        for s in l[1:]:
            iLC=len(sCur)
            iLA=len(s)
            if iLA==1:
                if s=='\n':
                    bLF=True
            if bLF==False:
                if iLA>=iLC:
                    sCur=s
                else:
                    sCur=s+sCur[iLA:]
        if self.chcScroll.GetValue():
            self.lstPipes.EnsureVisible(iIdx)
        s=[]
        for c in sCur:
            iC=ord(c)
            if iC<27 and iC!=10:
                s.append('^%c'%(64+ord(c)))
            else:
                s.append(c)
        s=''.join(s)
        if len(s)!=len(sCur):
            sCur=s
        
        if len(sCur)>0 and sCur[-1]=='\n':
            self.lstPipes.SetStringItem(iIdx,1,sCur[:-1],-1)
            iIdx=iMax
        else:
            self.lstPipes.SetStringItem(iIdx,1,sCur,-1)
            if bLF:
                iIdx=iMax
        self.lIdx[iOfs]=iIdx
    def WriteStdIn(self,s,bLineSep=False,bEnter=True):
        if self.cmd is not None:
            if bLineSep or bEnter:
                if len(s)>0:
                    self.cmd.AddRecent(s)
        self.proc.WriteStdIn(s,bLineSep=bLineSep,bEnter=bEnter)
        self.__add2Lst__(0,s)
    def AddStdOut(self,s):
        try:
            self.__add2Lst__(1,s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def AddStdErr(self,s):
        try:
            self.__add2Lst__(2,s)
            
            self.SetIcon(getIconByName('ICON_ERROR'))
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vLauncherInputDialog(self,title=self.GetTitle())#,sz,wx.SIMPLE_BORDER)
                #self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                #self.popWin.SetNodeTree(self.nodeTree)
                #if self.trSetNode is not None:
                #    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
            self.popWin.SetData(self.cmd.lHist)
        else:
            pass
    def OnCbPopupButton(self, event):
        if event is not None:
            event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
    def OnCbEnterButton(self, event):
        event.Skip()
        try:
            s=self.txtIn.GetValue()
            self.WriteStdIn(s)
            self.txtIn.SetValue(u'')
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbClrButton(self, event):
        event.Skip()
        try:
            self.lstPipes.DeleteAllItems()
            if self.proc is None:
                self.SetIcon(getIconByName('ICON_STOPPED'))
            else:
                if self.proc.IsRunning():
                    self.SetIcon(getIconByName('ICON_RUNNING'))
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbSaveButton(self, event):
        event.Skip()

    def OnTxtInTextEnter(self, event):
        event.Skip()
        try:
            s=self.txtIn.GetValue()
            self.WriteStdIn(s)
            self.txtIn.SetValue(u'')
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def HistDel(self,idxSel):
        if self.cmd is not None:
            del self.cmd.lHist[idxSel]
    def HistUp(self,idxSel):
        if self.cmd is not None:
            l=self.cmd.lHist
            ll=l[:idxSel-1]+[l[idxSel],l[idxSel-1]]+l[idxSel+1:]
            self.cmd.lHist=ll
    def HistDown(self,idxSel):
        if self.cmd is not None:
            l=self.cmd.lHist
            ll=l[:idxSel]+[l[idxSel+1],l[idxSel]]+l[idxSel+2:]
            self.cmd.lHist=ll
    def OnCbEnterAddButton(self, event):
        event.Skip()
        try:
            self.__createPopup__()
            s=self.txtIn.GetValue()
            self.popWin.Add(s)
            self.WriteStdIn(s)
            self.txtIn.SetValue(u'')
            if self.cmd.lHist is None:
                self.cmd.lHist=[]
            self.cmd.lHist.append(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnVLauncherPipesFrameMove(self, event):
        event.Skip()
        if self.cmd is not None:
            pos=self.GetPosition()
            size=self.GetSize()
            self.cmd.SetGeometry(pos[0],pos[1],size[0],size[1])
    def OnVLauncherPipesFrameSize(self, event):
        event.Skip()
        if self.cmd is not None:
            pos=self.GetPosition()
            size=self.GetSize()
            self.cmd.SetGeometry(pos[0],pos[1],size[0],size[1])

    def OnVLauncherPipesFrameClose(self, event):
        try:
            if self.proc is not None:
                if self.proc.IsRunning():
                    dlg=vtmMsgDialog(self,_(u'Process still active!'),
                            'vLauncher',wx.OK)
                    dlg.ShowModal()
                    dlg.Destroy()
                    event.Veto()
                    return
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if self.proc is not None:
            par=self.GetParent()
            par.DelProc(self.proc.tiSel)
        event.Skip()

    def OnCbKeyBoardButton(self, event):
        event.Skip()
        try:
            dlg=vLauncherSpecialKeysDialog(self)
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __dataPopupComplete__(self):
        if self.cmd is not None:
            l=self.cmd.GetRecent(self.txtIn.GetValue())
            l.reverse()
            return l
        return []
    def __setPopupComplete__(self,i,s):
        if i>=0:
            self.txtIn.SetValue(s)
            self.txtIn.SetInsertionPointEnd()
    def __createPopupComplete__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popComplete is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popComplete=vtmListCtrlPopupCtrlAttached(self,
                        self.__dataPopupComplete__)
                self.popComplete.SetCB(self.__setPopupComplete__)
            except:
                vtLog.vtLngTB(self.GetName())
        else:
            pass
    def __showPopupComplete__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopupComplete__()
        
        if 1==1:
            self.popComplete.Attach(self.txtIn)
            #self.popComplete.SetSize((iW,iH))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popComplete.Show(True)
            #self.popComplete.Popup()
        else:
            self.popComplete.Show(False)
    def OnTxtInChar(self, event):
        iKey=event.GetKeyCode()
        if iKey in [wx.WXK_RIGHT,wx.WXK_NUMPAD_RIGHT]:
            #w=event.GetEventObject()
            #pos=w.ClientToScreen((0,0))
            #sz=w.GetSize()
            if self.txtIn.GetInsertionPoint()==self.txtIn.GetLastPosition():
                self.__showPopupComplete__()#(pos,sz)
            else:
                event.Skip()
        else:
            event.Skip()

    def OnSpSzTextEnter(self, event):
        event.Skip()
        try:
            if self.cmd is not None:
                self.cmd.recent_size=str(self.spSz.GetValue())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSpSzSpin(self, event):
        event.Skip()
        try:
            if self.cmd is not None:
                self.cmd.recent_size=str(self.spSz.GetValue())
        except:
            vtLog.vtLngTB(self.GetName())
