#Boa:Dialog:vLauncherInputDialog
#----------------------------------------------------------------------------
# Name:         vLauncherMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherInputDialog.py,v 1.2 2009/01/13 10:32:01 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.misc.vtmListCtrl

import sys
try:
    import vidarc.tool.log.vtLog as vtLog
    
    import vidarc.tool.art.vtArt as vtArt
except:
    vtLog.vtLngTB('import')

def create(parent):
    return vLauncherInputDialog(parent)

[wxID_VLAUNCHERINPUTDIALOG, wxID_VLAUNCHERINPUTDIALOGCBAPPLY, 
 wxID_VLAUNCHERINPUTDIALOGCBCANCEL, wxID_VLAUNCHERINPUTDIALOGCBDEL, 
 wxID_VLAUNCHERINPUTDIALOGCBDN, wxID_VLAUNCHERINPUTDIALOGCBSEND, 
 wxID_VLAUNCHERINPUTDIALOGCBUP, wxID_VLAUNCHERINPUTDIALOGLSTINPUT, 
 wxID_VLAUNCHERINPUTDIALOGTGSEND, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vLauncherInputDialog(wx.Dialog,vtLog.vtLogOrigin):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSend, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.tgSend, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsBtDlg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBtDlg, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstInput, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=0)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_lstInput_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'Input',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBtDlg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsBtDlg_Items(self.bxsBtDlg)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLAUNCHERINPUTDIALOG,
              name=u'vLauncherInputDialog', parent=prnt, pos=wx.Point(336, 168),
              size=wx.Size(400, 250), style=wx.RESIZE_BORDER,
              title=u'vLauncherInputDialog')
        self.SetClientSize(wx.Size(392, 223))

        self.lstInput = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VLAUNCHERINPUTDIALOGLSTINPUT,
              name=u'lstInput', parent=self, pos=wx.Point(0, 30),
              size=wx.Size(357, 193), style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
        self._init_coll_lstInput_Columns(self.lstInput)
        self.lstInput.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInputListItemSelected,
              id=wxID_VLAUNCHERINPUTDIALOGLSTINPUT)
        self.lstInput.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstInputListItemDeselected,
              id=wxID_VLAUNCHERINPUTDIALOGLSTINPUT)
        self.lstInput.Bind(wx.EVT_LEFT_DCLICK, self.OnLstInputLeftDclick)

        self.cbSend = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBSEND,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbSend', parent=self,
              pos=wx.Point(361, 30), size=wx.Size(31, 30), style=0)
        self.cbSend.Bind(wx.EVT_BUTTON, self.OnCbSendButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBSEND)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBDEL,
              bitmap=vtArt.getBitmap(vtArt.DelAttr), name=u'cbDel', parent=self,
              pos=wx.Point(361, 110), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self,
              pos=wx.Point(361, 148), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBDN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDn', parent=self,
              pos=wx.Point(361, 182), size=wx.Size(31, 30), style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBDN)

        self.tgSend = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VLAUNCHERINPUTDIALOGTGSEND,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'tgSend', parent=self,
              pos=wx.Point(361, 68), size=wx.Size(31, 30), style=0)
        self.tgSend.SetValue(True)
        self.tgSend.Bind(wx.EVT_BUTTON, self.OnTgSendButton,
              id=wxID_VLAUNCHERINPUTDIALOGTGSEND)

        self.cbApply = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(31, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapButton(ID=wxID_VLAUNCHERINPUTDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=wx.Point(35, 0), size=wx.Size(31, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VLAUNCHERINPUTDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent,title=None):
        self._init_ctrls(parent)
        vtLog.vtLogOrigin.__init__(self,origin=self.GetName())
        self.lstInput.SetStretchLst([(0,-1.0)])
        if title is not None:
            self.SetTitle(title)
        self.Clear()
    def Clear(self):
        self.lstInput.DeleteAllItems()
        self.idxSel=-1
    def SetData(self,lHist):
        self.Clear()
        if lHist is not None:
            for s in lHist:
                self.lstInput.InsertStringItem(sys.maxint,s)
    def Add(self,s):
        self.lstInput.InsertStringItem(sys.maxint,s)
    def OnCbDelButton(self, event):
        event.Skip()
        try:
            if self.idxSel>=0:
                par=self.GetParent()
                idxSel=self.idxSel
                iCount=self.lstInput.GetItemCount()-1
                par.HistDel(idxSel)
                self.lstInput.DeleteItem(idxSel)
                if idxSel>=iCount:
                    idxSel=idxSel-1
                if idxSel>=0:
                    self.lstInput.SetItemState(idxSel,
                                wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbUpButton(self, event):
        event.Skip()
        try:
            if self.idxSel>0:
                par=self.GetParent()
                idxSel=self.idxSel
                par.HistUp(idxSel)
                s=self.lstInput.GetItemText(idxSel)
                self.lstInput.DeleteItem(idxSel)
                idxSel=idxSel-1
                self.lstInput.InsertStringItem(idxSel,s)
                self.lstInput.SetItemState(idxSel,
                        wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbDnButton(self, event):
        event.Skip()
        try:
            iCount=self.lstInput.GetItemCount()
            if (self.idxSel>=0) and (self.idxSel<(iCount-1)):
                par=self.GetParent()
                idxSel=self.idxSel
                par.HistDown(idxSel)
                s=self.lstInput.GetItemText(idxSel)
                self.lstInput.DeleteItem(idxSel)
                idxSel=idxSel+1
                self.lstInput.InsertStringItem(idxSel,s)
                self.lstInput.SetItemState(idxSel,
                        wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnTgSendButton(self, event):
        event.Skip()
    def OnCbApplyButton(self, event):
        event.Skip()
        self.Show(False)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.Show(False)
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                #doc=par.__getDoc__()
                #node=par.__getNode__()
                #fid=par.__getSelID__()
                #self.trInfo.SelectByID(fid)
                pass
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)

    def OnLstInputListItemSelected(self, event):
        event.Skip()
        try:
            idx=event.GetIndex()
            self.idxSel=idx
            if self.tgSend.GetValue():
                #ti=event.GetItem()
                s=self.lstInput.GetItemText(idx)
                par=self.GetParent()
                par.WriteStdIn(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnLstInputListItemDeselected(self, event):
        event.Skip()
        try:
            self.idxSel=-1
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnLstInputLeftDclick(self, event):
        event.Skip()
        try:
            if self.idxSel>=0:
                s=self.lstInput.GetItemText(self.idxSel)
                par=self.GetParent()
                par.WriteStdIn(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbSendButton(self, event):
        event.Skip()
        try:
            if self.idxSel>=0:
                s=self.lstInput.GetItemText(self.idxSel)
                par=self.GetParent()
                par.WriteStdIn(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
