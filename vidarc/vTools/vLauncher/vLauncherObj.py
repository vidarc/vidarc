#----------------------------------------------------------------------------
# Name:         vLauncherObj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherObj.py,v 1.7 2011/05/30 15:29:17 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os
import subprocess
import platform
WIN=0
POSIX=0
if platform.system()=='Windows':
    WIN=1

try:
    import vidarc.tool.log.vtLog as vtLog
    from vidarc.tool.vtThread import vtThread
    from vidarc.tool.vtProcess import vtProcess
    from vidarc.vTools.vLauncher.vLauncherPipesFrame import vLauncherPipesFrame
except:
    vtLog.vtLngTB('import')

class vLauncherGrpIter:
    def __init__(self,o):
        self.o=o
        self.idx=0
        self.len=o.len()
    def next(self):
        if self.idx>=self.len:
            raise StopIteration
        try:
            o=self.o[self.idx]
            self.idx+=1
            return o
        except:
            raise StopIteration

class vLauncherGrp:
    def __init__(self,name=None,img=''):
        self.name=name
        self.img=img
        self.lC=[]
        self.ti=None
    def append(self,o):
        self.lC.append(o)
    def remove(self,o):
        iIdx=self.lC.index(o)
        del self.lC[iIdx]
    def move(self,i,o):
        iIdx=self.lC.index(o)
        iLen=len(self.lC)
        o=self.lC[iIdx]
        if i>0:
            if iIdx<(iLen-1):
                del self.lC[iIdx]
                self.lC.insert(iIdx+1,o)
                return True
        elif i<0:
            if iIdx>0:
                del self.lC[iIdx]
                self.lC.insert(iIdx-1,o)
                return True
        return False
    def len(self):
        return len(self.lC)
    def __getitem__(self,key):
        return self.lC[key]
    def __str__(self):
        l=['   %s'%s.__str__() for s in self.lC]
        return '\n'.join(['group:%s'%self.name]+l)
    def __iter__(self):
        return vLauncherGrpIter(self)
    def Save(self,n,doc):
        if self.name is not None:
            nC=doc.createSubNode(n,'grp',False)
            doc.createSubNodeText(nC,'name',   self.name,      False)
            doc.createSubNodeText(nC,'img',    self.img,       False)
        else:
            nC=n
        for o in self:
            o.Save(nC,doc)
    def AddTI(self,tip,tr,dImg,which,funcIdx):
        if self.name is not None:
            tic=tr.AppendItem(tip,self.name)
            #if 'grp' in dImg:
            #    idx=dImg['grp']
            #else:
            #    idx=dImg['empty']
            idx=funcIdx('grp',self.img)
            tr.SetItemImage(tic,idx,0,which=which)
            tr.SetItemHasChildren(tic,True)
            tr.SetPyData(tic,self)
        else:
            tic=tip
        self.ti=tic
        for o in self:
            o.AddTI(tic,tr,dImg,which,funcIdx)
    def Launch(self,thdExec,parent,tr,ti):
        for o in self:
            o.Launch(thdExec,parent,tr,ti)
    def LaunchAuto(self,thdExec,parent,tr):
        for o in self:
            o.LaunchAuto(thdExec,parent,tr)
    def GetLaunchCount(self):
        i=0
        for o in self:
            i+=o.GetLaunchCount()
        return i
    def AlmHighAcknowledge(self,tr,bReset=True):
        for o in self:
            if o.AlmHighAcknowledge(tr,bReset=bReset)>0:
                bReset=False
        if bReset==False:
            return 1
        return 0


class vLauncherCmd:
    def __init__(self,name='',appl='',dn='',args='',delay='',usr='',
                img='',recent_size='',
                hist=None,recent=None,geometry='',opt=''):
        self.name=name
        self.appl=appl
        self.dn=dn
        self.args=args
        self.delay=delay
        self.img=img
        self.usr=usr
        self.recent_size=recent_size
        self.lHist=hist
        self.lRecent=recent
        self.geometry=geometry
        self.opt=opt
        self.lProc=[]
        self.ti=None
        self.CalcInternal()
    def CalcInternal(self):
        l=self.opt.split(',')
        self.bShell=False
        self.bSavePipes=False
        self.bDetached=False
        self.bAutoLaunch=False
        for s in l:
            if s=='shell':
                self.bShell=True
            elif s=='save_pipes':
                self.bSavePipes=True
            elif s=='detached':
                self.bDetached=True
            elif s=='autolaunch':
                self.bAutoLaunch=True
    def __str__(self):
        return 'name:%s'%self.name
    def EnvSubstitute(self,s):
        return s
    def EnvEvaluate(self,s):
        iP=0
        iS=0
        iE=-1
        iLen=len(s)
        sN=''
        l=[]
        while iS>=0:
            iS=s.find('%',iP)
            if iS>=0:
                l.append(s[iP:iS])
                iE=s.find('%',iS+1)
                if iE>=0:
                    sH=s[iS+1:iE]
                    sE=os.getenv(sH,None)
                    if sE is not None:
                        l.append(sE)
                    else:
                        l.append('%')
                        l.append(sH)
                        l.append('%')
                    iP=iE+1
                else:
                    l.append(s[iS:])
                    iS=-1
            else:
                l.append(s[iP:])
                iS=-1
        return u''.join(l)#sN
    def AddRecent(self,s):
        if self.lRecent is None:
            self.lRecent=[]
        if s in self.lRecent:
            i=self.lRecent.index(s)
            del self.lRecent[i]
        self.lRecent.append(s)
        try:
            i=int(self.recent_size)
        except:
            i=100
            self.recent_size='100'
        for i in xrange(i,len(self.lRecent)):
            del self.lRecent[0]
    def GetRecent(self,sPrefix=''):
        if self.lRecent is None:
            return []
        return [s for s in self.lRecent if s.startswith(sPrefix)]
    def Save(self,n,doc):
        nC=doc.createSubNode(n,'cmd',False)
        doc.createSubNodeText(nC,'name',   self.name,      False)
        doc.createSubNodeText(nC,'appl',   self.appl,      False)
        doc.createSubNodeText(nC,'dn',     self.dn,        False)
        doc.createSubNodeText(nC,'args',   self.args,      False)
        doc.createSubNodeText(nC,'delay',  self.delay,     False)
        doc.createSubNodeText(nC,'opt',    self.opt,       False)
        doc.createSubNodeText(nC,'usr',    self.usr,       False)
        doc.createSubNodeText(nC,'img',    self.img,       False)
        if len(self.geometry)>0:
            doc.createSubNodeText(nC,'geometry',self.geometry, False)
        if self.lHist is not None:
            nH=doc.createSubNode(nC,'hist',False)
            for s in self.lHist:
                doc.createSubNodeText(nH,'value',   s,     False)
        doc.createSubNodeText(nC,'recent_size', self.recent_size,   False)
        if self.lRecent is not None:
            nH=doc.createSubNode(nC,'recent',False)
            for s in self.lRecent:
                doc.createSubNodeText(nH,'value',   s,     False)
    def SetGeometry(self,x,y,w,h):
        self.geometry='%d,%d,%d,%d'%(x,y,w,h)
    def GetGeometry(self):
        try:
            l=[int(s) for s in self.geometry.split(',')]
            return l
        except:
            return None
    def AddTI(self,tip,tr,dImg,which,funcIdx):
        tic=tr.AppendItem(tip,self.name)
        tr.SetItemHasChildren(tic,True)
        #if 'cmd' in dImg:
        #    idx=dImg['cmd']
        #else:
        #    idx=dImg['empty']
        
        idx=funcIdx('cmd',self.img)
        tr.SetItemImage(tic,idx,0,which=which)
        tr.SetPyData(tic,self)
        self.ti=tic
        self.UpdateTI(tr,tic)
    def Launch(self,thdExec,parent,tr,ti):
        sCmd=u' '.join([
            self.EnvEvaluate(self.appl),self.EnvEvaluate(self.args)])
        #sCmd=[self.EnvEvaluate(self.appl),self.EnvEvaluate(self.args)]
        if len(self.usr)>0:
            if WIN and 0:
                sCmd=u''.join(['runas /user:%s "'%self.usr,sCmd,u'"'])
                #p=subprocess.Popen(sCmd,shell=False,
                #            stdin=subprocess.PIPE,stdout=None,stderr=None)
                os.system(sCmd)
                return None
        p=vLauncherProc(self,sCmd,self.dn,thdExec,parent)
        if self.bDetached==False:
            self.lProc.append(p)
            self.UpdateTI(tr,ti)
            return p
        else:
            return None
    def LaunchAuto(self,thdExec,parent,tr):
        if self.bAutoLaunch:
            try:
                fD=float(self.delay)
                thdExec.CallBackDelayed(fD,
                            parent.Launch,self.ti)
            except:
                self.Launch(thdExec,parent,tr,self.ti)
    def DelProc(self,oProc):
        self.lProc.remove(oProc)
    def UpdateTI(self,tr,ti):
        tr.SetItemText(ti,str(len(self.lProc)),1)
    def GetLaunchCount(self):
        return len(self.lProc)
    def AlmHighAcknowledge(self,tr,bReset=True):
        bAlmHigh=False
        for o in self.lProc:
            if o.HasAlmHigh():
                bAlmHigh=True
            o.UpdateTI(tr,o.tiSel)
            if bReset==True:
                if bAlmHigh:
                    tr.SelectItem(o.tiSel)
                    o.ResetAlmHigh()
                    bReset=False
        if bAlmHigh:
            tr.Expand(self.ti)
            return 1
        return 0

class vLauncherProc:
    def __init__(self,cmd,sCmd,dn,thdExec,parent):
        self.iAlmCount=0
        sCwd=os.getcwdu()
        try:
            self.tiSel=None
            self.funcNotify=None
            try:
                dn=cmd.dn
                if len(dn)>0:
                    os.chdir(dn)
            except:
                vtLog.vtLngTB('vLauncherProc')
            self.ident=u':'.join([cmd.name,u'???'])
            if cmd.bDetached:
                self.p=subprocess.Popen(sCmd,shell=False,
                            stdin=None,stdout=None,stderr=None)
            else:
                self.frmPipes=vLauncherPipesFrame(parent)
                self.frmPipes.Centre()
                self.thdExec=vtThread(self.frmPipes,bPost=True)
                self.thdExec.BindEvents(funcProc=self.OnExecProc,
                        funcAborted=self.OnExecAborted,funcFinished=self.OnExecFin)
                self.thdExecParent=thdExec
                self.p=vtProcess(sCmd,shell=cmd.bShell,
                            timeout=2)
                self.res=[]
                self.p.SetCB(1,'\n',self.handleStdOut)
                self.p.SetCB(2,'\n',self.handleStdErr)
                #self.p.SetCB(2,None,self.handleStdErr)
                
                self.ident=u':'.join([cmd.name,str(self.p._p.pid)])
                self.frmPipes.SetState('running')
                self.frmPipes.SetTitle(self.__str__())
                
                #self.frmPipes.Show()
                self.frmPipes.SetCmd(cmd)
                self.frmPipes.SetProc(self)
                sFN=u'.'.join([cmd.name,str(self.p._p.pid),'.txt'])
                self.bWrite2File=cmd.bSavePipes
                if self.bWrite2File:
                    self.fOut=open(sFN,'wb')
                else:
                    self.fOut=None
                self.thdExec.Do(self.p.communicateLoop)
                if self.bWrite2File:
                    self.thdExec.Do(self.fOut.close)
        except:
            vtLog.vtLngTB('vLauncherObj')
        os.chdir(sCwd)
    def SetTreeItem(self,ti):
        self.tiSel=ti
    def SetFuncNotify(self,func):
        self.funcNotify=func
    def Show(self,flag):
        if self.frmPipes is not None:
            self.frmPipes.Show(flag)
            if flag:
                try:
                    if self.frmPipes.IsIconized():
                        self.frmPipes.Iconize(False)
                except:
                    pass
                self.frmPipes.Raise()
    def SetFocus(self):
        if self.frmPipes is not None:
            self.frmPipes.SetFocus()
    def __str__(self):
        return self.ident
    def OnExecProc(self,evt):
        evt.Skip()
    def OnExecFin(self,evt):
        evt.Skip()
        self.frmPipes.SetState('stopped')
    def OnExecAborted(self,evt):
        evt.Skip()
        self.frmPipes.SetState('aborted')
    def HasAlmHigh(self):
        return self.iAlmCount>0
    def ResetAlmHigh(self):
        self.iAlmCount=0
    def UpdateTI(self,tr,ti):
        if self.HasAlmHigh():
            tr.SetItemText(ti,str(self.iAlmCount),1)
        else:
            tr.SetItemText(ti,u'',1)
    #def handleStdErr(self,s1):
    #    s=s1.replace('\r','')
    def handleStdErr(self,s):
        self.res.append(s)
        if self.bWrite2File:
            self.fOut.write(s)
        #self.fOut.write('\n')
        #self.fOut.write()
        self.iAlmCount+=1
        #if self.tr is not None:
        #    self.thdExec.CallBack(self.UpdateTI,self.tr,self.tiSel)
        if self.frmPipes is not None:
            self.thdExec.CallBack(self.frmPipes.AddStdErr,s)
        if self.funcNotify is not None:
            self.thdExec.CallBack(self.funcNotify,1)
    #def handleStdOut(self,s1):
    #    s=s1.replace('\r','')
    def handleStdOut(self,s):
        if self.bWrite2File:
            self.fOut.write(s)
        #self.fOut.write('\n')
        #os.write(self.fOut,s)
        #os.write(self.fOut,'\n')
        if self.frmPipes is not None:
            self.thdExec.CallBack(self.frmPipes.AddStdOut,s)
    def WriteStdIn(self,s,bLineSep=False,bEnter=True):
        self.p._p.stdin.write(s)
        if bLineSep:
            self.p._p.stdin.write(os.linesep)
        if bEnter:
            self.p._p.stdin.write('\n')
        self.p._p.stdin.flush()
    def IsRunning(self):
        return self.p.isRunning()
