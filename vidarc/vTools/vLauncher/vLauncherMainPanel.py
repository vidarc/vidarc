#Boa:FramePanel:vLauncherMainPanel
#----------------------------------------------------------------------------
# Name:         vLauncherMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: vLauncherMainPanel.py,v 1.6 2009/10/18 18:53:42 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.misc.vtmTreeListCtrl
import time,binascii,os
import cStringIO

import wx.tools.img2img as img2img
import wx.tools.img2py as img2py

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

try:
    #from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.vtThread import vtThreadWX
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.art.state.vtArtState as vtArtState
    
    import vidarc.vTools.vLauncher.images as imgLauncher
    from vidarc.vTools.vLauncher.vLauncherTreeDialog import vLauncherTreeDialog
    from vidarc.vTools.vLauncher.vLauncherPipesDialog import vLauncherPipesDialog
    from vidarc.vTools.vLauncher.vLauncherPipesFrame import vLauncherPipesFrame
    from vidarc.vTools.vLauncher.vLauncherObj import vLauncherProc
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vLauncherMainPanel(parent)

[wxID_VLAUNCHERMAINPANEL, wxID_VLAUNCHERMAINPANELCBLAUNCH, 
 wxID_VLAUNCHERMAINPANELCBPIPES, wxID_VLAUNCHERMAINPANELCBPOPUP, 
 wxID_VLAUNCHERMAINPANELTRLAUNCH, 
] = [wx.NewId() for _init_ctrls in range(5)]

def getPluginImage():
    return imgLauncher.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(imgLauncher.getPluginBitmap())
    return icon

class vLauncherMainPanel(wx.Panel,vtLog.vtLogOrigin):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trLaunch, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsData, 1, border=0, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbPopup, 0, border=0, flag=0)
        parent.AddWindow(self.cbLaunch, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbPipes, 0, border=8, flag=wx.TOP)

    def _init_coll_trLaunch_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=_(u'Name'))

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self._init_coll_bxsData_Items(self.bxsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VLAUNCHERMAINPANEL,
              name=u'vLauncherMainPanel', parent=prnt, pos=wx.Point(60, 185),
              size=wx.Size(185, 218), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(177, 191))

        self.trLaunch = vidarc.tool.misc.vtmTreeListCtrl.vtmTreeListCtrl(id=wxID_VLAUNCHERMAINPANELTRLAUNCH,
              name=u'trLaunch', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(153, 191), style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT)
        self._init_coll_trLaunch_Columns(self.trLaunch)
        self.trLaunch.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrLaunchTreeSelChanged,
              id=wxID_VLAUNCHERMAINPANELTRLAUNCH)
        self.trLaunch.Bind(wx.EVT_LEFT_DCLICK, self.OnTrLaunchLeftDclick)

        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERMAINPANELCBPOPUP, name=u'cbPopup',
              parent=self, pos=wx.Point(153, 0), size=wx.Size(24, 24), style=0)
        self.cbPopup.Bind(wx.EVT_BUTTON, self.OnCbPopupButton,
              id=wxID_VLAUNCHERMAINPANELCBPOPUP)

        self.cbLaunch = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERMAINPANELCBLAUNCH, name=u'cbLaunch',
              parent=self, pos=wx.Point(153, 32), size=wx.Size(24, 24),
              style=0)
        self.cbLaunch.Enable(False)
        self.cbLaunch.Bind(wx.EVT_BUTTON, self.OnCbLaunchButton,
              id=wxID_VLAUNCHERMAINPANELCBLAUNCH)

        self.cbPipes = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VLAUNCHERMAINPANELCBPIPES, name=u'cbPipes',
              parent=self, pos=wx.Point(153, 64), size=wx.Size(24, 24),
              style=0)
        self.cbPipes.Enable(False)
        self.cbPipes.Bind(wx.EVT_BUTTON, self.OnCbPipesButton,
              id=wxID_VLAUNCHERMAINPANELCBPIPES)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vLauncher')
        self._init_ctrls(parent)
        self.SetName(name)
        
        # ++++++++++++++
        # add stuff here
        #vtLog.vtLogOrigin.__init__(self,origin=vtLog.vtLngGetRelevantNamesWX(self))
        vtLog.vtLogOrigin.__init__(self,origin=self.GetName())
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        
        self.thdExec=vtThreadWX(self,True)
        self.thdExec.BindEvents(funcProc=self.OnExecProc,
                funcAborted=self.OnExecAborted,funcFinished=self.OnExecFin)
        
        self.popWin=None
        self.launches=None
        self.bBusy=False
        self.tiSel=None
        self.funcNotify=None
        
        self.imgDict={}
        self.imgLst=wx.ImageList(16,16)
        self.imgDict['host']=self.imgLst.Add(imgLauncher.getHostBitmap())
        self.imgDict['usr']=self.imgLst.Add(imgLauncher.getUsrBitmap())
        self.imgDict['launch']=self.imgLst.Add(imgLauncher.getLaunchBitmap())
        self.imgDict['cmd']={'':self.imgLst.Add(imgLauncher.getLaunchBitmap())}
        self.imgDict['grp']={'':self.imgLst.Add(imgLauncher.getGroupingBitmap())}
        self.imgDict['empty']=self.imgLst.Add(imgLauncher.getInvisibleBitmap())
        self.imgDict['running']=self.imgLst.Add(vtArtState.getBitmap(vtArtState.Running))
        self.imgDict['stopped']=self.imgLst.Add(vtArtState.getBitmap(vtArtState.Stopped))
        self.imgDict['aborted']=self.imgLst.Add(vtArtState.getBitmap(vtArtState.Aborted))
        
        #self.imgDict['include']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Apply))
        #self.imgDict['exclude']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Cancel))
        self.trLaunch.SetImageList(self.imgLst)
        #self.trLaunch.AddColumn(_('active'),width=80,flag=wx.ALIGN_RIGHT)
        self.trLaunch.AddColumn(_('active'))
        self.trLaunch.SetColumnAlignment(1,wx.ALIGN_RIGHT)
        self.trLaunch.SetStretchLst([
                    (0,-2.0),
                    (1,0.2),
                    ])
        
        #self.docData=vtXmlDom(appl='vLauncherCfg',audit_trail=False)
        
        self.cbPopup.SetBitmapLabel(imgLauncher.getDownBitmap())
        self.cbPopup.SetBitmapSelected(imgLauncher.getDownBitmap())
        
        self.cbLaunch.SetBitmapLabel(imgLauncher.getLaunchBitmap())
        self.cbPipes.SetBitmapLabel(imgLauncher.getShellBitmap())
        # add stuff here
        # --------------
        
        self.Move(pos)
        self.SetSize(size)
    def SetFuncNotify(self,func):
        self.funcNotify=func
    def NotifyStdErr(self,iDelta,ti):
        if self.funcNotify is not None:
            self.funcNotify(iDelta)
    def AlmHighAcknowledge(self):
        try:
            if self.launches.AlmHighAcknowledge(self.trLaunch)>0:
                if self.funcNotify is not None:
                    wx.CallAfter(self.funcNotify,1)
            self.trLaunch.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def getTreeItemImageIdx(self,sType,sImg):
        if sType in self.imgDict:
            d=self.imgDict[sType]
        else:
            d={}
            self.imgDict[sType]=d
        if sImg in d:
            return d[sImg]
        else:
            if sImg[:3]=='wx.':
                bmp = wx.ArtProvider_GetBitmap(eval(sImg), eval('wx.ART_TOOLBAR'), (16,16))
            elif sImg[:6]=='vtArt.':
                bmp=vtArt.getBitmap(eval(sImg))
            else:
                stream = cStringIO.StringIO(binascii.unhexlify(sImg))
                bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
            idx=self.imgLst.Add(bmp)
            d[sImg]=idx
            return idx
            #else:
        return self.imgDict['cmd']['']
    def OnExecProc(self,evt):
        evt.Skip()
    def OnExecFin(self,evt):
        evt.Skip()
    def OnExecAborted(self,evt):
        evt.Skip()
    #def OpenFile(self,fn):
    #    if self.docData.ClearAutoFN()>0:
    #        self.docData.Open(fn)
    def ShutDown(self):
        self.thdExec.Stop()
        self.thdExec.SetPostWid(None,False)
    def LaunchAuto(self):
        try:
            if self.launches is not None:
                self.launches.LaunchAuto(self.thdExec,self,self.trLaunch)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetLaunches(self,launches):
        self.launches=launches
        self.tiSel=None
        self.cbLaunch.Enable(False)
        if self.popWin is not None:
            self.popWin.SetData(None)
        self.trLaunch.DeleteAllItems()
        tip=self.trLaunch.AddRoot('launches')
        idx=self.imgDict['launch']
        self.trLaunch.SetItemImage(tip,idx,0,which=wx.TreeItemIcon_Normal)
        self.launches.AddTI(tip,self.trLaunch,self.imgDict,
                            wx.TreeItemIcon_Normal,
                            self.getTreeItemImageIdx)
    def AddLaunchEntry(self,oP,oA):
        try:
            if self.tiSel is not None:
                if oP is None:
                    tip=self.trLaunch.GetItemParent(self.tiSel)
                    oP=self.trLaunch.GetPyData(tip)
                    if oP is None:
                        oP=self.launches
                        tip=self.trLaunch.GetRootItem()
                else:
                    tip=self.tiSel
                oP.append(oA)
                #o=self.trLaunch.GetPyData(self.tiSel)
                #oA.AddTI(tip,self.trLaunch,self.imgDict,
                #            wx.TreeItemIcon_Normal)
                
            else:
                tip=self.trLaunch.GetRootItem()
                self.launches.append(oA)
            oA.AddTI(tip,self.trLaunch,self.imgDict,
                            wx.TreeItemIcon_Normal,
                            self.getTreeItemImageIdx)
            self.trLaunch.Expand(tip)
        except:
            vtLog.vtLngTB(self.GetName())
    def DelLaunchEntry(self,o):
        try:
            if self.tiSel is not None:
                tip=self.trLaunch.GetItemParent(self.tiSel)
                oP=self.trLaunch.GetPyData(tip)
                if oP is None:
                    oP=self.launches
                oP.remove(o)
                ti=self.tiSel
                self.tiSel=None
                self.trLaunch.Delete(ti)
        except:
            vtLog.vtLngTB(self.GetName())
    def MoveLaunchEntry(self,iOfs,o):
        try:
            if self.tiSel is not None:
                tip=self.trLaunch.GetItemParent(self.tiSel)
                oP=self.trLaunch.GetPyData(tip)
                if oP is None:
                    oP=self.launches
                    tip=self.trLaunch.GetRootItem()
                    bFull=True
                else:
                    bFull=False
                if oP.move(iOfs,o):
                    
                    ti=self.tiSel
                    self.tiSel=None
                    try:
                        if bFull==False:
                            self.trLaunch.DeleteChildren(tip)
                            for o in oP:
                                o.AddTI(tip,self.trLaunch,self.imgDict,
                                            wx.TreeItemIcon_Normal,
                                            self.getTreeItemImageIdx)
                                self.trLaunch.Expand(tip)
                    except:
                        bFull=True
                        oP=self.launches
                        tip=self.trLaunch.GetRootItem()
                    if bFull:
                        self.trLaunch.DeleteChildren(tip)
                        oP.AddTI(tip,self.trLaunch,self.imgDict,
                                wx.TreeItemIcon_Normal,
                                self.getTreeItemImageIdx)
                        self.trLaunch.Expand(tip)
                    
        except:
            vtLog.vtLngTB(self.GetName())
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vLauncherTreeDialog(self)#,sz,wx.SIMPLE_BORDER)
                #self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                #self.popWin.SetNodeTree(self.nodeTree)
                #if self.trSetNode is not None:
                #    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
            if self.tiSel is not None:
                o=self.trLaunch.GetPyData(self.tiSel)
                self.popWin.SetData(o)
        else:
            pass
    def OnCbPopupButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
    def SetState(self,state,tiSel):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            if tiSel is not None:
                o=self.trLaunch.GetPyData(tiSel)
                if isinstance(o,vLauncherProc)==True:
                    if state in self.imgDict:
                        idx=self.imgDict[state]
                    else:
                        idx=self.imgDict['launch']
                    self.trLaunch.SetItemImage(tiSel,idx,0,which=wx.TreeItemIcon_Normal)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def DelProc(self,tiSel):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            if tiSel is not None:
                o=self.trLaunch.GetPyData(tiSel)
                if isinstance(o,vLauncherProc)==True:
                    self.trLaunch.Delete(tiSel)
                    ti=self.trLaunch.GetItemParent(tiSel)
                    oPar=self.trLaunch.GetPyData(ti)
                    try:
                        oPar.DelProc(o)
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                    o.SetTreeItem(None)
                    oPar.UpdateTI(self.trLaunch,ti)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbLaunchButton(self, event):
        if event is not None:
            event.Skip()
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            if self.tiSel is not None:
                self.Launch(self.tiSel)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Launch(self,ti):
        try:
            o=self.trLaunch.GetPyData(ti)
            if isinstance(o,vLauncherProc)==False:
                p=o.Launch(self.thdExec,self,self.trLaunch,ti)
                if p is not None:
                    p.SetFuncNotify(self.funcNotify)
                    tic=self.trLaunch.AppendItem(ti,p.__str__())
                    #p.SetFuncNotify(self.NotifyStdErr)
                    idx=self.imgDict['running']
                    self.trLaunch.SetPyData(tic,p)
                    self.trLaunch.SetItemImage(tic,idx,0,which=wx.TreeItemIcon_Normal)
                    p.SetTreeItem(tic)
                    p.Show(True)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbPipesButton(self, event):
        event.Skip()
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            if self.tiSel is not None:
                o=self.trLaunch.GetPyData(self.tiSel)
                if isinstance(o,vLauncherProc)==True:
                    #o.Show(False)
                    o.Show(True)
                    #o.SetFocus()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #dlg=vLauncherPipesDialog(self)
        #dlg.Centre()
        #dlg.Show(True)
        #frm=vLauncherPipesFrame(self)
        #frm.Centre()
        #frm.Show(True)
    def OnTrLaunchTreeSelChanged(self, event):
        event.Skip()
        self.tiSel=event.GetItem()
        try:
            if self.tiSel is not None:
                o=self.trLaunch.GetPyData(self.tiSel)
                if isinstance(o,vLauncherProc)==True:
                    self.cbLaunch.Enable(False)
                    self.cbPipes.Enable(True)
                else:
                    self.cbLaunch.Enable(True)
                    self.cbPipes.Enable(False)
                    if self.popWin is not None:
                        self.popWin.SetData(o)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnTrLaunchLeftDclick(self, event):
        event.Skip()
        try:
            vtLog.CallStack('')
            print self.tiSel
            if self.tiSel is not None:
                o=self.trLaunch.GetPyData(self.tiSel)
                print o
            
        except:
            vtLog.vtLngTB(self.GetName())
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""vLauncher module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Launcher'),desc,version

