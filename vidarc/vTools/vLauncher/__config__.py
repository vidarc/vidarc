#----------------------------------------------------------------------------
# Name:         __config__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20080802
# CVS-ID:       $Id: __config__.py,v 1.4 2009/10/18 18:53:42 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------
VER_MAJOR        = 1      # The first three must match wxWidgets
VER_MINOR        = 0
VER_RELEASE      = 4
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.
VERSION          = u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE)

DESCRIPTION      = "VIDARC Launcher"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
MAINTAINER       = "Walter Obweger"
MAINTAINER_EMAIL = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "MES Project Management"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "tools,launcher"

LONG_DESCRIPTION = """\

"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000/XP
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""
