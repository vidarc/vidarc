#Boa:FramePanel:vCalendarPanel
#----------------------------------------------------------------------------
# Name:         vCalendarPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060209
# CVS-ID:       $Id: vCalendarPanel.py,v 1.1 2008/08/16 14:14:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import vidarc.tool.time.vtTimeInputDate
import wx.lib.buttons
import wx.calendar

import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.vTools.vLauncher.images as images
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getCalendarImage()

[wxID_VCALENDARPANEL, wxID_VCALENDARPANELCBSTORE, wxID_VCALENDARPANELCBTODAY, 
 wxID_VCALENDARPANELLBLDAY, wxID_VCALENDARPANELLBLDIFFDAY, 
 wxID_VCALENDARPANELLBLDIFFHR, wxID_VCALENDARPANELLBLDIFFMIN, 
 wxID_VCALENDARPANELLBLDIFFSEC, wxID_VCALENDARPANELLBLDIFFWK, 
 wxID_VCALENDARPANELLBLDY, wxID_VCALENDARPANELLBLHR, wxID_VCALENDARPANELLBLMN, 
 wxID_VCALENDARPANELLBLSEC, wxID_VCALENDARPANELLBLWK, 
 wxID_VCALENDARPANELPNDATA, wxID_VCALENDARPANELSPWEEK, 
 wxID_VCALENDARPANELVIDATE, 
] = [wx.NewId() for _init_ctrls in range(17)]

class vCalendarPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbToDay, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbStore, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.fgsVal, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsVal_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)

    def _init_coll_fgsVal_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spWeek, 0, border=0, flag=0)
        parent.AddWindow(self.lblDay, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblWk, 0, border=0, flag=wx.ALIGN_RIGHT)
        parent.AddWindow(self.lblDiffWk, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDy, 0, border=0, flag=wx.ALIGN_RIGHT)
        parent.AddWindow(self.lblDiffDay, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblHr, 0, border=0, flag=wx.ALIGN_RIGHT)
        parent.AddWindow(self.lblDiffHr, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblMn, 0, border=0, flag=wx.ALIGN_RIGHT)
        parent.AddWindow(self.lblDiffMin, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblSec, 0, border=0, flag=wx.ALIGN_RIGHT)
        parent.AddWindow(self.lblDiffSec, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=4, rows=3, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsVal = wx.FlexGridSizer(cols=2, hgap=4, rows=4, vgap=4)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsVal_Items(self.fgsVal)
        self._init_coll_fgsVal_Growables(self.fgsVal)

        self.pnData.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCALENDARPANEL, name=u'pnCalendar',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(476, 295),
              style=wx.TAB_TRAVERSAL)
        self.SetAutoLayout(True)

        self.pnData = wx.Panel(id=wxID_VCALENDARPANEL, name=u'pnData',
              parent=self, pos=wx.Point(208, 0), size=wx.Size(256, 264),
              style=wx.TAB_TRAVERSAL)
        self.pnData.SetConstraints(LayoutAnchors(self.pnData, True, True, True,
              True))

        self.cbToDay = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VCALENDARPANELCBTODAY, label=u'today',
              name=u'cbToDay', parent=self.pnData, pos=wx.Point(0, 0),
              size=wx.Size(100, 30), style=0)
        self.cbToDay.Bind(wx.EVT_BUTTON, self.OnCbToDayButton,
              id=wxID_VCALENDARPANELCBTODAY)

        self.spWeek = wx.SpinCtrl(id=wxID_VCALENDARPANELSPWEEK, initial=0,
              max=53, min=1, name=u'spWeek', parent=self.pnData, pos=wx.Point(0,
              46), size=wx.Size(40, 21), style=wx.SP_ARROW_KEYS)
        self.spWeek.Bind(wx.EVT_TEXT, self.OnSpWeekText,
              id=wxID_VCALENDARPANELSPWEEK)
        self.spWeek.Bind(wx.EVT_SPIN, self.OnSpWeekSpin,
              id=wxID_VCALENDARPANELSPWEEK)

        self.lblDay = wx.StaticText(id=wxID_VCALENDARPANELLBLDAY, label=u'',
              name=u'lblDay', parent=self.pnData, pos=wx.Point(44, 46),
              size=wx.Size(50, 21), style=0)
        self.lblDay.SetToolTipString(u'Day Of Year')

        self.cbStore = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VCALENDARPANELCBSTORE, label=u'YYYY-MM-DD',
              name=u'cbStore', parent=self.pnData, pos=wx.Point(132, 0),
              size=wx.Size(100, 30), style=0)
        self.cbStore.Bind(wx.EVT_BUTTON, self.OnCbStoreButton,
              id=wxID_VCALENDARPANELCBSTORE)

        self.lblDiffWk = wx.TextCtrl(id=wxID_VCALENDARPANELLBLDIFFWK,
              name=u'lblDiffWk', parent=self.pnData, pos=wx.Point(44, 83),
              size=wx.Size(212, 20), style=wx.ALIGN_RIGHT, value=u'')
        self.lblDiffWk.SetToolTipString(u'Day Of Year')
        self.lblDiffWk.SetForegroundColour(wx.Colour(0, 128, 0))

        self.lblDiffDay = wx.TextCtrl(id=wxID_VCALENDARPANELLBLDIFFDAY,
              name=u'lblDiffDay', parent=self.pnData, pos=wx.Point(44, 107),
              size=wx.Size(212, 20), style=wx.ALIGN_RIGHT, value=u'')
        self.lblDiffDay.SetToolTipString(u'Day Of Year')
        self.lblDiffDay.SetForegroundColour(wx.Colour(0, 128, 0))

        self.lblWk = wx.StaticText(id=wxID_VCALENDARPANELLBLWK, label=u'wk',
              name=u'lblWk', parent=self.pnData, pos=wx.Point(27, 83),
              size=wx.Size(13, 13), style=0)

        self.lblDy = wx.StaticText(id=wxID_VCALENDARPANELLBLDY, label=u'day',
              name=u'lblDy', parent=self.pnData, pos=wx.Point(22, 107),
              size=wx.Size(18, 13), style=0)

        self.lblHr = wx.StaticText(id=wxID_VCALENDARPANELLBLHR, label=u'hr',
              name=u'lblHr', parent=self.pnData, pos=wx.Point(30, 131),
              size=wx.Size(10, 13), style=0)

        self.lblMn = wx.StaticText(id=wxID_VCALENDARPANELLBLMN, label=u'min',
              name=u'lblMn', parent=self.pnData, pos=wx.Point(24, 155),
              size=wx.Size(16, 13), style=0)

        self.lblDiffHr = wx.TextCtrl(id=wxID_VCALENDARPANELLBLDIFFHR,
              name=u'lblDiffHr', parent=self.pnData, pos=wx.Point(44, 131),
              size=wx.Size(212, 20), style=wx.ALIGN_RIGHT, value=u'')
        self.lblDiffHr.SetToolTipString(u'Day Of Year')
        self.lblDiffHr.SetForegroundColour(wx.Colour(0, 128, 0))

        self.lblSec = wx.StaticText(id=wxID_VCALENDARPANELLBLSEC, label=u'sec',
              name=u'lblSec', parent=self.pnData, pos=wx.Point(0, 179),
              size=wx.Size(40, 26), style=wx.ALIGN_RIGHT)

        self.lblDiffMin = wx.TextCtrl(id=wxID_VCALENDARPANELLBLDIFFMIN,
              name=u'lblDiffMin', parent=self.pnData, pos=wx.Point(44, 155),
              size=wx.Size(212, 20), style=wx.ALIGN_RIGHT, value=u'')
        self.lblDiffMin.SetToolTipString(u'Day Of Year')
        self.lblDiffMin.SetForegroundColour(wx.Colour(0, 128, 0))

        self.lblDiffSec = wx.TextCtrl(id=wxID_VCALENDARPANELLBLDIFFSEC,
              name=u'lblDiffSec', parent=self.pnData, pos=wx.Point(44, 179),
              size=wx.Size(212, 26), style=wx.ALIGN_RIGHT, value=u'')
        self.lblDiffSec.SetToolTipString(u'Day Of Year')
        self.lblDiffSec.SetForegroundColour(wx.Colour(0, 128, 0))

        self.viDate = vidarc.tool.time.vtTimeInputDate.vtTimeInputDate(id=wxID_VCALENDARPANELVIDATE,
              name=u'viDate', parent=self, pos=wx.Point(0, 0), size=wx.Size(192,
              192), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        
        self.bBlock=True
        self.dtMark=wx.DateTime().Today()
        self.dt=wx.DateTime().Today()
        self.dtStart=vtDateTime(True)
        self.dtEnd=vtDateTime(True)
        self.cbToDay.SetBitmapLabel(images.getToDayBitmap())
        self.cbStore.SetBitmapLabel(images.getStoreBitmap())
        vidarc.tool.time.vtTimeInputDate.EVT_VTTIME_INPUT_DATE_CHANGED(self.viDate,
                    self.OnViDateVttimeInputDateChanged)
        vidarc.tool.time.vtTimeInputDateTime.EVT_VTTIME_INPUT_DATETIME_CHANGED(self.viStart,
                    self.OnViStartChanged)
        vidarc.tool.time.vtTimeInputDateTime.EVT_VTTIME_INPUT_DATETIME_CHANGED(self.viEnd,
                    self.OnViEndChanged)
        self.viStart.SetEnableMark(False)
        self.viEnd.SetEnableMark(False)
        #self.gbsData.AddGrowableCol(3)
        #self.gbsData.Layout()
        #self.gbsData.Fit(self)
        
        self.__today__()
        self.__store__()
        #self.dtMark.Set(self.calCalendar.GetDate())
        #self.dtMark=self.calCalendar.GetDate()
        self.__show__()
        #wx.CallAfter(self.__show__)
        wx.CallAfter(self.__clearBlock__)
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def __store__(self):
        dt=self.viDate.GetDate()
        self.dtMark.SetYear(dt.GetYear())
        self.dtMark.SetMonth(dt.GetMonth()-1)
        self.dtMark.SetDay(dt.GetDay())
    def __today__(self):
        self.viDate.SetNow()
        self.__show__()
    def __show__(self):
        dt=self.viDate.GetDate()
        self.dt.Set(dt.GetDay(),dt.GetMonth()-1,dt.GetYear(),0,0,0)
        iW=self.dt.GetWeekOfYear()
        self.spWeek.SetValue(iW)
        iDay=self.dt.GetDayOfYear()
        self.lblDay.SetLabel(_(u'day: %03d')%iDay)
        
        self.cbStore.SetLabel(self.dtMark.FormatISODate())
        zDiff=self.dt-self.dtMark
        if zDiff.IsNegative():
            self.lblDiffWk.SetForegroundColour(wx.Colour(128, 0, 0))
            self.lblDiffDay.SetForegroundColour(wx.Colour(128, 0, 0))
            self.lblDiffHr.SetForegroundColour(wx.Colour(128, 0, 0))
            self.lblDiffMin.SetForegroundColour(wx.Colour(128, 0, 0))
            self.lblDiffSec.SetForegroundColour(wx.Colour(128, 0, 0))
            sSig='-'
            zDiff=zDiff.Abs()
        else:
            self.lblDiffWk.SetForegroundColour(wx.Colour(0, 128, 0))
            self.lblDiffDay.SetForegroundColour(wx.Colour(0, 128, 0))
            self.lblDiffHr.SetForegroundColour(wx.Colour(0, 128, 0))
            self.lblDiffMin.SetForegroundColour(wx.Colour(0, 128, 0))
            self.lblDiffSec.SetForegroundColour(wx.Colour(0, 128, 0))
            sSig='+'
        self.lblDiffWk.SetLabel('%s%04d'%(sSig,zDiff.GetWeeks()))
        self.lblDiffDay.SetLabel('%s%04d'%(sSig,zDiff.GetDays()))
        self.lblDiffHr.SetLabel('%s%04d'%(sSig,zDiff.GetHours()))
        self.lblDiffMin.SetLabel('%s%04d'%(sSig,zDiff.GetMinutes()))
        self.lblDiffSec.SetLabel('%s%04d'%(sSig,zDiff.GetSeconds()))
        self.Refresh()
    def __showStartEnd__(self):
        try:
            self.dtStart.SetStr(self.viStart.GetValue())
            self.dtEnd.SetStr(self.viEnd.GetValue())
        except:
            return
        try:
            dt=self.dtStart
            dtS=wx.DateTime().Today()
            dtS.Set(dt.GetDay(),dt.GetMonth()-1,dt.GetYear(),dt.GetHour(),dt.GetMinute(),dt.GetSecond())
            dt=self.dtEnd
            dtE=wx.DateTime().Today()
            dtE.Set(dt.GetDay(),dt.GetMonth()-1,dt.GetYear(),dt.GetHour(),dt.GetMinute(),dt.GetSecond())
            zDiff=dtE-dtS
            if zDiff.IsNegative():
                self.lblDiffWk.SetForegroundColour(wx.Colour(128, 0, 0))
                self.lblDiffDay.SetForegroundColour(wx.Colour(128, 0, 0))
                self.lblDiffHr.SetForegroundColour(wx.Colour(128, 0, 0))
                self.lblDiffMin.SetForegroundColour(wx.Colour(128, 0, 0))
                self.lblDiffSec.SetForegroundColour(wx.Colour(128, 0, 0))
                sSig='-'
                zDiff=zDiff.Abs()
            else:
                self.lblDiffWk.SetForegroundColour(wx.Colour(0, 128, 0))
                self.lblDiffDay.SetForegroundColour(wx.Colour(0, 128, 0))
                self.lblDiffHr.SetForegroundColour(wx.Colour(0, 128, 0))
                self.lblDiffMin.SetForegroundColour(wx.Colour(0, 128, 0))
                self.lblDiffSec.SetForegroundColour(wx.Colour(0, 128, 0))
                sSig='+'
            self.lblDiffWk.SetLabel('%s%04d'%(sSig,zDiff.GetWeeks()))
            self.lblDiffDay.SetLabel('%s%04d'%(sSig,zDiff.GetDays()))
            self.lblDiffHr.SetLabel('%s%04d'%(sSig,zDiff.GetHours()))
            self.lblDiffMin.SetLabel('%s%04d'%(sSig,zDiff.GetMinutes()))
            self.lblDiffSec.SetLabel('%s%04d'%(sSig,zDiff.GetSeconds()))
            self.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbToDayButton(self, event):
        self.bBlock=True
        self.__today__()
        wx.CallAfter(self.__clearBlock__)
        event.Skip()
    def OnSpWeekText(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.bBlock,self)
            if self.bBlock:
                return
            self.bBlock=True
            iW=self.spWeek.GetValue()
            dt=self.viDate.GetDate()
            self.dt.Set(dt.GetDay(),dt.GetMonth()-1,dt.GetYear(),0,0,0)
            iY=self.dt.GetYear()
            dt=self.dt.SetToWeekOfYear(iY,iW)
            self.viDate.SetValueStr('%04d-%02d-%02d'%(dt.GetYear(),
                            dt.GetMonth()+1,dt.GetDay()))
            wx.CallAfter(self.__show__)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnSpWeekSpin(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.bBlock,self)
            if self.bBlock:
                return
            self.bBlock=True
            iW=self.spWeek.GetValue()
            dt=self.viDate.GetDate()
            self.dt.Set(dt.GetDay(),dt.GetMonth()-1,dt.GetYear(),0,0,0)
            iY=self.dt.GetYear()
            dt=self.dt.SetToWeekOfYear(iY,iW)
            self.viDate.SetValueStr('%04d-%02d-%02d'%(dt.GetYear(),
                            dt.GetMonth()+1,dt.GetDay()))
            wx.CallAfter(self.__show__)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnCalCalendarCalendarSelChanged(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.bBlock,self)
            if self.bBlock:
                return
            self.bBlock=True
            #dt=self.calCalendar.GetDate()
            #iW=dt.GetWeekOfYear()
            #self.spWeek.SetValue(iW)
            self.__show__()

        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def __clearBlock__(self):
        self.bBlock=False
    def OnCbStoreButton(self, event):
        event.Skip()
        #self.dtMark.Set(self.calCalendar.GetDate())
        #self.dtMark=self.calCalendar.GetDate()
        self.__store__()
        self.__show__()
        #self.cbStore.Refresh()
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def OnViDateVttimeInputDateChanged(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.bBlock,self)
            if self.bBlock:
                return
            self.bBlock=True
            #dt=self.calCalendar.GetDate()
            #iW=dt.GetWeekOfYear()
            #self.spWeek.SetValue(iW)
            self.__show__()

        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnViStartChanged(self,evt):
        self.__showStartEnd__()
    def OnViEndChanged(self,evt):
        self.__showStartEnd__()

