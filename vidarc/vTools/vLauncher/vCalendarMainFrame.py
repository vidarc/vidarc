#Boa:Frame:vCalendarMainFrame
#----------------------------------------------------------------------------
# Name:         vCalendarMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071106
# CVS-ID:       $Id: vCalendarMainFrame.py,v 1.2 2008/08/16 14:14:46 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vTools.vLauncher.images as images
    from vidarc.vTools.vLauncher.vCalendarPanel import vCalendarPanel
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return images.getCalendarImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getCalendarBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vContactMain'):
    return vCalendarMainFrame(parent,id,pos,size,style,name)

[wxID_VCALENDARMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vCalendarMainFrame(wx.Frame):
    STATUS_CLK_POS=4
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VCALENDARMAINFRAME,
              name=u'vCalendarMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 300), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vContact')
        self.SetClientSize(wx.Size(492, 293))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        try:
            self.pn=vCalendarPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vContactMainPanel')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainClose(self,event):
        event.Skip()
