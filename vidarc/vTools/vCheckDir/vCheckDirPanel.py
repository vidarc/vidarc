#Boa:FramePanel:vCheckDirPanel
#----------------------------------------------------------------------------
# Name:         vCheckDirPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060604
# CVS-ID:       $Id: vCheckDirPanel.py,v 1.9 2008/08/11 09:22:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton
import time,os,os.path,stat,threading
import Queue,sys

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

import images as vCheckDirImages

from vidarc.tool.InOut.fnUtil import *
import vidarc.tool.InOut.listFiles as listFiles
from vidarc.tool.InOut.listFilesXml import *
from vidarc.tool.InOut.vtInOutComp import vtInOutCompDirEntry
from vidarc.tool.InOut.vtInOutReadThread import *
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
import vidarc.tool.misc.vtmTreeListCtrl
#from vidarc.tool.vtThread import vtThreadWX
from vCheckDirResultDialog import vCheckDirResultDialog

VERBOSE=0

def create(parent):
    return vCheckDirPanel(parent)

[wxID_VCHECKDIRPANEL, wxID_VCHECKDIRPANELCBADDDIR, 
 wxID_VCHECKDIRPANELCBCALCSRC, wxID_VCHECKDIRPANELCBCALCSRC2, 
 wxID_VCHECKDIRPANELCBCMP, wxID_VCHECKDIRPANELCBDELDIR, 
 wxID_VCHECKDIRPANELCBOPENSRC1, wxID_VCHECKDIRPANELCBOPENSRC2, 
 wxID_VCHECKDIRPANELCBPOPUP, wxID_VCHECKDIRPANELCBREFRESHDIR, 
 wxID_VCHECKDIRPANELCBSAVEDIR, wxID_VCHECKDIRPANELCBSETDATA, 
 wxID_VCHECKDIRPANELCBSETDIR, wxID_VCHECKDIRPANELCBSETSNAP, 
 wxID_VCHECKDIRPANELCBSNAPSHOT, wxID_VCHECKDIRPANELCHCSHOWEQUAL, 
 wxID_VCHECKDIRPANELCHKACCESS, wxID_VCHECKDIRPANELCHKCALCMD5, 
 wxID_VCHECKDIRPANELCHKCREATE, wxID_VCHECKDIRPANELCHKFLAT, 
 wxID_VCHECKDIRPANELCHKMOD, wxID_VCHECKDIRPANELDBBDATA, 
 wxID_VCHECKDIRPANELDBBDIR, wxID_VCHECKDIRPANELGAGPROC, 
 wxID_VCHECKDIRPANELLBLCALC, wxID_VCHECKDIRPANELLBLCHECK, 
 wxID_VCHECKDIRPANELLBLSHOW, wxID_VCHECKDIRPANELNBDATA, 
 wxID_VCHECKDIRPANELPNCFG, wxID_VCHECKDIRPANELPNDATA, 
 wxID_VCHECKDIRPANELSLWNAV, wxID_VCHECKDIRPANELSPREC, 
 wxID_VCHECKDIRPANELTRDIRSNAP, wxID_VCHECKDIRPANELTRLSTRES, 
 wxID_VCHECKDIRPANELTXTSNAP, 
] = [wx.NewId() for _init_ctrls in range(35)]

def getPluginImage():
    return vCheckDirImages.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(vCheckDirImages.getPluginBitmap())
    return icon

class vCheckDirPanel(wx.Panel):
    def _init_coll_bxsBrowse_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCalcSrc, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbCalcSrc2, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_coll_gbsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCheck, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chkAccess, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chkMod, (2, 2), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.chkCreate, (3, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblCalc, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblShow, (0, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chkCalcMD5, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chkFlat, (2, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcShowEqual, (1, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.spRec, (2, 0), border=0, flag=wx.EXPAND, span=(1, 1))

    def _init_coll_gbsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)
        parent.AddGrowableCol(3)
        parent.AddGrowableRow(6)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbRefreshDir, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbSetSnap, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtSnap, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.cbCmp, (1, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.cbSnapShot, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbSaveDir, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.trlstRes, (3, 0), border=0, flag=wx.EXPAND,
              span=(4, 4))
        parent.AddWindow(self.cbOpenSrc1, (3, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbOpenSrc2, (4, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbPopup, (5, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddSizer(self.bxsBrowse, (0, 1), border=0, flag=0,#wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.gagProc, (7, 0), border=0, flag=wx.EXPAND,
              span=(1, 4))

    def _init_coll_gbsCfg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(2)
        parent.AddGrowableCol(4)

    def _init_coll_fgsCfg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbData, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbSetData, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.dbbDir, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbSetDir, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbAddDir, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbDelDir, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.gbsCfg, 0, border=0, flag=wx.EXPAND)

    def _init_coll_trlstRes_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(u'name')
        parent.AddColumn(u'size')
        parent.AddColumn(u'MD5')
        parent.AddColumn(u'S')
        parent.AddColumn(u'M')
        parent.AddColumn(u'tA')
        parent.AddColumn(u'tM')
        parent.AddColumn(u'tC')

    def _init_coll_nbData_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnCfg, select=False,
              text=_(u'config'))
        parent.AddPage(imageId=-1, page=self.pnData, select=True,
              text=_(u'result'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsCfg = wx.FlexGridSizer(cols=3, hgap=4, rows=3, vgap=4)

        self.gbsCfg = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsData = wx.GridBagSizer(hgap=4, vgap=3)

        self.bxsBrowse = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsCfg_Items(self.fgsCfg)
        self._init_coll_fgsCfg_Growables(self.fgsCfg)
        self._init_coll_gbsCfg_Items(self.gbsCfg)
        self._init_coll_gbsCfg_Growables(self.gbsCfg)
        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_gbsData_Growables(self.gbsData)
        self._init_coll_bxsBrowse_Items(self.bxsBrowse)

        self.pnData.SetSizer(self.gbsData)
        self.pnCfg.SetSizer(self.fgsCfg)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VCHECKDIRPANEL, name=u'vCheckDirPanel',
              parent=prnt, pos=wx.Point(53, 18), size=wx.Size(772, 332),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(764, 305))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VCHECKDIRPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              304), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.SetExtraBorderSize(0)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VCHECKDIRPANELSLWNAV)

        self.trDirSnap = wx.TreeCtrl(id=wxID_VCHECKDIRPANELTRDIRSNAP,
              name=u'trDirSnap', parent=self.slwNav, pos=wx.Point(0, 0),
              size=wx.Size(197, 304), style=wx.TR_HAS_BUTTONS)
        self.trDirSnap.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrDirSnapTreeSelChanged, id=wxID_VCHECKDIRPANELTRDIRSNAP)

        self.nbData = wx.Notebook(id=wxID_VCHECKDIRPANELNBDATA, name=u'nbData',
              parent=self, pos=wx.Point(200, 8), size=wx.Size(560, 296),
              style=wx.SUNKEN_BORDER)

        self.pnCfg = wx.Panel(id=wxID_VCHECKDIRPANELPNCFG, name=u'pnCfg',
              parent=self.nbData, pos=wx.Point(0, 0), size=wx.Size(552, 270),
              style=wx.TAB_TRAVERSAL)

        self.dbbData = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=u'Choose a directory', id=wxID_VCHECKDIRPANELDBBDATA,
              labelText=_(u'data directory:'), parent=self.pnCfg,
              pos=wx.Point(0, 0), size=wx.Size(392, 30), startDirectory='.',
              style=0)

        self.cbSetData = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VCHECKDIRPANELCBSETDATA, label=_(u'Apply'),
              name=u'cbSetData', parent=self.pnCfg, pos=wx.Point(396, 0),
              size=wx.Size(76, 30), style=0)
        self.cbSetData.SetMinSize((-1 , -1))
        self.cbSetData.Bind(wx.EVT_BUTTON, self.OnCbSetDataButton,
              id=wxID_VCHECKDIRPANELCBSETDATA)

        self.dbbDir = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'), id=wxID_VCHECKDIRPANELDBBDIR,
              labelText=_(u'directory to check:'), parent=self.pnCfg,
              pos=wx.Point(0, 46), size=wx.Size(392, 34), startDirectory='.',
              style=0)

        self.cbSetDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VCHECKDIRPANELCBSETDIR, label=u'Set', name=u'cbSetDir',
              parent=self.pnCfg, pos=wx.Point(396, 46), size=wx.Size(76, 34),
              style=0)
        self.cbSetDir.SetMinSize(wx.Size(-1, -1))
        self.cbSetDir.Bind(wx.EVT_BUTTON, self.OnCbSetDirButton,
              id=wxID_VCHECKDIRPANELCBSETDIR)

        self.cbAddDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VCHECKDIRPANELCBADDDIR, label=_(u'Add'), name=u'cbAddDir',
              parent=self.pnCfg, pos=wx.Point(476, 46), size=wx.Size(76, 34),
              style=0)
        self.cbAddDir.SetMinSize((-1 , -1))
        self.cbAddDir.Bind(wx.EVT_BUTTON, self.OnCbAddDirButton,
              id=wxID_VCHECKDIRPANELCBADDDIR)

        self.cbDelDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VCHECKDIRPANELCBDELDIR, label=_(u'Delete'),
              name=u'cbDelDir', parent=self.pnCfg, pos=wx.Point(396, 84),
              size=wx.Size(76, 30), style=0)
        self.cbDelDir.SetMinSize((-1 , -1))
        self.cbDelDir.Bind(wx.EVT_BUTTON, self.OnCbDelDirButton,
              id=wxID_VCHECKDIRPANELCBDELDIR)

        self.pnData = wx.Panel(id=wxID_VCHECKDIRPANELPNDATA, name=u'pnData',
              parent=self.nbData, pos=wx.Point(0, 0), size=wx.Size(552, 270),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.cbRefreshDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Measure),
              id=wxID_VCHECKDIRPANELCBREFRESHDIR, label=_(u'Calculate'),
              name=u'cbRefreshDir', parent=self.pnData, pos=wx.Point(0, 0),
              size=wx.Size(94, 30), style=0)
        self.cbRefreshDir.SetMinSize((-1 , -1))
        self.cbRefreshDir.Bind(wx.EVT_BUTTON, self.OnCbRefreshDirButton,
              id=wxID_VCHECKDIRPANELCBREFRESHDIR)

        self.cbCalcSrc = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Measure),
              id=wxID_VCHECKDIRPANELCBCALCSRC, label=_(u'browse source 1'),
              name=u'cbCalcSrc', parent=self.pnData, pos=wx.Point(118, 0),
              size=wx.Size(162, 30), style=0)
        self.cbCalcSrc.Bind(wx.EVT_BUTTON, self.OnCbCalcSrcButton,
              id=wxID_VCHECKDIRPANELCBCALCSRC)

        self.cbCalcSrc2 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Report),
              id=wxID_VCHECKDIRPANELCBCALCSRC2, label=_(u'browse source 2'),
              name=u'cbCalcSrc2', parent=self.pnData, pos=wx.Point(305, 0),
              size=wx.Size(162, 30), style=0)
        self.cbCalcSrc2.Bind(wx.EVT_BUTTON, self.OnCbCalcSrc2Button,
              id=wxID_VCHECKDIRPANELCBCALCSRC2)

        self.gagProc = wx.Gauge(id=wxID_VCHECKDIRPANELGAGPROC, name=u'gagProc',
              parent=self.pnData, pos=wx.Point(0, 246), range=1000,
              size=wx.Size(486, 20), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.gagProc.SetMinSize(wx.Size(-1, 12))

        self.trlstRes = vidarc.tool.misc.vtmTreeListCtrl.vtmTreeListCtrl(id=wxID_VCHECKDIRPANELTRLSTRES,
              name=u'trlstRes', parent=self.pnData, pos=wx.Point(0, 99),
              size=wx.Size(486, 144), style=wx.TR_HAS_BUTTONS|wx.TR_SINGLE)
        self.trlstRes.SetMinSize((-1 , -1))
        self._init_coll_trlstRes_Columns(self.trlstRes)
        self.trlstRes.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrlstResTreeSelChanged, id=wxID_VCHECKDIRPANELTRLSTRES)
        self.trlstRes.Bind(wx.EVT_TREE_ITEM_EXPANDING,
              self.OnTrlstResTreeItemExpanding, id=wxID_VCHECKDIRPANELTRLSTRES)

        self.chkCreate = wx.CheckBox(id=wxID_VCHECKDIRPANELCHKCREATE,
              label=_(u'create time'), name=u'chkCreate', parent=self.pnCfg,
              pos=wx.Point(90, 222), size=wx.Size(123, 20), style=0)
        self.chkCreate.SetValue(False)

        self.chkAccess = wx.CheckBox(id=wxID_VCHECKDIRPANELCHKACCESS,
              label=_(u'access time'), name=u'chkAccess', parent=self.pnCfg,
              pos=wx.Point(90, 154), size=wx.Size(123, 30), style=0)
        self.chkAccess.SetValue(False)

        self.chkMod = wx.CheckBox(id=wxID_VCHECKDIRPANELCHKMOD,
              label=_(u'modification time'), name=u'chkMod', parent=self.pnCfg,
              pos=wx.Point(90, 188), size=wx.Size(123, 30), style=0)
        self.chkMod.SetValue(False)

        self.lblCheck = wx.StaticText(id=wxID_VCHECKDIRPANELLBLCHECK,
              label=_(u'check'), name=u'lblCheck', parent=self.pnCfg,
              pos=wx.Point(90, 130), size=wx.Size(123, 20), style=0)

        self.cbSetSnap = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Pin),
              id=wxID_VCHECKDIRPANELCBSETSNAP, label=u'set', name=u'cbSetSnap',
              parent=self.pnData, pos=wx.Point(0, 33), size=wx.Size(94, 30),
              style=0)
        self.cbSetSnap.SetMinSize((-1 , -1))
        self.cbSetSnap.Enable(False)
        self.cbSetSnap.Bind(wx.EVT_BUTTON, self.OnCbSetSource1Button,
              id=wxID_VCHECKDIRPANELCBSETSNAP)

        self.txtSnap = wx.TextCtrl(id=wxID_VCHECKDIRPANELTXTSNAP,
              name=u'txtSnap', parent=self.pnData, pos=wx.Point(98, 33),
              size=wx.Size(302, 30), style=wx.TE_READONLY, value=u'')

        self.cbCmp = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Report),
              id=wxID_VCHECKDIRPANELCBCMP, label=u'compare', name=u'cbCmp',
              parent=self.pnData, pos=wx.Point(404, 33), size=wx.Size(82, 30),
              style=0)
        self.cbCmp.SetMinSize((-1 , -1))
        self.cbCmp.Bind(wx.EVT_BUTTON, self.OnCbCmpButton,
              id=wxID_VCHECKDIRPANELCBCMP)

        self.cbSaveDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VCHECKDIRPANELCBSAVEDIR, label=_(u'Save Result'),
              name=u'cbSaveDir', parent=self.pnData, pos=wx.Point(0, 66),
              size=wx.Size(94, 30), style=0)
        self.cbSaveDir.SetMinSize((-1 , -1))
        self.cbSaveDir.Bind(wx.EVT_BUTTON, self.OnCbSaveDirButton,
              id=wxID_VCHECKDIRPANELCBSAVEDIR)

        self.cbSnapShot = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Zip),
              id=wxID_VCHECKDIRPANELCBSNAPSHOT, label=u'snap shot',
              name=u'cbSnapShot', parent=self.pnData, pos=wx.Point(98, 66),
              size=wx.Size(149, 30), style=0)
        self.cbSnapShot.Bind(wx.EVT_BUTTON, self.OnCbSnapShotButton,
              id=wxID_VCHECKDIRPANELCBSNAPSHOT)

        self.lblCalc = wx.StaticText(id=wxID_VCHECKDIRPANELLBLCALC,
              label=_(u'calculation'), name=u'lblCalc', parent=self.pnCfg,
              pos=wx.Point(0, 130), size=wx.Size(72, 20), style=0)

        self.lblShow = wx.StaticText(id=wxID_VCHECKDIRPANELLBLSHOW,
              label=_(u'show'), name=u'lblShow', parent=self.pnCfg,
              pos=wx.Point(231, 130), size=wx.Size(159, 20), style=0)

        self.chkCalcMD5 = wx.CheckBox(id=wxID_VCHECKDIRPANELCHKCALCMD5,
              label=u'MD5', name=u'chkCalcMD5', parent=self.pnCfg,
              pos=wx.Point(0, 154), size=wx.Size(72, 30), style=0)
        self.chkCalcMD5.SetValue(True)

        self.spRec = wx.SpinCtrl(id=wxID_VCHECKDIRPANELSPREC, initial=0,
              max=100, min=0, name=u'spRec', parent=self.pnCfg, pos=wx.Point(0,
              188), size=wx.Size(60, 21), style=wx.SP_ARROW_KEYS)
        self.spRec.SetToolTipString(_(u'recursion levels'))
        self.spRec.SetMinSize((60 , -1))
        self.spRec.SetMaxSize((60 , -1))

        self.chkFlat = wx.CheckBox(id=wxID_VCHECKDIRPANELCHKFLAT, label=u'flat',
              name=u'chkFlat', parent=self.pnCfg, pos=wx.Point(231, 188),
              size=wx.Size(159, 30), style=0)
        self.chkFlat.SetValue(False)

        self.chcShowEqual = wx.CheckBox(id=wxID_VCHECKDIRPANELCHCSHOWEQUAL,
              label=u'showEqual', name=u'chcShowEqual', parent=self.pnCfg,
              pos=wx.Point(231, 154), size=wx.Size(159, 30), style=0)
        self.chcShowEqual.SetValue(False)

        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Down),
              id=wxID_VCHECKDIRPANELCBPOPUP, name=u'cbPopup',
              parent=self.pnData, pos=wx.Point(490, 165), size=wx.Size(56, 30),
              style=0)
        self.cbPopup.Bind(wx.EVT_BUTTON, self.OnPopupButton,
              id=wxID_VCHECKDIRPANELCBPOPUP)

        self.cbOpenSrc1 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Browse),
              id=wxID_VCHECKDIRPANELCBOPENSRC1, label=u'1', name=u'cbOpenSrc1',
              parent=self.pnData, pos=wx.Point(490, 99), size=wx.Size(56, 30),
              style=0)
        self.cbOpenSrc1.Bind(wx.EVT_BUTTON, self.OnCbOpenSrc1Button,
              id=wxID_VCHECKDIRPANELCBOPENSRC1)

        self.cbOpenSrc2 = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Browse),
              id=wxID_VCHECKDIRPANELCBOPENSRC2, label=u'2', name=u'cbOpenSrc2',
              parent=self.pnData, pos=wx.Point(490, 132), size=wx.Size(56, 30),
              style=0)
        self.cbOpenSrc2.Bind(wx.EVT_BUTTON, self.OnCbOpenSrc2Button,
              id=wxID_VCHECKDIRPANELCBOPENSRC2)

        self._init_coll_nbData_Pages(self.nbData)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vToolsCheckDir')
        self._init_ctrls(parent)
        self.SetName(name)
        self.cfgFunc=None
        self.dCfg={}
        self.xdCfg=vtXmlDom(appl='vCheckDirCfg',audit_trail=False)
        
        self.lDN=[]
        self.baseDN='.'
        self.tiSel=None
        self.tiResSel=None
        self.dirEntrySrc1=None
        self.dirEntrySrc2=None
        self.thdRead=vtInOutReadThread(self,True)
        self.thdRead.BindEvents(self.OnInOutReadProc,
                            self.OnInOutReadFin,
                            self.OnInOutReadAbort)
        #EVT_VTINOUT_READ_THREAD_ELEMENTS(self,self.OnInOutReadProc)
        #EVT_VTINOUT_READ_THREAD_FINISHED(self,self.OnInOutReadFin)
        #EVT_VTINOUT_READ_THREAD_ABORTED(self,self.OnInOutReadAbort)
        
        #self.thdProc=vtThreadWX(self,bPost=False)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        idxNrm=self.imgLst.Add(vCheckDirImages.getDirCloseBitmap())
        idxSel=self.imgLst.Add(vCheckDirImages.getDirOpenBitmap())
        self.dImg['Dir']=[idxNrm,idxSel]
        idxNrm=self.imgLst.Add(vCheckDirImages.getSnapBitmap())
        self.dImg['Snap']=[idxNrm,idxNrm]
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Cancel))
        self.dImg['qst']=self.imgLst.Add(vtArt.getBitmap(vtArt.Question))
        self.dImg['wrn']=self.imgLst.Add(vtArt.getBitmap(vtArt.Warning))
        self.dImg['file']=self.imgLst.Add(vtArt.getBitmap(vtArt.Element))
        self.dImg['Cal']=self.imgLst.Add(vtArt.getBitmap(vtArt.Measure))
        self.dImg['Cmp']=self.imgLst.Add(vtArt.getBitmap(vtArt.Report))
        self.trDirSnap.SetImageList(self.imgLst)
        t=self.dImg['Dir']
        self.trDirSnap.AddRoot('Root',t[0],t[1])
        
        if wx.VERSION >= (2,8):
            iAlignFlag=wx.ALIGN_RIGHT
        else:
            iAlignFlag=wx.LIST_FORMAT_RIGHT
        self.trlstRes.SetColumnAlignment(1,iAlignFlag)
        self.trlstRes.SetColumnAlignment(3,iAlignFlag)
        self.trlstRes.SetColumnAlignment(4,iAlignFlag)
        self.trlstRes.SetColumnAlignment(5,iAlignFlag)
        self.trlstRes.SetColumnAlignment(6,iAlignFlag)
        self.trlstRes.SetColumnAlignment(7,iAlignFlag)
        self.trlstRes.SetColumnWidth(1,80)
        self.trlstRes.SetColumnWidth(2,80)
        self.trlstRes.SetColumnWidth(3,30)
        self.trlstRes.SetColumnWidth(4,30)
        self.trlstRes.SetColumnWidth(5,30)
        self.trlstRes.SetColumnWidth(6,30)
        self.trlstRes.SetColumnWidth(7,30)
        self.trlstRes.SetStretchLst([(0,-1)])
        self.trlstRes.SetImageList(self.imgLst)
        
        self.popWin=vCheckDirResultDialog(self)
        self.bBusy=False
        self.widLogging=vtLog.GetPrintMsgWid(self)
        self.cbOpenSrc1.Enable(False)
        self.cbOpenSrc2.Enable(False)
        self.popWin.SetSrc1(None,'','')
        self.popWin.SetSrc2(None,'','')
        self.dlgFile=None
        self.spRec.SetValue(10)
        #self.Move(pos)
        #self.SetSize(size)
        #parent.Bind(wx.EVT_CLOSE, self.OnFrmClose)
    def GetDocMain(self):
        return None
    def GetNetMaster(self):
        return False
    def GetTreeMain(self):
        return None
    def OnInOutReadProc(self,evt):
        self.gagProc.SetValue(evt.GetNormalized())
        return
        iSize=evt.GetCount()
        iVal=evt.GetValue()
        try:
            fVal=int(iVal/float(iSize)*1000.0)
        except:
            fVal=0
        self.gagProc.SetValue(evt.GetNormalized())
    def OnInOutReadFin(self,evt):
        self.gagProc.SetValue(0)
    def OnInOutReadAbort(self,evt):
        self.gagProc.SetValue(0)
    def OnFrmClose(self,evt):
        self.GetCfgData()
        evt.Skip()
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.nbData)
            self.nbData.Refresh()
            #self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            bMod=False
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
            wx.LayoutAlgorithm().LayoutWindow(self, self.nbData)
            self.nbData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMove(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            #pos=self.GetParent().GetPosition()
            #self.dCfg['pos']='%d,%d'%(pos[0],pos[1])
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                return
        self.xdCfg.New(root='config')
    # ----------------------------------------
    # plugin interface for vMESCenter
    # ----------------------------------------
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(d),self)
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        #try:
        #    pos=map(int,self.dCfg['pos'].split(','))
        #    self.GetParent().Move(pos)
        #except:
        #    pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
        self.lDN=[]
        self.baseDN='.'
        try:
            keys=self.dCfg.keys()
            for k in keys:
                if k[:2].find('DN')==0:
                    if len(k)==2:
                        self.baseDN=self.dCfg[k]
                    else:
                        self.lDN.append(self.dCfg[k])
        except:
            pass
        self.__setTreeDN__()
    def GetCfgData(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
        keys=self.dCfg.keys()
        for k in keys:
            if k[:2].find('DN')==0:
                del self.dCfg[k]
        self.dCfg['DN']=self.baseDN
        self.lDN.sort()
        i=0
        for sDN in self.lDN:
            self.dCfg['DN_%03d'%i]=sDN
            i+=1
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def __readSnap__(self):
        try:
            self.dSnap={}
            l=[]
            for s in self.lDN:
                sFN=replaceSuspectChars(s)
                l.append(sFN)
            files=os.listdir(self.baseDN)
            for f in files:
                sFN=os.path.join(self.baseDN,f)
                st=os.stat(sFN)
                if stat.S_ISREG(st[stat.ST_MODE]):
                    try:
                        i=l.index(f[:-20])
                        if self.dSnap.has_key(f[:-20]):
                            self.dSnap[f[:-20]].append(f)
                        else:
                            self.dSnap[f[:-20]]=[f]
                    except:
                        pass
        except:
            vtLog.vtLngTB(self.GetName())
    def __setTreeDN__(self):
        try:
            self.__readSnap__()
            vtLog.vtLngCurWX(vtLog.DEBUG,self.lDN,self)
            self.trDirSnap.DeleteAllItems()
            self.cbSetSnap.Enable(False)
            self.txtSnap.SetValue(u'')
            t=self.dImg['Dir']
            tSnap=self.dImg['Snap']
            self.dbbData.SetValue(self.baseDN)
            tiRoot=self.trDirSnap.AddRoot(self.baseDN,t[0],t[1])
            self.lDN.sort()
            for sDN in self.lDN:
                t=self.dImg['Dir']
                ti=self.trDirSnap.AppendItem(tiRoot,sDN,t[0],t[1])
                try:
                    l=self.dSnap[replaceSuspectChars(sDN)]
                    for s in l:
                        t=xml2ftime(s[-19:-4])
                        tLocal=time.localtime(t)
                        sDt=time.strftime("%Y-%m-%d %H:%M:%S",tLocal)
                        tid=wx.TreeItemData()
                        tid.SetData(s)
                        tic=self.trDirSnap.AppendItem(ti,sDt,tSnap[0],-1,tid)
                        
                except:
                    pass
            self.trDirSnap.Expand(tiRoot)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbAddDirButton(self, event):
        event.Skip()
        try:
            sDN=self.dbbDir.GetValue().replace('\\','/')
            vtLog.vtLngCurWX(vtLog.DEBUG,sDN,self)
            try:
                sDNSel=self.trDirSnap.GetItemText(self.tiSel)
            except:
                sDNSel=None
            try:
                i=self.lDN.index(sDN)
                return
            except:
                self.lDN.append(sDN)
            tiRoot=self.trDirSnap.GetRootItem()
            t=self.dImg['Dir']
            ti=self.trDirSnap.AppendItem(tiRoot,sDN,t[0],t[1])
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSetDirButton(self, event):
        event.Skip()
        try:
            sDN=self.dbbDir.GetValue().replace('\\','/')
            vtLog.vtLngCurWX(vtLog.DEBUG,sDN,self)
            if self.tiSel is None:
                return
            sDNSel=self.trDirSnap.GetItemText(self.tiSel)
            self.trDirSnap.SetItemText(self.tiSel,sDN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDelDirButton(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if self.tiSel is None:
                return
            if self.tiSel==self.trDirSnap.GetRootItem():
                return
            sDNSel=self.trDirSnap.GetItemText(self.tiSel)
            dlg=vtmMsgDialog(self,_(u'Delete %s?')%sDNSel,'vCheckDir',
                            wx.YES_NO|wx.ICON_QUESTION)
            dlg.Centre()
            iRet=dlg.ShowModal()
            dlg.Destroy()
            if iRet!=wx.ID_YES:
                return
            sFN=self.trDirSnap.GetPyData(self.tiSel)
            if sFN is None:
                # remove directory
                if sDNSel in self.lDN:
                    self.lDN.remove(sDNSel)
            else:
                # remove snapshot
                os.remove(os.path.join(self.baseDN,sFN))
            self.trDirSnap.Delete(self.tiSel)
            self.tiSel=None
            wx.CallAfter(self.getTreeSel,self.trDirSnap)
        except:
            vtLog.vtLngTB(self.GetName())
    def getTreeSel(self,tr):
        self.tiSel=tr.GetSelection()
    def refreshSel(self,dn,dirEntry,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        self.thdRead.DoCallBack(self.showRes,dn,dirEntry)
    def showRes(self,dn,dirEntry):
        self.dirEntrySrc1=dirEntry
        imgIdx=self.dImg['Cal']
        tiRoot=self.trlstRes.AddRoot(dn,imgIdx,imgIdx)
        self.trlstRes.SetPyData(tiRoot,[dirEntry,1,False])
        sz=listFiles.getSizeStr(dirEntry.size,2)
        self.trlstRes.SetItemText(tiRoot,sz,1)
        self.showResSub(self.trlstRes,tiRoot)
        self.trlstRes.Expand(tiRoot)
    def showResSub(self,trlst,ti):
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iSkip=len(dE.base)+1
            t=self.dImg['Dir']
            for d in dE.dirs:
                tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetPyData(tc,[d,1,False])
                sz=listFiles.getSizeStr(d.size,2)
                trlst.SetItemText(tc,sz,1)
                trlst.SetItemHasChildren(tc,True)
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
            tf=trlst.AppendItem(ti,'.')
            trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
            trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
            t=self.dImg['file']
            for f in dE.files:
                tc=trlst.AppendItem(tf,f.name)
                trlst.SetPyData(tc,[f,4,True])
                sz=listFiles.getSizeStr(f.size,2)
                md5=f.md5 or ''
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
                trlst.SetItemText(tc,sz,1)
                trlst.SetItemText(tc,md5,2)
        except:
            vtLog.vtLngTB(self.GetName())
    def showCmp(self,dn,dirCmp,bEqual):
        imgIdx=self.dImg['Cmp']
        tiRoot=self.trlstRes.AddRoot(dn,imgIdx,imgIdx)
        self.trlstRes.SetPyData(tiRoot,[dirCmp,2,False])
        self.showCmpSub(self.trlstRes,tiRoot,bEqual)
        self.trlstRes.Expand(tiRoot)
    def showCmpSub(self,trlst,ti,bEqual):
        try:
            l=trlst.GetPyData(ti)
            if l is None:
                return
            if l[2]==True:
                return
            dE=l[0]
            l[2]=True
            if dE is None:
                return
            iSkip=len(dE.base)#+1
            t=self.dImg['Dir']
            for d in dE.dir_results:
                bAdd=False
                for f in d.file_results:
                    if f.isEqual():
                        if bEqual==False:
                            continue
                    bAdd=True
                if len(d.dir_results)==0 and bAdd==False:
                    continue
                if d.base[iSkip]=='/':
                    tc=trlst.AppendItem(ti,d.base[iSkip+1:])
                else:
                    tc=trlst.AppendItem(ti,d.base[iSkip:])
                trlst.SetItemImage(tc, t[0], which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t[1], which = wx.TreeItemIcon_Expanded)
                trlst.SetPyData(tc,[d,2,False])
                trlst.SetItemHasChildren(tc,True)
            tf=trlst.AppendItem(ti,'.')
            trlst.SetItemImage(tf, t[0], which = wx.TreeItemIcon_Normal)
            trlst.SetItemImage(tf, t[1], which = wx.TreeItemIcon_Expanded)
            bAdd=False
            for f in dE.file_results:
                t=self.dImg['wrn']
                if f.isEqual():
                    if bEqual==False:
                        continue
                    t=self.dImg['ok']
                bAdd=True
                name=f.getName()
                iSz=f.getSize()
                if iSz<0:
                    sz=''
                else:
                    sz=listFiles.getSizeStr(iSz,2)
                md5=f.getMD5()
                tc=trlst.AppendItem(tf,name)
                trlst.SetPyData(tc,[f,3,True])
                trlst.SetItemText(tc,sz,1)
                trlst.SetItemText(tc,md5,2)
                if f.isDiffSize():
                    trlst.SetItemText(tc,'X',3)
                if f.isDiffContent():
                    trlst.SetItemText(tc,'X',4)
                if f.isDiffAccess():
                    trlst.SetItemText(tc,'X',5)
                if f.isDiffModify():
                    trlst.SetItemText(tc,'X',6)
                if f.isDiffCreate():
                    trlst.SetItemText(tc,'X',7)
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Normal)
                trlst.SetItemImage(tc, t, which = wx.TreeItemIcon_Expanded)
            if bAdd==False:
                trlst.Delete(tf)
        except:
            vtLog.vtLngTB(self.GetName())
    def getTreeLabel(self,trlst,ti):
        tp=ti
        tr=trlst.GetRootItem()
        l=[]
        while tp is not None and tp!=tr:
            l.append(trlst.GetItemText(tp))
            tp=trlst.GetItemParent(tp)
        l.reverse()
        return l
    def findTreeItem(self,trlst,ti,sLbl,bRec=False):
        tp=ti
        t=trlst.GetFirstChild(ti)
        while t[0].IsOk():
            if trlst.GetItemText(t[0])==sLbl:
                return t[0]
            t=trlst.GetNextChild(t[0],t[1])
        return None
    def enableResWid(self,trlst,ti):
        try:
            if ti is None:
                pass
            else:
                l=trlst.GetPyData(ti)
                if l is None:
                    pass
                elif l[1]==3:
                    f=l[0]
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=u'/'.join(l[:-1])
                    if f.hasSrc1():
                        self.cbOpenSrc1.Enable(True)
                        self.popWin.SetSrc1(f.file1,self.dirEntrySrc1.base,sRelDN)
                    else:
                        self.cbOpenSrc1.Enable(False)
                        self.popWin.SetSrc1(None,'','')
                    if f.hasSrc2():
                        if self.dirEntrySrc1.base==self.dirEntrySrc2.base:
                            self.cbOpenSrc2.Enable(False)
                            self.popWin.SetSrc2(None,'','')
                        else:
                            self.cbOpenSrc2.Enable(True)
                        self.popWin.SetSrc2(f.file2,self.dirEntrySrc2.base,sRelDN)
                    else:
                        self.cbOpenSrc2.Enable(False)
                        self.popWin.SetSrc2(None,'','')
                    return
                elif l[1]==4:
                    f=l[0]
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=u'/'.join(l[:-1])
                    self.cbOpenSrc1.Enable(True)
                    self.popWin.SetSrc1(f,self.dirEntrySrc1.base,sRelDN)
                    self.cbOpenSrc2.Enable(False)
                    self.popWin.SetSrc2(None,'','')
                    return
            self.cbOpenSrc1.Enable(False)
            self.cbOpenSrc2.Enable(False)
            self.popWin.SetSrc1(None,'','')
            self.popWin.SetSrc2(None,'','')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTrlstResTreeItemExpanding(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            l=self.trlstRes.GetPyData(ti)
            if l is None:
                return
            if l[1]==1:
                self.showResSub(self.trlstRes,ti)
            elif l[1]==2:
                bEqual=self.chcShowEqual.GetValue()
                self.showCmpSub(self.trlstRes,ti,bEqual)
        except:
            vtLog.vtLngTB(self.GetName())
    def refreshComp(self,dn,dirCmp):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        bEqual=self.chcShowEqual.GetValue()
        self.thdRead.DoCallBack(self.showCmp,dn,dirCmp,bEqual)
    def OnCbRefreshDirButton(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.trlstRes.DeleteAllItems()
            self.cbOpenSrc1.Enable(False)
            self.cbOpenSrc2.Enable(False)
            self.popWin.SetSrc1(None,'','')
            self.popWin.SetSrc2(None,'','')
            if self.tiSel is None:
                return
            if self.tiSel==self.trDirSnap.GetRootItem():
                return
            sDNSel=self.trDirSnap.GetItemText(self.tiSel)
            vtLog.vtLngCurWX(vtLog.INFO,'dn:%s'%(sDNSel),self)
            iStyleRd=0
            if self.chkCalcMD5.GetValue():
                iStyleRd|=0x01
            iRecLv=self.spRec.GetValue()
            self.thdRead.Read(sDNSel,None,iRecLv,0,iStyleRd,self.refreshSel)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSaveDirButton(self, event):
        event.Skip()
        try:
            if self.dirEntrySrc1 is not None:
                t=time.gmtime(time.time())
                s=time.strftime("%Y%m%d_%H%M%S",t)
                sFN=replaceSuspectChars(self.dirEntrySrc1.base+'+'+s+'.xml')
                dirEntry=DirEntryXml()
                dirEntry.buildXml(self.dirEntrySrc1)
                sFullFN=os.path.join(self.baseDN,sFN)
                dirEntry.write(sFullFN,0)
                if self.dSnap.has_key(sFN[:-20]):
                    self.dSnap[sFN[:-20]].append(sFN)
                else:
                    self.dSnap[sFN[:-20]]=[sFN]
                ti=self.findTreeItem(self.trDirSnap,self.trDirSnap.GetRootItem(),
                            self.dirEntrySrc1.base)
                if ti is None:
                    return
                #if self.tiSel==self.trDirSnap.GetRootItem():
                #    return
                t=xml2ftime(sFN[-19:-4])
                tLocal=time.localtime(t)
                sDt=time.strftime("%Y-%m-%d %H:%M:%S",tLocal)
                tid=wx.TreeItemData()
                tid.SetData(sFN)
                tSnap=self.dImg['Snap']
                tic=self.trDirSnap.AppendItem(ti,sDt,tSnap[0],-1,tid)
                self.trDirSnap.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTrDirSnapTreeSelChanged(self, event):
        event.Skip()
        try:
            self.tiSel=event.GetItem()
            if self.tiSel is None:
                #self.txtSnap.SetValue(u'')
                self.cbSetSnap.Enable(False)
                return
            s=self.trDirSnap.GetItemText(self.tiSel)
            vtLog.vtLngCurWX(vtLog.INFO,'lbl:%s'%(s),self)
            fn=self.trDirSnap.GetPyData(self.tiSel)
            if fn is None:
                #self.txtSnap.SetValue(u'')
                self.cbSetSnap.Enable(False)
            elif fn[-4:].lower()=='.xml':
                #self.txtSnap.SetValue(fn)
                self.cbSetSnap.Enable(True)
            else:
                #self.txtSnap.SetValue(u'')
                self.cbSetSnap.Enable(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSetSource1Button(self, event):
        event.Skip()
        try:
            fn=self.trDirSnap.GetPyData(self.tiSel)
            if fn is None:
                self.txtSnap.SetValue(u'')
            elif fn[-4:].lower()=='.xml':
                self.txtSnap.SetValue(fn)
            else:
                self.txtSnap.SetValue(u'')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCmpButton(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            dirEntry=DirEntryXml()
            if self.tiSel is None:
                return
            #fn=self.trDirSnap.GetPyData(self.tiSel)
            fn=self.txtSnap.GetValue()
            dirEntry.read(os.path.join(self.baseDN,fn))
            self.dirEntrySrc2=dirEntry
            self.dirComp=vtInOutCompDirEntry()
            iStyle=0x08
            if self.chkCreate.GetValue():
                iStyle|=0x04
            if self.chkMod.GetValue():
                iStyle|=0x02
            if self.chkAccess.GetValue():
                iStyle|=0x01
            self.dirComp.compareHier(self.dirEntrySrc1,dirEntry,iStyle)
            self.refreshComp(self.dirEntrySrc1.base,self.dirComp)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbSetSource2Button(self, event):
        event.Skip()

    def OnCbSnapShotButton(self, event):
        event.Skip()

    def OnCbSetDataButton(self, event):
        event.Skip()
        try:
            sDN=self.dbbData.GetValue().replace('\\','/')
            vtLog.vtLngCurWX(vtLog.DEBUG,sDN,self)
            ti=self.trDirSnap.GetRootItem()
            if ti is None:
                return
            self.trDirSnap.SetItemText(self.tiSel,sDN)
            self.baseDN=sDN
        except:
            vtLog.vtLngTB(self.GetName())
    def __openFile__(self,sDN,sFN):
        sExts=sFN.split('.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        filename=os.path.join(sDN,sFN)
        try:
            mime = fileType.GetMimeType()
            if sys.platform=='win32':
                filename=filename.replace('/','\\')
            cmd = fileType.GetOpenCommand(filename, mime)
            wx.Execute(cmd)
            vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
            try:
                vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
            except:
                pass
        except:
            sMsg=u"Cann't open %s!"%(filename)
            dlg=vtmMsgDialog(self,sMsg ,
                                u'vCheckDirPanel',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
    def OnCbOpenSrc1Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==3:
                    f=l[0]
                    if f.hasSrc1()==False:
                        return
                    sFN=f.file1.name
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=os.sep.join(l[:-1])
                    sDN=os.path.join(self.dirEntrySrc1.base,sRelDN)
                elif l[1]==4:
                    f=l[0]
                    sFN=f.name
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=os.sep.join(l[:-1])
                    sDN=os.path.join(self.dirEntrySrc1.base,sRelDN)
                else:
                    return
                self.__openFile__(sDN,sFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbOpenSrc2Button(self, event):
        event.Skip()
        try:
            trlst=self.trlstRes
            ti=trlst.GetSelection()
            if ti is not None:
                l=trlst.GetPyData(ti)
                if l is None:
                    return
                elif l[1]==3:
                    f=l[0]
                    if f.hasSrc2()==False:
                        return
                    sFN=f.file2.name
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=os.sep.join(l[:-1])
                    sDN=os.path.join(self.dirEntrySrc2.base,sRelDN)
                elif l[1]==4:
                    f=l[0]
                    sFN=f.name
                    l=self.getTreeLabel(trlst,trlst.GetItemParent(ti))
                    sRelDN=os.sep.join(l[:-1])
                    sDN=os.path.join(self.dirEntrySrc1.base,sRelDN)
                else:
                    return
                self.__openFile__(sDN,sFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnPopupButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
    def OnTrlstResTreeSelChanged(self, event):
        event.Skip()
        try:
            ti=event.GetItem()
            self.enableResWid(self.trlstRes,ti)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCalcSrcButton(self, event):
        event.Skip()
        try:
            if self.dlgFile is None:
                self.dlgFile = wx.DirDialog(self, _(u"Choose a directory"), ".", 0,name='dlgDirChoose')
                self.dlgFile.Centre()
            if self.dlgFile.ShowModal() == wx.ID_OK:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                self.trlstRes.DeleteAllItems()
                self.cbOpenSrc1.Enable(False)
                self.cbOpenSrc2.Enable(False)
                self.popWin.SetSrc1(None,'','')
                self.popWin.SetSrc2(None,'','')
                sDN=self.dlgFile.GetPath()
                vtLog.vtLngCurWX(vtLog.INFO,'dn:%s'%(sDN),self)
                iStyleRd=0
                if self.chkCalcMD5.GetValue():
                    iStyleRd|=0x01
                iRecLv=self.spRec.GetValue()
                self.thdRead.Read(sDN,None,iRecLv,0,iStyleRd,self.refreshSel)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCalcSrc2Button(self, event):
        event.Skip()
        try:
            self.trlstRes.DeleteAllItems()
            self.enableResWid(self.trlstRes,None)
            if self.dlgFile is None:
                self.dlgFile = wx.DirDialog(self, _(u"Choose a directory"), ".", 0,name='dlgDirChoose')
                self.dlgFile.Centre()
            if self.dlgFile.ShowModal() == wx.ID_OK:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                self.trlstRes.DeleteAllItems()
                self.cbOpenSrc1.Enable(False)
                self.cbOpenSrc2.Enable(False)
                self.popWin.SetSrc1(None,'','')
                self.popWin.SetSrc2(None,'','')
                sDN=self.dlgFile.GetPath()
                #self.dirEntrySrc2=DirEntryXml()
                #self.dirComp=vtInOutCompDirEntry()
                iStyle=0x08
                if self.chkCreate.GetValue():
                    iStyle|=0x04
                if self.chkMod.GetValue():
                    iStyle|=0x02
                if self.chkAccess.GetValue():
                    iStyle|=0x01
                
                vtLog.vtLngCurWX(vtLog.INFO,'dn:%s'%(sDN),self)
                iStyleRd=0
                if self.chkCalcMD5.GetValue():
                    iStyleRd|=0x01
                iRecLv=self.spRec.GetValue()
                self.thdRead.Read(sDN,None,iRecLv,0,iStyleRd,self.refreshSrc2,iStyle)
                #self.thdRead.Do(self.dirEntrySrc2.process,sDN,10,None,None,0,1)
                #self.thdRead.Do(self.dirComp.compareHier,self.dirEntrySrc1,self.dirEntrySrc2,iStyle)
                #self.thdRead.DoCallBack(self.refreshComp,self.dirEntrySrc1.base,self.dirComp)
        except:
            vtLog.vtLngTB(self.GetName())
    def refreshSrc2(self,dn,dE,iStyle):
        try:
            self.dirEntrySrc2=dE
            self.dirComp=vtInOutCompDirEntry()
            self.thdRead.Do(self.dirComp.compareHier,self.dirEntrySrc1,self.dirEntrySrc2,iStyle)
            self.thdRead.DoCallBack(self.refreshComp,self.dirEntrySrc1.base,self.dirComp)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""CheckDir module.

This module generate file information from configurable directories,
and store them as snap shot. 
Snap shots can be used to determine file modification.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Tools CheckDir'),desc,version
