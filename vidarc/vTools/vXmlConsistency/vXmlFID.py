#Boa:FramePanel:vXmlFID
#----------------------------------------------------------------------------
# Name:         vXmlFID.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060718
# CVS-ID:       $Id: vXmlFID.py,v 1.2 2007/05/07 18:48:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import vidarc.tool.xml.vtXmlTreeListFull
import vidarc.tool.xml.vtXmlGrpAttrTreeList
import vidarc.tool.xml.vtXmlGrpTree
import vidarc.tool.xml.vtXmlTree
import wx.lib.buttons
import wx.lib.filebrowsebutton
import time,sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt
import vidarc.vApps.common.vSystem as vSystem
from vidarc.tool.vtThread import vtThread

from vNetDom import vNetDom
from vidarc.tool.net.vNetXmlWxGuiEvents import *

import images as vXmlConsistencyImages

VERBOSE=0

def create(parent):
    return vXmlFID(parent)

[wxID_VXMLFID, wxID_VXMLFIDCBADD, wxID_VXMLFIDCBDEL, wxID_VXMLFIDCBMOD, 
 wxID_VXMLFIDCBOPEN, wxID_VXMLFIDFBBFN, wxID_VXMLFIDLBLFOREIGN, 
 wxID_VXMLFIDLSTFOREIGN, wxID_VXMLFIDPNDATA, wxID_VXMLFIDSLWNAV, 
 wxID_VXMLFIDTHRMOD, wxID_VXMLFIDTRDATA, wxID_VXMLFIDTXTFOREIGN, 
] = [wx.NewId() for _init_ctrls in range(13)]

def getPluginImage():
    return vXmlConsistencyImages.getPluginFIDImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(vXmlConsistencyImages.getApplicationFIDBitmap())
    return icon

class vXmlFID(wx.Panel):
    def _init_coll_bxsTree_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trData, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsForeign_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblForeign, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtForeign, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbFN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.ALIGN_CENTER | wx.TOP)
        parent.AddSizer(self.bxsLst, 0, border=4, flag=wx.EXPAND | wx.TOP)
        parent.AddSizer(self.bxsForeign, 0, border=4, flag=wx.EXPAND | wx.TOP)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOpen, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbMod, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.thrMod, 0, border=0, flag=0)

    def _init_coll_bxsLst_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstForeign, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_lstForeign_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'tag'), width=140)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'foreign'), width=100)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLst = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsForeign = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTree = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLst_Items(self.bxsLst)
        self._init_coll_bxsForeign_Items(self.bxsForeign)
        self._init_coll_bxsTree_Items(self.bxsTree)

        self.pnData.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLFID, name=u'vXmlFID', parent=prnt,
              pos=wx.Point(115, 28), size=wx.Size(665, 269),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(657, 242))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VXMLFIDSLWNAV, name=u'slwNav',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(200, 240),
              style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VXMLFIDSLWNAV)

        self.pnData = wx.Panel(id=wxID_VXMLFIDPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(440, 232),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.fbbFN = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a file'), fileMask='*.xml',
              id=wxID_VXMLFIDFBBFN, labelText=_(u'File Entry:'),
              parent=self.pnData, pos=wx.Point(0, 0), size=wx.Size(436, 29),
              startDirectory='.', style=0)
        self.fbbFN.SetMinSize(wx.Size(-1, -1))

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLFIDCBOPEN,
              bitmap=vtArt.getBitmap(vtArt.Open), label=_(u'Open'),
              name=u'cbOpen', parent=self.pnData, pos=wx.Point(110, 33),
              size=wx.Size(76, 30), style=0)
        self.cbOpen.SetMinSize(wx.Size(-1, -1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VXMLFIDCBOPEN)

        self.cbMod = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLFIDCBMOD,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Modify'),
              name=u'cbMod', parent=self.pnData, pos=wx.Point(218, 33),
              size=wx.Size(76, 30), style=0)
        self.cbMod.SetMinSize(wx.Size(-1, -1))
        self.cbMod.Bind(wx.EVT_BUTTON, self.OnCbModButton, id=wxID_VXMLFIDCBMOD)

        self.lstForeign = wx.ListCtrl(id=wxID_VXMLFIDLSTFOREIGN,
              name=u'lstForeign', parent=self.pnData, pos=wx.Point(0, 67),
              size=wx.Size(397, 127), style=wx.LC_REPORT)
        self.lstForeign.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstForeign_Columns(self.lstForeign)
        self.lstForeign.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstForeignListItemDeselected, id=wxID_VXMLFIDLSTFOREIGN)
        self.lstForeign.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstForeignListItemSelected, id=wxID_VXMLFIDLSTFOREIGN)

        self.lblForeign = wx.StaticText(id=wxID_VXMLFIDLBLFOREIGN,
              label=_(u'foreign'), name=u'lblForeign', parent=self.pnData,
              pos=wx.Point(0, 198), size=wx.Size(194, 30),
              style=wx.ALIGN_RIGHT)
        self.lblForeign.SetMinSize(wx.Size(-1, -1))

        self.txtForeign = wx.TextCtrl(id=wxID_VXMLFIDTXTFOREIGN,
              name=u'txtForeign', parent=self.pnData, pos=wx.Point(198, 198),
              size=wx.Size(198, 30), style=0, value=u'')
        self.txtForeign.SetMinSize(wx.Size(-1, -1))

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLFIDCBADD,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbAdd',
              parent=self.pnData, pos=wx.Point(400, 198), size=wx.Size(31, 30),
              style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton, id=wxID_VXMLFIDCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLFIDCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel',
              parent=self.pnData, pos=wx.Point(401, 67), size=wx.Size(31, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton, id=wxID_VXMLFIDCBDEL)

        self.trData = vidarc.tool.xml.vtXmlTreeListFull.vtXmlTreeListFull(cols=['tag',
              'value'], controller=False, id=wxID_VXMLFIDTRDATA, master=True,
              name=u'trData', parent=self.slwNav, pos=wx.Point(0, 0),
              size=wx.Size(0, 80), style=0)

        self.thrMod = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLFIDTHRMOD,
              name=u'thrMod', parent=self.pnData, pos=wx.Point(310, 33),
              size=wx.Size(15, 15), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vToolsXml')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.iSelForeign=-1
        self.nodeSel=None
        self.sSelTag=''
        self.lTag=[]
        self.thdMod=vtThread(self)
        
        self.doc=vNetDom(self.pnData,'genericDom')
        self.doc.Show(False)
        EVT_NET_XML_OPEN_OK(self.doc,self.OnOpenOk)
        self.bxsBt.AddWindow(self.doc, 0, border=0, flag=0)
        #self.trData.SetNodeInfos(['tag','name'])
        #self.trData.SetGrouping([],[('tag',''),('name','')])
        vidarc.tool.xml.vtXmlTree.EVT_VTXMLTREE_ITEM_SELECTED(self.trData,self.OnItemSel)
        self.trData.SetDoc(self.doc)
        
        self.Move(pos)
        self.SetSize(size)
    def OnOpenOk(self,evt):
        self.trData.SetNode(None)
    def OnItemSel(self,evt):
        ti=evt.GetTreeNode()
        tid=evt.GetTreeNodeData()
        self.sSelTag=self.trData.GetItemText(ti)
        self.nodeSel=tid
    def __setCfg__(self):
        try:
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            #sz=self.GetParent().GetSizeTuple()
            #self.dCfg['size']='%d,%d'%(sz)
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnMove(self, event):
        event.Skip()
        try:
            #pos=self.GetParent().GetPosition()
            #self.dCfg['pos']='%d,%d'%(pos)
            #self.GetCfgData()
            pass
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)

    def OnCbOpenButton(self, event):
        event.Skip()
        self.doc.Close()
        self.doc.Open(self.fbbFN.GetValue())

    def __procMod__(self,node,doc,d):
        sTag=doc.getTagName(node)
        if sTag in d:
            sFID=doc.getAttribute(node,'fid')
            if len(sFID)>0:
                i=sFID.find('@')
                if i<0:
                    sFID='@'.join([sFID,d[sTag]])
                else:
                    sFID='@'.join([sFID[:i],d[sTag]])
                doc.setAttribute(node,'fid',sFID)
        doc.procChildsExt(node,self.__procMod__,doc,d)
        return 0
    def DoMod(self,doc):
        d={}
        for sTag,sForeign in self.lTag:
            d[sTag]=sForeign
        doc.procChildsExt(doc.getBaseNode(),self.__procMod__,doc,d)
        doc.Save()
        self.thrMod.Stop()
    def OnCbModButton(self, event):
        event.Skip()
        self.thrMod.Start()
        self.thdMod.Do(self.DoMod,self.doc)
    def OnLstForeignListItemDeselected(self, event):
        event.Skip()
        self.iSelForeign=-1

    def OnLstForeignListItemSelected(self, event):
        event.Skip()
        self.iSelForeign=event.GetIndex()
    def __showForeign__(self):
        self.iSelForeign=-1
        self.lstForeign.DeleteAllItems()
        self.lTag.sort()
        for sTag,sForeign in self.lTag:
            idx=self.lstForeign.InsertStringItem(sys.maxint,sTag)
            self.lstForeign.SetStringItem(idx,1,sForeign)
    def OnCbAddButton(self, event):
        event.Skip()
        if self.nodeSel is not None:
            sFor=self.txtForeign.GetValue()
            if len(sFor)==0:
                return
            self.lTag.append((self.sSelTag,sFor))
            self.__showForeign__()
    def OnCbDelButton(self, event):
        event.Skip()
        if self.iSelForeign<0:
            return
        del self.lTag[self.iSelForeign]
        self.lstForeign.DeleteItem(self.iSelForeign)
