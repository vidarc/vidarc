#----------------------------------------------------------------------------
# Name:         vNetDom.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060718
# CVS-ID:       $Id: vNetDom.py,v 1.3 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.net.vNetXmlWxGui import *

class vNetDom(vtXmlDom,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vtXmlDom.__init__(self,attr=attr,skip=skip,synch=synch,verbose=verbose)
        vtXmlDom.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlPrj.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlPrj.IsRunning(self):
    #        return True
    #    return False
    def getBaseNode(self):
        if self.root is None:
            return None
        for c in self.getChilds(self.root):
            if self.hasAttribute(c,'fp'):
                return c
        return self.root
    def New(self,root='domRoot'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'New',origin=self.appl)
        vNetXmlWxGui.New(self)
        vtXmlDom.New(self,root=root,bConnection=self.IsFlag(self.CONN_ACTIVE))
    def GetFN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vtXmlDom.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Open',origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vtXmlDom.New()
            return -2
        if bThread:
            self.silent=silent
            iRet=vtXmlDom.Open(self,fn,True)
        else:
            iRet=vtXmlDom.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vtXmlDom.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vtXmlDom.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vtXmlDom.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'delNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.delNode(self,node)
        vtXmlDom.deleteNode(self,node)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vtXmlDom.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vtXmlDom.__stop__(self)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyContent',origin=self.appl)
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyGetNode',origin=self.appl)
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyAddNode',origin=self.appl)
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyRemoveNode',origin=self.appl)
        vNetXmlWxGui.NotifyRemoveNode(self,id)
