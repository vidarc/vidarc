#Boa:Panel:vArMsgTopPanel
#----------------------------------------------------------------------------
# Name:         vArMsgTopPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070124
# CVS-ID:       $Id: vArMsgTopPanel.py,v 1.2 2007/07/03 08:54:24 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.vAutoRun.common.vArcMainPanel import vArcMainPanel
    import vidarc.vAutoRun.vMsg.images as imgMsg
    from vidarc.vAutoRun.vMsg.vArMsgMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgMsg.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgMsg.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vXXXMDIFrame(parent,id,pos,size,style,name)

[wxID_VARMSGTOPPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vArMsgTopPanel(wx.Panel,vArcMainPanel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=-1,
              name=u'vArMsgTopPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(100, 80), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(92, 73))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        
        vArcMainPanel.__init__(self,u'AutoRunMsg')
        self.dCfg.update({'x':0,'y':0,'width':100,'height':80})
        
        try:
            self.pn=vArMsgMainPanel(self,-1,
                    wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,
                    'vAutoRunMsg')
            self.pn.OpenCfgFile('vArMsgCfg.xml')
            bxs=wx.BoxSizer(orient=wx.VERTICAL)
            bxs.AddWindow(self.pn, 1, border=0, flag=wx.ALL|wx.EXPAND)
            self.SetSizer(bxs)
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            #EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            #EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            #EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            #EVT_NET_XML_SYNCH_START(self.netDocMain,self.OnSynchStart)
            #EVT_NET_XML_SYNCH_PROC(self.netDocMain,self.OnSynchProc)
            #EVT_NET_XML_SYNCH_FINISHED(self.netDocMain,self.OnSynchFinished)
            #EVT_NET_XML_SYNCH_ABORTED(self.netDocMain,self.OnSynchAborted)
            
            #EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            #self.AddMenus(self , self.trMain , self.netMaster,
            #    self.mnFile , self.mnView , self.mnTools)
        
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        self.SetName(name)
        #self.Layout()
        #self.Refresh()
