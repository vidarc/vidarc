#----------------------------------------------------------------------------
# Name:         vArMsgMainPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070124
# CVS-ID:       $Id: vArMsgMainPanel.py,v 1.5 2010/03/03 02:17:18 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.tool.art.vtThrobber import vtThrobberDelay
    
    from vidarc.tool.xml.vtXmlMsgList import vtXmlMsgList
    from vidarc.tool.xml.vtXmlMsgList import EVT_XML_MSG_LIST_VIEW
    from vidarc.tool.xml.vtXmlMsgList import EVT_XML_MSG_LIST_GOTO
    from vidarc.tool.xml.vtXmlMsgList import EVT_XML_MSG_LIST_FINISHED
    from vidarc.tool.xml.vtXmlNodeMsgPanel import vtXmlNodeMsgPanel
    from vidarc.tool.xml.vtXmlNodeMsgPanel import EVT_VTMSG_PANEL_GOTO
    from vidarc.tool.net.vtNetSecXmlGuiSlaveMultiple import *
    
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    from vidarc.vApps.vHum.vNetHum import vNetHum
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')

#import vidarc.tool.xml.vtXmlDom as vtXmlDom

wxEVT_VTXML_MSG_LISTNAV_GOTO=wx.NewEventType()
vEVT_VTXML_MSG_LISTNAV_GOTO=wx.PyEventBinder(wxEVT_VTXML_MSG_LISTNAV_GOTO,1)
def EVT_VTXML_MSG_LISTNAV_GOTO(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_MSG_LISTNAV_GOTO,func)
def EVT_VTXML_MSG_LISTNAV_GOTO_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTXML_MSG_LISTNAV_GOTO)
class vtXmlMsgListNavGoto(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_MSG_LISTNAV_GOTO(<widget_name>, self.OnMsgGoTo)
    """
    def __init__(self,obj,appl,alias,fid):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXML_MSG_LISTNAV_GOTO)
        self.appl=appl
        self.alias=alias
        self.fid=fid
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetFID(self):
        return self.fid
    def GetInfos(self):
        return self.appl,self.alias,self.fid

class vArMsgMainPanelTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,name='vtXmlMsgListNavPopup',style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        self.SetMinSize((350,50))
        #id=wx.NewId()
        #wx.EVT_SIZE(self,self.OnSize)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.pnMsg=vtXmlNodeMsgPanel(parent=self,id=-1,pos=(0,0),size=(200,150),style=0,name='pnMsg')
        #EVT_VTMSG_PANEL_VIEW(self.pnMsg,self.OnMsgView)
        EVT_VTMSG_PANEL_GOTO(self.pnMsg,self.OnMsgGoTo)
        
        bxs.AddWindow(self.pnMsg, 1, border=4, flag=wx.ALL|wx.EXPAND)
        self.SetSizer(bxs)
        self.par=parent
        self.Layout()
        self.appl=None
        self.alias=None
        self.fid=None
        self.applStore=None
        self.aliasStore=None
        self.fidStore=None
    def SetApplAliasID(self,appl,alias,fid):
        self.applStore,self.aliasStore,self.fidStore=appl,alias,fid
        print 'set',self.applStore,self.aliasStore,self.fidStore
        if self.IsShown():
            self.__SetApplAliasID__(appl,alias,fid)
    def __SetApplAliasID__(self,appl,alias,fid):
        #print appl,alias,fid
        print '_et',self.appl,self.alias,self.fid
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if appl is None:
            self.pnMsg.Clear()
            self.pnMsg.SetDoc(None)
            self.appl=None
            return
        if self.appl!=appl:
            netDoc=self.par.lstMsg.GetNetDoc(appl,alias)
            #print netDoc
            self.pnMsg.GetNode()
            self.pnMsg.SetDoc(netDoc)
        elif self.alias!=alias:
            netDoc=self.par.lstMsg.GetNetDoc(appl,alias)
            #print netDoc
            self.pnMsg.GetNode()
            self.pnMsg.SetDoc(netDoc)
        self.appl,self.alias,self.fid=appl,alias,fid
        self.pnMsg.SetNodeById(fid)
    def GetInfos(self):
        return self.applStore,self.aliasStore,self.fidStore
    def Is2SetApplAliasID(self):
        if self.appl!=self.applStore:
            return True
        if self.alias!=self.aliasStore:
            return True
        if self.fid!=self.fidStore:
            return True
        return False
    def OnMsgGoTo(self,evt):
        evt.Skip()
        #fid=evt.GetID()
        appl,alias,fid=evt.GetInfos()
        self.par.DoGoToCorr(appl,alias,fid)
    def SetViewed(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        if self.Is2SetApplAliasID():
            self.__SetApplAliasID__(self.applStore,self.aliasStore,
                        self.fidStore)
        self.pnMsg.SetViewed()
    def SetProcessed(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        if self.Is2SetApplAliasID():
            self.__SetApplAliasID__(self.applStore,self.aliasStore,
                        self.fidStore)
        self.pnMsg.SetProcessed()
    def Show(self,bFlag):
        wx.Dialog.Show(self,bFlag)
        if bFlag:
            print 'shw',self.applStore,self.aliasStore,self.fidStore
            self.__SetApplAliasID__(self.applStore,self.aliasStore,
                        self.fidStore)
        else:
            self.pnMsg.GetNode(None)
            self.pnMsg.Clear()
            self.pnMsg.SetDoc(None)
            self.appl=None
            self.alias=None
            self.fid=None

class vArMsgMainPanel(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        #vtXmlDom.initLibXml()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        self.xdCfg=vtXmlDom(appl='vAutoRunMsgCfg',audit_trail=False)
        
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.lstMsg=vtXmlMsgList(parent=self,pos=(0,0),size=wx.DefaultSize)
        bxs.AddWindow(self.lstMsg, 1, border=0, flag=wx.EXPAND|wx.ALL)
        EVT_XML_MSG_LIST_VIEW(self.lstMsg,self.OnMsgView)
        EVT_XML_MSG_LIST_GOTO(self.lstMsg,self.OnMsgGoTo)
        EVT_XML_MSG_LIST_FINISHED(self.lstMsg,self.OnMsgFin)
        
        bxsBt = wx.BoxSizer(orient=wx.VERTICAL)
        self.cbViewed = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'cbViewed',
              parent=self, pos=wx.Point(437, 21), size=wx.Size(31, 30),
              style=0)
        self.cbViewed.SetToolTipString(_(u'set viewed'))
        self.cbViewed.Bind(wx.EVT_BUTTON, self.OnCbViewedButton,self.cbViewed)
        bxsBt.AddWindow(self.cbViewed, 0, border=4, flag=wx.ALIGN_CENTER)

        self.cbProcessed = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbProcessed',
              parent=self, pos=wx.Point(437, 55), size=wx.Size(31, 30),
              style=0)
        self.cbProcessed.SetToolTipString(_(u'set processed'))
        self.cbProcessed.Bind(wx.EVT_BUTTON, self.OnCbProcessedButton,self.cbProcessed)
        bxsBt.AddWindow(self.cbProcessed, 0, border=4, flag=wx.ALIGN_CENTER|wx.TOP)
        
        self.cbUpdate = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbUpdate',
              parent=self, pos=wx.Point(437, 101), size=wx.Size(31, 30),
              style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,self.cbUpdate)
        bxsBt.AddWindow(self.cbUpdate, 0, border=4, flag=wx.ALIGN_CENTER|wx.TOP)
        
        self.tgViewed = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'tgViewed', parent=self,
              pos=wx.Point(437, 21), size=wx.Size(31, 30), style=0)
        self.tgViewed.SetToolTipString(_(u'show viewed'))
        self.tgViewed.Bind(wx.EVT_BUTTON, self.OnTgViewedButton,self.tgViewed)
        bxsBt.AddWindow(self.tgViewed, 0, border=4, flag=wx.ALIGN_CENTER|wx.TOP)

        self.tgProcessed = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'tgProcessed',
              parent=self, pos=wx.Point(437, 55), size=wx.Size(31, 30),
              style=0)
        self.tgProcessed.SetToolTipString(_(u'show processed'))
        self.tgProcessed.Bind(wx.EVT_BUTTON, self.OnTgProcessedButton,self.tgProcessed)
        bxsBt.AddWindow(self.tgProcessed, 0, border=4, flag=wx.ALIGN_CENTER|wx.TOP)

        bxsBtCmd = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.cbGoTo = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Navigate), name=u'cbGoTo',
              parent=self, pos=wx.Point(4, 38), size=wx.Size(31, 30),
              style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,self.cbGoTo)
        bxsBtCmd.AddWindow(self.cbGoTo, 0, border=0, flag=wx.ALIGN_CENTER)
        
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=(0,0), size=(24,24), style=wx.BU_AUTODRAW)
        bxsBtCmd.AddWindow(self.cbPopup, 0, border=8, flag=wx.ALIGN_CENTER|wx.TOP)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)
        
        self.netSlave = vtNetSecXmlGuiSlaveMultiple(id=-1,
              name=u'netSlave', parent=self, pos=wx.Point(0, 390),
              size=wx.Size(31, 30), style=0)
        bxsBtCmd.AddWindow(self.netSlave, 0, border=4, flag=wx.TOP | wx.ALIGN_CENTER)
        #EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN(self.netSlave,self.OnShutDown)
        
        self.netSlave.InitWidgetStatus(None)
        self.netSlave.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netSlave.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netSlave.AddNetControl(None,None)
        self.SetCfgDoc(self.xdCfg)
        
        self.thrBuild = vtThrobberDelay(id=-1,
              name=u'thrBuild', parent=self, pos=wx.Point(445, 139),
              size=wx.Size(15, 15), style=0)
        bxsBtCmd.AddWindow(self.thrBuild, 0, border=4, flag=wx.ALIGN_CENTER|wx.TOP)
        
        bxs.AddSizer(bxsBtCmd,0,border=4,flag=wx.LEFT | wx.RIGHT)
        bxs.AddSizer(bxsBt,0,border=4,flag=wx.LEFT | wx.RIGHT)
        self.popWin=None
        self.SetSizer(bxs)
        bxs.Layout()
        self.lstMsg.SetShowViewed(self.tgViewed.GetValue())
        self.lstMsg.SetShowProcessed(self.tgProcessed.GetValue())
        self.__createPopup__()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def IsShutDown(self):
        return self.netSlave.IsShutDown()
    def GetNetSlave(self):
        return self.netSlave
    def BindEvents(self,lEventFunc):
        for evt,func in lEventFunc:
            if evt=='navigate':
                EVT_VTXML_MSG_LISTNAV_GOTO(self,func)
    def SetCfgDoc(self,xdCfg):
        self.netSlave.SetCfg(xdCfg,[],['vMsg'])
        self.netSlave.SetCB('vMsg',self.__doAddMsg__,self.lstMsg)
    def SetCfgNode(self):
        self.netSlave.SetCfgNode()
    def OpenFile(self,fn):
        #if self.netContact.ClearAutoFN()>0:
        #    self.netContact.Open(fn)
        pass
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netSlave.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netSlave.SetCfgNode()
        self.__setCfg__()
    def DoConnect(self,idx):
        self.netSlave.DoConnect(idx)
    def __doAddMsg__(self,appl,alias,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s'%(appl,alias),self)
        netDoc=self.netSlave.GetNetDoc(appl,alias)
        self.lstMsg.AddMsg(appl,alias,netDoc,True)
    def OnTgViewedButton(self,evt):
        self.lstMsg.SetShowViewed(self.tgViewed.GetValue())
    def OnTgProcessedButton(self,evt):
        self.lstMsg.SetShowProcessed(self.tgProcessed.GetValue())
    def OnCbUpdateButton(self,evt):
        self.thrBuild.Start()
        self.cbUpdate.Enable(False)
        self.tgViewed.Enable(False)
        self.tgProcessed.Enable(False)
        
        self.lstMsg.Update()
    def OnCbViewedButton(self,evt):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            if self.popWin is None:
                vtLog.vtLngCurWX(vtLog.WARN,'popWin is None'%(),self)
                return
            self.popWin.SetViewed()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbProcessedButton(self,evt):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            if self.popWin is None:
                vtLog.vtLngCurWX(vtLog.WARN,'popWin is None'%(),self)
                return
            self.popWin.SetProcessed()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCfgButton(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        pass
    def OnCbGoToButton(self,evt):
        evt.Skip()
        if self.popWin is None:
            return
        appl,alias,fid=self.popWin.GetInfos()
        self.DoGoTo(appl,alias,fid)
    def __createPopup__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'__createPopup__',
                        self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vArMsgMainPanelTransientPopup(self,sz,wx.SIMPLE_BORDER)
                #self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                #self.popWin.SetNodeTree(self.nodeTree)
                #if self.trSetNode is not None:
                #    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iX+=iW-iPopW
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        
    def OnMsgView(self,evt):
        evt.Skip()
        appl,alias,fid=evt.GetInfos()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s;id:%s'%(appl,alias,fid),self)
        if self.popWin is None:
            return
        self.popWin.SetApplAliasID(appl,alias,fid)
        if self.cbPopup.GetValue()==True:
            self.popWin.SetViewed()
    def OnMsgGoTo(self,evt):
        appl,alias,fid=evt.GetInfos()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s;id:%s'%(appl,alias,fid),self)
        wx.PostEvent(self,vtXmlMsgListNavGoto(self,appl,alias,fid))
    def DoGoToCorr(self,appl,alias,fid):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s;id:%s'%(appl,alias,fid),self)
        wx.PostEvent(self,vtXmlMsgListNavGoto(self,appl,alias,fid))
    def DoGoTo(self,appl,alias,id):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s;id:%s'%(appl,alias,id),self)
        try:
            dNetDoc=self.lstMsg.dNet[(appl,alias)]
            netDoc=dNetDoc['doc']
            tmp=netDoc.getNodeByIdNum(id)
            fid=netDoc.getAttribute(tmp,'fid')
            
            par=netDoc.getParent(tmp)
            sAlias=netDoc.getAttribute(par,'alias')
            appl,alias='',''
            if len(sAlias)>0:
                strs=sAlias.split(':')
                if len(strs)==2:
                    appl=strs[0]
                    alias=strs[1]
                    #idNav=netDoc.getAttribute(self.node,'fid')
                    #wx.PostEvent(self,vtXmlMsgListGoto(self,appl,alias,fid))
                    wx.PostEvent(self,vtXmlMsgListNavGoto(self,appl,alias,fid))
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'no alias defined',self)
        except:
            vtLog.vtLngTB(self.GetName())
        #wx.PostEvent(self,vtXmlMsgListNavGoto(self,appl,alias,fid))
    def OnMsgFin(self,evt):
        self.thrBuild.Stop()
        self.cbUpdate.Enable(True)
        self.tgViewed.Enable(True)
        self.tgProcessed.Enable(True)
    def AddMsg(self,appl,alias,netDoc,bNet):
        self.thrBuild.Start()
        self.cbUpdate.Enable(False)
        self.tgViewed.Enable(False)
        self.tgProcessed.Enable(False)
        self.lstMsg.AddMsg(appl,alias,netDoc,bNet)
    def IsModified(self):
        return self.netSlave.IsModified()
    def Save(self,bModified=False):
        self.netSlave.Save(bModified)
    def Stop(self,bModal=True):
        self.xdCfg.Save()
        self.netSlave.Stop(bModal=bModal)
    def ShutDown(self,bModal=False):
        self.xdCfg.Save()
        self.netSlave.ShutDown(bModal=bModal)
    def Clear(self):
        self.lstMsg.Clear()
        self.netSlave.Close()
    def OnShutDown(self,evt):
        evt.Skip()
        #vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        #self.lstMsg.Clear()
    def GetNetMaster(self):
        try:
            return self.netSlave
        except:
            return None
    def GetDocMain(self):
        return None
    def GetTreeMain(self):
        return None
    def __setCfg__(self):
        pass
