#----------------------------------------------------------------------------
# Name:         vArcMainPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070124
# CVS-ID:       $Id: vArcMainPanel.py,v 1.6 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import time
import sys
import types

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.gui.vtgCore as vtgCore
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLogScreen as vtLogScreen
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

class vArcMainPanel:
    VERBOSE=0
    def __init__(self,appl):
        global _
        _=vtLgBase.assignPluginLang('vArCommon')
        self.appl=appl
        self.dCfg={'x':0,'y':0,'width':200,'height':100}
        self.cfgFunc=None
        self.cfgOrigin=None
        self.bActivated=False
        self.autoConn=None
        
        #self.Bind(wx.EVT_CLOSE, self.OnMainClose)
        # 070703:wro make sure this method is called at first
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnMainClose)
        #self.Bind(wx.EVT_ACTIVATE, self.OnMainActivate)
        self.Bind(wx.EVT_SIZE, self.OnMainFrameSize)
        #self.Bind(wx.EVT_MOVE, self.OnMainFrameMove)
        self.Bind(wx.EVT_IDLE, self.OnMainFrameIdle)
        
        
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def OnExit(self,evt):
        self.Close()
        evt.Skip()
    def __getPluginImage__(self):
        return None
    def __getPluginBitmap__(self):
        return None
    def SetCfgData(self,func,l,d):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(d)
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg.update(d)
        try:
            self.pn.SetCfgData(func,l,self.dCfg)
        except:
            pass
        try:
            x=int(d['x'])
            y=int(d['y'])
            #self.GetParent().Move(x,y)
            #wx.CallAfter(self.Move,(x,y))
            #sz=map(int,self.dCfg['size'].split(','))
            #self.GetParent().SetSize(sz)
        except:
            pass
        try:
            self.SetSize((int(d['width']),int(d['height'])))
        except:
            pass#vtLog.vtLngTB(self.GetName())
        
    def GetCfgData(self):
        try:
            self.pn.GetCfgData()
        except:
            pass
        try:
            pos=self.GetPositionTuple()
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            #iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['x']=str(iX)
            self.dCfg['y']=str(iY)
            sz=self.GetSize()
            iWidth=sz[0]
            iHeight=sz[1]
            #iWidth-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['width']=str(iWidth)
            self.dCfg['height']=str(iHeight)
        except:
            vtLog.vtLngTB(self.GetName())
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dCfg)
    def OnSetNode(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('set response, permission denied %s')%(evt.GetID()))
    def OnDelNode(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('del response, permission denied %s')%(evt.GetID()))
    def OnAddResponse(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('add response, permission denied %s')%(evt.GetID()))
    def OnMoveResponse(self,evt):
        evt.Skip()
        if evt.GetResponse()=='acl':
            self.PrintMsg(_('move response, permission denied %s')%(evt.GetID()))
    def OnAddElementsProgress(self,evt):
        evt.Skip()
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
    def OnAddElementsFin(self,evt):
        evt.Skip()
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
    def OnMasterStart(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.__makeTitle__()
        self.PrintMsg(_('master started ...'))
    def OnMasterFinish(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('master finished.'))
    def OnMasterShutDown(self,evt):
        evt.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        #self.Destroy()
        #vtLog.CallStack('')
        #self.Show(False)
        self.Close()
    def OnOpenStart(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file opened ...'))
    def OnOpenOk(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file opened and parsed.'))
        #self.__getXmlBaseNodes__()
    def OnOpenFault(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('file open fault.'))
        #self.__getXmlBaseNodes__()
    def __getNetDocOk4Processbar__(self,netDoc):
        try:
            netMaster=self.GetNetMaster()
            appl=netDoc.GetAppl()
            appls=netMaster.GetAppls()
            idx=appls.index(appl)
            for a in appls[:idx]:
                tmp=netMaster.GetNetDoc(a)
                if tmp.IsSettled()==False:
                    return False
            return True
        except:
            vtLog.vtLngTB(self.GetName())
        return False
    def OnSynchStart(self,evt):
        evt.Skip()
        netDoc=evt.GetNetDoc()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
        self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
    def OnSynchProc(self,evt):
        evt.Skip()
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.INFO,'',self)
        netDoc=evt.GetNetDoc()
        if self.__getNetDocOk4Processbar__(netDoc):
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
            self.gProcess.SetRange(evt.GetCount())
            self.gProcess.SetValue(evt.GetAct())
            self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
    def OnSynchFinished(self,evt):
        evt.Skip()
        netDoc=evt.GetNetDoc()
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%netDoc.GetAppl(),self)
        self.PrintMsg(netDoc.GetAppl()+ u' '+_('synch finished.'))
        if self.__getNetDocOk4Processbar__(netDoc):
            self.gProcess.SetValue(0)
            self.__makeTitle__()
        #self.__getXmlBaseNodes__()
    def OnSynchAborted(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('synch aborted.'))
        #self.__getXmlBaseNodes__()
    def OnGotContent(self,evt):
        evt.Skip()
        self.PrintMsg(_('content got. generate ...'))
        #self.setDoc()
    def OnConnect(self,evt):
        evt.Skip()
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
    def OnDisConnect(self,evt):
        evt.Skip()
        self.PrintMsg(_('connection lost.'))
    def OnLoggedin(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('get content ... '))
    def OnChanged(self,evt):
        evt.Skip()
        self.__setModified__(True)
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            netMaster=self.pn.GetNetMaster()
            if netMaster is not None:
                if self.autoConn is not None:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'autoconn:%s'%(vtLog.pformat(self.autoConn)),self)
                    wx.CallAfter(netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                    self.autoConn=None
                    #wx.CallAfter(self.netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                    #self.autoConn=None
                    #self.autoConn=None
                    #self.bActivated=True
                else:
                    netMaster.ShowPopup()
            self.Disconnect(-1,-1,wx.EVT_IDLE.evtType[0])
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        event.Skip()
        if self.IsShown():
            if self.bActivated==False:
                if 0:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'autoconn:%s'%(vtLog.pformat(self.autoConn)),self)
                    if self.autoConn is not None:
                        netMaster=self.pn.GetNetMaster()
                        wx.CallAfter(netMaster.DoAutoConnect,self.autoConn[0],self.autoConn[1])
                        self.autoConn=None
                        #self.netMaster.ShowPopup()
                        pass
                    else:
                        netMaster=self.pn.GetNetMaster()
                        netMaster.ShowPopup()
                self.bActivated=True
            self.Disconnect(-1,-1,wx.EVT_ACTIVATE.evtType[0])
    def OnMainFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            #self.dCfg['x']=str(pos[0])
            #self.dCfg['y']=str(pos[1])
            iX=pos[0]
            iY=pos[1]
            #iX-=wx.SystemSettings.GetMetric(wx.SYS_MENU_X)
            iY-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            self.dCfg['x']=str(iX)
            self.dCfg['y']=str(iY)
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(self.dCfg)
                print pos
                print iX,iY
        except:
            pass
    def OnMainFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            self.dCfg['width']=str(sz[0])
            self.dCfg['height']=str(sz[1])
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(self.dCfg)
                print sz
        except:
            pass
    def OnMainClose(self, event):
        try:
            vtLog.vtLngCur(vtLog.INFO,'CanVeto:%s'%(event.CanVeto()),'')
            vtLog.vtLngCurWX(vtLog.INFO,'CanVeto:%s'%(event.CanVeto()),self)
            if self.netMaster is not None:
                if self.netMaster.IsShutDownActive()==False:
                    if event.CanVeto():
                        if self.netMaster.IsModified()==True:
                            dlg = vtmMsgDialog(self,
                                        _(u'Unsaved data present!\n\nDo you want to save data?'),
                                        u'VIDARC ' + self.appl + u' '+ _(u'Close'),
                                        wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                                        )
                            if dlg.ShowModal()==wx.ID_YES:
                                self.netMaster.Save()
                        self.pn.ShutDown()
                        event.Veto()
                        vtLog.vtLngCurWX(vtLog.INFO,'veto',self)
                    else:
                        event.Skip()
                else:
                    if self.netMaster.IsShutDownFinished()==False:
                        if event.CanVeto():
                            event.Veto()
                        else:
                            event.Skip()
                    else:
                        event.Skip()
            else:
                event.Skip()
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        except:
            import traceback
            traceback.print_stack()
            traceback.print_exc()
            vtLog.vtLngTB('')
            #vtLog.vtLngTB(self.GetName())
            event.Skip()
    def GetDocMain(self):
        try:
            return self.pn.GetDocMain()
        except:
            return None
    def GetNetMaster(self):
        try:
            return self.pn.GetNetMaster()
        except:
            return None
    def GetTreeMain(self):
        try:
            return self.pn.GetTreeMain()
        except:
            return None
    def BindEvents(self,lOrigEventFunc):
        import vidarc.tool.net.vNetXmlWxGuiEvents as vtNetEvents
        netMaster=self.GetNetMaster()
        for origin,lEventFunc in lOrigEventFunc:
            try:
                if origin is None:
                    if hasattr(self.pn,'BindEvents'):
                        self.pn.BindEvents(lEventFunc)
                else:
                    netDoc=netMaster.GetNetDoc(origin)
                    for evt,func in lEventFunc:
                        if hasattr(vtNetEvents,evt):
                            vtNetEvents.__dict__[evt](netDoc,func)
            except:
                vtLog.vtLngTB(self.GetName())
    def __bindNetDocEvents__(self,appl,lEvents=None):
        try:
            import vidarc.tool.net.vNetXmlWxGuiEvents as vtNetEvents
            netMaster=self.GetNetMaster()
            netDoc=netMaster.GetNetDoc(appl)
            if lEvents is None:
                lEvents=['synch']
            for evt in lEvents:
                if evt=='synch':
                    vtNetEvents.EVT_NET_XML_SYNCH_START(netDoc,self.OnSynchStart)
                    vtNetEvents.EVT_NET_XML_SYNCH_PROC(netDoc,self.OnSynchProc)
                    vtNetEvents.EVT_NET_XML_SYNCH_FINISHED(netDoc,self.OnSynchFinished)
                    vtNetEvents.EVT_NET_XML_SYNCH_ABORTED(netDoc,self.OnSynchAborted)
        except:
            vtLog.vtLngTB(self.GetName())
    def BindNetDocEvents(self,appl=None,lEvents=None):
        try:
            netMaster=self.GetNetMaster()
            if appl is None:
                appls=netMaster.GetAppls()
                for appl in appls:
                    self.__bindNetDocEvents__(appl)
            else:
                self.__bindNetDocEvents__(appl)
        except:
            vtLog.vtLngTB(self.GetName())
    def DoAutoConnect(self,name,bOffline=False):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'name:%s;offline:%s'%(name,bOffline),self)
            if self.bActivated:
                #netMaster=self.pn.GetNetMaster()
                #self.netMaster.DoAutoConnect(name,bOffline)
                #wx.CallAfter(netMaster.DoAutoConnect,name,bOffline)
                self.autoConn=[name,bOffline]
            else:
                self.autoConn=[name,bOffline]
                #netMaster=self.pn.GetNetMaster()
                #wx.CallAfter(netMaster.DoAutoConnect,name,bOffline)
            #netMaster=self.pn.GetNetMaster()
            #self.bActivated=True
            #netMaster.DoAutoConnect(name,bOffline)
            vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
        except:
            vtLog.vtLngTB(self.GetName())
    def __get_refcounts(self):
        d = {}
        # collect all classes
        for m in sys.modules.values():
            for sym in dir(m):
                o = getattr (m, sym)
                if type(o) is types.ClassType:
                    d[o] = sys.getrefcount (o)
        # sort by refcount
        pairs = map (lambda x: (x[1],x[0]), d.items())
        pairs.sort()
        pairs.reverse()
        return pairs
    
    def __print_top_100(self):
        for n, c in self.__get_refcounts()[:100]:
            print '%10d %s' % (n, c.__name__)

