#----------------------------------------------------------------------------
# Name:         vTimeStamp.py
# Purpose:      time stamping object, which uses high resolution time info
#               in UTC zone
#
# Author:       Walter Obweger
#
# Created:      20071023
# CVS-ID:       $Id: vTimeStamp.py,v 1.4 2009/04/26 21:51:44 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import datetime
__import__('time')
from threading import Lock
# A UTC class.
ZERO = datetime.timedelta(0)

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO

utc = UTC()
class vTimeStamp:
    def __init__(self):
        self.__semAccess=Lock()
        self.dt=datetime.datetime(2007,10,23)#,tzinfo=utc)
        self.Now()
    def Now(self):
        try:
            self.__semAccess.acquire()
            #self.dt=self.dt.now(utc)
            dt=self.dt.utcnow()
            s=u'%04d-%02d-%02dT%02d:%02d:%02d.%06d'%(
                dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second,dt.microsecond)
            self.dt=dt
            return s
        finally:
            self.__semAccess.release()
    def __str__(self):
        try:
            self.__semAccess.acquire()
            dt=self.dt
            s=u'%04d-%02d-%02dT%02d:%02d:%02d.%06d'%(
                dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second,dt.microsecond)
            return s
        finally:
            self.__semAccess.release()
    def __repr__(self):
        return self.__str__()
    def Get(self):
        return self.__str__()
    def Get4FN(self):
        try:
            self.__semAccess.acquire()
            dt=self.dt
            s=u'%04d%02d%02d_%02d%02d%02d'%(
                dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)
            return s
        finally:
            self.__semAccess.release()

#__all__=['getTimeStamp','getTimeStamp4FN']


zDt=vTimeStamp()
def getTimeStamp():
    global zDt
    return zDt.Now()
    #return zDt.Get()
def getTimeStamp4FN():
    global zDt
    zDt.Now()
    return zDt.Get4FN()
