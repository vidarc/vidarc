#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20070121
# CVS-ID:       $Id: __init__.py,v 1.1 2007/01/22 16:52:22 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#PLUGABLE='vProfMDIFrame'
#PLUGABLE_FRAME=True
#PLUGABLE_ACTIVE=0

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
