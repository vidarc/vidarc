#Boa:Frame:vProfMainFrame
#----------------------------------------------------------------------------
# Name:         vProfMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070121
# CVS-ID:       $Id: vProfMainFrame.py,v 1.2 2008/05/29 01:23:06 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    
    import vidarc.vProg.vProf.images as imgProf
    from vidarc.vProg.vProf.vProfMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgProf.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgProf.getApplicationBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vProfMain'):
    return vProfMainFrame(parent,id,pos,size,style,name)

[wxID_VPROFMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vProfMainFrame(wx.Frame,vMDIFrame,vCfg):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VPROFMAINFRAME, name=u'vProfMainFrame',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(400, 250),
              style=wx.DEFAULT_FRAME_STYLE, title=u'vProf')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Prof')
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vProfMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vProfMainPanel')
            self.pn.OpenCfgFile('vProfCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        
        except:
            vtLog.vtLngTB(self.GetName())
        self.SetName(name)
        self.dCfg.update(self._getCfgData(['vProfGui']))
        self.pn.SetCfgData(self._setCfgData,['vProfGui'],self.dCfg)
        self.__setCfg__()
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def SetLocalDN(self,dn):
        self.localDN=dn
        try:
            import vidarc.config.vcCust as vcCust
            vcCust.USR_LOCAL_DN=self.localDN
        except:
            vtLog.vtLngTB(self.GetName())
    def OpenCfgFile(self,fn=None):
        self.pn.OpenCfgFile(fn)
        self.dCfg.update(self._getCfgData(['vProfGui']))
        self.pn.SetCfgData(self._setCfgData,['vProfGui'],self.dCfg)
        self.__setCfg__()
    def __setCfg__(self):
        try:
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def __getPluginImage__(self):
        return imgProf.getPluginImage()
    def __getPluginBitmap__(self):
        return imgProf.getPluginBitmap()
    def OnMainClose(self,event):
        if self.pn.xdCfg is not None:
            self._setCfgData(['vProfGui'],self.dCfg)
            self.pn.xdCfg.Save()
        vMDIFrame.OnMainClose(self,event)
